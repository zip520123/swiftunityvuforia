﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t1111370293;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2904461592;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t2057874452;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t522200103;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2413660594;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t863727431;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// PlayMaker.ConditionalExpression.CompiledAst
struct CompiledAst_t1416449954;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.String
struct String_t;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ARRAYPASTETYPE_T1937951914_H
#define ARRAYPASTETYPE_T1937951914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayTransferValue/ArrayPasteType
struct  ArrayPasteType_t1937951914 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayTransferValue/ArrayPasteType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ArrayPasteType_t1937951914, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYPASTETYPE_T1937951914_H
#ifndef ARRAYTRANSFERTYPE_T2458748193_H
#define ARRAYTRANSFERTYPE_T2458748193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayTransferValue/ArrayTransferType
struct  ArrayTransferType_t2458748193 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayTransferValue/ArrayTransferType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ArrayTransferType_t2458748193, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYTRANSFERTYPE_T2458748193_H
#ifndef ASSERTTYPE_T117737542_H
#define ASSERTTYPE_T117737542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Assert/AssertType
struct  AssertType_t117737542 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.Assert/AssertType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AssertType_t117737542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERTTYPE_T117737542_H
#ifndef UPDATETYPE_T4176746407_H
#define UPDATETYPE_T4176746407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseUpdateAction/UpdateType
struct  UpdateType_t4176746407 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.BaseUpdateAction/UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t4176746407, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T4176746407_H
#ifndef FLOATROUNDING_T1677084411_H
#define FLOATROUNDING_T1677084411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertFloatToInt/FloatRounding
struct  FloatRounding_t1677084411 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.ConvertFloatToInt/FloatRounding::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatRounding_t1677084411, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATROUNDING_T1677084411_H
#ifndef SHAPETYPE_T1684390997_H
#define SHAPETYPE_T1684390997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugDrawShape/ShapeType
struct  ShapeType_t1684390997 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.DebugDrawShape/ShapeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShapeType_t1684390997, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPETYPE_T1684390997_H
#ifndef DESTINATION_T3397433683_H
#define DESTINATION_T3397433683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TakeScreenshot/Destination
struct  Destination_t3397433683 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.TakeScreenshot/Destination::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Destination_t3397433683, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTINATION_T3397433683_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef LOGLEVEL_T183558972_H
#define LOGLEVEL_T183558972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.LogLevel
struct  LogLevel_t183558972 
{
public:
	// System.Int32 HutongGames.PlayMaker.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t183558972, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T183558972_H
#ifndef AVATARTARGET_T2782276764_H
#define AVATARTARGET_T2782276764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarTarget
struct  AvatarTarget_t2782276764 
{
public:
	// System.Int32 UnityEngine.AvatarTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AvatarTarget_t2782276764, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARTARGET_T2782276764_H
#ifndef DEVICEORIENTATION_T3526859474_H
#define DEVICEORIENTATION_T3526859474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DeviceOrientation
struct  DeviceOrientation_t3526859474 
{
public:
	// System.Int32 UnityEngine.DeviceOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeviceOrientation_t3526859474, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEORIENTATION_T3526859474_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef APPLICATIONQUIT_T2436658396_H
#define APPLICATIONQUIT_T2436658396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ApplicationQuit
struct  ApplicationQuit_t2436658396  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONQUIT_T2436658396_H
#ifndef APPLICATIONRUNINBACKGROUND_T2120927555_H
#define APPLICATIONRUNINBACKGROUND_T2120927555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ApplicationRunInBackground
struct  ApplicationRunInBackground_t2120927555  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ApplicationRunInBackground::runInBackground
	FsmBool_t163807967 * ___runInBackground_14;

public:
	inline static int32_t get_offset_of_runInBackground_14() { return static_cast<int32_t>(offsetof(ApplicationRunInBackground_t2120927555, ___runInBackground_14)); }
	inline FsmBool_t163807967 * get_runInBackground_14() const { return ___runInBackground_14; }
	inline FsmBool_t163807967 ** get_address_of_runInBackground_14() { return &___runInBackground_14; }
	inline void set_runInBackground_14(FsmBool_t163807967 * value)
	{
		___runInBackground_14 = value;
		Il2CppCodeGenWriteBarrier((&___runInBackground_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONRUNINBACKGROUND_T2120927555_H
#ifndef ARRAYADD_T3533783165_H
#define ARRAYADD_T3533783165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayAdd
struct  ArrayAdd_t3533783165  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayAdd::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayAdd::value
	FsmVar_t2413660594 * ___value_15;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayAdd_t3533783165, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_value_15() { return static_cast<int32_t>(offsetof(ArrayAdd_t3533783165, ___value_15)); }
	inline FsmVar_t2413660594 * get_value_15() const { return ___value_15; }
	inline FsmVar_t2413660594 ** get_address_of_value_15() { return &___value_15; }
	inline void set_value_15(FsmVar_t2413660594 * value)
	{
		___value_15 = value;
		Il2CppCodeGenWriteBarrier((&___value_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYADD_T3533783165_H
#ifndef ARRAYADDRANGE_T2097782657_H
#define ARRAYADDRANGE_T2097782657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayAddRange
struct  ArrayAddRange_t2097782657  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayAddRange::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.ArrayAddRange::variables
	FsmVarU5BU5D_t863727431* ___variables_15;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayAddRange_t2097782657, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(ArrayAddRange_t2097782657, ___variables_15)); }
	inline FsmVarU5BU5D_t863727431* get_variables_15() const { return ___variables_15; }
	inline FsmVarU5BU5D_t863727431** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmVarU5BU5D_t863727431* value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYADDRANGE_T2097782657_H
#ifndef ARRAYCLEAR_T1592763762_H
#define ARRAYCLEAR_T1592763762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayClear
struct  ArrayClear_t1592763762  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayClear::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayClear::resetValue
	FsmVar_t2413660594 * ___resetValue_15;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayClear_t1592763762, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_resetValue_15() { return static_cast<int32_t>(offsetof(ArrayClear_t1592763762, ___resetValue_15)); }
	inline FsmVar_t2413660594 * get_resetValue_15() const { return ___resetValue_15; }
	inline FsmVar_t2413660594 ** get_address_of_resetValue_15() { return &___resetValue_15; }
	inline void set_resetValue_15(FsmVar_t2413660594 * value)
	{
		___resetValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___resetValue_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYCLEAR_T1592763762_H
#ifndef ARRAYCOMPARE_T1926373437_H
#define ARRAYCOMPARE_T1926373437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayCompare
struct  ArrayCompare_t1926373437  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayCompare::array1
	FsmArray_t1756862219 * ___array1_14;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayCompare::array2
	FsmArray_t1756862219 * ___array2_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayCompare::SequenceEqual
	FsmEvent_t3736299882 * ___SequenceEqual_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayCompare::SequenceNotEqual
	FsmEvent_t3736299882 * ___SequenceNotEqual_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayCompare::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_array1_14() { return static_cast<int32_t>(offsetof(ArrayCompare_t1926373437, ___array1_14)); }
	inline FsmArray_t1756862219 * get_array1_14() const { return ___array1_14; }
	inline FsmArray_t1756862219 ** get_address_of_array1_14() { return &___array1_14; }
	inline void set_array1_14(FsmArray_t1756862219 * value)
	{
		___array1_14 = value;
		Il2CppCodeGenWriteBarrier((&___array1_14), value);
	}

	inline static int32_t get_offset_of_array2_15() { return static_cast<int32_t>(offsetof(ArrayCompare_t1926373437, ___array2_15)); }
	inline FsmArray_t1756862219 * get_array2_15() const { return ___array2_15; }
	inline FsmArray_t1756862219 ** get_address_of_array2_15() { return &___array2_15; }
	inline void set_array2_15(FsmArray_t1756862219 * value)
	{
		___array2_15 = value;
		Il2CppCodeGenWriteBarrier((&___array2_15), value);
	}

	inline static int32_t get_offset_of_SequenceEqual_16() { return static_cast<int32_t>(offsetof(ArrayCompare_t1926373437, ___SequenceEqual_16)); }
	inline FsmEvent_t3736299882 * get_SequenceEqual_16() const { return ___SequenceEqual_16; }
	inline FsmEvent_t3736299882 ** get_address_of_SequenceEqual_16() { return &___SequenceEqual_16; }
	inline void set_SequenceEqual_16(FsmEvent_t3736299882 * value)
	{
		___SequenceEqual_16 = value;
		Il2CppCodeGenWriteBarrier((&___SequenceEqual_16), value);
	}

	inline static int32_t get_offset_of_SequenceNotEqual_17() { return static_cast<int32_t>(offsetof(ArrayCompare_t1926373437, ___SequenceNotEqual_17)); }
	inline FsmEvent_t3736299882 * get_SequenceNotEqual_17() const { return ___SequenceNotEqual_17; }
	inline FsmEvent_t3736299882 ** get_address_of_SequenceNotEqual_17() { return &___SequenceNotEqual_17; }
	inline void set_SequenceNotEqual_17(FsmEvent_t3736299882 * value)
	{
		___SequenceNotEqual_17 = value;
		Il2CppCodeGenWriteBarrier((&___SequenceNotEqual_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(ArrayCompare_t1926373437, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(ArrayCompare_t1926373437, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYCOMPARE_T1926373437_H
#ifndef ARRAYCONTAINS_T1590434515_H
#define ARRAYCONTAINS_T1590434515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayContains
struct  ArrayContains_t1590434515  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayContains::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayContains::value
	FsmVar_t2413660594 * ___value_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayContains::index
	FsmInt_t874273141 * ___index_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayContains::isContained
	FsmBool_t163807967 * ___isContained_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayContains::isContainedEvent
	FsmEvent_t3736299882 * ___isContainedEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayContains::isNotContainedEvent
	FsmEvent_t3736299882 * ___isNotContainedEvent_19;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_value_15() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515, ___value_15)); }
	inline FsmVar_t2413660594 * get_value_15() const { return ___value_15; }
	inline FsmVar_t2413660594 ** get_address_of_value_15() { return &___value_15; }
	inline void set_value_15(FsmVar_t2413660594 * value)
	{
		___value_15 = value;
		Il2CppCodeGenWriteBarrier((&___value_15), value);
	}

	inline static int32_t get_offset_of_index_16() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515, ___index_16)); }
	inline FsmInt_t874273141 * get_index_16() const { return ___index_16; }
	inline FsmInt_t874273141 ** get_address_of_index_16() { return &___index_16; }
	inline void set_index_16(FsmInt_t874273141 * value)
	{
		___index_16 = value;
		Il2CppCodeGenWriteBarrier((&___index_16), value);
	}

	inline static int32_t get_offset_of_isContained_17() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515, ___isContained_17)); }
	inline FsmBool_t163807967 * get_isContained_17() const { return ___isContained_17; }
	inline FsmBool_t163807967 ** get_address_of_isContained_17() { return &___isContained_17; }
	inline void set_isContained_17(FsmBool_t163807967 * value)
	{
		___isContained_17 = value;
		Il2CppCodeGenWriteBarrier((&___isContained_17), value);
	}

	inline static int32_t get_offset_of_isContainedEvent_18() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515, ___isContainedEvent_18)); }
	inline FsmEvent_t3736299882 * get_isContainedEvent_18() const { return ___isContainedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_isContainedEvent_18() { return &___isContainedEvent_18; }
	inline void set_isContainedEvent_18(FsmEvent_t3736299882 * value)
	{
		___isContainedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___isContainedEvent_18), value);
	}

	inline static int32_t get_offset_of_isNotContainedEvent_19() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515, ___isNotContainedEvent_19)); }
	inline FsmEvent_t3736299882 * get_isNotContainedEvent_19() const { return ___isNotContainedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotContainedEvent_19() { return &___isNotContainedEvent_19; }
	inline void set_isNotContainedEvent_19(FsmEvent_t3736299882 * value)
	{
		___isNotContainedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isNotContainedEvent_19), value);
	}
};

struct ArrayContains_t1590434515_StaticFields
{
public:
	// System.Predicate`1<System.Object> HutongGames.PlayMaker.Actions.ArrayContains::<>f__am$cache0
	Predicate_1_t3905400288 * ___U3CU3Ef__amU24cache0_20;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_20() { return static_cast<int32_t>(offsetof(ArrayContains_t1590434515_StaticFields, ___U3CU3Ef__amU24cache0_20)); }
	inline Predicate_1_t3905400288 * get_U3CU3Ef__amU24cache0_20() const { return ___U3CU3Ef__amU24cache0_20; }
	inline Predicate_1_t3905400288 ** get_address_of_U3CU3Ef__amU24cache0_20() { return &___U3CU3Ef__amU24cache0_20; }
	inline void set_U3CU3Ef__amU24cache0_20(Predicate_1_t3905400288 * value)
	{
		___U3CU3Ef__amU24cache0_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYCONTAINS_T1590434515_H
#ifndef ARRAYDELETEAT_T1998213995_H
#define ARRAYDELETEAT_T1998213995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayDeleteAt
struct  ArrayDeleteAt_t1998213995  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayDeleteAt::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayDeleteAt::index
	FsmInt_t874273141 * ___index_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayDeleteAt::indexOutOfRangeEvent
	FsmEvent_t3736299882 * ___indexOutOfRangeEvent_16;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayDeleteAt_t1998213995, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_index_15() { return static_cast<int32_t>(offsetof(ArrayDeleteAt_t1998213995, ___index_15)); }
	inline FsmInt_t874273141 * get_index_15() const { return ___index_15; }
	inline FsmInt_t874273141 ** get_address_of_index_15() { return &___index_15; }
	inline void set_index_15(FsmInt_t874273141 * value)
	{
		___index_15 = value;
		Il2CppCodeGenWriteBarrier((&___index_15), value);
	}

	inline static int32_t get_offset_of_indexOutOfRangeEvent_16() { return static_cast<int32_t>(offsetof(ArrayDeleteAt_t1998213995, ___indexOutOfRangeEvent_16)); }
	inline FsmEvent_t3736299882 * get_indexOutOfRangeEvent_16() const { return ___indexOutOfRangeEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_indexOutOfRangeEvent_16() { return &___indexOutOfRangeEvent_16; }
	inline void set_indexOutOfRangeEvent_16(FsmEvent_t3736299882 * value)
	{
		___indexOutOfRangeEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRangeEvent_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYDELETEAT_T1998213995_H
#ifndef ARRAYGET_T3917168766_H
#define ARRAYGET_T3917168766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayGet
struct  ArrayGet_t3917168766  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayGet::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayGet::index
	FsmInt_t874273141 * ___index_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayGet::storeValue
	FsmVar_t2413660594 * ___storeValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayGet::everyFrame
	bool ___everyFrame_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayGet::indexOutOfRange
	FsmEvent_t3736299882 * ___indexOutOfRange_18;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayGet_t3917168766, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_index_15() { return static_cast<int32_t>(offsetof(ArrayGet_t3917168766, ___index_15)); }
	inline FsmInt_t874273141 * get_index_15() const { return ___index_15; }
	inline FsmInt_t874273141 ** get_address_of_index_15() { return &___index_15; }
	inline void set_index_15(FsmInt_t874273141 * value)
	{
		___index_15 = value;
		Il2CppCodeGenWriteBarrier((&___index_15), value);
	}

	inline static int32_t get_offset_of_storeValue_16() { return static_cast<int32_t>(offsetof(ArrayGet_t3917168766, ___storeValue_16)); }
	inline FsmVar_t2413660594 * get_storeValue_16() const { return ___storeValue_16; }
	inline FsmVar_t2413660594 ** get_address_of_storeValue_16() { return &___storeValue_16; }
	inline void set_storeValue_16(FsmVar_t2413660594 * value)
	{
		___storeValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ArrayGet_t3917168766, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_indexOutOfRange_18() { return static_cast<int32_t>(offsetof(ArrayGet_t3917168766, ___indexOutOfRange_18)); }
	inline FsmEvent_t3736299882 * get_indexOutOfRange_18() const { return ___indexOutOfRange_18; }
	inline FsmEvent_t3736299882 ** get_address_of_indexOutOfRange_18() { return &___indexOutOfRange_18; }
	inline void set_indexOutOfRange_18(FsmEvent_t3736299882 * value)
	{
		___indexOutOfRange_18 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRange_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYGET_T3917168766_H
#ifndef ARRAYGETNEXT_T3616358887_H
#define ARRAYGETNEXT_T3616358887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayGetNext
struct  ArrayGetNext_t3616358887  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayGetNext::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayGetNext::startIndex
	FsmInt_t874273141 * ___startIndex_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayGetNext::endIndex
	FsmInt_t874273141 * ___endIndex_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayGetNext::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayGetNext::resetFlag
	FsmBool_t163807967 * ___resetFlag_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayGetNext::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_19;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayGetNext::result
	FsmVar_t2413660594 * ___result_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayGetNext::currentIndex
	FsmInt_t874273141 * ___currentIndex_21;
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayGetNext::nextItemIndex
	int32_t ___nextItemIndex_22;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_startIndex_15() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___startIndex_15)); }
	inline FsmInt_t874273141 * get_startIndex_15() const { return ___startIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_startIndex_15() { return &___startIndex_15; }
	inline void set_startIndex_15(FsmInt_t874273141 * value)
	{
		___startIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___startIndex_15), value);
	}

	inline static int32_t get_offset_of_endIndex_16() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___endIndex_16)); }
	inline FsmInt_t874273141 * get_endIndex_16() const { return ___endIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_endIndex_16() { return &___endIndex_16; }
	inline void set_endIndex_16(FsmInt_t874273141 * value)
	{
		___endIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___endIndex_16), value);
	}

	inline static int32_t get_offset_of_loopEvent_17() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___loopEvent_17)); }
	inline FsmEvent_t3736299882 * get_loopEvent_17() const { return ___loopEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_17() { return &___loopEvent_17; }
	inline void set_loopEvent_17(FsmEvent_t3736299882 * value)
	{
		___loopEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_17), value);
	}

	inline static int32_t get_offset_of_resetFlag_18() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___resetFlag_18)); }
	inline FsmBool_t163807967 * get_resetFlag_18() const { return ___resetFlag_18; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_18() { return &___resetFlag_18; }
	inline void set_resetFlag_18(FsmBool_t163807967 * value)
	{
		___resetFlag_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_18), value);
	}

	inline static int32_t get_offset_of_finishedEvent_19() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___finishedEvent_19)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_19() const { return ___finishedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_19() { return &___finishedEvent_19; }
	inline void set_finishedEvent_19(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_19), value);
	}

	inline static int32_t get_offset_of_result_20() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___result_20)); }
	inline FsmVar_t2413660594 * get_result_20() const { return ___result_20; }
	inline FsmVar_t2413660594 ** get_address_of_result_20() { return &___result_20; }
	inline void set_result_20(FsmVar_t2413660594 * value)
	{
		___result_20 = value;
		Il2CppCodeGenWriteBarrier((&___result_20), value);
	}

	inline static int32_t get_offset_of_currentIndex_21() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___currentIndex_21)); }
	inline FsmInt_t874273141 * get_currentIndex_21() const { return ___currentIndex_21; }
	inline FsmInt_t874273141 ** get_address_of_currentIndex_21() { return &___currentIndex_21; }
	inline void set_currentIndex_21(FsmInt_t874273141 * value)
	{
		___currentIndex_21 = value;
		Il2CppCodeGenWriteBarrier((&___currentIndex_21), value);
	}

	inline static int32_t get_offset_of_nextItemIndex_22() { return static_cast<int32_t>(offsetof(ArrayGetNext_t3616358887, ___nextItemIndex_22)); }
	inline int32_t get_nextItemIndex_22() const { return ___nextItemIndex_22; }
	inline int32_t* get_address_of_nextItemIndex_22() { return &___nextItemIndex_22; }
	inline void set_nextItemIndex_22(int32_t value)
	{
		___nextItemIndex_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYGETNEXT_T3616358887_H
#ifndef ARRAYGETRANDOM_T3984396624_H
#define ARRAYGETRANDOM_T3984396624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayGetRandom
struct  ArrayGetRandom_t3984396624  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayGetRandom::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayGetRandom::storeValue
	FsmVar_t2413660594 * ___storeValue_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayGetRandom::index
	FsmInt_t874273141 * ___index_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayGetRandom::noRepeat
	FsmBool_t163807967 * ___noRepeat_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayGetRandom::everyFrame
	bool ___everyFrame_18;
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayGetRandom::randomIndex
	int32_t ___randomIndex_19;
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayGetRandom::lastIndex
	int32_t ___lastIndex_20;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_storeValue_15() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___storeValue_15)); }
	inline FsmVar_t2413660594 * get_storeValue_15() const { return ___storeValue_15; }
	inline FsmVar_t2413660594 ** get_address_of_storeValue_15() { return &___storeValue_15; }
	inline void set_storeValue_15(FsmVar_t2413660594 * value)
	{
		___storeValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_15), value);
	}

	inline static int32_t get_offset_of_index_16() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___index_16)); }
	inline FsmInt_t874273141 * get_index_16() const { return ___index_16; }
	inline FsmInt_t874273141 ** get_address_of_index_16() { return &___index_16; }
	inline void set_index_16(FsmInt_t874273141 * value)
	{
		___index_16 = value;
		Il2CppCodeGenWriteBarrier((&___index_16), value);
	}

	inline static int32_t get_offset_of_noRepeat_17() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___noRepeat_17)); }
	inline FsmBool_t163807967 * get_noRepeat_17() const { return ___noRepeat_17; }
	inline FsmBool_t163807967 ** get_address_of_noRepeat_17() { return &___noRepeat_17; }
	inline void set_noRepeat_17(FsmBool_t163807967 * value)
	{
		___noRepeat_17 = value;
		Il2CppCodeGenWriteBarrier((&___noRepeat_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_randomIndex_19() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___randomIndex_19)); }
	inline int32_t get_randomIndex_19() const { return ___randomIndex_19; }
	inline int32_t* get_address_of_randomIndex_19() { return &___randomIndex_19; }
	inline void set_randomIndex_19(int32_t value)
	{
		___randomIndex_19 = value;
	}

	inline static int32_t get_offset_of_lastIndex_20() { return static_cast<int32_t>(offsetof(ArrayGetRandom_t3984396624, ___lastIndex_20)); }
	inline int32_t get_lastIndex_20() const { return ___lastIndex_20; }
	inline int32_t* get_address_of_lastIndex_20() { return &___lastIndex_20; }
	inline void set_lastIndex_20(int32_t value)
	{
		___lastIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYGETRANDOM_T3984396624_H
#ifndef ARRAYLENGTH_T1798162410_H
#define ARRAYLENGTH_T1798162410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayLength
struct  ArrayLength_t1798162410  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayLength::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayLength::length
	FsmInt_t874273141 * ___length_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayLength::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayLength_t1798162410, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_length_15() { return static_cast<int32_t>(offsetof(ArrayLength_t1798162410, ___length_15)); }
	inline FsmInt_t874273141 * get_length_15() const { return ___length_15; }
	inline FsmInt_t874273141 ** get_address_of_length_15() { return &___length_15; }
	inline void set_length_15(FsmInt_t874273141 * value)
	{
		___length_15 = value;
		Il2CppCodeGenWriteBarrier((&___length_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ArrayLength_t1798162410, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYLENGTH_T1798162410_H
#ifndef ARRAYRESIZE_T2334538209_H
#define ARRAYRESIZE_T2334538209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayResize
struct  ArrayResize_t2334538209  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayResize::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayResize::newSize
	FsmInt_t874273141 * ___newSize_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayResize::sizeOutOfRangeEvent
	FsmEvent_t3736299882 * ___sizeOutOfRangeEvent_16;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayResize_t2334538209, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_newSize_15() { return static_cast<int32_t>(offsetof(ArrayResize_t2334538209, ___newSize_15)); }
	inline FsmInt_t874273141 * get_newSize_15() const { return ___newSize_15; }
	inline FsmInt_t874273141 ** get_address_of_newSize_15() { return &___newSize_15; }
	inline void set_newSize_15(FsmInt_t874273141 * value)
	{
		___newSize_15 = value;
		Il2CppCodeGenWriteBarrier((&___newSize_15), value);
	}

	inline static int32_t get_offset_of_sizeOutOfRangeEvent_16() { return static_cast<int32_t>(offsetof(ArrayResize_t2334538209, ___sizeOutOfRangeEvent_16)); }
	inline FsmEvent_t3736299882 * get_sizeOutOfRangeEvent_16() const { return ___sizeOutOfRangeEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_sizeOutOfRangeEvent_16() { return &___sizeOutOfRangeEvent_16; }
	inline void set_sizeOutOfRangeEvent_16(FsmEvent_t3736299882 * value)
	{
		___sizeOutOfRangeEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sizeOutOfRangeEvent_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYRESIZE_T2334538209_H
#ifndef ARRAYREVERSE_T2263986487_H
#define ARRAYREVERSE_T2263986487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayReverse
struct  ArrayReverse_t2263986487  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayReverse::array
	FsmArray_t1756862219 * ___array_14;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayReverse_t2263986487, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYREVERSE_T2263986487_H
#ifndef ARRAYSET_T93798526_H
#define ARRAYSET_T93798526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArraySet
struct  ArraySet_t93798526  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArraySet::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArraySet::index
	FsmInt_t874273141 * ___index_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArraySet::value
	FsmVar_t2413660594 * ___value_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ArraySet::everyFrame
	bool ___everyFrame_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArraySet::indexOutOfRange
	FsmEvent_t3736299882 * ___indexOutOfRange_18;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArraySet_t93798526, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_index_15() { return static_cast<int32_t>(offsetof(ArraySet_t93798526, ___index_15)); }
	inline FsmInt_t874273141 * get_index_15() const { return ___index_15; }
	inline FsmInt_t874273141 ** get_address_of_index_15() { return &___index_15; }
	inline void set_index_15(FsmInt_t874273141 * value)
	{
		___index_15 = value;
		Il2CppCodeGenWriteBarrier((&___index_15), value);
	}

	inline static int32_t get_offset_of_value_16() { return static_cast<int32_t>(offsetof(ArraySet_t93798526, ___value_16)); }
	inline FsmVar_t2413660594 * get_value_16() const { return ___value_16; }
	inline FsmVar_t2413660594 ** get_address_of_value_16() { return &___value_16; }
	inline void set_value_16(FsmVar_t2413660594 * value)
	{
		___value_16 = value;
		Il2CppCodeGenWriteBarrier((&___value_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ArraySet_t93798526, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_indexOutOfRange_18() { return static_cast<int32_t>(offsetof(ArraySet_t93798526, ___indexOutOfRange_18)); }
	inline FsmEvent_t3736299882 * get_indexOutOfRange_18() const { return ___indexOutOfRange_18; }
	inline FsmEvent_t3736299882 ** get_address_of_indexOutOfRange_18() { return &___indexOutOfRange_18; }
	inline void set_indexOutOfRange_18(FsmEvent_t3736299882 * value)
	{
		___indexOutOfRange_18 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRange_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSET_T93798526_H
#ifndef ARRAYSHUFFLE_T887312886_H
#define ARRAYSHUFFLE_T887312886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayShuffle
struct  ArrayShuffle_t887312886  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayShuffle::array
	FsmArray_t1756862219 * ___array_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayShuffle::startIndex
	FsmInt_t874273141 * ___startIndex_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayShuffle::shufflingRange
	FsmInt_t874273141 * ___shufflingRange_16;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArrayShuffle_t887312886, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}

	inline static int32_t get_offset_of_startIndex_15() { return static_cast<int32_t>(offsetof(ArrayShuffle_t887312886, ___startIndex_15)); }
	inline FsmInt_t874273141 * get_startIndex_15() const { return ___startIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_startIndex_15() { return &___startIndex_15; }
	inline void set_startIndex_15(FsmInt_t874273141 * value)
	{
		___startIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___startIndex_15), value);
	}

	inline static int32_t get_offset_of_shufflingRange_16() { return static_cast<int32_t>(offsetof(ArrayShuffle_t887312886, ___shufflingRange_16)); }
	inline FsmInt_t874273141 * get_shufflingRange_16() const { return ___shufflingRange_16; }
	inline FsmInt_t874273141 ** get_address_of_shufflingRange_16() { return &___shufflingRange_16; }
	inline void set_shufflingRange_16(FsmInt_t874273141 * value)
	{
		___shufflingRange_16 = value;
		Il2CppCodeGenWriteBarrier((&___shufflingRange_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSHUFFLE_T887312886_H
#ifndef ARRAYSORT_T2823797142_H
#define ARRAYSORT_T2823797142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArraySort
struct  ArraySort_t2823797142  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArraySort::array
	FsmArray_t1756862219 * ___array_14;

public:
	inline static int32_t get_offset_of_array_14() { return static_cast<int32_t>(offsetof(ArraySort_t2823797142, ___array_14)); }
	inline FsmArray_t1756862219 * get_array_14() const { return ___array_14; }
	inline FsmArray_t1756862219 ** get_address_of_array_14() { return &___array_14; }
	inline void set_array_14(FsmArray_t1756862219 * value)
	{
		___array_14 = value;
		Il2CppCodeGenWriteBarrier((&___array_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSORT_T2823797142_H
#ifndef ARRAYTRANSFERVALUE_T4010721501_H
#define ARRAYTRANSFERVALUE_T4010721501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayTransferValue
struct  ArrayTransferValue_t4010721501  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayTransferValue::arraySource
	FsmArray_t1756862219 * ___arraySource_14;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayTransferValue::arrayTarget
	FsmArray_t1756862219 * ___arrayTarget_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayTransferValue::indexToTransfer
	FsmInt_t874273141 * ___indexToTransfer_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.ArrayTransferValue::copyType
	FsmEnum_t2861764163 * ___copyType_17;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.ArrayTransferValue::pasteType
	FsmEnum_t2861764163 * ___pasteType_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayTransferValue::indexOutOfRange
	FsmEvent_t3736299882 * ___indexOutOfRange_19;

public:
	inline static int32_t get_offset_of_arraySource_14() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t4010721501, ___arraySource_14)); }
	inline FsmArray_t1756862219 * get_arraySource_14() const { return ___arraySource_14; }
	inline FsmArray_t1756862219 ** get_address_of_arraySource_14() { return &___arraySource_14; }
	inline void set_arraySource_14(FsmArray_t1756862219 * value)
	{
		___arraySource_14 = value;
		Il2CppCodeGenWriteBarrier((&___arraySource_14), value);
	}

	inline static int32_t get_offset_of_arrayTarget_15() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t4010721501, ___arrayTarget_15)); }
	inline FsmArray_t1756862219 * get_arrayTarget_15() const { return ___arrayTarget_15; }
	inline FsmArray_t1756862219 ** get_address_of_arrayTarget_15() { return &___arrayTarget_15; }
	inline void set_arrayTarget_15(FsmArray_t1756862219 * value)
	{
		___arrayTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___arrayTarget_15), value);
	}

	inline static int32_t get_offset_of_indexToTransfer_16() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t4010721501, ___indexToTransfer_16)); }
	inline FsmInt_t874273141 * get_indexToTransfer_16() const { return ___indexToTransfer_16; }
	inline FsmInt_t874273141 ** get_address_of_indexToTransfer_16() { return &___indexToTransfer_16; }
	inline void set_indexToTransfer_16(FsmInt_t874273141 * value)
	{
		___indexToTransfer_16 = value;
		Il2CppCodeGenWriteBarrier((&___indexToTransfer_16), value);
	}

	inline static int32_t get_offset_of_copyType_17() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t4010721501, ___copyType_17)); }
	inline FsmEnum_t2861764163 * get_copyType_17() const { return ___copyType_17; }
	inline FsmEnum_t2861764163 ** get_address_of_copyType_17() { return &___copyType_17; }
	inline void set_copyType_17(FsmEnum_t2861764163 * value)
	{
		___copyType_17 = value;
		Il2CppCodeGenWriteBarrier((&___copyType_17), value);
	}

	inline static int32_t get_offset_of_pasteType_18() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t4010721501, ___pasteType_18)); }
	inline FsmEnum_t2861764163 * get_pasteType_18() const { return ___pasteType_18; }
	inline FsmEnum_t2861764163 ** get_address_of_pasteType_18() { return &___pasteType_18; }
	inline void set_pasteType_18(FsmEnum_t2861764163 * value)
	{
		___pasteType_18 = value;
		Il2CppCodeGenWriteBarrier((&___pasteType_18), value);
	}

	inline static int32_t get_offset_of_indexOutOfRange_19() { return static_cast<int32_t>(offsetof(ArrayTransferValue_t4010721501, ___indexOutOfRange_19)); }
	inline FsmEvent_t3736299882 * get_indexOutOfRange_19() const { return ___indexOutOfRange_19; }
	inline FsmEvent_t3736299882 ** get_address_of_indexOutOfRange_19() { return &___indexOutOfRange_19; }
	inline void set_indexOutOfRange_19(FsmEvent_t3736299882 * value)
	{
		___indexOutOfRange_19 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRange_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYTRANSFERVALUE_T4010721501_H
#ifndef ASSERT_T3430802513_H
#define ASSERT_T3430802513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Assert
struct  Assert_t3430802513  : public FsmStateAction_t3801304101
{
public:
	// PlayMaker.ConditionalExpression.CompiledAst HutongGames.PlayMaker.Actions.Assert::<Ast>k__BackingField
	CompiledAst_t1416449954 * ___U3CAstU3Ek__BackingField_14;
	// System.String HutongGames.PlayMaker.Actions.Assert::<LastErrorMessage>k__BackingField
	String_t* ___U3CLastErrorMessageU3Ek__BackingField_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.Assert::expression
	FsmString_t1785915204 * ___expression_16;
	// HutongGames.PlayMaker.Actions.Assert/AssertType HutongGames.PlayMaker.Actions.Assert::assert
	int32_t ___assert_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Assert::everyFrame
	bool ___everyFrame_18;
	// System.String HutongGames.PlayMaker.Actions.Assert::cachedExpression
	String_t* ___cachedExpression_19;

public:
	inline static int32_t get_offset_of_U3CAstU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Assert_t3430802513, ___U3CAstU3Ek__BackingField_14)); }
	inline CompiledAst_t1416449954 * get_U3CAstU3Ek__BackingField_14() const { return ___U3CAstU3Ek__BackingField_14; }
	inline CompiledAst_t1416449954 ** get_address_of_U3CAstU3Ek__BackingField_14() { return &___U3CAstU3Ek__BackingField_14; }
	inline void set_U3CAstU3Ek__BackingField_14(CompiledAst_t1416449954 * value)
	{
		___U3CAstU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAstU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CLastErrorMessageU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Assert_t3430802513, ___U3CLastErrorMessageU3Ek__BackingField_15)); }
	inline String_t* get_U3CLastErrorMessageU3Ek__BackingField_15() const { return ___U3CLastErrorMessageU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CLastErrorMessageU3Ek__BackingField_15() { return &___U3CLastErrorMessageU3Ek__BackingField_15; }
	inline void set_U3CLastErrorMessageU3Ek__BackingField_15(String_t* value)
	{
		___U3CLastErrorMessageU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastErrorMessageU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_expression_16() { return static_cast<int32_t>(offsetof(Assert_t3430802513, ___expression_16)); }
	inline FsmString_t1785915204 * get_expression_16() const { return ___expression_16; }
	inline FsmString_t1785915204 ** get_address_of_expression_16() { return &___expression_16; }
	inline void set_expression_16(FsmString_t1785915204 * value)
	{
		___expression_16 = value;
		Il2CppCodeGenWriteBarrier((&___expression_16), value);
	}

	inline static int32_t get_offset_of_assert_17() { return static_cast<int32_t>(offsetof(Assert_t3430802513, ___assert_17)); }
	inline int32_t get_assert_17() const { return ___assert_17; }
	inline int32_t* get_address_of_assert_17() { return &___assert_17; }
	inline void set_assert_17(int32_t value)
	{
		___assert_17 = value;
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(Assert_t3430802513, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_cachedExpression_19() { return static_cast<int32_t>(offsetof(Assert_t3430802513, ___cachedExpression_19)); }
	inline String_t* get_cachedExpression_19() const { return ___cachedExpression_19; }
	inline String_t** get_address_of_cachedExpression_19() { return &___cachedExpression_19; }
	inline void set_cachedExpression_19(String_t* value)
	{
		___cachedExpression_19 = value;
		Il2CppCodeGenWriteBarrier((&___cachedExpression_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERT_T3430802513_H
#ifndef AUDIOMUTE_T195113627_H
#define AUDIOMUTE_T195113627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AudioMute
struct  AudioMute_t195113627  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AudioMute::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AudioMute::mute
	FsmBool_t163807967 * ___mute_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AudioMute_t195113627, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_mute_15() { return static_cast<int32_t>(offsetof(AudioMute_t195113627, ___mute_15)); }
	inline FsmBool_t163807967 * get_mute_15() const { return ___mute_15; }
	inline FsmBool_t163807967 ** get_address_of_mute_15() { return &___mute_15; }
	inline void set_mute_15(FsmBool_t163807967 * value)
	{
		___mute_15 = value;
		Il2CppCodeGenWriteBarrier((&___mute_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMUTE_T195113627_H
#ifndef AUDIOPAUSE_T1465946913_H
#define AUDIOPAUSE_T1465946913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AudioPause
struct  AudioPause_t1465946913  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AudioPause::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AudioPause_t1465946913, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPAUSE_T1465946913_H
#ifndef AUDIOPLAY_T3994223902_H
#define AUDIOPLAY_T3994223902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AudioPlay
struct  AudioPlay_t3994223902  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AudioPlay::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AudioPlay::volume
	FsmFloat_t2883254149 * ___volume_15;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.AudioPlay::oneShotClip
	FsmObject_t2606870197 * ___oneShotClip_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AudioPlay::WaitForEndOfClip
	FsmBool_t163807967 * ___WaitForEndOfClip_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AudioPlay::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_18;
	// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.AudioPlay::audio
	AudioSource_t3935305588 * ___audio_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AudioPlay_t3994223902, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_volume_15() { return static_cast<int32_t>(offsetof(AudioPlay_t3994223902, ___volume_15)); }
	inline FsmFloat_t2883254149 * get_volume_15() const { return ___volume_15; }
	inline FsmFloat_t2883254149 ** get_address_of_volume_15() { return &___volume_15; }
	inline void set_volume_15(FsmFloat_t2883254149 * value)
	{
		___volume_15 = value;
		Il2CppCodeGenWriteBarrier((&___volume_15), value);
	}

	inline static int32_t get_offset_of_oneShotClip_16() { return static_cast<int32_t>(offsetof(AudioPlay_t3994223902, ___oneShotClip_16)); }
	inline FsmObject_t2606870197 * get_oneShotClip_16() const { return ___oneShotClip_16; }
	inline FsmObject_t2606870197 ** get_address_of_oneShotClip_16() { return &___oneShotClip_16; }
	inline void set_oneShotClip_16(FsmObject_t2606870197 * value)
	{
		___oneShotClip_16 = value;
		Il2CppCodeGenWriteBarrier((&___oneShotClip_16), value);
	}

	inline static int32_t get_offset_of_WaitForEndOfClip_17() { return static_cast<int32_t>(offsetof(AudioPlay_t3994223902, ___WaitForEndOfClip_17)); }
	inline FsmBool_t163807967 * get_WaitForEndOfClip_17() const { return ___WaitForEndOfClip_17; }
	inline FsmBool_t163807967 ** get_address_of_WaitForEndOfClip_17() { return &___WaitForEndOfClip_17; }
	inline void set_WaitForEndOfClip_17(FsmBool_t163807967 * value)
	{
		___WaitForEndOfClip_17 = value;
		Il2CppCodeGenWriteBarrier((&___WaitForEndOfClip_17), value);
	}

	inline static int32_t get_offset_of_finishedEvent_18() { return static_cast<int32_t>(offsetof(AudioPlay_t3994223902, ___finishedEvent_18)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_18() const { return ___finishedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_18() { return &___finishedEvent_18; }
	inline void set_finishedEvent_18(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_18), value);
	}

	inline static int32_t get_offset_of_audio_19() { return static_cast<int32_t>(offsetof(AudioPlay_t3994223902, ___audio_19)); }
	inline AudioSource_t3935305588 * get_audio_19() const { return ___audio_19; }
	inline AudioSource_t3935305588 ** get_address_of_audio_19() { return &___audio_19; }
	inline void set_audio_19(AudioSource_t3935305588 * value)
	{
		___audio_19 = value;
		Il2CppCodeGenWriteBarrier((&___audio_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAY_T3994223902_H
#ifndef AUDIOSTOP_T1488336195_H
#define AUDIOSTOP_T1488336195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AudioStop
struct  AudioStop_t1488336195  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AudioStop::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AudioStop_t1488336195, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSTOP_T1488336195_H
#ifndef BASEFSMVARIABLEACTION_T2713817827_H
#define BASEFSMVARIABLEACTION_T2713817827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseFsmVariableAction
struct  BaseFsmVariableAction_t2713817827  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableAction::fsmNotFound
	FsmEvent_t3736299882 * ___fsmNotFound_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableAction::variableNotFound
	FsmEvent_t3736299882 * ___variableNotFound_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.BaseFsmVariableAction::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_16;
	// System.String HutongGames.PlayMaker.Actions.BaseFsmVariableAction::cachedFsmName
	String_t* ___cachedFsmName_17;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.BaseFsmVariableAction::fsm
	PlayMakerFSM_t1613010231 * ___fsm_18;

public:
	inline static int32_t get_offset_of_fsmNotFound_14() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___fsmNotFound_14)); }
	inline FsmEvent_t3736299882 * get_fsmNotFound_14() const { return ___fsmNotFound_14; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmNotFound_14() { return &___fsmNotFound_14; }
	inline void set_fsmNotFound_14(FsmEvent_t3736299882 * value)
	{
		___fsmNotFound_14 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNotFound_14), value);
	}

	inline static int32_t get_offset_of_variableNotFound_15() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___variableNotFound_15)); }
	inline FsmEvent_t3736299882 * get_variableNotFound_15() const { return ___variableNotFound_15; }
	inline FsmEvent_t3736299882 ** get_address_of_variableNotFound_15() { return &___variableNotFound_15; }
	inline void set_variableNotFound_15(FsmEvent_t3736299882 * value)
	{
		___variableNotFound_15 = value;
		Il2CppCodeGenWriteBarrier((&___variableNotFound_15), value);
	}

	inline static int32_t get_offset_of_cachedGameObject_16() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___cachedGameObject_16)); }
	inline GameObject_t1113636619 * get_cachedGameObject_16() const { return ___cachedGameObject_16; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_16() { return &___cachedGameObject_16; }
	inline void set_cachedGameObject_16(GameObject_t1113636619 * value)
	{
		___cachedGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_16), value);
	}

	inline static int32_t get_offset_of_cachedFsmName_17() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___cachedFsmName_17)); }
	inline String_t* get_cachedFsmName_17() const { return ___cachedFsmName_17; }
	inline String_t** get_address_of_cachedFsmName_17() { return &___cachedFsmName_17; }
	inline void set_cachedFsmName_17(String_t* value)
	{
		___cachedFsmName_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFsmName_17), value);
	}

	inline static int32_t get_offset_of_fsm_18() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___fsm_18)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_18() const { return ___fsm_18; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_18() { return &___fsm_18; }
	inline void set_fsm_18(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_18 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFSMVARIABLEACTION_T2713817827_H
#ifndef BASEFSMVARIABLEINDEXACTION_T3448140877_H
#define BASEFSMVARIABLEINDEXACTION_T3448140877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction
struct  BaseFsmVariableIndexAction_t3448140877  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::indexOutOfRange
	FsmEvent_t3736299882 * ___indexOutOfRange_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::fsmNotFound
	FsmEvent_t3736299882 * ___fsmNotFound_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::variableNotFound
	FsmEvent_t3736299882 * ___variableNotFound_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_17;
	// System.String HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::cachedFsmName
	String_t* ___cachedFsmName_18;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::fsm
	PlayMakerFSM_t1613010231 * ___fsm_19;

public:
	inline static int32_t get_offset_of_indexOutOfRange_14() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___indexOutOfRange_14)); }
	inline FsmEvent_t3736299882 * get_indexOutOfRange_14() const { return ___indexOutOfRange_14; }
	inline FsmEvent_t3736299882 ** get_address_of_indexOutOfRange_14() { return &___indexOutOfRange_14; }
	inline void set_indexOutOfRange_14(FsmEvent_t3736299882 * value)
	{
		___indexOutOfRange_14 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRange_14), value);
	}

	inline static int32_t get_offset_of_fsmNotFound_15() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___fsmNotFound_15)); }
	inline FsmEvent_t3736299882 * get_fsmNotFound_15() const { return ___fsmNotFound_15; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmNotFound_15() { return &___fsmNotFound_15; }
	inline void set_fsmNotFound_15(FsmEvent_t3736299882 * value)
	{
		___fsmNotFound_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNotFound_15), value);
	}

	inline static int32_t get_offset_of_variableNotFound_16() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___variableNotFound_16)); }
	inline FsmEvent_t3736299882 * get_variableNotFound_16() const { return ___variableNotFound_16; }
	inline FsmEvent_t3736299882 ** get_address_of_variableNotFound_16() { return &___variableNotFound_16; }
	inline void set_variableNotFound_16(FsmEvent_t3736299882 * value)
	{
		___variableNotFound_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableNotFound_16), value);
	}

	inline static int32_t get_offset_of_cachedGameObject_17() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___cachedGameObject_17)); }
	inline GameObject_t1113636619 * get_cachedGameObject_17() const { return ___cachedGameObject_17; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_17() { return &___cachedGameObject_17; }
	inline void set_cachedGameObject_17(GameObject_t1113636619 * value)
	{
		___cachedGameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_17), value);
	}

	inline static int32_t get_offset_of_cachedFsmName_18() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___cachedFsmName_18)); }
	inline String_t* get_cachedFsmName_18() const { return ___cachedFsmName_18; }
	inline String_t** get_address_of_cachedFsmName_18() { return &___cachedFsmName_18; }
	inline void set_cachedFsmName_18(String_t* value)
	{
		___cachedFsmName_18 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFsmName_18), value);
	}

	inline static int32_t get_offset_of_fsm_19() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___fsm_19)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_19() const { return ___fsm_19; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_19() { return &___fsm_19; }
	inline void set_fsm_19(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_19 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFSMVARIABLEINDEXACTION_T3448140877_H
#ifndef BASELOGACTION_T823318833_H
#define BASELOGACTION_T823318833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseLogAction
struct  BaseLogAction_t823318833  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.BaseLogAction::sendToUnityLog
	bool ___sendToUnityLog_14;

public:
	inline static int32_t get_offset_of_sendToUnityLog_14() { return static_cast<int32_t>(offsetof(BaseLogAction_t823318833, ___sendToUnityLog_14)); }
	inline bool get_sendToUnityLog_14() const { return ___sendToUnityLog_14; }
	inline bool* get_address_of_sendToUnityLog_14() { return &___sendToUnityLog_14; }
	inline void set_sendToUnityLog_14(bool value)
	{
		___sendToUnityLog_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASELOGACTION_T823318833_H
#ifndef BASEUPDATEACTION_T497653765_H
#define BASEUPDATEACTION_T497653765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseUpdateAction
struct  BaseUpdateAction_t497653765  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.BaseUpdateAction::everyFrame
	bool ___everyFrame_14;
	// HutongGames.PlayMaker.Actions.BaseUpdateAction/UpdateType HutongGames.PlayMaker.Actions.BaseUpdateAction::updateType
	int32_t ___updateType_15;

public:
	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(BaseUpdateAction_t497653765, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_updateType_15() { return static_cast<int32_t>(offsetof(BaseUpdateAction_t497653765, ___updateType_15)); }
	inline int32_t get_updateType_15() const { return ___updateType_15; }
	inline int32_t* get_address_of_updateType_15() { return &___updateType_15; }
	inline void set_updateType_15(int32_t value)
	{
		___updateType_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEUPDATEACTION_T497653765_H
#ifndef CAMERAFADEIN_T119214444_H
#define CAMERAFADEIN_T119214444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CameraFadeIn
struct  CameraFadeIn_t119214444  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CameraFadeIn::color
	FsmColor_t1738900188 * ___color_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CameraFadeIn::time
	FsmFloat_t2883254149 * ___time_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.CameraFadeIn::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.CameraFadeIn::realTime
	bool ___realTime_17;
	// System.Single HutongGames.PlayMaker.Actions.CameraFadeIn::startTime
	float ___startTime_18;
	// System.Single HutongGames.PlayMaker.Actions.CameraFadeIn::currentTime
	float ___currentTime_19;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.CameraFadeIn::colorLerp
	Color_t2555686324  ___colorLerp_20;

public:
	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___color_14)); }
	inline FsmColor_t1738900188 * get_color_14() const { return ___color_14; }
	inline FsmColor_t1738900188 ** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(FsmColor_t1738900188 * value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}

	inline static int32_t get_offset_of_time_15() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___time_15)); }
	inline FsmFloat_t2883254149 * get_time_15() const { return ___time_15; }
	inline FsmFloat_t2883254149 ** get_address_of_time_15() { return &___time_15; }
	inline void set_time_15(FsmFloat_t2883254149 * value)
	{
		___time_15 = value;
		Il2CppCodeGenWriteBarrier((&___time_15), value);
	}

	inline static int32_t get_offset_of_finishEvent_16() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___finishEvent_16)); }
	inline FsmEvent_t3736299882 * get_finishEvent_16() const { return ___finishEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_16() { return &___finishEvent_16; }
	inline void set_finishEvent_16(FsmEvent_t3736299882 * value)
	{
		___finishEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_16), value);
	}

	inline static int32_t get_offset_of_realTime_17() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___realTime_17)); }
	inline bool get_realTime_17() const { return ___realTime_17; }
	inline bool* get_address_of_realTime_17() { return &___realTime_17; }
	inline void set_realTime_17(bool value)
	{
		___realTime_17 = value;
	}

	inline static int32_t get_offset_of_startTime_18() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___startTime_18)); }
	inline float get_startTime_18() const { return ___startTime_18; }
	inline float* get_address_of_startTime_18() { return &___startTime_18; }
	inline void set_startTime_18(float value)
	{
		___startTime_18 = value;
	}

	inline static int32_t get_offset_of_currentTime_19() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___currentTime_19)); }
	inline float get_currentTime_19() const { return ___currentTime_19; }
	inline float* get_address_of_currentTime_19() { return &___currentTime_19; }
	inline void set_currentTime_19(float value)
	{
		___currentTime_19 = value;
	}

	inline static int32_t get_offset_of_colorLerp_20() { return static_cast<int32_t>(offsetof(CameraFadeIn_t119214444, ___colorLerp_20)); }
	inline Color_t2555686324  get_colorLerp_20() const { return ___colorLerp_20; }
	inline Color_t2555686324 * get_address_of_colorLerp_20() { return &___colorLerp_20; }
	inline void set_colorLerp_20(Color_t2555686324  value)
	{
		___colorLerp_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFADEIN_T119214444_H
#ifndef CAMERAFADEOUT_T3832293099_H
#define CAMERAFADEOUT_T3832293099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CameraFadeOut
struct  CameraFadeOut_t3832293099  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CameraFadeOut::color
	FsmColor_t1738900188 * ___color_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CameraFadeOut::time
	FsmFloat_t2883254149 * ___time_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.CameraFadeOut::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.CameraFadeOut::realTime
	bool ___realTime_17;
	// System.Single HutongGames.PlayMaker.Actions.CameraFadeOut::startTime
	float ___startTime_18;
	// System.Single HutongGames.PlayMaker.Actions.CameraFadeOut::currentTime
	float ___currentTime_19;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.CameraFadeOut::colorLerp
	Color_t2555686324  ___colorLerp_20;

public:
	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___color_14)); }
	inline FsmColor_t1738900188 * get_color_14() const { return ___color_14; }
	inline FsmColor_t1738900188 ** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(FsmColor_t1738900188 * value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}

	inline static int32_t get_offset_of_time_15() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___time_15)); }
	inline FsmFloat_t2883254149 * get_time_15() const { return ___time_15; }
	inline FsmFloat_t2883254149 ** get_address_of_time_15() { return &___time_15; }
	inline void set_time_15(FsmFloat_t2883254149 * value)
	{
		___time_15 = value;
		Il2CppCodeGenWriteBarrier((&___time_15), value);
	}

	inline static int32_t get_offset_of_finishEvent_16() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___finishEvent_16)); }
	inline FsmEvent_t3736299882 * get_finishEvent_16() const { return ___finishEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_16() { return &___finishEvent_16; }
	inline void set_finishEvent_16(FsmEvent_t3736299882 * value)
	{
		___finishEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_16), value);
	}

	inline static int32_t get_offset_of_realTime_17() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___realTime_17)); }
	inline bool get_realTime_17() const { return ___realTime_17; }
	inline bool* get_address_of_realTime_17() { return &___realTime_17; }
	inline void set_realTime_17(bool value)
	{
		___realTime_17 = value;
	}

	inline static int32_t get_offset_of_startTime_18() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___startTime_18)); }
	inline float get_startTime_18() const { return ___startTime_18; }
	inline float* get_address_of_startTime_18() { return &___startTime_18; }
	inline void set_startTime_18(float value)
	{
		___startTime_18 = value;
	}

	inline static int32_t get_offset_of_currentTime_19() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___currentTime_19)); }
	inline float get_currentTime_19() const { return ___currentTime_19; }
	inline float* get_address_of_currentTime_19() { return &___currentTime_19; }
	inline void set_currentTime_19(float value)
	{
		___currentTime_19 = value;
	}

	inline static int32_t get_offset_of_colorLerp_20() { return static_cast<int32_t>(offsetof(CameraFadeOut_t3832293099, ___colorLerp_20)); }
	inline Color_t2555686324  get_colorLerp_20() const { return ___colorLerp_20; }
	inline Color_t2555686324 * get_address_of_colorLerp_20() { return &___colorLerp_20; }
	inline void set_colorLerp_20(Color_t2555686324  value)
	{
		___colorLerp_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFADEOUT_T3832293099_H
#ifndef COLORINTERPOLATE_T701671268_H
#define COLORINTERPOLATE_T701671268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ColorInterpolate
struct  ColorInterpolate_t701671268  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.Actions.ColorInterpolate::colors
	FsmColorU5BU5D_t1111370293* ___colors_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ColorInterpolate::time
	FsmFloat_t2883254149 * ___time_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ColorInterpolate::storeColor
	FsmColor_t1738900188 * ___storeColor_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ColorInterpolate::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ColorInterpolate::realTime
	bool ___realTime_18;
	// System.Single HutongGames.PlayMaker.Actions.ColorInterpolate::startTime
	float ___startTime_19;
	// System.Single HutongGames.PlayMaker.Actions.ColorInterpolate::currentTime
	float ___currentTime_20;

public:
	inline static int32_t get_offset_of_colors_14() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___colors_14)); }
	inline FsmColorU5BU5D_t1111370293* get_colors_14() const { return ___colors_14; }
	inline FsmColorU5BU5D_t1111370293** get_address_of_colors_14() { return &___colors_14; }
	inline void set_colors_14(FsmColorU5BU5D_t1111370293* value)
	{
		___colors_14 = value;
		Il2CppCodeGenWriteBarrier((&___colors_14), value);
	}

	inline static int32_t get_offset_of_time_15() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___time_15)); }
	inline FsmFloat_t2883254149 * get_time_15() const { return ___time_15; }
	inline FsmFloat_t2883254149 ** get_address_of_time_15() { return &___time_15; }
	inline void set_time_15(FsmFloat_t2883254149 * value)
	{
		___time_15 = value;
		Il2CppCodeGenWriteBarrier((&___time_15), value);
	}

	inline static int32_t get_offset_of_storeColor_16() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___storeColor_16)); }
	inline FsmColor_t1738900188 * get_storeColor_16() const { return ___storeColor_16; }
	inline FsmColor_t1738900188 ** get_address_of_storeColor_16() { return &___storeColor_16; }
	inline void set_storeColor_16(FsmColor_t1738900188 * value)
	{
		___storeColor_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeColor_16), value);
	}

	inline static int32_t get_offset_of_finishEvent_17() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___finishEvent_17)); }
	inline FsmEvent_t3736299882 * get_finishEvent_17() const { return ___finishEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_17() { return &___finishEvent_17; }
	inline void set_finishEvent_17(FsmEvent_t3736299882 * value)
	{
		___finishEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_17), value);
	}

	inline static int32_t get_offset_of_realTime_18() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___realTime_18)); }
	inline bool get_realTime_18() const { return ___realTime_18; }
	inline bool* get_address_of_realTime_18() { return &___realTime_18; }
	inline void set_realTime_18(bool value)
	{
		___realTime_18 = value;
	}

	inline static int32_t get_offset_of_startTime_19() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___startTime_19)); }
	inline float get_startTime_19() const { return ___startTime_19; }
	inline float* get_address_of_startTime_19() { return &___startTime_19; }
	inline void set_startTime_19(float value)
	{
		___startTime_19 = value;
	}

	inline static int32_t get_offset_of_currentTime_20() { return static_cast<int32_t>(offsetof(ColorInterpolate_t701671268, ___currentTime_20)); }
	inline float get_currentTime_20() const { return ___currentTime_20; }
	inline float* get_address_of_currentTime_20() { return &___currentTime_20; }
	inline void set_currentTime_20(float value)
	{
		___currentTime_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORINTERPOLATE_T701671268_H
#ifndef COLORRAMP_T3017487916_H
#define COLORRAMP_T3017487916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ColorRamp
struct  ColorRamp_t3017487916  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.Actions.ColorRamp::colors
	FsmColorU5BU5D_t1111370293* ___colors_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ColorRamp::sampleAt
	FsmFloat_t2883254149 * ___sampleAt_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ColorRamp::storeColor
	FsmColor_t1738900188 * ___storeColor_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ColorRamp::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_colors_14() { return static_cast<int32_t>(offsetof(ColorRamp_t3017487916, ___colors_14)); }
	inline FsmColorU5BU5D_t1111370293* get_colors_14() const { return ___colors_14; }
	inline FsmColorU5BU5D_t1111370293** get_address_of_colors_14() { return &___colors_14; }
	inline void set_colors_14(FsmColorU5BU5D_t1111370293* value)
	{
		___colors_14 = value;
		Il2CppCodeGenWriteBarrier((&___colors_14), value);
	}

	inline static int32_t get_offset_of_sampleAt_15() { return static_cast<int32_t>(offsetof(ColorRamp_t3017487916, ___sampleAt_15)); }
	inline FsmFloat_t2883254149 * get_sampleAt_15() const { return ___sampleAt_15; }
	inline FsmFloat_t2883254149 ** get_address_of_sampleAt_15() { return &___sampleAt_15; }
	inline void set_sampleAt_15(FsmFloat_t2883254149 * value)
	{
		___sampleAt_15 = value;
		Il2CppCodeGenWriteBarrier((&___sampleAt_15), value);
	}

	inline static int32_t get_offset_of_storeColor_16() { return static_cast<int32_t>(offsetof(ColorRamp_t3017487916, ___storeColor_16)); }
	inline FsmColor_t1738900188 * get_storeColor_16() const { return ___storeColor_16; }
	inline FsmColor_t1738900188 ** get_address_of_storeColor_16() { return &___storeColor_16; }
	inline void set_storeColor_16(FsmColor_t1738900188 * value)
	{
		___storeColor_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeColor_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ColorRamp_t3017487916, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORRAMP_T3017487916_H
#ifndef COMMENT_T3901720946_H
#define COMMENT_T3901720946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Comment
struct  Comment_t3901720946  : public FsmStateAction_t3801304101
{
public:
	// System.String HutongGames.PlayMaker.Actions.Comment::comment
	String_t* ___comment_14;

public:
	inline static int32_t get_offset_of_comment_14() { return static_cast<int32_t>(offsetof(Comment_t3901720946, ___comment_14)); }
	inline String_t* get_comment_14() const { return ___comment_14; }
	inline String_t** get_address_of_comment_14() { return &___comment_14; }
	inline void set_comment_14(String_t* value)
	{
		___comment_14 = value;
		Il2CppCodeGenWriteBarrier((&___comment_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENT_T3901720946_H
#ifndef COMPONENTACTION_1_T2517625514_H
#define COMPONENTACTION_1_T2517625514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.AudioSource>
struct  ComponentAction_1_t2517625514  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	AudioSource_t3935305588 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2517625514, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2517625514, ___cachedComponent_15)); }
	inline AudioSource_t3935305588 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline AudioSource_t3935305588 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(AudioSource_t3935305588 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2517625514_H
#ifndef COMPONENTACTION_1_T2739473797_H
#define COMPONENTACTION_1_T2739473797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Camera>
struct  ComponentAction_1_t2739473797  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Camera_t4157153871 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2739473797, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2739473797, ___cachedComponent_15)); }
	inline Camera_t4157153871 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Camera_t4157153871 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Camera_t4157153871 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2739473797_H
#ifndef CONTROLLERISGROUNDED_T987874773_H
#define CONTROLLERISGROUNDED_T987874773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ControllerIsGrounded
struct  ControllerIsGrounded_t987874773  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ControllerIsGrounded::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ControllerIsGrounded::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ControllerIsGrounded::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ControllerIsGrounded::storeResult
	FsmBool_t163807967 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ControllerIsGrounded::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ControllerIsGrounded::previousGo
	GameObject_t1113636619 * ___previousGo_19;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.ControllerIsGrounded::controller
	CharacterController_t1138636865 * ___controller_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_trueEvent_15() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___trueEvent_15)); }
	inline FsmEvent_t3736299882 * get_trueEvent_15() const { return ___trueEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_15() { return &___trueEvent_15; }
	inline void set_trueEvent_15(FsmEvent_t3736299882 * value)
	{
		___trueEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_15), value);
	}

	inline static int32_t get_offset_of_falseEvent_16() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___falseEvent_16)); }
	inline FsmEvent_t3736299882 * get_falseEvent_16() const { return ___falseEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_16() { return &___falseEvent_16; }
	inline void set_falseEvent_16(FsmEvent_t3736299882 * value)
	{
		___falseEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___storeResult_17)); }
	inline FsmBool_t163807967 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmBool_t163807967 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_previousGo_19() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___previousGo_19)); }
	inline GameObject_t1113636619 * get_previousGo_19() const { return ___previousGo_19; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_19() { return &___previousGo_19; }
	inline void set_previousGo_19(GameObject_t1113636619 * value)
	{
		___previousGo_19 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_19), value);
	}

	inline static int32_t get_offset_of_controller_20() { return static_cast<int32_t>(offsetof(ControllerIsGrounded_t987874773, ___controller_20)); }
	inline CharacterController_t1138636865 * get_controller_20() const { return ___controller_20; }
	inline CharacterController_t1138636865 ** get_address_of_controller_20() { return &___controller_20; }
	inline void set_controller_20(CharacterController_t1138636865 * value)
	{
		___controller_20 = value;
		Il2CppCodeGenWriteBarrier((&___controller_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERISGROUNDED_T987874773_H
#ifndef CONTROLLERMOVE_T2604891426_H
#define CONTROLLERMOVE_T2604891426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ControllerMove
struct  ControllerMove_t2604891426  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ControllerMove::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ControllerMove::moveVector
	FsmVector3_t626444517 * ___moveVector_15;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.ControllerMove::space
	int32_t ___space_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ControllerMove::perSecond
	FsmBool_t163807967 * ___perSecond_17;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ControllerMove::previousGo
	GameObject_t1113636619 * ___previousGo_18;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.ControllerMove::controller
	CharacterController_t1138636865 * ___controller_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(ControllerMove_t2604891426, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_moveVector_15() { return static_cast<int32_t>(offsetof(ControllerMove_t2604891426, ___moveVector_15)); }
	inline FsmVector3_t626444517 * get_moveVector_15() const { return ___moveVector_15; }
	inline FsmVector3_t626444517 ** get_address_of_moveVector_15() { return &___moveVector_15; }
	inline void set_moveVector_15(FsmVector3_t626444517 * value)
	{
		___moveVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___moveVector_15), value);
	}

	inline static int32_t get_offset_of_space_16() { return static_cast<int32_t>(offsetof(ControllerMove_t2604891426, ___space_16)); }
	inline int32_t get_space_16() const { return ___space_16; }
	inline int32_t* get_address_of_space_16() { return &___space_16; }
	inline void set_space_16(int32_t value)
	{
		___space_16 = value;
	}

	inline static int32_t get_offset_of_perSecond_17() { return static_cast<int32_t>(offsetof(ControllerMove_t2604891426, ___perSecond_17)); }
	inline FsmBool_t163807967 * get_perSecond_17() const { return ___perSecond_17; }
	inline FsmBool_t163807967 ** get_address_of_perSecond_17() { return &___perSecond_17; }
	inline void set_perSecond_17(FsmBool_t163807967 * value)
	{
		___perSecond_17 = value;
		Il2CppCodeGenWriteBarrier((&___perSecond_17), value);
	}

	inline static int32_t get_offset_of_previousGo_18() { return static_cast<int32_t>(offsetof(ControllerMove_t2604891426, ___previousGo_18)); }
	inline GameObject_t1113636619 * get_previousGo_18() const { return ___previousGo_18; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_18() { return &___previousGo_18; }
	inline void set_previousGo_18(GameObject_t1113636619 * value)
	{
		___previousGo_18 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_18), value);
	}

	inline static int32_t get_offset_of_controller_19() { return static_cast<int32_t>(offsetof(ControllerMove_t2604891426, ___controller_19)); }
	inline CharacterController_t1138636865 * get_controller_19() const { return ___controller_19; }
	inline CharacterController_t1138636865 ** get_address_of_controller_19() { return &___controller_19; }
	inline void set_controller_19(CharacterController_t1138636865 * value)
	{
		___controller_19 = value;
		Il2CppCodeGenWriteBarrier((&___controller_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERMOVE_T2604891426_H
#ifndef CONTROLLERSETTINGS_T305849206_H
#define CONTROLLERSETTINGS_T305849206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ControllerSettings
struct  ControllerSettings_t305849206  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ControllerSettings::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ControllerSettings::height
	FsmFloat_t2883254149 * ___height_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ControllerSettings::radius
	FsmFloat_t2883254149 * ___radius_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ControllerSettings::slopeLimit
	FsmFloat_t2883254149 * ___slopeLimit_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ControllerSettings::stepOffset
	FsmFloat_t2883254149 * ___stepOffset_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ControllerSettings::center
	FsmVector3_t626444517 * ___center_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ControllerSettings::detectCollisions
	FsmBool_t163807967 * ___detectCollisions_20;
	// System.Boolean HutongGames.PlayMaker.Actions.ControllerSettings::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ControllerSettings::previousGo
	GameObject_t1113636619 * ___previousGo_22;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.ControllerSettings::controller
	CharacterController_t1138636865 * ___controller_23;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___height_15)); }
	inline FsmFloat_t2883254149 * get_height_15() const { return ___height_15; }
	inline FsmFloat_t2883254149 ** get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(FsmFloat_t2883254149 * value)
	{
		___height_15 = value;
		Il2CppCodeGenWriteBarrier((&___height_15), value);
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___radius_16)); }
	inline FsmFloat_t2883254149 * get_radius_16() const { return ___radius_16; }
	inline FsmFloat_t2883254149 ** get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(FsmFloat_t2883254149 * value)
	{
		___radius_16 = value;
		Il2CppCodeGenWriteBarrier((&___radius_16), value);
	}

	inline static int32_t get_offset_of_slopeLimit_17() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___slopeLimit_17)); }
	inline FsmFloat_t2883254149 * get_slopeLimit_17() const { return ___slopeLimit_17; }
	inline FsmFloat_t2883254149 ** get_address_of_slopeLimit_17() { return &___slopeLimit_17; }
	inline void set_slopeLimit_17(FsmFloat_t2883254149 * value)
	{
		___slopeLimit_17 = value;
		Il2CppCodeGenWriteBarrier((&___slopeLimit_17), value);
	}

	inline static int32_t get_offset_of_stepOffset_18() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___stepOffset_18)); }
	inline FsmFloat_t2883254149 * get_stepOffset_18() const { return ___stepOffset_18; }
	inline FsmFloat_t2883254149 ** get_address_of_stepOffset_18() { return &___stepOffset_18; }
	inline void set_stepOffset_18(FsmFloat_t2883254149 * value)
	{
		___stepOffset_18 = value;
		Il2CppCodeGenWriteBarrier((&___stepOffset_18), value);
	}

	inline static int32_t get_offset_of_center_19() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___center_19)); }
	inline FsmVector3_t626444517 * get_center_19() const { return ___center_19; }
	inline FsmVector3_t626444517 ** get_address_of_center_19() { return &___center_19; }
	inline void set_center_19(FsmVector3_t626444517 * value)
	{
		___center_19 = value;
		Il2CppCodeGenWriteBarrier((&___center_19), value);
	}

	inline static int32_t get_offset_of_detectCollisions_20() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___detectCollisions_20)); }
	inline FsmBool_t163807967 * get_detectCollisions_20() const { return ___detectCollisions_20; }
	inline FsmBool_t163807967 ** get_address_of_detectCollisions_20() { return &___detectCollisions_20; }
	inline void set_detectCollisions_20(FsmBool_t163807967 * value)
	{
		___detectCollisions_20 = value;
		Il2CppCodeGenWriteBarrier((&___detectCollisions_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_previousGo_22() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___previousGo_22)); }
	inline GameObject_t1113636619 * get_previousGo_22() const { return ___previousGo_22; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_22() { return &___previousGo_22; }
	inline void set_previousGo_22(GameObject_t1113636619 * value)
	{
		___previousGo_22 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_22), value);
	}

	inline static int32_t get_offset_of_controller_23() { return static_cast<int32_t>(offsetof(ControllerSettings_t305849206, ___controller_23)); }
	inline CharacterController_t1138636865 * get_controller_23() const { return ___controller_23; }
	inline CharacterController_t1138636865 ** get_address_of_controller_23() { return &___controller_23; }
	inline void set_controller_23(CharacterController_t1138636865 * value)
	{
		___controller_23 = value;
		Il2CppCodeGenWriteBarrier((&___controller_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSETTINGS_T305849206_H
#ifndef CONTROLLERSIMPLEMOVE_T3829253614_H
#define CONTROLLERSIMPLEMOVE_T3829253614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ControllerSimpleMove
struct  ControllerSimpleMove_t3829253614  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ControllerSimpleMove::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ControllerSimpleMove::moveVector
	FsmVector3_t626444517 * ___moveVector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ControllerSimpleMove::speed
	FsmFloat_t2883254149 * ___speed_16;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.ControllerSimpleMove::space
	int32_t ___space_17;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ControllerSimpleMove::previousGo
	GameObject_t1113636619 * ___previousGo_18;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.ControllerSimpleMove::controller
	CharacterController_t1138636865 * ___controller_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t3829253614, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_moveVector_15() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t3829253614, ___moveVector_15)); }
	inline FsmVector3_t626444517 * get_moveVector_15() const { return ___moveVector_15; }
	inline FsmVector3_t626444517 ** get_address_of_moveVector_15() { return &___moveVector_15; }
	inline void set_moveVector_15(FsmVector3_t626444517 * value)
	{
		___moveVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___moveVector_15), value);
	}

	inline static int32_t get_offset_of_speed_16() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t3829253614, ___speed_16)); }
	inline FsmFloat_t2883254149 * get_speed_16() const { return ___speed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_16() { return &___speed_16; }
	inline void set_speed_16(FsmFloat_t2883254149 * value)
	{
		___speed_16 = value;
		Il2CppCodeGenWriteBarrier((&___speed_16), value);
	}

	inline static int32_t get_offset_of_space_17() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t3829253614, ___space_17)); }
	inline int32_t get_space_17() const { return ___space_17; }
	inline int32_t* get_address_of_space_17() { return &___space_17; }
	inline void set_space_17(int32_t value)
	{
		___space_17 = value;
	}

	inline static int32_t get_offset_of_previousGo_18() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t3829253614, ___previousGo_18)); }
	inline GameObject_t1113636619 * get_previousGo_18() const { return ___previousGo_18; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_18() { return &___previousGo_18; }
	inline void set_previousGo_18(GameObject_t1113636619 * value)
	{
		___previousGo_18 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_18), value);
	}

	inline static int32_t get_offset_of_controller_19() { return static_cast<int32_t>(offsetof(ControllerSimpleMove_t3829253614, ___controller_19)); }
	inline CharacterController_t1138636865 * get_controller_19() const { return ___controller_19; }
	inline CharacterController_t1138636865 ** get_address_of_controller_19() { return &___controller_19; }
	inline void set_controller_19(CharacterController_t1138636865 * value)
	{
		___controller_19 = value;
		Il2CppCodeGenWriteBarrier((&___controller_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSIMPLEMOVE_T3829253614_H
#ifndef CONVERTBOOLTOCOLOR_T4204121452_H
#define CONVERTBOOLTOCOLOR_T4204121452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertBoolToColor
struct  ConvertBoolToColor_t4204121452  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ConvertBoolToColor::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ConvertBoolToColor::colorVariable
	FsmColor_t1738900188 * ___colorVariable_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ConvertBoolToColor::falseColor
	FsmColor_t1738900188 * ___falseColor_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ConvertBoolToColor::trueColor
	FsmColor_t1738900188 * ___trueColor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertBoolToColor::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(ConvertBoolToColor_t4204121452, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_colorVariable_15() { return static_cast<int32_t>(offsetof(ConvertBoolToColor_t4204121452, ___colorVariable_15)); }
	inline FsmColor_t1738900188 * get_colorVariable_15() const { return ___colorVariable_15; }
	inline FsmColor_t1738900188 ** get_address_of_colorVariable_15() { return &___colorVariable_15; }
	inline void set_colorVariable_15(FsmColor_t1738900188 * value)
	{
		___colorVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariable_15), value);
	}

	inline static int32_t get_offset_of_falseColor_16() { return static_cast<int32_t>(offsetof(ConvertBoolToColor_t4204121452, ___falseColor_16)); }
	inline FsmColor_t1738900188 * get_falseColor_16() const { return ___falseColor_16; }
	inline FsmColor_t1738900188 ** get_address_of_falseColor_16() { return &___falseColor_16; }
	inline void set_falseColor_16(FsmColor_t1738900188 * value)
	{
		___falseColor_16 = value;
		Il2CppCodeGenWriteBarrier((&___falseColor_16), value);
	}

	inline static int32_t get_offset_of_trueColor_17() { return static_cast<int32_t>(offsetof(ConvertBoolToColor_t4204121452, ___trueColor_17)); }
	inline FsmColor_t1738900188 * get_trueColor_17() const { return ___trueColor_17; }
	inline FsmColor_t1738900188 ** get_address_of_trueColor_17() { return &___trueColor_17; }
	inline void set_trueColor_17(FsmColor_t1738900188 * value)
	{
		___trueColor_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueColor_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ConvertBoolToColor_t4204121452, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTBOOLTOCOLOR_T4204121452_H
#ifndef CONVERTBOOLTOFLOAT_T2875111075_H
#define CONVERTBOOLTOFLOAT_T2875111075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertBoolToFloat
struct  ConvertBoolToFloat_t2875111075  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ConvertBoolToFloat::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertBoolToFloat::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertBoolToFloat::falseValue
	FsmFloat_t2883254149 * ___falseValue_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertBoolToFloat::trueValue
	FsmFloat_t2883254149 * ___trueValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertBoolToFloat::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t2875111075, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_floatVariable_15() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t2875111075, ___floatVariable_15)); }
	inline FsmFloat_t2883254149 * get_floatVariable_15() const { return ___floatVariable_15; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_15() { return &___floatVariable_15; }
	inline void set_floatVariable_15(FsmFloat_t2883254149 * value)
	{
		___floatVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_15), value);
	}

	inline static int32_t get_offset_of_falseValue_16() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t2875111075, ___falseValue_16)); }
	inline FsmFloat_t2883254149 * get_falseValue_16() const { return ___falseValue_16; }
	inline FsmFloat_t2883254149 ** get_address_of_falseValue_16() { return &___falseValue_16; }
	inline void set_falseValue_16(FsmFloat_t2883254149 * value)
	{
		___falseValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___falseValue_16), value);
	}

	inline static int32_t get_offset_of_trueValue_17() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t2875111075, ___trueValue_17)); }
	inline FsmFloat_t2883254149 * get_trueValue_17() const { return ___trueValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_trueValue_17() { return &___trueValue_17; }
	inline void set_trueValue_17(FsmFloat_t2883254149 * value)
	{
		___trueValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ConvertBoolToFloat_t2875111075, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTBOOLTOFLOAT_T2875111075_H
#ifndef CONVERTBOOLTOINT_T441841880_H
#define CONVERTBOOLTOINT_T441841880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertBoolToInt
struct  ConvertBoolToInt_t441841880  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ConvertBoolToInt::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertBoolToInt::intVariable
	FsmInt_t874273141 * ___intVariable_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertBoolToInt::falseValue
	FsmInt_t874273141 * ___falseValue_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertBoolToInt::trueValue
	FsmInt_t874273141 * ___trueValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertBoolToInt::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(ConvertBoolToInt_t441841880, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_intVariable_15() { return static_cast<int32_t>(offsetof(ConvertBoolToInt_t441841880, ___intVariable_15)); }
	inline FsmInt_t874273141 * get_intVariable_15() const { return ___intVariable_15; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_15() { return &___intVariable_15; }
	inline void set_intVariable_15(FsmInt_t874273141 * value)
	{
		___intVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_15), value);
	}

	inline static int32_t get_offset_of_falseValue_16() { return static_cast<int32_t>(offsetof(ConvertBoolToInt_t441841880, ___falseValue_16)); }
	inline FsmInt_t874273141 * get_falseValue_16() const { return ___falseValue_16; }
	inline FsmInt_t874273141 ** get_address_of_falseValue_16() { return &___falseValue_16; }
	inline void set_falseValue_16(FsmInt_t874273141 * value)
	{
		___falseValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___falseValue_16), value);
	}

	inline static int32_t get_offset_of_trueValue_17() { return static_cast<int32_t>(offsetof(ConvertBoolToInt_t441841880, ___trueValue_17)); }
	inline FsmInt_t874273141 * get_trueValue_17() const { return ___trueValue_17; }
	inline FsmInt_t874273141 ** get_address_of_trueValue_17() { return &___trueValue_17; }
	inline void set_trueValue_17(FsmInt_t874273141 * value)
	{
		___trueValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ConvertBoolToInt_t441841880, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTBOOLTOINT_T441841880_H
#ifndef CONVERTBOOLTOSTRING_T4106080164_H
#define CONVERTBOOLTOSTRING_T4106080164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertBoolToString
struct  ConvertBoolToString_t4106080164  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ConvertBoolToString::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertBoolToString::stringVariable
	FsmString_t1785915204 * ___stringVariable_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertBoolToString::falseString
	FsmString_t1785915204 * ___falseString_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertBoolToString::trueString
	FsmString_t1785915204 * ___trueString_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertBoolToString::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(ConvertBoolToString_t4106080164, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_stringVariable_15() { return static_cast<int32_t>(offsetof(ConvertBoolToString_t4106080164, ___stringVariable_15)); }
	inline FsmString_t1785915204 * get_stringVariable_15() const { return ___stringVariable_15; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_15() { return &___stringVariable_15; }
	inline void set_stringVariable_15(FsmString_t1785915204 * value)
	{
		___stringVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_15), value);
	}

	inline static int32_t get_offset_of_falseString_16() { return static_cast<int32_t>(offsetof(ConvertBoolToString_t4106080164, ___falseString_16)); }
	inline FsmString_t1785915204 * get_falseString_16() const { return ___falseString_16; }
	inline FsmString_t1785915204 ** get_address_of_falseString_16() { return &___falseString_16; }
	inline void set_falseString_16(FsmString_t1785915204 * value)
	{
		___falseString_16 = value;
		Il2CppCodeGenWriteBarrier((&___falseString_16), value);
	}

	inline static int32_t get_offset_of_trueString_17() { return static_cast<int32_t>(offsetof(ConvertBoolToString_t4106080164, ___trueString_17)); }
	inline FsmString_t1785915204 * get_trueString_17() const { return ___trueString_17; }
	inline FsmString_t1785915204 ** get_address_of_trueString_17() { return &___trueString_17; }
	inline void set_trueString_17(FsmString_t1785915204 * value)
	{
		___trueString_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueString_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ConvertBoolToString_t4106080164, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTBOOLTOSTRING_T4106080164_H
#ifndef CONVERTENUMTOSTRING_T3588283646_H
#define CONVERTENUMTOSTRING_T3588283646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertEnumToString
struct  ConvertEnumToString_t3588283646  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.ConvertEnumToString::enumVariable
	FsmEnum_t2861764163 * ___enumVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertEnumToString::stringVariable
	FsmString_t1785915204 * ___stringVariable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertEnumToString::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_enumVariable_14() { return static_cast<int32_t>(offsetof(ConvertEnumToString_t3588283646, ___enumVariable_14)); }
	inline FsmEnum_t2861764163 * get_enumVariable_14() const { return ___enumVariable_14; }
	inline FsmEnum_t2861764163 ** get_address_of_enumVariable_14() { return &___enumVariable_14; }
	inline void set_enumVariable_14(FsmEnum_t2861764163 * value)
	{
		___enumVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariable_14), value);
	}

	inline static int32_t get_offset_of_stringVariable_15() { return static_cast<int32_t>(offsetof(ConvertEnumToString_t3588283646, ___stringVariable_15)); }
	inline FsmString_t1785915204 * get_stringVariable_15() const { return ___stringVariable_15; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_15() { return &___stringVariable_15; }
	inline void set_stringVariable_15(FsmString_t1785915204 * value)
	{
		___stringVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ConvertEnumToString_t3588283646, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTENUMTOSTRING_T3588283646_H
#ifndef CONVERTFLOATTOINT_T1882659568_H
#define CONVERTFLOATTOINT_T1882659568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertFloatToInt
struct  ConvertFloatToInt_t1882659568  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertFloatToInt::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertFloatToInt::intVariable
	FsmInt_t874273141 * ___intVariable_15;
	// HutongGames.PlayMaker.Actions.ConvertFloatToInt/FloatRounding HutongGames.PlayMaker.Actions.ConvertFloatToInt::rounding
	int32_t ___rounding_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertFloatToInt::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1882659568, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_intVariable_15() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1882659568, ___intVariable_15)); }
	inline FsmInt_t874273141 * get_intVariable_15() const { return ___intVariable_15; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_15() { return &___intVariable_15; }
	inline void set_intVariable_15(FsmInt_t874273141 * value)
	{
		___intVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_15), value);
	}

	inline static int32_t get_offset_of_rounding_16() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1882659568, ___rounding_16)); }
	inline int32_t get_rounding_16() const { return ___rounding_16; }
	inline int32_t* get_address_of_rounding_16() { return &___rounding_16; }
	inline void set_rounding_16(int32_t value)
	{
		___rounding_16 = value;
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ConvertFloatToInt_t1882659568, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTFLOATTOINT_T1882659568_H
#ifndef CONVERTFLOATTOSTRING_T2375739146_H
#define CONVERTFLOATTOSTRING_T2375739146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertFloatToString
struct  ConvertFloatToString_t2375739146  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertFloatToString::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertFloatToString::stringVariable
	FsmString_t1785915204 * ___stringVariable_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertFloatToString::format
	FsmString_t1785915204 * ___format_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertFloatToString::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(ConvertFloatToString_t2375739146, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_stringVariable_15() { return static_cast<int32_t>(offsetof(ConvertFloatToString_t2375739146, ___stringVariable_15)); }
	inline FsmString_t1785915204 * get_stringVariable_15() const { return ___stringVariable_15; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_15() { return &___stringVariable_15; }
	inline void set_stringVariable_15(FsmString_t1785915204 * value)
	{
		___stringVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_15), value);
	}

	inline static int32_t get_offset_of_format_16() { return static_cast<int32_t>(offsetof(ConvertFloatToString_t2375739146, ___format_16)); }
	inline FsmString_t1785915204 * get_format_16() const { return ___format_16; }
	inline FsmString_t1785915204 ** get_address_of_format_16() { return &___format_16; }
	inline void set_format_16(FsmString_t1785915204 * value)
	{
		___format_16 = value;
		Il2CppCodeGenWriteBarrier((&___format_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ConvertFloatToString_t2375739146, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTFLOATTOSTRING_T2375739146_H
#ifndef CONVERTINTTOFLOAT_T2045532169_H
#define CONVERTINTTOFLOAT_T2045532169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertIntToFloat
struct  ConvertIntToFloat_t2045532169  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertIntToFloat::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertIntToFloat::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertIntToFloat::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(ConvertIntToFloat_t2045532169, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_floatVariable_15() { return static_cast<int32_t>(offsetof(ConvertIntToFloat_t2045532169, ___floatVariable_15)); }
	inline FsmFloat_t2883254149 * get_floatVariable_15() const { return ___floatVariable_15; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_15() { return &___floatVariable_15; }
	inline void set_floatVariable_15(FsmFloat_t2883254149 * value)
	{
		___floatVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ConvertIntToFloat_t2045532169, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTINTTOFLOAT_T2045532169_H
#ifndef CONVERTINTTOSTRING_T2050799787_H
#define CONVERTINTTOSTRING_T2050799787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertIntToString
struct  ConvertIntToString_t2050799787  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertIntToString::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertIntToString::stringVariable
	FsmString_t1785915204 * ___stringVariable_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertIntToString::format
	FsmString_t1785915204 * ___format_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertIntToString::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(ConvertIntToString_t2050799787, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_stringVariable_15() { return static_cast<int32_t>(offsetof(ConvertIntToString_t2050799787, ___stringVariable_15)); }
	inline FsmString_t1785915204 * get_stringVariable_15() const { return ___stringVariable_15; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_15() { return &___stringVariable_15; }
	inline void set_stringVariable_15(FsmString_t1785915204 * value)
	{
		___stringVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_15), value);
	}

	inline static int32_t get_offset_of_format_16() { return static_cast<int32_t>(offsetof(ConvertIntToString_t2050799787, ___format_16)); }
	inline FsmString_t1785915204 * get_format_16() const { return ___format_16; }
	inline FsmString_t1785915204 ** get_address_of_format_16() { return &___format_16; }
	inline void set_format_16(FsmString_t1785915204 * value)
	{
		___format_16 = value;
		Il2CppCodeGenWriteBarrier((&___format_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ConvertIntToString_t2050799787, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTINTTOSTRING_T2050799787_H
#ifndef CONVERTMATERIALTOOBJECT_T3938955979_H
#define CONVERTMATERIALTOOBJECT_T3938955979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertMaterialToObject
struct  ConvertMaterialToObject_t3938955979  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.ConvertMaterialToObject::materialVariable
	FsmMaterial_t2057874452 * ___materialVariable_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.ConvertMaterialToObject::objectVariable
	FsmObject_t2606870197 * ___objectVariable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertMaterialToObject::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_materialVariable_14() { return static_cast<int32_t>(offsetof(ConvertMaterialToObject_t3938955979, ___materialVariable_14)); }
	inline FsmMaterial_t2057874452 * get_materialVariable_14() const { return ___materialVariable_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_materialVariable_14() { return &___materialVariable_14; }
	inline void set_materialVariable_14(FsmMaterial_t2057874452 * value)
	{
		___materialVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___materialVariable_14), value);
	}

	inline static int32_t get_offset_of_objectVariable_15() { return static_cast<int32_t>(offsetof(ConvertMaterialToObject_t3938955979, ___objectVariable_15)); }
	inline FsmObject_t2606870197 * get_objectVariable_15() const { return ___objectVariable_15; }
	inline FsmObject_t2606870197 ** get_address_of_objectVariable_15() { return &___objectVariable_15; }
	inline void set_objectVariable_15(FsmObject_t2606870197 * value)
	{
		___objectVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___objectVariable_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ConvertMaterialToObject_t3938955979, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTMATERIALTOOBJECT_T3938955979_H
#ifndef CONVERTSECONDSTOSTRING_T1664221685_H
#define CONVERTSECONDSTOSTRING_T1664221685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertSecondsToString
struct  ConvertSecondsToString_t1664221685  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ConvertSecondsToString::secondsVariable
	FsmFloat_t2883254149 * ___secondsVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertSecondsToString::stringVariable
	FsmString_t1785915204 * ___stringVariable_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertSecondsToString::format
	FsmString_t1785915204 * ___format_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertSecondsToString::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_secondsVariable_14() { return static_cast<int32_t>(offsetof(ConvertSecondsToString_t1664221685, ___secondsVariable_14)); }
	inline FsmFloat_t2883254149 * get_secondsVariable_14() const { return ___secondsVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_secondsVariable_14() { return &___secondsVariable_14; }
	inline void set_secondsVariable_14(FsmFloat_t2883254149 * value)
	{
		___secondsVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___secondsVariable_14), value);
	}

	inline static int32_t get_offset_of_stringVariable_15() { return static_cast<int32_t>(offsetof(ConvertSecondsToString_t1664221685, ___stringVariable_15)); }
	inline FsmString_t1785915204 * get_stringVariable_15() const { return ___stringVariable_15; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_15() { return &___stringVariable_15; }
	inline void set_stringVariable_15(FsmString_t1785915204 * value)
	{
		___stringVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_15), value);
	}

	inline static int32_t get_offset_of_format_16() { return static_cast<int32_t>(offsetof(ConvertSecondsToString_t1664221685, ___format_16)); }
	inline FsmString_t1785915204 * get_format_16() const { return ___format_16; }
	inline FsmString_t1785915204 ** get_address_of_format_16() { return &___format_16; }
	inline void set_format_16(FsmString_t1785915204 * value)
	{
		___format_16 = value;
		Il2CppCodeGenWriteBarrier((&___format_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(ConvertSecondsToString_t1664221685, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTSECONDSTOSTRING_T1664221685_H
#ifndef CONVERTSTRINGTOINT_T4270893755_H
#define CONVERTSTRINGTOINT_T4270893755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ConvertStringToInt
struct  ConvertStringToInt_t4270893755  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ConvertStringToInt::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ConvertStringToInt::intVariable
	FsmInt_t874273141 * ___intVariable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ConvertStringToInt::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(ConvertStringToInt_t4270893755, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_intVariable_15() { return static_cast<int32_t>(offsetof(ConvertStringToInt_t4270893755, ___intVariable_15)); }
	inline FsmInt_t874273141 * get_intVariable_15() const { return ___intVariable_15; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_15() { return &___intVariable_15; }
	inline void set_intVariable_15(FsmInt_t874273141 * value)
	{
		___intVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ConvertStringToInt_t4270893755, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTSTRINGTOINT_T4270893755_H
#ifndef CUTTOCAMERA_T3140260074_H
#define CUTTOCAMERA_T3140260074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CutToCamera
struct  CutToCamera_t3140260074  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.CutToCamera::camera
	Camera_t4157153871 * ___camera_14;
	// System.Boolean HutongGames.PlayMaker.Actions.CutToCamera::makeMainCamera
	bool ___makeMainCamera_15;
	// System.Boolean HutongGames.PlayMaker.Actions.CutToCamera::cutBackOnExit
	bool ___cutBackOnExit_16;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.CutToCamera::oldCamera
	Camera_t4157153871 * ___oldCamera_17;

public:
	inline static int32_t get_offset_of_camera_14() { return static_cast<int32_t>(offsetof(CutToCamera_t3140260074, ___camera_14)); }
	inline Camera_t4157153871 * get_camera_14() const { return ___camera_14; }
	inline Camera_t4157153871 ** get_address_of_camera_14() { return &___camera_14; }
	inline void set_camera_14(Camera_t4157153871 * value)
	{
		___camera_14 = value;
		Il2CppCodeGenWriteBarrier((&___camera_14), value);
	}

	inline static int32_t get_offset_of_makeMainCamera_15() { return static_cast<int32_t>(offsetof(CutToCamera_t3140260074, ___makeMainCamera_15)); }
	inline bool get_makeMainCamera_15() const { return ___makeMainCamera_15; }
	inline bool* get_address_of_makeMainCamera_15() { return &___makeMainCamera_15; }
	inline void set_makeMainCamera_15(bool value)
	{
		___makeMainCamera_15 = value;
	}

	inline static int32_t get_offset_of_cutBackOnExit_16() { return static_cast<int32_t>(offsetof(CutToCamera_t3140260074, ___cutBackOnExit_16)); }
	inline bool get_cutBackOnExit_16() const { return ___cutBackOnExit_16; }
	inline bool* get_address_of_cutBackOnExit_16() { return &___cutBackOnExit_16; }
	inline void set_cutBackOnExit_16(bool value)
	{
		___cutBackOnExit_16 = value;
	}

	inline static int32_t get_offset_of_oldCamera_17() { return static_cast<int32_t>(offsetof(CutToCamera_t3140260074, ___oldCamera_17)); }
	inline Camera_t4157153871 * get_oldCamera_17() const { return ___oldCamera_17; }
	inline Camera_t4157153871 ** get_address_of_oldCamera_17() { return &___oldCamera_17; }
	inline void set_oldCamera_17(Camera_t4157153871 * value)
	{
		___oldCamera_17 = value;
		Il2CppCodeGenWriteBarrier((&___oldCamera_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUTTOCAMERA_T3140260074_H
#ifndef DEBUGDRAWSHAPE_T1342584760_H
#define DEBUGDRAWSHAPE_T1342584760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugDrawShape
struct  DebugDrawShape_t1342584760  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DebugDrawShape::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.Actions.DebugDrawShape/ShapeType HutongGames.PlayMaker.Actions.DebugDrawShape::shape
	int32_t ___shape_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DebugDrawShape::color
	FsmColor_t1738900188 * ___color_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DebugDrawShape::radius
	FsmFloat_t2883254149 * ___radius_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DebugDrawShape::size
	FsmVector3_t626444517 * ___size_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(DebugDrawShape_t1342584760, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_shape_15() { return static_cast<int32_t>(offsetof(DebugDrawShape_t1342584760, ___shape_15)); }
	inline int32_t get_shape_15() const { return ___shape_15; }
	inline int32_t* get_address_of_shape_15() { return &___shape_15; }
	inline void set_shape_15(int32_t value)
	{
		___shape_15 = value;
	}

	inline static int32_t get_offset_of_color_16() { return static_cast<int32_t>(offsetof(DebugDrawShape_t1342584760, ___color_16)); }
	inline FsmColor_t1738900188 * get_color_16() const { return ___color_16; }
	inline FsmColor_t1738900188 ** get_address_of_color_16() { return &___color_16; }
	inline void set_color_16(FsmColor_t1738900188 * value)
	{
		___color_16 = value;
		Il2CppCodeGenWriteBarrier((&___color_16), value);
	}

	inline static int32_t get_offset_of_radius_17() { return static_cast<int32_t>(offsetof(DebugDrawShape_t1342584760, ___radius_17)); }
	inline FsmFloat_t2883254149 * get_radius_17() const { return ___radius_17; }
	inline FsmFloat_t2883254149 ** get_address_of_radius_17() { return &___radius_17; }
	inline void set_radius_17(FsmFloat_t2883254149 * value)
	{
		___radius_17 = value;
		Il2CppCodeGenWriteBarrier((&___radius_17), value);
	}

	inline static int32_t get_offset_of_size_18() { return static_cast<int32_t>(offsetof(DebugDrawShape_t1342584760, ___size_18)); }
	inline FsmVector3_t626444517 * get_size_18() const { return ___size_18; }
	inline FsmVector3_t626444517 ** get_address_of_size_18() { return &___size_18; }
	inline void set_size_18(FsmVector3_t626444517 * value)
	{
		___size_18 = value;
		Il2CppCodeGenWriteBarrier((&___size_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGDRAWSHAPE_T1342584760_H
#ifndef DEVICEORIENTATIONEVENT_T3091729835_H
#define DEVICEORIENTATIONEVENT_T3091729835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DeviceOrientationEvent
struct  DeviceOrientationEvent_t3091729835  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.DeviceOrientation HutongGames.PlayMaker.Actions.DeviceOrientationEvent::orientation
	int32_t ___orientation_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DeviceOrientationEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// System.Boolean HutongGames.PlayMaker.Actions.DeviceOrientationEvent::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_orientation_14() { return static_cast<int32_t>(offsetof(DeviceOrientationEvent_t3091729835, ___orientation_14)); }
	inline int32_t get_orientation_14() const { return ___orientation_14; }
	inline int32_t* get_address_of_orientation_14() { return &___orientation_14; }
	inline void set_orientation_14(int32_t value)
	{
		___orientation_14 = value;
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(DeviceOrientationEvent_t3091729835, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(DeviceOrientationEvent_t3091729835, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEORIENTATIONEVENT_T3091729835_H
#ifndef DRAWDEBUGLINE_T21833544_H
#define DRAWDEBUGLINE_T21833544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawDebugLine
struct  DrawDebugLine_t21833544  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DrawDebugLine::fromObject
	FsmGameObject_t3581898942 * ___fromObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DrawDebugLine::fromPosition
	FsmVector3_t626444517 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DrawDebugLine::toObject
	FsmGameObject_t3581898942 * ___toObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DrawDebugLine::toPosition
	FsmVector3_t626444517 * ___toPosition_17;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DrawDebugLine::color
	FsmColor_t1738900188 * ___color_18;

public:
	inline static int32_t get_offset_of_fromObject_14() { return static_cast<int32_t>(offsetof(DrawDebugLine_t21833544, ___fromObject_14)); }
	inline FsmGameObject_t3581898942 * get_fromObject_14() const { return ___fromObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_fromObject_14() { return &___fromObject_14; }
	inline void set_fromObject_14(FsmGameObject_t3581898942 * value)
	{
		___fromObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(DrawDebugLine_t21833544, ___fromPosition_15)); }
	inline FsmVector3_t626444517 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector3_t626444517 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector3_t626444517 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_toObject_16() { return static_cast<int32_t>(offsetof(DrawDebugLine_t21833544, ___toObject_16)); }
	inline FsmGameObject_t3581898942 * get_toObject_16() const { return ___toObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_toObject_16() { return &___toObject_16; }
	inline void set_toObject_16(FsmGameObject_t3581898942 * value)
	{
		___toObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___toObject_16), value);
	}

	inline static int32_t get_offset_of_toPosition_17() { return static_cast<int32_t>(offsetof(DrawDebugLine_t21833544, ___toPosition_17)); }
	inline FsmVector3_t626444517 * get_toPosition_17() const { return ___toPosition_17; }
	inline FsmVector3_t626444517 ** get_address_of_toPosition_17() { return &___toPosition_17; }
	inline void set_toPosition_17(FsmVector3_t626444517 * value)
	{
		___toPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___toPosition_17), value);
	}

	inline static int32_t get_offset_of_color_18() { return static_cast<int32_t>(offsetof(DrawDebugLine_t21833544, ___color_18)); }
	inline FsmColor_t1738900188 * get_color_18() const { return ___color_18; }
	inline FsmColor_t1738900188 ** get_address_of_color_18() { return &___color_18; }
	inline void set_color_18(FsmColor_t1738900188 * value)
	{
		___color_18 = value;
		Il2CppCodeGenWriteBarrier((&___color_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWDEBUGLINE_T21833544_H
#ifndef DRAWDEBUGRAY_T1994944557_H
#define DRAWDEBUGRAY_T1994944557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawDebugRay
struct  DrawDebugRay_t1994944557  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DrawDebugRay::fromObject
	FsmGameObject_t3581898942 * ___fromObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DrawDebugRay::fromPosition
	FsmVector3_t626444517 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DrawDebugRay::direction
	FsmVector3_t626444517 * ___direction_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DrawDebugRay::color
	FsmColor_t1738900188 * ___color_17;

public:
	inline static int32_t get_offset_of_fromObject_14() { return static_cast<int32_t>(offsetof(DrawDebugRay_t1994944557, ___fromObject_14)); }
	inline FsmGameObject_t3581898942 * get_fromObject_14() const { return ___fromObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_fromObject_14() { return &___fromObject_14; }
	inline void set_fromObject_14(FsmGameObject_t3581898942 * value)
	{
		___fromObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(DrawDebugRay_t1994944557, ___fromPosition_15)); }
	inline FsmVector3_t626444517 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector3_t626444517 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector3_t626444517 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(DrawDebugRay_t1994944557, ___direction_16)); }
	inline FsmVector3_t626444517 * get_direction_16() const { return ___direction_16; }
	inline FsmVector3_t626444517 ** get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(FsmVector3_t626444517 * value)
	{
		___direction_16 = value;
		Il2CppCodeGenWriteBarrier((&___direction_16), value);
	}

	inline static int32_t get_offset_of_color_17() { return static_cast<int32_t>(offsetof(DrawDebugRay_t1994944557, ___color_17)); }
	inline FsmColor_t1738900188 * get_color_17() const { return ___color_17; }
	inline FsmColor_t1738900188 ** get_address_of_color_17() { return &___color_17; }
	inline void set_color_17(FsmColor_t1738900188 * value)
	{
		___color_17 = value;
		Il2CppCodeGenWriteBarrier((&___color_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWDEBUGRAY_T1994944557_H
#ifndef DRAWSTATELABEL_T3230669138_H
#define DRAWSTATELABEL_T3230669138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawStateLabel
struct  DrawStateLabel_t3230669138  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DrawStateLabel::showLabel
	FsmBool_t163807967 * ___showLabel_14;

public:
	inline static int32_t get_offset_of_showLabel_14() { return static_cast<int32_t>(offsetof(DrawStateLabel_t3230669138, ___showLabel_14)); }
	inline FsmBool_t163807967 * get_showLabel_14() const { return ___showLabel_14; }
	inline FsmBool_t163807967 ** get_address_of_showLabel_14() { return &___showLabel_14; }
	inline void set_showLabel_14(FsmBool_t163807967 * value)
	{
		___showLabel_14 = value;
		Il2CppCodeGenWriteBarrier((&___showLabel_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWSTATELABEL_T3230669138_H
#ifndef FSMARRAYSET_T3520048074_H
#define FSMARRAYSET_T3520048074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmArraySet
struct  FsmArraySet_t3520048074  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.FsmArraySet::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmArraySet::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmArraySet::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmArraySet::setValue
	FsmString_t1785915204 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.FsmArraySet::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.FsmArraySet::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.FsmArraySet::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___setValue_17)); }
	inline FsmString_t1785915204 * get_setValue_17() const { return ___setValue_17; }
	inline FsmString_t1785915204 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmString_t1785915204 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(FsmArraySet_t3520048074, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMARRAYSET_T3520048074_H
#ifndef GETCOLORRGBA_T3455744901_H
#define GETCOLORRGBA_T3455744901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetColorRGBA
struct  GetColorRGBA_t3455744901  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.GetColorRGBA::color
	FsmColor_t1738900188 * ___color_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetColorRGBA::storeRed
	FsmFloat_t2883254149 * ___storeRed_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetColorRGBA::storeGreen
	FsmFloat_t2883254149 * ___storeGreen_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetColorRGBA::storeBlue
	FsmFloat_t2883254149 * ___storeBlue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetColorRGBA::storeAlpha
	FsmFloat_t2883254149 * ___storeAlpha_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetColorRGBA::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(GetColorRGBA_t3455744901, ___color_14)); }
	inline FsmColor_t1738900188 * get_color_14() const { return ___color_14; }
	inline FsmColor_t1738900188 ** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(FsmColor_t1738900188 * value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}

	inline static int32_t get_offset_of_storeRed_15() { return static_cast<int32_t>(offsetof(GetColorRGBA_t3455744901, ___storeRed_15)); }
	inline FsmFloat_t2883254149 * get_storeRed_15() const { return ___storeRed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeRed_15() { return &___storeRed_15; }
	inline void set_storeRed_15(FsmFloat_t2883254149 * value)
	{
		___storeRed_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeRed_15), value);
	}

	inline static int32_t get_offset_of_storeGreen_16() { return static_cast<int32_t>(offsetof(GetColorRGBA_t3455744901, ___storeGreen_16)); }
	inline FsmFloat_t2883254149 * get_storeGreen_16() const { return ___storeGreen_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeGreen_16() { return &___storeGreen_16; }
	inline void set_storeGreen_16(FsmFloat_t2883254149 * value)
	{
		___storeGreen_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeGreen_16), value);
	}

	inline static int32_t get_offset_of_storeBlue_17() { return static_cast<int32_t>(offsetof(GetColorRGBA_t3455744901, ___storeBlue_17)); }
	inline FsmFloat_t2883254149 * get_storeBlue_17() const { return ___storeBlue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeBlue_17() { return &___storeBlue_17; }
	inline void set_storeBlue_17(FsmFloat_t2883254149 * value)
	{
		___storeBlue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeBlue_17), value);
	}

	inline static int32_t get_offset_of_storeAlpha_18() { return static_cast<int32_t>(offsetof(GetColorRGBA_t3455744901, ___storeAlpha_18)); }
	inline FsmFloat_t2883254149 * get_storeAlpha_18() const { return ___storeAlpha_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeAlpha_18() { return &___storeAlpha_18; }
	inline void set_storeAlpha_18(FsmFloat_t2883254149 * value)
	{
		___storeAlpha_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeAlpha_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GetColorRGBA_t3455744901, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLORRGBA_T3455744901_H
#ifndef GETCONTROLLERCOLLISIONFLAGS_T2967667631_H
#define GETCONTROLLERCOLLISIONFLAGS_T2967667631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetControllerCollisionFlags
struct  GetControllerCollisionFlags_t2967667631  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::isGrounded
	FsmBool_t163807967 * ___isGrounded_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::none
	FsmBool_t163807967 * ___none_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::sides
	FsmBool_t163807967 * ___sides_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::above
	FsmBool_t163807967 * ___above_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::below
	FsmBool_t163807967 * ___below_19;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::previousGo
	GameObject_t1113636619 * ___previousGo_20;
	// UnityEngine.CharacterController HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::controller
	CharacterController_t1138636865 * ___controller_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_isGrounded_15() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___isGrounded_15)); }
	inline FsmBool_t163807967 * get_isGrounded_15() const { return ___isGrounded_15; }
	inline FsmBool_t163807967 ** get_address_of_isGrounded_15() { return &___isGrounded_15; }
	inline void set_isGrounded_15(FsmBool_t163807967 * value)
	{
		___isGrounded_15 = value;
		Il2CppCodeGenWriteBarrier((&___isGrounded_15), value);
	}

	inline static int32_t get_offset_of_none_16() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___none_16)); }
	inline FsmBool_t163807967 * get_none_16() const { return ___none_16; }
	inline FsmBool_t163807967 ** get_address_of_none_16() { return &___none_16; }
	inline void set_none_16(FsmBool_t163807967 * value)
	{
		___none_16 = value;
		Il2CppCodeGenWriteBarrier((&___none_16), value);
	}

	inline static int32_t get_offset_of_sides_17() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___sides_17)); }
	inline FsmBool_t163807967 * get_sides_17() const { return ___sides_17; }
	inline FsmBool_t163807967 ** get_address_of_sides_17() { return &___sides_17; }
	inline void set_sides_17(FsmBool_t163807967 * value)
	{
		___sides_17 = value;
		Il2CppCodeGenWriteBarrier((&___sides_17), value);
	}

	inline static int32_t get_offset_of_above_18() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___above_18)); }
	inline FsmBool_t163807967 * get_above_18() const { return ___above_18; }
	inline FsmBool_t163807967 ** get_address_of_above_18() { return &___above_18; }
	inline void set_above_18(FsmBool_t163807967 * value)
	{
		___above_18 = value;
		Il2CppCodeGenWriteBarrier((&___above_18), value);
	}

	inline static int32_t get_offset_of_below_19() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___below_19)); }
	inline FsmBool_t163807967 * get_below_19() const { return ___below_19; }
	inline FsmBool_t163807967 ** get_address_of_below_19() { return &___below_19; }
	inline void set_below_19(FsmBool_t163807967 * value)
	{
		___below_19 = value;
		Il2CppCodeGenWriteBarrier((&___below_19), value);
	}

	inline static int32_t get_offset_of_previousGo_20() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___previousGo_20)); }
	inline GameObject_t1113636619 * get_previousGo_20() const { return ___previousGo_20; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_20() { return &___previousGo_20; }
	inline void set_previousGo_20(GameObject_t1113636619 * value)
	{
		___previousGo_20 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_20), value);
	}

	inline static int32_t get_offset_of_controller_21() { return static_cast<int32_t>(offsetof(GetControllerCollisionFlags_t2967667631, ___controller_21)); }
	inline CharacterController_t1138636865 * get_controller_21() const { return ___controller_21; }
	inline CharacterController_t1138636865 ** get_address_of_controller_21() { return &___controller_21; }
	inline void set_controller_21(CharacterController_t1138636865 * value)
	{
		___controller_21 = value;
		Il2CppCodeGenWriteBarrier((&___controller_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCONTROLLERCOLLISIONFLAGS_T2967667631_H
#ifndef GETCONTROLLERHITINFO_T3436369072_H
#define GETCONTROLLERHITINFO_T3436369072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetControllerHitInfo
struct  GetControllerHitInfo_t3436369072  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetControllerHitInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetControllerHitInfo::contactPoint
	FsmVector3_t626444517 * ___contactPoint_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetControllerHitInfo::contactNormal
	FsmVector3_t626444517 * ___contactNormal_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetControllerHitInfo::moveDirection
	FsmVector3_t626444517 * ___moveDirection_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetControllerHitInfo::moveLength
	FsmFloat_t2883254149 * ___moveLength_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetControllerHitInfo::physicsMaterialName
	FsmString_t1785915204 * ___physicsMaterialName_19;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3436369072, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_contactPoint_15() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3436369072, ___contactPoint_15)); }
	inline FsmVector3_t626444517 * get_contactPoint_15() const { return ___contactPoint_15; }
	inline FsmVector3_t626444517 ** get_address_of_contactPoint_15() { return &___contactPoint_15; }
	inline void set_contactPoint_15(FsmVector3_t626444517 * value)
	{
		___contactPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___contactPoint_15), value);
	}

	inline static int32_t get_offset_of_contactNormal_16() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3436369072, ___contactNormal_16)); }
	inline FsmVector3_t626444517 * get_contactNormal_16() const { return ___contactNormal_16; }
	inline FsmVector3_t626444517 ** get_address_of_contactNormal_16() { return &___contactNormal_16; }
	inline void set_contactNormal_16(FsmVector3_t626444517 * value)
	{
		___contactNormal_16 = value;
		Il2CppCodeGenWriteBarrier((&___contactNormal_16), value);
	}

	inline static int32_t get_offset_of_moveDirection_17() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3436369072, ___moveDirection_17)); }
	inline FsmVector3_t626444517 * get_moveDirection_17() const { return ___moveDirection_17; }
	inline FsmVector3_t626444517 ** get_address_of_moveDirection_17() { return &___moveDirection_17; }
	inline void set_moveDirection_17(FsmVector3_t626444517 * value)
	{
		___moveDirection_17 = value;
		Il2CppCodeGenWriteBarrier((&___moveDirection_17), value);
	}

	inline static int32_t get_offset_of_moveLength_18() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3436369072, ___moveLength_18)); }
	inline FsmFloat_t2883254149 * get_moveLength_18() const { return ___moveLength_18; }
	inline FsmFloat_t2883254149 ** get_address_of_moveLength_18() { return &___moveLength_18; }
	inline void set_moveLength_18(FsmFloat_t2883254149 * value)
	{
		___moveLength_18 = value;
		Il2CppCodeGenWriteBarrier((&___moveLength_18), value);
	}

	inline static int32_t get_offset_of_physicsMaterialName_19() { return static_cast<int32_t>(offsetof(GetControllerHitInfo_t3436369072, ___physicsMaterialName_19)); }
	inline FsmString_t1785915204 * get_physicsMaterialName_19() const { return ___physicsMaterialName_19; }
	inline FsmString_t1785915204 ** get_address_of_physicsMaterialName_19() { return &___physicsMaterialName_19; }
	inline void set_physicsMaterialName_19(FsmString_t1785915204 * value)
	{
		___physicsMaterialName_19 = value;
		Il2CppCodeGenWriteBarrier((&___physicsMaterialName_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCONTROLLERHITINFO_T3436369072_H
#ifndef GETMAINCAMERA_T1071044421_H
#define GETMAINCAMERA_T1071044421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMainCamera
struct  GetMainCamera_t1071044421  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetMainCamera::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_14;

public:
	inline static int32_t get_offset_of_storeGameObject_14() { return static_cast<int32_t>(offsetof(GetMainCamera_t1071044421, ___storeGameObject_14)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_14() const { return ___storeGameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_14() { return &___storeGameObject_14; }
	inline void set_storeGameObject_14(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMAINCAMERA_T1071044421_H
#ifndef GETSCREENHEIGHT_T3875108194_H
#define GETSCREENHEIGHT_T3875108194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetScreenHeight
struct  GetScreenHeight_t3875108194  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScreenHeight::storeScreenHeight
	FsmFloat_t2883254149 * ___storeScreenHeight_14;

public:
	inline static int32_t get_offset_of_storeScreenHeight_14() { return static_cast<int32_t>(offsetof(GetScreenHeight_t3875108194, ___storeScreenHeight_14)); }
	inline FsmFloat_t2883254149 * get_storeScreenHeight_14() const { return ___storeScreenHeight_14; }
	inline FsmFloat_t2883254149 ** get_address_of_storeScreenHeight_14() { return &___storeScreenHeight_14; }
	inline void set_storeScreenHeight_14(FsmFloat_t2883254149 * value)
	{
		___storeScreenHeight_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeScreenHeight_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCREENHEIGHT_T3875108194_H
#ifndef GETSCREENWIDTH_T1171329459_H
#define GETSCREENWIDTH_T1171329459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetScreenWidth
struct  GetScreenWidth_t1171329459  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScreenWidth::storeScreenWidth
	FsmFloat_t2883254149 * ___storeScreenWidth_14;

public:
	inline static int32_t get_offset_of_storeScreenWidth_14() { return static_cast<int32_t>(offsetof(GetScreenWidth_t1171329459, ___storeScreenWidth_14)); }
	inline FsmFloat_t2883254149 * get_storeScreenWidth_14() const { return ___storeScreenWidth_14; }
	inline FsmFloat_t2883254149 ** get_address_of_storeScreenWidth_14() { return &___storeScreenWidth_14; }
	inline void set_storeScreenWidth_14(FsmFloat_t2883254149 * value)
	{
		___storeScreenWidth_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeScreenWidth_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCREENWIDTH_T1171329459_H
#ifndef PLAYRANDOMSOUND_T3770793793_H
#define PLAYRANDOMSOUND_T3770793793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayRandomSound
struct  PlayRandomSound_t3770793793  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlayRandomSound::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.PlayRandomSound::position
	FsmVector3_t626444517 * ___position_15;
	// UnityEngine.AudioClip[] HutongGames.PlayMaker.Actions.PlayRandomSound::audioClips
	AudioClipU5BU5D_t143221404* ___audioClips_16;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayRandomSound::weights
	FsmFloatU5BU5D_t3637897416* ___weights_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlayRandomSound::volume
	FsmFloat_t2883254149 * ___volume_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PlayRandomSound::noRepeat
	FsmBool_t163807967 * ___noRepeat_19;
	// System.Int32 HutongGames.PlayMaker.Actions.PlayRandomSound::randomIndex
	int32_t ___randomIndex_20;
	// System.Int32 HutongGames.PlayMaker.Actions.PlayRandomSound::lastIndex
	int32_t ___lastIndex_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___position_15)); }
	inline FsmVector3_t626444517 * get_position_15() const { return ___position_15; }
	inline FsmVector3_t626444517 ** get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(FsmVector3_t626444517 * value)
	{
		___position_15 = value;
		Il2CppCodeGenWriteBarrier((&___position_15), value);
	}

	inline static int32_t get_offset_of_audioClips_16() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___audioClips_16)); }
	inline AudioClipU5BU5D_t143221404* get_audioClips_16() const { return ___audioClips_16; }
	inline AudioClipU5BU5D_t143221404** get_address_of_audioClips_16() { return &___audioClips_16; }
	inline void set_audioClips_16(AudioClipU5BU5D_t143221404* value)
	{
		___audioClips_16 = value;
		Il2CppCodeGenWriteBarrier((&___audioClips_16), value);
	}

	inline static int32_t get_offset_of_weights_17() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___weights_17)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_17() const { return ___weights_17; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_17() { return &___weights_17; }
	inline void set_weights_17(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_17 = value;
		Il2CppCodeGenWriteBarrier((&___weights_17), value);
	}

	inline static int32_t get_offset_of_volume_18() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___volume_18)); }
	inline FsmFloat_t2883254149 * get_volume_18() const { return ___volume_18; }
	inline FsmFloat_t2883254149 ** get_address_of_volume_18() { return &___volume_18; }
	inline void set_volume_18(FsmFloat_t2883254149 * value)
	{
		___volume_18 = value;
		Il2CppCodeGenWriteBarrier((&___volume_18), value);
	}

	inline static int32_t get_offset_of_noRepeat_19() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___noRepeat_19)); }
	inline FsmBool_t163807967 * get_noRepeat_19() const { return ___noRepeat_19; }
	inline FsmBool_t163807967 ** get_address_of_noRepeat_19() { return &___noRepeat_19; }
	inline void set_noRepeat_19(FsmBool_t163807967 * value)
	{
		___noRepeat_19 = value;
		Il2CppCodeGenWriteBarrier((&___noRepeat_19), value);
	}

	inline static int32_t get_offset_of_randomIndex_20() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___randomIndex_20)); }
	inline int32_t get_randomIndex_20() const { return ___randomIndex_20; }
	inline int32_t* get_address_of_randomIndex_20() { return &___randomIndex_20; }
	inline void set_randomIndex_20(int32_t value)
	{
		___randomIndex_20 = value;
	}

	inline static int32_t get_offset_of_lastIndex_21() { return static_cast<int32_t>(offsetof(PlayRandomSound_t3770793793, ___lastIndex_21)); }
	inline int32_t get_lastIndex_21() const { return ___lastIndex_21; }
	inline int32_t* get_address_of_lastIndex_21() { return &___lastIndex_21; }
	inline void set_lastIndex_21(int32_t value)
	{
		___lastIndex_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYRANDOMSOUND_T3770793793_H
#ifndef PLAYSOUND_T1299560039_H
#define PLAYSOUND_T1299560039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlaySound
struct  PlaySound_t1299560039  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlaySound::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.PlaySound::position
	FsmVector3_t626444517 * ___position_15;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.PlaySound::clip
	FsmObject_t2606870197 * ___clip_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlaySound::volume
	FsmFloat_t2883254149 * ___volume_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(PlaySound_t1299560039, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(PlaySound_t1299560039, ___position_15)); }
	inline FsmVector3_t626444517 * get_position_15() const { return ___position_15; }
	inline FsmVector3_t626444517 ** get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(FsmVector3_t626444517 * value)
	{
		___position_15 = value;
		Il2CppCodeGenWriteBarrier((&___position_15), value);
	}

	inline static int32_t get_offset_of_clip_16() { return static_cast<int32_t>(offsetof(PlaySound_t1299560039, ___clip_16)); }
	inline FsmObject_t2606870197 * get_clip_16() const { return ___clip_16; }
	inline FsmObject_t2606870197 ** get_address_of_clip_16() { return &___clip_16; }
	inline void set_clip_16(FsmObject_t2606870197 * value)
	{
		___clip_16 = value;
		Il2CppCodeGenWriteBarrier((&___clip_16), value);
	}

	inline static int32_t get_offset_of_volume_17() { return static_cast<int32_t>(offsetof(PlaySound_t1299560039, ___volume_17)); }
	inline FsmFloat_t2883254149 * get_volume_17() const { return ___volume_17; }
	inline FsmFloat_t2883254149 ** get_address_of_volume_17() { return &___volume_17; }
	inline void set_volume_17(FsmFloat_t2883254149 * value)
	{
		___volume_17 = value;
		Il2CppCodeGenWriteBarrier((&___volume_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYSOUND_T1299560039_H
#ifndef RUNFSMACTION_T3423579716_H
#define RUNFSMACTION_T3423579716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RunFSMAction
struct  RunFSMAction_t3423579716  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Actions.RunFSMAction::runFsm
	Fsm_t4127147824 * ___runFsm_14;

public:
	inline static int32_t get_offset_of_runFsm_14() { return static_cast<int32_t>(offsetof(RunFSMAction_t3423579716, ___runFsm_14)); }
	inline Fsm_t4127147824 * get_runFsm_14() const { return ___runFsm_14; }
	inline Fsm_t4127147824 ** get_address_of_runFsm_14() { return &___runFsm_14; }
	inline void set_runFsm_14(Fsm_t4127147824 * value)
	{
		___runFsm_14 = value;
		Il2CppCodeGenWriteBarrier((&___runFsm_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNFSMACTION_T3423579716_H
#ifndef SCREENTOWORLDPOINT_T1514121105_H
#define SCREENTOWORLDPOINT_T1514121105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenToWorldPoint
struct  ScreenToWorldPoint_t1514121105  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenVector
	FsmVector3_t626444517 * ___screenVector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenX
	FsmFloat_t2883254149 * ___screenX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenY
	FsmFloat_t2883254149 * ___screenY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::screenZ
	FsmFloat_t2883254149 * ___screenZ_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenToWorldPoint::normalized
	FsmBool_t163807967 * ___normalized_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldVector
	FsmVector3_t626444517 * ___storeWorldVector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldX
	FsmFloat_t2883254149 * ___storeWorldX_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldY
	FsmFloat_t2883254149 * ___storeWorldY_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenToWorldPoint::storeWorldZ
	FsmFloat_t2883254149 * ___storeWorldZ_22;
	// System.Boolean HutongGames.PlayMaker.Actions.ScreenToWorldPoint::everyFrame
	bool ___everyFrame_23;

public:
	inline static int32_t get_offset_of_screenVector_14() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___screenVector_14)); }
	inline FsmVector3_t626444517 * get_screenVector_14() const { return ___screenVector_14; }
	inline FsmVector3_t626444517 ** get_address_of_screenVector_14() { return &___screenVector_14; }
	inline void set_screenVector_14(FsmVector3_t626444517 * value)
	{
		___screenVector_14 = value;
		Il2CppCodeGenWriteBarrier((&___screenVector_14), value);
	}

	inline static int32_t get_offset_of_screenX_15() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___screenX_15)); }
	inline FsmFloat_t2883254149 * get_screenX_15() const { return ___screenX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_screenX_15() { return &___screenX_15; }
	inline void set_screenX_15(FsmFloat_t2883254149 * value)
	{
		___screenX_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenX_15), value);
	}

	inline static int32_t get_offset_of_screenY_16() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___screenY_16)); }
	inline FsmFloat_t2883254149 * get_screenY_16() const { return ___screenY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_screenY_16() { return &___screenY_16; }
	inline void set_screenY_16(FsmFloat_t2883254149 * value)
	{
		___screenY_16 = value;
		Il2CppCodeGenWriteBarrier((&___screenY_16), value);
	}

	inline static int32_t get_offset_of_screenZ_17() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___screenZ_17)); }
	inline FsmFloat_t2883254149 * get_screenZ_17() const { return ___screenZ_17; }
	inline FsmFloat_t2883254149 ** get_address_of_screenZ_17() { return &___screenZ_17; }
	inline void set_screenZ_17(FsmFloat_t2883254149 * value)
	{
		___screenZ_17 = value;
		Il2CppCodeGenWriteBarrier((&___screenZ_17), value);
	}

	inline static int32_t get_offset_of_normalized_18() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___normalized_18)); }
	inline FsmBool_t163807967 * get_normalized_18() const { return ___normalized_18; }
	inline FsmBool_t163807967 ** get_address_of_normalized_18() { return &___normalized_18; }
	inline void set_normalized_18(FsmBool_t163807967 * value)
	{
		___normalized_18 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_18), value);
	}

	inline static int32_t get_offset_of_storeWorldVector_19() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___storeWorldVector_19)); }
	inline FsmVector3_t626444517 * get_storeWorldVector_19() const { return ___storeWorldVector_19; }
	inline FsmVector3_t626444517 ** get_address_of_storeWorldVector_19() { return &___storeWorldVector_19; }
	inline void set_storeWorldVector_19(FsmVector3_t626444517 * value)
	{
		___storeWorldVector_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeWorldVector_19), value);
	}

	inline static int32_t get_offset_of_storeWorldX_20() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___storeWorldX_20)); }
	inline FsmFloat_t2883254149 * get_storeWorldX_20() const { return ___storeWorldX_20; }
	inline FsmFloat_t2883254149 ** get_address_of_storeWorldX_20() { return &___storeWorldX_20; }
	inline void set_storeWorldX_20(FsmFloat_t2883254149 * value)
	{
		___storeWorldX_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeWorldX_20), value);
	}

	inline static int32_t get_offset_of_storeWorldY_21() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___storeWorldY_21)); }
	inline FsmFloat_t2883254149 * get_storeWorldY_21() const { return ___storeWorldY_21; }
	inline FsmFloat_t2883254149 ** get_address_of_storeWorldY_21() { return &___storeWorldY_21; }
	inline void set_storeWorldY_21(FsmFloat_t2883254149 * value)
	{
		___storeWorldY_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeWorldY_21), value);
	}

	inline static int32_t get_offset_of_storeWorldZ_22() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___storeWorldZ_22)); }
	inline FsmFloat_t2883254149 * get_storeWorldZ_22() const { return ___storeWorldZ_22; }
	inline FsmFloat_t2883254149 ** get_address_of_storeWorldZ_22() { return &___storeWorldZ_22; }
	inline void set_storeWorldZ_22(FsmFloat_t2883254149 * value)
	{
		___storeWorldZ_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeWorldZ_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(ScreenToWorldPoint_t1514121105, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENTOWORLDPOINT_T1514121105_H
#ifndef SELECTRANDOMCOLOR_T3414736926_H
#define SELECTRANDOMCOLOR_T3414736926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomColor
struct  SelectRandomColor_t3414736926  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.Actions.SelectRandomColor::colors
	FsmColorU5BU5D_t1111370293* ___colors_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomColor::weights
	FsmFloatU5BU5D_t3637897416* ___weights_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SelectRandomColor::storeColor
	FsmColor_t1738900188 * ___storeColor_16;

public:
	inline static int32_t get_offset_of_colors_14() { return static_cast<int32_t>(offsetof(SelectRandomColor_t3414736926, ___colors_14)); }
	inline FsmColorU5BU5D_t1111370293* get_colors_14() const { return ___colors_14; }
	inline FsmColorU5BU5D_t1111370293** get_address_of_colors_14() { return &___colors_14; }
	inline void set_colors_14(FsmColorU5BU5D_t1111370293* value)
	{
		___colors_14 = value;
		Il2CppCodeGenWriteBarrier((&___colors_14), value);
	}

	inline static int32_t get_offset_of_weights_15() { return static_cast<int32_t>(offsetof(SelectRandomColor_t3414736926, ___weights_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_15() const { return ___weights_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_15() { return &___weights_15; }
	inline void set_weights_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_15 = value;
		Il2CppCodeGenWriteBarrier((&___weights_15), value);
	}

	inline static int32_t get_offset_of_storeColor_16() { return static_cast<int32_t>(offsetof(SelectRandomColor_t3414736926, ___storeColor_16)); }
	inline FsmColor_t1738900188 * get_storeColor_16() const { return ___storeColor_16; }
	inline FsmColor_t1738900188 ** get_address_of_storeColor_16() { return &___storeColor_16; }
	inline void set_storeColor_16(FsmColor_t1738900188 * value)
	{
		___storeColor_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeColor_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTRANDOMCOLOR_T3414736926_H
#ifndef SETANIMATORTARGET_T2874597898_H
#define SETANIMATORTARGET_T2874597898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorTarget
struct  SetAnimatorTarget_t2874597898  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// UnityEngine.AvatarTarget HutongGames.PlayMaker.Actions.SetAnimatorTarget::avatarTarget
	int32_t ___avatarTarget_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorTarget::targetNormalizedTime
	FsmFloat_t2883254149 * ___targetNormalizedTime_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorTarget::everyFrame
	bool ___everyFrame_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorTarget::_animator
	Animator_t434523843 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2874597898, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_avatarTarget_15() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2874597898, ___avatarTarget_15)); }
	inline int32_t get_avatarTarget_15() const { return ___avatarTarget_15; }
	inline int32_t* get_address_of_avatarTarget_15() { return &___avatarTarget_15; }
	inline void set_avatarTarget_15(int32_t value)
	{
		___avatarTarget_15 = value;
	}

	inline static int32_t get_offset_of_targetNormalizedTime_16() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2874597898, ___targetNormalizedTime_16)); }
	inline FsmFloat_t2883254149 * get_targetNormalizedTime_16() const { return ___targetNormalizedTime_16; }
	inline FsmFloat_t2883254149 ** get_address_of_targetNormalizedTime_16() { return &___targetNormalizedTime_16; }
	inline void set_targetNormalizedTime_16(FsmFloat_t2883254149 * value)
	{
		___targetNormalizedTime_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetNormalizedTime_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2874597898, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(SetAnimatorTarget_t2874597898, ____animator_18)); }
	inline Animator_t434523843 * get__animator_18() const { return ____animator_18; }
	inline Animator_t434523843 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t434523843 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier((&____animator_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORTARGET_T2874597898_H
#ifndef SETANIMATORTRIGGER_T3267197606_H
#define SETANIMATORTRIGGER_T3267197606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorTrigger
struct  SetAnimatorTrigger_t3267197606  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorTrigger::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimatorTrigger::trigger
	FsmString_t1785915204 * ___trigger_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorTrigger::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t3267197606, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_trigger_15() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t3267197606, ___trigger_15)); }
	inline FsmString_t1785915204 * get_trigger_15() const { return ___trigger_15; }
	inline FsmString_t1785915204 ** get_address_of_trigger_15() { return &___trigger_15; }
	inline void set_trigger_15(FsmString_t1785915204 * value)
	{
		___trigger_15 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(SetAnimatorTrigger_t3267197606, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORTRIGGER_T3267197606_H
#ifndef SETCOLORRGBA_T569523049_H
#define SETCOLORRGBA_T569523049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetColorRGBA
struct  SetColorRGBA_t569523049  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetColorRGBA::colorVariable
	FsmColor_t1738900188 * ___colorVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::red
	FsmFloat_t2883254149 * ___red_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::green
	FsmFloat_t2883254149 * ___green_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::blue
	FsmFloat_t2883254149 * ___blue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetColorRGBA::alpha
	FsmFloat_t2883254149 * ___alpha_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetColorRGBA::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_colorVariable_14() { return static_cast<int32_t>(offsetof(SetColorRGBA_t569523049, ___colorVariable_14)); }
	inline FsmColor_t1738900188 * get_colorVariable_14() const { return ___colorVariable_14; }
	inline FsmColor_t1738900188 ** get_address_of_colorVariable_14() { return &___colorVariable_14; }
	inline void set_colorVariable_14(FsmColor_t1738900188 * value)
	{
		___colorVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariable_14), value);
	}

	inline static int32_t get_offset_of_red_15() { return static_cast<int32_t>(offsetof(SetColorRGBA_t569523049, ___red_15)); }
	inline FsmFloat_t2883254149 * get_red_15() const { return ___red_15; }
	inline FsmFloat_t2883254149 ** get_address_of_red_15() { return &___red_15; }
	inline void set_red_15(FsmFloat_t2883254149 * value)
	{
		___red_15 = value;
		Il2CppCodeGenWriteBarrier((&___red_15), value);
	}

	inline static int32_t get_offset_of_green_16() { return static_cast<int32_t>(offsetof(SetColorRGBA_t569523049, ___green_16)); }
	inline FsmFloat_t2883254149 * get_green_16() const { return ___green_16; }
	inline FsmFloat_t2883254149 ** get_address_of_green_16() { return &___green_16; }
	inline void set_green_16(FsmFloat_t2883254149 * value)
	{
		___green_16 = value;
		Il2CppCodeGenWriteBarrier((&___green_16), value);
	}

	inline static int32_t get_offset_of_blue_17() { return static_cast<int32_t>(offsetof(SetColorRGBA_t569523049, ___blue_17)); }
	inline FsmFloat_t2883254149 * get_blue_17() const { return ___blue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_blue_17() { return &___blue_17; }
	inline void set_blue_17(FsmFloat_t2883254149 * value)
	{
		___blue_17 = value;
		Il2CppCodeGenWriteBarrier((&___blue_17), value);
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(SetColorRGBA_t569523049, ___alpha_18)); }
	inline FsmFloat_t2883254149 * get_alpha_18() const { return ___alpha_18; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(FsmFloat_t2883254149 * value)
	{
		___alpha_18 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetColorRGBA_t569523049, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETCOLORRGBA_T569523049_H
#ifndef SETCOLORVALUE_T1163264102_H
#define SETCOLORVALUE_T1163264102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetColorValue
struct  SetColorValue_t1163264102  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetColorValue::colorVariable
	FsmColor_t1738900188 * ___colorVariable_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetColorValue::color
	FsmColor_t1738900188 * ___color_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetColorValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_colorVariable_14() { return static_cast<int32_t>(offsetof(SetColorValue_t1163264102, ___colorVariable_14)); }
	inline FsmColor_t1738900188 * get_colorVariable_14() const { return ___colorVariable_14; }
	inline FsmColor_t1738900188 ** get_address_of_colorVariable_14() { return &___colorVariable_14; }
	inline void set_colorVariable_14(FsmColor_t1738900188 * value)
	{
		___colorVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariable_14), value);
	}

	inline static int32_t get_offset_of_color_15() { return static_cast<int32_t>(offsetof(SetColorValue_t1163264102, ___color_15)); }
	inline FsmColor_t1738900188 * get_color_15() const { return ___color_15; }
	inline FsmColor_t1738900188 ** get_address_of_color_15() { return &___color_15; }
	inline void set_color_15(FsmColor_t1738900188 * value)
	{
		___color_15 = value;
		Il2CppCodeGenWriteBarrier((&___color_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetColorValue_t1163264102, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETCOLORVALUE_T1163264102_H
#ifndef SETGAMEVOLUME_T3279791200_H
#define SETGAMEVOLUME_T3279791200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGameVolume
struct  SetGameVolume_t3279791200  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGameVolume::volume
	FsmFloat_t2883254149 * ___volume_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGameVolume::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_volume_14() { return static_cast<int32_t>(offsetof(SetGameVolume_t3279791200, ___volume_14)); }
	inline FsmFloat_t2883254149 * get_volume_14() const { return ___volume_14; }
	inline FsmFloat_t2883254149 ** get_address_of_volume_14() { return &___volume_14; }
	inline void set_volume_14(FsmFloat_t2883254149 * value)
	{
		___volume_14 = value;
		Il2CppCodeGenWriteBarrier((&___volume_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetGameVolume_t3279791200, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGAMEVOLUME_T3279791200_H
#ifndef SETMAINCAMERA_T3201888481_H
#define SETMAINCAMERA_T3201888481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMainCamera
struct  SetMainCamera_t3201888481  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetMainCamera::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetMainCamera_t3201888481, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMAINCAMERA_T3201888481_H
#ifndef TAKESCREENSHOT_T2218975985_H
#define TAKESCREENSHOT_T2218975985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TakeScreenshot
struct  TakeScreenshot_t2218975985  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.TakeScreenshot/Destination HutongGames.PlayMaker.Actions.TakeScreenshot::destination
	int32_t ___destination_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.TakeScreenshot::customPath
	FsmString_t1785915204 * ___customPath_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.TakeScreenshot::filename
	FsmString_t1785915204 * ___filename_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.TakeScreenshot::autoNumber
	FsmBool_t163807967 * ___autoNumber_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TakeScreenshot::superSize
	FsmInt_t874273141 * ___superSize_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.TakeScreenshot::debugLog
	FsmBool_t163807967 * ___debugLog_19;
	// System.Int32 HutongGames.PlayMaker.Actions.TakeScreenshot::screenshotCount
	int32_t ___screenshotCount_20;

public:
	inline static int32_t get_offset_of_destination_14() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___destination_14)); }
	inline int32_t get_destination_14() const { return ___destination_14; }
	inline int32_t* get_address_of_destination_14() { return &___destination_14; }
	inline void set_destination_14(int32_t value)
	{
		___destination_14 = value;
	}

	inline static int32_t get_offset_of_customPath_15() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___customPath_15)); }
	inline FsmString_t1785915204 * get_customPath_15() const { return ___customPath_15; }
	inline FsmString_t1785915204 ** get_address_of_customPath_15() { return &___customPath_15; }
	inline void set_customPath_15(FsmString_t1785915204 * value)
	{
		___customPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___customPath_15), value);
	}

	inline static int32_t get_offset_of_filename_16() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___filename_16)); }
	inline FsmString_t1785915204 * get_filename_16() const { return ___filename_16; }
	inline FsmString_t1785915204 ** get_address_of_filename_16() { return &___filename_16; }
	inline void set_filename_16(FsmString_t1785915204 * value)
	{
		___filename_16 = value;
		Il2CppCodeGenWriteBarrier((&___filename_16), value);
	}

	inline static int32_t get_offset_of_autoNumber_17() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___autoNumber_17)); }
	inline FsmBool_t163807967 * get_autoNumber_17() const { return ___autoNumber_17; }
	inline FsmBool_t163807967 ** get_address_of_autoNumber_17() { return &___autoNumber_17; }
	inline void set_autoNumber_17(FsmBool_t163807967 * value)
	{
		___autoNumber_17 = value;
		Il2CppCodeGenWriteBarrier((&___autoNumber_17), value);
	}

	inline static int32_t get_offset_of_superSize_18() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___superSize_18)); }
	inline FsmInt_t874273141 * get_superSize_18() const { return ___superSize_18; }
	inline FsmInt_t874273141 ** get_address_of_superSize_18() { return &___superSize_18; }
	inline void set_superSize_18(FsmInt_t874273141 * value)
	{
		___superSize_18 = value;
		Il2CppCodeGenWriteBarrier((&___superSize_18), value);
	}

	inline static int32_t get_offset_of_debugLog_19() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___debugLog_19)); }
	inline FsmBool_t163807967 * get_debugLog_19() const { return ___debugLog_19; }
	inline FsmBool_t163807967 ** get_address_of_debugLog_19() { return &___debugLog_19; }
	inline void set_debugLog_19(FsmBool_t163807967 * value)
	{
		___debugLog_19 = value;
		Il2CppCodeGenWriteBarrier((&___debugLog_19), value);
	}

	inline static int32_t get_offset_of_screenshotCount_20() { return static_cast<int32_t>(offsetof(TakeScreenshot_t2218975985, ___screenshotCount_20)); }
	inline int32_t get_screenshotCount_20() const { return ___screenshotCount_20; }
	inline int32_t* get_address_of_screenshotCount_20() { return &___screenshotCount_20; }
	inline void set_screenshotCount_20(int32_t value)
	{
		___screenshotCount_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAKESCREENSHOT_T2218975985_H
#ifndef WORLDTOSCREENPOINT_T3427403956_H
#define WORLDTOSCREENPOINT_T3427403956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WorldToScreenPoint
struct  WorldToScreenPoint_t3427403956  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldPosition
	FsmVector3_t626444517 * ___worldPosition_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldX
	FsmFloat_t2883254149 * ___worldX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldY
	FsmFloat_t2883254149 * ___worldY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::worldZ
	FsmFloat_t2883254149 * ___worldZ_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.WorldToScreenPoint::storeScreenPoint
	FsmVector3_t626444517 * ___storeScreenPoint_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::storeScreenX
	FsmFloat_t2883254149 * ___storeScreenX_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.WorldToScreenPoint::storeScreenY
	FsmFloat_t2883254149 * ___storeScreenY_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.WorldToScreenPoint::normalize
	FsmBool_t163807967 * ___normalize_21;
	// System.Boolean HutongGames.PlayMaker.Actions.WorldToScreenPoint::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_worldPosition_14() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___worldPosition_14)); }
	inline FsmVector3_t626444517 * get_worldPosition_14() const { return ___worldPosition_14; }
	inline FsmVector3_t626444517 ** get_address_of_worldPosition_14() { return &___worldPosition_14; }
	inline void set_worldPosition_14(FsmVector3_t626444517 * value)
	{
		___worldPosition_14 = value;
		Il2CppCodeGenWriteBarrier((&___worldPosition_14), value);
	}

	inline static int32_t get_offset_of_worldX_15() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___worldX_15)); }
	inline FsmFloat_t2883254149 * get_worldX_15() const { return ___worldX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_worldX_15() { return &___worldX_15; }
	inline void set_worldX_15(FsmFloat_t2883254149 * value)
	{
		___worldX_15 = value;
		Il2CppCodeGenWriteBarrier((&___worldX_15), value);
	}

	inline static int32_t get_offset_of_worldY_16() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___worldY_16)); }
	inline FsmFloat_t2883254149 * get_worldY_16() const { return ___worldY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_worldY_16() { return &___worldY_16; }
	inline void set_worldY_16(FsmFloat_t2883254149 * value)
	{
		___worldY_16 = value;
		Il2CppCodeGenWriteBarrier((&___worldY_16), value);
	}

	inline static int32_t get_offset_of_worldZ_17() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___worldZ_17)); }
	inline FsmFloat_t2883254149 * get_worldZ_17() const { return ___worldZ_17; }
	inline FsmFloat_t2883254149 ** get_address_of_worldZ_17() { return &___worldZ_17; }
	inline void set_worldZ_17(FsmFloat_t2883254149 * value)
	{
		___worldZ_17 = value;
		Il2CppCodeGenWriteBarrier((&___worldZ_17), value);
	}

	inline static int32_t get_offset_of_storeScreenPoint_18() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___storeScreenPoint_18)); }
	inline FsmVector3_t626444517 * get_storeScreenPoint_18() const { return ___storeScreenPoint_18; }
	inline FsmVector3_t626444517 ** get_address_of_storeScreenPoint_18() { return &___storeScreenPoint_18; }
	inline void set_storeScreenPoint_18(FsmVector3_t626444517 * value)
	{
		___storeScreenPoint_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeScreenPoint_18), value);
	}

	inline static int32_t get_offset_of_storeScreenX_19() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___storeScreenX_19)); }
	inline FsmFloat_t2883254149 * get_storeScreenX_19() const { return ___storeScreenX_19; }
	inline FsmFloat_t2883254149 ** get_address_of_storeScreenX_19() { return &___storeScreenX_19; }
	inline void set_storeScreenX_19(FsmFloat_t2883254149 * value)
	{
		___storeScreenX_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeScreenX_19), value);
	}

	inline static int32_t get_offset_of_storeScreenY_20() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___storeScreenY_20)); }
	inline FsmFloat_t2883254149 * get_storeScreenY_20() const { return ___storeScreenY_20; }
	inline FsmFloat_t2883254149 ** get_address_of_storeScreenY_20() { return &___storeScreenY_20; }
	inline void set_storeScreenY_20(FsmFloat_t2883254149 * value)
	{
		___storeScreenY_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeScreenY_20), value);
	}

	inline static int32_t get_offset_of_normalize_21() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___normalize_21)); }
	inline FsmBool_t163807967 * get_normalize_21() const { return ___normalize_21; }
	inline FsmBool_t163807967 ** get_address_of_normalize_21() { return &___normalize_21; }
	inline void set_normalize_21(FsmBool_t163807967 * value)
	{
		___normalize_21 = value;
		Il2CppCodeGenWriteBarrier((&___normalize_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(WorldToScreenPoint_t3427403956, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDTOSCREENPOINT_T3427403956_H
#ifndef ARRAYFOREACH_T4198384709_H
#define ARRAYFOREACH_T4198384709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayForEach
struct  ArrayForEach_t4198384709  : public RunFSMAction_t3423579716
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayForEach::array
	FsmArray_t1756862219 * ___array_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayForEach::storeItem
	FsmVar_t2413660594 * ___storeItem_16;
	// HutongGames.PlayMaker.FsmTemplateControl HutongGames.PlayMaker.Actions.ArrayForEach::fsmTemplateControl
	FsmTemplateControl_t522200103 * ___fsmTemplateControl_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayForEach::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_18;
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayForEach::currentIndex
	int32_t ___currentIndex_19;

public:
	inline static int32_t get_offset_of_array_15() { return static_cast<int32_t>(offsetof(ArrayForEach_t4198384709, ___array_15)); }
	inline FsmArray_t1756862219 * get_array_15() const { return ___array_15; }
	inline FsmArray_t1756862219 ** get_address_of_array_15() { return &___array_15; }
	inline void set_array_15(FsmArray_t1756862219 * value)
	{
		___array_15 = value;
		Il2CppCodeGenWriteBarrier((&___array_15), value);
	}

	inline static int32_t get_offset_of_storeItem_16() { return static_cast<int32_t>(offsetof(ArrayForEach_t4198384709, ___storeItem_16)); }
	inline FsmVar_t2413660594 * get_storeItem_16() const { return ___storeItem_16; }
	inline FsmVar_t2413660594 ** get_address_of_storeItem_16() { return &___storeItem_16; }
	inline void set_storeItem_16(FsmVar_t2413660594 * value)
	{
		___storeItem_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeItem_16), value);
	}

	inline static int32_t get_offset_of_fsmTemplateControl_17() { return static_cast<int32_t>(offsetof(ArrayForEach_t4198384709, ___fsmTemplateControl_17)); }
	inline FsmTemplateControl_t522200103 * get_fsmTemplateControl_17() const { return ___fsmTemplateControl_17; }
	inline FsmTemplateControl_t522200103 ** get_address_of_fsmTemplateControl_17() { return &___fsmTemplateControl_17; }
	inline void set_fsmTemplateControl_17(FsmTemplateControl_t522200103 * value)
	{
		___fsmTemplateControl_17 = value;
		Il2CppCodeGenWriteBarrier((&___fsmTemplateControl_17), value);
	}

	inline static int32_t get_offset_of_finishEvent_18() { return static_cast<int32_t>(offsetof(ArrayForEach_t4198384709, ___finishEvent_18)); }
	inline FsmEvent_t3736299882 * get_finishEvent_18() const { return ___finishEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_18() { return &___finishEvent_18; }
	inline void set_finishEvent_18(FsmEvent_t3736299882 * value)
	{
		___finishEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_18), value);
	}

	inline static int32_t get_offset_of_currentIndex_19() { return static_cast<int32_t>(offsetof(ArrayForEach_t4198384709, ___currentIndex_19)); }
	inline int32_t get_currentIndex_19() const { return ___currentIndex_19; }
	inline int32_t* get_address_of_currentIndex_19() { return &___currentIndex_19; }
	inline void set_currentIndex_19(int32_t value)
	{
		___currentIndex_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYFOREACH_T4198384709_H
#ifndef DEBUGBOOL_T4160504148_H
#define DEBUGBOOL_T4160504148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugBool
struct  DebugBool_t4160504148  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugBool::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DebugBool::boolVariable
	FsmBool_t163807967 * ___boolVariable_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugBool_t4160504148, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_boolVariable_16() { return static_cast<int32_t>(offsetof(DebugBool_t4160504148, ___boolVariable_16)); }
	inline FsmBool_t163807967 * get_boolVariable_16() const { return ___boolVariable_16; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_16() { return &___boolVariable_16; }
	inline void set_boolVariable_16(FsmBool_t163807967 * value)
	{
		___boolVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGBOOL_T4160504148_H
#ifndef DEBUGENUM_T1656796654_H
#define DEBUGENUM_T1656796654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugEnum
struct  DebugEnum_t1656796654  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugEnum::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.DebugEnum::enumVariable
	FsmEnum_t2861764163 * ___enumVariable_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugEnum_t1656796654, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_enumVariable_16() { return static_cast<int32_t>(offsetof(DebugEnum_t1656796654, ___enumVariable_16)); }
	inline FsmEnum_t2861764163 * get_enumVariable_16() const { return ___enumVariable_16; }
	inline FsmEnum_t2861764163 ** get_address_of_enumVariable_16() { return &___enumVariable_16; }
	inline void set_enumVariable_16(FsmEnum_t2861764163 * value)
	{
		___enumVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariable_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGENUM_T1656796654_H
#ifndef DEBUGFLOAT_T854975360_H
#define DEBUGFLOAT_T854975360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugFloat
struct  DebugFloat_t854975360  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugFloat::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DebugFloat::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugFloat_t854975360, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_floatVariable_16() { return static_cast<int32_t>(offsetof(DebugFloat_t854975360, ___floatVariable_16)); }
	inline FsmFloat_t2883254149 * get_floatVariable_16() const { return ___floatVariable_16; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_16() { return &___floatVariable_16; }
	inline void set_floatVariable_16(FsmFloat_t2883254149 * value)
	{
		___floatVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGFLOAT_T854975360_H
#ifndef DEBUGFSMVARIABLE_T3344673650_H
#define DEBUGFSMVARIABLE_T3344673650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugFsmVariable
struct  DebugFsmVariable_t3344673650  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugFsmVariable::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.DebugFsmVariable::variable
	FsmVar_t2413660594 * ___variable_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugFsmVariable_t3344673650, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_variable_16() { return static_cast<int32_t>(offsetof(DebugFsmVariable_t3344673650, ___variable_16)); }
	inline FsmVar_t2413660594 * get_variable_16() const { return ___variable_16; }
	inline FsmVar_t2413660594 ** get_address_of_variable_16() { return &___variable_16; }
	inline void set_variable_16(FsmVar_t2413660594 * value)
	{
		___variable_16 = value;
		Il2CppCodeGenWriteBarrier((&___variable_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGFSMVARIABLE_T3344673650_H
#ifndef DEBUGGAMEOBJECT_T1715091115_H
#define DEBUGGAMEOBJECT_T1715091115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugGameObject
struct  DebugGameObject_t1715091115  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugGameObject::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DebugGameObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugGameObject_t1715091115, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(DebugGameObject_t1715091115, ___gameObject_16)); }
	inline FsmGameObject_t3581898942 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmGameObject_t3581898942 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGAMEOBJECT_T1715091115_H
#ifndef DEBUGINT_T2483236234_H
#define DEBUGINT_T2483236234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugInt
struct  DebugInt_t2483236234  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugInt::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DebugInt::intVariable
	FsmInt_t874273141 * ___intVariable_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugInt_t2483236234, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_intVariable_16() { return static_cast<int32_t>(offsetof(DebugInt_t2483236234, ___intVariable_16)); }
	inline FsmInt_t874273141 * get_intVariable_16() const { return ___intVariable_16; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_16() { return &___intVariable_16; }
	inline void set_intVariable_16(FsmInt_t874273141 * value)
	{
		___intVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINT_T2483236234_H
#ifndef DEBUGLOG_T910110091_H
#define DEBUGLOG_T910110091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugLog
struct  DebugLog_t910110091  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugLog::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DebugLog::text
	FsmString_t1785915204 * ___text_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugLog_t910110091, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(DebugLog_t910110091, ___text_16)); }
	inline FsmString_t1785915204 * get_text_16() const { return ___text_16; }
	inline FsmString_t1785915204 ** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(FsmString_t1785915204 * value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOG_T910110091_H
#ifndef DEBUGOBJECT_T4048031855_H
#define DEBUGOBJECT_T4048031855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugObject
struct  DebugObject_t4048031855  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugObject::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.DebugObject::fsmObject
	FsmObject_t2606870197 * ___fsmObject_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugObject_t4048031855, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_fsmObject_16() { return static_cast<int32_t>(offsetof(DebugObject_t4048031855, ___fsmObject_16)); }
	inline FsmObject_t2606870197 * get_fsmObject_16() const { return ___fsmObject_16; }
	inline FsmObject_t2606870197 ** get_address_of_fsmObject_16() { return &___fsmObject_16; }
	inline void set_fsmObject_16(FsmObject_t2606870197 * value)
	{
		___fsmObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___fsmObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGOBJECT_T4048031855_H
#ifndef DEBUGVECTOR3_T1875006145_H
#define DEBUGVECTOR3_T1875006145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugVector3
struct  DebugVector3_t1875006145  : public BaseLogAction_t823318833
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugVector3::logLevel
	int32_t ___logLevel_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DebugVector3::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_16;

public:
	inline static int32_t get_offset_of_logLevel_15() { return static_cast<int32_t>(offsetof(DebugVector3_t1875006145, ___logLevel_15)); }
	inline int32_t get_logLevel_15() const { return ___logLevel_15; }
	inline int32_t* get_address_of_logLevel_15() { return &___logLevel_15; }
	inline void set_logLevel_15(int32_t value)
	{
		___logLevel_15 = value;
	}

	inline static int32_t get_offset_of_vector3Variable_16() { return static_cast<int32_t>(offsetof(DebugVector3_t1875006145, ___vector3Variable_16)); }
	inline FsmVector3_t626444517 * get_vector3Variable_16() const { return ___vector3Variable_16; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_16() { return &___vector3Variable_16; }
	inline void set_vector3Variable_16(FsmVector3_t626444517 * value)
	{
		___vector3Variable_16 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGVECTOR3_T1875006145_H
#ifndef GETFSMARRAY_T1165031280_H
#define GETFSMARRAY_T1165031280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmArray
struct  GetFsmArray_t1165031280  : public BaseFsmVariableAction_t2713817827
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmArray::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmArray::fsmName
	FsmString_t1785915204 * ___fsmName_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmArray::variableName
	FsmString_t1785915204 * ___variableName_21;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetFsmArray::storeValue
	FsmArray_t1756862219 * ___storeValue_22;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmArray::copyValues
	bool ___copyValues_23;

public:
	inline static int32_t get_offset_of_gameObject_19() { return static_cast<int32_t>(offsetof(GetFsmArray_t1165031280, ___gameObject_19)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_19() const { return ___gameObject_19; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_19() { return &___gameObject_19; }
	inline void set_gameObject_19(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_19), value);
	}

	inline static int32_t get_offset_of_fsmName_20() { return static_cast<int32_t>(offsetof(GetFsmArray_t1165031280, ___fsmName_20)); }
	inline FsmString_t1785915204 * get_fsmName_20() const { return ___fsmName_20; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_20() { return &___fsmName_20; }
	inline void set_fsmName_20(FsmString_t1785915204 * value)
	{
		___fsmName_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_20), value);
	}

	inline static int32_t get_offset_of_variableName_21() { return static_cast<int32_t>(offsetof(GetFsmArray_t1165031280, ___variableName_21)); }
	inline FsmString_t1785915204 * get_variableName_21() const { return ___variableName_21; }
	inline FsmString_t1785915204 ** get_address_of_variableName_21() { return &___variableName_21; }
	inline void set_variableName_21(FsmString_t1785915204 * value)
	{
		___variableName_21 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_21), value);
	}

	inline static int32_t get_offset_of_storeValue_22() { return static_cast<int32_t>(offsetof(GetFsmArray_t1165031280, ___storeValue_22)); }
	inline FsmArray_t1756862219 * get_storeValue_22() const { return ___storeValue_22; }
	inline FsmArray_t1756862219 ** get_address_of_storeValue_22() { return &___storeValue_22; }
	inline void set_storeValue_22(FsmArray_t1756862219 * value)
	{
		___storeValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_22), value);
	}

	inline static int32_t get_offset_of_copyValues_23() { return static_cast<int32_t>(offsetof(GetFsmArray_t1165031280, ___copyValues_23)); }
	inline bool get_copyValues_23() const { return ___copyValues_23; }
	inline bool* get_address_of_copyValues_23() { return &___copyValues_23; }
	inline void set_copyValues_23(bool value)
	{
		___copyValues_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMARRAY_T1165031280_H
#ifndef GETFSMARRAYITEM_T1269604847_H
#define GETFSMARRAYITEM_T1269604847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmArrayItem
struct  GetFsmArrayItem_t1269604847  : public BaseFsmVariableIndexAction_t3448140877
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmArrayItem::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmArrayItem::fsmName
	FsmString_t1785915204 * ___fsmName_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmArrayItem::variableName
	FsmString_t1785915204 * ___variableName_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetFsmArrayItem::index
	FsmInt_t874273141 * ___index_23;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.GetFsmArrayItem::storeValue
	FsmVar_t2413660594 * ___storeValue_24;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmArrayItem::everyFrame
	bool ___everyFrame_25;

public:
	inline static int32_t get_offset_of_gameObject_20() { return static_cast<int32_t>(offsetof(GetFsmArrayItem_t1269604847, ___gameObject_20)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_20() const { return ___gameObject_20; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_20() { return &___gameObject_20; }
	inline void set_gameObject_20(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_20), value);
	}

	inline static int32_t get_offset_of_fsmName_21() { return static_cast<int32_t>(offsetof(GetFsmArrayItem_t1269604847, ___fsmName_21)); }
	inline FsmString_t1785915204 * get_fsmName_21() const { return ___fsmName_21; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_21() { return &___fsmName_21; }
	inline void set_fsmName_21(FsmString_t1785915204 * value)
	{
		___fsmName_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_21), value);
	}

	inline static int32_t get_offset_of_variableName_22() { return static_cast<int32_t>(offsetof(GetFsmArrayItem_t1269604847, ___variableName_22)); }
	inline FsmString_t1785915204 * get_variableName_22() const { return ___variableName_22; }
	inline FsmString_t1785915204 ** get_address_of_variableName_22() { return &___variableName_22; }
	inline void set_variableName_22(FsmString_t1785915204 * value)
	{
		___variableName_22 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_22), value);
	}

	inline static int32_t get_offset_of_index_23() { return static_cast<int32_t>(offsetof(GetFsmArrayItem_t1269604847, ___index_23)); }
	inline FsmInt_t874273141 * get_index_23() const { return ___index_23; }
	inline FsmInt_t874273141 ** get_address_of_index_23() { return &___index_23; }
	inline void set_index_23(FsmInt_t874273141 * value)
	{
		___index_23 = value;
		Il2CppCodeGenWriteBarrier((&___index_23), value);
	}

	inline static int32_t get_offset_of_storeValue_24() { return static_cast<int32_t>(offsetof(GetFsmArrayItem_t1269604847, ___storeValue_24)); }
	inline FsmVar_t2413660594 * get_storeValue_24() const { return ___storeValue_24; }
	inline FsmVar_t2413660594 ** get_address_of_storeValue_24() { return &___storeValue_24; }
	inline void set_storeValue_24(FsmVar_t2413660594 * value)
	{
		___storeValue_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_24), value);
	}

	inline static int32_t get_offset_of_everyFrame_25() { return static_cast<int32_t>(offsetof(GetFsmArrayItem_t1269604847, ___everyFrame_25)); }
	inline bool get_everyFrame_25() const { return ___everyFrame_25; }
	inline bool* get_address_of_everyFrame_25() { return &___everyFrame_25; }
	inline void set_everyFrame_25(bool value)
	{
		___everyFrame_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMARRAYITEM_T1269604847_H
#ifndef SETAUDIOCLIP_T1573938335_H
#define SETAUDIOCLIP_T1573938335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioClip
struct  SetAudioClip_t1573938335  : public ComponentAction_1_t2517625514
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioClip::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetAudioClip::audioClip
	FsmObject_t2606870197 * ___audioClip_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAudioClip_t1573938335, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_audioClip_17() { return static_cast<int32_t>(offsetof(SetAudioClip_t1573938335, ___audioClip_17)); }
	inline FsmObject_t2606870197 * get_audioClip_17() const { return ___audioClip_17; }
	inline FsmObject_t2606870197 ** get_address_of_audioClip_17() { return &___audioClip_17; }
	inline void set_audioClip_17(FsmObject_t2606870197 * value)
	{
		___audioClip_17 = value;
		Il2CppCodeGenWriteBarrier((&___audioClip_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETAUDIOCLIP_T1573938335_H
#ifndef SETAUDIOLOOP_T3865536326_H
#define SETAUDIOLOOP_T3865536326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioLoop
struct  SetAudioLoop_t3865536326  : public ComponentAction_1_t2517625514
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioLoop::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAudioLoop::loop
	FsmBool_t163807967 * ___loop_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAudioLoop_t3865536326, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_loop_17() { return static_cast<int32_t>(offsetof(SetAudioLoop_t3865536326, ___loop_17)); }
	inline FsmBool_t163807967 * get_loop_17() const { return ___loop_17; }
	inline FsmBool_t163807967 ** get_address_of_loop_17() { return &___loop_17; }
	inline void set_loop_17(FsmBool_t163807967 * value)
	{
		___loop_17 = value;
		Il2CppCodeGenWriteBarrier((&___loop_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETAUDIOLOOP_T3865536326_H
#ifndef SETAUDIOPITCH_T4104971792_H
#define SETAUDIOPITCH_T4104971792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioPitch
struct  SetAudioPitch_t4104971792  : public ComponentAction_1_t2517625514
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioPitch::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAudioPitch::pitch
	FsmFloat_t2883254149 * ___pitch_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAudioPitch::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAudioPitch_t4104971792, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_pitch_17() { return static_cast<int32_t>(offsetof(SetAudioPitch_t4104971792, ___pitch_17)); }
	inline FsmFloat_t2883254149 * get_pitch_17() const { return ___pitch_17; }
	inline FsmFloat_t2883254149 ** get_address_of_pitch_17() { return &___pitch_17; }
	inline void set_pitch_17(FsmFloat_t2883254149 * value)
	{
		___pitch_17 = value;
		Il2CppCodeGenWriteBarrier((&___pitch_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetAudioPitch_t4104971792, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETAUDIOPITCH_T4104971792_H
#ifndef SETAUDIOVOLUME_T2383236921_H
#define SETAUDIOVOLUME_T2383236921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAudioVolume
struct  SetAudioVolume_t2383236921  : public ComponentAction_1_t2517625514
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAudioVolume::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAudioVolume::volume
	FsmFloat_t2883254149 * ___volume_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAudioVolume::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAudioVolume_t2383236921, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_volume_17() { return static_cast<int32_t>(offsetof(SetAudioVolume_t2383236921, ___volume_17)); }
	inline FsmFloat_t2883254149 * get_volume_17() const { return ___volume_17; }
	inline FsmFloat_t2883254149 ** get_address_of_volume_17() { return &___volume_17; }
	inline void set_volume_17(FsmFloat_t2883254149 * value)
	{
		___volume_17 = value;
		Il2CppCodeGenWriteBarrier((&___volume_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetAudioVolume_t2383236921, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETAUDIOVOLUME_T2383236921_H
#ifndef SETBACKGROUNDCOLOR_T4243691100_H
#define SETBACKGROUNDCOLOR_T4243691100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetBackgroundColor
struct  SetBackgroundColor_t4243691100  : public ComponentAction_1_t2739473797
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetBackgroundColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetBackgroundColor::backgroundColor
	FsmColor_t1738900188 * ___backgroundColor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetBackgroundColor::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t4243691100, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_backgroundColor_17() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t4243691100, ___backgroundColor_17)); }
	inline FsmColor_t1738900188 * get_backgroundColor_17() const { return ___backgroundColor_17; }
	inline FsmColor_t1738900188 ** get_address_of_backgroundColor_17() { return &___backgroundColor_17; }
	inline void set_backgroundColor_17(FsmColor_t1738900188 * value)
	{
		___backgroundColor_17 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundColor_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetBackgroundColor_t4243691100, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETBACKGROUNDCOLOR_T4243691100_H
#ifndef SETCAMERACULLINGMASK_T2305940360_H
#define SETCAMERACULLINGMASK_T2305940360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetCameraCullingMask
struct  SetCameraCullingMask_t2305940360  : public ComponentAction_1_t2739473797
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetCameraCullingMask::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.SetCameraCullingMask::cullingMask
	FsmIntU5BU5D_t2904461592* ___cullingMask_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetCameraCullingMask::invertMask
	FsmBool_t163807967 * ___invertMask_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetCameraCullingMask::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2305940360, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_cullingMask_17() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2305940360, ___cullingMask_17)); }
	inline FsmIntU5BU5D_t2904461592* get_cullingMask_17() const { return ___cullingMask_17; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_cullingMask_17() { return &___cullingMask_17; }
	inline void set_cullingMask_17(FsmIntU5BU5D_t2904461592* value)
	{
		___cullingMask_17 = value;
		Il2CppCodeGenWriteBarrier((&___cullingMask_17), value);
	}

	inline static int32_t get_offset_of_invertMask_18() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2305940360, ___invertMask_18)); }
	inline FsmBool_t163807967 * get_invertMask_18() const { return ___invertMask_18; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_18() { return &___invertMask_18; }
	inline void set_invertMask_18(FsmBool_t163807967 * value)
	{
		___invertMask_18 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetCameraCullingMask_t2305940360, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETCAMERACULLINGMASK_T2305940360_H
#ifndef SETCAMERAFOV_T2938174343_H
#define SETCAMERAFOV_T2938174343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetCameraFOV
struct  SetCameraFOV_t2938174343  : public ComponentAction_1_t2739473797
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetCameraFOV::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetCameraFOV::fieldOfView
	FsmFloat_t2883254149 * ___fieldOfView_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetCameraFOV::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetCameraFOV_t2938174343, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_fieldOfView_17() { return static_cast<int32_t>(offsetof(SetCameraFOV_t2938174343, ___fieldOfView_17)); }
	inline FsmFloat_t2883254149 * get_fieldOfView_17() const { return ___fieldOfView_17; }
	inline FsmFloat_t2883254149 ** get_address_of_fieldOfView_17() { return &___fieldOfView_17; }
	inline void set_fieldOfView_17(FsmFloat_t2883254149 * value)
	{
		___fieldOfView_17 = value;
		Il2CppCodeGenWriteBarrier((&___fieldOfView_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetCameraFOV_t2938174343, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETCAMERAFOV_T2938174343_H
#ifndef SETFSMARRAY_T364677932_H
#define SETFSMARRAY_T364677932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmArray
struct  SetFsmArray_t364677932  : public BaseFsmVariableAction_t2713817827
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmArray::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmArray::fsmName
	FsmString_t1785915204 * ___fsmName_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmArray::variableName
	FsmString_t1785915204 * ___variableName_21;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.SetFsmArray::setValue
	FsmArray_t1756862219 * ___setValue_22;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmArray::copyValues
	bool ___copyValues_23;

public:
	inline static int32_t get_offset_of_gameObject_19() { return static_cast<int32_t>(offsetof(SetFsmArray_t364677932, ___gameObject_19)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_19() const { return ___gameObject_19; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_19() { return &___gameObject_19; }
	inline void set_gameObject_19(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_19), value);
	}

	inline static int32_t get_offset_of_fsmName_20() { return static_cast<int32_t>(offsetof(SetFsmArray_t364677932, ___fsmName_20)); }
	inline FsmString_t1785915204 * get_fsmName_20() const { return ___fsmName_20; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_20() { return &___fsmName_20; }
	inline void set_fsmName_20(FsmString_t1785915204 * value)
	{
		___fsmName_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_20), value);
	}

	inline static int32_t get_offset_of_variableName_21() { return static_cast<int32_t>(offsetof(SetFsmArray_t364677932, ___variableName_21)); }
	inline FsmString_t1785915204 * get_variableName_21() const { return ___variableName_21; }
	inline FsmString_t1785915204 ** get_address_of_variableName_21() { return &___variableName_21; }
	inline void set_variableName_21(FsmString_t1785915204 * value)
	{
		___variableName_21 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_21), value);
	}

	inline static int32_t get_offset_of_setValue_22() { return static_cast<int32_t>(offsetof(SetFsmArray_t364677932, ___setValue_22)); }
	inline FsmArray_t1756862219 * get_setValue_22() const { return ___setValue_22; }
	inline FsmArray_t1756862219 ** get_address_of_setValue_22() { return &___setValue_22; }
	inline void set_setValue_22(FsmArray_t1756862219 * value)
	{
		___setValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_22), value);
	}

	inline static int32_t get_offset_of_copyValues_23() { return static_cast<int32_t>(offsetof(SetFsmArray_t364677932, ___copyValues_23)); }
	inline bool get_copyValues_23() const { return ___copyValues_23; }
	inline bool* get_address_of_copyValues_23() { return &___copyValues_23; }
	inline void set_copyValues_23(bool value)
	{
		___copyValues_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMARRAY_T364677932_H
#ifndef SETFSMARRAYITEM_T2346556339_H
#define SETFSMARRAYITEM_T2346556339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmArrayItem
struct  SetFsmArrayItem_t2346556339  : public BaseFsmVariableIndexAction_t3448140877
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmArrayItem::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmArrayItem::fsmName
	FsmString_t1785915204 * ___fsmName_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmArrayItem::variableName
	FsmString_t1785915204 * ___variableName_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetFsmArrayItem::index
	FsmInt_t874273141 * ___index_23;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.SetFsmArrayItem::value
	FsmVar_t2413660594 * ___value_24;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmArrayItem::everyFrame
	bool ___everyFrame_25;

public:
	inline static int32_t get_offset_of_gameObject_20() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t2346556339, ___gameObject_20)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_20() const { return ___gameObject_20; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_20() { return &___gameObject_20; }
	inline void set_gameObject_20(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_20), value);
	}

	inline static int32_t get_offset_of_fsmName_21() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t2346556339, ___fsmName_21)); }
	inline FsmString_t1785915204 * get_fsmName_21() const { return ___fsmName_21; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_21() { return &___fsmName_21; }
	inline void set_fsmName_21(FsmString_t1785915204 * value)
	{
		___fsmName_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_21), value);
	}

	inline static int32_t get_offset_of_variableName_22() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t2346556339, ___variableName_22)); }
	inline FsmString_t1785915204 * get_variableName_22() const { return ___variableName_22; }
	inline FsmString_t1785915204 ** get_address_of_variableName_22() { return &___variableName_22; }
	inline void set_variableName_22(FsmString_t1785915204 * value)
	{
		___variableName_22 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_22), value);
	}

	inline static int32_t get_offset_of_index_23() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t2346556339, ___index_23)); }
	inline FsmInt_t874273141 * get_index_23() const { return ___index_23; }
	inline FsmInt_t874273141 ** get_address_of_index_23() { return &___index_23; }
	inline void set_index_23(FsmInt_t874273141 * value)
	{
		___index_23 = value;
		Il2CppCodeGenWriteBarrier((&___index_23), value);
	}

	inline static int32_t get_offset_of_value_24() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t2346556339, ___value_24)); }
	inline FsmVar_t2413660594 * get_value_24() const { return ___value_24; }
	inline FsmVar_t2413660594 ** get_address_of_value_24() { return &___value_24; }
	inline void set_value_24(FsmVar_t2413660594 * value)
	{
		___value_24 = value;
		Il2CppCodeGenWriteBarrier((&___value_24), value);
	}

	inline static int32_t get_offset_of_everyFrame_25() { return static_cast<int32_t>(offsetof(SetFsmArrayItem_t2346556339, ___everyFrame_25)); }
	inline bool get_everyFrame_25() const { return ___everyFrame_25; }
	inline bool* get_address_of_everyFrame_25() { return &___everyFrame_25; }
	inline void set_everyFrame_25(bool value)
	{
		___everyFrame_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMARRAYITEM_T2346556339_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (SetAnimatorTarget_t2874597898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3100[5] = 
{
	SetAnimatorTarget_t2874597898::get_offset_of_gameObject_14(),
	SetAnimatorTarget_t2874597898::get_offset_of_avatarTarget_15(),
	SetAnimatorTarget_t2874597898::get_offset_of_targetNormalizedTime_16(),
	SetAnimatorTarget_t2874597898::get_offset_of_everyFrame_17(),
	SetAnimatorTarget_t2874597898::get_offset_of__animator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (SetAnimatorTrigger_t3267197606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[3] = 
{
	SetAnimatorTrigger_t3267197606::get_offset_of_gameObject_14(),
	SetAnimatorTrigger_t3267197606::get_offset_of_trigger_15(),
	SetAnimatorTrigger_t3267197606::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (ApplicationQuit_t2436658396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (ApplicationRunInBackground_t2120927555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[1] = 
{
	ApplicationRunInBackground_t2120927555::get_offset_of_runInBackground_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (GetScreenHeight_t3875108194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[1] = 
{
	GetScreenHeight_t3875108194::get_offset_of_storeScreenHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (GetScreenWidth_t1171329459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[1] = 
{
	GetScreenWidth_t1171329459::get_offset_of_storeScreenWidth_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (TakeScreenshot_t2218975985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[7] = 
{
	TakeScreenshot_t2218975985::get_offset_of_destination_14(),
	TakeScreenshot_t2218975985::get_offset_of_customPath_15(),
	TakeScreenshot_t2218975985::get_offset_of_filename_16(),
	TakeScreenshot_t2218975985::get_offset_of_autoNumber_17(),
	TakeScreenshot_t2218975985::get_offset_of_superSize_18(),
	TakeScreenshot_t2218975985::get_offset_of_debugLog_19(),
	TakeScreenshot_t2218975985::get_offset_of_screenshotCount_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (Destination_t3397433683)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3107[4] = 
{
	Destination_t3397433683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (ArrayAdd_t3533783165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[2] = 
{
	ArrayAdd_t3533783165::get_offset_of_array_14(),
	ArrayAdd_t3533783165::get_offset_of_value_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (ArrayAddRange_t2097782657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[2] = 
{
	ArrayAddRange_t2097782657::get_offset_of_array_14(),
	ArrayAddRange_t2097782657::get_offset_of_variables_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (ArrayClear_t1592763762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[2] = 
{
	ArrayClear_t1592763762::get_offset_of_array_14(),
	ArrayClear_t1592763762::get_offset_of_resetValue_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (ArrayCompare_t1926373437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[6] = 
{
	ArrayCompare_t1926373437::get_offset_of_array1_14(),
	ArrayCompare_t1926373437::get_offset_of_array2_15(),
	ArrayCompare_t1926373437::get_offset_of_SequenceEqual_16(),
	ArrayCompare_t1926373437::get_offset_of_SequenceNotEqual_17(),
	ArrayCompare_t1926373437::get_offset_of_storeResult_18(),
	ArrayCompare_t1926373437::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (ArrayContains_t1590434515), -1, sizeof(ArrayContains_t1590434515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3112[7] = 
{
	ArrayContains_t1590434515::get_offset_of_array_14(),
	ArrayContains_t1590434515::get_offset_of_value_15(),
	ArrayContains_t1590434515::get_offset_of_index_16(),
	ArrayContains_t1590434515::get_offset_of_isContained_17(),
	ArrayContains_t1590434515::get_offset_of_isContainedEvent_18(),
	ArrayContains_t1590434515::get_offset_of_isNotContainedEvent_19(),
	ArrayContains_t1590434515_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (ArrayDeleteAt_t1998213995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3113[3] = 
{
	ArrayDeleteAt_t1998213995::get_offset_of_array_14(),
	ArrayDeleteAt_t1998213995::get_offset_of_index_15(),
	ArrayDeleteAt_t1998213995::get_offset_of_indexOutOfRangeEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (ArrayForEach_t4198384709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[5] = 
{
	ArrayForEach_t4198384709::get_offset_of_array_15(),
	ArrayForEach_t4198384709::get_offset_of_storeItem_16(),
	ArrayForEach_t4198384709::get_offset_of_fsmTemplateControl_17(),
	ArrayForEach_t4198384709::get_offset_of_finishEvent_18(),
	ArrayForEach_t4198384709::get_offset_of_currentIndex_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (ArrayGet_t3917168766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[5] = 
{
	ArrayGet_t3917168766::get_offset_of_array_14(),
	ArrayGet_t3917168766::get_offset_of_index_15(),
	ArrayGet_t3917168766::get_offset_of_storeValue_16(),
	ArrayGet_t3917168766::get_offset_of_everyFrame_17(),
	ArrayGet_t3917168766::get_offset_of_indexOutOfRange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (ArrayGetNext_t3616358887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[9] = 
{
	ArrayGetNext_t3616358887::get_offset_of_array_14(),
	ArrayGetNext_t3616358887::get_offset_of_startIndex_15(),
	ArrayGetNext_t3616358887::get_offset_of_endIndex_16(),
	ArrayGetNext_t3616358887::get_offset_of_loopEvent_17(),
	ArrayGetNext_t3616358887::get_offset_of_resetFlag_18(),
	ArrayGetNext_t3616358887::get_offset_of_finishedEvent_19(),
	ArrayGetNext_t3616358887::get_offset_of_result_20(),
	ArrayGetNext_t3616358887::get_offset_of_currentIndex_21(),
	ArrayGetNext_t3616358887::get_offset_of_nextItemIndex_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (ArrayGetRandom_t3984396624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[7] = 
{
	ArrayGetRandom_t3984396624::get_offset_of_array_14(),
	ArrayGetRandom_t3984396624::get_offset_of_storeValue_15(),
	ArrayGetRandom_t3984396624::get_offset_of_index_16(),
	ArrayGetRandom_t3984396624::get_offset_of_noRepeat_17(),
	ArrayGetRandom_t3984396624::get_offset_of_everyFrame_18(),
	ArrayGetRandom_t3984396624::get_offset_of_randomIndex_19(),
	ArrayGetRandom_t3984396624::get_offset_of_lastIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (ArrayLength_t1798162410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[3] = 
{
	ArrayLength_t1798162410::get_offset_of_array_14(),
	ArrayLength_t1798162410::get_offset_of_length_15(),
	ArrayLength_t1798162410::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (ArrayResize_t2334538209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[3] = 
{
	ArrayResize_t2334538209::get_offset_of_array_14(),
	ArrayResize_t2334538209::get_offset_of_newSize_15(),
	ArrayResize_t2334538209::get_offset_of_sizeOutOfRangeEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (ArrayReverse_t2263986487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[1] = 
{
	ArrayReverse_t2263986487::get_offset_of_array_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (ArraySet_t93798526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[5] = 
{
	ArraySet_t93798526::get_offset_of_array_14(),
	ArraySet_t93798526::get_offset_of_index_15(),
	ArraySet_t93798526::get_offset_of_value_16(),
	ArraySet_t93798526::get_offset_of_everyFrame_17(),
	ArraySet_t93798526::get_offset_of_indexOutOfRange_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (ArrayShuffle_t887312886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[3] = 
{
	ArrayShuffle_t887312886::get_offset_of_array_14(),
	ArrayShuffle_t887312886::get_offset_of_startIndex_15(),
	ArrayShuffle_t887312886::get_offset_of_shufflingRange_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (ArraySort_t2823797142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[1] = 
{
	ArraySort_t2823797142::get_offset_of_array_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (ArrayTransferValue_t4010721501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[6] = 
{
	ArrayTransferValue_t4010721501::get_offset_of_arraySource_14(),
	ArrayTransferValue_t4010721501::get_offset_of_arrayTarget_15(),
	ArrayTransferValue_t4010721501::get_offset_of_indexToTransfer_16(),
	ArrayTransferValue_t4010721501::get_offset_of_copyType_17(),
	ArrayTransferValue_t4010721501::get_offset_of_pasteType_18(),
	ArrayTransferValue_t4010721501::get_offset_of_indexOutOfRange_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (ArrayTransferType_t2458748193)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3125[4] = 
{
	ArrayTransferType_t2458748193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (ArrayPasteType_t1937951914)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3126[5] = 
{
	ArrayPasteType_t1937951914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (FsmArraySet_t3520048074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[7] = 
{
	FsmArraySet_t3520048074::get_offset_of_gameObject_14(),
	FsmArraySet_t3520048074::get_offset_of_fsmName_15(),
	FsmArraySet_t3520048074::get_offset_of_variableName_16(),
	FsmArraySet_t3520048074::get_offset_of_setValue_17(),
	FsmArraySet_t3520048074::get_offset_of_everyFrame_18(),
	FsmArraySet_t3520048074::get_offset_of_goLastFrame_19(),
	FsmArraySet_t3520048074::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (GetFsmArray_t1165031280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[5] = 
{
	GetFsmArray_t1165031280::get_offset_of_gameObject_19(),
	GetFsmArray_t1165031280::get_offset_of_fsmName_20(),
	GetFsmArray_t1165031280::get_offset_of_variableName_21(),
	GetFsmArray_t1165031280::get_offset_of_storeValue_22(),
	GetFsmArray_t1165031280::get_offset_of_copyValues_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (GetFsmArrayItem_t1269604847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[6] = 
{
	GetFsmArrayItem_t1269604847::get_offset_of_gameObject_20(),
	GetFsmArrayItem_t1269604847::get_offset_of_fsmName_21(),
	GetFsmArrayItem_t1269604847::get_offset_of_variableName_22(),
	GetFsmArrayItem_t1269604847::get_offset_of_index_23(),
	GetFsmArrayItem_t1269604847::get_offset_of_storeValue_24(),
	GetFsmArrayItem_t1269604847::get_offset_of_everyFrame_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (SetFsmArray_t364677932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[5] = 
{
	SetFsmArray_t364677932::get_offset_of_gameObject_19(),
	SetFsmArray_t364677932::get_offset_of_fsmName_20(),
	SetFsmArray_t364677932::get_offset_of_variableName_21(),
	SetFsmArray_t364677932::get_offset_of_setValue_22(),
	SetFsmArray_t364677932::get_offset_of_copyValues_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (SetFsmArrayItem_t2346556339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[6] = 
{
	SetFsmArrayItem_t2346556339::get_offset_of_gameObject_20(),
	SetFsmArrayItem_t2346556339::get_offset_of_fsmName_21(),
	SetFsmArrayItem_t2346556339::get_offset_of_variableName_22(),
	SetFsmArrayItem_t2346556339::get_offset_of_index_23(),
	SetFsmArrayItem_t2346556339::get_offset_of_value_24(),
	SetFsmArrayItem_t2346556339::get_offset_of_everyFrame_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (AudioMute_t195113627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[2] = 
{
	AudioMute_t195113627::get_offset_of_gameObject_14(),
	AudioMute_t195113627::get_offset_of_mute_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (AudioPause_t1465946913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[1] = 
{
	AudioPause_t1465946913::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (AudioPlay_t3994223902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[6] = 
{
	AudioPlay_t3994223902::get_offset_of_gameObject_14(),
	AudioPlay_t3994223902::get_offset_of_volume_15(),
	AudioPlay_t3994223902::get_offset_of_oneShotClip_16(),
	AudioPlay_t3994223902::get_offset_of_WaitForEndOfClip_17(),
	AudioPlay_t3994223902::get_offset_of_finishedEvent_18(),
	AudioPlay_t3994223902::get_offset_of_audio_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (AudioStop_t1488336195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[1] = 
{
	AudioStop_t1488336195::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (PlayRandomSound_t3770793793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[8] = 
{
	PlayRandomSound_t3770793793::get_offset_of_gameObject_14(),
	PlayRandomSound_t3770793793::get_offset_of_position_15(),
	PlayRandomSound_t3770793793::get_offset_of_audioClips_16(),
	PlayRandomSound_t3770793793::get_offset_of_weights_17(),
	PlayRandomSound_t3770793793::get_offset_of_volume_18(),
	PlayRandomSound_t3770793793::get_offset_of_noRepeat_19(),
	PlayRandomSound_t3770793793::get_offset_of_randomIndex_20(),
	PlayRandomSound_t3770793793::get_offset_of_lastIndex_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (PlaySound_t1299560039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[4] = 
{
	PlaySound_t1299560039::get_offset_of_gameObject_14(),
	PlaySound_t1299560039::get_offset_of_position_15(),
	PlaySound_t1299560039::get_offset_of_clip_16(),
	PlaySound_t1299560039::get_offset_of_volume_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (SetAudioClip_t1573938335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[2] = 
{
	SetAudioClip_t1573938335::get_offset_of_gameObject_16(),
	SetAudioClip_t1573938335::get_offset_of_audioClip_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (SetAudioLoop_t3865536326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[2] = 
{
	SetAudioLoop_t3865536326::get_offset_of_gameObject_16(),
	SetAudioLoop_t3865536326::get_offset_of_loop_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (SetAudioPitch_t4104971792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[3] = 
{
	SetAudioPitch_t4104971792::get_offset_of_gameObject_16(),
	SetAudioPitch_t4104971792::get_offset_of_pitch_17(),
	SetAudioPitch_t4104971792::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (SetAudioVolume_t2383236921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[3] = 
{
	SetAudioVolume_t2383236921::get_offset_of_gameObject_16(),
	SetAudioVolume_t2383236921::get_offset_of_volume_17(),
	SetAudioVolume_t2383236921::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (SetGameVolume_t3279791200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[2] = 
{
	SetGameVolume_t3279791200::get_offset_of_volume_14(),
	SetGameVolume_t3279791200::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (BaseUpdateAction_t497653765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[2] = 
{
	BaseUpdateAction_t497653765::get_offset_of_everyFrame_14(),
	BaseUpdateAction_t497653765::get_offset_of_updateType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (UpdateType_t4176746407)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3144[4] = 
{
	UpdateType_t4176746407::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (CameraFadeIn_t119214444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[7] = 
{
	CameraFadeIn_t119214444::get_offset_of_color_14(),
	CameraFadeIn_t119214444::get_offset_of_time_15(),
	CameraFadeIn_t119214444::get_offset_of_finishEvent_16(),
	CameraFadeIn_t119214444::get_offset_of_realTime_17(),
	CameraFadeIn_t119214444::get_offset_of_startTime_18(),
	CameraFadeIn_t119214444::get_offset_of_currentTime_19(),
	CameraFadeIn_t119214444::get_offset_of_colorLerp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (CameraFadeOut_t3832293099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[7] = 
{
	CameraFadeOut_t3832293099::get_offset_of_color_14(),
	CameraFadeOut_t3832293099::get_offset_of_time_15(),
	CameraFadeOut_t3832293099::get_offset_of_finishEvent_16(),
	CameraFadeOut_t3832293099::get_offset_of_realTime_17(),
	CameraFadeOut_t3832293099::get_offset_of_startTime_18(),
	CameraFadeOut_t3832293099::get_offset_of_currentTime_19(),
	CameraFadeOut_t3832293099::get_offset_of_colorLerp_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (CutToCamera_t3140260074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[4] = 
{
	CutToCamera_t3140260074::get_offset_of_camera_14(),
	CutToCamera_t3140260074::get_offset_of_makeMainCamera_15(),
	CutToCamera_t3140260074::get_offset_of_cutBackOnExit_16(),
	CutToCamera_t3140260074::get_offset_of_oldCamera_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (GetMainCamera_t1071044421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[1] = 
{
	GetMainCamera_t1071044421::get_offset_of_storeGameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (ScreenToWorldPoint_t1514121105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[10] = 
{
	ScreenToWorldPoint_t1514121105::get_offset_of_screenVector_14(),
	ScreenToWorldPoint_t1514121105::get_offset_of_screenX_15(),
	ScreenToWorldPoint_t1514121105::get_offset_of_screenY_16(),
	ScreenToWorldPoint_t1514121105::get_offset_of_screenZ_17(),
	ScreenToWorldPoint_t1514121105::get_offset_of_normalized_18(),
	ScreenToWorldPoint_t1514121105::get_offset_of_storeWorldVector_19(),
	ScreenToWorldPoint_t1514121105::get_offset_of_storeWorldX_20(),
	ScreenToWorldPoint_t1514121105::get_offset_of_storeWorldY_21(),
	ScreenToWorldPoint_t1514121105::get_offset_of_storeWorldZ_22(),
	ScreenToWorldPoint_t1514121105::get_offset_of_everyFrame_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (SetBackgroundColor_t4243691100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[3] = 
{
	SetBackgroundColor_t4243691100::get_offset_of_gameObject_16(),
	SetBackgroundColor_t4243691100::get_offset_of_backgroundColor_17(),
	SetBackgroundColor_t4243691100::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (SetCameraCullingMask_t2305940360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[4] = 
{
	SetCameraCullingMask_t2305940360::get_offset_of_gameObject_16(),
	SetCameraCullingMask_t2305940360::get_offset_of_cullingMask_17(),
	SetCameraCullingMask_t2305940360::get_offset_of_invertMask_18(),
	SetCameraCullingMask_t2305940360::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (SetCameraFOV_t2938174343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[3] = 
{
	SetCameraFOV_t2938174343::get_offset_of_gameObject_16(),
	SetCameraFOV_t2938174343::get_offset_of_fieldOfView_17(),
	SetCameraFOV_t2938174343::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (SetMainCamera_t3201888481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[1] = 
{
	SetMainCamera_t3201888481::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (WorldToScreenPoint_t3427403956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[9] = 
{
	WorldToScreenPoint_t3427403956::get_offset_of_worldPosition_14(),
	WorldToScreenPoint_t3427403956::get_offset_of_worldX_15(),
	WorldToScreenPoint_t3427403956::get_offset_of_worldY_16(),
	WorldToScreenPoint_t3427403956::get_offset_of_worldZ_17(),
	WorldToScreenPoint_t3427403956::get_offset_of_storeScreenPoint_18(),
	WorldToScreenPoint_t3427403956::get_offset_of_storeScreenX_19(),
	WorldToScreenPoint_t3427403956::get_offset_of_storeScreenY_20(),
	WorldToScreenPoint_t3427403956::get_offset_of_normalize_21(),
	WorldToScreenPoint_t3427403956::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (ControllerIsGrounded_t987874773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[7] = 
{
	ControllerIsGrounded_t987874773::get_offset_of_gameObject_14(),
	ControllerIsGrounded_t987874773::get_offset_of_trueEvent_15(),
	ControllerIsGrounded_t987874773::get_offset_of_falseEvent_16(),
	ControllerIsGrounded_t987874773::get_offset_of_storeResult_17(),
	ControllerIsGrounded_t987874773::get_offset_of_everyFrame_18(),
	ControllerIsGrounded_t987874773::get_offset_of_previousGo_19(),
	ControllerIsGrounded_t987874773::get_offset_of_controller_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (ControllerMove_t2604891426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[6] = 
{
	ControllerMove_t2604891426::get_offset_of_gameObject_14(),
	ControllerMove_t2604891426::get_offset_of_moveVector_15(),
	ControllerMove_t2604891426::get_offset_of_space_16(),
	ControllerMove_t2604891426::get_offset_of_perSecond_17(),
	ControllerMove_t2604891426::get_offset_of_previousGo_18(),
	ControllerMove_t2604891426::get_offset_of_controller_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (ControllerSettings_t305849206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[10] = 
{
	ControllerSettings_t305849206::get_offset_of_gameObject_14(),
	ControllerSettings_t305849206::get_offset_of_height_15(),
	ControllerSettings_t305849206::get_offset_of_radius_16(),
	ControllerSettings_t305849206::get_offset_of_slopeLimit_17(),
	ControllerSettings_t305849206::get_offset_of_stepOffset_18(),
	ControllerSettings_t305849206::get_offset_of_center_19(),
	ControllerSettings_t305849206::get_offset_of_detectCollisions_20(),
	ControllerSettings_t305849206::get_offset_of_everyFrame_21(),
	ControllerSettings_t305849206::get_offset_of_previousGo_22(),
	ControllerSettings_t305849206::get_offset_of_controller_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (ControllerSimpleMove_t3829253614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[6] = 
{
	ControllerSimpleMove_t3829253614::get_offset_of_gameObject_14(),
	ControllerSimpleMove_t3829253614::get_offset_of_moveVector_15(),
	ControllerSimpleMove_t3829253614::get_offset_of_speed_16(),
	ControllerSimpleMove_t3829253614::get_offset_of_space_17(),
	ControllerSimpleMove_t3829253614::get_offset_of_previousGo_18(),
	ControllerSimpleMove_t3829253614::get_offset_of_controller_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (GetControllerCollisionFlags_t2967667631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[8] = 
{
	GetControllerCollisionFlags_t2967667631::get_offset_of_gameObject_14(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_isGrounded_15(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_none_16(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_sides_17(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_above_18(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_below_19(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_previousGo_20(),
	GetControllerCollisionFlags_t2967667631::get_offset_of_controller_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (GetControllerHitInfo_t3436369072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[6] = 
{
	GetControllerHitInfo_t3436369072::get_offset_of_gameObjectHit_14(),
	GetControllerHitInfo_t3436369072::get_offset_of_contactPoint_15(),
	GetControllerHitInfo_t3436369072::get_offset_of_contactNormal_16(),
	GetControllerHitInfo_t3436369072::get_offset_of_moveDirection_17(),
	GetControllerHitInfo_t3436369072::get_offset_of_moveLength_18(),
	GetControllerHitInfo_t3436369072::get_offset_of_physicsMaterialName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (ColorInterpolate_t701671268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[7] = 
{
	ColorInterpolate_t701671268::get_offset_of_colors_14(),
	ColorInterpolate_t701671268::get_offset_of_time_15(),
	ColorInterpolate_t701671268::get_offset_of_storeColor_16(),
	ColorInterpolate_t701671268::get_offset_of_finishEvent_17(),
	ColorInterpolate_t701671268::get_offset_of_realTime_18(),
	ColorInterpolate_t701671268::get_offset_of_startTime_19(),
	ColorInterpolate_t701671268::get_offset_of_currentTime_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (ColorRamp_t3017487916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[4] = 
{
	ColorRamp_t3017487916::get_offset_of_colors_14(),
	ColorRamp_t3017487916::get_offset_of_sampleAt_15(),
	ColorRamp_t3017487916::get_offset_of_storeColor_16(),
	ColorRamp_t3017487916::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (GetColorRGBA_t3455744901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[6] = 
{
	GetColorRGBA_t3455744901::get_offset_of_color_14(),
	GetColorRGBA_t3455744901::get_offset_of_storeRed_15(),
	GetColorRGBA_t3455744901::get_offset_of_storeGreen_16(),
	GetColorRGBA_t3455744901::get_offset_of_storeBlue_17(),
	GetColorRGBA_t3455744901::get_offset_of_storeAlpha_18(),
	GetColorRGBA_t3455744901::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (SelectRandomColor_t3414736926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[3] = 
{
	SelectRandomColor_t3414736926::get_offset_of_colors_14(),
	SelectRandomColor_t3414736926::get_offset_of_weights_15(),
	SelectRandomColor_t3414736926::get_offset_of_storeColor_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (SetColorRGBA_t569523049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[6] = 
{
	SetColorRGBA_t569523049::get_offset_of_colorVariable_14(),
	SetColorRGBA_t569523049::get_offset_of_red_15(),
	SetColorRGBA_t569523049::get_offset_of_green_16(),
	SetColorRGBA_t569523049::get_offset_of_blue_17(),
	SetColorRGBA_t569523049::get_offset_of_alpha_18(),
	SetColorRGBA_t569523049::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (SetColorValue_t1163264102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[3] = 
{
	SetColorValue_t1163264102::get_offset_of_colorVariable_14(),
	SetColorValue_t1163264102::get_offset_of_color_15(),
	SetColorValue_t1163264102::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3167[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (ConvertBoolToColor_t4204121452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[5] = 
{
	ConvertBoolToColor_t4204121452::get_offset_of_boolVariable_14(),
	ConvertBoolToColor_t4204121452::get_offset_of_colorVariable_15(),
	ConvertBoolToColor_t4204121452::get_offset_of_falseColor_16(),
	ConvertBoolToColor_t4204121452::get_offset_of_trueColor_17(),
	ConvertBoolToColor_t4204121452::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (ConvertBoolToFloat_t2875111075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3169[5] = 
{
	ConvertBoolToFloat_t2875111075::get_offset_of_boolVariable_14(),
	ConvertBoolToFloat_t2875111075::get_offset_of_floatVariable_15(),
	ConvertBoolToFloat_t2875111075::get_offset_of_falseValue_16(),
	ConvertBoolToFloat_t2875111075::get_offset_of_trueValue_17(),
	ConvertBoolToFloat_t2875111075::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (ConvertBoolToInt_t441841880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[5] = 
{
	ConvertBoolToInt_t441841880::get_offset_of_boolVariable_14(),
	ConvertBoolToInt_t441841880::get_offset_of_intVariable_15(),
	ConvertBoolToInt_t441841880::get_offset_of_falseValue_16(),
	ConvertBoolToInt_t441841880::get_offset_of_trueValue_17(),
	ConvertBoolToInt_t441841880::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (ConvertBoolToString_t4106080164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[5] = 
{
	ConvertBoolToString_t4106080164::get_offset_of_boolVariable_14(),
	ConvertBoolToString_t4106080164::get_offset_of_stringVariable_15(),
	ConvertBoolToString_t4106080164::get_offset_of_falseString_16(),
	ConvertBoolToString_t4106080164::get_offset_of_trueString_17(),
	ConvertBoolToString_t4106080164::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (ConvertEnumToString_t3588283646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[3] = 
{
	ConvertEnumToString_t3588283646::get_offset_of_enumVariable_14(),
	ConvertEnumToString_t3588283646::get_offset_of_stringVariable_15(),
	ConvertEnumToString_t3588283646::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (ConvertFloatToInt_t1882659568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[4] = 
{
	ConvertFloatToInt_t1882659568::get_offset_of_floatVariable_14(),
	ConvertFloatToInt_t1882659568::get_offset_of_intVariable_15(),
	ConvertFloatToInt_t1882659568::get_offset_of_rounding_16(),
	ConvertFloatToInt_t1882659568::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (FloatRounding_t1677084411)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3174[4] = 
{
	FloatRounding_t1677084411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (ConvertFloatToString_t2375739146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[4] = 
{
	ConvertFloatToString_t2375739146::get_offset_of_floatVariable_14(),
	ConvertFloatToString_t2375739146::get_offset_of_stringVariable_15(),
	ConvertFloatToString_t2375739146::get_offset_of_format_16(),
	ConvertFloatToString_t2375739146::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (ConvertIntToFloat_t2045532169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[3] = 
{
	ConvertIntToFloat_t2045532169::get_offset_of_intVariable_14(),
	ConvertIntToFloat_t2045532169::get_offset_of_floatVariable_15(),
	ConvertIntToFloat_t2045532169::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (ConvertIntToString_t2050799787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[4] = 
{
	ConvertIntToString_t2050799787::get_offset_of_intVariable_14(),
	ConvertIntToString_t2050799787::get_offset_of_stringVariable_15(),
	ConvertIntToString_t2050799787::get_offset_of_format_16(),
	ConvertIntToString_t2050799787::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (ConvertMaterialToObject_t3938955979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[3] = 
{
	ConvertMaterialToObject_t3938955979::get_offset_of_materialVariable_14(),
	ConvertMaterialToObject_t3938955979::get_offset_of_objectVariable_15(),
	ConvertMaterialToObject_t3938955979::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (ConvertSecondsToString_t1664221685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[4] = 
{
	ConvertSecondsToString_t1664221685::get_offset_of_secondsVariable_14(),
	ConvertSecondsToString_t1664221685::get_offset_of_stringVariable_15(),
	ConvertSecondsToString_t1664221685::get_offset_of_format_16(),
	ConvertSecondsToString_t1664221685::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (ConvertStringToInt_t4270893755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[3] = 
{
	ConvertStringToInt_t4270893755::get_offset_of_stringVariable_14(),
	ConvertStringToInt_t4270893755::get_offset_of_intVariable_15(),
	ConvertStringToInt_t4270893755::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (Assert_t3430802513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[6] = 
{
	Assert_t3430802513::get_offset_of_U3CAstU3Ek__BackingField_14(),
	Assert_t3430802513::get_offset_of_U3CLastErrorMessageU3Ek__BackingField_15(),
	Assert_t3430802513::get_offset_of_expression_16(),
	Assert_t3430802513::get_offset_of_assert_17(),
	Assert_t3430802513::get_offset_of_everyFrame_18(),
	Assert_t3430802513::get_offset_of_cachedExpression_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (AssertType_t117737542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3182[3] = 
{
	AssertType_t117737542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (BaseLogAction_t823318833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[1] = 
{
	BaseLogAction_t823318833::get_offset_of_sendToUnityLog_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (Comment_t3901720946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[1] = 
{
	Comment_t3901720946::get_offset_of_comment_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (DebugBool_t4160504148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3185[2] = 
{
	DebugBool_t4160504148::get_offset_of_logLevel_15(),
	DebugBool_t4160504148::get_offset_of_boolVariable_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (DebugDrawShape_t1342584760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[5] = 
{
	DebugDrawShape_t1342584760::get_offset_of_gameObject_14(),
	DebugDrawShape_t1342584760::get_offset_of_shape_15(),
	DebugDrawShape_t1342584760::get_offset_of_color_16(),
	DebugDrawShape_t1342584760::get_offset_of_radius_17(),
	DebugDrawShape_t1342584760::get_offset_of_size_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (ShapeType_t1684390997)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3187[5] = 
{
	ShapeType_t1684390997::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (DebugEnum_t1656796654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[2] = 
{
	DebugEnum_t1656796654::get_offset_of_logLevel_15(),
	DebugEnum_t1656796654::get_offset_of_enumVariable_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (DebugFloat_t854975360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[2] = 
{
	DebugFloat_t854975360::get_offset_of_logLevel_15(),
	DebugFloat_t854975360::get_offset_of_floatVariable_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (DebugFsmVariable_t3344673650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[2] = 
{
	DebugFsmVariable_t3344673650::get_offset_of_logLevel_15(),
	DebugFsmVariable_t3344673650::get_offset_of_variable_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (DebugGameObject_t1715091115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[2] = 
{
	DebugGameObject_t1715091115::get_offset_of_logLevel_15(),
	DebugGameObject_t1715091115::get_offset_of_gameObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (DebugInt_t2483236234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3192[2] = 
{
	DebugInt_t2483236234::get_offset_of_logLevel_15(),
	DebugInt_t2483236234::get_offset_of_intVariable_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (DebugLog_t910110091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3193[2] = 
{
	DebugLog_t910110091::get_offset_of_logLevel_15(),
	DebugLog_t910110091::get_offset_of_text_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (DebugObject_t4048031855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[2] = 
{
	DebugObject_t4048031855::get_offset_of_logLevel_15(),
	DebugObject_t4048031855::get_offset_of_fsmObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (DebugVector3_t1875006145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[2] = 
{
	DebugVector3_t1875006145::get_offset_of_logLevel_15(),
	DebugVector3_t1875006145::get_offset_of_vector3Variable_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (DrawDebugLine_t21833544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[5] = 
{
	DrawDebugLine_t21833544::get_offset_of_fromObject_14(),
	DrawDebugLine_t21833544::get_offset_of_fromPosition_15(),
	DrawDebugLine_t21833544::get_offset_of_toObject_16(),
	DrawDebugLine_t21833544::get_offset_of_toPosition_17(),
	DrawDebugLine_t21833544::get_offset_of_color_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (DrawDebugRay_t1994944557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3197[4] = 
{
	DrawDebugRay_t1994944557::get_offset_of_fromObject_14(),
	DrawDebugRay_t1994944557::get_offset_of_fromPosition_15(),
	DrawDebugRay_t1994944557::get_offset_of_direction_16(),
	DrawDebugRay_t1994944557::get_offset_of_color_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (DrawStateLabel_t3230669138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[1] = 
{
	DrawStateLabel_t3230669138::get_offset_of_showLabel_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (DeviceOrientationEvent_t3091729835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[3] = 
{
	DeviceOrientationEvent_t3091729835::get_offset_of_orientation_14(),
	DeviceOrientationEvent_t3091729835::get_offset_of_sendEvent_15(),
	DeviceOrientationEvent_t3091729835::get_offset_of_everyFrame_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
