﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation[]
struct CalculationU5BU5D_t3925020946;
// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation[]
struct CalculationU5BU5D_t3587777084;
// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction
struct EasingFunction_t338733390;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t3987626228;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t3432711236;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t4259038327;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t3649179877;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.Animation
struct Animation_t3648466861;
// UnityEngine.AnimationCurve[]
struct AnimationCurveU5BU5D_t4291922187;
// UnityEngine.AnimationState
struct AnimationState_t1108360407;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef CALCULATION_T2075304323_H
#define CALCULATION_T2075304323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation
struct  Calculation_t2075304323 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Calculation_t2075304323, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALCULATION_T2075304323_H
#ifndef CALCULATION_T4245531681_H
#define CALCULATION_T4245531681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation
struct  Calculation_t4245531681 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Calculation_t4245531681, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALCULATION_T4245531681_H
#ifndef EASETYPE_T1028795025_H
#define EASETYPE_T1028795025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType
struct  EaseType_t1028795025 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t1028795025, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T1028795025_H
#ifndef ANIMATORFRAMEUPDATESELECTOR_T3520526877_H
#define ANIMATORFRAMEUPDATESELECTOR_T3520526877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase/AnimatorFrameUpdateSelector
struct  AnimatorFrameUpdateSelector_t3520526877 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase/AnimatorFrameUpdateSelector::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimatorFrameUpdateSelector_t3520526877, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORFRAMEUPDATESELECTOR_T3520526877_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ANIMATIONBLENDMODE_T656007753_H
#define ANIMATIONBLENDMODE_T656007753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationBlendMode
struct  AnimationBlendMode_t656007753 
{
public:
	// System.Int32 UnityEngine.AnimationBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationBlendMode_t656007753, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONBLENDMODE_T656007753_H
#ifndef AVATARIKGOAL_T2138471376_H
#define AVATARIKGOAL_T2138471376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarIKGoal
struct  AvatarIKGoal_t2138471376 
{
public:
	// System.Int32 UnityEngine.AvatarIKGoal::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AvatarIKGoal_t2138471376, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARIKGOAL_T2138471376_H
#ifndef AVATARTARGET_T2782276764_H
#define AVATARTARGET_T2782276764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarTarget
struct  AvatarTarget_t2782276764 
{
public:
	// System.Int32 UnityEngine.AvatarTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AvatarTarget_t2782276764, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARTARGET_T2782276764_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PLAYMODE_T3051407859_H
#define PLAYMODE_T3051407859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayMode
struct  PlayMode_t3051407859 
{
public:
	// System.Int32 UnityEngine.PlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayMode_t3051407859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODE_T3051407859_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef WRAPMODE_T730450702_H
#define WRAPMODE_T730450702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t730450702 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WrapMode_t730450702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T730450702_H
#ifndef ACTIONHELPERS_T2911535836_H
#define ACTIONHELPERS_T2911535836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionHelpers
struct  ActionHelpers_t2911535836  : public RuntimeObject
{
public:

public:
};

struct ActionHelpers_t2911535836_StaticFields
{
public:
	// UnityEngine.RaycastHit HutongGames.PlayMaker.ActionHelpers::mousePickInfo
	RaycastHit_t1056001966  ___mousePickInfo_0;
	// System.Single HutongGames.PlayMaker.ActionHelpers::mousePickRaycastTime
	float ___mousePickRaycastTime_1;
	// System.Single HutongGames.PlayMaker.ActionHelpers::mousePickDistanceUsed
	float ___mousePickDistanceUsed_2;
	// System.Int32 HutongGames.PlayMaker.ActionHelpers::mousePickLayerMaskUsed
	int32_t ___mousePickLayerMaskUsed_3;

public:
	inline static int32_t get_offset_of_mousePickInfo_0() { return static_cast<int32_t>(offsetof(ActionHelpers_t2911535836_StaticFields, ___mousePickInfo_0)); }
	inline RaycastHit_t1056001966  get_mousePickInfo_0() const { return ___mousePickInfo_0; }
	inline RaycastHit_t1056001966 * get_address_of_mousePickInfo_0() { return &___mousePickInfo_0; }
	inline void set_mousePickInfo_0(RaycastHit_t1056001966  value)
	{
		___mousePickInfo_0 = value;
	}

	inline static int32_t get_offset_of_mousePickRaycastTime_1() { return static_cast<int32_t>(offsetof(ActionHelpers_t2911535836_StaticFields, ___mousePickRaycastTime_1)); }
	inline float get_mousePickRaycastTime_1() const { return ___mousePickRaycastTime_1; }
	inline float* get_address_of_mousePickRaycastTime_1() { return &___mousePickRaycastTime_1; }
	inline void set_mousePickRaycastTime_1(float value)
	{
		___mousePickRaycastTime_1 = value;
	}

	inline static int32_t get_offset_of_mousePickDistanceUsed_2() { return static_cast<int32_t>(offsetof(ActionHelpers_t2911535836_StaticFields, ___mousePickDistanceUsed_2)); }
	inline float get_mousePickDistanceUsed_2() const { return ___mousePickDistanceUsed_2; }
	inline float* get_address_of_mousePickDistanceUsed_2() { return &___mousePickDistanceUsed_2; }
	inline void set_mousePickDistanceUsed_2(float value)
	{
		___mousePickDistanceUsed_2 = value;
	}

	inline static int32_t get_offset_of_mousePickLayerMaskUsed_3() { return static_cast<int32_t>(offsetof(ActionHelpers_t2911535836_StaticFields, ___mousePickLayerMaskUsed_3)); }
	inline int32_t get_mousePickLayerMaskUsed_3() const { return ___mousePickLayerMaskUsed_3; }
	inline int32_t* get_address_of_mousePickLayerMaskUsed_3() { return &___mousePickLayerMaskUsed_3; }
	inline void set_mousePickLayerMaskUsed_3(int32_t value)
	{
		___mousePickLayerMaskUsed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONHELPERS_T2911535836_H
#ifndef ADDANIMATIONCLIP_T1597356349_H
#define ADDANIMATIONCLIP_T1597356349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddAnimationClip
struct  AddAnimationClip_t1597356349  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddAnimationClip::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.AddAnimationClip::animationClip
	FsmObject_t2606870197 * ___animationClip_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddAnimationClip::animationName
	FsmString_t1785915204 * ___animationName_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AddAnimationClip::firstFrame
	FsmInt_t874273141 * ___firstFrame_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AddAnimationClip::lastFrame
	FsmInt_t874273141 * ___lastFrame_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddAnimationClip::addLoopFrame
	FsmBool_t163807967 * ___addLoopFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AddAnimationClip_t1597356349, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_animationClip_15() { return static_cast<int32_t>(offsetof(AddAnimationClip_t1597356349, ___animationClip_15)); }
	inline FsmObject_t2606870197 * get_animationClip_15() const { return ___animationClip_15; }
	inline FsmObject_t2606870197 ** get_address_of_animationClip_15() { return &___animationClip_15; }
	inline void set_animationClip_15(FsmObject_t2606870197 * value)
	{
		___animationClip_15 = value;
		Il2CppCodeGenWriteBarrier((&___animationClip_15), value);
	}

	inline static int32_t get_offset_of_animationName_16() { return static_cast<int32_t>(offsetof(AddAnimationClip_t1597356349, ___animationName_16)); }
	inline FsmString_t1785915204 * get_animationName_16() const { return ___animationName_16; }
	inline FsmString_t1785915204 ** get_address_of_animationName_16() { return &___animationName_16; }
	inline void set_animationName_16(FsmString_t1785915204 * value)
	{
		___animationName_16 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_16), value);
	}

	inline static int32_t get_offset_of_firstFrame_17() { return static_cast<int32_t>(offsetof(AddAnimationClip_t1597356349, ___firstFrame_17)); }
	inline FsmInt_t874273141 * get_firstFrame_17() const { return ___firstFrame_17; }
	inline FsmInt_t874273141 ** get_address_of_firstFrame_17() { return &___firstFrame_17; }
	inline void set_firstFrame_17(FsmInt_t874273141 * value)
	{
		___firstFrame_17 = value;
		Il2CppCodeGenWriteBarrier((&___firstFrame_17), value);
	}

	inline static int32_t get_offset_of_lastFrame_18() { return static_cast<int32_t>(offsetof(AddAnimationClip_t1597356349, ___lastFrame_18)); }
	inline FsmInt_t874273141 * get_lastFrame_18() const { return ___lastFrame_18; }
	inline FsmInt_t874273141 ** get_address_of_lastFrame_18() { return &___lastFrame_18; }
	inline void set_lastFrame_18(FsmInt_t874273141 * value)
	{
		___lastFrame_18 = value;
		Il2CppCodeGenWriteBarrier((&___lastFrame_18), value);
	}

	inline static int32_t get_offset_of_addLoopFrame_19() { return static_cast<int32_t>(offsetof(AddAnimationClip_t1597356349, ___addLoopFrame_19)); }
	inline FsmBool_t163807967 * get_addLoopFrame_19() const { return ___addLoopFrame_19; }
	inline FsmBool_t163807967 ** get_address_of_addLoopFrame_19() { return &___addLoopFrame_19; }
	inline void set_addLoopFrame_19(FsmBool_t163807967 * value)
	{
		___addLoopFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___addLoopFrame_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDANIMATIONCLIP_T1597356349_H
#ifndef ANIMATEFLOAT_T1684987476_H
#define ANIMATEFLOAT_T1684987476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateFloat
struct  AnimateFloat_t1684987476  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateFloat::animCurve
	FsmAnimationCurve_t3432711236 * ___animCurve_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFloat::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AnimateFloat::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFloat::realTime
	bool ___realTime_17;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFloat::startTime
	float ___startTime_18;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFloat::currentTime
	float ___currentTime_19;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFloat::endTime
	float ___endTime_20;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFloat::looping
	bool ___looping_21;

public:
	inline static int32_t get_offset_of_animCurve_14() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___animCurve_14)); }
	inline FsmAnimationCurve_t3432711236 * get_animCurve_14() const { return ___animCurve_14; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_animCurve_14() { return &___animCurve_14; }
	inline void set_animCurve_14(FsmAnimationCurve_t3432711236 * value)
	{
		___animCurve_14 = value;
		Il2CppCodeGenWriteBarrier((&___animCurve_14), value);
	}

	inline static int32_t get_offset_of_floatVariable_15() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___floatVariable_15)); }
	inline FsmFloat_t2883254149 * get_floatVariable_15() const { return ___floatVariable_15; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_15() { return &___floatVariable_15; }
	inline void set_floatVariable_15(FsmFloat_t2883254149 * value)
	{
		___floatVariable_15 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_15), value);
	}

	inline static int32_t get_offset_of_finishEvent_16() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___finishEvent_16)); }
	inline FsmEvent_t3736299882 * get_finishEvent_16() const { return ___finishEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_16() { return &___finishEvent_16; }
	inline void set_finishEvent_16(FsmEvent_t3736299882 * value)
	{
		___finishEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_16), value);
	}

	inline static int32_t get_offset_of_realTime_17() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___realTime_17)); }
	inline bool get_realTime_17() const { return ___realTime_17; }
	inline bool* get_address_of_realTime_17() { return &___realTime_17; }
	inline void set_realTime_17(bool value)
	{
		___realTime_17 = value;
	}

	inline static int32_t get_offset_of_startTime_18() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___startTime_18)); }
	inline float get_startTime_18() const { return ___startTime_18; }
	inline float* get_address_of_startTime_18() { return &___startTime_18; }
	inline void set_startTime_18(float value)
	{
		___startTime_18 = value;
	}

	inline static int32_t get_offset_of_currentTime_19() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___currentTime_19)); }
	inline float get_currentTime_19() const { return ___currentTime_19; }
	inline float* get_address_of_currentTime_19() { return &___currentTime_19; }
	inline void set_currentTime_19(float value)
	{
		___currentTime_19 = value;
	}

	inline static int32_t get_offset_of_endTime_20() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___endTime_20)); }
	inline float get_endTime_20() const { return ___endTime_20; }
	inline float* get_address_of_endTime_20() { return &___endTime_20; }
	inline void set_endTime_20(float value)
	{
		___endTime_20 = value;
	}

	inline static int32_t get_offset_of_looping_21() { return static_cast<int32_t>(offsetof(AnimateFloat_t1684987476, ___looping_21)); }
	inline bool get_looping_21() const { return ___looping_21; }
	inline bool* get_address_of_looping_21() { return &___looping_21; }
	inline void set_looping_21(bool value)
	{
		___looping_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEFLOAT_T1684987476_H
#ifndef ANIMATEFSMACTION_T2616519849_H
#define ANIMATEFSMACTION_T2616519849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateFsmAction
struct  AnimateFsmAction_t2616519849  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFsmAction::time
	FsmFloat_t2883254149 * ___time_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFsmAction::speed
	FsmFloat_t2883254149 * ___speed_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFsmAction::delay
	FsmFloat_t2883254149 * ___delay_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AnimateFsmAction::ignoreCurveOffset
	FsmBool_t163807967 * ___ignoreCurveOffset_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AnimateFsmAction::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_18;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFsmAction::realTime
	bool ___realTime_19;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFsmAction::startTime
	float ___startTime_20;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFsmAction::currentTime
	float ___currentTime_21;
	// System.Single[] HutongGames.PlayMaker.Actions.AnimateFsmAction::endTimes
	SingleU5BU5D_t1444911251* ___endTimes_22;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFsmAction::lastTime
	float ___lastTime_23;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFsmAction::deltaTime
	float ___deltaTime_24;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFsmAction::delayTime
	float ___delayTime_25;
	// System.Single[] HutongGames.PlayMaker.Actions.AnimateFsmAction::keyOffsets
	SingleU5BU5D_t1444911251* ___keyOffsets_26;
	// UnityEngine.AnimationCurve[] HutongGames.PlayMaker.Actions.AnimateFsmAction::curves
	AnimationCurveU5BU5D_t4291922187* ___curves_27;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation[] HutongGames.PlayMaker.Actions.AnimateFsmAction::calculations
	CalculationU5BU5D_t3925020946* ___calculations_28;
	// System.Single[] HutongGames.PlayMaker.Actions.AnimateFsmAction::resultFloats
	SingleU5BU5D_t1444911251* ___resultFloats_29;
	// System.Single[] HutongGames.PlayMaker.Actions.AnimateFsmAction::fromFloats
	SingleU5BU5D_t1444911251* ___fromFloats_30;
	// System.Single[] HutongGames.PlayMaker.Actions.AnimateFsmAction::toFloats
	SingleU5BU5D_t1444911251* ___toFloats_31;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFsmAction::finishAction
	bool ___finishAction_32;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFsmAction::isRunning
	bool ___isRunning_33;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFsmAction::looping
	bool ___looping_34;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFsmAction::start
	bool ___start_35;
	// System.Single HutongGames.PlayMaker.Actions.AnimateFsmAction::largestEndTime
	float ___largestEndTime_36;

public:
	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___time_14)); }
	inline FsmFloat_t2883254149 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2883254149 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2883254149 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier((&___time_14), value);
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___speed_15)); }
	inline FsmFloat_t2883254149 * get_speed_15() const { return ___speed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(FsmFloat_t2883254149 * value)
	{
		___speed_15 = value;
		Il2CppCodeGenWriteBarrier((&___speed_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_ignoreCurveOffset_17() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___ignoreCurveOffset_17)); }
	inline FsmBool_t163807967 * get_ignoreCurveOffset_17() const { return ___ignoreCurveOffset_17; }
	inline FsmBool_t163807967 ** get_address_of_ignoreCurveOffset_17() { return &___ignoreCurveOffset_17; }
	inline void set_ignoreCurveOffset_17(FsmBool_t163807967 * value)
	{
		___ignoreCurveOffset_17 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreCurveOffset_17), value);
	}

	inline static int32_t get_offset_of_finishEvent_18() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___finishEvent_18)); }
	inline FsmEvent_t3736299882 * get_finishEvent_18() const { return ___finishEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_18() { return &___finishEvent_18; }
	inline void set_finishEvent_18(FsmEvent_t3736299882 * value)
	{
		___finishEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_18), value);
	}

	inline static int32_t get_offset_of_realTime_19() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___realTime_19)); }
	inline bool get_realTime_19() const { return ___realTime_19; }
	inline bool* get_address_of_realTime_19() { return &___realTime_19; }
	inline void set_realTime_19(bool value)
	{
		___realTime_19 = value;
	}

	inline static int32_t get_offset_of_startTime_20() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___startTime_20)); }
	inline float get_startTime_20() const { return ___startTime_20; }
	inline float* get_address_of_startTime_20() { return &___startTime_20; }
	inline void set_startTime_20(float value)
	{
		___startTime_20 = value;
	}

	inline static int32_t get_offset_of_currentTime_21() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___currentTime_21)); }
	inline float get_currentTime_21() const { return ___currentTime_21; }
	inline float* get_address_of_currentTime_21() { return &___currentTime_21; }
	inline void set_currentTime_21(float value)
	{
		___currentTime_21 = value;
	}

	inline static int32_t get_offset_of_endTimes_22() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___endTimes_22)); }
	inline SingleU5BU5D_t1444911251* get_endTimes_22() const { return ___endTimes_22; }
	inline SingleU5BU5D_t1444911251** get_address_of_endTimes_22() { return &___endTimes_22; }
	inline void set_endTimes_22(SingleU5BU5D_t1444911251* value)
	{
		___endTimes_22 = value;
		Il2CppCodeGenWriteBarrier((&___endTimes_22), value);
	}

	inline static int32_t get_offset_of_lastTime_23() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___lastTime_23)); }
	inline float get_lastTime_23() const { return ___lastTime_23; }
	inline float* get_address_of_lastTime_23() { return &___lastTime_23; }
	inline void set_lastTime_23(float value)
	{
		___lastTime_23 = value;
	}

	inline static int32_t get_offset_of_deltaTime_24() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___deltaTime_24)); }
	inline float get_deltaTime_24() const { return ___deltaTime_24; }
	inline float* get_address_of_deltaTime_24() { return &___deltaTime_24; }
	inline void set_deltaTime_24(float value)
	{
		___deltaTime_24 = value;
	}

	inline static int32_t get_offset_of_delayTime_25() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___delayTime_25)); }
	inline float get_delayTime_25() const { return ___delayTime_25; }
	inline float* get_address_of_delayTime_25() { return &___delayTime_25; }
	inline void set_delayTime_25(float value)
	{
		___delayTime_25 = value;
	}

	inline static int32_t get_offset_of_keyOffsets_26() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___keyOffsets_26)); }
	inline SingleU5BU5D_t1444911251* get_keyOffsets_26() const { return ___keyOffsets_26; }
	inline SingleU5BU5D_t1444911251** get_address_of_keyOffsets_26() { return &___keyOffsets_26; }
	inline void set_keyOffsets_26(SingleU5BU5D_t1444911251* value)
	{
		___keyOffsets_26 = value;
		Il2CppCodeGenWriteBarrier((&___keyOffsets_26), value);
	}

	inline static int32_t get_offset_of_curves_27() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___curves_27)); }
	inline AnimationCurveU5BU5D_t4291922187* get_curves_27() const { return ___curves_27; }
	inline AnimationCurveU5BU5D_t4291922187** get_address_of_curves_27() { return &___curves_27; }
	inline void set_curves_27(AnimationCurveU5BU5D_t4291922187* value)
	{
		___curves_27 = value;
		Il2CppCodeGenWriteBarrier((&___curves_27), value);
	}

	inline static int32_t get_offset_of_calculations_28() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___calculations_28)); }
	inline CalculationU5BU5D_t3925020946* get_calculations_28() const { return ___calculations_28; }
	inline CalculationU5BU5D_t3925020946** get_address_of_calculations_28() { return &___calculations_28; }
	inline void set_calculations_28(CalculationU5BU5D_t3925020946* value)
	{
		___calculations_28 = value;
		Il2CppCodeGenWriteBarrier((&___calculations_28), value);
	}

	inline static int32_t get_offset_of_resultFloats_29() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___resultFloats_29)); }
	inline SingleU5BU5D_t1444911251* get_resultFloats_29() const { return ___resultFloats_29; }
	inline SingleU5BU5D_t1444911251** get_address_of_resultFloats_29() { return &___resultFloats_29; }
	inline void set_resultFloats_29(SingleU5BU5D_t1444911251* value)
	{
		___resultFloats_29 = value;
		Il2CppCodeGenWriteBarrier((&___resultFloats_29), value);
	}

	inline static int32_t get_offset_of_fromFloats_30() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___fromFloats_30)); }
	inline SingleU5BU5D_t1444911251* get_fromFloats_30() const { return ___fromFloats_30; }
	inline SingleU5BU5D_t1444911251** get_address_of_fromFloats_30() { return &___fromFloats_30; }
	inline void set_fromFloats_30(SingleU5BU5D_t1444911251* value)
	{
		___fromFloats_30 = value;
		Il2CppCodeGenWriteBarrier((&___fromFloats_30), value);
	}

	inline static int32_t get_offset_of_toFloats_31() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___toFloats_31)); }
	inline SingleU5BU5D_t1444911251* get_toFloats_31() const { return ___toFloats_31; }
	inline SingleU5BU5D_t1444911251** get_address_of_toFloats_31() { return &___toFloats_31; }
	inline void set_toFloats_31(SingleU5BU5D_t1444911251* value)
	{
		___toFloats_31 = value;
		Il2CppCodeGenWriteBarrier((&___toFloats_31), value);
	}

	inline static int32_t get_offset_of_finishAction_32() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___finishAction_32)); }
	inline bool get_finishAction_32() const { return ___finishAction_32; }
	inline bool* get_address_of_finishAction_32() { return &___finishAction_32; }
	inline void set_finishAction_32(bool value)
	{
		___finishAction_32 = value;
	}

	inline static int32_t get_offset_of_isRunning_33() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___isRunning_33)); }
	inline bool get_isRunning_33() const { return ___isRunning_33; }
	inline bool* get_address_of_isRunning_33() { return &___isRunning_33; }
	inline void set_isRunning_33(bool value)
	{
		___isRunning_33 = value;
	}

	inline static int32_t get_offset_of_looping_34() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___looping_34)); }
	inline bool get_looping_34() const { return ___looping_34; }
	inline bool* get_address_of_looping_34() { return &___looping_34; }
	inline void set_looping_34(bool value)
	{
		___looping_34 = value;
	}

	inline static int32_t get_offset_of_start_35() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___start_35)); }
	inline bool get_start_35() const { return ___start_35; }
	inline bool* get_address_of_start_35() { return &___start_35; }
	inline void set_start_35(bool value)
	{
		___start_35 = value;
	}

	inline static int32_t get_offset_of_largestEndTime_36() { return static_cast<int32_t>(offsetof(AnimateFsmAction_t2616519849, ___largestEndTime_36)); }
	inline float get_largestEndTime_36() const { return ___largestEndTime_36; }
	inline float* get_address_of_largestEndTime_36() { return &___largestEndTime_36; }
	inline void set_largestEndTime_36(float value)
	{
		___largestEndTime_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEFSMACTION_T2616519849_H
#ifndef ANIMATORCROSSFADE_T3536162959_H
#define ANIMATORCROSSFADE_T3536162959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorCrossFade
struct  AnimatorCrossFade_t3536162959  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorCrossFade::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimatorCrossFade::stateName
	FsmString_t1785915204 * ___stateName_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorCrossFade::transitionDuration
	FsmFloat_t2883254149 * ___transitionDuration_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimatorCrossFade::layer
	FsmInt_t874273141 * ___layer_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorCrossFade::normalizedTime
	FsmFloat_t2883254149 * ___normalizedTime_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorCrossFade::_animator
	Animator_t434523843 * ____animator_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3536162959, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_stateName_15() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3536162959, ___stateName_15)); }
	inline FsmString_t1785915204 * get_stateName_15() const { return ___stateName_15; }
	inline FsmString_t1785915204 ** get_address_of_stateName_15() { return &___stateName_15; }
	inline void set_stateName_15(FsmString_t1785915204 * value)
	{
		___stateName_15 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_15), value);
	}

	inline static int32_t get_offset_of_transitionDuration_16() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3536162959, ___transitionDuration_16)); }
	inline FsmFloat_t2883254149 * get_transitionDuration_16() const { return ___transitionDuration_16; }
	inline FsmFloat_t2883254149 ** get_address_of_transitionDuration_16() { return &___transitionDuration_16; }
	inline void set_transitionDuration_16(FsmFloat_t2883254149 * value)
	{
		___transitionDuration_16 = value;
		Il2CppCodeGenWriteBarrier((&___transitionDuration_16), value);
	}

	inline static int32_t get_offset_of_layer_17() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3536162959, ___layer_17)); }
	inline FsmInt_t874273141 * get_layer_17() const { return ___layer_17; }
	inline FsmInt_t874273141 ** get_address_of_layer_17() { return &___layer_17; }
	inline void set_layer_17(FsmInt_t874273141 * value)
	{
		___layer_17 = value;
		Il2CppCodeGenWriteBarrier((&___layer_17), value);
	}

	inline static int32_t get_offset_of_normalizedTime_18() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3536162959, ___normalizedTime_18)); }
	inline FsmFloat_t2883254149 * get_normalizedTime_18() const { return ___normalizedTime_18; }
	inline FsmFloat_t2883254149 ** get_address_of_normalizedTime_18() { return &___normalizedTime_18; }
	inline void set_normalizedTime_18(FsmFloat_t2883254149 * value)
	{
		___normalizedTime_18 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedTime_18), value);
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(AnimatorCrossFade_t3536162959, ____animator_19)); }
	inline Animator_t434523843 * get__animator_19() const { return ____animator_19; }
	inline Animator_t434523843 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t434523843 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier((&____animator_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCROSSFADE_T3536162959_H
#ifndef ANIMATORINTERRUPTMATCHTARGET_T173563957_H
#define ANIMATORINTERRUPTMATCHTARGET_T173563957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget
struct  AnimatorInterruptMatchTarget_t173563957  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AnimatorInterruptMatchTarget::completeMatch
	FsmBool_t163807967 * ___completeMatch_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorInterruptMatchTarget_t173563957, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_completeMatch_15() { return static_cast<int32_t>(offsetof(AnimatorInterruptMatchTarget_t173563957, ___completeMatch_15)); }
	inline FsmBool_t163807967 * get_completeMatch_15() const { return ___completeMatch_15; }
	inline FsmBool_t163807967 ** get_address_of_completeMatch_15() { return &___completeMatch_15; }
	inline void set_completeMatch_15(FsmBool_t163807967 * value)
	{
		___completeMatch_15 = value;
		Il2CppCodeGenWriteBarrier((&___completeMatch_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORINTERRUPTMATCHTARGET_T173563957_H
#ifndef ANIMATORMATCHTARGET_T3919190371_H
#define ANIMATORMATCHTARGET_T3919190371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorMatchTarget
struct  AnimatorMatchTarget_t3919190371  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorMatchTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// UnityEngine.AvatarTarget HutongGames.PlayMaker.Actions.AnimatorMatchTarget::bodyPart
	int32_t ___bodyPart_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.AnimatorMatchTarget::target
	FsmGameObject_t3581898942 * ___target_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AnimatorMatchTarget::targetPosition
	FsmVector3_t626444517 * ___targetPosition_17;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.AnimatorMatchTarget::targetRotation
	FsmQuaternion_t4259038327 * ___targetRotation_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AnimatorMatchTarget::positionWeight
	FsmVector3_t626444517 * ___positionWeight_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorMatchTarget::rotationWeight
	FsmFloat_t2883254149 * ___rotationWeight_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorMatchTarget::startNormalizedTime
	FsmFloat_t2883254149 * ___startNormalizedTime_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorMatchTarget::targetNormalizedTime
	FsmFloat_t2883254149 * ___targetNormalizedTime_22;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimatorMatchTarget::everyFrame
	bool ___everyFrame_23;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorMatchTarget::_animator
	Animator_t434523843 * ____animator_24;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.AnimatorMatchTarget::_transform
	Transform_t3600365921 * ____transform_25;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_bodyPart_15() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___bodyPart_15)); }
	inline int32_t get_bodyPart_15() const { return ___bodyPart_15; }
	inline int32_t* get_address_of_bodyPart_15() { return &___bodyPart_15; }
	inline void set_bodyPart_15(int32_t value)
	{
		___bodyPart_15 = value;
	}

	inline static int32_t get_offset_of_target_16() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___target_16)); }
	inline FsmGameObject_t3581898942 * get_target_16() const { return ___target_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_target_16() { return &___target_16; }
	inline void set_target_16(FsmGameObject_t3581898942 * value)
	{
		___target_16 = value;
		Il2CppCodeGenWriteBarrier((&___target_16), value);
	}

	inline static int32_t get_offset_of_targetPosition_17() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___targetPosition_17)); }
	inline FsmVector3_t626444517 * get_targetPosition_17() const { return ___targetPosition_17; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_17() { return &___targetPosition_17; }
	inline void set_targetPosition_17(FsmVector3_t626444517 * value)
	{
		___targetPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_17), value);
	}

	inline static int32_t get_offset_of_targetRotation_18() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___targetRotation_18)); }
	inline FsmQuaternion_t4259038327 * get_targetRotation_18() const { return ___targetRotation_18; }
	inline FsmQuaternion_t4259038327 ** get_address_of_targetRotation_18() { return &___targetRotation_18; }
	inline void set_targetRotation_18(FsmQuaternion_t4259038327 * value)
	{
		___targetRotation_18 = value;
		Il2CppCodeGenWriteBarrier((&___targetRotation_18), value);
	}

	inline static int32_t get_offset_of_positionWeight_19() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___positionWeight_19)); }
	inline FsmVector3_t626444517 * get_positionWeight_19() const { return ___positionWeight_19; }
	inline FsmVector3_t626444517 ** get_address_of_positionWeight_19() { return &___positionWeight_19; }
	inline void set_positionWeight_19(FsmVector3_t626444517 * value)
	{
		___positionWeight_19 = value;
		Il2CppCodeGenWriteBarrier((&___positionWeight_19), value);
	}

	inline static int32_t get_offset_of_rotationWeight_20() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___rotationWeight_20)); }
	inline FsmFloat_t2883254149 * get_rotationWeight_20() const { return ___rotationWeight_20; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationWeight_20() { return &___rotationWeight_20; }
	inline void set_rotationWeight_20(FsmFloat_t2883254149 * value)
	{
		___rotationWeight_20 = value;
		Il2CppCodeGenWriteBarrier((&___rotationWeight_20), value);
	}

	inline static int32_t get_offset_of_startNormalizedTime_21() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___startNormalizedTime_21)); }
	inline FsmFloat_t2883254149 * get_startNormalizedTime_21() const { return ___startNormalizedTime_21; }
	inline FsmFloat_t2883254149 ** get_address_of_startNormalizedTime_21() { return &___startNormalizedTime_21; }
	inline void set_startNormalizedTime_21(FsmFloat_t2883254149 * value)
	{
		___startNormalizedTime_21 = value;
		Il2CppCodeGenWriteBarrier((&___startNormalizedTime_21), value);
	}

	inline static int32_t get_offset_of_targetNormalizedTime_22() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___targetNormalizedTime_22)); }
	inline FsmFloat_t2883254149 * get_targetNormalizedTime_22() const { return ___targetNormalizedTime_22; }
	inline FsmFloat_t2883254149 ** get_address_of_targetNormalizedTime_22() { return &___targetNormalizedTime_22; }
	inline void set_targetNormalizedTime_22(FsmFloat_t2883254149 * value)
	{
		___targetNormalizedTime_22 = value;
		Il2CppCodeGenWriteBarrier((&___targetNormalizedTime_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}

	inline static int32_t get_offset_of__animator_24() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ____animator_24)); }
	inline Animator_t434523843 * get__animator_24() const { return ____animator_24; }
	inline Animator_t434523843 ** get_address_of__animator_24() { return &____animator_24; }
	inline void set__animator_24(Animator_t434523843 * value)
	{
		____animator_24 = value;
		Il2CppCodeGenWriteBarrier((&____animator_24), value);
	}

	inline static int32_t get_offset_of__transform_25() { return static_cast<int32_t>(offsetof(AnimatorMatchTarget_t3919190371, ____transform_25)); }
	inline Transform_t3600365921 * get__transform_25() const { return ____transform_25; }
	inline Transform_t3600365921 ** get_address_of__transform_25() { return &____transform_25; }
	inline void set__transform_25(Transform_t3600365921 * value)
	{
		____transform_25 = value;
		Il2CppCodeGenWriteBarrier((&____transform_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORMATCHTARGET_T3919190371_H
#ifndef ANIMATORPLAY_T2112008182_H
#define ANIMATORPLAY_T2112008182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorPlay
struct  AnimatorPlay_t2112008182  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorPlay::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimatorPlay::stateName
	FsmString_t1785915204 * ___stateName_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimatorPlay::layer
	FsmInt_t874273141 * ___layer_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorPlay::normalizedTime
	FsmFloat_t2883254149 * ___normalizedTime_17;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimatorPlay::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.AnimatorPlay::_animator
	Animator_t434523843 * ____animator_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorPlay_t2112008182, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_stateName_15() { return static_cast<int32_t>(offsetof(AnimatorPlay_t2112008182, ___stateName_15)); }
	inline FsmString_t1785915204 * get_stateName_15() const { return ___stateName_15; }
	inline FsmString_t1785915204 ** get_address_of_stateName_15() { return &___stateName_15; }
	inline void set_stateName_15(FsmString_t1785915204 * value)
	{
		___stateName_15 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_15), value);
	}

	inline static int32_t get_offset_of_layer_16() { return static_cast<int32_t>(offsetof(AnimatorPlay_t2112008182, ___layer_16)); }
	inline FsmInt_t874273141 * get_layer_16() const { return ___layer_16; }
	inline FsmInt_t874273141 ** get_address_of_layer_16() { return &___layer_16; }
	inline void set_layer_16(FsmInt_t874273141 * value)
	{
		___layer_16 = value;
		Il2CppCodeGenWriteBarrier((&___layer_16), value);
	}

	inline static int32_t get_offset_of_normalizedTime_17() { return static_cast<int32_t>(offsetof(AnimatorPlay_t2112008182, ___normalizedTime_17)); }
	inline FsmFloat_t2883254149 * get_normalizedTime_17() const { return ___normalizedTime_17; }
	inline FsmFloat_t2883254149 ** get_address_of_normalizedTime_17() { return &___normalizedTime_17; }
	inline void set_normalizedTime_17(FsmFloat_t2883254149 * value)
	{
		___normalizedTime_17 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedTime_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(AnimatorPlay_t2112008182, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(AnimatorPlay_t2112008182, ____animator_19)); }
	inline Animator_t434523843 * get__animator_19() const { return ____animator_19; }
	inline Animator_t434523843 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t434523843 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier((&____animator_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORPLAY_T2112008182_H
#ifndef ANIMATORSTARTPLAYBACK_T3399045872_H
#define ANIMATORSTARTPLAYBACK_T3399045872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorStartPlayback
struct  AnimatorStartPlayback_t3399045872  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorStartPlayback::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorStartPlayback_t3399045872, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTARTPLAYBACK_T3399045872_H
#ifndef ANIMATORSTARTRECORDING_T3982064907_H
#define ANIMATORSTARTRECORDING_T3982064907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorStartRecording
struct  AnimatorStartRecording_t3982064907  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorStartRecording::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimatorStartRecording::frameCount
	FsmInt_t874273141 * ___frameCount_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorStartRecording_t3982064907, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_frameCount_15() { return static_cast<int32_t>(offsetof(AnimatorStartRecording_t3982064907, ___frameCount_15)); }
	inline FsmInt_t874273141 * get_frameCount_15() const { return ___frameCount_15; }
	inline FsmInt_t874273141 ** get_address_of_frameCount_15() { return &___frameCount_15; }
	inline void set_frameCount_15(FsmInt_t874273141 * value)
	{
		___frameCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___frameCount_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTARTRECORDING_T3982064907_H
#ifndef ANIMATORSTOPPLAYBACK_T184465721_H
#define ANIMATORSTOPPLAYBACK_T184465721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorStopPlayback
struct  AnimatorStopPlayback_t184465721  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorStopPlayback::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorStopPlayback_t184465721, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTOPPLAYBACK_T184465721_H
#ifndef ANIMATORSTOPRECORDING_T646066457_H
#define ANIMATORSTOPRECORDING_T646066457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimatorStopRecording
struct  AnimatorStopRecording_t646066457  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimatorStopRecording::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorStopRecording::recorderStartTime
	FsmFloat_t2883254149 * ___recorderStartTime_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimatorStopRecording::recorderStopTime
	FsmFloat_t2883254149 * ___recorderStopTime_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AnimatorStopRecording_t646066457, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_recorderStartTime_15() { return static_cast<int32_t>(offsetof(AnimatorStopRecording_t646066457, ___recorderStartTime_15)); }
	inline FsmFloat_t2883254149 * get_recorderStartTime_15() const { return ___recorderStartTime_15; }
	inline FsmFloat_t2883254149 ** get_address_of_recorderStartTime_15() { return &___recorderStartTime_15; }
	inline void set_recorderStartTime_15(FsmFloat_t2883254149 * value)
	{
		___recorderStartTime_15 = value;
		Il2CppCodeGenWriteBarrier((&___recorderStartTime_15), value);
	}

	inline static int32_t get_offset_of_recorderStopTime_16() { return static_cast<int32_t>(offsetof(AnimatorStopRecording_t646066457, ___recorderStopTime_16)); }
	inline FsmFloat_t2883254149 * get_recorderStopTime_16() const { return ___recorderStopTime_16; }
	inline FsmFloat_t2883254149 ** get_address_of_recorderStopTime_16() { return &___recorderStopTime_16; }
	inline void set_recorderStopTime_16(FsmFloat_t2883254149 * value)
	{
		___recorderStopTime_16 = value;
		Il2CppCodeGenWriteBarrier((&___recorderStopTime_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTOPRECORDING_T646066457_H
#ifndef CAPTUREPOSEASANIMATIONCLIP_T1458556555_H
#define CAPTUREPOSEASANIMATIONCLIP_T1458556555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip
struct  CapturePoseAsAnimationClip_t1458556555  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::position
	FsmBool_t163807967 * ___position_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::rotation
	FsmBool_t163807967 * ___rotation_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::scale
	FsmBool_t163807967 * ___scale_17;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::storeAnimationClip
	FsmObject_t2606870197 * ___storeAnimationClip_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1458556555, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1458556555, ___position_15)); }
	inline FsmBool_t163807967 * get_position_15() const { return ___position_15; }
	inline FsmBool_t163807967 ** get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(FsmBool_t163807967 * value)
	{
		___position_15 = value;
		Il2CppCodeGenWriteBarrier((&___position_15), value);
	}

	inline static int32_t get_offset_of_rotation_16() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1458556555, ___rotation_16)); }
	inline FsmBool_t163807967 * get_rotation_16() const { return ___rotation_16; }
	inline FsmBool_t163807967 ** get_address_of_rotation_16() { return &___rotation_16; }
	inline void set_rotation_16(FsmBool_t163807967 * value)
	{
		___rotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_16), value);
	}

	inline static int32_t get_offset_of_scale_17() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1458556555, ___scale_17)); }
	inline FsmBool_t163807967 * get_scale_17() const { return ___scale_17; }
	inline FsmBool_t163807967 ** get_address_of_scale_17() { return &___scale_17; }
	inline void set_scale_17(FsmBool_t163807967 * value)
	{
		___scale_17 = value;
		Il2CppCodeGenWriteBarrier((&___scale_17), value);
	}

	inline static int32_t get_offset_of_storeAnimationClip_18() { return static_cast<int32_t>(offsetof(CapturePoseAsAnimationClip_t1458556555, ___storeAnimationClip_18)); }
	inline FsmObject_t2606870197 * get_storeAnimationClip_18() const { return ___storeAnimationClip_18; }
	inline FsmObject_t2606870197 ** get_address_of_storeAnimationClip_18() { return &___storeAnimationClip_18; }
	inline void set_storeAnimationClip_18(FsmObject_t2606870197 * value)
	{
		___storeAnimationClip_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeAnimationClip_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREPOSEASANIMATIONCLIP_T1458556555_H
#ifndef COMPONENTACTION_1_T2230786787_H
#define COMPONENTACTION_1_T2230786787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Animation>
struct  ComponentAction_1_t2230786787  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Animation_t3648466861 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2230786787, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2230786787, ___cachedComponent_15)); }
	inline Animation_t3648466861 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Animation_t3648466861 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Animation_t3648466861 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2230786787_H
#ifndef CURVEFSMACTION_T928230875_H
#define CURVEFSMACTION_T928230875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveFsmAction
struct  CurveFsmAction_t928230875  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFsmAction::time
	FsmFloat_t2883254149 * ___time_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFsmAction::speed
	FsmFloat_t2883254149 * ___speed_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFsmAction::delay
	FsmFloat_t2883254149 * ___delay_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.CurveFsmAction::ignoreCurveOffset
	FsmBool_t163807967 * ___ignoreCurveOffset_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.CurveFsmAction::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_18;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFsmAction::realTime
	bool ___realTime_19;
	// System.Single HutongGames.PlayMaker.Actions.CurveFsmAction::startTime
	float ___startTime_20;
	// System.Single HutongGames.PlayMaker.Actions.CurveFsmAction::currentTime
	float ___currentTime_21;
	// System.Single[] HutongGames.PlayMaker.Actions.CurveFsmAction::endTimes
	SingleU5BU5D_t1444911251* ___endTimes_22;
	// System.Single HutongGames.PlayMaker.Actions.CurveFsmAction::lastTime
	float ___lastTime_23;
	// System.Single HutongGames.PlayMaker.Actions.CurveFsmAction::deltaTime
	float ___deltaTime_24;
	// System.Single HutongGames.PlayMaker.Actions.CurveFsmAction::delayTime
	float ___delayTime_25;
	// System.Single[] HutongGames.PlayMaker.Actions.CurveFsmAction::keyOffsets
	SingleU5BU5D_t1444911251* ___keyOffsets_26;
	// UnityEngine.AnimationCurve[] HutongGames.PlayMaker.Actions.CurveFsmAction::curves
	AnimationCurveU5BU5D_t4291922187* ___curves_27;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation[] HutongGames.PlayMaker.Actions.CurveFsmAction::calculations
	CalculationU5BU5D_t3587777084* ___calculations_28;
	// System.Single[] HutongGames.PlayMaker.Actions.CurveFsmAction::resultFloats
	SingleU5BU5D_t1444911251* ___resultFloats_29;
	// System.Single[] HutongGames.PlayMaker.Actions.CurveFsmAction::fromFloats
	SingleU5BU5D_t1444911251* ___fromFloats_30;
	// System.Single[] HutongGames.PlayMaker.Actions.CurveFsmAction::toFloats
	SingleU5BU5D_t1444911251* ___toFloats_31;
	// System.Single[] HutongGames.PlayMaker.Actions.CurveFsmAction::distances
	SingleU5BU5D_t1444911251* ___distances_32;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFsmAction::finishAction
	bool ___finishAction_33;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFsmAction::isRunning
	bool ___isRunning_34;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFsmAction::looping
	bool ___looping_35;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFsmAction::start
	bool ___start_36;
	// System.Single HutongGames.PlayMaker.Actions.CurveFsmAction::largestEndTime
	float ___largestEndTime_37;

public:
	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___time_14)); }
	inline FsmFloat_t2883254149 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2883254149 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2883254149 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier((&___time_14), value);
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___speed_15)); }
	inline FsmFloat_t2883254149 * get_speed_15() const { return ___speed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(FsmFloat_t2883254149 * value)
	{
		___speed_15 = value;
		Il2CppCodeGenWriteBarrier((&___speed_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_ignoreCurveOffset_17() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___ignoreCurveOffset_17)); }
	inline FsmBool_t163807967 * get_ignoreCurveOffset_17() const { return ___ignoreCurveOffset_17; }
	inline FsmBool_t163807967 ** get_address_of_ignoreCurveOffset_17() { return &___ignoreCurveOffset_17; }
	inline void set_ignoreCurveOffset_17(FsmBool_t163807967 * value)
	{
		___ignoreCurveOffset_17 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreCurveOffset_17), value);
	}

	inline static int32_t get_offset_of_finishEvent_18() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___finishEvent_18)); }
	inline FsmEvent_t3736299882 * get_finishEvent_18() const { return ___finishEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_18() { return &___finishEvent_18; }
	inline void set_finishEvent_18(FsmEvent_t3736299882 * value)
	{
		___finishEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_18), value);
	}

	inline static int32_t get_offset_of_realTime_19() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___realTime_19)); }
	inline bool get_realTime_19() const { return ___realTime_19; }
	inline bool* get_address_of_realTime_19() { return &___realTime_19; }
	inline void set_realTime_19(bool value)
	{
		___realTime_19 = value;
	}

	inline static int32_t get_offset_of_startTime_20() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___startTime_20)); }
	inline float get_startTime_20() const { return ___startTime_20; }
	inline float* get_address_of_startTime_20() { return &___startTime_20; }
	inline void set_startTime_20(float value)
	{
		___startTime_20 = value;
	}

	inline static int32_t get_offset_of_currentTime_21() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___currentTime_21)); }
	inline float get_currentTime_21() const { return ___currentTime_21; }
	inline float* get_address_of_currentTime_21() { return &___currentTime_21; }
	inline void set_currentTime_21(float value)
	{
		___currentTime_21 = value;
	}

	inline static int32_t get_offset_of_endTimes_22() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___endTimes_22)); }
	inline SingleU5BU5D_t1444911251* get_endTimes_22() const { return ___endTimes_22; }
	inline SingleU5BU5D_t1444911251** get_address_of_endTimes_22() { return &___endTimes_22; }
	inline void set_endTimes_22(SingleU5BU5D_t1444911251* value)
	{
		___endTimes_22 = value;
		Il2CppCodeGenWriteBarrier((&___endTimes_22), value);
	}

	inline static int32_t get_offset_of_lastTime_23() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___lastTime_23)); }
	inline float get_lastTime_23() const { return ___lastTime_23; }
	inline float* get_address_of_lastTime_23() { return &___lastTime_23; }
	inline void set_lastTime_23(float value)
	{
		___lastTime_23 = value;
	}

	inline static int32_t get_offset_of_deltaTime_24() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___deltaTime_24)); }
	inline float get_deltaTime_24() const { return ___deltaTime_24; }
	inline float* get_address_of_deltaTime_24() { return &___deltaTime_24; }
	inline void set_deltaTime_24(float value)
	{
		___deltaTime_24 = value;
	}

	inline static int32_t get_offset_of_delayTime_25() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___delayTime_25)); }
	inline float get_delayTime_25() const { return ___delayTime_25; }
	inline float* get_address_of_delayTime_25() { return &___delayTime_25; }
	inline void set_delayTime_25(float value)
	{
		___delayTime_25 = value;
	}

	inline static int32_t get_offset_of_keyOffsets_26() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___keyOffsets_26)); }
	inline SingleU5BU5D_t1444911251* get_keyOffsets_26() const { return ___keyOffsets_26; }
	inline SingleU5BU5D_t1444911251** get_address_of_keyOffsets_26() { return &___keyOffsets_26; }
	inline void set_keyOffsets_26(SingleU5BU5D_t1444911251* value)
	{
		___keyOffsets_26 = value;
		Il2CppCodeGenWriteBarrier((&___keyOffsets_26), value);
	}

	inline static int32_t get_offset_of_curves_27() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___curves_27)); }
	inline AnimationCurveU5BU5D_t4291922187* get_curves_27() const { return ___curves_27; }
	inline AnimationCurveU5BU5D_t4291922187** get_address_of_curves_27() { return &___curves_27; }
	inline void set_curves_27(AnimationCurveU5BU5D_t4291922187* value)
	{
		___curves_27 = value;
		Il2CppCodeGenWriteBarrier((&___curves_27), value);
	}

	inline static int32_t get_offset_of_calculations_28() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___calculations_28)); }
	inline CalculationU5BU5D_t3587777084* get_calculations_28() const { return ___calculations_28; }
	inline CalculationU5BU5D_t3587777084** get_address_of_calculations_28() { return &___calculations_28; }
	inline void set_calculations_28(CalculationU5BU5D_t3587777084* value)
	{
		___calculations_28 = value;
		Il2CppCodeGenWriteBarrier((&___calculations_28), value);
	}

	inline static int32_t get_offset_of_resultFloats_29() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___resultFloats_29)); }
	inline SingleU5BU5D_t1444911251* get_resultFloats_29() const { return ___resultFloats_29; }
	inline SingleU5BU5D_t1444911251** get_address_of_resultFloats_29() { return &___resultFloats_29; }
	inline void set_resultFloats_29(SingleU5BU5D_t1444911251* value)
	{
		___resultFloats_29 = value;
		Il2CppCodeGenWriteBarrier((&___resultFloats_29), value);
	}

	inline static int32_t get_offset_of_fromFloats_30() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___fromFloats_30)); }
	inline SingleU5BU5D_t1444911251* get_fromFloats_30() const { return ___fromFloats_30; }
	inline SingleU5BU5D_t1444911251** get_address_of_fromFloats_30() { return &___fromFloats_30; }
	inline void set_fromFloats_30(SingleU5BU5D_t1444911251* value)
	{
		___fromFloats_30 = value;
		Il2CppCodeGenWriteBarrier((&___fromFloats_30), value);
	}

	inline static int32_t get_offset_of_toFloats_31() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___toFloats_31)); }
	inline SingleU5BU5D_t1444911251* get_toFloats_31() const { return ___toFloats_31; }
	inline SingleU5BU5D_t1444911251** get_address_of_toFloats_31() { return &___toFloats_31; }
	inline void set_toFloats_31(SingleU5BU5D_t1444911251* value)
	{
		___toFloats_31 = value;
		Il2CppCodeGenWriteBarrier((&___toFloats_31), value);
	}

	inline static int32_t get_offset_of_distances_32() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___distances_32)); }
	inline SingleU5BU5D_t1444911251* get_distances_32() const { return ___distances_32; }
	inline SingleU5BU5D_t1444911251** get_address_of_distances_32() { return &___distances_32; }
	inline void set_distances_32(SingleU5BU5D_t1444911251* value)
	{
		___distances_32 = value;
		Il2CppCodeGenWriteBarrier((&___distances_32), value);
	}

	inline static int32_t get_offset_of_finishAction_33() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___finishAction_33)); }
	inline bool get_finishAction_33() const { return ___finishAction_33; }
	inline bool* get_address_of_finishAction_33() { return &___finishAction_33; }
	inline void set_finishAction_33(bool value)
	{
		___finishAction_33 = value;
	}

	inline static int32_t get_offset_of_isRunning_34() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___isRunning_34)); }
	inline bool get_isRunning_34() const { return ___isRunning_34; }
	inline bool* get_address_of_isRunning_34() { return &___isRunning_34; }
	inline void set_isRunning_34(bool value)
	{
		___isRunning_34 = value;
	}

	inline static int32_t get_offset_of_looping_35() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___looping_35)); }
	inline bool get_looping_35() const { return ___looping_35; }
	inline bool* get_address_of_looping_35() { return &___looping_35; }
	inline void set_looping_35(bool value)
	{
		___looping_35 = value;
	}

	inline static int32_t get_offset_of_start_36() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___start_36)); }
	inline bool get_start_36() const { return ___start_36; }
	inline bool* get_address_of_start_36() { return &___start_36; }
	inline void set_start_36(bool value)
	{
		___start_36 = value;
	}

	inline static int32_t get_offset_of_largestEndTime_37() { return static_cast<int32_t>(offsetof(CurveFsmAction_t928230875, ___largestEndTime_37)); }
	inline float get_largestEndTime_37() const { return ___largestEndTime_37; }
	inline float* get_address_of_largestEndTime_37() { return &___largestEndTime_37; }
	inline void set_largestEndTime_37(float value)
	{
		___largestEndTime_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEFSMACTION_T928230875_H
#ifndef EASEFSMACTION_T2585076379_H
#define EASEFSMACTION_T2585076379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFsmAction
struct  EaseFsmAction_t2585076379  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::time
	FsmFloat_t2883254149 * ___time_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::speed
	FsmFloat_t2883254149 * ___speed_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::delay
	FsmFloat_t2883254149 * ___delay_16;
	// HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType HutongGames.PlayMaker.Actions.EaseFsmAction::easeType
	int32_t ___easeType_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EaseFsmAction::reverse
	FsmBool_t163807967 * ___reverse_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EaseFsmAction::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::realTime
	bool ___realTime_20;
	// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction HutongGames.PlayMaker.Actions.EaseFsmAction::ease
	EasingFunction_t338733390 * ___ease_21;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::runningTime
	float ___runningTime_22;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::lastTime
	float ___lastTime_23;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::startTime
	float ___startTime_24;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::deltaTime
	float ___deltaTime_25;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::delayTime
	float ___delayTime_26;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::percentage
	float ___percentage_27;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::fromFloats
	SingleU5BU5D_t1444911251* ___fromFloats_28;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::toFloats
	SingleU5BU5D_t1444911251* ___toFloats_29;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::resultFloats
	SingleU5BU5D_t1444911251* ___resultFloats_30;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::finishAction
	bool ___finishAction_31;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::start
	bool ___start_32;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::finished
	bool ___finished_33;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::isRunning
	bool ___isRunning_34;

public:
	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___time_14)); }
	inline FsmFloat_t2883254149 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2883254149 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2883254149 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier((&___time_14), value);
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___speed_15)); }
	inline FsmFloat_t2883254149 * get_speed_15() const { return ___speed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(FsmFloat_t2883254149 * value)
	{
		___speed_15 = value;
		Il2CppCodeGenWriteBarrier((&___speed_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_easeType_17() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___easeType_17)); }
	inline int32_t get_easeType_17() const { return ___easeType_17; }
	inline int32_t* get_address_of_easeType_17() { return &___easeType_17; }
	inline void set_easeType_17(int32_t value)
	{
		___easeType_17 = value;
	}

	inline static int32_t get_offset_of_reverse_18() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___reverse_18)); }
	inline FsmBool_t163807967 * get_reverse_18() const { return ___reverse_18; }
	inline FsmBool_t163807967 ** get_address_of_reverse_18() { return &___reverse_18; }
	inline void set_reverse_18(FsmBool_t163807967 * value)
	{
		___reverse_18 = value;
		Il2CppCodeGenWriteBarrier((&___reverse_18), value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___finishEvent_19)); }
	inline FsmEvent_t3736299882 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t3736299882 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_19), value);
	}

	inline static int32_t get_offset_of_realTime_20() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___realTime_20)); }
	inline bool get_realTime_20() const { return ___realTime_20; }
	inline bool* get_address_of_realTime_20() { return &___realTime_20; }
	inline void set_realTime_20(bool value)
	{
		___realTime_20 = value;
	}

	inline static int32_t get_offset_of_ease_21() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___ease_21)); }
	inline EasingFunction_t338733390 * get_ease_21() const { return ___ease_21; }
	inline EasingFunction_t338733390 ** get_address_of_ease_21() { return &___ease_21; }
	inline void set_ease_21(EasingFunction_t338733390 * value)
	{
		___ease_21 = value;
		Il2CppCodeGenWriteBarrier((&___ease_21), value);
	}

	inline static int32_t get_offset_of_runningTime_22() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___runningTime_22)); }
	inline float get_runningTime_22() const { return ___runningTime_22; }
	inline float* get_address_of_runningTime_22() { return &___runningTime_22; }
	inline void set_runningTime_22(float value)
	{
		___runningTime_22 = value;
	}

	inline static int32_t get_offset_of_lastTime_23() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___lastTime_23)); }
	inline float get_lastTime_23() const { return ___lastTime_23; }
	inline float* get_address_of_lastTime_23() { return &___lastTime_23; }
	inline void set_lastTime_23(float value)
	{
		___lastTime_23 = value;
	}

	inline static int32_t get_offset_of_startTime_24() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___startTime_24)); }
	inline float get_startTime_24() const { return ___startTime_24; }
	inline float* get_address_of_startTime_24() { return &___startTime_24; }
	inline void set_startTime_24(float value)
	{
		___startTime_24 = value;
	}

	inline static int32_t get_offset_of_deltaTime_25() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___deltaTime_25)); }
	inline float get_deltaTime_25() const { return ___deltaTime_25; }
	inline float* get_address_of_deltaTime_25() { return &___deltaTime_25; }
	inline void set_deltaTime_25(float value)
	{
		___deltaTime_25 = value;
	}

	inline static int32_t get_offset_of_delayTime_26() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___delayTime_26)); }
	inline float get_delayTime_26() const { return ___delayTime_26; }
	inline float* get_address_of_delayTime_26() { return &___delayTime_26; }
	inline void set_delayTime_26(float value)
	{
		___delayTime_26 = value;
	}

	inline static int32_t get_offset_of_percentage_27() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___percentage_27)); }
	inline float get_percentage_27() const { return ___percentage_27; }
	inline float* get_address_of_percentage_27() { return &___percentage_27; }
	inline void set_percentage_27(float value)
	{
		___percentage_27 = value;
	}

	inline static int32_t get_offset_of_fromFloats_28() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___fromFloats_28)); }
	inline SingleU5BU5D_t1444911251* get_fromFloats_28() const { return ___fromFloats_28; }
	inline SingleU5BU5D_t1444911251** get_address_of_fromFloats_28() { return &___fromFloats_28; }
	inline void set_fromFloats_28(SingleU5BU5D_t1444911251* value)
	{
		___fromFloats_28 = value;
		Il2CppCodeGenWriteBarrier((&___fromFloats_28), value);
	}

	inline static int32_t get_offset_of_toFloats_29() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___toFloats_29)); }
	inline SingleU5BU5D_t1444911251* get_toFloats_29() const { return ___toFloats_29; }
	inline SingleU5BU5D_t1444911251** get_address_of_toFloats_29() { return &___toFloats_29; }
	inline void set_toFloats_29(SingleU5BU5D_t1444911251* value)
	{
		___toFloats_29 = value;
		Il2CppCodeGenWriteBarrier((&___toFloats_29), value);
	}

	inline static int32_t get_offset_of_resultFloats_30() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___resultFloats_30)); }
	inline SingleU5BU5D_t1444911251* get_resultFloats_30() const { return ___resultFloats_30; }
	inline SingleU5BU5D_t1444911251** get_address_of_resultFloats_30() { return &___resultFloats_30; }
	inline void set_resultFloats_30(SingleU5BU5D_t1444911251* value)
	{
		___resultFloats_30 = value;
		Il2CppCodeGenWriteBarrier((&___resultFloats_30), value);
	}

	inline static int32_t get_offset_of_finishAction_31() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___finishAction_31)); }
	inline bool get_finishAction_31() const { return ___finishAction_31; }
	inline bool* get_address_of_finishAction_31() { return &___finishAction_31; }
	inline void set_finishAction_31(bool value)
	{
		___finishAction_31 = value;
	}

	inline static int32_t get_offset_of_start_32() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___start_32)); }
	inline bool get_start_32() const { return ___start_32; }
	inline bool* get_address_of_start_32() { return &___start_32; }
	inline void set_start_32(bool value)
	{
		___start_32 = value;
	}

	inline static int32_t get_offset_of_finished_33() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___finished_33)); }
	inline bool get_finished_33() const { return ___finished_33; }
	inline bool* get_address_of_finished_33() { return &___finished_33; }
	inline void set_finished_33(bool value)
	{
		___finished_33 = value;
	}

	inline static int32_t get_offset_of_isRunning_34() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___isRunning_34)); }
	inline bool get_isRunning_34() const { return ___isRunning_34; }
	inline bool* get_address_of_isRunning_34() { return &___isRunning_34; }
	inline void set_isRunning_34(bool value)
	{
		___isRunning_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFSMACTION_T2585076379_H
#ifndef FSMSTATEACTIONANIMATORBASE_T3384377168_H
#define FSMSTATEACTIONANIMATORBASE_T3384377168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase
struct  FsmStateActionAnimatorBase_t3384377168  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::everyFrame
	bool ___everyFrame_14;
	// HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase/AnimatorFrameUpdateSelector HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::everyFrameOption
	int32_t ___everyFrameOption_15;
	// System.Int32 HutongGames.PlayMaker.Actions.FsmStateActionAnimatorBase::IklayerIndex
	int32_t ___IklayerIndex_16;

public:
	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(FsmStateActionAnimatorBase_t3384377168, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_everyFrameOption_15() { return static_cast<int32_t>(offsetof(FsmStateActionAnimatorBase_t3384377168, ___everyFrameOption_15)); }
	inline int32_t get_everyFrameOption_15() const { return ___everyFrameOption_15; }
	inline int32_t* get_address_of_everyFrameOption_15() { return &___everyFrameOption_15; }
	inline void set_everyFrameOption_15(int32_t value)
	{
		___everyFrameOption_15 = value;
	}

	inline static int32_t get_offset_of_IklayerIndex_16() { return static_cast<int32_t>(offsetof(FsmStateActionAnimatorBase_t3384377168, ___IklayerIndex_16)); }
	inline int32_t get_IklayerIndex_16() const { return ___IklayerIndex_16; }
	inline int32_t* get_address_of_IklayerIndex_16() { return &___IklayerIndex_16; }
	inline void set_IklayerIndex_16(int32_t value)
	{
		___IklayerIndex_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTIONANIMATORBASE_T3384377168_H
#ifndef GETANIMATORAPPLYROOTMOTION_T985245695_H
#define GETANIMATORAPPLYROOTMOTION_T985245695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion
struct  GetAnimatorApplyRootMotion_t985245695  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::rootMotionApplied
	FsmBool_t163807967 * ___rootMotionApplied_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::rootMotionIsAppliedEvent
	FsmEvent_t3736299882 * ___rootMotionIsAppliedEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::rootMotionIsNotAppliedEvent
	FsmEvent_t3736299882 * ___rootMotionIsNotAppliedEvent_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::_animator
	Animator_t434523843 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorApplyRootMotion_t985245695, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_rootMotionApplied_15() { return static_cast<int32_t>(offsetof(GetAnimatorApplyRootMotion_t985245695, ___rootMotionApplied_15)); }
	inline FsmBool_t163807967 * get_rootMotionApplied_15() const { return ___rootMotionApplied_15; }
	inline FsmBool_t163807967 ** get_address_of_rootMotionApplied_15() { return &___rootMotionApplied_15; }
	inline void set_rootMotionApplied_15(FsmBool_t163807967 * value)
	{
		___rootMotionApplied_15 = value;
		Il2CppCodeGenWriteBarrier((&___rootMotionApplied_15), value);
	}

	inline static int32_t get_offset_of_rootMotionIsAppliedEvent_16() { return static_cast<int32_t>(offsetof(GetAnimatorApplyRootMotion_t985245695, ___rootMotionIsAppliedEvent_16)); }
	inline FsmEvent_t3736299882 * get_rootMotionIsAppliedEvent_16() const { return ___rootMotionIsAppliedEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_rootMotionIsAppliedEvent_16() { return &___rootMotionIsAppliedEvent_16; }
	inline void set_rootMotionIsAppliedEvent_16(FsmEvent_t3736299882 * value)
	{
		___rootMotionIsAppliedEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___rootMotionIsAppliedEvent_16), value);
	}

	inline static int32_t get_offset_of_rootMotionIsNotAppliedEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorApplyRootMotion_t985245695, ___rootMotionIsNotAppliedEvent_17)); }
	inline FsmEvent_t3736299882 * get_rootMotionIsNotAppliedEvent_17() const { return ___rootMotionIsNotAppliedEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_rootMotionIsNotAppliedEvent_17() { return &___rootMotionIsNotAppliedEvent_17; }
	inline void set_rootMotionIsNotAppliedEvent_17(FsmEvent_t3736299882 * value)
	{
		___rootMotionIsNotAppliedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___rootMotionIsNotAppliedEvent_17), value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorApplyRootMotion_t985245695, ____animator_18)); }
	inline Animator_t434523843 * get__animator_18() const { return ____animator_18; }
	inline Animator_t434523843 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t434523843 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier((&____animator_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORAPPLYROOTMOTION_T985245695_H
#ifndef GETANIMATORBONEGAMEOBJECT_T1423048408_H
#define GETANIMATORBONEGAMEOBJECT_T1423048408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
struct  GetAnimatorBoneGameObject_t1423048408  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::bone
	FsmEnum_t2861764163 * ___bone_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::boneGameObject
	FsmGameObject_t3581898942 * ___boneGameObject_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t1423048408, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_bone_15() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t1423048408, ___bone_15)); }
	inline FsmEnum_t2861764163 * get_bone_15() const { return ___bone_15; }
	inline FsmEnum_t2861764163 ** get_address_of_bone_15() { return &___bone_15; }
	inline void set_bone_15(FsmEnum_t2861764163 * value)
	{
		___bone_15 = value;
		Il2CppCodeGenWriteBarrier((&___bone_15), value);
	}

	inline static int32_t get_offset_of_boneGameObject_16() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t1423048408, ___boneGameObject_16)); }
	inline FsmGameObject_t3581898942 * get_boneGameObject_16() const { return ___boneGameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_boneGameObject_16() { return &___boneGameObject_16; }
	inline void set_boneGameObject_16(FsmGameObject_t3581898942 * value)
	{
		___boneGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___boneGameObject_16), value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorBoneGameObject_t1423048408, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORBONEGAMEOBJECT_T1423048408_H
#ifndef GETANIMATORCULLINGMODE_T2982375881_H
#define GETANIMATORCULLINGMODE_T2982375881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCullingMode
struct  GetAnimatorCullingMode_t2982375881  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::alwaysAnimate
	FsmBool_t163807967 * ___alwaysAnimate_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::alwaysAnimateEvent
	FsmEvent_t3736299882 * ___alwaysAnimateEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::basedOnRenderersEvent
	FsmEvent_t3736299882 * ___basedOnRenderersEvent_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::_animator
	Animator_t434523843 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorCullingMode_t2982375881, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_alwaysAnimate_15() { return static_cast<int32_t>(offsetof(GetAnimatorCullingMode_t2982375881, ___alwaysAnimate_15)); }
	inline FsmBool_t163807967 * get_alwaysAnimate_15() const { return ___alwaysAnimate_15; }
	inline FsmBool_t163807967 ** get_address_of_alwaysAnimate_15() { return &___alwaysAnimate_15; }
	inline void set_alwaysAnimate_15(FsmBool_t163807967 * value)
	{
		___alwaysAnimate_15 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysAnimate_15), value);
	}

	inline static int32_t get_offset_of_alwaysAnimateEvent_16() { return static_cast<int32_t>(offsetof(GetAnimatorCullingMode_t2982375881, ___alwaysAnimateEvent_16)); }
	inline FsmEvent_t3736299882 * get_alwaysAnimateEvent_16() const { return ___alwaysAnimateEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_alwaysAnimateEvent_16() { return &___alwaysAnimateEvent_16; }
	inline void set_alwaysAnimateEvent_16(FsmEvent_t3736299882 * value)
	{
		___alwaysAnimateEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysAnimateEvent_16), value);
	}

	inline static int32_t get_offset_of_basedOnRenderersEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorCullingMode_t2982375881, ___basedOnRenderersEvent_17)); }
	inline FsmEvent_t3736299882 * get_basedOnRenderersEvent_17() const { return ___basedOnRenderersEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_basedOnRenderersEvent_17() { return &___basedOnRenderersEvent_17; }
	inline void set_basedOnRenderersEvent_17(FsmEvent_t3736299882 * value)
	{
		___basedOnRenderersEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___basedOnRenderersEvent_17), value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorCullingMode_t2982375881, ____animator_18)); }
	inline Animator_t434523843 * get__animator_18() const { return ____animator_18; }
	inline Animator_t434523843 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t434523843 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier((&____animator_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCULLINGMODE_T2982375881_H
#ifndef GETANIMATORFEETPIVOTACTIVE_T2091396342_H
#define GETANIMATORFEETPIVOTACTIVE_T2091396342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive
struct  GetAnimatorFeetPivotActive_t2091396342  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::feetPivotActive
	FsmFloat_t2883254149 * ___feetPivotActive_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorFeetPivotActive_t2091396342, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_feetPivotActive_15() { return static_cast<int32_t>(offsetof(GetAnimatorFeetPivotActive_t2091396342, ___feetPivotActive_15)); }
	inline FsmFloat_t2883254149 * get_feetPivotActive_15() const { return ___feetPivotActive_15; }
	inline FsmFloat_t2883254149 ** get_address_of_feetPivotActive_15() { return &___feetPivotActive_15; }
	inline void set_feetPivotActive_15(FsmFloat_t2883254149 * value)
	{
		___feetPivotActive_15 = value;
		Il2CppCodeGenWriteBarrier((&___feetPivotActive_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(GetAnimatorFeetPivotActive_t2091396342, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORFEETPIVOTACTIVE_T2091396342_H
#ifndef GETANIMATORHUMANSCALE_T2710047612_H
#define GETANIMATORHUMANSCALE_T2710047612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorHumanScale
struct  GetAnimatorHumanScale_t2710047612  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::humanScale
	FsmFloat_t2883254149 * ___humanScale_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorHumanScale_t2710047612, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_humanScale_15() { return static_cast<int32_t>(offsetof(GetAnimatorHumanScale_t2710047612, ___humanScale_15)); }
	inline FsmFloat_t2883254149 * get_humanScale_15() const { return ___humanScale_15; }
	inline FsmFloat_t2883254149 ** get_address_of_humanScale_15() { return &___humanScale_15; }
	inline void set_humanScale_15(FsmFloat_t2883254149 * value)
	{
		___humanScale_15 = value;
		Il2CppCodeGenWriteBarrier((&___humanScale_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(GetAnimatorHumanScale_t2710047612, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORHUMANSCALE_T2710047612_H
#ifndef GETANIMATORISHUMAN_T378752370_H
#define GETANIMATORISHUMAN_T378752370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
struct  GetAnimatorIsHuman_t378752370  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::isHuman
	FsmBool_t163807967 * ___isHuman_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::isHumanEvent
	FsmEvent_t3736299882 * ___isHumanEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::isGenericEvent
	FsmEvent_t3736299882 * ___isGenericEvent_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::_animator
	Animator_t434523843 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t378752370, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_isHuman_15() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t378752370, ___isHuman_15)); }
	inline FsmBool_t163807967 * get_isHuman_15() const { return ___isHuman_15; }
	inline FsmBool_t163807967 ** get_address_of_isHuman_15() { return &___isHuman_15; }
	inline void set_isHuman_15(FsmBool_t163807967 * value)
	{
		___isHuman_15 = value;
		Il2CppCodeGenWriteBarrier((&___isHuman_15), value);
	}

	inline static int32_t get_offset_of_isHumanEvent_16() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t378752370, ___isHumanEvent_16)); }
	inline FsmEvent_t3736299882 * get_isHumanEvent_16() const { return ___isHumanEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_isHumanEvent_16() { return &___isHumanEvent_16; }
	inline void set_isHumanEvent_16(FsmEvent_t3736299882 * value)
	{
		___isHumanEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___isHumanEvent_16), value);
	}

	inline static int32_t get_offset_of_isGenericEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t378752370, ___isGenericEvent_17)); }
	inline FsmEvent_t3736299882 * get_isGenericEvent_17() const { return ___isGenericEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_isGenericEvent_17() { return &___isGenericEvent_17; }
	inline void set_isGenericEvent_17(FsmEvent_t3736299882 * value)
	{
		___isGenericEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___isGenericEvent_17), value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorIsHuman_t378752370, ____animator_18)); }
	inline Animator_t434523843 * get__animator_18() const { return ____animator_18; }
	inline Animator_t434523843 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t434523843 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier((&____animator_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORISHUMAN_T378752370_H
#ifndef GETANIMATORISPARAMETERCONTROLLEDBYCURVE_T745733894_H
#define GETANIMATORISPARAMETERCONTROLLEDBYCURVE_T745733894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
struct  GetAnimatorIsParameterControlledByCurve_t745733894  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::parameterName
	FsmString_t1785915204 * ___parameterName_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::isControlledByCurve
	FsmBool_t163807967 * ___isControlledByCurve_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::isControlledByCurveEvent
	FsmEvent_t3736299882 * ___isControlledByCurveEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::isNotControlledByCurveEvent
	FsmEvent_t3736299882 * ___isNotControlledByCurveEvent_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::_animator
	Animator_t434523843 * ____animator_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t745733894, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_parameterName_15() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t745733894, ___parameterName_15)); }
	inline FsmString_t1785915204 * get_parameterName_15() const { return ___parameterName_15; }
	inline FsmString_t1785915204 ** get_address_of_parameterName_15() { return &___parameterName_15; }
	inline void set_parameterName_15(FsmString_t1785915204 * value)
	{
		___parameterName_15 = value;
		Il2CppCodeGenWriteBarrier((&___parameterName_15), value);
	}

	inline static int32_t get_offset_of_isControlledByCurve_16() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t745733894, ___isControlledByCurve_16)); }
	inline FsmBool_t163807967 * get_isControlledByCurve_16() const { return ___isControlledByCurve_16; }
	inline FsmBool_t163807967 ** get_address_of_isControlledByCurve_16() { return &___isControlledByCurve_16; }
	inline void set_isControlledByCurve_16(FsmBool_t163807967 * value)
	{
		___isControlledByCurve_16 = value;
		Il2CppCodeGenWriteBarrier((&___isControlledByCurve_16), value);
	}

	inline static int32_t get_offset_of_isControlledByCurveEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t745733894, ___isControlledByCurveEvent_17)); }
	inline FsmEvent_t3736299882 * get_isControlledByCurveEvent_17() const { return ___isControlledByCurveEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_isControlledByCurveEvent_17() { return &___isControlledByCurveEvent_17; }
	inline void set_isControlledByCurveEvent_17(FsmEvent_t3736299882 * value)
	{
		___isControlledByCurveEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___isControlledByCurveEvent_17), value);
	}

	inline static int32_t get_offset_of_isNotControlledByCurveEvent_18() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t745733894, ___isNotControlledByCurveEvent_18)); }
	inline FsmEvent_t3736299882 * get_isNotControlledByCurveEvent_18() const { return ___isNotControlledByCurveEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotControlledByCurveEvent_18() { return &___isNotControlledByCurveEvent_18; }
	inline void set_isNotControlledByCurveEvent_18(FsmEvent_t3736299882 * value)
	{
		___isNotControlledByCurveEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___isNotControlledByCurveEvent_18), value);
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(GetAnimatorIsParameterControlledByCurve_t745733894, ____animator_19)); }
	inline Animator_t434523843 * get__animator_19() const { return ____animator_19; }
	inline Animator_t434523843 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t434523843 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier((&____animator_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORISPARAMETERCONTROLLEDBYCURVE_T745733894_H
#ifndef GETANIMATORLAYERCOUNT_T4239821965_H
#define GETANIMATORLAYERCOUNT_T4239821965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayerCount
struct  GetAnimatorLayerCount_t4239821965  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::layerCount
	FsmInt_t874273141 * ___layerCount_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorLayerCount_t4239821965, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_layerCount_15() { return static_cast<int32_t>(offsetof(GetAnimatorLayerCount_t4239821965, ___layerCount_15)); }
	inline FsmInt_t874273141 * get_layerCount_15() const { return ___layerCount_15; }
	inline FsmInt_t874273141 ** get_address_of_layerCount_15() { return &___layerCount_15; }
	inline void set_layerCount_15(FsmInt_t874273141 * value)
	{
		___layerCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___layerCount_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(GetAnimatorLayerCount_t4239821965, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORLAYERCOUNT_T4239821965_H
#ifndef GETANIMATORLAYERNAME_T3204126378_H
#define GETANIMATORLAYERNAME_T3204126378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayerName
struct  GetAnimatorLayerName_t3204126378  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayerName::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorLayerName::layerIndex
	FsmInt_t874273141 * ___layerIndex_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorLayerName::layerName
	FsmString_t1785915204 * ___layerName_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayerName::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t3204126378, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_layerIndex_15() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t3204126378, ___layerIndex_15)); }
	inline FsmInt_t874273141 * get_layerIndex_15() const { return ___layerIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_15() { return &___layerIndex_15; }
	inline void set_layerIndex_15(FsmInt_t874273141 * value)
	{
		___layerIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_15), value);
	}

	inline static int32_t get_offset_of_layerName_16() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t3204126378, ___layerName_16)); }
	inline FsmString_t1785915204 * get_layerName_16() const { return ___layerName_16; }
	inline FsmString_t1785915204 ** get_address_of_layerName_16() { return &___layerName_16; }
	inline void set_layerName_16(FsmString_t1785915204 * value)
	{
		___layerName_16 = value;
		Il2CppCodeGenWriteBarrier((&___layerName_16), value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorLayerName_t3204126378, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORLAYERNAME_T3204126378_H
#ifndef GETANIMATORLAYERSAFFECTMASSCENTER_T1877605656_H
#define GETANIMATORLAYERSAFFECTMASSCENTER_T1877605656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter
struct  GetAnimatorLayersAffectMassCenter_t1877605656  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::affectMassCenter
	FsmBool_t163807967 * ___affectMassCenter_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::affectMassCenterEvent
	FsmEvent_t3736299882 * ___affectMassCenterEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::doNotAffectMassCenterEvent
	FsmEvent_t3736299882 * ___doNotAffectMassCenterEvent_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayersAffectMassCenter::_animator
	Animator_t434523843 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t1877605656, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_affectMassCenter_15() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t1877605656, ___affectMassCenter_15)); }
	inline FsmBool_t163807967 * get_affectMassCenter_15() const { return ___affectMassCenter_15; }
	inline FsmBool_t163807967 ** get_address_of_affectMassCenter_15() { return &___affectMassCenter_15; }
	inline void set_affectMassCenter_15(FsmBool_t163807967 * value)
	{
		___affectMassCenter_15 = value;
		Il2CppCodeGenWriteBarrier((&___affectMassCenter_15), value);
	}

	inline static int32_t get_offset_of_affectMassCenterEvent_16() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t1877605656, ___affectMassCenterEvent_16)); }
	inline FsmEvent_t3736299882 * get_affectMassCenterEvent_16() const { return ___affectMassCenterEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_affectMassCenterEvent_16() { return &___affectMassCenterEvent_16; }
	inline void set_affectMassCenterEvent_16(FsmEvent_t3736299882 * value)
	{
		___affectMassCenterEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___affectMassCenterEvent_16), value);
	}

	inline static int32_t get_offset_of_doNotAffectMassCenterEvent_17() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t1877605656, ___doNotAffectMassCenterEvent_17)); }
	inline FsmEvent_t3736299882 * get_doNotAffectMassCenterEvent_17() const { return ___doNotAffectMassCenterEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_doNotAffectMassCenterEvent_17() { return &___doNotAffectMassCenterEvent_17; }
	inline void set_doNotAffectMassCenterEvent_17(FsmEvent_t3736299882 * value)
	{
		___doNotAffectMassCenterEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___doNotAffectMassCenterEvent_17), value);
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(GetAnimatorLayersAffectMassCenter_t1877605656, ____animator_18)); }
	inline Animator_t434523843 * get__animator_18() const { return ____animator_18; }
	inline Animator_t434523843 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t434523843 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier((&____animator_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORLAYERSAFFECTMASSCENTER_T1877605656_H
#ifndef GETANIMATORLEFTFOOTBOTTOMHEIGHT_T1731074969_H
#define GETANIMATORLEFTFOOTBOTTOMHEIGHT_T1731074969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight
struct  GetAnimatorLeftFootBottomHeight_t1731074969  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::leftFootHeight
	FsmFloat_t2883254149 * ___leftFootHeight_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorLeftFootBottomHeight_t1731074969, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_leftFootHeight_15() { return static_cast<int32_t>(offsetof(GetAnimatorLeftFootBottomHeight_t1731074969, ___leftFootHeight_15)); }
	inline FsmFloat_t2883254149 * get_leftFootHeight_15() const { return ___leftFootHeight_15; }
	inline FsmFloat_t2883254149 ** get_address_of_leftFootHeight_15() { return &___leftFootHeight_15; }
	inline void set_leftFootHeight_15(FsmFloat_t2883254149 * value)
	{
		___leftFootHeight_15 = value;
		Il2CppCodeGenWriteBarrier((&___leftFootHeight_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetAnimatorLeftFootBottomHeight_t1731074969, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorLeftFootBottomHeight_t1731074969, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORLEFTFOOTBOTTOMHEIGHT_T1731074969_H
#ifndef GETANIMATORPLAYBACKSPEED_T335189200_H
#define GETANIMATORPLAYBACKSPEED_T335189200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed
struct  GetAnimatorPlayBackSpeed_t335189200  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::playBackSpeed
	FsmFloat_t2883254149 * ___playBackSpeed_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackSpeed_t335189200, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_playBackSpeed_15() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackSpeed_t335189200, ___playBackSpeed_15)); }
	inline FsmFloat_t2883254149 * get_playBackSpeed_15() const { return ___playBackSpeed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_playBackSpeed_15() { return &___playBackSpeed_15; }
	inline void set_playBackSpeed_15(FsmFloat_t2883254149 * value)
	{
		___playBackSpeed_15 = value;
		Il2CppCodeGenWriteBarrier((&___playBackSpeed_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackSpeed_t335189200, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackSpeed_t335189200, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORPLAYBACKSPEED_T335189200_H
#ifndef GETANIMATORPLAYBACKTIME_T3615175085_H
#define GETANIMATORPLAYBACKTIME_T3615175085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime
struct  GetAnimatorPlayBackTime_t3615175085  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::playBackTime
	FsmFloat_t2883254149 * ___playBackTime_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackTime_t3615175085, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_playBackTime_15() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackTime_t3615175085, ___playBackTime_15)); }
	inline FsmFloat_t2883254149 * get_playBackTime_15() const { return ___playBackTime_15; }
	inline FsmFloat_t2883254149 ** get_address_of_playBackTime_15() { return &___playBackTime_15; }
	inline void set_playBackTime_15(FsmFloat_t2883254149 * value)
	{
		___playBackTime_15 = value;
		Il2CppCodeGenWriteBarrier((&___playBackTime_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackTime_t3615175085, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorPlayBackTime_t3615175085, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORPLAYBACKTIME_T3615175085_H
#ifndef GETANIMATORRIGHTFOOTBOTTOMHEIGHT_T2632401101_H
#define GETANIMATORRIGHTFOOTBOTTOMHEIGHT_T2632401101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight
struct  GetAnimatorRightFootBottomHeight_t2632401101  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::rightFootHeight
	FsmFloat_t2883254149 * ___rightFootHeight_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAnimatorRightFootBottomHeight_t2632401101, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_rightFootHeight_15() { return static_cast<int32_t>(offsetof(GetAnimatorRightFootBottomHeight_t2632401101, ___rightFootHeight_15)); }
	inline FsmFloat_t2883254149 * get_rightFootHeight_15() const { return ___rightFootHeight_15; }
	inline FsmFloat_t2883254149 ** get_address_of_rightFootHeight_15() { return &___rightFootHeight_15; }
	inline void set_rightFootHeight_15(FsmFloat_t2883254149 * value)
	{
		___rightFootHeight_15 = value;
		Il2CppCodeGenWriteBarrier((&___rightFootHeight_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetAnimatorRightFootBottomHeight_t2632401101, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(GetAnimatorRightFootBottomHeight_t2632401101, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORRIGHTFOOTBOTTOMHEIGHT_T2632401101_H
#ifndef NAVMESHAGENTANIMATORSYNCHRONIZER_T1968050959_H
#define NAVMESHAGENTANIMATORSYNCHRONIZER_T1968050959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer
struct  NavMeshAgentAnimatorSynchronizer_t1968050959  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_animator
	Animator_t434523843 * ____animator_15;
	// UnityEngine.AI.NavMeshAgent HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_agent
	NavMeshAgent_t1276799816 * ____agent_16;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::_trans
	Transform_t3600365921 * ____trans_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t1968050959, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t1968050959, ____animator_15)); }
	inline Animator_t434523843 * get__animator_15() const { return ____animator_15; }
	inline Animator_t434523843 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t434523843 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier((&____animator_15), value);
	}

	inline static int32_t get_offset_of__agent_16() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t1968050959, ____agent_16)); }
	inline NavMeshAgent_t1276799816 * get__agent_16() const { return ____agent_16; }
	inline NavMeshAgent_t1276799816 ** get_address_of__agent_16() { return &____agent_16; }
	inline void set__agent_16(NavMeshAgent_t1276799816 * value)
	{
		____agent_16 = value;
		Il2CppCodeGenWriteBarrier((&____agent_16), value);
	}

	inline static int32_t get_offset_of__trans_17() { return static_cast<int32_t>(offsetof(NavMeshAgentAnimatorSynchronizer_t1968050959, ____trans_17)); }
	inline Transform_t3600365921 * get__trans_17() const { return ____trans_17; }
	inline Transform_t3600365921 ** get_address_of__trans_17() { return &____trans_17; }
	inline void set__trans_17(Transform_t3600365921 * value)
	{
		____trans_17 = value;
		Il2CppCodeGenWriteBarrier((&____trans_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHAGENTANIMATORSYNCHRONIZER_T1968050959_H
#ifndef SETANIMATORAPPLYROOTMOTION_T437523402_H
#define SETANIMATORAPPLYROOTMOTION_T437523402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion
struct  SetAnimatorApplyRootMotion_t437523402  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::applyRootMotion
	FsmBool_t163807967 * ___applyRootMotion_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorApplyRootMotion::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorApplyRootMotion_t437523402, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_applyRootMotion_15() { return static_cast<int32_t>(offsetof(SetAnimatorApplyRootMotion_t437523402, ___applyRootMotion_15)); }
	inline FsmBool_t163807967 * get_applyRootMotion_15() const { return ___applyRootMotion_15; }
	inline FsmBool_t163807967 ** get_address_of_applyRootMotion_15() { return &___applyRootMotion_15; }
	inline void set_applyRootMotion_15(FsmBool_t163807967 * value)
	{
		___applyRootMotion_15 = value;
		Il2CppCodeGenWriteBarrier((&___applyRootMotion_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(SetAnimatorApplyRootMotion_t437523402, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORAPPLYROOTMOTION_T437523402_H
#ifndef SETANIMATORBODY_T2450485891_H
#define SETANIMATORBODY_T2450485891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorBody
struct  SetAnimatorBody_t2450485891  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorBody::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetAnimatorBody::target
	FsmGameObject_t3581898942 * ___target_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetAnimatorBody::position
	FsmVector3_t626444517 * ___position_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetAnimatorBody::rotation
	FsmQuaternion_t4259038327 * ___rotation_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorBody::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorBody::_animator
	Animator_t434523843 * ____animator_19;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SetAnimatorBody::_transform
	Transform_t3600365921 * ____transform_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_target_15() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ___target_15)); }
	inline FsmGameObject_t3581898942 * get_target_15() const { return ___target_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_target_15() { return &___target_15; }
	inline void set_target_15(FsmGameObject_t3581898942 * value)
	{
		___target_15 = value;
		Il2CppCodeGenWriteBarrier((&___target_15), value);
	}

	inline static int32_t get_offset_of_position_16() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ___position_16)); }
	inline FsmVector3_t626444517 * get_position_16() const { return ___position_16; }
	inline FsmVector3_t626444517 ** get_address_of_position_16() { return &___position_16; }
	inline void set_position_16(FsmVector3_t626444517 * value)
	{
		___position_16 = value;
		Il2CppCodeGenWriteBarrier((&___position_16), value);
	}

	inline static int32_t get_offset_of_rotation_17() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ___rotation_17)); }
	inline FsmQuaternion_t4259038327 * get_rotation_17() const { return ___rotation_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_rotation_17() { return &___rotation_17; }
	inline void set_rotation_17(FsmQuaternion_t4259038327 * value)
	{
		___rotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ____animator_19)); }
	inline Animator_t434523843 * get__animator_19() const { return ____animator_19; }
	inline Animator_t434523843 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t434523843 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier((&____animator_19), value);
	}

	inline static int32_t get_offset_of__transform_20() { return static_cast<int32_t>(offsetof(SetAnimatorBody_t2450485891, ____transform_20)); }
	inline Transform_t3600365921 * get__transform_20() const { return ____transform_20; }
	inline Transform_t3600365921 ** get_address_of__transform_20() { return &____transform_20; }
	inline void set__transform_20(Transform_t3600365921 * value)
	{
		____transform_20 = value;
		Il2CppCodeGenWriteBarrier((&____transform_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORBODY_T2450485891_H
#ifndef SETANIMATORCULLINGMODE_T2980569892_H
#define SETANIMATORCULLINGMODE_T2980569892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorCullingMode
struct  SetAnimatorCullingMode_t2980569892  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::alwaysAnimate
	FsmBool_t163807967 * ___alwaysAnimate_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorCullingMode::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorCullingMode_t2980569892, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_alwaysAnimate_15() { return static_cast<int32_t>(offsetof(SetAnimatorCullingMode_t2980569892, ___alwaysAnimate_15)); }
	inline FsmBool_t163807967 * get_alwaysAnimate_15() const { return ___alwaysAnimate_15; }
	inline FsmBool_t163807967 ** get_address_of_alwaysAnimate_15() { return &___alwaysAnimate_15; }
	inline void set_alwaysAnimate_15(FsmBool_t163807967 * value)
	{
		___alwaysAnimate_15 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysAnimate_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(SetAnimatorCullingMode_t2980569892, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORCULLINGMODE_T2980569892_H
#ifndef SETANIMATORFEETPIVOTACTIVE_T937086675_H
#define SETANIMATORFEETPIVOTACTIVE_T937086675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive
struct  SetAnimatorFeetPivotActive_t937086675  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::feetPivotActive
	FsmFloat_t2883254149 * ___feetPivotActive_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorFeetPivotActive_t937086675, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_feetPivotActive_15() { return static_cast<int32_t>(offsetof(SetAnimatorFeetPivotActive_t937086675, ___feetPivotActive_15)); }
	inline FsmFloat_t2883254149 * get_feetPivotActive_15() const { return ___feetPivotActive_15; }
	inline FsmFloat_t2883254149 ** get_address_of_feetPivotActive_15() { return &___feetPivotActive_15; }
	inline void set_feetPivotActive_15(FsmFloat_t2883254149 * value)
	{
		___feetPivotActive_15 = value;
		Il2CppCodeGenWriteBarrier((&___feetPivotActive_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(SetAnimatorFeetPivotActive_t937086675, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORFEETPIVOTACTIVE_T937086675_H
#ifndef SETANIMATORIKGOAL_T839890372_H
#define SETANIMATORIKGOAL_T839890372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorIKGoal
struct  SetAnimatorIKGoal_t839890372  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// UnityEngine.AvatarIKGoal HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::iKGoal
	int32_t ___iKGoal_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::goal
	FsmGameObject_t3581898942 * ___goal_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::position
	FsmVector3_t626444517 * ___position_17;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::rotation
	FsmQuaternion_t4259038327 * ___rotation_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::positionWeight
	FsmFloat_t2883254149 * ___positionWeight_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::rotationWeight
	FsmFloat_t2883254149 * ___rotationWeight_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::_animator
	Animator_t434523843 * ____animator_22;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::_transform
	Transform_t3600365921 * ____transform_23;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_iKGoal_15() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___iKGoal_15)); }
	inline int32_t get_iKGoal_15() const { return ___iKGoal_15; }
	inline int32_t* get_address_of_iKGoal_15() { return &___iKGoal_15; }
	inline void set_iKGoal_15(int32_t value)
	{
		___iKGoal_15 = value;
	}

	inline static int32_t get_offset_of_goal_16() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___goal_16)); }
	inline FsmGameObject_t3581898942 * get_goal_16() const { return ___goal_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_goal_16() { return &___goal_16; }
	inline void set_goal_16(FsmGameObject_t3581898942 * value)
	{
		___goal_16 = value;
		Il2CppCodeGenWriteBarrier((&___goal_16), value);
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___position_17)); }
	inline FsmVector3_t626444517 * get_position_17() const { return ___position_17; }
	inline FsmVector3_t626444517 ** get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(FsmVector3_t626444517 * value)
	{
		___position_17 = value;
		Il2CppCodeGenWriteBarrier((&___position_17), value);
	}

	inline static int32_t get_offset_of_rotation_18() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___rotation_18)); }
	inline FsmQuaternion_t4259038327 * get_rotation_18() const { return ___rotation_18; }
	inline FsmQuaternion_t4259038327 ** get_address_of_rotation_18() { return &___rotation_18; }
	inline void set_rotation_18(FsmQuaternion_t4259038327 * value)
	{
		___rotation_18 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_18), value);
	}

	inline static int32_t get_offset_of_positionWeight_19() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___positionWeight_19)); }
	inline FsmFloat_t2883254149 * get_positionWeight_19() const { return ___positionWeight_19; }
	inline FsmFloat_t2883254149 ** get_address_of_positionWeight_19() { return &___positionWeight_19; }
	inline void set_positionWeight_19(FsmFloat_t2883254149 * value)
	{
		___positionWeight_19 = value;
		Il2CppCodeGenWriteBarrier((&___positionWeight_19), value);
	}

	inline static int32_t get_offset_of_rotationWeight_20() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___rotationWeight_20)); }
	inline FsmFloat_t2883254149 * get_rotationWeight_20() const { return ___rotationWeight_20; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationWeight_20() { return &___rotationWeight_20; }
	inline void set_rotationWeight_20(FsmFloat_t2883254149 * value)
	{
		___rotationWeight_20 = value;
		Il2CppCodeGenWriteBarrier((&___rotationWeight_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of__animator_22() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ____animator_22)); }
	inline Animator_t434523843 * get__animator_22() const { return ____animator_22; }
	inline Animator_t434523843 ** get_address_of__animator_22() { return &____animator_22; }
	inline void set__animator_22(Animator_t434523843 * value)
	{
		____animator_22 = value;
		Il2CppCodeGenWriteBarrier((&____animator_22), value);
	}

	inline static int32_t get_offset_of__transform_23() { return static_cast<int32_t>(offsetof(SetAnimatorIKGoal_t839890372, ____transform_23)); }
	inline Transform_t3600365921 * get__transform_23() const { return ____transform_23; }
	inline Transform_t3600365921 ** get_address_of__transform_23() { return &____transform_23; }
	inline void set__transform_23(Transform_t3600365921 * value)
	{
		____transform_23 = value;
		Il2CppCodeGenWriteBarrier((&____transform_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORIKGOAL_T839890372_H
#ifndef SETANIMATORLAYERWEIGHT_T3884658471_H
#define SETANIMATORLAYERWEIGHT_T3884658471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight
struct  SetAnimatorLayerWeight_t3884658471  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::layerIndex
	FsmInt_t874273141 * ___layerIndex_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::layerWeight
	FsmFloat_t2883254149 * ___layerWeight_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::everyFrame
	bool ___everyFrame_17;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::_animator
	Animator_t434523843 * ____animator_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorLayerWeight_t3884658471, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_layerIndex_15() { return static_cast<int32_t>(offsetof(SetAnimatorLayerWeight_t3884658471, ___layerIndex_15)); }
	inline FsmInt_t874273141 * get_layerIndex_15() const { return ___layerIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_15() { return &___layerIndex_15; }
	inline void set_layerIndex_15(FsmInt_t874273141 * value)
	{
		___layerIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_15), value);
	}

	inline static int32_t get_offset_of_layerWeight_16() { return static_cast<int32_t>(offsetof(SetAnimatorLayerWeight_t3884658471, ___layerWeight_16)); }
	inline FsmFloat_t2883254149 * get_layerWeight_16() const { return ___layerWeight_16; }
	inline FsmFloat_t2883254149 ** get_address_of_layerWeight_16() { return &___layerWeight_16; }
	inline void set_layerWeight_16(FsmFloat_t2883254149 * value)
	{
		___layerWeight_16 = value;
		Il2CppCodeGenWriteBarrier((&___layerWeight_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetAnimatorLayerWeight_t3884658471, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of__animator_18() { return static_cast<int32_t>(offsetof(SetAnimatorLayerWeight_t3884658471, ____animator_18)); }
	inline Animator_t434523843 * get__animator_18() const { return ____animator_18; }
	inline Animator_t434523843 ** get_address_of__animator_18() { return &____animator_18; }
	inline void set__animator_18(Animator_t434523843 * value)
	{
		____animator_18 = value;
		Il2CppCodeGenWriteBarrier((&____animator_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORLAYERWEIGHT_T3884658471_H
#ifndef SETANIMATORLAYERSAFFECTMASSCENTER_T405418020_H
#define SETANIMATORLAYERSAFFECTMASSCENTER_T405418020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter
struct  SetAnimatorLayersAffectMassCenter_t405418020  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::affectMassCenter
	FsmBool_t163807967 * ___affectMassCenter_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorLayersAffectMassCenter_t405418020, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_affectMassCenter_15() { return static_cast<int32_t>(offsetof(SetAnimatorLayersAffectMassCenter_t405418020, ___affectMassCenter_15)); }
	inline FsmBool_t163807967 * get_affectMassCenter_15() const { return ___affectMassCenter_15; }
	inline FsmBool_t163807967 ** get_address_of_affectMassCenter_15() { return &___affectMassCenter_15; }
	inline void set_affectMassCenter_15(FsmBool_t163807967 * value)
	{
		___affectMassCenter_15 = value;
		Il2CppCodeGenWriteBarrier((&___affectMassCenter_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(SetAnimatorLayersAffectMassCenter_t405418020, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORLAYERSAFFECTMASSCENTER_T405418020_H
#ifndef SETANIMATORLOOKAT_T1141504004_H
#define SETANIMATORLOOKAT_T1141504004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorLookAt
struct  SetAnimatorLookAt_t1141504004  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorLookAt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetAnimatorLookAt::target
	FsmGameObject_t3581898942 * ___target_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetAnimatorLookAt::targetPosition
	FsmVector3_t626444517 * ___targetPosition_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::weight
	FsmFloat_t2883254149 * ___weight_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::bodyWeight
	FsmFloat_t2883254149 * ___bodyWeight_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::headWeight
	FsmFloat_t2883254149 * ___headWeight_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::eyesWeight
	FsmFloat_t2883254149 * ___eyesWeight_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorLookAt::clampWeight
	FsmFloat_t2883254149 * ___clampWeight_21;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorLookAt::everyFrame
	bool ___everyFrame_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorLookAt::_animator
	Animator_t434523843 * ____animator_23;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SetAnimatorLookAt::_transform
	Transform_t3600365921 * ____transform_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_target_15() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___target_15)); }
	inline FsmGameObject_t3581898942 * get_target_15() const { return ___target_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_target_15() { return &___target_15; }
	inline void set_target_15(FsmGameObject_t3581898942 * value)
	{
		___target_15 = value;
		Il2CppCodeGenWriteBarrier((&___target_15), value);
	}

	inline static int32_t get_offset_of_targetPosition_16() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___targetPosition_16)); }
	inline FsmVector3_t626444517 * get_targetPosition_16() const { return ___targetPosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_16() { return &___targetPosition_16; }
	inline void set_targetPosition_16(FsmVector3_t626444517 * value)
	{
		___targetPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_16), value);
	}

	inline static int32_t get_offset_of_weight_17() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___weight_17)); }
	inline FsmFloat_t2883254149 * get_weight_17() const { return ___weight_17; }
	inline FsmFloat_t2883254149 ** get_address_of_weight_17() { return &___weight_17; }
	inline void set_weight_17(FsmFloat_t2883254149 * value)
	{
		___weight_17 = value;
		Il2CppCodeGenWriteBarrier((&___weight_17), value);
	}

	inline static int32_t get_offset_of_bodyWeight_18() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___bodyWeight_18)); }
	inline FsmFloat_t2883254149 * get_bodyWeight_18() const { return ___bodyWeight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_bodyWeight_18() { return &___bodyWeight_18; }
	inline void set_bodyWeight_18(FsmFloat_t2883254149 * value)
	{
		___bodyWeight_18 = value;
		Il2CppCodeGenWriteBarrier((&___bodyWeight_18), value);
	}

	inline static int32_t get_offset_of_headWeight_19() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___headWeight_19)); }
	inline FsmFloat_t2883254149 * get_headWeight_19() const { return ___headWeight_19; }
	inline FsmFloat_t2883254149 ** get_address_of_headWeight_19() { return &___headWeight_19; }
	inline void set_headWeight_19(FsmFloat_t2883254149 * value)
	{
		___headWeight_19 = value;
		Il2CppCodeGenWriteBarrier((&___headWeight_19), value);
	}

	inline static int32_t get_offset_of_eyesWeight_20() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___eyesWeight_20)); }
	inline FsmFloat_t2883254149 * get_eyesWeight_20() const { return ___eyesWeight_20; }
	inline FsmFloat_t2883254149 ** get_address_of_eyesWeight_20() { return &___eyesWeight_20; }
	inline void set_eyesWeight_20(FsmFloat_t2883254149 * value)
	{
		___eyesWeight_20 = value;
		Il2CppCodeGenWriteBarrier((&___eyesWeight_20), value);
	}

	inline static int32_t get_offset_of_clampWeight_21() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___clampWeight_21)); }
	inline FsmFloat_t2883254149 * get_clampWeight_21() const { return ___clampWeight_21; }
	inline FsmFloat_t2883254149 ** get_address_of_clampWeight_21() { return &___clampWeight_21; }
	inline void set_clampWeight_21(FsmFloat_t2883254149 * value)
	{
		___clampWeight_21 = value;
		Il2CppCodeGenWriteBarrier((&___clampWeight_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ____animator_23)); }
	inline Animator_t434523843 * get__animator_23() const { return ____animator_23; }
	inline Animator_t434523843 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t434523843 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier((&____animator_23), value);
	}

	inline static int32_t get_offset_of__transform_24() { return static_cast<int32_t>(offsetof(SetAnimatorLookAt_t1141504004, ____transform_24)); }
	inline Transform_t3600365921 * get__transform_24() const { return ____transform_24; }
	inline Transform_t3600365921 ** get_address_of__transform_24() { return &____transform_24; }
	inline void set__transform_24(Transform_t3600365921 * value)
	{
		____transform_24 = value;
		Il2CppCodeGenWriteBarrier((&____transform_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORLOOKAT_T1141504004_H
#ifndef SETANIMATORPLAYBACKSPEED_T1779881157_H
#define SETANIMATORPLAYBACKSPEED_T1779881157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed
struct  SetAnimatorPlayBackSpeed_t1779881157  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::playBackSpeed
	FsmFloat_t2883254149 * ___playBackSpeed_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t1779881157, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_playBackSpeed_15() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t1779881157, ___playBackSpeed_15)); }
	inline FsmFloat_t2883254149 * get_playBackSpeed_15() const { return ___playBackSpeed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_playBackSpeed_15() { return &___playBackSpeed_15; }
	inline void set_playBackSpeed_15(FsmFloat_t2883254149 * value)
	{
		___playBackSpeed_15 = value;
		Il2CppCodeGenWriteBarrier((&___playBackSpeed_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t1779881157, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackSpeed_t1779881157, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORPLAYBACKSPEED_T1779881157_H
#ifndef SETANIMATORPLAYBACKTIME_T3352620600_H
#define SETANIMATORPLAYBACKTIME_T3352620600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime
struct  SetAnimatorPlayBackTime_t3352620600  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::playbackTime
	FsmFloat_t2883254149 * ___playbackTime_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackTime_t3352620600, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_playbackTime_15() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackTime_t3352620600, ___playbackTime_15)); }
	inline FsmFloat_t2883254149 * get_playbackTime_15() const { return ___playbackTime_15; }
	inline FsmFloat_t2883254149 ** get_address_of_playbackTime_15() { return &___playbackTime_15; }
	inline void set_playbackTime_15(FsmFloat_t2883254149 * value)
	{
		___playbackTime_15 = value;
		Il2CppCodeGenWriteBarrier((&___playbackTime_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackTime_t3352620600, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(SetAnimatorPlayBackTime_t3352620600, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORPLAYBACKTIME_T3352620600_H
#ifndef SETANIMATORSPEED_T185298894_H
#define SETANIMATORSPEED_T185298894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorSpeed
struct  SetAnimatorSpeed_t185298894  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorSpeed::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorSpeed::speed
	FsmFloat_t2883254149 * ___speed_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimatorSpeed::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorSpeed::_animator
	Animator_t434523843 * ____animator_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorSpeed_t185298894, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(SetAnimatorSpeed_t185298894, ___speed_15)); }
	inline FsmFloat_t2883254149 * get_speed_15() const { return ___speed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(FsmFloat_t2883254149 * value)
	{
		___speed_15 = value;
		Il2CppCodeGenWriteBarrier((&___speed_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetAnimatorSpeed_t185298894, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(SetAnimatorSpeed_t185298894, ____animator_17)); }
	inline Animator_t434523843 * get__animator_17() const { return ____animator_17; }
	inline Animator_t434523843 ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_t434523843 * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORSPEED_T185298894_H
#ifndef SETANIMATORSTABILIZEFEET_T2162779602_H
#define SETANIMATORSTABILIZEFEET_T2162779602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet
struct  SetAnimatorStabilizeFeet_t2162779602  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::stabilizeFeet
	FsmBool_t163807967 * ___stabilizeFeet_15;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::_animator
	Animator_t434523843 * ____animator_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetAnimatorStabilizeFeet_t2162779602, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_stabilizeFeet_15() { return static_cast<int32_t>(offsetof(SetAnimatorStabilizeFeet_t2162779602, ___stabilizeFeet_15)); }
	inline FsmBool_t163807967 * get_stabilizeFeet_15() const { return ___stabilizeFeet_15; }
	inline FsmBool_t163807967 ** get_address_of_stabilizeFeet_15() { return &___stabilizeFeet_15; }
	inline void set_stabilizeFeet_15(FsmBool_t163807967 * value)
	{
		___stabilizeFeet_15 = value;
		Il2CppCodeGenWriteBarrier((&___stabilizeFeet_15), value);
	}

	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(SetAnimatorStabilizeFeet_t2162779602, ____animator_16)); }
	inline Animator_t434523843 * get__animator_16() const { return ____animator_16; }
	inline Animator_t434523843 ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_t434523843 * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORSTABILIZEFEET_T2162779602_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ANIMATECOLOR_T143032445_H
#define ANIMATECOLOR_T143032445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateColor
struct  AnimateColor_t143032445  : public AnimateFsmAction_t2616519849
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.AnimateColor::colorVariable
	FsmColor_t1738900188 * ___colorVariable_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveR
	FsmAnimationCurve_t3432711236 * ___curveR_38;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationR
	int32_t ___calculationR_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveG
	FsmAnimationCurve_t3432711236 * ___curveG_40;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationG
	int32_t ___calculationG_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveB
	FsmAnimationCurve_t3432711236 * ___curveB_42;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationB
	int32_t ___calculationB_43;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateColor::curveA
	FsmAnimationCurve_t3432711236 * ___curveA_44;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateColor::calculationA
	int32_t ___calculationA_45;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateColor::finishInNextStep
	bool ___finishInNextStep_46;

public:
	inline static int32_t get_offset_of_colorVariable_37() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___colorVariable_37)); }
	inline FsmColor_t1738900188 * get_colorVariable_37() const { return ___colorVariable_37; }
	inline FsmColor_t1738900188 ** get_address_of_colorVariable_37() { return &___colorVariable_37; }
	inline void set_colorVariable_37(FsmColor_t1738900188 * value)
	{
		___colorVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariable_37), value);
	}

	inline static int32_t get_offset_of_curveR_38() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___curveR_38)); }
	inline FsmAnimationCurve_t3432711236 * get_curveR_38() const { return ___curveR_38; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveR_38() { return &___curveR_38; }
	inline void set_curveR_38(FsmAnimationCurve_t3432711236 * value)
	{
		___curveR_38 = value;
		Il2CppCodeGenWriteBarrier((&___curveR_38), value);
	}

	inline static int32_t get_offset_of_calculationR_39() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___calculationR_39)); }
	inline int32_t get_calculationR_39() const { return ___calculationR_39; }
	inline int32_t* get_address_of_calculationR_39() { return &___calculationR_39; }
	inline void set_calculationR_39(int32_t value)
	{
		___calculationR_39 = value;
	}

	inline static int32_t get_offset_of_curveG_40() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___curveG_40)); }
	inline FsmAnimationCurve_t3432711236 * get_curveG_40() const { return ___curveG_40; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveG_40() { return &___curveG_40; }
	inline void set_curveG_40(FsmAnimationCurve_t3432711236 * value)
	{
		___curveG_40 = value;
		Il2CppCodeGenWriteBarrier((&___curveG_40), value);
	}

	inline static int32_t get_offset_of_calculationG_41() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___calculationG_41)); }
	inline int32_t get_calculationG_41() const { return ___calculationG_41; }
	inline int32_t* get_address_of_calculationG_41() { return &___calculationG_41; }
	inline void set_calculationG_41(int32_t value)
	{
		___calculationG_41 = value;
	}

	inline static int32_t get_offset_of_curveB_42() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___curveB_42)); }
	inline FsmAnimationCurve_t3432711236 * get_curveB_42() const { return ___curveB_42; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveB_42() { return &___curveB_42; }
	inline void set_curveB_42(FsmAnimationCurve_t3432711236 * value)
	{
		___curveB_42 = value;
		Il2CppCodeGenWriteBarrier((&___curveB_42), value);
	}

	inline static int32_t get_offset_of_calculationB_43() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___calculationB_43)); }
	inline int32_t get_calculationB_43() const { return ___calculationB_43; }
	inline int32_t* get_address_of_calculationB_43() { return &___calculationB_43; }
	inline void set_calculationB_43(int32_t value)
	{
		___calculationB_43 = value;
	}

	inline static int32_t get_offset_of_curveA_44() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___curveA_44)); }
	inline FsmAnimationCurve_t3432711236 * get_curveA_44() const { return ___curveA_44; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveA_44() { return &___curveA_44; }
	inline void set_curveA_44(FsmAnimationCurve_t3432711236 * value)
	{
		___curveA_44 = value;
		Il2CppCodeGenWriteBarrier((&___curveA_44), value);
	}

	inline static int32_t get_offset_of_calculationA_45() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___calculationA_45)); }
	inline int32_t get_calculationA_45() const { return ___calculationA_45; }
	inline int32_t* get_address_of_calculationA_45() { return &___calculationA_45; }
	inline void set_calculationA_45(int32_t value)
	{
		___calculationA_45 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_46() { return static_cast<int32_t>(offsetof(AnimateColor_t143032445, ___finishInNextStep_46)); }
	inline bool get_finishInNextStep_46() const { return ___finishInNextStep_46; }
	inline bool* get_address_of_finishInNextStep_46() { return &___finishInNextStep_46; }
	inline void set_finishInNextStep_46(bool value)
	{
		___finishInNextStep_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATECOLOR_T143032445_H
#ifndef ANIMATEFLOATV2_T2936973654_H
#define ANIMATEFLOATV2_T2936973654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateFloatV2
struct  AnimateFloatV2_t2936973654  : public AnimateFsmAction_t2616519849
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimateFloatV2::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateFloatV2::animCurve
	FsmAnimationCurve_t3432711236 * ___animCurve_38;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateFloatV2::calculation
	int32_t ___calculation_39;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateFloatV2::finishInNextStep
	bool ___finishInNextStep_40;

public:
	inline static int32_t get_offset_of_floatVariable_37() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t2936973654, ___floatVariable_37)); }
	inline FsmFloat_t2883254149 * get_floatVariable_37() const { return ___floatVariable_37; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_37() { return &___floatVariable_37; }
	inline void set_floatVariable_37(FsmFloat_t2883254149 * value)
	{
		___floatVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_37), value);
	}

	inline static int32_t get_offset_of_animCurve_38() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t2936973654, ___animCurve_38)); }
	inline FsmAnimationCurve_t3432711236 * get_animCurve_38() const { return ___animCurve_38; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_animCurve_38() { return &___animCurve_38; }
	inline void set_animCurve_38(FsmAnimationCurve_t3432711236 * value)
	{
		___animCurve_38 = value;
		Il2CppCodeGenWriteBarrier((&___animCurve_38), value);
	}

	inline static int32_t get_offset_of_calculation_39() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t2936973654, ___calculation_39)); }
	inline int32_t get_calculation_39() const { return ___calculation_39; }
	inline int32_t* get_address_of_calculation_39() { return &___calculation_39; }
	inline void set_calculation_39(int32_t value)
	{
		___calculation_39 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_40() { return static_cast<int32_t>(offsetof(AnimateFloatV2_t2936973654, ___finishInNextStep_40)); }
	inline bool get_finishInNextStep_40() const { return ___finishInNextStep_40; }
	inline bool* get_address_of_finishInNextStep_40() { return &___finishInNextStep_40; }
	inline void set_finishInNextStep_40(bool value)
	{
		___finishInNextStep_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEFLOATV2_T2936973654_H
#ifndef ANIMATERECT_T3641165014_H
#define ANIMATERECT_T3641165014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateRect
struct  AnimateRect_t3641165014  : public AnimateFsmAction_t2616519849
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.AnimateRect::rectVariable
	FsmRect_t3649179877 * ___rectVariable_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateRect::curveX
	FsmAnimationCurve_t3432711236 * ___curveX_38;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateRect::calculationX
	int32_t ___calculationX_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateRect::curveY
	FsmAnimationCurve_t3432711236 * ___curveY_40;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateRect::calculationY
	int32_t ___calculationY_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateRect::curveW
	FsmAnimationCurve_t3432711236 * ___curveW_42;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateRect::calculationW
	int32_t ___calculationW_43;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateRect::curveH
	FsmAnimationCurve_t3432711236 * ___curveH_44;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateRect::calculationH
	int32_t ___calculationH_45;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateRect::finishInNextStep
	bool ___finishInNextStep_46;

public:
	inline static int32_t get_offset_of_rectVariable_37() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___rectVariable_37)); }
	inline FsmRect_t3649179877 * get_rectVariable_37() const { return ___rectVariable_37; }
	inline FsmRect_t3649179877 ** get_address_of_rectVariable_37() { return &___rectVariable_37; }
	inline void set_rectVariable_37(FsmRect_t3649179877 * value)
	{
		___rectVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariable_37), value);
	}

	inline static int32_t get_offset_of_curveX_38() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___curveX_38)); }
	inline FsmAnimationCurve_t3432711236 * get_curveX_38() const { return ___curveX_38; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveX_38() { return &___curveX_38; }
	inline void set_curveX_38(FsmAnimationCurve_t3432711236 * value)
	{
		___curveX_38 = value;
		Il2CppCodeGenWriteBarrier((&___curveX_38), value);
	}

	inline static int32_t get_offset_of_calculationX_39() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___calculationX_39)); }
	inline int32_t get_calculationX_39() const { return ___calculationX_39; }
	inline int32_t* get_address_of_calculationX_39() { return &___calculationX_39; }
	inline void set_calculationX_39(int32_t value)
	{
		___calculationX_39 = value;
	}

	inline static int32_t get_offset_of_curveY_40() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___curveY_40)); }
	inline FsmAnimationCurve_t3432711236 * get_curveY_40() const { return ___curveY_40; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveY_40() { return &___curveY_40; }
	inline void set_curveY_40(FsmAnimationCurve_t3432711236 * value)
	{
		___curveY_40 = value;
		Il2CppCodeGenWriteBarrier((&___curveY_40), value);
	}

	inline static int32_t get_offset_of_calculationY_41() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___calculationY_41)); }
	inline int32_t get_calculationY_41() const { return ___calculationY_41; }
	inline int32_t* get_address_of_calculationY_41() { return &___calculationY_41; }
	inline void set_calculationY_41(int32_t value)
	{
		___calculationY_41 = value;
	}

	inline static int32_t get_offset_of_curveW_42() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___curveW_42)); }
	inline FsmAnimationCurve_t3432711236 * get_curveW_42() const { return ___curveW_42; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveW_42() { return &___curveW_42; }
	inline void set_curveW_42(FsmAnimationCurve_t3432711236 * value)
	{
		___curveW_42 = value;
		Il2CppCodeGenWriteBarrier((&___curveW_42), value);
	}

	inline static int32_t get_offset_of_calculationW_43() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___calculationW_43)); }
	inline int32_t get_calculationW_43() const { return ___calculationW_43; }
	inline int32_t* get_address_of_calculationW_43() { return &___calculationW_43; }
	inline void set_calculationW_43(int32_t value)
	{
		___calculationW_43 = value;
	}

	inline static int32_t get_offset_of_curveH_44() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___curveH_44)); }
	inline FsmAnimationCurve_t3432711236 * get_curveH_44() const { return ___curveH_44; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveH_44() { return &___curveH_44; }
	inline void set_curveH_44(FsmAnimationCurve_t3432711236 * value)
	{
		___curveH_44 = value;
		Il2CppCodeGenWriteBarrier((&___curveH_44), value);
	}

	inline static int32_t get_offset_of_calculationH_45() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___calculationH_45)); }
	inline int32_t get_calculationH_45() const { return ___calculationH_45; }
	inline int32_t* get_address_of_calculationH_45() { return &___calculationH_45; }
	inline void set_calculationH_45(int32_t value)
	{
		___calculationH_45 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_46() { return static_cast<int32_t>(offsetof(AnimateRect_t3641165014, ___finishInNextStep_46)); }
	inline bool get_finishInNextStep_46() const { return ___finishInNextStep_46; }
	inline bool* get_address_of_finishInNextStep_46() { return &___finishInNextStep_46; }
	inline void set_finishInNextStep_46(bool value)
	{
		___finishInNextStep_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATERECT_T3641165014_H
#ifndef ANIMATEVECTOR3_T2247115148_H
#define ANIMATEVECTOR3_T2247115148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimateVector3
struct  AnimateVector3_t2247115148  : public AnimateFsmAction_t2616519849
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AnimateVector3::vectorVariable
	FsmVector3_t626444517 * ___vectorVariable_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateVector3::curveX
	FsmAnimationCurve_t3432711236 * ___curveX_38;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateVector3::calculationX
	int32_t ___calculationX_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateVector3::curveY
	FsmAnimationCurve_t3432711236 * ___curveY_40;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateVector3::calculationY
	int32_t ___calculationY_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.AnimateVector3::curveZ
	FsmAnimationCurve_t3432711236 * ___curveZ_42;
	// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation HutongGames.PlayMaker.Actions.AnimateVector3::calculationZ
	int32_t ___calculationZ_43;
	// System.Boolean HutongGames.PlayMaker.Actions.AnimateVector3::finishInNextStep
	bool ___finishInNextStep_44;

public:
	inline static int32_t get_offset_of_vectorVariable_37() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___vectorVariable_37)); }
	inline FsmVector3_t626444517 * get_vectorVariable_37() const { return ___vectorVariable_37; }
	inline FsmVector3_t626444517 ** get_address_of_vectorVariable_37() { return &___vectorVariable_37; }
	inline void set_vectorVariable_37(FsmVector3_t626444517 * value)
	{
		___vectorVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___vectorVariable_37), value);
	}

	inline static int32_t get_offset_of_curveX_38() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___curveX_38)); }
	inline FsmAnimationCurve_t3432711236 * get_curveX_38() const { return ___curveX_38; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveX_38() { return &___curveX_38; }
	inline void set_curveX_38(FsmAnimationCurve_t3432711236 * value)
	{
		___curveX_38 = value;
		Il2CppCodeGenWriteBarrier((&___curveX_38), value);
	}

	inline static int32_t get_offset_of_calculationX_39() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___calculationX_39)); }
	inline int32_t get_calculationX_39() const { return ___calculationX_39; }
	inline int32_t* get_address_of_calculationX_39() { return &___calculationX_39; }
	inline void set_calculationX_39(int32_t value)
	{
		___calculationX_39 = value;
	}

	inline static int32_t get_offset_of_curveY_40() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___curveY_40)); }
	inline FsmAnimationCurve_t3432711236 * get_curveY_40() const { return ___curveY_40; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveY_40() { return &___curveY_40; }
	inline void set_curveY_40(FsmAnimationCurve_t3432711236 * value)
	{
		___curveY_40 = value;
		Il2CppCodeGenWriteBarrier((&___curveY_40), value);
	}

	inline static int32_t get_offset_of_calculationY_41() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___calculationY_41)); }
	inline int32_t get_calculationY_41() const { return ___calculationY_41; }
	inline int32_t* get_address_of_calculationY_41() { return &___calculationY_41; }
	inline void set_calculationY_41(int32_t value)
	{
		___calculationY_41 = value;
	}

	inline static int32_t get_offset_of_curveZ_42() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___curveZ_42)); }
	inline FsmAnimationCurve_t3432711236 * get_curveZ_42() const { return ___curveZ_42; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveZ_42() { return &___curveZ_42; }
	inline void set_curveZ_42(FsmAnimationCurve_t3432711236 * value)
	{
		___curveZ_42 = value;
		Il2CppCodeGenWriteBarrier((&___curveZ_42), value);
	}

	inline static int32_t get_offset_of_calculationZ_43() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___calculationZ_43)); }
	inline int32_t get_calculationZ_43() const { return ___calculationZ_43; }
	inline int32_t* get_address_of_calculationZ_43() { return &___calculationZ_43; }
	inline void set_calculationZ_43(int32_t value)
	{
		___calculationZ_43 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_44() { return static_cast<int32_t>(offsetof(AnimateVector3_t2247115148, ___finishInNextStep_44)); }
	inline bool get_finishInNextStep_44() const { return ___finishInNextStep_44; }
	inline bool* get_address_of_finishInNextStep_44() { return &___finishInNextStep_44; }
	inline void set_finishInNextStep_44(bool value)
	{
		___finishInNextStep_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEVECTOR3_T2247115148_H
#ifndef BASEANIMATIONACTION_T1134898484_H
#define BASEANIMATIONACTION_T1134898484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseAnimationAction
struct  BaseAnimationAction_t1134898484  : public ComponentAction_1_t2230786787
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEANIMATIONACTION_T1134898484_H
#ifndef CURVECOLOR_T2104014689_H
#define CURVECOLOR_T2104014689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveColor
struct  CurveColor_t2104014689  : public CurveFsmAction_t928230875
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CurveColor::colorVariable
	FsmColor_t1738900188 * ___colorVariable_38;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CurveColor::fromValue
	FsmColor_t1738900188 * ___fromValue_39;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.CurveColor::toValue
	FsmColor_t1738900188 * ___toValue_40;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveR
	FsmAnimationCurve_t3432711236 * ___curveR_41;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationR
	int32_t ___calculationR_42;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveG
	FsmAnimationCurve_t3432711236 * ___curveG_43;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationG
	int32_t ___calculationG_44;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveB
	FsmAnimationCurve_t3432711236 * ___curveB_45;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationB
	int32_t ___calculationB_46;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveColor::curveA
	FsmAnimationCurve_t3432711236 * ___curveA_47;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveColor::calculationA
	int32_t ___calculationA_48;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.CurveColor::clr
	Color_t2555686324  ___clr_49;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveColor::finishInNextStep
	bool ___finishInNextStep_50;

public:
	inline static int32_t get_offset_of_colorVariable_38() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___colorVariable_38)); }
	inline FsmColor_t1738900188 * get_colorVariable_38() const { return ___colorVariable_38; }
	inline FsmColor_t1738900188 ** get_address_of_colorVariable_38() { return &___colorVariable_38; }
	inline void set_colorVariable_38(FsmColor_t1738900188 * value)
	{
		___colorVariable_38 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariable_38), value);
	}

	inline static int32_t get_offset_of_fromValue_39() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___fromValue_39)); }
	inline FsmColor_t1738900188 * get_fromValue_39() const { return ___fromValue_39; }
	inline FsmColor_t1738900188 ** get_address_of_fromValue_39() { return &___fromValue_39; }
	inline void set_fromValue_39(FsmColor_t1738900188 * value)
	{
		___fromValue_39 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_39), value);
	}

	inline static int32_t get_offset_of_toValue_40() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___toValue_40)); }
	inline FsmColor_t1738900188 * get_toValue_40() const { return ___toValue_40; }
	inline FsmColor_t1738900188 ** get_address_of_toValue_40() { return &___toValue_40; }
	inline void set_toValue_40(FsmColor_t1738900188 * value)
	{
		___toValue_40 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_40), value);
	}

	inline static int32_t get_offset_of_curveR_41() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___curveR_41)); }
	inline FsmAnimationCurve_t3432711236 * get_curveR_41() const { return ___curveR_41; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveR_41() { return &___curveR_41; }
	inline void set_curveR_41(FsmAnimationCurve_t3432711236 * value)
	{
		___curveR_41 = value;
		Il2CppCodeGenWriteBarrier((&___curveR_41), value);
	}

	inline static int32_t get_offset_of_calculationR_42() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___calculationR_42)); }
	inline int32_t get_calculationR_42() const { return ___calculationR_42; }
	inline int32_t* get_address_of_calculationR_42() { return &___calculationR_42; }
	inline void set_calculationR_42(int32_t value)
	{
		___calculationR_42 = value;
	}

	inline static int32_t get_offset_of_curveG_43() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___curveG_43)); }
	inline FsmAnimationCurve_t3432711236 * get_curveG_43() const { return ___curveG_43; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveG_43() { return &___curveG_43; }
	inline void set_curveG_43(FsmAnimationCurve_t3432711236 * value)
	{
		___curveG_43 = value;
		Il2CppCodeGenWriteBarrier((&___curveG_43), value);
	}

	inline static int32_t get_offset_of_calculationG_44() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___calculationG_44)); }
	inline int32_t get_calculationG_44() const { return ___calculationG_44; }
	inline int32_t* get_address_of_calculationG_44() { return &___calculationG_44; }
	inline void set_calculationG_44(int32_t value)
	{
		___calculationG_44 = value;
	}

	inline static int32_t get_offset_of_curveB_45() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___curveB_45)); }
	inline FsmAnimationCurve_t3432711236 * get_curveB_45() const { return ___curveB_45; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveB_45() { return &___curveB_45; }
	inline void set_curveB_45(FsmAnimationCurve_t3432711236 * value)
	{
		___curveB_45 = value;
		Il2CppCodeGenWriteBarrier((&___curveB_45), value);
	}

	inline static int32_t get_offset_of_calculationB_46() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___calculationB_46)); }
	inline int32_t get_calculationB_46() const { return ___calculationB_46; }
	inline int32_t* get_address_of_calculationB_46() { return &___calculationB_46; }
	inline void set_calculationB_46(int32_t value)
	{
		___calculationB_46 = value;
	}

	inline static int32_t get_offset_of_curveA_47() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___curveA_47)); }
	inline FsmAnimationCurve_t3432711236 * get_curveA_47() const { return ___curveA_47; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveA_47() { return &___curveA_47; }
	inline void set_curveA_47(FsmAnimationCurve_t3432711236 * value)
	{
		___curveA_47 = value;
		Il2CppCodeGenWriteBarrier((&___curveA_47), value);
	}

	inline static int32_t get_offset_of_calculationA_48() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___calculationA_48)); }
	inline int32_t get_calculationA_48() const { return ___calculationA_48; }
	inline int32_t* get_address_of_calculationA_48() { return &___calculationA_48; }
	inline void set_calculationA_48(int32_t value)
	{
		___calculationA_48 = value;
	}

	inline static int32_t get_offset_of_clr_49() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___clr_49)); }
	inline Color_t2555686324  get_clr_49() const { return ___clr_49; }
	inline Color_t2555686324 * get_address_of_clr_49() { return &___clr_49; }
	inline void set_clr_49(Color_t2555686324  value)
	{
		___clr_49 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_50() { return static_cast<int32_t>(offsetof(CurveColor_t2104014689, ___finishInNextStep_50)); }
	inline bool get_finishInNextStep_50() const { return ___finishInNextStep_50; }
	inline bool* get_address_of_finishInNextStep_50() { return &___finishInNextStep_50; }
	inline void set_finishInNextStep_50(bool value)
	{
		___finishInNextStep_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECOLOR_T2104014689_H
#ifndef CURVEFLOAT_T2152879844_H
#define CURVEFLOAT_T2152879844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveFloat
struct  CurveFloat_t2152879844  : public CurveFsmAction_t928230875
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFloat::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_38;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFloat::fromValue
	FsmFloat_t2883254149 * ___fromValue_39;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CurveFloat::toValue
	FsmFloat_t2883254149 * ___toValue_40;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveFloat::animCurve
	FsmAnimationCurve_t3432711236 * ___animCurve_41;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveFloat::calculation
	int32_t ___calculation_42;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveFloat::finishInNextStep
	bool ___finishInNextStep_43;

public:
	inline static int32_t get_offset_of_floatVariable_38() { return static_cast<int32_t>(offsetof(CurveFloat_t2152879844, ___floatVariable_38)); }
	inline FsmFloat_t2883254149 * get_floatVariable_38() const { return ___floatVariable_38; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_38() { return &___floatVariable_38; }
	inline void set_floatVariable_38(FsmFloat_t2883254149 * value)
	{
		___floatVariable_38 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_38), value);
	}

	inline static int32_t get_offset_of_fromValue_39() { return static_cast<int32_t>(offsetof(CurveFloat_t2152879844, ___fromValue_39)); }
	inline FsmFloat_t2883254149 * get_fromValue_39() const { return ___fromValue_39; }
	inline FsmFloat_t2883254149 ** get_address_of_fromValue_39() { return &___fromValue_39; }
	inline void set_fromValue_39(FsmFloat_t2883254149 * value)
	{
		___fromValue_39 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_39), value);
	}

	inline static int32_t get_offset_of_toValue_40() { return static_cast<int32_t>(offsetof(CurveFloat_t2152879844, ___toValue_40)); }
	inline FsmFloat_t2883254149 * get_toValue_40() const { return ___toValue_40; }
	inline FsmFloat_t2883254149 ** get_address_of_toValue_40() { return &___toValue_40; }
	inline void set_toValue_40(FsmFloat_t2883254149 * value)
	{
		___toValue_40 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_40), value);
	}

	inline static int32_t get_offset_of_animCurve_41() { return static_cast<int32_t>(offsetof(CurveFloat_t2152879844, ___animCurve_41)); }
	inline FsmAnimationCurve_t3432711236 * get_animCurve_41() const { return ___animCurve_41; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_animCurve_41() { return &___animCurve_41; }
	inline void set_animCurve_41(FsmAnimationCurve_t3432711236 * value)
	{
		___animCurve_41 = value;
		Il2CppCodeGenWriteBarrier((&___animCurve_41), value);
	}

	inline static int32_t get_offset_of_calculation_42() { return static_cast<int32_t>(offsetof(CurveFloat_t2152879844, ___calculation_42)); }
	inline int32_t get_calculation_42() const { return ___calculation_42; }
	inline int32_t* get_address_of_calculation_42() { return &___calculation_42; }
	inline void set_calculation_42(int32_t value)
	{
		___calculation_42 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_43() { return static_cast<int32_t>(offsetof(CurveFloat_t2152879844, ___finishInNextStep_43)); }
	inline bool get_finishInNextStep_43() const { return ___finishInNextStep_43; }
	inline bool* get_address_of_finishInNextStep_43() { return &___finishInNextStep_43; }
	inline void set_finishInNextStep_43(bool value)
	{
		___finishInNextStep_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEFLOAT_T2152879844_H
#ifndef CURVERECT_T119892750_H
#define CURVERECT_T119892750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveRect
struct  CurveRect_t119892750  : public CurveFsmAction_t928230875
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::rectVariable
	FsmRect_t3649179877 * ___rectVariable_38;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::fromValue
	FsmRect_t3649179877 * ___fromValue_39;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::toValue
	FsmRect_t3649179877 * ___toValue_40;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveX
	FsmAnimationCurve_t3432711236 * ___curveX_41;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationX
	int32_t ___calculationX_42;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveY
	FsmAnimationCurve_t3432711236 * ___curveY_43;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationY
	int32_t ___calculationY_44;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveW
	FsmAnimationCurve_t3432711236 * ___curveW_45;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationW
	int32_t ___calculationW_46;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveH
	FsmAnimationCurve_t3432711236 * ___curveH_47;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationH
	int32_t ___calculationH_48;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.CurveRect::rct
	Rect_t2360479859  ___rct_49;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveRect::finishInNextStep
	bool ___finishInNextStep_50;

public:
	inline static int32_t get_offset_of_rectVariable_38() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___rectVariable_38)); }
	inline FsmRect_t3649179877 * get_rectVariable_38() const { return ___rectVariable_38; }
	inline FsmRect_t3649179877 ** get_address_of_rectVariable_38() { return &___rectVariable_38; }
	inline void set_rectVariable_38(FsmRect_t3649179877 * value)
	{
		___rectVariable_38 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariable_38), value);
	}

	inline static int32_t get_offset_of_fromValue_39() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___fromValue_39)); }
	inline FsmRect_t3649179877 * get_fromValue_39() const { return ___fromValue_39; }
	inline FsmRect_t3649179877 ** get_address_of_fromValue_39() { return &___fromValue_39; }
	inline void set_fromValue_39(FsmRect_t3649179877 * value)
	{
		___fromValue_39 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_39), value);
	}

	inline static int32_t get_offset_of_toValue_40() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___toValue_40)); }
	inline FsmRect_t3649179877 * get_toValue_40() const { return ___toValue_40; }
	inline FsmRect_t3649179877 ** get_address_of_toValue_40() { return &___toValue_40; }
	inline void set_toValue_40(FsmRect_t3649179877 * value)
	{
		___toValue_40 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_40), value);
	}

	inline static int32_t get_offset_of_curveX_41() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___curveX_41)); }
	inline FsmAnimationCurve_t3432711236 * get_curveX_41() const { return ___curveX_41; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveX_41() { return &___curveX_41; }
	inline void set_curveX_41(FsmAnimationCurve_t3432711236 * value)
	{
		___curveX_41 = value;
		Il2CppCodeGenWriteBarrier((&___curveX_41), value);
	}

	inline static int32_t get_offset_of_calculationX_42() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___calculationX_42)); }
	inline int32_t get_calculationX_42() const { return ___calculationX_42; }
	inline int32_t* get_address_of_calculationX_42() { return &___calculationX_42; }
	inline void set_calculationX_42(int32_t value)
	{
		___calculationX_42 = value;
	}

	inline static int32_t get_offset_of_curveY_43() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___curveY_43)); }
	inline FsmAnimationCurve_t3432711236 * get_curveY_43() const { return ___curveY_43; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveY_43() { return &___curveY_43; }
	inline void set_curveY_43(FsmAnimationCurve_t3432711236 * value)
	{
		___curveY_43 = value;
		Il2CppCodeGenWriteBarrier((&___curveY_43), value);
	}

	inline static int32_t get_offset_of_calculationY_44() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___calculationY_44)); }
	inline int32_t get_calculationY_44() const { return ___calculationY_44; }
	inline int32_t* get_address_of_calculationY_44() { return &___calculationY_44; }
	inline void set_calculationY_44(int32_t value)
	{
		___calculationY_44 = value;
	}

	inline static int32_t get_offset_of_curveW_45() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___curveW_45)); }
	inline FsmAnimationCurve_t3432711236 * get_curveW_45() const { return ___curveW_45; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveW_45() { return &___curveW_45; }
	inline void set_curveW_45(FsmAnimationCurve_t3432711236 * value)
	{
		___curveW_45 = value;
		Il2CppCodeGenWriteBarrier((&___curveW_45), value);
	}

	inline static int32_t get_offset_of_calculationW_46() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___calculationW_46)); }
	inline int32_t get_calculationW_46() const { return ___calculationW_46; }
	inline int32_t* get_address_of_calculationW_46() { return &___calculationW_46; }
	inline void set_calculationW_46(int32_t value)
	{
		___calculationW_46 = value;
	}

	inline static int32_t get_offset_of_curveH_47() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___curveH_47)); }
	inline FsmAnimationCurve_t3432711236 * get_curveH_47() const { return ___curveH_47; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveH_47() { return &___curveH_47; }
	inline void set_curveH_47(FsmAnimationCurve_t3432711236 * value)
	{
		___curveH_47 = value;
		Il2CppCodeGenWriteBarrier((&___curveH_47), value);
	}

	inline static int32_t get_offset_of_calculationH_48() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___calculationH_48)); }
	inline int32_t get_calculationH_48() const { return ___calculationH_48; }
	inline int32_t* get_address_of_calculationH_48() { return &___calculationH_48; }
	inline void set_calculationH_48(int32_t value)
	{
		___calculationH_48 = value;
	}

	inline static int32_t get_offset_of_rct_49() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___rct_49)); }
	inline Rect_t2360479859  get_rct_49() const { return ___rct_49; }
	inline Rect_t2360479859 * get_address_of_rct_49() { return &___rct_49; }
	inline void set_rct_49(Rect_t2360479859  value)
	{
		___rct_49 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_50() { return static_cast<int32_t>(offsetof(CurveRect_t119892750, ___finishInNextStep_50)); }
	inline bool get_finishInNextStep_50() const { return ___finishInNextStep_50; }
	inline bool* get_address_of_finishInNextStep_50() { return &___finishInNextStep_50; }
	inline void set_finishInNextStep_50(bool value)
	{
		___finishInNextStep_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVERECT_T119892750_H
#ifndef CURVEVECTOR3_T247899229_H
#define CURVEVECTOR3_T247899229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveVector3
struct  CurveVector3_t247899229  : public CurveFsmAction_t928230875
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::vectorVariable
	FsmVector3_t626444517 * ___vectorVariable_38;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::fromValue
	FsmVector3_t626444517 * ___fromValue_39;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CurveVector3::toValue
	FsmVector3_t626444517 * ___toValue_40;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveX
	FsmAnimationCurve_t3432711236 * ___curveX_41;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationX
	int32_t ___calculationX_42;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveY
	FsmAnimationCurve_t3432711236 * ___curveY_43;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationY
	int32_t ___calculationY_44;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveVector3::curveZ
	FsmAnimationCurve_t3432711236 * ___curveZ_45;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveVector3::calculationZ
	int32_t ___calculationZ_46;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.CurveVector3::vct
	Vector3_t3722313464  ___vct_47;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveVector3::finishInNextStep
	bool ___finishInNextStep_48;

public:
	inline static int32_t get_offset_of_vectorVariable_38() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___vectorVariable_38)); }
	inline FsmVector3_t626444517 * get_vectorVariable_38() const { return ___vectorVariable_38; }
	inline FsmVector3_t626444517 ** get_address_of_vectorVariable_38() { return &___vectorVariable_38; }
	inline void set_vectorVariable_38(FsmVector3_t626444517 * value)
	{
		___vectorVariable_38 = value;
		Il2CppCodeGenWriteBarrier((&___vectorVariable_38), value);
	}

	inline static int32_t get_offset_of_fromValue_39() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___fromValue_39)); }
	inline FsmVector3_t626444517 * get_fromValue_39() const { return ___fromValue_39; }
	inline FsmVector3_t626444517 ** get_address_of_fromValue_39() { return &___fromValue_39; }
	inline void set_fromValue_39(FsmVector3_t626444517 * value)
	{
		___fromValue_39 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_39), value);
	}

	inline static int32_t get_offset_of_toValue_40() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___toValue_40)); }
	inline FsmVector3_t626444517 * get_toValue_40() const { return ___toValue_40; }
	inline FsmVector3_t626444517 ** get_address_of_toValue_40() { return &___toValue_40; }
	inline void set_toValue_40(FsmVector3_t626444517 * value)
	{
		___toValue_40 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_40), value);
	}

	inline static int32_t get_offset_of_curveX_41() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___curveX_41)); }
	inline FsmAnimationCurve_t3432711236 * get_curveX_41() const { return ___curveX_41; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveX_41() { return &___curveX_41; }
	inline void set_curveX_41(FsmAnimationCurve_t3432711236 * value)
	{
		___curveX_41 = value;
		Il2CppCodeGenWriteBarrier((&___curveX_41), value);
	}

	inline static int32_t get_offset_of_calculationX_42() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___calculationX_42)); }
	inline int32_t get_calculationX_42() const { return ___calculationX_42; }
	inline int32_t* get_address_of_calculationX_42() { return &___calculationX_42; }
	inline void set_calculationX_42(int32_t value)
	{
		___calculationX_42 = value;
	}

	inline static int32_t get_offset_of_curveY_43() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___curveY_43)); }
	inline FsmAnimationCurve_t3432711236 * get_curveY_43() const { return ___curveY_43; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveY_43() { return &___curveY_43; }
	inline void set_curveY_43(FsmAnimationCurve_t3432711236 * value)
	{
		___curveY_43 = value;
		Il2CppCodeGenWriteBarrier((&___curveY_43), value);
	}

	inline static int32_t get_offset_of_calculationY_44() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___calculationY_44)); }
	inline int32_t get_calculationY_44() const { return ___calculationY_44; }
	inline int32_t* get_address_of_calculationY_44() { return &___calculationY_44; }
	inline void set_calculationY_44(int32_t value)
	{
		___calculationY_44 = value;
	}

	inline static int32_t get_offset_of_curveZ_45() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___curveZ_45)); }
	inline FsmAnimationCurve_t3432711236 * get_curveZ_45() const { return ___curveZ_45; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curveZ_45() { return &___curveZ_45; }
	inline void set_curveZ_45(FsmAnimationCurve_t3432711236 * value)
	{
		___curveZ_45 = value;
		Il2CppCodeGenWriteBarrier((&___curveZ_45), value);
	}

	inline static int32_t get_offset_of_calculationZ_46() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___calculationZ_46)); }
	inline int32_t get_calculationZ_46() const { return ___calculationZ_46; }
	inline int32_t* get_address_of_calculationZ_46() { return &___calculationZ_46; }
	inline void set_calculationZ_46(int32_t value)
	{
		___calculationZ_46 = value;
	}

	inline static int32_t get_offset_of_vct_47() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___vct_47)); }
	inline Vector3_t3722313464  get_vct_47() const { return ___vct_47; }
	inline Vector3_t3722313464 * get_address_of_vct_47() { return &___vct_47; }
	inline void set_vct_47(Vector3_t3722313464  value)
	{
		___vct_47 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_48() { return static_cast<int32_t>(offsetof(CurveVector3_t247899229, ___finishInNextStep_48)); }
	inline bool get_finishInNextStep_48() const { return ___finishInNextStep_48; }
	inline bool* get_address_of_finishInNextStep_48() { return &___finishInNextStep_48; }
	inline void set_finishInNextStep_48(bool value)
	{
		___finishInNextStep_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEVECTOR3_T247899229_H
#ifndef EASECOLOR_T1835015595_H
#define EASECOLOR_T1835015595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseColor
struct  EaseColor_t1835015595  : public EaseFsmAction_t2585076379
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.EaseColor::fromValue
	FsmColor_t1738900188 * ___fromValue_35;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.EaseColor::toValue
	FsmColor_t1738900188 * ___toValue_36;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.EaseColor::colorVariable
	FsmColor_t1738900188 * ___colorVariable_37;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseColor::finishInNextStep
	bool ___finishInNextStep_38;

public:
	inline static int32_t get_offset_of_fromValue_35() { return static_cast<int32_t>(offsetof(EaseColor_t1835015595, ___fromValue_35)); }
	inline FsmColor_t1738900188 * get_fromValue_35() const { return ___fromValue_35; }
	inline FsmColor_t1738900188 ** get_address_of_fromValue_35() { return &___fromValue_35; }
	inline void set_fromValue_35(FsmColor_t1738900188 * value)
	{
		___fromValue_35 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_35), value);
	}

	inline static int32_t get_offset_of_toValue_36() { return static_cast<int32_t>(offsetof(EaseColor_t1835015595, ___toValue_36)); }
	inline FsmColor_t1738900188 * get_toValue_36() const { return ___toValue_36; }
	inline FsmColor_t1738900188 ** get_address_of_toValue_36() { return &___toValue_36; }
	inline void set_toValue_36(FsmColor_t1738900188 * value)
	{
		___toValue_36 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_36), value);
	}

	inline static int32_t get_offset_of_colorVariable_37() { return static_cast<int32_t>(offsetof(EaseColor_t1835015595, ___colorVariable_37)); }
	inline FsmColor_t1738900188 * get_colorVariable_37() const { return ___colorVariable_37; }
	inline FsmColor_t1738900188 ** get_address_of_colorVariable_37() { return &___colorVariable_37; }
	inline void set_colorVariable_37(FsmColor_t1738900188 * value)
	{
		___colorVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariable_37), value);
	}

	inline static int32_t get_offset_of_finishInNextStep_38() { return static_cast<int32_t>(offsetof(EaseColor_t1835015595, ___finishInNextStep_38)); }
	inline bool get_finishInNextStep_38() const { return ___finishInNextStep_38; }
	inline bool* get_address_of_finishInNextStep_38() { return &___finishInNextStep_38; }
	inline void set_finishInNextStep_38(bool value)
	{
		___finishInNextStep_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASECOLOR_T1835015595_H
#ifndef EASEFLOAT_T3247924017_H
#define EASEFLOAT_T3247924017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFloat
struct  EaseFloat_t3247924017  : public EaseFsmAction_t2585076379
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFloat::fromValue
	FsmFloat_t2883254149 * ___fromValue_35;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFloat::toValue
	FsmFloat_t2883254149 * ___toValue_36;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFloat::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_37;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFloat::finishInNextStep
	bool ___finishInNextStep_38;

public:
	inline static int32_t get_offset_of_fromValue_35() { return static_cast<int32_t>(offsetof(EaseFloat_t3247924017, ___fromValue_35)); }
	inline FsmFloat_t2883254149 * get_fromValue_35() const { return ___fromValue_35; }
	inline FsmFloat_t2883254149 ** get_address_of_fromValue_35() { return &___fromValue_35; }
	inline void set_fromValue_35(FsmFloat_t2883254149 * value)
	{
		___fromValue_35 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_35), value);
	}

	inline static int32_t get_offset_of_toValue_36() { return static_cast<int32_t>(offsetof(EaseFloat_t3247924017, ___toValue_36)); }
	inline FsmFloat_t2883254149 * get_toValue_36() const { return ___toValue_36; }
	inline FsmFloat_t2883254149 ** get_address_of_toValue_36() { return &___toValue_36; }
	inline void set_toValue_36(FsmFloat_t2883254149 * value)
	{
		___toValue_36 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_36), value);
	}

	inline static int32_t get_offset_of_floatVariable_37() { return static_cast<int32_t>(offsetof(EaseFloat_t3247924017, ___floatVariable_37)); }
	inline FsmFloat_t2883254149 * get_floatVariable_37() const { return ___floatVariable_37; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_37() { return &___floatVariable_37; }
	inline void set_floatVariable_37(FsmFloat_t2883254149 * value)
	{
		___floatVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_37), value);
	}

	inline static int32_t get_offset_of_finishInNextStep_38() { return static_cast<int32_t>(offsetof(EaseFloat_t3247924017, ___finishInNextStep_38)); }
	inline bool get_finishInNextStep_38() const { return ___finishInNextStep_38; }
	inline bool* get_address_of_finishInNextStep_38() { return &___finishInNextStep_38; }
	inline void set_finishInNextStep_38(bool value)
	{
		___finishInNextStep_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFLOAT_T3247924017_H
#ifndef EASINGFUNCTION_T338733390_H
#define EASINGFUNCTION_T338733390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction
struct  EasingFunction_t338733390  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGFUNCTION_T338733390_H
#ifndef EASERECT_T3338712696_H
#define EASERECT_T3338712696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseRect
struct  EaseRect_t3338712696  : public EaseFsmAction_t2585076379
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.EaseRect::fromValue
	FsmRect_t3649179877 * ___fromValue_35;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.EaseRect::toValue
	FsmRect_t3649179877 * ___toValue_36;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.EaseRect::rectVariable
	FsmRect_t3649179877 * ___rectVariable_37;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseRect::finishInNextStep
	bool ___finishInNextStep_38;

public:
	inline static int32_t get_offset_of_fromValue_35() { return static_cast<int32_t>(offsetof(EaseRect_t3338712696, ___fromValue_35)); }
	inline FsmRect_t3649179877 * get_fromValue_35() const { return ___fromValue_35; }
	inline FsmRect_t3649179877 ** get_address_of_fromValue_35() { return &___fromValue_35; }
	inline void set_fromValue_35(FsmRect_t3649179877 * value)
	{
		___fromValue_35 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_35), value);
	}

	inline static int32_t get_offset_of_toValue_36() { return static_cast<int32_t>(offsetof(EaseRect_t3338712696, ___toValue_36)); }
	inline FsmRect_t3649179877 * get_toValue_36() const { return ___toValue_36; }
	inline FsmRect_t3649179877 ** get_address_of_toValue_36() { return &___toValue_36; }
	inline void set_toValue_36(FsmRect_t3649179877 * value)
	{
		___toValue_36 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_36), value);
	}

	inline static int32_t get_offset_of_rectVariable_37() { return static_cast<int32_t>(offsetof(EaseRect_t3338712696, ___rectVariable_37)); }
	inline FsmRect_t3649179877 * get_rectVariable_37() const { return ___rectVariable_37; }
	inline FsmRect_t3649179877 ** get_address_of_rectVariable_37() { return &___rectVariable_37; }
	inline void set_rectVariable_37(FsmRect_t3649179877 * value)
	{
		___rectVariable_37 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariable_37), value);
	}

	inline static int32_t get_offset_of_finishInNextStep_38() { return static_cast<int32_t>(offsetof(EaseRect_t3338712696, ___finishInNextStep_38)); }
	inline bool get_finishInNextStep_38() const { return ___finishInNextStep_38; }
	inline bool* get_address_of_finishInNextStep_38() { return &___finishInNextStep_38; }
	inline void set_finishInNextStep_38(bool value)
	{
		___finishInNextStep_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASERECT_T3338712696_H
#ifndef EASEVECTOR3_T2036336192_H
#define EASEVECTOR3_T2036336192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseVector3
struct  EaseVector3_t2036336192  : public EaseFsmAction_t2585076379
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EaseVector3::fromValue
	FsmVector3_t626444517 * ___fromValue_35;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EaseVector3::toValue
	FsmVector3_t626444517 * ___toValue_36;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EaseVector3::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_37;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseVector3::finishInNextStep
	bool ___finishInNextStep_38;

public:
	inline static int32_t get_offset_of_fromValue_35() { return static_cast<int32_t>(offsetof(EaseVector3_t2036336192, ___fromValue_35)); }
	inline FsmVector3_t626444517 * get_fromValue_35() const { return ___fromValue_35; }
	inline FsmVector3_t626444517 ** get_address_of_fromValue_35() { return &___fromValue_35; }
	inline void set_fromValue_35(FsmVector3_t626444517 * value)
	{
		___fromValue_35 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_35), value);
	}

	inline static int32_t get_offset_of_toValue_36() { return static_cast<int32_t>(offsetof(EaseVector3_t2036336192, ___toValue_36)); }
	inline FsmVector3_t626444517 * get_toValue_36() const { return ___toValue_36; }
	inline FsmVector3_t626444517 ** get_address_of_toValue_36() { return &___toValue_36; }
	inline void set_toValue_36(FsmVector3_t626444517 * value)
	{
		___toValue_36 = value;
		Il2CppCodeGenWriteBarrier((&___toValue_36), value);
	}

	inline static int32_t get_offset_of_vector3Variable_37() { return static_cast<int32_t>(offsetof(EaseVector3_t2036336192, ___vector3Variable_37)); }
	inline FsmVector3_t626444517 * get_vector3Variable_37() const { return ___vector3Variable_37; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_37() { return &___vector3Variable_37; }
	inline void set_vector3Variable_37(FsmVector3_t626444517 * value)
	{
		___vector3Variable_37 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_37), value);
	}

	inline static int32_t get_offset_of_finishInNextStep_38() { return static_cast<int32_t>(offsetof(EaseVector3_t2036336192, ___finishInNextStep_38)); }
	inline bool get_finishInNextStep_38() const { return ___finishInNextStep_38; }
	inline bool* get_address_of_finishInNextStep_38() { return &___finishInNextStep_38; }
	inline void set_finishInNextStep_38(bool value)
	{
		___finishInNextStep_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEVECTOR3_T2036336192_H
#ifndef GETANIMATORBODY_T3473142879_H
#define GETANIMATORBODY_T3473142879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct  GetAnimatorBody_t3473142879  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBody::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyPosition
	FsmVector3_t626444517 * ___bodyPosition_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyRotation
	FsmQuaternion_t4259038327 * ___bodyRotation_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorBody::bodyGameObject
	FsmGameObject_t3581898942 * ___bodyGameObject_20;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBody::_animator
	Animator_t434523843 * ____animator_21;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorBody::_transform
	Transform_t3600365921 * ____transform_22;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3473142879, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_bodyPosition_18() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3473142879, ___bodyPosition_18)); }
	inline FsmVector3_t626444517 * get_bodyPosition_18() const { return ___bodyPosition_18; }
	inline FsmVector3_t626444517 ** get_address_of_bodyPosition_18() { return &___bodyPosition_18; }
	inline void set_bodyPosition_18(FsmVector3_t626444517 * value)
	{
		___bodyPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___bodyPosition_18), value);
	}

	inline static int32_t get_offset_of_bodyRotation_19() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3473142879, ___bodyRotation_19)); }
	inline FsmQuaternion_t4259038327 * get_bodyRotation_19() const { return ___bodyRotation_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_bodyRotation_19() { return &___bodyRotation_19; }
	inline void set_bodyRotation_19(FsmQuaternion_t4259038327 * value)
	{
		___bodyRotation_19 = value;
		Il2CppCodeGenWriteBarrier((&___bodyRotation_19), value);
	}

	inline static int32_t get_offset_of_bodyGameObject_20() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3473142879, ___bodyGameObject_20)); }
	inline FsmGameObject_t3581898942 * get_bodyGameObject_20() const { return ___bodyGameObject_20; }
	inline FsmGameObject_t3581898942 ** get_address_of_bodyGameObject_20() { return &___bodyGameObject_20; }
	inline void set_bodyGameObject_20(FsmGameObject_t3581898942 * value)
	{
		___bodyGameObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___bodyGameObject_20), value);
	}

	inline static int32_t get_offset_of__animator_21() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3473142879, ____animator_21)); }
	inline Animator_t434523843 * get__animator_21() const { return ____animator_21; }
	inline Animator_t434523843 ** get_address_of__animator_21() { return &____animator_21; }
	inline void set__animator_21(Animator_t434523843 * value)
	{
		____animator_21 = value;
		Il2CppCodeGenWriteBarrier((&____animator_21), value);
	}

	inline static int32_t get_offset_of__transform_22() { return static_cast<int32_t>(offsetof(GetAnimatorBody_t3473142879, ____transform_22)); }
	inline Transform_t3600365921 * get__transform_22() const { return ____transform_22; }
	inline Transform_t3600365921 ** get_address_of__transform_22() { return &____transform_22; }
	inline void set__transform_22(Transform_t3600365921 * value)
	{
		____transform_22 = value;
		Il2CppCodeGenWriteBarrier((&____transform_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORBODY_T3473142879_H
#ifndef GETANIMATORBOOL_T3900109898_H
#define GETANIMATORBOOL_T3900109898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct  GetAnimatorBool_t3900109898  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorBool::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorBool::parameter
	FsmString_t1785915204 * ___parameter_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorBool::result
	FsmBool_t163807967 * ___result_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorBool::_animator
	Animator_t434523843 * ____animator_20;
	// System.Int32 HutongGames.PlayMaker.Actions.GetAnimatorBool::_paramID
	int32_t ____paramID_21;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3900109898, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_parameter_18() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3900109898, ___parameter_18)); }
	inline FsmString_t1785915204 * get_parameter_18() const { return ___parameter_18; }
	inline FsmString_t1785915204 ** get_address_of_parameter_18() { return &___parameter_18; }
	inline void set_parameter_18(FsmString_t1785915204 * value)
	{
		___parameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_18), value);
	}

	inline static int32_t get_offset_of_result_19() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3900109898, ___result_19)); }
	inline FsmBool_t163807967 * get_result_19() const { return ___result_19; }
	inline FsmBool_t163807967 ** get_address_of_result_19() { return &___result_19; }
	inline void set_result_19(FsmBool_t163807967 * value)
	{
		___result_19 = value;
		Il2CppCodeGenWriteBarrier((&___result_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3900109898, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}

	inline static int32_t get_offset_of__paramID_21() { return static_cast<int32_t>(offsetof(GetAnimatorBool_t3900109898, ____paramID_21)); }
	inline int32_t get__paramID_21() const { return ____paramID_21; }
	inline int32_t* get_address_of__paramID_21() { return &____paramID_21; }
	inline void set__paramID_21(int32_t value)
	{
		____paramID_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORBOOL_T3900109898_H
#ifndef GETANIMATORCURRENTSTATEINFO_T3870799634_H
#define GETANIMATORCURRENTSTATEINFO_T3870799634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo
struct  GetAnimatorCurrentStateInfo_t3870799634  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::name
	FsmString_t1785915204 * ___name_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::nameHash
	FsmInt_t874273141 * ___nameHash_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::fullPathHash
	FsmInt_t874273141 * ___fullPathHash_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::shortPathHash
	FsmInt_t874273141 * ___shortPathHash_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::tagHash
	FsmInt_t874273141 * ___tagHash_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::isStateLooping
	FsmBool_t163807967 * ___isStateLooping_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::length
	FsmFloat_t2883254149 * ___length_25;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::normalizedTime
	FsmFloat_t2883254149 * ___normalizedTime_26;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::loopCount
	FsmInt_t874273141 * ___loopCount_27;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::currentLoopProgress
	FsmFloat_t2883254149 * ___currentLoopProgress_28;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::_animator
	Animator_t434523843 * ____animator_29;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_name_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___name_19)); }
	inline FsmString_t1785915204 * get_name_19() const { return ___name_19; }
	inline FsmString_t1785915204 ** get_address_of_name_19() { return &___name_19; }
	inline void set_name_19(FsmString_t1785915204 * value)
	{
		___name_19 = value;
		Il2CppCodeGenWriteBarrier((&___name_19), value);
	}

	inline static int32_t get_offset_of_nameHash_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___nameHash_20)); }
	inline FsmInt_t874273141 * get_nameHash_20() const { return ___nameHash_20; }
	inline FsmInt_t874273141 ** get_address_of_nameHash_20() { return &___nameHash_20; }
	inline void set_nameHash_20(FsmInt_t874273141 * value)
	{
		___nameHash_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameHash_20), value);
	}

	inline static int32_t get_offset_of_fullPathHash_21() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___fullPathHash_21)); }
	inline FsmInt_t874273141 * get_fullPathHash_21() const { return ___fullPathHash_21; }
	inline FsmInt_t874273141 ** get_address_of_fullPathHash_21() { return &___fullPathHash_21; }
	inline void set_fullPathHash_21(FsmInt_t874273141 * value)
	{
		___fullPathHash_21 = value;
		Il2CppCodeGenWriteBarrier((&___fullPathHash_21), value);
	}

	inline static int32_t get_offset_of_shortPathHash_22() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___shortPathHash_22)); }
	inline FsmInt_t874273141 * get_shortPathHash_22() const { return ___shortPathHash_22; }
	inline FsmInt_t874273141 ** get_address_of_shortPathHash_22() { return &___shortPathHash_22; }
	inline void set_shortPathHash_22(FsmInt_t874273141 * value)
	{
		___shortPathHash_22 = value;
		Il2CppCodeGenWriteBarrier((&___shortPathHash_22), value);
	}

	inline static int32_t get_offset_of_tagHash_23() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___tagHash_23)); }
	inline FsmInt_t874273141 * get_tagHash_23() const { return ___tagHash_23; }
	inline FsmInt_t874273141 ** get_address_of_tagHash_23() { return &___tagHash_23; }
	inline void set_tagHash_23(FsmInt_t874273141 * value)
	{
		___tagHash_23 = value;
		Il2CppCodeGenWriteBarrier((&___tagHash_23), value);
	}

	inline static int32_t get_offset_of_isStateLooping_24() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___isStateLooping_24)); }
	inline FsmBool_t163807967 * get_isStateLooping_24() const { return ___isStateLooping_24; }
	inline FsmBool_t163807967 ** get_address_of_isStateLooping_24() { return &___isStateLooping_24; }
	inline void set_isStateLooping_24(FsmBool_t163807967 * value)
	{
		___isStateLooping_24 = value;
		Il2CppCodeGenWriteBarrier((&___isStateLooping_24), value);
	}

	inline static int32_t get_offset_of_length_25() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___length_25)); }
	inline FsmFloat_t2883254149 * get_length_25() const { return ___length_25; }
	inline FsmFloat_t2883254149 ** get_address_of_length_25() { return &___length_25; }
	inline void set_length_25(FsmFloat_t2883254149 * value)
	{
		___length_25 = value;
		Il2CppCodeGenWriteBarrier((&___length_25), value);
	}

	inline static int32_t get_offset_of_normalizedTime_26() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___normalizedTime_26)); }
	inline FsmFloat_t2883254149 * get_normalizedTime_26() const { return ___normalizedTime_26; }
	inline FsmFloat_t2883254149 ** get_address_of_normalizedTime_26() { return &___normalizedTime_26; }
	inline void set_normalizedTime_26(FsmFloat_t2883254149 * value)
	{
		___normalizedTime_26 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedTime_26), value);
	}

	inline static int32_t get_offset_of_loopCount_27() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___loopCount_27)); }
	inline FsmInt_t874273141 * get_loopCount_27() const { return ___loopCount_27; }
	inline FsmInt_t874273141 ** get_address_of_loopCount_27() { return &___loopCount_27; }
	inline void set_loopCount_27(FsmInt_t874273141 * value)
	{
		___loopCount_27 = value;
		Il2CppCodeGenWriteBarrier((&___loopCount_27), value);
	}

	inline static int32_t get_offset_of_currentLoopProgress_28() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ___currentLoopProgress_28)); }
	inline FsmFloat_t2883254149 * get_currentLoopProgress_28() const { return ___currentLoopProgress_28; }
	inline FsmFloat_t2883254149 ** get_address_of_currentLoopProgress_28() { return &___currentLoopProgress_28; }
	inline void set_currentLoopProgress_28(FsmFloat_t2883254149 * value)
	{
		___currentLoopProgress_28 = value;
		Il2CppCodeGenWriteBarrier((&___currentLoopProgress_28), value);
	}

	inline static int32_t get_offset_of__animator_29() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfo_t3870799634, ____animator_29)); }
	inline Animator_t434523843 * get__animator_29() const { return ____animator_29; }
	inline Animator_t434523843 ** get_address_of__animator_29() { return &____animator_29; }
	inline void set__animator_29(Animator_t434523843 * value)
	{
		____animator_29 = value;
		Il2CppCodeGenWriteBarrier((&____animator_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCURRENTSTATEINFO_T3870799634_H
#ifndef GETANIMATORCURRENTSTATEINFOISNAME_T1291145368_H
#define GETANIMATORCURRENTSTATEINFOISNAME_T1291145368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName
struct  GetAnimatorCurrentStateInfoIsName_t1291145368  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::name
	FsmString_t1785915204 * ___name_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::isMatching
	FsmBool_t163807967 * ___isMatching_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::nameMatchEvent
	FsmEvent_t3736299882 * ___nameMatchEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::nameDoNotMatchEvent
	FsmEvent_t3736299882 * ___nameDoNotMatchEvent_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::_animator
	Animator_t434523843 * ____animator_23;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_name_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ___name_19)); }
	inline FsmString_t1785915204 * get_name_19() const { return ___name_19; }
	inline FsmString_t1785915204 ** get_address_of_name_19() { return &___name_19; }
	inline void set_name_19(FsmString_t1785915204 * value)
	{
		___name_19 = value;
		Il2CppCodeGenWriteBarrier((&___name_19), value);
	}

	inline static int32_t get_offset_of_isMatching_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ___isMatching_20)); }
	inline FsmBool_t163807967 * get_isMatching_20() const { return ___isMatching_20; }
	inline FsmBool_t163807967 ** get_address_of_isMatching_20() { return &___isMatching_20; }
	inline void set_isMatching_20(FsmBool_t163807967 * value)
	{
		___isMatching_20 = value;
		Il2CppCodeGenWriteBarrier((&___isMatching_20), value);
	}

	inline static int32_t get_offset_of_nameMatchEvent_21() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ___nameMatchEvent_21)); }
	inline FsmEvent_t3736299882 * get_nameMatchEvent_21() const { return ___nameMatchEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_nameMatchEvent_21() { return &___nameMatchEvent_21; }
	inline void set_nameMatchEvent_21(FsmEvent_t3736299882 * value)
	{
		___nameMatchEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___nameMatchEvent_21), value);
	}

	inline static int32_t get_offset_of_nameDoNotMatchEvent_22() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ___nameDoNotMatchEvent_22)); }
	inline FsmEvent_t3736299882 * get_nameDoNotMatchEvent_22() const { return ___nameDoNotMatchEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_nameDoNotMatchEvent_22() { return &___nameDoNotMatchEvent_22; }
	inline void set_nameDoNotMatchEvent_22(FsmEvent_t3736299882 * value)
	{
		___nameDoNotMatchEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___nameDoNotMatchEvent_22), value);
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsName_t1291145368, ____animator_23)); }
	inline Animator_t434523843 * get__animator_23() const { return ____animator_23; }
	inline Animator_t434523843 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t434523843 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier((&____animator_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCURRENTSTATEINFOISNAME_T1291145368_H
#ifndef GETANIMATORCURRENTSTATEINFOISTAG_T1935834350_H
#define GETANIMATORCURRENTSTATEINFOISTAG_T1935834350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag
struct  GetAnimatorCurrentStateInfoIsTag_t1935834350  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tag
	FsmString_t1785915204 * ___tag_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagMatch
	FsmBool_t163807967 * ___tagMatch_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagMatchEvent
	FsmEvent_t3736299882 * ___tagMatchEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::tagDoNotMatchEvent
	FsmEvent_t3736299882 * ___tagDoNotMatchEvent_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsTag::_animator
	Animator_t434523843 * ____animator_23;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_tag_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ___tag_19)); }
	inline FsmString_t1785915204 * get_tag_19() const { return ___tag_19; }
	inline FsmString_t1785915204 ** get_address_of_tag_19() { return &___tag_19; }
	inline void set_tag_19(FsmString_t1785915204 * value)
	{
		___tag_19 = value;
		Il2CppCodeGenWriteBarrier((&___tag_19), value);
	}

	inline static int32_t get_offset_of_tagMatch_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ___tagMatch_20)); }
	inline FsmBool_t163807967 * get_tagMatch_20() const { return ___tagMatch_20; }
	inline FsmBool_t163807967 ** get_address_of_tagMatch_20() { return &___tagMatch_20; }
	inline void set_tagMatch_20(FsmBool_t163807967 * value)
	{
		___tagMatch_20 = value;
		Il2CppCodeGenWriteBarrier((&___tagMatch_20), value);
	}

	inline static int32_t get_offset_of_tagMatchEvent_21() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ___tagMatchEvent_21)); }
	inline FsmEvent_t3736299882 * get_tagMatchEvent_21() const { return ___tagMatchEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_tagMatchEvent_21() { return &___tagMatchEvent_21; }
	inline void set_tagMatchEvent_21(FsmEvent_t3736299882 * value)
	{
		___tagMatchEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___tagMatchEvent_21), value);
	}

	inline static int32_t get_offset_of_tagDoNotMatchEvent_22() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ___tagDoNotMatchEvent_22)); }
	inline FsmEvent_t3736299882 * get_tagDoNotMatchEvent_22() const { return ___tagDoNotMatchEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_tagDoNotMatchEvent_22() { return &___tagDoNotMatchEvent_22; }
	inline void set_tagDoNotMatchEvent_22(FsmEvent_t3736299882 * value)
	{
		___tagDoNotMatchEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___tagDoNotMatchEvent_22), value);
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentStateInfoIsTag_t1935834350, ____animator_23)); }
	inline Animator_t434523843 * get__animator_23() const { return ____animator_23; }
	inline Animator_t434523843 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t434523843 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier((&____animator_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCURRENTSTATEINFOISTAG_T1935834350_H
#ifndef GETANIMATORCURRENTTRANSITIONINFO_T1367284229_H
#define GETANIMATORCURRENTTRANSITIONINFO_T1367284229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
struct  GetAnimatorCurrentTransitionInfo_t1367284229  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::name
	FsmString_t1785915204 * ___name_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::nameHash
	FsmInt_t874273141 * ___nameHash_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::userNameHash
	FsmInt_t874273141 * ___userNameHash_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::normalizedTime
	FsmFloat_t2883254149 * ___normalizedTime_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::_animator
	Animator_t434523843 * ____animator_23;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_name_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ___name_19)); }
	inline FsmString_t1785915204 * get_name_19() const { return ___name_19; }
	inline FsmString_t1785915204 ** get_address_of_name_19() { return &___name_19; }
	inline void set_name_19(FsmString_t1785915204 * value)
	{
		___name_19 = value;
		Il2CppCodeGenWriteBarrier((&___name_19), value);
	}

	inline static int32_t get_offset_of_nameHash_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ___nameHash_20)); }
	inline FsmInt_t874273141 * get_nameHash_20() const { return ___nameHash_20; }
	inline FsmInt_t874273141 ** get_address_of_nameHash_20() { return &___nameHash_20; }
	inline void set_nameHash_20(FsmInt_t874273141 * value)
	{
		___nameHash_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameHash_20), value);
	}

	inline static int32_t get_offset_of_userNameHash_21() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ___userNameHash_21)); }
	inline FsmInt_t874273141 * get_userNameHash_21() const { return ___userNameHash_21; }
	inline FsmInt_t874273141 ** get_address_of_userNameHash_21() { return &___userNameHash_21; }
	inline void set_userNameHash_21(FsmInt_t874273141 * value)
	{
		___userNameHash_21 = value;
		Il2CppCodeGenWriteBarrier((&___userNameHash_21), value);
	}

	inline static int32_t get_offset_of_normalizedTime_22() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ___normalizedTime_22)); }
	inline FsmFloat_t2883254149 * get_normalizedTime_22() const { return ___normalizedTime_22; }
	inline FsmFloat_t2883254149 ** get_address_of_normalizedTime_22() { return &___normalizedTime_22; }
	inline void set_normalizedTime_22(FsmFloat_t2883254149 * value)
	{
		___normalizedTime_22 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedTime_22), value);
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfo_t1367284229, ____animator_23)); }
	inline Animator_t434523843 * get__animator_23() const { return ____animator_23; }
	inline Animator_t434523843 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t434523843 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier((&____animator_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCURRENTTRANSITIONINFO_T1367284229_H
#ifndef GETANIMATORCURRENTTRANSITIONINFOISNAME_T1387398839_H
#define GETANIMATORCURRENTTRANSITIONINFOISNAME_T1387398839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName
struct  GetAnimatorCurrentTransitionInfoIsName_t1387398839  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::name
	FsmString_t1785915204 * ___name_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::nameMatch
	FsmBool_t163807967 * ___nameMatch_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::nameMatchEvent
	FsmEvent_t3736299882 * ___nameMatchEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::nameDoNotMatchEvent
	FsmEvent_t3736299882 * ___nameDoNotMatchEvent_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::_animator
	Animator_t434523843 * ____animator_23;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_name_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ___name_19)); }
	inline FsmString_t1785915204 * get_name_19() const { return ___name_19; }
	inline FsmString_t1785915204 ** get_address_of_name_19() { return &___name_19; }
	inline void set_name_19(FsmString_t1785915204 * value)
	{
		___name_19 = value;
		Il2CppCodeGenWriteBarrier((&___name_19), value);
	}

	inline static int32_t get_offset_of_nameMatch_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ___nameMatch_20)); }
	inline FsmBool_t163807967 * get_nameMatch_20() const { return ___nameMatch_20; }
	inline FsmBool_t163807967 ** get_address_of_nameMatch_20() { return &___nameMatch_20; }
	inline void set_nameMatch_20(FsmBool_t163807967 * value)
	{
		___nameMatch_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameMatch_20), value);
	}

	inline static int32_t get_offset_of_nameMatchEvent_21() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ___nameMatchEvent_21)); }
	inline FsmEvent_t3736299882 * get_nameMatchEvent_21() const { return ___nameMatchEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_nameMatchEvent_21() { return &___nameMatchEvent_21; }
	inline void set_nameMatchEvent_21(FsmEvent_t3736299882 * value)
	{
		___nameMatchEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___nameMatchEvent_21), value);
	}

	inline static int32_t get_offset_of_nameDoNotMatchEvent_22() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ___nameDoNotMatchEvent_22)); }
	inline FsmEvent_t3736299882 * get_nameDoNotMatchEvent_22() const { return ___nameDoNotMatchEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_nameDoNotMatchEvent_22() { return &___nameDoNotMatchEvent_22; }
	inline void set_nameDoNotMatchEvent_22(FsmEvent_t3736299882 * value)
	{
		___nameDoNotMatchEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___nameDoNotMatchEvent_22), value);
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsName_t1387398839, ____animator_23)); }
	inline Animator_t434523843 * get__animator_23() const { return ____animator_23; }
	inline Animator_t434523843 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t434523843 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier((&____animator_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCURRENTTRANSITIONINFOISNAME_T1387398839_H
#ifndef GETANIMATORCURRENTTRANSITIONINFOISUSERNAME_T596063340_H
#define GETANIMATORCURRENTTRANSITIONINFOISUSERNAME_T596063340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName
struct  GetAnimatorCurrentTransitionInfoIsUserName_t596063340  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::userName
	FsmString_t1785915204 * ___userName_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::nameMatch
	FsmBool_t163807967 * ___nameMatch_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::nameMatchEvent
	FsmEvent_t3736299882 * ___nameMatchEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::nameDoNotMatchEvent
	FsmEvent_t3736299882 * ___nameDoNotMatchEvent_22;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::_animator
	Animator_t434523843 * ____animator_23;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_userName_19() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ___userName_19)); }
	inline FsmString_t1785915204 * get_userName_19() const { return ___userName_19; }
	inline FsmString_t1785915204 ** get_address_of_userName_19() { return &___userName_19; }
	inline void set_userName_19(FsmString_t1785915204 * value)
	{
		___userName_19 = value;
		Il2CppCodeGenWriteBarrier((&___userName_19), value);
	}

	inline static int32_t get_offset_of_nameMatch_20() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ___nameMatch_20)); }
	inline FsmBool_t163807967 * get_nameMatch_20() const { return ___nameMatch_20; }
	inline FsmBool_t163807967 ** get_address_of_nameMatch_20() { return &___nameMatch_20; }
	inline void set_nameMatch_20(FsmBool_t163807967 * value)
	{
		___nameMatch_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameMatch_20), value);
	}

	inline static int32_t get_offset_of_nameMatchEvent_21() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ___nameMatchEvent_21)); }
	inline FsmEvent_t3736299882 * get_nameMatchEvent_21() const { return ___nameMatchEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_nameMatchEvent_21() { return &___nameMatchEvent_21; }
	inline void set_nameMatchEvent_21(FsmEvent_t3736299882 * value)
	{
		___nameMatchEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___nameMatchEvent_21), value);
	}

	inline static int32_t get_offset_of_nameDoNotMatchEvent_22() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ___nameDoNotMatchEvent_22)); }
	inline FsmEvent_t3736299882 * get_nameDoNotMatchEvent_22() const { return ___nameDoNotMatchEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_nameDoNotMatchEvent_22() { return &___nameDoNotMatchEvent_22; }
	inline void set_nameDoNotMatchEvent_22(FsmEvent_t3736299882 * value)
	{
		___nameDoNotMatchEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___nameDoNotMatchEvent_22), value);
	}

	inline static int32_t get_offset_of__animator_23() { return static_cast<int32_t>(offsetof(GetAnimatorCurrentTransitionInfoIsUserName_t596063340, ____animator_23)); }
	inline Animator_t434523843 * get__animator_23() const { return ____animator_23; }
	inline Animator_t434523843 ** get_address_of__animator_23() { return &____animator_23; }
	inline void set__animator_23(Animator_t434523843 * value)
	{
		____animator_23 = value;
		Il2CppCodeGenWriteBarrier((&____animator_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORCURRENTTRANSITIONINFOISUSERNAME_T596063340_H
#ifndef GETANIMATORDELTA_T2748201152_H
#define GETANIMATORDELTA_T2748201152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorDelta
struct  GetAnimatorDelta_t2748201152  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorDelta::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorDelta::deltaPosition
	FsmVector3_t626444517 * ___deltaPosition_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorDelta::deltaRotation
	FsmQuaternion_t4259038327 * ___deltaRotation_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorDelta::_animator
	Animator_t434523843 * ____animator_20;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t2748201152, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_deltaPosition_18() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t2748201152, ___deltaPosition_18)); }
	inline FsmVector3_t626444517 * get_deltaPosition_18() const { return ___deltaPosition_18; }
	inline FsmVector3_t626444517 ** get_address_of_deltaPosition_18() { return &___deltaPosition_18; }
	inline void set_deltaPosition_18(FsmVector3_t626444517 * value)
	{
		___deltaPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___deltaPosition_18), value);
	}

	inline static int32_t get_offset_of_deltaRotation_19() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t2748201152, ___deltaRotation_19)); }
	inline FsmQuaternion_t4259038327 * get_deltaRotation_19() const { return ___deltaRotation_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_deltaRotation_19() { return &___deltaRotation_19; }
	inline void set_deltaRotation_19(FsmQuaternion_t4259038327 * value)
	{
		___deltaRotation_19 = value;
		Il2CppCodeGenWriteBarrier((&___deltaRotation_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorDelta_t2748201152, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORDELTA_T2748201152_H
#ifndef GETANIMATORFLOAT_T2318297330_H
#define GETANIMATORFLOAT_T2318297330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorFloat
struct  GetAnimatorFloat_t2318297330  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorFloat::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorFloat::parameter
	FsmString_t1785915204 * ___parameter_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorFloat::result
	FsmFloat_t2883254149 * ___result_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorFloat::_animator
	Animator_t434523843 * ____animator_20;
	// System.Int32 HutongGames.PlayMaker.Actions.GetAnimatorFloat::_paramID
	int32_t ____paramID_21;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorFloat_t2318297330, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_parameter_18() { return static_cast<int32_t>(offsetof(GetAnimatorFloat_t2318297330, ___parameter_18)); }
	inline FsmString_t1785915204 * get_parameter_18() const { return ___parameter_18; }
	inline FsmString_t1785915204 ** get_address_of_parameter_18() { return &___parameter_18; }
	inline void set_parameter_18(FsmString_t1785915204 * value)
	{
		___parameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_18), value);
	}

	inline static int32_t get_offset_of_result_19() { return static_cast<int32_t>(offsetof(GetAnimatorFloat_t2318297330, ___result_19)); }
	inline FsmFloat_t2883254149 * get_result_19() const { return ___result_19; }
	inline FsmFloat_t2883254149 ** get_address_of_result_19() { return &___result_19; }
	inline void set_result_19(FsmFloat_t2883254149 * value)
	{
		___result_19 = value;
		Il2CppCodeGenWriteBarrier((&___result_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorFloat_t2318297330, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}

	inline static int32_t get_offset_of__paramID_21() { return static_cast<int32_t>(offsetof(GetAnimatorFloat_t2318297330, ____paramID_21)); }
	inline int32_t get__paramID_21() const { return ____paramID_21; }
	inline int32_t* get_address_of__paramID_21() { return &____paramID_21; }
	inline void set__paramID_21(int32_t value)
	{
		____paramID_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORFLOAT_T2318297330_H
#ifndef GETANIMATORGRAVITYWEIGHT_T130115598_H
#define GETANIMATORGRAVITYWEIGHT_T130115598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight
struct  GetAnimatorGravityWeight_t130115598  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::gravityWeight
	FsmFloat_t2883254149 * ___gravityWeight_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::_animator
	Animator_t434523843 * ____animator_19;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t130115598, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_gravityWeight_18() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t130115598, ___gravityWeight_18)); }
	inline FsmFloat_t2883254149 * get_gravityWeight_18() const { return ___gravityWeight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_gravityWeight_18() { return &___gravityWeight_18; }
	inline void set_gravityWeight_18(FsmFloat_t2883254149 * value)
	{
		___gravityWeight_18 = value;
		Il2CppCodeGenWriteBarrier((&___gravityWeight_18), value);
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(GetAnimatorGravityWeight_t130115598, ____animator_19)); }
	inline Animator_t434523843 * get__animator_19() const { return ____animator_19; }
	inline Animator_t434523843 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t434523843 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier((&____animator_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORGRAVITYWEIGHT_T130115598_H
#ifndef GETANIMATORIKGOAL_T2372078135_H
#define GETANIMATORIKGOAL_T2372078135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIKGoal
struct  GetAnimatorIKGoal_t2372078135  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::iKGoal
	FsmEnum_t2861764163 * ___iKGoal_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::goal
	FsmGameObject_t3581898942 * ___goal_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::position
	FsmVector3_t626444517 * ___position_20;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::rotation
	FsmQuaternion_t4259038327 * ___rotation_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::positionWeight
	FsmFloat_t2883254149 * ___positionWeight_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::rotationWeight
	FsmFloat_t2883254149 * ___rotationWeight_23;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::_animator
	Animator_t434523843 * ____animator_24;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::_transform
	Transform_t3600365921 * ____transform_25;
	// UnityEngine.AvatarIKGoal HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::_iKGoal
	int32_t ____iKGoal_26;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_iKGoal_18() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___iKGoal_18)); }
	inline FsmEnum_t2861764163 * get_iKGoal_18() const { return ___iKGoal_18; }
	inline FsmEnum_t2861764163 ** get_address_of_iKGoal_18() { return &___iKGoal_18; }
	inline void set_iKGoal_18(FsmEnum_t2861764163 * value)
	{
		___iKGoal_18 = value;
		Il2CppCodeGenWriteBarrier((&___iKGoal_18), value);
	}

	inline static int32_t get_offset_of_goal_19() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___goal_19)); }
	inline FsmGameObject_t3581898942 * get_goal_19() const { return ___goal_19; }
	inline FsmGameObject_t3581898942 ** get_address_of_goal_19() { return &___goal_19; }
	inline void set_goal_19(FsmGameObject_t3581898942 * value)
	{
		___goal_19 = value;
		Il2CppCodeGenWriteBarrier((&___goal_19), value);
	}

	inline static int32_t get_offset_of_position_20() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___position_20)); }
	inline FsmVector3_t626444517 * get_position_20() const { return ___position_20; }
	inline FsmVector3_t626444517 ** get_address_of_position_20() { return &___position_20; }
	inline void set_position_20(FsmVector3_t626444517 * value)
	{
		___position_20 = value;
		Il2CppCodeGenWriteBarrier((&___position_20), value);
	}

	inline static int32_t get_offset_of_rotation_21() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___rotation_21)); }
	inline FsmQuaternion_t4259038327 * get_rotation_21() const { return ___rotation_21; }
	inline FsmQuaternion_t4259038327 ** get_address_of_rotation_21() { return &___rotation_21; }
	inline void set_rotation_21(FsmQuaternion_t4259038327 * value)
	{
		___rotation_21 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_21), value);
	}

	inline static int32_t get_offset_of_positionWeight_22() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___positionWeight_22)); }
	inline FsmFloat_t2883254149 * get_positionWeight_22() const { return ___positionWeight_22; }
	inline FsmFloat_t2883254149 ** get_address_of_positionWeight_22() { return &___positionWeight_22; }
	inline void set_positionWeight_22(FsmFloat_t2883254149 * value)
	{
		___positionWeight_22 = value;
		Il2CppCodeGenWriteBarrier((&___positionWeight_22), value);
	}

	inline static int32_t get_offset_of_rotationWeight_23() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ___rotationWeight_23)); }
	inline FsmFloat_t2883254149 * get_rotationWeight_23() const { return ___rotationWeight_23; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationWeight_23() { return &___rotationWeight_23; }
	inline void set_rotationWeight_23(FsmFloat_t2883254149 * value)
	{
		___rotationWeight_23 = value;
		Il2CppCodeGenWriteBarrier((&___rotationWeight_23), value);
	}

	inline static int32_t get_offset_of__animator_24() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ____animator_24)); }
	inline Animator_t434523843 * get__animator_24() const { return ____animator_24; }
	inline Animator_t434523843 ** get_address_of__animator_24() { return &____animator_24; }
	inline void set__animator_24(Animator_t434523843 * value)
	{
		____animator_24 = value;
		Il2CppCodeGenWriteBarrier((&____animator_24), value);
	}

	inline static int32_t get_offset_of__transform_25() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ____transform_25)); }
	inline Transform_t3600365921 * get__transform_25() const { return ____transform_25; }
	inline Transform_t3600365921 ** get_address_of__transform_25() { return &____transform_25; }
	inline void set__transform_25(Transform_t3600365921 * value)
	{
		____transform_25 = value;
		Il2CppCodeGenWriteBarrier((&____transform_25), value);
	}

	inline static int32_t get_offset_of__iKGoal_26() { return static_cast<int32_t>(offsetof(GetAnimatorIKGoal_t2372078135, ____iKGoal_26)); }
	inline int32_t get__iKGoal_26() const { return ____iKGoal_26; }
	inline int32_t* get_address_of__iKGoal_26() { return &____iKGoal_26; }
	inline void set__iKGoal_26(int32_t value)
	{
		____iKGoal_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORIKGOAL_T2372078135_H
#ifndef GETANIMATORINT_T1761492912_H
#define GETANIMATORINT_T1761492912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorInt
struct  GetAnimatorInt_t1761492912  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorInt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorInt::parameter
	FsmString_t1785915204 * ___parameter_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorInt::result
	FsmInt_t874273141 * ___result_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorInt::_animator
	Animator_t434523843 * ____animator_20;
	// System.Int32 HutongGames.PlayMaker.Actions.GetAnimatorInt::_paramID
	int32_t ____paramID_21;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorInt_t1761492912, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_parameter_18() { return static_cast<int32_t>(offsetof(GetAnimatorInt_t1761492912, ___parameter_18)); }
	inline FsmString_t1785915204 * get_parameter_18() const { return ___parameter_18; }
	inline FsmString_t1785915204 ** get_address_of_parameter_18() { return &___parameter_18; }
	inline void set_parameter_18(FsmString_t1785915204 * value)
	{
		___parameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_18), value);
	}

	inline static int32_t get_offset_of_result_19() { return static_cast<int32_t>(offsetof(GetAnimatorInt_t1761492912, ___result_19)); }
	inline FsmInt_t874273141 * get_result_19() const { return ___result_19; }
	inline FsmInt_t874273141 ** get_address_of_result_19() { return &___result_19; }
	inline void set_result_19(FsmInt_t874273141 * value)
	{
		___result_19 = value;
		Il2CppCodeGenWriteBarrier((&___result_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorInt_t1761492912, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}

	inline static int32_t get_offset_of__paramID_21() { return static_cast<int32_t>(offsetof(GetAnimatorInt_t1761492912, ____paramID_21)); }
	inline int32_t get__paramID_21() const { return ____paramID_21; }
	inline int32_t* get_address_of__paramID_21() { return &____paramID_21; }
	inline void set__paramID_21(int32_t value)
	{
		____paramID_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORINT_T1761492912_H
#ifndef GETANIMATORISLAYERINTRANSITION_T3891240677_H
#define GETANIMATORISLAYERINTRANSITION_T3891240677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct  GetAnimatorIsLayerInTransition_t3891240677  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isInTransition
	FsmBool_t163807967 * ___isInTransition_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isInTransitionEvent
	FsmEvent_t3736299882 * ___isInTransitionEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::isNotInTransitionEvent
	FsmEvent_t3736299882 * ___isNotInTransitionEvent_21;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::_animator
	Animator_t434523843 * ____animator_22;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3891240677, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3891240677, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_isInTransition_19() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3891240677, ___isInTransition_19)); }
	inline FsmBool_t163807967 * get_isInTransition_19() const { return ___isInTransition_19; }
	inline FsmBool_t163807967 ** get_address_of_isInTransition_19() { return &___isInTransition_19; }
	inline void set_isInTransition_19(FsmBool_t163807967 * value)
	{
		___isInTransition_19 = value;
		Il2CppCodeGenWriteBarrier((&___isInTransition_19), value);
	}

	inline static int32_t get_offset_of_isInTransitionEvent_20() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3891240677, ___isInTransitionEvent_20)); }
	inline FsmEvent_t3736299882 * get_isInTransitionEvent_20() const { return ___isInTransitionEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_isInTransitionEvent_20() { return &___isInTransitionEvent_20; }
	inline void set_isInTransitionEvent_20(FsmEvent_t3736299882 * value)
	{
		___isInTransitionEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___isInTransitionEvent_20), value);
	}

	inline static int32_t get_offset_of_isNotInTransitionEvent_21() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3891240677, ___isNotInTransitionEvent_21)); }
	inline FsmEvent_t3736299882 * get_isNotInTransitionEvent_21() const { return ___isNotInTransitionEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotInTransitionEvent_21() { return &___isNotInTransitionEvent_21; }
	inline void set_isNotInTransitionEvent_21(FsmEvent_t3736299882 * value)
	{
		___isNotInTransitionEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___isNotInTransitionEvent_21), value);
	}

	inline static int32_t get_offset_of__animator_22() { return static_cast<int32_t>(offsetof(GetAnimatorIsLayerInTransition_t3891240677, ____animator_22)); }
	inline Animator_t434523843 * get__animator_22() const { return ____animator_22; }
	inline Animator_t434523843 ** get_address_of__animator_22() { return &____animator_22; }
	inline void set__animator_22(Animator_t434523843 * value)
	{
		____animator_22 = value;
		Il2CppCodeGenWriteBarrier((&____animator_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORISLAYERINTRANSITION_T3891240677_H
#ifndef GETANIMATORISMATCHINGTARGET_T3396280219_H
#define GETANIMATORISMATCHINGTARGET_T3396280219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct  GetAnimatorIsMatchingTarget_t3396280219  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::isMatchingActive
	FsmBool_t163807967 * ___isMatchingActive_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::matchingActivatedEvent
	FsmEvent_t3736299882 * ___matchingActivatedEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::matchingDeactivedEvent
	FsmEvent_t3736299882 * ___matchingDeactivedEvent_20;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::_animator
	Animator_t434523843 * ____animator_21;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t3396280219, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_isMatchingActive_18() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t3396280219, ___isMatchingActive_18)); }
	inline FsmBool_t163807967 * get_isMatchingActive_18() const { return ___isMatchingActive_18; }
	inline FsmBool_t163807967 ** get_address_of_isMatchingActive_18() { return &___isMatchingActive_18; }
	inline void set_isMatchingActive_18(FsmBool_t163807967 * value)
	{
		___isMatchingActive_18 = value;
		Il2CppCodeGenWriteBarrier((&___isMatchingActive_18), value);
	}

	inline static int32_t get_offset_of_matchingActivatedEvent_19() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t3396280219, ___matchingActivatedEvent_19)); }
	inline FsmEvent_t3736299882 * get_matchingActivatedEvent_19() const { return ___matchingActivatedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_matchingActivatedEvent_19() { return &___matchingActivatedEvent_19; }
	inline void set_matchingActivatedEvent_19(FsmEvent_t3736299882 * value)
	{
		___matchingActivatedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___matchingActivatedEvent_19), value);
	}

	inline static int32_t get_offset_of_matchingDeactivedEvent_20() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t3396280219, ___matchingDeactivedEvent_20)); }
	inline FsmEvent_t3736299882 * get_matchingDeactivedEvent_20() const { return ___matchingDeactivedEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_matchingDeactivedEvent_20() { return &___matchingDeactivedEvent_20; }
	inline void set_matchingDeactivedEvent_20(FsmEvent_t3736299882 * value)
	{
		___matchingDeactivedEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___matchingDeactivedEvent_20), value);
	}

	inline static int32_t get_offset_of__animator_21() { return static_cast<int32_t>(offsetof(GetAnimatorIsMatchingTarget_t3396280219, ____animator_21)); }
	inline Animator_t434523843 * get__animator_21() const { return ____animator_21; }
	inline Animator_t434523843 ** get_address_of__animator_21() { return &____animator_21; }
	inline void set__animator_21(Animator_t434523843 * value)
	{
		____animator_21 = value;
		Il2CppCodeGenWriteBarrier((&____animator_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORISMATCHINGTARGET_T3396280219_H
#ifndef GETANIMATORLAYERWEIGHT_T4155071065_H
#define GETANIMATORLAYERWEIGHT_T4155071065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight
struct  GetAnimatorLayerWeight_t4155071065  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::layerWeight
	FsmFloat_t2883254149 * ___layerWeight_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::_animator
	Animator_t434523843 * ____animator_20;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t4155071065, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t4155071065, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_layerWeight_19() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t4155071065, ___layerWeight_19)); }
	inline FsmFloat_t2883254149 * get_layerWeight_19() const { return ___layerWeight_19; }
	inline FsmFloat_t2883254149 ** get_address_of_layerWeight_19() { return &___layerWeight_19; }
	inline void set_layerWeight_19(FsmFloat_t2883254149 * value)
	{
		___layerWeight_19 = value;
		Il2CppCodeGenWriteBarrier((&___layerWeight_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorLayerWeight_t4155071065, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORLAYERWEIGHT_T4155071065_H
#ifndef GETANIMATORNEXTSTATEINFO_T936547330_H
#define GETANIMATORNEXTSTATEINFO_T936547330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct  GetAnimatorNextStateInfo_t936547330  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::layerIndex
	FsmInt_t874273141 * ___layerIndex_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::name
	FsmString_t1785915204 * ___name_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::nameHash
	FsmInt_t874273141 * ___nameHash_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::fullPathHash
	FsmInt_t874273141 * ___fullPathHash_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::shortPathHash
	FsmInt_t874273141 * ___shortPathHash_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::tagHash
	FsmInt_t874273141 * ___tagHash_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::isStateLooping
	FsmBool_t163807967 * ___isStateLooping_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::length
	FsmFloat_t2883254149 * ___length_25;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::normalizedTime
	FsmFloat_t2883254149 * ___normalizedTime_26;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::loopCount
	FsmInt_t874273141 * ___loopCount_27;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::currentLoopProgress
	FsmFloat_t2883254149 * ___currentLoopProgress_28;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::_animator
	Animator_t434523843 * ____animator_29;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_layerIndex_18() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___layerIndex_18)); }
	inline FsmInt_t874273141 * get_layerIndex_18() const { return ___layerIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_layerIndex_18() { return &___layerIndex_18; }
	inline void set_layerIndex_18(FsmInt_t874273141 * value)
	{
		___layerIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___layerIndex_18), value);
	}

	inline static int32_t get_offset_of_name_19() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___name_19)); }
	inline FsmString_t1785915204 * get_name_19() const { return ___name_19; }
	inline FsmString_t1785915204 ** get_address_of_name_19() { return &___name_19; }
	inline void set_name_19(FsmString_t1785915204 * value)
	{
		___name_19 = value;
		Il2CppCodeGenWriteBarrier((&___name_19), value);
	}

	inline static int32_t get_offset_of_nameHash_20() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___nameHash_20)); }
	inline FsmInt_t874273141 * get_nameHash_20() const { return ___nameHash_20; }
	inline FsmInt_t874273141 ** get_address_of_nameHash_20() { return &___nameHash_20; }
	inline void set_nameHash_20(FsmInt_t874273141 * value)
	{
		___nameHash_20 = value;
		Il2CppCodeGenWriteBarrier((&___nameHash_20), value);
	}

	inline static int32_t get_offset_of_fullPathHash_21() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___fullPathHash_21)); }
	inline FsmInt_t874273141 * get_fullPathHash_21() const { return ___fullPathHash_21; }
	inline FsmInt_t874273141 ** get_address_of_fullPathHash_21() { return &___fullPathHash_21; }
	inline void set_fullPathHash_21(FsmInt_t874273141 * value)
	{
		___fullPathHash_21 = value;
		Il2CppCodeGenWriteBarrier((&___fullPathHash_21), value);
	}

	inline static int32_t get_offset_of_shortPathHash_22() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___shortPathHash_22)); }
	inline FsmInt_t874273141 * get_shortPathHash_22() const { return ___shortPathHash_22; }
	inline FsmInt_t874273141 ** get_address_of_shortPathHash_22() { return &___shortPathHash_22; }
	inline void set_shortPathHash_22(FsmInt_t874273141 * value)
	{
		___shortPathHash_22 = value;
		Il2CppCodeGenWriteBarrier((&___shortPathHash_22), value);
	}

	inline static int32_t get_offset_of_tagHash_23() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___tagHash_23)); }
	inline FsmInt_t874273141 * get_tagHash_23() const { return ___tagHash_23; }
	inline FsmInt_t874273141 ** get_address_of_tagHash_23() { return &___tagHash_23; }
	inline void set_tagHash_23(FsmInt_t874273141 * value)
	{
		___tagHash_23 = value;
		Il2CppCodeGenWriteBarrier((&___tagHash_23), value);
	}

	inline static int32_t get_offset_of_isStateLooping_24() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___isStateLooping_24)); }
	inline FsmBool_t163807967 * get_isStateLooping_24() const { return ___isStateLooping_24; }
	inline FsmBool_t163807967 ** get_address_of_isStateLooping_24() { return &___isStateLooping_24; }
	inline void set_isStateLooping_24(FsmBool_t163807967 * value)
	{
		___isStateLooping_24 = value;
		Il2CppCodeGenWriteBarrier((&___isStateLooping_24), value);
	}

	inline static int32_t get_offset_of_length_25() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___length_25)); }
	inline FsmFloat_t2883254149 * get_length_25() const { return ___length_25; }
	inline FsmFloat_t2883254149 ** get_address_of_length_25() { return &___length_25; }
	inline void set_length_25(FsmFloat_t2883254149 * value)
	{
		___length_25 = value;
		Il2CppCodeGenWriteBarrier((&___length_25), value);
	}

	inline static int32_t get_offset_of_normalizedTime_26() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___normalizedTime_26)); }
	inline FsmFloat_t2883254149 * get_normalizedTime_26() const { return ___normalizedTime_26; }
	inline FsmFloat_t2883254149 ** get_address_of_normalizedTime_26() { return &___normalizedTime_26; }
	inline void set_normalizedTime_26(FsmFloat_t2883254149 * value)
	{
		___normalizedTime_26 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedTime_26), value);
	}

	inline static int32_t get_offset_of_loopCount_27() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___loopCount_27)); }
	inline FsmInt_t874273141 * get_loopCount_27() const { return ___loopCount_27; }
	inline FsmInt_t874273141 ** get_address_of_loopCount_27() { return &___loopCount_27; }
	inline void set_loopCount_27(FsmInt_t874273141 * value)
	{
		___loopCount_27 = value;
		Il2CppCodeGenWriteBarrier((&___loopCount_27), value);
	}

	inline static int32_t get_offset_of_currentLoopProgress_28() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ___currentLoopProgress_28)); }
	inline FsmFloat_t2883254149 * get_currentLoopProgress_28() const { return ___currentLoopProgress_28; }
	inline FsmFloat_t2883254149 ** get_address_of_currentLoopProgress_28() { return &___currentLoopProgress_28; }
	inline void set_currentLoopProgress_28(FsmFloat_t2883254149 * value)
	{
		___currentLoopProgress_28 = value;
		Il2CppCodeGenWriteBarrier((&___currentLoopProgress_28), value);
	}

	inline static int32_t get_offset_of__animator_29() { return static_cast<int32_t>(offsetof(GetAnimatorNextStateInfo_t936547330, ____animator_29)); }
	inline Animator_t434523843 * get__animator_29() const { return ____animator_29; }
	inline Animator_t434523843 ** get_address_of__animator_29() { return &____animator_29; }
	inline void set__animator_29(Animator_t434523843 * value)
	{
		____animator_29 = value;
		Il2CppCodeGenWriteBarrier((&____animator_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORNEXTSTATEINFO_T936547330_H
#ifndef GETANIMATORPIVOT_T1085583339_H
#define GETANIMATORPIVOT_T1085583339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorPivot
struct  GetAnimatorPivot_t1085583339  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorPivot::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorPivot::pivotWeight
	FsmFloat_t2883254149 * ___pivotWeight_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorPivot::pivotPosition
	FsmVector3_t626444517 * ___pivotPosition_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorPivot::_animator
	Animator_t434523843 * ____animator_20;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorPivot_t1085583339, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_pivotWeight_18() { return static_cast<int32_t>(offsetof(GetAnimatorPivot_t1085583339, ___pivotWeight_18)); }
	inline FsmFloat_t2883254149 * get_pivotWeight_18() const { return ___pivotWeight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_pivotWeight_18() { return &___pivotWeight_18; }
	inline void set_pivotWeight_18(FsmFloat_t2883254149 * value)
	{
		___pivotWeight_18 = value;
		Il2CppCodeGenWriteBarrier((&___pivotWeight_18), value);
	}

	inline static int32_t get_offset_of_pivotPosition_19() { return static_cast<int32_t>(offsetof(GetAnimatorPivot_t1085583339, ___pivotPosition_19)); }
	inline FsmVector3_t626444517 * get_pivotPosition_19() const { return ___pivotPosition_19; }
	inline FsmVector3_t626444517 ** get_address_of_pivotPosition_19() { return &___pivotPosition_19; }
	inline void set_pivotPosition_19(FsmVector3_t626444517 * value)
	{
		___pivotPosition_19 = value;
		Il2CppCodeGenWriteBarrier((&___pivotPosition_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(GetAnimatorPivot_t1085583339, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORPIVOT_T1085583339_H
#ifndef GETANIMATORROOT_T3934712930_H
#define GETANIMATORROOT_T3934712930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorRoot
struct  GetAnimatorRoot_t3934712930  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorRoot::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorRoot::rootPosition
	FsmVector3_t626444517 * ___rootPosition_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorRoot::rootRotation
	FsmQuaternion_t4259038327 * ___rootRotation_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorRoot::bodyGameObject
	FsmGameObject_t3581898942 * ___bodyGameObject_20;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorRoot::_animator
	Animator_t434523843 * ____animator_21;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorRoot::_transform
	Transform_t3600365921 * ____transform_22;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorRoot_t3934712930, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_rootPosition_18() { return static_cast<int32_t>(offsetof(GetAnimatorRoot_t3934712930, ___rootPosition_18)); }
	inline FsmVector3_t626444517 * get_rootPosition_18() const { return ___rootPosition_18; }
	inline FsmVector3_t626444517 ** get_address_of_rootPosition_18() { return &___rootPosition_18; }
	inline void set_rootPosition_18(FsmVector3_t626444517 * value)
	{
		___rootPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___rootPosition_18), value);
	}

	inline static int32_t get_offset_of_rootRotation_19() { return static_cast<int32_t>(offsetof(GetAnimatorRoot_t3934712930, ___rootRotation_19)); }
	inline FsmQuaternion_t4259038327 * get_rootRotation_19() const { return ___rootRotation_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_rootRotation_19() { return &___rootRotation_19; }
	inline void set_rootRotation_19(FsmQuaternion_t4259038327 * value)
	{
		___rootRotation_19 = value;
		Il2CppCodeGenWriteBarrier((&___rootRotation_19), value);
	}

	inline static int32_t get_offset_of_bodyGameObject_20() { return static_cast<int32_t>(offsetof(GetAnimatorRoot_t3934712930, ___bodyGameObject_20)); }
	inline FsmGameObject_t3581898942 * get_bodyGameObject_20() const { return ___bodyGameObject_20; }
	inline FsmGameObject_t3581898942 ** get_address_of_bodyGameObject_20() { return &___bodyGameObject_20; }
	inline void set_bodyGameObject_20(FsmGameObject_t3581898942 * value)
	{
		___bodyGameObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___bodyGameObject_20), value);
	}

	inline static int32_t get_offset_of__animator_21() { return static_cast<int32_t>(offsetof(GetAnimatorRoot_t3934712930, ____animator_21)); }
	inline Animator_t434523843 * get__animator_21() const { return ____animator_21; }
	inline Animator_t434523843 ** get_address_of__animator_21() { return &____animator_21; }
	inline void set__animator_21(Animator_t434523843 * value)
	{
		____animator_21 = value;
		Il2CppCodeGenWriteBarrier((&____animator_21), value);
	}

	inline static int32_t get_offset_of__transform_22() { return static_cast<int32_t>(offsetof(GetAnimatorRoot_t3934712930, ____transform_22)); }
	inline Transform_t3600365921 * get__transform_22() const { return ____transform_22; }
	inline Transform_t3600365921 ** get_address_of__transform_22() { return &____transform_22; }
	inline void set__transform_22(Transform_t3600365921 * value)
	{
		____transform_22 = value;
		Il2CppCodeGenWriteBarrier((&____transform_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORROOT_T3934712930_H
#ifndef GETANIMATORSPEED_T2658983986_H
#define GETANIMATORSPEED_T2658983986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorSpeed
struct  GetAnimatorSpeed_t2658983986  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorSpeed::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAnimatorSpeed::speed
	FsmFloat_t2883254149 * ___speed_18;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorSpeed::_animator
	Animator_t434523843 * ____animator_19;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorSpeed_t2658983986, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(GetAnimatorSpeed_t2658983986, ___speed_18)); }
	inline FsmFloat_t2883254149 * get_speed_18() const { return ___speed_18; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(FsmFloat_t2883254149 * value)
	{
		___speed_18 = value;
		Il2CppCodeGenWriteBarrier((&___speed_18), value);
	}

	inline static int32_t get_offset_of__animator_19() { return static_cast<int32_t>(offsetof(GetAnimatorSpeed_t2658983986, ____animator_19)); }
	inline Animator_t434523843 * get__animator_19() const { return ____animator_19; }
	inline Animator_t434523843 ** get_address_of__animator_19() { return &____animator_19; }
	inline void set__animator_19(Animator_t434523843 * value)
	{
		____animator_19 = value;
		Il2CppCodeGenWriteBarrier((&____animator_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORSPEED_T2658983986_H
#ifndef GETANIMATORTARGET_T2390406205_H
#define GETANIMATORTARGET_T2390406205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAnimatorTarget
struct  GetAnimatorTarget_t2390406205  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAnimatorTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAnimatorTarget::targetPosition
	FsmVector3_t626444517 * ___targetPosition_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetAnimatorTarget::targetRotation
	FsmQuaternion_t4259038327 * ___targetRotation_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAnimatorTarget::targetGameObject
	FsmGameObject_t3581898942 * ___targetGameObject_20;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.GetAnimatorTarget::_animator
	Animator_t434523843 * ____animator_21;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.GetAnimatorTarget::_transform
	Transform_t3600365921 * ____transform_22;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t2390406205, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t2390406205, ___targetPosition_18)); }
	inline FsmVector3_t626444517 * get_targetPosition_18() const { return ___targetPosition_18; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(FsmVector3_t626444517 * value)
	{
		___targetPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_18), value);
	}

	inline static int32_t get_offset_of_targetRotation_19() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t2390406205, ___targetRotation_19)); }
	inline FsmQuaternion_t4259038327 * get_targetRotation_19() const { return ___targetRotation_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_targetRotation_19() { return &___targetRotation_19; }
	inline void set_targetRotation_19(FsmQuaternion_t4259038327 * value)
	{
		___targetRotation_19 = value;
		Il2CppCodeGenWriteBarrier((&___targetRotation_19), value);
	}

	inline static int32_t get_offset_of_targetGameObject_20() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t2390406205, ___targetGameObject_20)); }
	inline FsmGameObject_t3581898942 * get_targetGameObject_20() const { return ___targetGameObject_20; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetGameObject_20() { return &___targetGameObject_20; }
	inline void set_targetGameObject_20(FsmGameObject_t3581898942 * value)
	{
		___targetGameObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___targetGameObject_20), value);
	}

	inline static int32_t get_offset_of__animator_21() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t2390406205, ____animator_21)); }
	inline Animator_t434523843 * get__animator_21() const { return ____animator_21; }
	inline Animator_t434523843 ** get_address_of__animator_21() { return &____animator_21; }
	inline void set__animator_21(Animator_t434523843 * value)
	{
		____animator_21 = value;
		Il2CppCodeGenWriteBarrier((&____animator_21), value);
	}

	inline static int32_t get_offset_of__transform_22() { return static_cast<int32_t>(offsetof(GetAnimatorTarget_t2390406205, ____transform_22)); }
	inline Transform_t3600365921 * get__transform_22() const { return ____transform_22; }
	inline Transform_t3600365921 ** get_address_of__transform_22() { return &____transform_22; }
	inline void set__transform_22(Transform_t3600365921 * value)
	{
		____transform_22 = value;
		Il2CppCodeGenWriteBarrier((&____transform_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANIMATORTARGET_T2390406205_H
#ifndef SETANIMATORBOOL_T1258844782_H
#define SETANIMATORBOOL_T1258844782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorBool
struct  SetAnimatorBool_t1258844782  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorBool::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimatorBool::parameter
	FsmString_t1785915204 * ___parameter_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetAnimatorBool::Value
	FsmBool_t163807967 * ___Value_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorBool::_animator
	Animator_t434523843 * ____animator_20;
	// System.Int32 HutongGames.PlayMaker.Actions.SetAnimatorBool::_paramID
	int32_t ____paramID_21;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(SetAnimatorBool_t1258844782, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_parameter_18() { return static_cast<int32_t>(offsetof(SetAnimatorBool_t1258844782, ___parameter_18)); }
	inline FsmString_t1785915204 * get_parameter_18() const { return ___parameter_18; }
	inline FsmString_t1785915204 ** get_address_of_parameter_18() { return &___parameter_18; }
	inline void set_parameter_18(FsmString_t1785915204 * value)
	{
		___parameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_18), value);
	}

	inline static int32_t get_offset_of_Value_19() { return static_cast<int32_t>(offsetof(SetAnimatorBool_t1258844782, ___Value_19)); }
	inline FsmBool_t163807967 * get_Value_19() const { return ___Value_19; }
	inline FsmBool_t163807967 ** get_address_of_Value_19() { return &___Value_19; }
	inline void set_Value_19(FsmBool_t163807967 * value)
	{
		___Value_19 = value;
		Il2CppCodeGenWriteBarrier((&___Value_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(SetAnimatorBool_t1258844782, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}

	inline static int32_t get_offset_of__paramID_21() { return static_cast<int32_t>(offsetof(SetAnimatorBool_t1258844782, ____paramID_21)); }
	inline int32_t get__paramID_21() const { return ____paramID_21; }
	inline int32_t* get_address_of__paramID_21() { return &____paramID_21; }
	inline void set__paramID_21(int32_t value)
	{
		____paramID_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORBOOL_T1258844782_H
#ifndef SETANIMATORFLOAT_T3971999510_H
#define SETANIMATORFLOAT_T3971999510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorFloat
struct  SetAnimatorFloat_t3971999510  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorFloat::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimatorFloat::parameter
	FsmString_t1785915204 * ___parameter_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorFloat::Value
	FsmFloat_t2883254149 * ___Value_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimatorFloat::dampTime
	FsmFloat_t2883254149 * ___dampTime_20;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorFloat::_animator
	Animator_t434523843 * ____animator_21;
	// System.Int32 HutongGames.PlayMaker.Actions.SetAnimatorFloat::_paramID
	int32_t ____paramID_22;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3971999510, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_parameter_18() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3971999510, ___parameter_18)); }
	inline FsmString_t1785915204 * get_parameter_18() const { return ___parameter_18; }
	inline FsmString_t1785915204 ** get_address_of_parameter_18() { return &___parameter_18; }
	inline void set_parameter_18(FsmString_t1785915204 * value)
	{
		___parameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_18), value);
	}

	inline static int32_t get_offset_of_Value_19() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3971999510, ___Value_19)); }
	inline FsmFloat_t2883254149 * get_Value_19() const { return ___Value_19; }
	inline FsmFloat_t2883254149 ** get_address_of_Value_19() { return &___Value_19; }
	inline void set_Value_19(FsmFloat_t2883254149 * value)
	{
		___Value_19 = value;
		Il2CppCodeGenWriteBarrier((&___Value_19), value);
	}

	inline static int32_t get_offset_of_dampTime_20() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3971999510, ___dampTime_20)); }
	inline FsmFloat_t2883254149 * get_dampTime_20() const { return ___dampTime_20; }
	inline FsmFloat_t2883254149 ** get_address_of_dampTime_20() { return &___dampTime_20; }
	inline void set_dampTime_20(FsmFloat_t2883254149 * value)
	{
		___dampTime_20 = value;
		Il2CppCodeGenWriteBarrier((&___dampTime_20), value);
	}

	inline static int32_t get_offset_of__animator_21() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3971999510, ____animator_21)); }
	inline Animator_t434523843 * get__animator_21() const { return ____animator_21; }
	inline Animator_t434523843 ** get_address_of__animator_21() { return &____animator_21; }
	inline void set__animator_21(Animator_t434523843 * value)
	{
		____animator_21 = value;
		Il2CppCodeGenWriteBarrier((&____animator_21), value);
	}

	inline static int32_t get_offset_of__paramID_22() { return static_cast<int32_t>(offsetof(SetAnimatorFloat_t3971999510, ____paramID_22)); }
	inline int32_t get__paramID_22() const { return ____paramID_22; }
	inline int32_t* get_address_of__paramID_22() { return &____paramID_22; }
	inline void set__paramID_22(int32_t value)
	{
		____paramID_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORFLOAT_T3971999510_H
#ifndef SETANIMATORINT_T738835924_H
#define SETANIMATORINT_T738835924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimatorInt
struct  SetAnimatorInt_t738835924  : public FsmStateActionAnimatorBase_t3384377168
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimatorInt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimatorInt::parameter
	FsmString_t1785915204 * ___parameter_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetAnimatorInt::Value
	FsmInt_t874273141 * ___Value_19;
	// UnityEngine.Animator HutongGames.PlayMaker.Actions.SetAnimatorInt::_animator
	Animator_t434523843 * ____animator_20;
	// System.Int32 HutongGames.PlayMaker.Actions.SetAnimatorInt::_paramID
	int32_t ____paramID_21;

public:
	inline static int32_t get_offset_of_gameObject_17() { return static_cast<int32_t>(offsetof(SetAnimatorInt_t738835924, ___gameObject_17)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_17() const { return ___gameObject_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_17() { return &___gameObject_17; }
	inline void set_gameObject_17(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_17), value);
	}

	inline static int32_t get_offset_of_parameter_18() { return static_cast<int32_t>(offsetof(SetAnimatorInt_t738835924, ___parameter_18)); }
	inline FsmString_t1785915204 * get_parameter_18() const { return ___parameter_18; }
	inline FsmString_t1785915204 ** get_address_of_parameter_18() { return &___parameter_18; }
	inline void set_parameter_18(FsmString_t1785915204 * value)
	{
		___parameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_18), value);
	}

	inline static int32_t get_offset_of_Value_19() { return static_cast<int32_t>(offsetof(SetAnimatorInt_t738835924, ___Value_19)); }
	inline FsmInt_t874273141 * get_Value_19() const { return ___Value_19; }
	inline FsmInt_t874273141 ** get_address_of_Value_19() { return &___Value_19; }
	inline void set_Value_19(FsmInt_t874273141 * value)
	{
		___Value_19 = value;
		Il2CppCodeGenWriteBarrier((&___Value_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(SetAnimatorInt_t738835924, ____animator_20)); }
	inline Animator_t434523843 * get__animator_20() const { return ____animator_20; }
	inline Animator_t434523843 ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_t434523843 * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}

	inline static int32_t get_offset_of__paramID_21() { return static_cast<int32_t>(offsetof(SetAnimatorInt_t738835924, ____paramID_21)); }
	inline int32_t get__paramID_21() const { return ____paramID_21; }
	inline int32_t* get_address_of__paramID_21() { return &____paramID_21; }
	inline void set__paramID_21(int32_t value)
	{
		____paramID_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATORINT_T738835924_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ADDMIXINGTRANSFORM_T4262639660_H
#define ADDMIXINGTRANSFORM_T4262639660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddMixingTransform
struct  AddMixingTransform_t4262639660  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddMixingTransform::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddMixingTransform::animationName
	FsmString_t1785915204 * ___animationName_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddMixingTransform::transform
	FsmString_t1785915204 * ___transform_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddMixingTransform::recursive
	FsmBool_t163807967 * ___recursive_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddMixingTransform_t4262639660, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animationName_17() { return static_cast<int32_t>(offsetof(AddMixingTransform_t4262639660, ___animationName_17)); }
	inline FsmString_t1785915204 * get_animationName_17() const { return ___animationName_17; }
	inline FsmString_t1785915204 ** get_address_of_animationName_17() { return &___animationName_17; }
	inline void set_animationName_17(FsmString_t1785915204 * value)
	{
		___animationName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_17), value);
	}

	inline static int32_t get_offset_of_transform_18() { return static_cast<int32_t>(offsetof(AddMixingTransform_t4262639660, ___transform_18)); }
	inline FsmString_t1785915204 * get_transform_18() const { return ___transform_18; }
	inline FsmString_t1785915204 ** get_address_of_transform_18() { return &___transform_18; }
	inline void set_transform_18(FsmString_t1785915204 * value)
	{
		___transform_18 = value;
		Il2CppCodeGenWriteBarrier((&___transform_18), value);
	}

	inline static int32_t get_offset_of_recursive_19() { return static_cast<int32_t>(offsetof(AddMixingTransform_t4262639660, ___recursive_19)); }
	inline FsmBool_t163807967 * get_recursive_19() const { return ___recursive_19; }
	inline FsmBool_t163807967 ** get_address_of_recursive_19() { return &___recursive_19; }
	inline void set_recursive_19(FsmBool_t163807967 * value)
	{
		___recursive_19 = value;
		Il2CppCodeGenWriteBarrier((&___recursive_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDMIXINGTRANSFORM_T4262639660_H
#ifndef ANIMATIONSETTINGS_T424863638_H
#define ANIMATIONSETTINGS_T424863638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnimationSettings
struct  AnimationSettings_t424863638  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AnimationSettings::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AnimationSettings::animName
	FsmString_t1785915204 * ___animName_17;
	// UnityEngine.WrapMode HutongGames.PlayMaker.Actions.AnimationSettings::wrapMode
	int32_t ___wrapMode_18;
	// UnityEngine.AnimationBlendMode HutongGames.PlayMaker.Actions.AnimationSettings::blendMode
	int32_t ___blendMode_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AnimationSettings::speed
	FsmFloat_t2883254149 * ___speed_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AnimationSettings::layer
	FsmInt_t874273141 * ___layer_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AnimationSettings_t424863638, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(AnimationSettings_t424863638, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_wrapMode_18() { return static_cast<int32_t>(offsetof(AnimationSettings_t424863638, ___wrapMode_18)); }
	inline int32_t get_wrapMode_18() const { return ___wrapMode_18; }
	inline int32_t* get_address_of_wrapMode_18() { return &___wrapMode_18; }
	inline void set_wrapMode_18(int32_t value)
	{
		___wrapMode_18 = value;
	}

	inline static int32_t get_offset_of_blendMode_19() { return static_cast<int32_t>(offsetof(AnimationSettings_t424863638, ___blendMode_19)); }
	inline int32_t get_blendMode_19() const { return ___blendMode_19; }
	inline int32_t* get_address_of_blendMode_19() { return &___blendMode_19; }
	inline void set_blendMode_19(int32_t value)
	{
		___blendMode_19 = value;
	}

	inline static int32_t get_offset_of_speed_20() { return static_cast<int32_t>(offsetof(AnimationSettings_t424863638, ___speed_20)); }
	inline FsmFloat_t2883254149 * get_speed_20() const { return ___speed_20; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_20() { return &___speed_20; }
	inline void set_speed_20(FsmFloat_t2883254149 * value)
	{
		___speed_20 = value;
		Il2CppCodeGenWriteBarrier((&___speed_20), value);
	}

	inline static int32_t get_offset_of_layer_21() { return static_cast<int32_t>(offsetof(AnimationSettings_t424863638, ___layer_21)); }
	inline FsmInt_t874273141 * get_layer_21() const { return ___layer_21; }
	inline FsmInt_t874273141 ** get_address_of_layer_21() { return &___layer_21; }
	inline void set_layer_21(FsmInt_t874273141 * value)
	{
		___layer_21 = value;
		Il2CppCodeGenWriteBarrier((&___layer_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSETTINGS_T424863638_H
#ifndef BLENDANIMATION_T985073980_H
#define BLENDANIMATION_T985073980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BlendAnimation
struct  BlendAnimation_t985073980  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.BlendAnimation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BlendAnimation::animName
	FsmString_t1785915204 * ___animName_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.BlendAnimation::targetWeight
	FsmFloat_t2883254149 * ___targetWeight_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.BlendAnimation::time
	FsmFloat_t2883254149 * ___time_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BlendAnimation::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_20;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.BlendAnimation::delayedFinishEvent
	DelayedEvent_t3987626228 * ___delayedFinishEvent_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(BlendAnimation_t985073980, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(BlendAnimation_t985073980, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_targetWeight_18() { return static_cast<int32_t>(offsetof(BlendAnimation_t985073980, ___targetWeight_18)); }
	inline FsmFloat_t2883254149 * get_targetWeight_18() const { return ___targetWeight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_targetWeight_18() { return &___targetWeight_18; }
	inline void set_targetWeight_18(FsmFloat_t2883254149 * value)
	{
		___targetWeight_18 = value;
		Il2CppCodeGenWriteBarrier((&___targetWeight_18), value);
	}

	inline static int32_t get_offset_of_time_19() { return static_cast<int32_t>(offsetof(BlendAnimation_t985073980, ___time_19)); }
	inline FsmFloat_t2883254149 * get_time_19() const { return ___time_19; }
	inline FsmFloat_t2883254149 ** get_address_of_time_19() { return &___time_19; }
	inline void set_time_19(FsmFloat_t2883254149 * value)
	{
		___time_19 = value;
		Il2CppCodeGenWriteBarrier((&___time_19), value);
	}

	inline static int32_t get_offset_of_finishEvent_20() { return static_cast<int32_t>(offsetof(BlendAnimation_t985073980, ___finishEvent_20)); }
	inline FsmEvent_t3736299882 * get_finishEvent_20() const { return ___finishEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_20() { return &___finishEvent_20; }
	inline void set_finishEvent_20(FsmEvent_t3736299882 * value)
	{
		___finishEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_20), value);
	}

	inline static int32_t get_offset_of_delayedFinishEvent_21() { return static_cast<int32_t>(offsetof(BlendAnimation_t985073980, ___delayedFinishEvent_21)); }
	inline DelayedEvent_t3987626228 * get_delayedFinishEvent_21() const { return ___delayedFinishEvent_21; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedFinishEvent_21() { return &___delayedFinishEvent_21; }
	inline void set_delayedFinishEvent_21(DelayedEvent_t3987626228 * value)
	{
		___delayedFinishEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___delayedFinishEvent_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDANIMATION_T985073980_H
#ifndef ENABLEANIMATION_T2755334398_H
#define ENABLEANIMATION_T2755334398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableAnimation
struct  EnableAnimation_t2755334398  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableAnimation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableAnimation::animName
	FsmString_t1785915204 * ___animName_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableAnimation::enable
	FsmBool_t163807967 * ___enable_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableAnimation::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_19;
	// UnityEngine.AnimationState HutongGames.PlayMaker.Actions.EnableAnimation::anim
	AnimationState_t1108360407 * ___anim_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(EnableAnimation_t2755334398, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(EnableAnimation_t2755334398, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_enable_18() { return static_cast<int32_t>(offsetof(EnableAnimation_t2755334398, ___enable_18)); }
	inline FsmBool_t163807967 * get_enable_18() const { return ___enable_18; }
	inline FsmBool_t163807967 ** get_address_of_enable_18() { return &___enable_18; }
	inline void set_enable_18(FsmBool_t163807967 * value)
	{
		___enable_18 = value;
		Il2CppCodeGenWriteBarrier((&___enable_18), value);
	}

	inline static int32_t get_offset_of_resetOnExit_19() { return static_cast<int32_t>(offsetof(EnableAnimation_t2755334398, ___resetOnExit_19)); }
	inline FsmBool_t163807967 * get_resetOnExit_19() const { return ___resetOnExit_19; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_19() { return &___resetOnExit_19; }
	inline void set_resetOnExit_19(FsmBool_t163807967 * value)
	{
		___resetOnExit_19 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_19), value);
	}

	inline static int32_t get_offset_of_anim_20() { return static_cast<int32_t>(offsetof(EnableAnimation_t2755334398, ___anim_20)); }
	inline AnimationState_t1108360407 * get_anim_20() const { return ___anim_20; }
	inline AnimationState_t1108360407 ** get_address_of_anim_20() { return &___anim_20; }
	inline void set_anim_20(AnimationState_t1108360407 * value)
	{
		___anim_20 = value;
		Il2CppCodeGenWriteBarrier((&___anim_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEANIMATION_T2755334398_H
#ifndef PLAYANIMATION_T4013957021_H
#define PLAYANIMATION_T4013957021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayAnimation
struct  PlayAnimation_t4013957021  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlayAnimation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PlayAnimation::animName
	FsmString_t1785915204 * ___animName_17;
	// UnityEngine.PlayMode HutongGames.PlayMaker.Actions.PlayAnimation::playMode
	int32_t ___playMode_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlayAnimation::blendTime
	FsmFloat_t2883254149 * ___blendTime_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayAnimation::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayAnimation::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_21;
	// System.Boolean HutongGames.PlayMaker.Actions.PlayAnimation::stopOnExit
	bool ___stopOnExit_22;
	// UnityEngine.AnimationState HutongGames.PlayMaker.Actions.PlayAnimation::anim
	AnimationState_t1108360407 * ___anim_23;
	// System.Single HutongGames.PlayMaker.Actions.PlayAnimation::prevAnimtTime
	float ___prevAnimtTime_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_playMode_18() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___playMode_18)); }
	inline int32_t get_playMode_18() const { return ___playMode_18; }
	inline int32_t* get_address_of_playMode_18() { return &___playMode_18; }
	inline void set_playMode_18(int32_t value)
	{
		___playMode_18 = value;
	}

	inline static int32_t get_offset_of_blendTime_19() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___blendTime_19)); }
	inline FsmFloat_t2883254149 * get_blendTime_19() const { return ___blendTime_19; }
	inline FsmFloat_t2883254149 ** get_address_of_blendTime_19() { return &___blendTime_19; }
	inline void set_blendTime_19(FsmFloat_t2883254149 * value)
	{
		___blendTime_19 = value;
		Il2CppCodeGenWriteBarrier((&___blendTime_19), value);
	}

	inline static int32_t get_offset_of_finishEvent_20() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___finishEvent_20)); }
	inline FsmEvent_t3736299882 * get_finishEvent_20() const { return ___finishEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_20() { return &___finishEvent_20; }
	inline void set_finishEvent_20(FsmEvent_t3736299882 * value)
	{
		___finishEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_20), value);
	}

	inline static int32_t get_offset_of_loopEvent_21() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___loopEvent_21)); }
	inline FsmEvent_t3736299882 * get_loopEvent_21() const { return ___loopEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_21() { return &___loopEvent_21; }
	inline void set_loopEvent_21(FsmEvent_t3736299882 * value)
	{
		___loopEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_21), value);
	}

	inline static int32_t get_offset_of_stopOnExit_22() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___stopOnExit_22)); }
	inline bool get_stopOnExit_22() const { return ___stopOnExit_22; }
	inline bool* get_address_of_stopOnExit_22() { return &___stopOnExit_22; }
	inline void set_stopOnExit_22(bool value)
	{
		___stopOnExit_22 = value;
	}

	inline static int32_t get_offset_of_anim_23() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___anim_23)); }
	inline AnimationState_t1108360407 * get_anim_23() const { return ___anim_23; }
	inline AnimationState_t1108360407 ** get_address_of_anim_23() { return &___anim_23; }
	inline void set_anim_23(AnimationState_t1108360407 * value)
	{
		___anim_23 = value;
		Il2CppCodeGenWriteBarrier((&___anim_23), value);
	}

	inline static int32_t get_offset_of_prevAnimtTime_24() { return static_cast<int32_t>(offsetof(PlayAnimation_t4013957021, ___prevAnimtTime_24)); }
	inline float get_prevAnimtTime_24() const { return ___prevAnimtTime_24; }
	inline float* get_address_of_prevAnimtTime_24() { return &___prevAnimtTime_24; }
	inline void set_prevAnimtTime_24(float value)
	{
		___prevAnimtTime_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYANIMATION_T4013957021_H
#ifndef PLAYRANDOMANIMATION_T3880340942_H
#define PLAYRANDOMANIMATION_T3880340942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayRandomAnimation
struct  PlayRandomAnimation_t3880340942  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.PlayRandomAnimation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayRandomAnimation::animations
	FsmStringU5BU5D_t252501805* ___animations_17;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayRandomAnimation::weights
	FsmFloatU5BU5D_t3637897416* ___weights_18;
	// UnityEngine.PlayMode HutongGames.PlayMaker.Actions.PlayRandomAnimation::playMode
	int32_t ___playMode_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PlayRandomAnimation::blendTime
	FsmFloat_t2883254149 * ___blendTime_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayRandomAnimation::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayRandomAnimation::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_22;
	// System.Boolean HutongGames.PlayMaker.Actions.PlayRandomAnimation::stopOnExit
	bool ___stopOnExit_23;
	// UnityEngine.AnimationState HutongGames.PlayMaker.Actions.PlayRandomAnimation::anim
	AnimationState_t1108360407 * ___anim_24;
	// System.Single HutongGames.PlayMaker.Actions.PlayRandomAnimation::prevAnimtTime
	float ___prevAnimtTime_25;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animations_17() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___animations_17)); }
	inline FsmStringU5BU5D_t252501805* get_animations_17() const { return ___animations_17; }
	inline FsmStringU5BU5D_t252501805** get_address_of_animations_17() { return &___animations_17; }
	inline void set_animations_17(FsmStringU5BU5D_t252501805* value)
	{
		___animations_17 = value;
		Il2CppCodeGenWriteBarrier((&___animations_17), value);
	}

	inline static int32_t get_offset_of_weights_18() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___weights_18)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_18() const { return ___weights_18; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_18() { return &___weights_18; }
	inline void set_weights_18(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_18 = value;
		Il2CppCodeGenWriteBarrier((&___weights_18), value);
	}

	inline static int32_t get_offset_of_playMode_19() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___playMode_19)); }
	inline int32_t get_playMode_19() const { return ___playMode_19; }
	inline int32_t* get_address_of_playMode_19() { return &___playMode_19; }
	inline void set_playMode_19(int32_t value)
	{
		___playMode_19 = value;
	}

	inline static int32_t get_offset_of_blendTime_20() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___blendTime_20)); }
	inline FsmFloat_t2883254149 * get_blendTime_20() const { return ___blendTime_20; }
	inline FsmFloat_t2883254149 ** get_address_of_blendTime_20() { return &___blendTime_20; }
	inline void set_blendTime_20(FsmFloat_t2883254149 * value)
	{
		___blendTime_20 = value;
		Il2CppCodeGenWriteBarrier((&___blendTime_20), value);
	}

	inline static int32_t get_offset_of_finishEvent_21() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___finishEvent_21)); }
	inline FsmEvent_t3736299882 * get_finishEvent_21() const { return ___finishEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_21() { return &___finishEvent_21; }
	inline void set_finishEvent_21(FsmEvent_t3736299882 * value)
	{
		___finishEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_21), value);
	}

	inline static int32_t get_offset_of_loopEvent_22() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___loopEvent_22)); }
	inline FsmEvent_t3736299882 * get_loopEvent_22() const { return ___loopEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_22() { return &___loopEvent_22; }
	inline void set_loopEvent_22(FsmEvent_t3736299882 * value)
	{
		___loopEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_22), value);
	}

	inline static int32_t get_offset_of_stopOnExit_23() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___stopOnExit_23)); }
	inline bool get_stopOnExit_23() const { return ___stopOnExit_23; }
	inline bool* get_address_of_stopOnExit_23() { return &___stopOnExit_23; }
	inline void set_stopOnExit_23(bool value)
	{
		___stopOnExit_23 = value;
	}

	inline static int32_t get_offset_of_anim_24() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___anim_24)); }
	inline AnimationState_t1108360407 * get_anim_24() const { return ___anim_24; }
	inline AnimationState_t1108360407 ** get_address_of_anim_24() { return &___anim_24; }
	inline void set_anim_24(AnimationState_t1108360407 * value)
	{
		___anim_24 = value;
		Il2CppCodeGenWriteBarrier((&___anim_24), value);
	}

	inline static int32_t get_offset_of_prevAnimtTime_25() { return static_cast<int32_t>(offsetof(PlayRandomAnimation_t3880340942, ___prevAnimtTime_25)); }
	inline float get_prevAnimtTime_25() const { return ___prevAnimtTime_25; }
	inline float* get_address_of_prevAnimtTime_25() { return &___prevAnimtTime_25; }
	inline void set_prevAnimtTime_25(float value)
	{
		___prevAnimtTime_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYRANDOMANIMATION_T3880340942_H
#ifndef REMOVEMIXINGTRANSFORM_T466720318_H
#define REMOVEMIXINGTRANSFORM_T466720318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RemoveMixingTransform
struct  RemoveMixingTransform_t466720318  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RemoveMixingTransform::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.RemoveMixingTransform::animationName
	FsmString_t1785915204 * ___animationName_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.RemoveMixingTransform::transfrom
	FsmString_t1785915204 * ___transfrom_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RemoveMixingTransform_t466720318, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animationName_17() { return static_cast<int32_t>(offsetof(RemoveMixingTransform_t466720318, ___animationName_17)); }
	inline FsmString_t1785915204 * get_animationName_17() const { return ___animationName_17; }
	inline FsmString_t1785915204 ** get_address_of_animationName_17() { return &___animationName_17; }
	inline void set_animationName_17(FsmString_t1785915204 * value)
	{
		___animationName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_17), value);
	}

	inline static int32_t get_offset_of_transfrom_18() { return static_cast<int32_t>(offsetof(RemoveMixingTransform_t466720318, ___transfrom_18)); }
	inline FsmString_t1785915204 * get_transfrom_18() const { return ___transfrom_18; }
	inline FsmString_t1785915204 ** get_address_of_transfrom_18() { return &___transfrom_18; }
	inline void set_transfrom_18(FsmString_t1785915204 * value)
	{
		___transfrom_18 = value;
		Il2CppCodeGenWriteBarrier((&___transfrom_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVEMIXINGTRANSFORM_T466720318_H
#ifndef REWINDANIMATION_T4084631938_H
#define REWINDANIMATION_T4084631938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RewindAnimation
struct  RewindAnimation_t4084631938  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RewindAnimation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.RewindAnimation::animName
	FsmString_t1785915204 * ___animName_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RewindAnimation_t4084631938, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(RewindAnimation_t4084631938, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWINDANIMATION_T4084631938_H
#ifndef SETANIMATIONSPEED_T585003002_H
#define SETANIMATIONSPEED_T585003002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimationSpeed
struct  SetAnimationSpeed_t585003002  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimationSpeed::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimationSpeed::animName
	FsmString_t1785915204 * ___animName_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimationSpeed::speed
	FsmFloat_t2883254149 * ___speed_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimationSpeed::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t585003002, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t585003002, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t585003002, ___speed_18)); }
	inline FsmFloat_t2883254149 * get_speed_18() const { return ___speed_18; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(FsmFloat_t2883254149 * value)
	{
		___speed_18 = value;
		Il2CppCodeGenWriteBarrier((&___speed_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetAnimationSpeed_t585003002, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATIONSPEED_T585003002_H
#ifndef SETANIMATIONTIME_T1212068435_H
#define SETANIMATIONTIME_T1212068435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimationTime
struct  SetAnimationTime_t1212068435  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimationTime::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimationTime::animName
	FsmString_t1785915204 * ___animName_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimationTime::time
	FsmFloat_t2883254149 * ___time_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimationTime::normalized
	bool ___normalized_19;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimationTime::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAnimationTime_t1212068435, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(SetAnimationTime_t1212068435, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_time_18() { return static_cast<int32_t>(offsetof(SetAnimationTime_t1212068435, ___time_18)); }
	inline FsmFloat_t2883254149 * get_time_18() const { return ___time_18; }
	inline FsmFloat_t2883254149 ** get_address_of_time_18() { return &___time_18; }
	inline void set_time_18(FsmFloat_t2883254149 * value)
	{
		___time_18 = value;
		Il2CppCodeGenWriteBarrier((&___time_18), value);
	}

	inline static int32_t get_offset_of_normalized_19() { return static_cast<int32_t>(offsetof(SetAnimationTime_t1212068435, ___normalized_19)); }
	inline bool get_normalized_19() const { return ___normalized_19; }
	inline bool* get_address_of_normalized_19() { return &___normalized_19; }
	inline void set_normalized_19(bool value)
	{
		___normalized_19 = value;
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(SetAnimationTime_t1212068435, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATIONTIME_T1212068435_H
#ifndef SETANIMATIONWEIGHT_T2943869022_H
#define SETANIMATIONWEIGHT_T2943869022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAnimationWeight
struct  SetAnimationWeight_t2943869022  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetAnimationWeight::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetAnimationWeight::animName
	FsmString_t1785915204 * ___animName_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetAnimationWeight::weight
	FsmFloat_t2883254149 * ___weight_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAnimationWeight::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetAnimationWeight_t2943869022, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(SetAnimationWeight_t2943869022, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}

	inline static int32_t get_offset_of_weight_18() { return static_cast<int32_t>(offsetof(SetAnimationWeight_t2943869022, ___weight_18)); }
	inline FsmFloat_t2883254149 * get_weight_18() const { return ___weight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_weight_18() { return &___weight_18; }
	inline void set_weight_18(FsmFloat_t2883254149 * value)
	{
		___weight_18 = value;
		Il2CppCodeGenWriteBarrier((&___weight_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetAnimationWeight_t2943869022, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETANIMATIONWEIGHT_T2943869022_H
#ifndef STOPANIMATION_T2812777501_H
#define STOPANIMATION_T2812777501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StopAnimation
struct  StopAnimation_t2812777501  : public BaseAnimationAction_t1134898484
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.StopAnimation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StopAnimation::animName
	FsmString_t1785915204 * ___animName_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(StopAnimation_t2812777501, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_animName_17() { return static_cast<int32_t>(offsetof(StopAnimation_t2812777501, ___animName_17)); }
	inline FsmString_t1785915204 * get_animName_17() const { return ___animName_17; }
	inline FsmString_t1785915204 ** get_address_of_animName_17() { return &___animName_17; }
	inline void set_animName_17(FsmString_t1785915204 * value)
	{
		___animName_17 = value;
		Il2CppCodeGenWriteBarrier((&___animName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPANIMATION_T2812777501_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef GESTURES_MOCAP_01_FOLLOW_PLAYER_T1997237248_H
#define GESTURES_MOCAP_01_FOLLOW_PLAYER_T1997237248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gestures_mocap_01_follow_player
struct  gestures_mocap_01_follow_player_t1997237248  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject gestures_mocap_01_follow_player::Player
	GameObject_t1113636619 * ___Player_4;
	// System.Single gestures_mocap_01_follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single gestures_mocap_01_follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(gestures_mocap_01_follow_player_t1997237248, ___Player_4)); }
	inline GameObject_t1113636619 * get_Player_4() const { return ___Player_4; }
	inline GameObject_t1113636619 ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_t1113636619 * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(gestures_mocap_01_follow_player_t1997237248, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(gestures_mocap_01_follow_player_t1997237248, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURES_MOCAP_01_FOLLOW_PLAYER_T1997237248_H
#ifndef GESTURES_MOCAP_02_FOLLOW_PLAYER_T1993869347_H
#define GESTURES_MOCAP_02_FOLLOW_PLAYER_T1993869347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gestures_mocap_02_follow_player
struct  gestures_mocap_02_follow_player_t1993869347  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject gestures_mocap_02_follow_player::Player
	GameObject_t1113636619 * ___Player_4;
	// System.Single gestures_mocap_02_follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single gestures_mocap_02_follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(gestures_mocap_02_follow_player_t1993869347, ___Player_4)); }
	inline GameObject_t1113636619 * get_Player_4() const { return ___Player_4; }
	inline GameObject_t1113636619 ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_t1113636619 * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(gestures_mocap_02_follow_player_t1993869347, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(gestures_mocap_02_follow_player_t1993869347, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTURES_MOCAP_02_FOLLOW_PLAYER_T1993869347_H
#ifndef MOVEMENT_MOCAP_FOLLOW_PLAYER_T3377973691_H
#define MOVEMENT_MOCAP_FOLLOW_PLAYER_T3377973691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// movement_mocap_follow_player
struct  movement_mocap_follow_player_t3377973691  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject movement_mocap_follow_player::Player
	GameObject_t1113636619 * ___Player_4;
	// System.Single movement_mocap_follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single movement_mocap_follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(movement_mocap_follow_player_t3377973691, ___Player_4)); }
	inline GameObject_t1113636619 * get_Player_4() const { return ___Player_4; }
	inline GameObject_t1113636619 ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_t1113636619 * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(movement_mocap_follow_player_t3377973691, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(movement_mocap_follow_player_t3377973691, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENT_MOCAP_FOLLOW_PLAYER_T3377973691_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (gestures_mocap_01_follow_player_t1997237248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3000[3] = 
{
	gestures_mocap_01_follow_player_t1997237248::get_offset_of_Player_4(),
	gestures_mocap_01_follow_player_t1997237248::get_offset_of_cameraHeight_5(),
	gestures_mocap_01_follow_player_t1997237248::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (gestures_mocap_02_follow_player_t1993869347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[3] = 
{
	gestures_mocap_02_follow_player_t1993869347::get_offset_of_Player_4(),
	gestures_mocap_02_follow_player_t1993869347::get_offset_of_cameraHeight_5(),
	gestures_mocap_02_follow_player_t1993869347::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (movement_mocap_follow_player_t3377973691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[3] = 
{
	movement_mocap_follow_player_t3377973691::get_offset_of_Player_4(),
	movement_mocap_follow_player_t3377973691::get_offset_of_cameraHeight_5(),
	movement_mocap_follow_player_t3377973691::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (ActionHelpers_t2911535836), -1, sizeof(ActionHelpers_t2911535836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3003[4] = 
{
	ActionHelpers_t2911535836_StaticFields::get_offset_of_mousePickInfo_0(),
	ActionHelpers_t2911535836_StaticFields::get_offset_of_mousePickRaycastTime_1(),
	ActionHelpers_t2911535836_StaticFields::get_offset_of_mousePickDistanceUsed_2(),
	ActionHelpers_t2911535836_StaticFields::get_offset_of_mousePickLayerMaskUsed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (AnimateColor_t143032445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[10] = 
{
	AnimateColor_t143032445::get_offset_of_colorVariable_37(),
	AnimateColor_t143032445::get_offset_of_curveR_38(),
	AnimateColor_t143032445::get_offset_of_calculationR_39(),
	AnimateColor_t143032445::get_offset_of_curveG_40(),
	AnimateColor_t143032445::get_offset_of_calculationG_41(),
	AnimateColor_t143032445::get_offset_of_curveB_42(),
	AnimateColor_t143032445::get_offset_of_calculationB_43(),
	AnimateColor_t143032445::get_offset_of_curveA_44(),
	AnimateColor_t143032445::get_offset_of_calculationA_45(),
	AnimateColor_t143032445::get_offset_of_finishInNextStep_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (AnimateFloat_t1684987476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3005[8] = 
{
	AnimateFloat_t1684987476::get_offset_of_animCurve_14(),
	AnimateFloat_t1684987476::get_offset_of_floatVariable_15(),
	AnimateFloat_t1684987476::get_offset_of_finishEvent_16(),
	AnimateFloat_t1684987476::get_offset_of_realTime_17(),
	AnimateFloat_t1684987476::get_offset_of_startTime_18(),
	AnimateFloat_t1684987476::get_offset_of_currentTime_19(),
	AnimateFloat_t1684987476::get_offset_of_endTime_20(),
	AnimateFloat_t1684987476::get_offset_of_looping_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (AnimateFloatV2_t2936973654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[4] = 
{
	AnimateFloatV2_t2936973654::get_offset_of_floatVariable_37(),
	AnimateFloatV2_t2936973654::get_offset_of_animCurve_38(),
	AnimateFloatV2_t2936973654::get_offset_of_calculation_39(),
	AnimateFloatV2_t2936973654::get_offset_of_finishInNextStep_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (AnimateFsmAction_t2616519849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[23] = 
{
	AnimateFsmAction_t2616519849::get_offset_of_time_14(),
	AnimateFsmAction_t2616519849::get_offset_of_speed_15(),
	AnimateFsmAction_t2616519849::get_offset_of_delay_16(),
	AnimateFsmAction_t2616519849::get_offset_of_ignoreCurveOffset_17(),
	AnimateFsmAction_t2616519849::get_offset_of_finishEvent_18(),
	AnimateFsmAction_t2616519849::get_offset_of_realTime_19(),
	AnimateFsmAction_t2616519849::get_offset_of_startTime_20(),
	AnimateFsmAction_t2616519849::get_offset_of_currentTime_21(),
	AnimateFsmAction_t2616519849::get_offset_of_endTimes_22(),
	AnimateFsmAction_t2616519849::get_offset_of_lastTime_23(),
	AnimateFsmAction_t2616519849::get_offset_of_deltaTime_24(),
	AnimateFsmAction_t2616519849::get_offset_of_delayTime_25(),
	AnimateFsmAction_t2616519849::get_offset_of_keyOffsets_26(),
	AnimateFsmAction_t2616519849::get_offset_of_curves_27(),
	AnimateFsmAction_t2616519849::get_offset_of_calculations_28(),
	AnimateFsmAction_t2616519849::get_offset_of_resultFloats_29(),
	AnimateFsmAction_t2616519849::get_offset_of_fromFloats_30(),
	AnimateFsmAction_t2616519849::get_offset_of_toFloats_31(),
	AnimateFsmAction_t2616519849::get_offset_of_finishAction_32(),
	AnimateFsmAction_t2616519849::get_offset_of_isRunning_33(),
	AnimateFsmAction_t2616519849::get_offset_of_looping_34(),
	AnimateFsmAction_t2616519849::get_offset_of_start_35(),
	AnimateFsmAction_t2616519849::get_offset_of_largestEndTime_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (Calculation_t2075304323)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3008[9] = 
{
	Calculation_t2075304323::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (AnimateRect_t3641165014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[10] = 
{
	AnimateRect_t3641165014::get_offset_of_rectVariable_37(),
	AnimateRect_t3641165014::get_offset_of_curveX_38(),
	AnimateRect_t3641165014::get_offset_of_calculationX_39(),
	AnimateRect_t3641165014::get_offset_of_curveY_40(),
	AnimateRect_t3641165014::get_offset_of_calculationY_41(),
	AnimateRect_t3641165014::get_offset_of_curveW_42(),
	AnimateRect_t3641165014::get_offset_of_calculationW_43(),
	AnimateRect_t3641165014::get_offset_of_curveH_44(),
	AnimateRect_t3641165014::get_offset_of_calculationH_45(),
	AnimateRect_t3641165014::get_offset_of_finishInNextStep_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (AnimateVector3_t2247115148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[8] = 
{
	AnimateVector3_t2247115148::get_offset_of_vectorVariable_37(),
	AnimateVector3_t2247115148::get_offset_of_curveX_38(),
	AnimateVector3_t2247115148::get_offset_of_calculationX_39(),
	AnimateVector3_t2247115148::get_offset_of_curveY_40(),
	AnimateVector3_t2247115148::get_offset_of_calculationY_41(),
	AnimateVector3_t2247115148::get_offset_of_curveZ_42(),
	AnimateVector3_t2247115148::get_offset_of_calculationZ_43(),
	AnimateVector3_t2247115148::get_offset_of_finishInNextStep_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (CurveColor_t2104014689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[13] = 
{
	CurveColor_t2104014689::get_offset_of_colorVariable_38(),
	CurveColor_t2104014689::get_offset_of_fromValue_39(),
	CurveColor_t2104014689::get_offset_of_toValue_40(),
	CurveColor_t2104014689::get_offset_of_curveR_41(),
	CurveColor_t2104014689::get_offset_of_calculationR_42(),
	CurveColor_t2104014689::get_offset_of_curveG_43(),
	CurveColor_t2104014689::get_offset_of_calculationG_44(),
	CurveColor_t2104014689::get_offset_of_curveB_45(),
	CurveColor_t2104014689::get_offset_of_calculationB_46(),
	CurveColor_t2104014689::get_offset_of_curveA_47(),
	CurveColor_t2104014689::get_offset_of_calculationA_48(),
	CurveColor_t2104014689::get_offset_of_clr_49(),
	CurveColor_t2104014689::get_offset_of_finishInNextStep_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (CurveFloat_t2152879844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[6] = 
{
	CurveFloat_t2152879844::get_offset_of_floatVariable_38(),
	CurveFloat_t2152879844::get_offset_of_fromValue_39(),
	CurveFloat_t2152879844::get_offset_of_toValue_40(),
	CurveFloat_t2152879844::get_offset_of_animCurve_41(),
	CurveFloat_t2152879844::get_offset_of_calculation_42(),
	CurveFloat_t2152879844::get_offset_of_finishInNextStep_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (CurveFsmAction_t928230875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[24] = 
{
	CurveFsmAction_t928230875::get_offset_of_time_14(),
	CurveFsmAction_t928230875::get_offset_of_speed_15(),
	CurveFsmAction_t928230875::get_offset_of_delay_16(),
	CurveFsmAction_t928230875::get_offset_of_ignoreCurveOffset_17(),
	CurveFsmAction_t928230875::get_offset_of_finishEvent_18(),
	CurveFsmAction_t928230875::get_offset_of_realTime_19(),
	CurveFsmAction_t928230875::get_offset_of_startTime_20(),
	CurveFsmAction_t928230875::get_offset_of_currentTime_21(),
	CurveFsmAction_t928230875::get_offset_of_endTimes_22(),
	CurveFsmAction_t928230875::get_offset_of_lastTime_23(),
	CurveFsmAction_t928230875::get_offset_of_deltaTime_24(),
	CurveFsmAction_t928230875::get_offset_of_delayTime_25(),
	CurveFsmAction_t928230875::get_offset_of_keyOffsets_26(),
	CurveFsmAction_t928230875::get_offset_of_curves_27(),
	CurveFsmAction_t928230875::get_offset_of_calculations_28(),
	CurveFsmAction_t928230875::get_offset_of_resultFloats_29(),
	CurveFsmAction_t928230875::get_offset_of_fromFloats_30(),
	CurveFsmAction_t928230875::get_offset_of_toFloats_31(),
	CurveFsmAction_t928230875::get_offset_of_distances_32(),
	CurveFsmAction_t928230875::get_offset_of_finishAction_33(),
	CurveFsmAction_t928230875::get_offset_of_isRunning_34(),
	CurveFsmAction_t928230875::get_offset_of_looping_35(),
	CurveFsmAction_t928230875::get_offset_of_start_36(),
	CurveFsmAction_t928230875::get_offset_of_largestEndTime_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (Calculation_t4245531681)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3014[8] = 
{
	Calculation_t4245531681::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (CurveRect_t119892750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[13] = 
{
	CurveRect_t119892750::get_offset_of_rectVariable_38(),
	CurveRect_t119892750::get_offset_of_fromValue_39(),
	CurveRect_t119892750::get_offset_of_toValue_40(),
	CurveRect_t119892750::get_offset_of_curveX_41(),
	CurveRect_t119892750::get_offset_of_calculationX_42(),
	CurveRect_t119892750::get_offset_of_curveY_43(),
	CurveRect_t119892750::get_offset_of_calculationY_44(),
	CurveRect_t119892750::get_offset_of_curveW_45(),
	CurveRect_t119892750::get_offset_of_calculationW_46(),
	CurveRect_t119892750::get_offset_of_curveH_47(),
	CurveRect_t119892750::get_offset_of_calculationH_48(),
	CurveRect_t119892750::get_offset_of_rct_49(),
	CurveRect_t119892750::get_offset_of_finishInNextStep_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (CurveVector3_t247899229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[11] = 
{
	CurveVector3_t247899229::get_offset_of_vectorVariable_38(),
	CurveVector3_t247899229::get_offset_of_fromValue_39(),
	CurveVector3_t247899229::get_offset_of_toValue_40(),
	CurveVector3_t247899229::get_offset_of_curveX_41(),
	CurveVector3_t247899229::get_offset_of_calculationX_42(),
	CurveVector3_t247899229::get_offset_of_curveY_43(),
	CurveVector3_t247899229::get_offset_of_calculationY_44(),
	CurveVector3_t247899229::get_offset_of_curveZ_45(),
	CurveVector3_t247899229::get_offset_of_calculationZ_46(),
	CurveVector3_t247899229::get_offset_of_vct_47(),
	CurveVector3_t247899229::get_offset_of_finishInNextStep_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (EaseColor_t1835015595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[4] = 
{
	EaseColor_t1835015595::get_offset_of_fromValue_35(),
	EaseColor_t1835015595::get_offset_of_toValue_36(),
	EaseColor_t1835015595::get_offset_of_colorVariable_37(),
	EaseColor_t1835015595::get_offset_of_finishInNextStep_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (EaseFloat_t3247924017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[4] = 
{
	EaseFloat_t3247924017::get_offset_of_fromValue_35(),
	EaseFloat_t3247924017::get_offset_of_toValue_36(),
	EaseFloat_t3247924017::get_offset_of_floatVariable_37(),
	EaseFloat_t3247924017::get_offset_of_finishInNextStep_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (EaseFsmAction_t2585076379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[21] = 
{
	EaseFsmAction_t2585076379::get_offset_of_time_14(),
	EaseFsmAction_t2585076379::get_offset_of_speed_15(),
	EaseFsmAction_t2585076379::get_offset_of_delay_16(),
	EaseFsmAction_t2585076379::get_offset_of_easeType_17(),
	EaseFsmAction_t2585076379::get_offset_of_reverse_18(),
	EaseFsmAction_t2585076379::get_offset_of_finishEvent_19(),
	EaseFsmAction_t2585076379::get_offset_of_realTime_20(),
	EaseFsmAction_t2585076379::get_offset_of_ease_21(),
	EaseFsmAction_t2585076379::get_offset_of_runningTime_22(),
	EaseFsmAction_t2585076379::get_offset_of_lastTime_23(),
	EaseFsmAction_t2585076379::get_offset_of_startTime_24(),
	EaseFsmAction_t2585076379::get_offset_of_deltaTime_25(),
	EaseFsmAction_t2585076379::get_offset_of_delayTime_26(),
	EaseFsmAction_t2585076379::get_offset_of_percentage_27(),
	EaseFsmAction_t2585076379::get_offset_of_fromFloats_28(),
	EaseFsmAction_t2585076379::get_offset_of_toFloats_29(),
	EaseFsmAction_t2585076379::get_offset_of_resultFloats_30(),
	EaseFsmAction_t2585076379::get_offset_of_finishAction_31(),
	EaseFsmAction_t2585076379::get_offset_of_start_32(),
	EaseFsmAction_t2585076379::get_offset_of_finished_33(),
	EaseFsmAction_t2585076379::get_offset_of_isRunning_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (EasingFunction_t338733390), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (EaseType_t1028795025)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3021[30] = 
{
	EaseType_t1028795025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (EaseRect_t3338712696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[4] = 
{
	EaseRect_t3338712696::get_offset_of_fromValue_35(),
	EaseRect_t3338712696::get_offset_of_toValue_36(),
	EaseRect_t3338712696::get_offset_of_rectVariable_37(),
	EaseRect_t3338712696::get_offset_of_finishInNextStep_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (EaseVector3_t2036336192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[4] = 
{
	EaseVector3_t2036336192::get_offset_of_fromValue_35(),
	EaseVector3_t2036336192::get_offset_of_toValue_36(),
	EaseVector3_t2036336192::get_offset_of_vector3Variable_37(),
	EaseVector3_t2036336192::get_offset_of_finishInNextStep_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (AddAnimationClip_t1597356349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[6] = 
{
	AddAnimationClip_t1597356349::get_offset_of_gameObject_14(),
	AddAnimationClip_t1597356349::get_offset_of_animationClip_15(),
	AddAnimationClip_t1597356349::get_offset_of_animationName_16(),
	AddAnimationClip_t1597356349::get_offset_of_firstFrame_17(),
	AddAnimationClip_t1597356349::get_offset_of_lastFrame_18(),
	AddAnimationClip_t1597356349::get_offset_of_addLoopFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (AddMixingTransform_t4262639660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[4] = 
{
	AddMixingTransform_t4262639660::get_offset_of_gameObject_16(),
	AddMixingTransform_t4262639660::get_offset_of_animationName_17(),
	AddMixingTransform_t4262639660::get_offset_of_transform_18(),
	AddMixingTransform_t4262639660::get_offset_of_recursive_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (AnimationSettings_t424863638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[6] = 
{
	AnimationSettings_t424863638::get_offset_of_gameObject_16(),
	AnimationSettings_t424863638::get_offset_of_animName_17(),
	AnimationSettings_t424863638::get_offset_of_wrapMode_18(),
	AnimationSettings_t424863638::get_offset_of_blendMode_19(),
	AnimationSettings_t424863638::get_offset_of_speed_20(),
	AnimationSettings_t424863638::get_offset_of_layer_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (BaseAnimationAction_t1134898484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (BlendAnimation_t985073980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[6] = 
{
	BlendAnimation_t985073980::get_offset_of_gameObject_16(),
	BlendAnimation_t985073980::get_offset_of_animName_17(),
	BlendAnimation_t985073980::get_offset_of_targetWeight_18(),
	BlendAnimation_t985073980::get_offset_of_time_19(),
	BlendAnimation_t985073980::get_offset_of_finishEvent_20(),
	BlendAnimation_t985073980::get_offset_of_delayedFinishEvent_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (CapturePoseAsAnimationClip_t1458556555), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[5] = 
{
	CapturePoseAsAnimationClip_t1458556555::get_offset_of_gameObject_14(),
	CapturePoseAsAnimationClip_t1458556555::get_offset_of_position_15(),
	CapturePoseAsAnimationClip_t1458556555::get_offset_of_rotation_16(),
	CapturePoseAsAnimationClip_t1458556555::get_offset_of_scale_17(),
	CapturePoseAsAnimationClip_t1458556555::get_offset_of_storeAnimationClip_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (EnableAnimation_t2755334398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[5] = 
{
	EnableAnimation_t2755334398::get_offset_of_gameObject_16(),
	EnableAnimation_t2755334398::get_offset_of_animName_17(),
	EnableAnimation_t2755334398::get_offset_of_enable_18(),
	EnableAnimation_t2755334398::get_offset_of_resetOnExit_19(),
	EnableAnimation_t2755334398::get_offset_of_anim_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (PlayAnimation_t4013957021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[9] = 
{
	PlayAnimation_t4013957021::get_offset_of_gameObject_16(),
	PlayAnimation_t4013957021::get_offset_of_animName_17(),
	PlayAnimation_t4013957021::get_offset_of_playMode_18(),
	PlayAnimation_t4013957021::get_offset_of_blendTime_19(),
	PlayAnimation_t4013957021::get_offset_of_finishEvent_20(),
	PlayAnimation_t4013957021::get_offset_of_loopEvent_21(),
	PlayAnimation_t4013957021::get_offset_of_stopOnExit_22(),
	PlayAnimation_t4013957021::get_offset_of_anim_23(),
	PlayAnimation_t4013957021::get_offset_of_prevAnimtTime_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (PlayRandomAnimation_t3880340942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[10] = 
{
	PlayRandomAnimation_t3880340942::get_offset_of_gameObject_16(),
	PlayRandomAnimation_t3880340942::get_offset_of_animations_17(),
	PlayRandomAnimation_t3880340942::get_offset_of_weights_18(),
	PlayRandomAnimation_t3880340942::get_offset_of_playMode_19(),
	PlayRandomAnimation_t3880340942::get_offset_of_blendTime_20(),
	PlayRandomAnimation_t3880340942::get_offset_of_finishEvent_21(),
	PlayRandomAnimation_t3880340942::get_offset_of_loopEvent_22(),
	PlayRandomAnimation_t3880340942::get_offset_of_stopOnExit_23(),
	PlayRandomAnimation_t3880340942::get_offset_of_anim_24(),
	PlayRandomAnimation_t3880340942::get_offset_of_prevAnimtTime_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (RemoveMixingTransform_t466720318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[3] = 
{
	RemoveMixingTransform_t466720318::get_offset_of_gameObject_16(),
	RemoveMixingTransform_t466720318::get_offset_of_animationName_17(),
	RemoveMixingTransform_t466720318::get_offset_of_transfrom_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (RewindAnimation_t4084631938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[2] = 
{
	RewindAnimation_t4084631938::get_offset_of_gameObject_16(),
	RewindAnimation_t4084631938::get_offset_of_animName_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (SetAnimationSpeed_t585003002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[4] = 
{
	SetAnimationSpeed_t585003002::get_offset_of_gameObject_16(),
	SetAnimationSpeed_t585003002::get_offset_of_animName_17(),
	SetAnimationSpeed_t585003002::get_offset_of_speed_18(),
	SetAnimationSpeed_t585003002::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (SetAnimationTime_t1212068435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[5] = 
{
	SetAnimationTime_t1212068435::get_offset_of_gameObject_16(),
	SetAnimationTime_t1212068435::get_offset_of_animName_17(),
	SetAnimationTime_t1212068435::get_offset_of_time_18(),
	SetAnimationTime_t1212068435::get_offset_of_normalized_19(),
	SetAnimationTime_t1212068435::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (SetAnimationWeight_t2943869022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[4] = 
{
	SetAnimationWeight_t2943869022::get_offset_of_gameObject_16(),
	SetAnimationWeight_t2943869022::get_offset_of_animName_17(),
	SetAnimationWeight_t2943869022::get_offset_of_weight_18(),
	SetAnimationWeight_t2943869022::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (StopAnimation_t2812777501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[2] = 
{
	StopAnimation_t2812777501::get_offset_of_gameObject_16(),
	StopAnimation_t2812777501::get_offset_of_animName_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (AnimatorCrossFade_t3536162959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[6] = 
{
	AnimatorCrossFade_t3536162959::get_offset_of_gameObject_14(),
	AnimatorCrossFade_t3536162959::get_offset_of_stateName_15(),
	AnimatorCrossFade_t3536162959::get_offset_of_transitionDuration_16(),
	AnimatorCrossFade_t3536162959::get_offset_of_layer_17(),
	AnimatorCrossFade_t3536162959::get_offset_of_normalizedTime_18(),
	AnimatorCrossFade_t3536162959::get_offset_of__animator_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (AnimatorInterruptMatchTarget_t173563957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[2] = 
{
	AnimatorInterruptMatchTarget_t173563957::get_offset_of_gameObject_14(),
	AnimatorInterruptMatchTarget_t173563957::get_offset_of_completeMatch_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (AnimatorMatchTarget_t3919190371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[12] = 
{
	AnimatorMatchTarget_t3919190371::get_offset_of_gameObject_14(),
	AnimatorMatchTarget_t3919190371::get_offset_of_bodyPart_15(),
	AnimatorMatchTarget_t3919190371::get_offset_of_target_16(),
	AnimatorMatchTarget_t3919190371::get_offset_of_targetPosition_17(),
	AnimatorMatchTarget_t3919190371::get_offset_of_targetRotation_18(),
	AnimatorMatchTarget_t3919190371::get_offset_of_positionWeight_19(),
	AnimatorMatchTarget_t3919190371::get_offset_of_rotationWeight_20(),
	AnimatorMatchTarget_t3919190371::get_offset_of_startNormalizedTime_21(),
	AnimatorMatchTarget_t3919190371::get_offset_of_targetNormalizedTime_22(),
	AnimatorMatchTarget_t3919190371::get_offset_of_everyFrame_23(),
	AnimatorMatchTarget_t3919190371::get_offset_of__animator_24(),
	AnimatorMatchTarget_t3919190371::get_offset_of__transform_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (AnimatorPlay_t2112008182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[6] = 
{
	AnimatorPlay_t2112008182::get_offset_of_gameObject_14(),
	AnimatorPlay_t2112008182::get_offset_of_stateName_15(),
	AnimatorPlay_t2112008182::get_offset_of_layer_16(),
	AnimatorPlay_t2112008182::get_offset_of_normalizedTime_17(),
	AnimatorPlay_t2112008182::get_offset_of_everyFrame_18(),
	AnimatorPlay_t2112008182::get_offset_of__animator_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (AnimatorStartPlayback_t3399045872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[1] = 
{
	AnimatorStartPlayback_t3399045872::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (AnimatorStartRecording_t3982064907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[2] = 
{
	AnimatorStartRecording_t3982064907::get_offset_of_gameObject_14(),
	AnimatorStartRecording_t3982064907::get_offset_of_frameCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (AnimatorStopPlayback_t184465721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[1] = 
{
	AnimatorStopPlayback_t184465721::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (AnimatorStopRecording_t646066457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3046[3] = 
{
	AnimatorStopRecording_t646066457::get_offset_of_gameObject_14(),
	AnimatorStopRecording_t646066457::get_offset_of_recorderStartTime_15(),
	AnimatorStopRecording_t646066457::get_offset_of_recorderStopTime_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (FsmStateActionAnimatorBase_t3384377168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[3] = 
{
	FsmStateActionAnimatorBase_t3384377168::get_offset_of_everyFrame_14(),
	FsmStateActionAnimatorBase_t3384377168::get_offset_of_everyFrameOption_15(),
	FsmStateActionAnimatorBase_t3384377168::get_offset_of_IklayerIndex_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (AnimatorFrameUpdateSelector_t3520526877)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3048[4] = 
{
	AnimatorFrameUpdateSelector_t3520526877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (GetAnimatorApplyRootMotion_t985245695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[5] = 
{
	GetAnimatorApplyRootMotion_t985245695::get_offset_of_gameObject_14(),
	GetAnimatorApplyRootMotion_t985245695::get_offset_of_rootMotionApplied_15(),
	GetAnimatorApplyRootMotion_t985245695::get_offset_of_rootMotionIsAppliedEvent_16(),
	GetAnimatorApplyRootMotion_t985245695::get_offset_of_rootMotionIsNotAppliedEvent_17(),
	GetAnimatorApplyRootMotion_t985245695::get_offset_of__animator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (GetAnimatorBody_t3473142879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[6] = 
{
	GetAnimatorBody_t3473142879::get_offset_of_gameObject_17(),
	GetAnimatorBody_t3473142879::get_offset_of_bodyPosition_18(),
	GetAnimatorBody_t3473142879::get_offset_of_bodyRotation_19(),
	GetAnimatorBody_t3473142879::get_offset_of_bodyGameObject_20(),
	GetAnimatorBody_t3473142879::get_offset_of__animator_21(),
	GetAnimatorBody_t3473142879::get_offset_of__transform_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (GetAnimatorBoneGameObject_t1423048408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[4] = 
{
	GetAnimatorBoneGameObject_t1423048408::get_offset_of_gameObject_14(),
	GetAnimatorBoneGameObject_t1423048408::get_offset_of_bone_15(),
	GetAnimatorBoneGameObject_t1423048408::get_offset_of_boneGameObject_16(),
	GetAnimatorBoneGameObject_t1423048408::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (GetAnimatorBool_t3900109898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[5] = 
{
	GetAnimatorBool_t3900109898::get_offset_of_gameObject_17(),
	GetAnimatorBool_t3900109898::get_offset_of_parameter_18(),
	GetAnimatorBool_t3900109898::get_offset_of_result_19(),
	GetAnimatorBool_t3900109898::get_offset_of__animator_20(),
	GetAnimatorBool_t3900109898::get_offset_of__paramID_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (GetAnimatorCullingMode_t2982375881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[5] = 
{
	GetAnimatorCullingMode_t2982375881::get_offset_of_gameObject_14(),
	GetAnimatorCullingMode_t2982375881::get_offset_of_alwaysAnimate_15(),
	GetAnimatorCullingMode_t2982375881::get_offset_of_alwaysAnimateEvent_16(),
	GetAnimatorCullingMode_t2982375881::get_offset_of_basedOnRenderersEvent_17(),
	GetAnimatorCullingMode_t2982375881::get_offset_of__animator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (GetAnimatorCurrentStateInfo_t3870799634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3054[13] = 
{
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_gameObject_17(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_layerIndex_18(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_name_19(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_nameHash_20(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_fullPathHash_21(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_shortPathHash_22(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_tagHash_23(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_isStateLooping_24(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_length_25(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_normalizedTime_26(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_loopCount_27(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of_currentLoopProgress_28(),
	GetAnimatorCurrentStateInfo_t3870799634::get_offset_of__animator_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (GetAnimatorCurrentStateInfoIsName_t1291145368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[7] = 
{
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of_gameObject_17(),
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of_layerIndex_18(),
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of_name_19(),
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of_isMatching_20(),
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of_nameMatchEvent_21(),
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of_nameDoNotMatchEvent_22(),
	GetAnimatorCurrentStateInfoIsName_t1291145368::get_offset_of__animator_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (GetAnimatorCurrentStateInfoIsTag_t1935834350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[7] = 
{
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of_gameObject_17(),
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of_layerIndex_18(),
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of_tag_19(),
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of_tagMatch_20(),
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of_tagMatchEvent_21(),
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of_tagDoNotMatchEvent_22(),
	GetAnimatorCurrentStateInfoIsTag_t1935834350::get_offset_of__animator_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (GetAnimatorCurrentTransitionInfo_t1367284229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[7] = 
{
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of_gameObject_17(),
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of_layerIndex_18(),
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of_name_19(),
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of_nameHash_20(),
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of_userNameHash_21(),
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of_normalizedTime_22(),
	GetAnimatorCurrentTransitionInfo_t1367284229::get_offset_of__animator_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (GetAnimatorCurrentTransitionInfoIsName_t1387398839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[7] = 
{
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of_gameObject_17(),
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of_layerIndex_18(),
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of_name_19(),
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of_nameMatch_20(),
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of_nameMatchEvent_21(),
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of_nameDoNotMatchEvent_22(),
	GetAnimatorCurrentTransitionInfoIsName_t1387398839::get_offset_of__animator_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (GetAnimatorCurrentTransitionInfoIsUserName_t596063340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[7] = 
{
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of_gameObject_17(),
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of_layerIndex_18(),
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of_userName_19(),
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of_nameMatch_20(),
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of_nameMatchEvent_21(),
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of_nameDoNotMatchEvent_22(),
	GetAnimatorCurrentTransitionInfoIsUserName_t596063340::get_offset_of__animator_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (GetAnimatorDelta_t2748201152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[4] = 
{
	GetAnimatorDelta_t2748201152::get_offset_of_gameObject_17(),
	GetAnimatorDelta_t2748201152::get_offset_of_deltaPosition_18(),
	GetAnimatorDelta_t2748201152::get_offset_of_deltaRotation_19(),
	GetAnimatorDelta_t2748201152::get_offset_of__animator_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (GetAnimatorFeetPivotActive_t2091396342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[3] = 
{
	GetAnimatorFeetPivotActive_t2091396342::get_offset_of_gameObject_14(),
	GetAnimatorFeetPivotActive_t2091396342::get_offset_of_feetPivotActive_15(),
	GetAnimatorFeetPivotActive_t2091396342::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (GetAnimatorFloat_t2318297330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[5] = 
{
	GetAnimatorFloat_t2318297330::get_offset_of_gameObject_17(),
	GetAnimatorFloat_t2318297330::get_offset_of_parameter_18(),
	GetAnimatorFloat_t2318297330::get_offset_of_result_19(),
	GetAnimatorFloat_t2318297330::get_offset_of__animator_20(),
	GetAnimatorFloat_t2318297330::get_offset_of__paramID_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (GetAnimatorGravityWeight_t130115598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[3] = 
{
	GetAnimatorGravityWeight_t130115598::get_offset_of_gameObject_17(),
	GetAnimatorGravityWeight_t130115598::get_offset_of_gravityWeight_18(),
	GetAnimatorGravityWeight_t130115598::get_offset_of__animator_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (GetAnimatorHumanScale_t2710047612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[3] = 
{
	GetAnimatorHumanScale_t2710047612::get_offset_of_gameObject_14(),
	GetAnimatorHumanScale_t2710047612::get_offset_of_humanScale_15(),
	GetAnimatorHumanScale_t2710047612::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (GetAnimatorIKGoal_t2372078135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[10] = 
{
	GetAnimatorIKGoal_t2372078135::get_offset_of_gameObject_17(),
	GetAnimatorIKGoal_t2372078135::get_offset_of_iKGoal_18(),
	GetAnimatorIKGoal_t2372078135::get_offset_of_goal_19(),
	GetAnimatorIKGoal_t2372078135::get_offset_of_position_20(),
	GetAnimatorIKGoal_t2372078135::get_offset_of_rotation_21(),
	GetAnimatorIKGoal_t2372078135::get_offset_of_positionWeight_22(),
	GetAnimatorIKGoal_t2372078135::get_offset_of_rotationWeight_23(),
	GetAnimatorIKGoal_t2372078135::get_offset_of__animator_24(),
	GetAnimatorIKGoal_t2372078135::get_offset_of__transform_25(),
	GetAnimatorIKGoal_t2372078135::get_offset_of__iKGoal_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (GetAnimatorInt_t1761492912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[5] = 
{
	GetAnimatorInt_t1761492912::get_offset_of_gameObject_17(),
	GetAnimatorInt_t1761492912::get_offset_of_parameter_18(),
	GetAnimatorInt_t1761492912::get_offset_of_result_19(),
	GetAnimatorInt_t1761492912::get_offset_of__animator_20(),
	GetAnimatorInt_t1761492912::get_offset_of__paramID_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (GetAnimatorIsHuman_t378752370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[5] = 
{
	GetAnimatorIsHuman_t378752370::get_offset_of_gameObject_14(),
	GetAnimatorIsHuman_t378752370::get_offset_of_isHuman_15(),
	GetAnimatorIsHuman_t378752370::get_offset_of_isHumanEvent_16(),
	GetAnimatorIsHuman_t378752370::get_offset_of_isGenericEvent_17(),
	GetAnimatorIsHuman_t378752370::get_offset_of__animator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (GetAnimatorIsLayerInTransition_t3891240677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3068[6] = 
{
	GetAnimatorIsLayerInTransition_t3891240677::get_offset_of_gameObject_17(),
	GetAnimatorIsLayerInTransition_t3891240677::get_offset_of_layerIndex_18(),
	GetAnimatorIsLayerInTransition_t3891240677::get_offset_of_isInTransition_19(),
	GetAnimatorIsLayerInTransition_t3891240677::get_offset_of_isInTransitionEvent_20(),
	GetAnimatorIsLayerInTransition_t3891240677::get_offset_of_isNotInTransitionEvent_21(),
	GetAnimatorIsLayerInTransition_t3891240677::get_offset_of__animator_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (GetAnimatorIsMatchingTarget_t3396280219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[5] = 
{
	GetAnimatorIsMatchingTarget_t3396280219::get_offset_of_gameObject_17(),
	GetAnimatorIsMatchingTarget_t3396280219::get_offset_of_isMatchingActive_18(),
	GetAnimatorIsMatchingTarget_t3396280219::get_offset_of_matchingActivatedEvent_19(),
	GetAnimatorIsMatchingTarget_t3396280219::get_offset_of_matchingDeactivedEvent_20(),
	GetAnimatorIsMatchingTarget_t3396280219::get_offset_of__animator_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (GetAnimatorIsParameterControlledByCurve_t745733894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[6] = 
{
	GetAnimatorIsParameterControlledByCurve_t745733894::get_offset_of_gameObject_14(),
	GetAnimatorIsParameterControlledByCurve_t745733894::get_offset_of_parameterName_15(),
	GetAnimatorIsParameterControlledByCurve_t745733894::get_offset_of_isControlledByCurve_16(),
	GetAnimatorIsParameterControlledByCurve_t745733894::get_offset_of_isControlledByCurveEvent_17(),
	GetAnimatorIsParameterControlledByCurve_t745733894::get_offset_of_isNotControlledByCurveEvent_18(),
	GetAnimatorIsParameterControlledByCurve_t745733894::get_offset_of__animator_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (GetAnimatorLayerCount_t4239821965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[3] = 
{
	GetAnimatorLayerCount_t4239821965::get_offset_of_gameObject_14(),
	GetAnimatorLayerCount_t4239821965::get_offset_of_layerCount_15(),
	GetAnimatorLayerCount_t4239821965::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (GetAnimatorLayerName_t3204126378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[4] = 
{
	GetAnimatorLayerName_t3204126378::get_offset_of_gameObject_14(),
	GetAnimatorLayerName_t3204126378::get_offset_of_layerIndex_15(),
	GetAnimatorLayerName_t3204126378::get_offset_of_layerName_16(),
	GetAnimatorLayerName_t3204126378::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (GetAnimatorLayersAffectMassCenter_t1877605656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[5] = 
{
	GetAnimatorLayersAffectMassCenter_t1877605656::get_offset_of_gameObject_14(),
	GetAnimatorLayersAffectMassCenter_t1877605656::get_offset_of_affectMassCenter_15(),
	GetAnimatorLayersAffectMassCenter_t1877605656::get_offset_of_affectMassCenterEvent_16(),
	GetAnimatorLayersAffectMassCenter_t1877605656::get_offset_of_doNotAffectMassCenterEvent_17(),
	GetAnimatorLayersAffectMassCenter_t1877605656::get_offset_of__animator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (GetAnimatorLayerWeight_t4155071065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[4] = 
{
	GetAnimatorLayerWeight_t4155071065::get_offset_of_gameObject_17(),
	GetAnimatorLayerWeight_t4155071065::get_offset_of_layerIndex_18(),
	GetAnimatorLayerWeight_t4155071065::get_offset_of_layerWeight_19(),
	GetAnimatorLayerWeight_t4155071065::get_offset_of__animator_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (GetAnimatorLeftFootBottomHeight_t1731074969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[4] = 
{
	GetAnimatorLeftFootBottomHeight_t1731074969::get_offset_of_gameObject_14(),
	GetAnimatorLeftFootBottomHeight_t1731074969::get_offset_of_leftFootHeight_15(),
	GetAnimatorLeftFootBottomHeight_t1731074969::get_offset_of_everyFrame_16(),
	GetAnimatorLeftFootBottomHeight_t1731074969::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (GetAnimatorNextStateInfo_t936547330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[13] = 
{
	GetAnimatorNextStateInfo_t936547330::get_offset_of_gameObject_17(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_layerIndex_18(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_name_19(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_nameHash_20(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_fullPathHash_21(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_shortPathHash_22(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_tagHash_23(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_isStateLooping_24(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_length_25(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_normalizedTime_26(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_loopCount_27(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of_currentLoopProgress_28(),
	GetAnimatorNextStateInfo_t936547330::get_offset_of__animator_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (GetAnimatorPivot_t1085583339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[4] = 
{
	GetAnimatorPivot_t1085583339::get_offset_of_gameObject_17(),
	GetAnimatorPivot_t1085583339::get_offset_of_pivotWeight_18(),
	GetAnimatorPivot_t1085583339::get_offset_of_pivotPosition_19(),
	GetAnimatorPivot_t1085583339::get_offset_of__animator_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (GetAnimatorPlayBackSpeed_t335189200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[4] = 
{
	GetAnimatorPlayBackSpeed_t335189200::get_offset_of_gameObject_14(),
	GetAnimatorPlayBackSpeed_t335189200::get_offset_of_playBackSpeed_15(),
	GetAnimatorPlayBackSpeed_t335189200::get_offset_of_everyFrame_16(),
	GetAnimatorPlayBackSpeed_t335189200::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (GetAnimatorPlayBackTime_t3615175085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[4] = 
{
	GetAnimatorPlayBackTime_t3615175085::get_offset_of_gameObject_14(),
	GetAnimatorPlayBackTime_t3615175085::get_offset_of_playBackTime_15(),
	GetAnimatorPlayBackTime_t3615175085::get_offset_of_everyFrame_16(),
	GetAnimatorPlayBackTime_t3615175085::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (GetAnimatorRightFootBottomHeight_t2632401101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[4] = 
{
	GetAnimatorRightFootBottomHeight_t2632401101::get_offset_of_gameObject_14(),
	GetAnimatorRightFootBottomHeight_t2632401101::get_offset_of_rightFootHeight_15(),
	GetAnimatorRightFootBottomHeight_t2632401101::get_offset_of_everyFrame_16(),
	GetAnimatorRightFootBottomHeight_t2632401101::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (GetAnimatorRoot_t3934712930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[6] = 
{
	GetAnimatorRoot_t3934712930::get_offset_of_gameObject_17(),
	GetAnimatorRoot_t3934712930::get_offset_of_rootPosition_18(),
	GetAnimatorRoot_t3934712930::get_offset_of_rootRotation_19(),
	GetAnimatorRoot_t3934712930::get_offset_of_bodyGameObject_20(),
	GetAnimatorRoot_t3934712930::get_offset_of__animator_21(),
	GetAnimatorRoot_t3934712930::get_offset_of__transform_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (GetAnimatorSpeed_t2658983986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[3] = 
{
	GetAnimatorSpeed_t2658983986::get_offset_of_gameObject_17(),
	GetAnimatorSpeed_t2658983986::get_offset_of_speed_18(),
	GetAnimatorSpeed_t2658983986::get_offset_of__animator_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (GetAnimatorTarget_t2390406205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[6] = 
{
	GetAnimatorTarget_t2390406205::get_offset_of_gameObject_17(),
	GetAnimatorTarget_t2390406205::get_offset_of_targetPosition_18(),
	GetAnimatorTarget_t2390406205::get_offset_of_targetRotation_19(),
	GetAnimatorTarget_t2390406205::get_offset_of_targetGameObject_20(),
	GetAnimatorTarget_t2390406205::get_offset_of__animator_21(),
	GetAnimatorTarget_t2390406205::get_offset_of__transform_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (NavMeshAgentAnimatorSynchronizer_t1968050959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[4] = 
{
	NavMeshAgentAnimatorSynchronizer_t1968050959::get_offset_of_gameObject_14(),
	NavMeshAgentAnimatorSynchronizer_t1968050959::get_offset_of__animator_15(),
	NavMeshAgentAnimatorSynchronizer_t1968050959::get_offset_of__agent_16(),
	NavMeshAgentAnimatorSynchronizer_t1968050959::get_offset_of__trans_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (SetAnimatorApplyRootMotion_t437523402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[3] = 
{
	SetAnimatorApplyRootMotion_t437523402::get_offset_of_gameObject_14(),
	SetAnimatorApplyRootMotion_t437523402::get_offset_of_applyRootMotion_15(),
	SetAnimatorApplyRootMotion_t437523402::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (SetAnimatorBody_t2450485891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3086[7] = 
{
	SetAnimatorBody_t2450485891::get_offset_of_gameObject_14(),
	SetAnimatorBody_t2450485891::get_offset_of_target_15(),
	SetAnimatorBody_t2450485891::get_offset_of_position_16(),
	SetAnimatorBody_t2450485891::get_offset_of_rotation_17(),
	SetAnimatorBody_t2450485891::get_offset_of_everyFrame_18(),
	SetAnimatorBody_t2450485891::get_offset_of__animator_19(),
	SetAnimatorBody_t2450485891::get_offset_of__transform_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (SetAnimatorBool_t1258844782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3087[5] = 
{
	SetAnimatorBool_t1258844782::get_offset_of_gameObject_17(),
	SetAnimatorBool_t1258844782::get_offset_of_parameter_18(),
	SetAnimatorBool_t1258844782::get_offset_of_Value_19(),
	SetAnimatorBool_t1258844782::get_offset_of__animator_20(),
	SetAnimatorBool_t1258844782::get_offset_of__paramID_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (SetAnimatorCullingMode_t2980569892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3088[3] = 
{
	SetAnimatorCullingMode_t2980569892::get_offset_of_gameObject_14(),
	SetAnimatorCullingMode_t2980569892::get_offset_of_alwaysAnimate_15(),
	SetAnimatorCullingMode_t2980569892::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (SetAnimatorFeetPivotActive_t937086675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[3] = 
{
	SetAnimatorFeetPivotActive_t937086675::get_offset_of_gameObject_14(),
	SetAnimatorFeetPivotActive_t937086675::get_offset_of_feetPivotActive_15(),
	SetAnimatorFeetPivotActive_t937086675::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (SetAnimatorFloat_t3971999510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[6] = 
{
	SetAnimatorFloat_t3971999510::get_offset_of_gameObject_17(),
	SetAnimatorFloat_t3971999510::get_offset_of_parameter_18(),
	SetAnimatorFloat_t3971999510::get_offset_of_Value_19(),
	SetAnimatorFloat_t3971999510::get_offset_of_dampTime_20(),
	SetAnimatorFloat_t3971999510::get_offset_of__animator_21(),
	SetAnimatorFloat_t3971999510::get_offset_of__paramID_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (SetAnimatorIKGoal_t839890372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[10] = 
{
	SetAnimatorIKGoal_t839890372::get_offset_of_gameObject_14(),
	SetAnimatorIKGoal_t839890372::get_offset_of_iKGoal_15(),
	SetAnimatorIKGoal_t839890372::get_offset_of_goal_16(),
	SetAnimatorIKGoal_t839890372::get_offset_of_position_17(),
	SetAnimatorIKGoal_t839890372::get_offset_of_rotation_18(),
	SetAnimatorIKGoal_t839890372::get_offset_of_positionWeight_19(),
	SetAnimatorIKGoal_t839890372::get_offset_of_rotationWeight_20(),
	SetAnimatorIKGoal_t839890372::get_offset_of_everyFrame_21(),
	SetAnimatorIKGoal_t839890372::get_offset_of__animator_22(),
	SetAnimatorIKGoal_t839890372::get_offset_of__transform_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (SetAnimatorInt_t738835924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[5] = 
{
	SetAnimatorInt_t738835924::get_offset_of_gameObject_17(),
	SetAnimatorInt_t738835924::get_offset_of_parameter_18(),
	SetAnimatorInt_t738835924::get_offset_of_Value_19(),
	SetAnimatorInt_t738835924::get_offset_of__animator_20(),
	SetAnimatorInt_t738835924::get_offset_of__paramID_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (SetAnimatorLayersAffectMassCenter_t405418020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[3] = 
{
	SetAnimatorLayersAffectMassCenter_t405418020::get_offset_of_gameObject_14(),
	SetAnimatorLayersAffectMassCenter_t405418020::get_offset_of_affectMassCenter_15(),
	SetAnimatorLayersAffectMassCenter_t405418020::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (SetAnimatorLayerWeight_t3884658471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[5] = 
{
	SetAnimatorLayerWeight_t3884658471::get_offset_of_gameObject_14(),
	SetAnimatorLayerWeight_t3884658471::get_offset_of_layerIndex_15(),
	SetAnimatorLayerWeight_t3884658471::get_offset_of_layerWeight_16(),
	SetAnimatorLayerWeight_t3884658471::get_offset_of_everyFrame_17(),
	SetAnimatorLayerWeight_t3884658471::get_offset_of__animator_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (SetAnimatorLookAt_t1141504004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[11] = 
{
	SetAnimatorLookAt_t1141504004::get_offset_of_gameObject_14(),
	SetAnimatorLookAt_t1141504004::get_offset_of_target_15(),
	SetAnimatorLookAt_t1141504004::get_offset_of_targetPosition_16(),
	SetAnimatorLookAt_t1141504004::get_offset_of_weight_17(),
	SetAnimatorLookAt_t1141504004::get_offset_of_bodyWeight_18(),
	SetAnimatorLookAt_t1141504004::get_offset_of_headWeight_19(),
	SetAnimatorLookAt_t1141504004::get_offset_of_eyesWeight_20(),
	SetAnimatorLookAt_t1141504004::get_offset_of_clampWeight_21(),
	SetAnimatorLookAt_t1141504004::get_offset_of_everyFrame_22(),
	SetAnimatorLookAt_t1141504004::get_offset_of__animator_23(),
	SetAnimatorLookAt_t1141504004::get_offset_of__transform_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (SetAnimatorPlayBackSpeed_t1779881157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[4] = 
{
	SetAnimatorPlayBackSpeed_t1779881157::get_offset_of_gameObject_14(),
	SetAnimatorPlayBackSpeed_t1779881157::get_offset_of_playBackSpeed_15(),
	SetAnimatorPlayBackSpeed_t1779881157::get_offset_of_everyFrame_16(),
	SetAnimatorPlayBackSpeed_t1779881157::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (SetAnimatorPlayBackTime_t3352620600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3097[4] = 
{
	SetAnimatorPlayBackTime_t3352620600::get_offset_of_gameObject_14(),
	SetAnimatorPlayBackTime_t3352620600::get_offset_of_playbackTime_15(),
	SetAnimatorPlayBackTime_t3352620600::get_offset_of_everyFrame_16(),
	SetAnimatorPlayBackTime_t3352620600::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (SetAnimatorSpeed_t185298894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3098[4] = 
{
	SetAnimatorSpeed_t185298894::get_offset_of_gameObject_14(),
	SetAnimatorSpeed_t185298894::get_offset_of_speed_15(),
	SetAnimatorSpeed_t185298894::get_offset_of_everyFrame_16(),
	SetAnimatorSpeed_t185298894::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (SetAnimatorStabilizeFeet_t2162779602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3099[3] = 
{
	SetAnimatorStabilizeFeet_t2162779602::get_offset_of_gameObject_14(),
	SetAnimatorStabilizeFeet_t2162779602::get_offset_of_stabilizeFeet_15(),
	SetAnimatorStabilizeFeet_t2162779602::get_offset_of__animator_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
