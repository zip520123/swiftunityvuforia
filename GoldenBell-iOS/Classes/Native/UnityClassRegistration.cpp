template <typename T> void RegisterUnityClass(const char*);
template <typename T> void RegisterStrippedType(int, const char*, const char*);

void InvokeRegisterStaticallyLinkedModuleClasses()
{
	// Do nothing (we're in stripping mode)
}

void RegisterStaticallyLinkedModulesGranular()
{
	void RegisterModule_SharedInternals();
	RegisterModule_SharedInternals();

	void RegisterModule_Core();
	RegisterModule_Core();

	void RegisterModule_AI();
	RegisterModule_AI();

	void RegisterModule_Animation();
	RegisterModule_Animation();

	void RegisterModule_Audio();
	RegisterModule_Audio();

	void RegisterModule_CloudWebServices();
	RegisterModule_CloudWebServices();

	void RegisterModule_Director();
	RegisterModule_Director();

	void RegisterModule_ParticleSystem();
	RegisterModule_ParticleSystem();

	void RegisterModule_Physics();
	RegisterModule_Physics();

	void RegisterModule_Physics2D();
	RegisterModule_Physics2D();

	void RegisterModule_TextRendering();
	RegisterModule_TextRendering();

	void RegisterModule_UI();
	RegisterModule_UI();

	void RegisterModule_UnityConnect();
	RegisterModule_UnityConnect();

	void RegisterModule_UnityAnalytics();
	RegisterModule_UnityAnalytics();

	void RegisterModule_PerformanceReporting();
	RegisterModule_PerformanceReporting();

	void RegisterModule_Video();
	RegisterModule_Video();

	void RegisterModule_IMGUI();
	RegisterModule_IMGUI();

	void RegisterModule_XR();
	RegisterModule_XR();

	void RegisterModule_ImageConversion();
	RegisterModule_ImageConversion();

	void RegisterModule_UnityWebRequest();
	RegisterModule_UnityWebRequest();

	void RegisterModule_ScreenCapture();
	RegisterModule_ScreenCapture();

	void RegisterModule_GameCenter();
	RegisterModule_GameCenter();

	void RegisterModule_JSONSerialize();
	RegisterModule_JSONSerialize();

	void RegisterModule_Input();
	RegisterModule_Input();

	void RegisterModule_VR();
	RegisterModule_VR();

	void RegisterModule_Wind();
	RegisterModule_Wind();

	void RegisterModule_TLS();
	RegisterModule_TLS();

}

class EditorExtension; template <> void RegisterUnityClass<EditorExtension>(const char*);
namespace Unity { class Component; } template <> void RegisterUnityClass<Unity::Component>(const char*);
class Behaviour; template <> void RegisterUnityClass<Behaviour>(const char*);
class Animation; template <> void RegisterUnityClass<Animation>(const char*);
class Animator; template <> void RegisterUnityClass<Animator>(const char*);
class AudioBehaviour; template <> void RegisterUnityClass<AudioBehaviour>(const char*);
class AudioListener; template <> void RegisterUnityClass<AudioListener>(const char*);
class AudioSource; template <> void RegisterUnityClass<AudioSource>(const char*);
class AudioFilter; 
class AudioChorusFilter; 
class AudioDistortionFilter; 
class AudioEchoFilter; 
class AudioHighPassFilter; 
class AudioLowPassFilter; 
class AudioReverbFilter; 
class AudioReverbZone; 
class Camera; template <> void RegisterUnityClass<Camera>(const char*);
namespace UI { class Canvas; } template <> void RegisterUnityClass<UI::Canvas>(const char*);
namespace UI { class CanvasGroup; } template <> void RegisterUnityClass<UI::CanvasGroup>(const char*);
namespace Unity { class Cloth; } 
class Collider2D; template <> void RegisterUnityClass<Collider2D>(const char*);
class BoxCollider2D; 
class CapsuleCollider2D; 
class CircleCollider2D; 
class CompositeCollider2D; 
class EdgeCollider2D; 
class PolygonCollider2D; 
class TilemapCollider2D; 
class ConstantForce; 
class Effector2D; 
class AreaEffector2D; 
class BuoyancyEffector2D; 
class PlatformEffector2D; 
class PointEffector2D; 
class SurfaceEffector2D; 
class FlareLayer; 
class GUIElement; template <> void RegisterUnityClass<GUIElement>(const char*);
namespace TextRenderingPrivate { class GUIText; } template <> void RegisterUnityClass<TextRenderingPrivate::GUIText>(const char*);
class GUITexture; template <> void RegisterUnityClass<GUITexture>(const char*);
class GUILayer; template <> void RegisterUnityClass<GUILayer>(const char*);
class GridLayout; 
class Grid; 
class Tilemap; 
class Halo; 
class HaloLayer; 
class IConstraint; 
class AimConstraint; 
class LookAtConstraint; 
class ParentConstraint; 
class PositionConstraint; 
class RotationConstraint; 
class ScaleConstraint; 
class Joint2D; template <> void RegisterUnityClass<Joint2D>(const char*);
class AnchoredJoint2D; template <> void RegisterUnityClass<AnchoredJoint2D>(const char*);
class DistanceJoint2D; 
class FixedJoint2D; 
class FrictionJoint2D; 
class HingeJoint2D; template <> void RegisterUnityClass<HingeJoint2D>(const char*);
class SliderJoint2D; 
class SpringJoint2D; 
class WheelJoint2D; template <> void RegisterUnityClass<WheelJoint2D>(const char*);
class RelativeJoint2D; 
class TargetJoint2D; 
class LensFlare; 
class Light; template <> void RegisterUnityClass<Light>(const char*);
class LightProbeGroup; 
class LightProbeProxyVolume; 
class MonoBehaviour; template <> void RegisterUnityClass<MonoBehaviour>(const char*);
class NavMeshAgent; template <> void RegisterUnityClass<NavMeshAgent>(const char*);
class NavMeshObstacle; 
class OffMeshLink; 
class PhysicsUpdateBehaviour2D; 
class ConstantForce2D; 
class PlayableDirector; template <> void RegisterUnityClass<PlayableDirector>(const char*);
class Projector; template <> void RegisterUnityClass<Projector>(const char*);
class ReflectionProbe; template <> void RegisterUnityClass<ReflectionProbe>(const char*);
class Skybox; 
class SortingGroup; 
class StreamingController; 
class Terrain; 
class VideoPlayer; template <> void RegisterUnityClass<VideoPlayer>(const char*);
class WindZone; 
namespace UI { class CanvasRenderer; } template <> void RegisterUnityClass<UI::CanvasRenderer>(const char*);
class Collider; template <> void RegisterUnityClass<Collider>(const char*);
class BoxCollider; template <> void RegisterUnityClass<BoxCollider>(const char*);
class CapsuleCollider; 
class CharacterController; template <> void RegisterUnityClass<CharacterController>(const char*);
class MeshCollider; 
class SphereCollider; 
class TerrainCollider; 
class WheelCollider; 
namespace Unity { class Joint; } template <> void RegisterUnityClass<Unity::Joint>(const char*);
namespace Unity { class CharacterJoint; } 
namespace Unity { class ConfigurableJoint; } 
namespace Unity { class FixedJoint; } 
namespace Unity { class HingeJoint; } 
namespace Unity { class SpringJoint; } 
class LODGroup; 
class MeshFilter; template <> void RegisterUnityClass<MeshFilter>(const char*);
class OcclusionArea; 
class OcclusionPortal; 
class ParticleAnimator; 
class ParticleEmitter; 
class EllipsoidParticleEmitter; 
class MeshParticleEmitter; 
class ParticleSystem; template <> void RegisterUnityClass<ParticleSystem>(const char*);
class Renderer; template <> void RegisterUnityClass<Renderer>(const char*);
class BillboardRenderer; 
class LineRenderer; 
class MeshRenderer; template <> void RegisterUnityClass<MeshRenderer>(const char*);
class ParticleRenderer; 
class ParticleSystemRenderer; template <> void RegisterUnityClass<ParticleSystemRenderer>(const char*);
class SkinnedMeshRenderer; template <> void RegisterUnityClass<SkinnedMeshRenderer>(const char*);
class SpriteMask; 
class SpriteRenderer; 
class SpriteShapeRenderer; 
class TilemapRenderer; 
class TrailRenderer; 
class Rigidbody; template <> void RegisterUnityClass<Rigidbody>(const char*);
class Rigidbody2D; template <> void RegisterUnityClass<Rigidbody2D>(const char*);
namespace TextRenderingPrivate { class TextMesh; } 
class Transform; template <> void RegisterUnityClass<Transform>(const char*);
namespace UI { class RectTransform; } template <> void RegisterUnityClass<UI::RectTransform>(const char*);
class Tree; 
class WorldAnchor; 
class WorldParticleCollider; 
class GameObject; template <> void RegisterUnityClass<GameObject>(const char*);
class NamedObject; template <> void RegisterUnityClass<NamedObject>(const char*);
class AssetBundle; 
class AssetBundleManifest; 
class ScriptedImporter; 
class AssetImporterLog; 
class AudioMixer; 
class AudioMixerController; 
class AudioMixerGroup; 
class AudioMixerGroupController; 
class AudioMixerSnapshot; 
class AudioMixerSnapshotController; 
class Avatar; template <> void RegisterUnityClass<Avatar>(const char*);
class AvatarMask; 
class BillboardAsset; 
class ComputeShader; 
class Flare; template <> void RegisterUnityClass<Flare>(const char*);
namespace TextRendering { class Font; } template <> void RegisterUnityClass<TextRendering::Font>(const char*);
class GameObjectRecorder; 
class LightProbes; template <> void RegisterUnityClass<LightProbes>(const char*);
class LocalizationAsset; 
class Material; template <> void RegisterUnityClass<Material>(const char*);
class ProceduralMaterial; 
class Mesh; template <> void RegisterUnityClass<Mesh>(const char*);
class Motion; template <> void RegisterUnityClass<Motion>(const char*);
class AnimationClip; template <> void RegisterUnityClass<AnimationClip>(const char*);
class PreviewAnimationClip; 
class NavMeshData; 
class OcclusionCullingData; 
class PhysicMaterial; template <> void RegisterUnityClass<PhysicMaterial>(const char*);
class PhysicsMaterial2D; template <> void RegisterUnityClass<PhysicsMaterial2D>(const char*);
class PreloadData; template <> void RegisterUnityClass<PreloadData>(const char*);
class RuntimeAnimatorController; template <> void RegisterUnityClass<RuntimeAnimatorController>(const char*);
class AnimatorController; template <> void RegisterUnityClass<AnimatorController>(const char*);
class AnimatorOverrideController; template <> void RegisterUnityClass<AnimatorOverrideController>(const char*);
class SampleClip; template <> void RegisterUnityClass<SampleClip>(const char*);
class AudioClip; template <> void RegisterUnityClass<AudioClip>(const char*);
class Shader; template <> void RegisterUnityClass<Shader>(const char*);
class ShaderVariantCollection; 
class SpeedTreeWindAsset; 
class Sprite; template <> void RegisterUnityClass<Sprite>(const char*);
class SpriteAtlas; 
class SubstanceArchive; 
class TerrainData; 
class TextAsset; template <> void RegisterUnityClass<TextAsset>(const char*);
class CGProgram; 
class MonoScript; template <> void RegisterUnityClass<MonoScript>(const char*);
class Texture; template <> void RegisterUnityClass<Texture>(const char*);
class BaseVideoTexture; template <> void RegisterUnityClass<BaseVideoTexture>(const char*);
class MovieTexture; 
class WebCamTexture; template <> void RegisterUnityClass<WebCamTexture>(const char*);
class CubemapArray; 
class LowerResBlitTexture; template <> void RegisterUnityClass<LowerResBlitTexture>(const char*);
class ProceduralTexture; 
class RenderTexture; template <> void RegisterUnityClass<RenderTexture>(const char*);
class CustomRenderTexture; 
class SparseTexture; 
class Texture2D; template <> void RegisterUnityClass<Texture2D>(const char*);
class Cubemap; template <> void RegisterUnityClass<Cubemap>(const char*);
class Texture2DArray; template <> void RegisterUnityClass<Texture2DArray>(const char*);
class Texture3D; template <> void RegisterUnityClass<Texture3D>(const char*);
class VideoClip; 
class GameManager; template <> void RegisterUnityClass<GameManager>(const char*);
class GlobalGameManager; template <> void RegisterUnityClass<GlobalGameManager>(const char*);
class AudioManager; template <> void RegisterUnityClass<AudioManager>(const char*);
class BuildSettings; template <> void RegisterUnityClass<BuildSettings>(const char*);
class CloudWebServicesManager; template <> void RegisterUnityClass<CloudWebServicesManager>(const char*);
class CrashReportManager; 
class DelayedCallManager; template <> void RegisterUnityClass<DelayedCallManager>(const char*);
class GraphicsSettings; template <> void RegisterUnityClass<GraphicsSettings>(const char*);
class InputManager; template <> void RegisterUnityClass<InputManager>(const char*);
class MonoManager; template <> void RegisterUnityClass<MonoManager>(const char*);
class NavMeshProjectSettings; template <> void RegisterUnityClass<NavMeshProjectSettings>(const char*);
class PerformanceReportingManager; template <> void RegisterUnityClass<PerformanceReportingManager>(const char*);
class Physics2DSettings; template <> void RegisterUnityClass<Physics2DSettings>(const char*);
class PhysicsManager; template <> void RegisterUnityClass<PhysicsManager>(const char*);
class PlayerSettings; template <> void RegisterUnityClass<PlayerSettings>(const char*);
class QualitySettings; template <> void RegisterUnityClass<QualitySettings>(const char*);
class ResourceManager; template <> void RegisterUnityClass<ResourceManager>(const char*);
class RuntimeInitializeOnLoadManager; template <> void RegisterUnityClass<RuntimeInitializeOnLoadManager>(const char*);
class ScriptMapper; template <> void RegisterUnityClass<ScriptMapper>(const char*);
class StreamingManager; 
class TagManager; template <> void RegisterUnityClass<TagManager>(const char*);
class TimeManager; template <> void RegisterUnityClass<TimeManager>(const char*);
class UnityAnalyticsManager; template <> void RegisterUnityClass<UnityAnalyticsManager>(const char*);
class UnityConnectSettings; template <> void RegisterUnityClass<UnityConnectSettings>(const char*);
class LevelGameManager; template <> void RegisterUnityClass<LevelGameManager>(const char*);
class LightmapSettings; template <> void RegisterUnityClass<LightmapSettings>(const char*);
class NavMeshSettings; template <> void RegisterUnityClass<NavMeshSettings>(const char*);
class OcclusionCullingSettings; 
class RenderSettings; template <> void RegisterUnityClass<RenderSettings>(const char*);
class RenderPassAttachment; 

void RegisterAllClasses()
{
void RegisterBuiltinTypes();
RegisterBuiltinTypes();
	//Total: 99 non stripped classes
	//0. Camera
	RegisterUnityClass<Camera>("Core");
	//1. Behaviour
	RegisterUnityClass<Behaviour>("Core");
	//2. Unity::Component
	RegisterUnityClass<Unity::Component>("Core");
	//3. EditorExtension
	RegisterUnityClass<EditorExtension>("Core");
	//4. Light
	RegisterUnityClass<Light>("Core");
	//5. Material
	RegisterUnityClass<Material>("Core");
	//6. NamedObject
	RegisterUnityClass<NamedObject>("Core");
	//7. Texture2D
	RegisterUnityClass<Texture2D>("Core");
	//8. Texture
	RegisterUnityClass<Texture>("Core");
	//9. Cubemap
	RegisterUnityClass<Cubemap>("Core");
	//10. Texture3D
	RegisterUnityClass<Texture3D>("Core");
	//11. Texture2DArray
	RegisterUnityClass<Texture2DArray>("Core");
	//12. RenderTexture
	RegisterUnityClass<RenderTexture>("Core");
	//13. LowerResBlitTexture
	RegisterUnityClass<LowerResBlitTexture>("Core");
	//14. PreloadData
	RegisterUnityClass<PreloadData>("Core");
	//15. GameObject
	RegisterUnityClass<GameObject>("Core");
	//16. QualitySettings
	RegisterUnityClass<QualitySettings>("Core");
	//17. GlobalGameManager
	RegisterUnityClass<GlobalGameManager>("Core");
	//18. GameManager
	RegisterUnityClass<GameManager>("Core");
	//19. Renderer
	RegisterUnityClass<Renderer>("Core");
	//20. RenderSettings
	RegisterUnityClass<RenderSettings>("Core");
	//21. LevelGameManager
	RegisterUnityClass<LevelGameManager>("Core");
	//22. Shader
	RegisterUnityClass<Shader>("Core");
	//23. Projector
	RegisterUnityClass<Projector>("Core");
	//24. MeshFilter
	RegisterUnityClass<MeshFilter>("Core");
	//25. MeshRenderer
	RegisterUnityClass<MeshRenderer>("Core");
	//26. GUIElement
	RegisterUnityClass<GUIElement>("Core");
	//27. GUITexture
	RegisterUnityClass<GUITexture>("Core");
	//28. GUILayer
	RegisterUnityClass<GUILayer>("Core");
	//29. Mesh
	RegisterUnityClass<Mesh>("Core");
	//30. MonoBehaviour
	RegisterUnityClass<MonoBehaviour>("Core");
	//31. ReflectionProbe
	RegisterUnityClass<ReflectionProbe>("Core");
	//32. UI::RectTransform
	RegisterUnityClass<UI::RectTransform>("Core");
	//33. Transform
	RegisterUnityClass<Transform>("Core");
	//34. Sprite
	RegisterUnityClass<Sprite>("Core");
	//35. AudioClip
	RegisterUnityClass<AudioClip>("Audio");
	//36. SampleClip
	RegisterUnityClass<SampleClip>("Audio");
	//37. AudioBehaviour
	RegisterUnityClass<AudioBehaviour>("Audio");
	//38. AudioListener
	RegisterUnityClass<AudioListener>("Audio");
	//39. AudioSource
	RegisterUnityClass<AudioSource>("Audio");
	//40. WebCamTexture
	RegisterUnityClass<WebCamTexture>("Audio");
	//41. BaseVideoTexture
	RegisterUnityClass<BaseVideoTexture>("Audio");
	//42. ParticleSystem
	RegisterUnityClass<ParticleSystem>("ParticleSystem");
	//43. ParticleSystemRenderer
	RegisterUnityClass<ParticleSystemRenderer>("ParticleSystem");
	//44. PhysicMaterial
	RegisterUnityClass<PhysicMaterial>("Physics");
	//45. Rigidbody
	RegisterUnityClass<Rigidbody>("Physics");
	//46. Collider
	RegisterUnityClass<Collider>("Physics");
	//47. CharacterController
	RegisterUnityClass<CharacterController>("Physics");
	//48. BoxCollider
	RegisterUnityClass<BoxCollider>("Physics");
	//49. Unity::Joint
	RegisterUnityClass<Unity::Joint>("Physics");
	//50. AnimationClip
	RegisterUnityClass<AnimationClip>("Animation");
	//51. Motion
	RegisterUnityClass<Motion>("Animation");
	//52. Animation
	RegisterUnityClass<Animation>("Animation");
	//53. Animator
	RegisterUnityClass<Animator>("Animation");
	//54. AnimatorOverrideController
	RegisterUnityClass<AnimatorOverrideController>("Animation");
	//55. RuntimeAnimatorController
	RegisterUnityClass<RuntimeAnimatorController>("Animation");
	//56. UI::Canvas
	RegisterUnityClass<UI::Canvas>("UI");
	//57. UI::CanvasGroup
	RegisterUnityClass<UI::CanvasGroup>("UI");
	//58. UI::CanvasRenderer
	RegisterUnityClass<UI::CanvasRenderer>("UI");
	//59. TextRenderingPrivate::GUIText
	RegisterUnityClass<TextRenderingPrivate::GUIText>("TextRendering");
	//60. TextRendering::Font
	RegisterUnityClass<TextRendering::Font>("TextRendering");
	//61. NavMeshAgent
	RegisterUnityClass<NavMeshAgent>("AI");
	//62. PlayableDirector
	RegisterUnityClass<PlayableDirector>("Director");
	//63. Rigidbody2D
	RegisterUnityClass<Rigidbody2D>("Physics2D");
	//64. Collider2D
	RegisterUnityClass<Collider2D>("Physics2D");
	//65. Joint2D
	RegisterUnityClass<Joint2D>("Physics2D");
	//66. HingeJoint2D
	RegisterUnityClass<HingeJoint2D>("Physics2D");
	//67. AnchoredJoint2D
	RegisterUnityClass<AnchoredJoint2D>("Physics2D");
	//68. WheelJoint2D
	RegisterUnityClass<WheelJoint2D>("Physics2D");
	//69. PhysicsMaterial2D
	RegisterUnityClass<PhysicsMaterial2D>("Physics2D");
	//70. VideoPlayer
	RegisterUnityClass<VideoPlayer>("Video");
	//71. Flare
	RegisterUnityClass<Flare>("Core");
	//72. TagManager
	RegisterUnityClass<TagManager>("Core");
	//73. GraphicsSettings
	RegisterUnityClass<GraphicsSettings>("Core");
	//74. DelayedCallManager
	RegisterUnityClass<DelayedCallManager>("Core");
	//75. InputManager
	RegisterUnityClass<InputManager>("Core");
	//76. TimeManager
	RegisterUnityClass<TimeManager>("Core");
	//77. BuildSettings
	RegisterUnityClass<BuildSettings>("Core");
	//78. PlayerSettings
	RegisterUnityClass<PlayerSettings>("Core");
	//79. ResourceManager
	RegisterUnityClass<ResourceManager>("Core");
	//80. RuntimeInitializeOnLoadManager
	RegisterUnityClass<RuntimeInitializeOnLoadManager>("Core");
	//81. ScriptMapper
	RegisterUnityClass<ScriptMapper>("Core");
	//82. PhysicsManager
	RegisterUnityClass<PhysicsManager>("Physics");
	//83. MonoManager
	RegisterUnityClass<MonoManager>("Core");
	//84. MonoScript
	RegisterUnityClass<MonoScript>("Core");
	//85. TextAsset
	RegisterUnityClass<TextAsset>("Core");
	//86. AudioManager
	RegisterUnityClass<AudioManager>("Audio");
	//87. NavMeshProjectSettings
	RegisterUnityClass<NavMeshProjectSettings>("AI");
	//88. CloudWebServicesManager
	RegisterUnityClass<CloudWebServicesManager>("CloudWebServices");
	//89. PerformanceReportingManager
	RegisterUnityClass<PerformanceReportingManager>("PerformanceReporting");
	//90. Physics2DSettings
	RegisterUnityClass<Physics2DSettings>("Physics2D");
	//91. UnityAnalyticsManager
	RegisterUnityClass<UnityAnalyticsManager>("UnityAnalytics");
	//92. UnityConnectSettings
	RegisterUnityClass<UnityConnectSettings>("UnityConnect");
	//93. AnimatorController
	RegisterUnityClass<AnimatorController>("Animation");
	//94. LightProbes
	RegisterUnityClass<LightProbes>("Core");
	//95. LightmapSettings
	RegisterUnityClass<LightmapSettings>("Core");
	//96. SkinnedMeshRenderer
	RegisterUnityClass<SkinnedMeshRenderer>("Core");
	//97. Avatar
	RegisterUnityClass<Avatar>("Animation");
	//98. NavMeshSettings
	RegisterUnityClass<NavMeshSettings>("AI");

}
