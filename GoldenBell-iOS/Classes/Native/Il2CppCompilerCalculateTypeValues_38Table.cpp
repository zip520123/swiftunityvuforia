﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t919699796;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t1450375506;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t1738787045;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t1097319912;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t402703848;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FSMPROCESSOR_T2217570134_H
#define FSMPROCESSOR_T2217570134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmProcessor
struct  FsmProcessor_t2217570134  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMPROCESSOR_T2217570134_H
#ifndef UPDATEHELPER_T1432531020_H
#define UPDATEHELPER_T1432531020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UpdateHelper
struct  UpdateHelper_t1432531020  : public RuntimeObject
{
public:

public:
};

struct UpdateHelper_t1432531020_StaticFields
{
public:
	// System.Boolean HutongGames.PlayMaker.UpdateHelper::editorPrefLoaded
	bool ___editorPrefLoaded_0;

public:
	inline static int32_t get_offset_of_editorPrefLoaded_0() { return static_cast<int32_t>(offsetof(UpdateHelper_t1432531020_StaticFields, ___editorPrefLoaded_0)); }
	inline bool get_editorPrefLoaded_0() const { return ___editorPrefLoaded_0; }
	inline bool* get_address_of_editorPrefLoaded_0() { return &___editorPrefLoaded_0; }
	inline void set_editorPrefLoaded_0(bool value)
	{
		___editorPrefLoaded_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEHELPER_T1432531020_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2OPERATION_T1926650760_H
#define VECTOR2OPERATION_T1926650760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Operator/Vector2Operation
struct  Vector2Operation_t1926650760 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.Vector2Operator/Vector2Operation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Vector2Operation_t1926650760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2OPERATION_T1926650760_H
#ifndef VECTOR3OPERATION_T3413508724_H
#define VECTOR3OPERATION_T3413508724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Operator/Vector3Operation
struct  Vector3Operation_t3413508724 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.Vector3Operator/Vector3Operation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Vector3Operation_t3413508724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3OPERATION_T3413508724_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef INTERPOLATIONTYPE_T358821704_H
#define INTERPOLATIONTYPE_T358821704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.InterpolationType
struct  InterpolationType_t358821704 
{
public:
	// System.Int32 HutongGames.PlayMaker.InterpolationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InterpolationType_t358821704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONTYPE_T358821704_H
#ifndef LOGLEVEL_T183558972_H
#define LOGLEVEL_T183558972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.LogLevel
struct  LogLevel_t183558972 
{
public:
	// System.Int32 HutongGames.PlayMaker.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t183558972, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T183558972_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CANVASUPDATE_T2572322932_H
#define CANVASUPDATE_T2572322932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasUpdate
struct  CanvasUpdate_t2572322932 
{
public:
	// System.Int32 UnityEngine.UI.CanvasUpdate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CanvasUpdate_t2572322932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASUPDATE_T2572322932_H
#ifndef DIRECTION_T3470714353_H
#define DIRECTION_T3470714353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Direction
struct  Direction_t3470714353 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t3470714353, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T3470714353_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef COMPONENTACTION_1_T242655537_H
#define COMPONENTACTION_1_T242655537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Graphic>
struct  ComponentAction_1_t242655537  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Graphic_t1660335611 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t242655537, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t242655537, ___cachedComponent_15)); }
	inline Graphic_t1660335611 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Graphic_t1660335611 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Graphic_t1660335611 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T242655537_H
#ifndef COMPONENTACTION_1_T2345237357_H
#define COMPONENTACTION_1_T2345237357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.InputField>
struct  ComponentAction_1_t2345237357  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	InputField_t3762917431 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2345237357, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2345237357, ___cachedComponent_15)); }
	inline InputField_t3762917431 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline InputField_t3762917431 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(InputField_t3762917431 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2345237357_H
#ifndef COMPONENTACTION_1_T1765238890_H
#define COMPONENTACTION_1_T1765238890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.RawImage>
struct  ComponentAction_1_t1765238890  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	RawImage_t3182918964 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1765238890, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1765238890, ___cachedComponent_15)); }
	inline RawImage_t3182918964 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline RawImage_t3182918964 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(RawImage_t3182918964 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1765238890_H
#ifndef COMPONENTACTION_1_T2720175740_H
#define COMPONENTACTION_1_T2720175740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.ScrollRect>
struct  ComponentAction_1_t2720175740  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	ScrollRect_t4137855814 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2720175740, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2720175740, ___cachedComponent_15)); }
	inline ScrollRect_t4137855814 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline ScrollRect_t4137855814 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(ScrollRect_t4137855814 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2720175740_H
#ifndef COMPONENTACTION_1_T76767159_H
#define COMPONENTACTION_1_T76767159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Scrollbar>
struct  ComponentAction_1_t76767159  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Scrollbar_t1494447233 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t76767159, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t76767159, ___cachedComponent_15)); }
	inline Scrollbar_t1494447233 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Scrollbar_t1494447233 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Scrollbar_t1494447233 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T76767159_H
#ifndef COMPONENTACTION_1_T2486048828_H
#define COMPONENTACTION_1_T2486048828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Slider>
struct  ComponentAction_1_t2486048828  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Slider_t3903728902 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2486048828, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2486048828, ___cachedComponent_15)); }
	inline Slider_t3903728902 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Slider_t3903728902 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Slider_t3903728902 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2486048828_H
#ifndef COMPONENTACTION_1_T484202640_H
#define COMPONENTACTION_1_T484202640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Text>
struct  ComponentAction_1_t484202640  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Text_t1901882714 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t484202640, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t484202640, ___cachedComponent_15)); }
	inline Text_t1901882714 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Text_t1901882714 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Text_t1901882714 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T484202640_H
#ifndef COMPONENTACTION_1_T1317696987_H
#define COMPONENTACTION_1_T1317696987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Toggle>
struct  ComponentAction_1_t1317696987  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Toggle_t2735377061 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1317696987, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1317696987, ___cachedComponent_15)); }
	inline Toggle_t2735377061 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Toggle_t2735377061 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Toggle_t2735377061 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1317696987_H
#ifndef DEBUGVECTOR2_T1875071681_H
#define DEBUGVECTOR2_T1875071681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DebugVector2
struct  DebugVector2_t1875071681  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.LogLevel HutongGames.PlayMaker.Actions.DebugVector2::logLevel
	int32_t ___logLevel_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.DebugVector2::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_15;

public:
	inline static int32_t get_offset_of_logLevel_14() { return static_cast<int32_t>(offsetof(DebugVector2_t1875071681, ___logLevel_14)); }
	inline int32_t get_logLevel_14() const { return ___logLevel_14; }
	inline int32_t* get_address_of_logLevel_14() { return &___logLevel_14; }
	inline void set_logLevel_14(int32_t value)
	{
		___logLevel_14 = value;
	}

	inline static int32_t get_offset_of_vector2Variable_15() { return static_cast<int32_t>(offsetof(DebugVector2_t1875071681, ___vector2Variable_15)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_15() const { return ___vector2Variable_15; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_15() { return &___vector2Variable_15; }
	inline void set_vector2Variable_15(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGVECTOR2_T1875071681_H
#ifndef GETCOMPONENT_T1263127638_H
#define GETCOMPONENT_T1263127638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetComponent
struct  GetComponent_t1263127638  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetComponent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetComponent::storeComponent
	FsmObject_t2606870197 * ___storeComponent_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetComponent::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetComponent_t1263127638, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeComponent_15() { return static_cast<int32_t>(offsetof(GetComponent_t1263127638, ___storeComponent_15)); }
	inline FsmObject_t2606870197 * get_storeComponent_15() const { return ___storeComponent_15; }
	inline FsmObject_t2606870197 ** get_address_of_storeComponent_15() { return &___storeComponent_15; }
	inline void set_storeComponent_15(FsmObject_t2606870197 * value)
	{
		___storeComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeComponent_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetComponent_t1263127638, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOMPONENT_T1263127638_H
#ifndef GETPROPERTY_T3607958757_H
#define GETPROPERTY_T3607958757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetProperty
struct  GetProperty_t3607958757  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmProperty HutongGames.PlayMaker.Actions.GetProperty::targetProperty
	FsmProperty_t1450375506 * ___targetProperty_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetProperty::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_targetProperty_14() { return static_cast<int32_t>(offsetof(GetProperty_t3607958757, ___targetProperty_14)); }
	inline FsmProperty_t1450375506 * get_targetProperty_14() const { return ___targetProperty_14; }
	inline FsmProperty_t1450375506 ** get_address_of_targetProperty_14() { return &___targetProperty_14; }
	inline void set_targetProperty_14(FsmProperty_t1450375506 * value)
	{
		___targetProperty_14 = value;
		Il2CppCodeGenWriteBarrier((&___targetProperty_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(GetProperty_t3607958757, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPROPERTY_T3607958757_H
#ifndef GETVECTOR2LENGTH_T488136250_H
#define GETVECTOR2LENGTH_T488136250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVector2Length
struct  GetVector2Length_t488136250  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetVector2Length::vector2
	FsmVector2_t2965096677 * ___vector2_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector2Length::storeLength
	FsmFloat_t2883254149 * ___storeLength_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVector2Length::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector2_14() { return static_cast<int32_t>(offsetof(GetVector2Length_t488136250, ___vector2_14)); }
	inline FsmVector2_t2965096677 * get_vector2_14() const { return ___vector2_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2_14() { return &___vector2_14; }
	inline void set_vector2_14(FsmVector2_t2965096677 * value)
	{
		___vector2_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2_14), value);
	}

	inline static int32_t get_offset_of_storeLength_15() { return static_cast<int32_t>(offsetof(GetVector2Length_t488136250, ___storeLength_15)); }
	inline FsmFloat_t2883254149 * get_storeLength_15() const { return ___storeLength_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeLength_15() { return &___storeLength_15; }
	inline void set_storeLength_15(FsmFloat_t2883254149 * value)
	{
		___storeLength_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeLength_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetVector2Length_t488136250, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVECTOR2LENGTH_T488136250_H
#ifndef GETVECTOR2XY_T4289145362_H
#define GETVECTOR2XY_T4289145362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVector2XY
struct  GetVector2XY_t4289145362  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetVector2XY::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector2XY::storeX
	FsmFloat_t2883254149 * ___storeX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector2XY::storeY
	FsmFloat_t2883254149 * ___storeY_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVector2XY::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(GetVector2XY_t4289145362, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_storeX_15() { return static_cast<int32_t>(offsetof(GetVector2XY_t4289145362, ___storeX_15)); }
	inline FsmFloat_t2883254149 * get_storeX_15() const { return ___storeX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeX_15() { return &___storeX_15; }
	inline void set_storeX_15(FsmFloat_t2883254149 * value)
	{
		___storeX_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeX_15), value);
	}

	inline static int32_t get_offset_of_storeY_16() { return static_cast<int32_t>(offsetof(GetVector2XY_t4289145362, ___storeY_16)); }
	inline FsmFloat_t2883254149 * get_storeY_16() const { return ___storeY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeY_16() { return &___storeY_16; }
	inline void set_storeY_16(FsmFloat_t2883254149 * value)
	{
		___storeY_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeY_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetVector2XY_t4289145362, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVECTOR2XY_T4289145362_H
#ifndef GETVECTOR3XYZ_T1886187690_H
#define GETVECTOR3XYZ_T1886187690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVector3XYZ
struct  GetVector3XYZ_t1886187690  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetVector3XYZ::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector3XYZ::storeX
	FsmFloat_t2883254149 * ___storeX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector3XYZ::storeY
	FsmFloat_t2883254149 * ___storeY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVector3XYZ::storeZ
	FsmFloat_t2883254149 * ___storeZ_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVector3XYZ::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t1886187690, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_storeX_15() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t1886187690, ___storeX_15)); }
	inline FsmFloat_t2883254149 * get_storeX_15() const { return ___storeX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeX_15() { return &___storeX_15; }
	inline void set_storeX_15(FsmFloat_t2883254149 * value)
	{
		___storeX_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeX_15), value);
	}

	inline static int32_t get_offset_of_storeY_16() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t1886187690, ___storeY_16)); }
	inline FsmFloat_t2883254149 * get_storeY_16() const { return ___storeY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeY_16() { return &___storeY_16; }
	inline void set_storeY_16(FsmFloat_t2883254149 * value)
	{
		___storeY_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeY_16), value);
	}

	inline static int32_t get_offset_of_storeZ_17() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t1886187690, ___storeZ_17)); }
	inline FsmFloat_t2883254149 * get_storeZ_17() const { return ___storeZ_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeZ_17() { return &___storeZ_17; }
	inline void set_storeZ_17(FsmFloat_t2883254149 * value)
	{
		___storeZ_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeZ_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetVector3XYZ_t1886187690, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVECTOR3XYZ_T1886187690_H
#ifndef GETVECTORLENGTH_T1339640164_H
#define GETVECTORLENGTH_T1339640164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVectorLength
struct  GetVectorLength_t1339640164  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetVectorLength::vector3
	FsmVector3_t626444517 * ___vector3_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVectorLength::storeLength
	FsmFloat_t2883254149 * ___storeLength_15;

public:
	inline static int32_t get_offset_of_vector3_14() { return static_cast<int32_t>(offsetof(GetVectorLength_t1339640164, ___vector3_14)); }
	inline FsmVector3_t626444517 * get_vector3_14() const { return ___vector3_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3_14() { return &___vector3_14; }
	inline void set_vector3_14(FsmVector3_t626444517 * value)
	{
		___vector3_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3_14), value);
	}

	inline static int32_t get_offset_of_storeLength_15() { return static_cast<int32_t>(offsetof(GetVectorLength_t1339640164, ___storeLength_15)); }
	inline FsmFloat_t2883254149 * get_storeLength_15() const { return ___storeLength_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeLength_15() { return &___storeLength_15; }
	inline void set_storeLength_15(FsmFloat_t2883254149 * value)
	{
		___storeLength_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeLength_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVECTORLENGTH_T1339640164_H
#ifndef SELECTRANDOMVECTOR2_T2658517560_H
#define SELECTRANDOMVECTOR2_T2658517560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomVector2
struct  SelectRandomVector2_t2658517560  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.Actions.SelectRandomVector2::vector2Array
	FsmVector2U5BU5D_t1097319912* ___vector2Array_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomVector2::weights
	FsmFloatU5BU5D_t3637897416* ___weights_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SelectRandomVector2::storeVector2
	FsmVector2_t2965096677 * ___storeVector2_16;

public:
	inline static int32_t get_offset_of_vector2Array_14() { return static_cast<int32_t>(offsetof(SelectRandomVector2_t2658517560, ___vector2Array_14)); }
	inline FsmVector2U5BU5D_t1097319912* get_vector2Array_14() const { return ___vector2Array_14; }
	inline FsmVector2U5BU5D_t1097319912** get_address_of_vector2Array_14() { return &___vector2Array_14; }
	inline void set_vector2Array_14(FsmVector2U5BU5D_t1097319912* value)
	{
		___vector2Array_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Array_14), value);
	}

	inline static int32_t get_offset_of_weights_15() { return static_cast<int32_t>(offsetof(SelectRandomVector2_t2658517560, ___weights_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_15() const { return ___weights_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_15() { return &___weights_15; }
	inline void set_weights_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_15 = value;
		Il2CppCodeGenWriteBarrier((&___weights_15), value);
	}

	inline static int32_t get_offset_of_storeVector2_16() { return static_cast<int32_t>(offsetof(SelectRandomVector2_t2658517560, ___storeVector2_16)); }
	inline FsmVector2_t2965096677 * get_storeVector2_16() const { return ___storeVector2_16; }
	inline FsmVector2_t2965096677 ** get_address_of_storeVector2_16() { return &___storeVector2_16; }
	inline void set_storeVector2_16(FsmVector2_t2965096677 * value)
	{
		___storeVector2_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector2_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTRANDOMVECTOR2_T2658517560_H
#ifndef SELECTRANDOMVECTOR3_T2658517559_H
#define SELECTRANDOMVECTOR3_T2658517559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomVector3
struct  SelectRandomVector3_t2658517559  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.Actions.SelectRandomVector3::vector3Array
	FsmVector3U5BU5D_t402703848* ___vector3Array_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomVector3::weights
	FsmFloatU5BU5D_t3637897416* ___weights_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SelectRandomVector3::storeVector3
	FsmVector3_t626444517 * ___storeVector3_16;

public:
	inline static int32_t get_offset_of_vector3Array_14() { return static_cast<int32_t>(offsetof(SelectRandomVector3_t2658517559, ___vector3Array_14)); }
	inline FsmVector3U5BU5D_t402703848* get_vector3Array_14() const { return ___vector3Array_14; }
	inline FsmVector3U5BU5D_t402703848** get_address_of_vector3Array_14() { return &___vector3Array_14; }
	inline void set_vector3Array_14(FsmVector3U5BU5D_t402703848* value)
	{
		___vector3Array_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Array_14), value);
	}

	inline static int32_t get_offset_of_weights_15() { return static_cast<int32_t>(offsetof(SelectRandomVector3_t2658517559, ___weights_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_15() const { return ___weights_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_15() { return &___weights_15; }
	inline void set_weights_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_15 = value;
		Il2CppCodeGenWriteBarrier((&___weights_15), value);
	}

	inline static int32_t get_offset_of_storeVector3_16() { return static_cast<int32_t>(offsetof(SelectRandomVector3_t2658517559, ___storeVector3_16)); }
	inline FsmVector3_t626444517 * get_storeVector3_16() const { return ___storeVector3_16; }
	inline FsmVector3_t626444517 ** get_address_of_storeVector3_16() { return &___storeVector3_16; }
	inline void set_storeVector3_16(FsmVector3_t626444517 * value)
	{
		___storeVector3_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector3_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTRANDOMVECTOR3_T2658517559_H
#ifndef SETOBJECTVALUE_T3915321542_H
#define SETOBJECTVALUE_T3915321542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetObjectValue
struct  SetObjectValue_t3915321542  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetObjectValue::objectVariable
	FsmObject_t2606870197 * ___objectVariable_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetObjectValue::objectValue
	FsmObject_t2606870197 * ___objectValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetObjectValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_objectVariable_14() { return static_cast<int32_t>(offsetof(SetObjectValue_t3915321542, ___objectVariable_14)); }
	inline FsmObject_t2606870197 * get_objectVariable_14() const { return ___objectVariable_14; }
	inline FsmObject_t2606870197 ** get_address_of_objectVariable_14() { return &___objectVariable_14; }
	inline void set_objectVariable_14(FsmObject_t2606870197 * value)
	{
		___objectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectVariable_14), value);
	}

	inline static int32_t get_offset_of_objectValue_15() { return static_cast<int32_t>(offsetof(SetObjectValue_t3915321542, ___objectValue_15)); }
	inline FsmObject_t2606870197 * get_objectValue_15() const { return ___objectValue_15; }
	inline FsmObject_t2606870197 ** get_address_of_objectValue_15() { return &___objectValue_15; }
	inline void set_objectValue_15(FsmObject_t2606870197 * value)
	{
		___objectValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___objectValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetObjectValue_t3915321542, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETOBJECTVALUE_T3915321542_H
#ifndef SETPROPERTY_T714161153_H
#define SETPROPERTY_T714161153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProperty
struct  SetProperty_t714161153  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmProperty HutongGames.PlayMaker.Actions.SetProperty::targetProperty
	FsmProperty_t1450375506 * ___targetProperty_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProperty::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_targetProperty_14() { return static_cast<int32_t>(offsetof(SetProperty_t714161153, ___targetProperty_14)); }
	inline FsmProperty_t1450375506 * get_targetProperty_14() const { return ___targetProperty_14; }
	inline FsmProperty_t1450375506 ** get_address_of_targetProperty_14() { return &___targetProperty_14; }
	inline void set_targetProperty_14(FsmProperty_t1450375506 * value)
	{
		___targetProperty_14 = value;
		Il2CppCodeGenWriteBarrier((&___targetProperty_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetProperty_t714161153, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROPERTY_T714161153_H
#ifndef SETVECTOR2VALUE_T1101835_H
#define SETVECTOR2VALUE_T1101835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVector2Value
struct  SetVector2Value_t1101835  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetVector2Value::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetVector2Value::vector2Value
	FsmVector2_t2965096677 * ___vector2Value_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVector2Value::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(SetVector2Value_t1101835, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_vector2Value_15() { return static_cast<int32_t>(offsetof(SetVector2Value_t1101835, ___vector2Value_15)); }
	inline FsmVector2_t2965096677 * get_vector2Value_15() const { return ___vector2Value_15; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Value_15() { return &___vector2Value_15; }
	inline void set_vector2Value_15(FsmVector2_t2965096677 * value)
	{
		___vector2Value_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Value_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetVector2Value_t1101835, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVECTOR2VALUE_T1101835_H
#ifndef SETVECTOR2XY_T2797569270_H
#define SETVECTOR2XY_T2797569270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVector2XY
struct  SetVector2XY_t2797569270  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetVector2XY::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetVector2XY::vector2Value
	FsmVector2_t2965096677 * ___vector2Value_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector2XY::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector2XY::y
	FsmFloat_t2883254149 * ___y_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVector2XY::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(SetVector2XY_t2797569270, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_vector2Value_15() { return static_cast<int32_t>(offsetof(SetVector2XY_t2797569270, ___vector2Value_15)); }
	inline FsmVector2_t2965096677 * get_vector2Value_15() const { return ___vector2Value_15; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Value_15() { return &___vector2Value_15; }
	inline void set_vector2Value_15(FsmVector2_t2965096677 * value)
	{
		___vector2Value_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Value_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(SetVector2XY_t2797569270, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(SetVector2XY_t2797569270, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetVector2XY_t2797569270, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVECTOR2XY_T2797569270_H
#ifndef SETVECTOR3VALUE_T606588939_H
#define SETVECTOR3VALUE_T606588939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVector3Value
struct  SetVector3Value_t606588939  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3Value::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3Value::vector3Value
	FsmVector3_t626444517 * ___vector3Value_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVector3Value::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(SetVector3Value_t606588939, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_vector3Value_15() { return static_cast<int32_t>(offsetof(SetVector3Value_t606588939, ___vector3Value_15)); }
	inline FsmVector3_t626444517 * get_vector3Value_15() const { return ___vector3Value_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Value_15() { return &___vector3Value_15; }
	inline void set_vector3Value_15(FsmVector3_t626444517 * value)
	{
		___vector3Value_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Value_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetVector3Value_t606588939, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVECTOR3VALUE_T606588939_H
#ifndef SETVECTOR3XYZ_T1065958254_H
#define SETVECTOR3XYZ_T1065958254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVector3XYZ
struct  SetVector3XYZ_t1065958254  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3XYZ::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVector3XYZ::vector3Value
	FsmVector3_t626444517 * ___vector3Value_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector3XYZ::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector3XYZ::y
	FsmFloat_t2883254149 * ___y_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVector3XYZ::z
	FsmFloat_t2883254149 * ___z_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVector3XYZ::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t1065958254, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_vector3Value_15() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t1065958254, ___vector3Value_15)); }
	inline FsmVector3_t626444517 * get_vector3Value_15() const { return ___vector3Value_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Value_15() { return &___vector3Value_15; }
	inline void set_vector3Value_15(FsmVector3_t626444517 * value)
	{
		___vector3Value_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Value_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t1065958254, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t1065958254, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_z_18() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t1065958254, ___z_18)); }
	inline FsmFloat_t2883254149 * get_z_18() const { return ___z_18; }
	inline FsmFloat_t2883254149 ** get_address_of_z_18() { return &___z_18; }
	inline void set_z_18(FsmFloat_t2883254149 * value)
	{
		___z_18 = value;
		Il2CppCodeGenWriteBarrier((&___z_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetVector3XYZ_t1065958254, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVECTOR3XYZ_T1065958254_H
#ifndef VECTOR2ADD_T3269288714_H
#define VECTOR2ADD_T3269288714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Add
struct  Vector2Add_t3269288714  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Add::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Add::addVector
	FsmVector2_t2965096677 * ___addVector_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Add::everyFrame
	bool ___everyFrame_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Add::perSecond
	bool ___perSecond_17;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2Add_t3269288714, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_addVector_15() { return static_cast<int32_t>(offsetof(Vector2Add_t3269288714, ___addVector_15)); }
	inline FsmVector2_t2965096677 * get_addVector_15() const { return ___addVector_15; }
	inline FsmVector2_t2965096677 ** get_address_of_addVector_15() { return &___addVector_15; }
	inline void set_addVector_15(FsmVector2_t2965096677 * value)
	{
		___addVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___addVector_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector2Add_t3269288714, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_perSecond_17() { return static_cast<int32_t>(offsetof(Vector2Add_t3269288714, ___perSecond_17)); }
	inline bool get_perSecond_17() const { return ___perSecond_17; }
	inline bool* get_address_of_perSecond_17() { return &___perSecond_17; }
	inline void set_perSecond_17(bool value)
	{
		___perSecond_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2ADD_T3269288714_H
#ifndef VECTOR2ADDXY_T1192973363_H
#define VECTOR2ADDXY_T1192973363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2AddXY
struct  Vector2AddXY_t1192973363  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2AddXY::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2AddXY::addX
	FsmFloat_t2883254149 * ___addX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2AddXY::addY
	FsmFloat_t2883254149 * ___addY_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2AddXY::everyFrame
	bool ___everyFrame_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2AddXY::perSecond
	bool ___perSecond_18;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2AddXY_t1192973363, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_addX_15() { return static_cast<int32_t>(offsetof(Vector2AddXY_t1192973363, ___addX_15)); }
	inline FsmFloat_t2883254149 * get_addX_15() const { return ___addX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_addX_15() { return &___addX_15; }
	inline void set_addX_15(FsmFloat_t2883254149 * value)
	{
		___addX_15 = value;
		Il2CppCodeGenWriteBarrier((&___addX_15), value);
	}

	inline static int32_t get_offset_of_addY_16() { return static_cast<int32_t>(offsetof(Vector2AddXY_t1192973363, ___addY_16)); }
	inline FsmFloat_t2883254149 * get_addY_16() const { return ___addY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_addY_16() { return &___addY_16; }
	inline void set_addY_16(FsmFloat_t2883254149 * value)
	{
		___addY_16 = value;
		Il2CppCodeGenWriteBarrier((&___addY_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(Vector2AddXY_t1192973363, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_perSecond_18() { return static_cast<int32_t>(offsetof(Vector2AddXY_t1192973363, ___perSecond_18)); }
	inline bool get_perSecond_18() const { return ___perSecond_18; }
	inline bool* get_address_of_perSecond_18() { return &___perSecond_18; }
	inline void set_perSecond_18(bool value)
	{
		___perSecond_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2ADDXY_T1192973363_H
#ifndef VECTOR2CLAMPMAGNITUDE_T243667646_H
#define VECTOR2CLAMPMAGNITUDE_T243667646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2ClampMagnitude
struct  Vector2ClampMagnitude_t243667646  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::maxLength
	FsmFloat_t2883254149 * ___maxLength_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2ClampMagnitude_t243667646, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_maxLength_15() { return static_cast<int32_t>(offsetof(Vector2ClampMagnitude_t243667646, ___maxLength_15)); }
	inline FsmFloat_t2883254149 * get_maxLength_15() const { return ___maxLength_15; }
	inline FsmFloat_t2883254149 ** get_address_of_maxLength_15() { return &___maxLength_15; }
	inline void set_maxLength_15(FsmFloat_t2883254149 * value)
	{
		___maxLength_15 = value;
		Il2CppCodeGenWriteBarrier((&___maxLength_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector2ClampMagnitude_t243667646, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2CLAMPMAGNITUDE_T243667646_H
#ifndef VECTOR2HIGHPASSFILTER_T3943959056_H
#define VECTOR2HIGHPASSFILTER_T3943959056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2HighPassFilter
struct  Vector2HighPassFilter_t3943959056  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2HighPassFilter::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2HighPassFilter::filteringFactor
	FsmFloat_t2883254149 * ___filteringFactor_15;
	// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.Vector2HighPassFilter::filteredVector
	Vector2_t2156229523  ___filteredVector_16;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2HighPassFilter_t3943959056, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_filteringFactor_15() { return static_cast<int32_t>(offsetof(Vector2HighPassFilter_t3943959056, ___filteringFactor_15)); }
	inline FsmFloat_t2883254149 * get_filteringFactor_15() const { return ___filteringFactor_15; }
	inline FsmFloat_t2883254149 ** get_address_of_filteringFactor_15() { return &___filteringFactor_15; }
	inline void set_filteringFactor_15(FsmFloat_t2883254149 * value)
	{
		___filteringFactor_15 = value;
		Il2CppCodeGenWriteBarrier((&___filteringFactor_15), value);
	}

	inline static int32_t get_offset_of_filteredVector_16() { return static_cast<int32_t>(offsetof(Vector2HighPassFilter_t3943959056, ___filteredVector_16)); }
	inline Vector2_t2156229523  get_filteredVector_16() const { return ___filteredVector_16; }
	inline Vector2_t2156229523 * get_address_of_filteredVector_16() { return &___filteredVector_16; }
	inline void set_filteredVector_16(Vector2_t2156229523  value)
	{
		___filteredVector_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2HIGHPASSFILTER_T3943959056_H
#ifndef VECTOR2INTERPOLATE_T1656170511_H
#define VECTOR2INTERPOLATE_T1656170511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Interpolate
struct  Vector2Interpolate_t1656170511  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.InterpolationType HutongGames.PlayMaker.Actions.Vector2Interpolate::mode
	int32_t ___mode_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Interpolate::fromVector
	FsmVector2_t2965096677 * ___fromVector_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Interpolate::toVector
	FsmVector2_t2965096677 * ___toVector_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2Interpolate::time
	FsmFloat_t2883254149 * ___time_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Interpolate::storeResult
	FsmVector2_t2965096677 * ___storeResult_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Vector2Interpolate::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Interpolate::realTime
	bool ___realTime_20;
	// System.Single HutongGames.PlayMaker.Actions.Vector2Interpolate::startTime
	float ___startTime_21;
	// System.Single HutongGames.PlayMaker.Actions.Vector2Interpolate::currentTime
	float ___currentTime_22;

public:
	inline static int32_t get_offset_of_mode_14() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___mode_14)); }
	inline int32_t get_mode_14() const { return ___mode_14; }
	inline int32_t* get_address_of_mode_14() { return &___mode_14; }
	inline void set_mode_14(int32_t value)
	{
		___mode_14 = value;
	}

	inline static int32_t get_offset_of_fromVector_15() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___fromVector_15)); }
	inline FsmVector2_t2965096677 * get_fromVector_15() const { return ___fromVector_15; }
	inline FsmVector2_t2965096677 ** get_address_of_fromVector_15() { return &___fromVector_15; }
	inline void set_fromVector_15(FsmVector2_t2965096677 * value)
	{
		___fromVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromVector_15), value);
	}

	inline static int32_t get_offset_of_toVector_16() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___toVector_16)); }
	inline FsmVector2_t2965096677 * get_toVector_16() const { return ___toVector_16; }
	inline FsmVector2_t2965096677 ** get_address_of_toVector_16() { return &___toVector_16; }
	inline void set_toVector_16(FsmVector2_t2965096677 * value)
	{
		___toVector_16 = value;
		Il2CppCodeGenWriteBarrier((&___toVector_16), value);
	}

	inline static int32_t get_offset_of_time_17() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___time_17)); }
	inline FsmFloat_t2883254149 * get_time_17() const { return ___time_17; }
	inline FsmFloat_t2883254149 ** get_address_of_time_17() { return &___time_17; }
	inline void set_time_17(FsmFloat_t2883254149 * value)
	{
		___time_17 = value;
		Il2CppCodeGenWriteBarrier((&___time_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___storeResult_18)); }
	inline FsmVector2_t2965096677 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmVector2_t2965096677 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmVector2_t2965096677 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___finishEvent_19)); }
	inline FsmEvent_t3736299882 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t3736299882 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_19), value);
	}

	inline static int32_t get_offset_of_realTime_20() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___realTime_20)); }
	inline bool get_realTime_20() const { return ___realTime_20; }
	inline bool* get_address_of_realTime_20() { return &___realTime_20; }
	inline void set_realTime_20(bool value)
	{
		___realTime_20 = value;
	}

	inline static int32_t get_offset_of_startTime_21() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___startTime_21)); }
	inline float get_startTime_21() const { return ___startTime_21; }
	inline float* get_address_of_startTime_21() { return &___startTime_21; }
	inline void set_startTime_21(float value)
	{
		___startTime_21 = value;
	}

	inline static int32_t get_offset_of_currentTime_22() { return static_cast<int32_t>(offsetof(Vector2Interpolate_t1656170511, ___currentTime_22)); }
	inline float get_currentTime_22() const { return ___currentTime_22; }
	inline float* get_address_of_currentTime_22() { return &___currentTime_22; }
	inline void set_currentTime_22(float value)
	{
		___currentTime_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INTERPOLATE_T1656170511_H
#ifndef VECTOR2INVERT_T838402421_H
#define VECTOR2INVERT_T838402421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Invert
struct  Vector2Invert_t838402421  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Invert::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Invert::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2Invert_t838402421, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector2Invert_t838402421, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INVERT_T838402421_H
#ifndef VECTOR2LERP_T3977169078_H
#define VECTOR2LERP_T3977169078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Lerp
struct  Vector2Lerp_t3977169078  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Lerp::fromVector
	FsmVector2_t2965096677 * ___fromVector_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Lerp::toVector
	FsmVector2_t2965096677 * ___toVector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2Lerp::amount
	FsmFloat_t2883254149 * ___amount_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Lerp::storeResult
	FsmVector2_t2965096677 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Lerp::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_fromVector_14() { return static_cast<int32_t>(offsetof(Vector2Lerp_t3977169078, ___fromVector_14)); }
	inline FsmVector2_t2965096677 * get_fromVector_14() const { return ___fromVector_14; }
	inline FsmVector2_t2965096677 ** get_address_of_fromVector_14() { return &___fromVector_14; }
	inline void set_fromVector_14(FsmVector2_t2965096677 * value)
	{
		___fromVector_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromVector_14), value);
	}

	inline static int32_t get_offset_of_toVector_15() { return static_cast<int32_t>(offsetof(Vector2Lerp_t3977169078, ___toVector_15)); }
	inline FsmVector2_t2965096677 * get_toVector_15() const { return ___toVector_15; }
	inline FsmVector2_t2965096677 ** get_address_of_toVector_15() { return &___toVector_15; }
	inline void set_toVector_15(FsmVector2_t2965096677 * value)
	{
		___toVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___toVector_15), value);
	}

	inline static int32_t get_offset_of_amount_16() { return static_cast<int32_t>(offsetof(Vector2Lerp_t3977169078, ___amount_16)); }
	inline FsmFloat_t2883254149 * get_amount_16() const { return ___amount_16; }
	inline FsmFloat_t2883254149 ** get_address_of_amount_16() { return &___amount_16; }
	inline void set_amount_16(FsmFloat_t2883254149 * value)
	{
		___amount_16 = value;
		Il2CppCodeGenWriteBarrier((&___amount_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(Vector2Lerp_t3977169078, ___storeResult_17)); }
	inline FsmVector2_t2965096677 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmVector2_t2965096677 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmVector2_t2965096677 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(Vector2Lerp_t3977169078, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2LERP_T3977169078_H
#ifndef VECTOR2LOWPASSFILTER_T435226625_H
#define VECTOR2LOWPASSFILTER_T435226625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2LowPassFilter
struct  Vector2LowPassFilter_t435226625  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2LowPassFilter::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2LowPassFilter::filteringFactor
	FsmFloat_t2883254149 * ___filteringFactor_15;
	// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.Vector2LowPassFilter::filteredVector
	Vector2_t2156229523  ___filteredVector_16;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2LowPassFilter_t435226625, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_filteringFactor_15() { return static_cast<int32_t>(offsetof(Vector2LowPassFilter_t435226625, ___filteringFactor_15)); }
	inline FsmFloat_t2883254149 * get_filteringFactor_15() const { return ___filteringFactor_15; }
	inline FsmFloat_t2883254149 ** get_address_of_filteringFactor_15() { return &___filteringFactor_15; }
	inline void set_filteringFactor_15(FsmFloat_t2883254149 * value)
	{
		___filteringFactor_15 = value;
		Il2CppCodeGenWriteBarrier((&___filteringFactor_15), value);
	}

	inline static int32_t get_offset_of_filteredVector_16() { return static_cast<int32_t>(offsetof(Vector2LowPassFilter_t435226625, ___filteredVector_16)); }
	inline Vector2_t2156229523  get_filteredVector_16() const { return ___filteredVector_16; }
	inline Vector2_t2156229523 * get_address_of_filteredVector_16() { return &___filteredVector_16; }
	inline void set_filteredVector_16(Vector2_t2156229523  value)
	{
		___filteredVector_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2LOWPASSFILTER_T435226625_H
#ifndef VECTOR2MOVETOWARDS_T306435168_H
#define VECTOR2MOVETOWARDS_T306435168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2MoveTowards
struct  Vector2MoveTowards_t306435168  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2MoveTowards::source
	FsmVector2_t2965096677 * ___source_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2MoveTowards::target
	FsmVector2_t2965096677 * ___target_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2MoveTowards::maxSpeed
	FsmFloat_t2883254149 * ___maxSpeed_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2MoveTowards::finishDistance
	FsmFloat_t2883254149 * ___finishDistance_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Vector2MoveTowards::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_18;

public:
	inline static int32_t get_offset_of_source_14() { return static_cast<int32_t>(offsetof(Vector2MoveTowards_t306435168, ___source_14)); }
	inline FsmVector2_t2965096677 * get_source_14() const { return ___source_14; }
	inline FsmVector2_t2965096677 ** get_address_of_source_14() { return &___source_14; }
	inline void set_source_14(FsmVector2_t2965096677 * value)
	{
		___source_14 = value;
		Il2CppCodeGenWriteBarrier((&___source_14), value);
	}

	inline static int32_t get_offset_of_target_15() { return static_cast<int32_t>(offsetof(Vector2MoveTowards_t306435168, ___target_15)); }
	inline FsmVector2_t2965096677 * get_target_15() const { return ___target_15; }
	inline FsmVector2_t2965096677 ** get_address_of_target_15() { return &___target_15; }
	inline void set_target_15(FsmVector2_t2965096677 * value)
	{
		___target_15 = value;
		Il2CppCodeGenWriteBarrier((&___target_15), value);
	}

	inline static int32_t get_offset_of_maxSpeed_16() { return static_cast<int32_t>(offsetof(Vector2MoveTowards_t306435168, ___maxSpeed_16)); }
	inline FsmFloat_t2883254149 * get_maxSpeed_16() const { return ___maxSpeed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_maxSpeed_16() { return &___maxSpeed_16; }
	inline void set_maxSpeed_16(FsmFloat_t2883254149 * value)
	{
		___maxSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&___maxSpeed_16), value);
	}

	inline static int32_t get_offset_of_finishDistance_17() { return static_cast<int32_t>(offsetof(Vector2MoveTowards_t306435168, ___finishDistance_17)); }
	inline FsmFloat_t2883254149 * get_finishDistance_17() const { return ___finishDistance_17; }
	inline FsmFloat_t2883254149 ** get_address_of_finishDistance_17() { return &___finishDistance_17; }
	inline void set_finishDistance_17(FsmFloat_t2883254149 * value)
	{
		___finishDistance_17 = value;
		Il2CppCodeGenWriteBarrier((&___finishDistance_17), value);
	}

	inline static int32_t get_offset_of_finishEvent_18() { return static_cast<int32_t>(offsetof(Vector2MoveTowards_t306435168, ___finishEvent_18)); }
	inline FsmEvent_t3736299882 * get_finishEvent_18() const { return ___finishEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_18() { return &___finishEvent_18; }
	inline void set_finishEvent_18(FsmEvent_t3736299882 * value)
	{
		___finishEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2MOVETOWARDS_T306435168_H
#ifndef VECTOR2MULTIPLY_T636802217_H
#define VECTOR2MULTIPLY_T636802217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Multiply
struct  Vector2Multiply_t636802217  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Multiply::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2Multiply::multiplyBy
	FsmFloat_t2883254149 * ___multiplyBy_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Multiply::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2Multiply_t636802217, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_multiplyBy_15() { return static_cast<int32_t>(offsetof(Vector2Multiply_t636802217, ___multiplyBy_15)); }
	inline FsmFloat_t2883254149 * get_multiplyBy_15() const { return ___multiplyBy_15; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplyBy_15() { return &___multiplyBy_15; }
	inline void set_multiplyBy_15(FsmFloat_t2883254149 * value)
	{
		___multiplyBy_15 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyBy_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector2Multiply_t636802217, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2MULTIPLY_T636802217_H
#ifndef VECTOR2NORMALIZE_T87641655_H
#define VECTOR2NORMALIZE_T87641655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Normalize
struct  Vector2Normalize_t87641655  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Normalize::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Normalize::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2Normalize_t87641655, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector2Normalize_t87641655, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2NORMALIZE_T87641655_H
#ifndef VECTOR2OPERATOR_T3401148003_H
#define VECTOR2OPERATOR_T3401148003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Operator
struct  Vector2Operator_t3401148003  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Operator::vector1
	FsmVector2_t2965096677 * ___vector1_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Operator::vector2
	FsmVector2_t2965096677 * ___vector2_15;
	// HutongGames.PlayMaker.Actions.Vector2Operator/Vector2Operation HutongGames.PlayMaker.Actions.Vector2Operator::operation
	int32_t ___operation_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Operator::storeVector2Result
	FsmVector2_t2965096677 * ___storeVector2Result_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2Operator::storeFloatResult
	FsmFloat_t2883254149 * ___storeFloatResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Operator::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_vector1_14() { return static_cast<int32_t>(offsetof(Vector2Operator_t3401148003, ___vector1_14)); }
	inline FsmVector2_t2965096677 * get_vector1_14() const { return ___vector1_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector1_14() { return &___vector1_14; }
	inline void set_vector1_14(FsmVector2_t2965096677 * value)
	{
		___vector1_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector1_14), value);
	}

	inline static int32_t get_offset_of_vector2_15() { return static_cast<int32_t>(offsetof(Vector2Operator_t3401148003, ___vector2_15)); }
	inline FsmVector2_t2965096677 * get_vector2_15() const { return ___vector2_15; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2_15() { return &___vector2_15; }
	inline void set_vector2_15(FsmVector2_t2965096677 * value)
	{
		___vector2_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2_15), value);
	}

	inline static int32_t get_offset_of_operation_16() { return static_cast<int32_t>(offsetof(Vector2Operator_t3401148003, ___operation_16)); }
	inline int32_t get_operation_16() const { return ___operation_16; }
	inline int32_t* get_address_of_operation_16() { return &___operation_16; }
	inline void set_operation_16(int32_t value)
	{
		___operation_16 = value;
	}

	inline static int32_t get_offset_of_storeVector2Result_17() { return static_cast<int32_t>(offsetof(Vector2Operator_t3401148003, ___storeVector2Result_17)); }
	inline FsmVector2_t2965096677 * get_storeVector2Result_17() const { return ___storeVector2Result_17; }
	inline FsmVector2_t2965096677 ** get_address_of_storeVector2Result_17() { return &___storeVector2Result_17; }
	inline void set_storeVector2Result_17(FsmVector2_t2965096677 * value)
	{
		___storeVector2Result_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector2Result_17), value);
	}

	inline static int32_t get_offset_of_storeFloatResult_18() { return static_cast<int32_t>(offsetof(Vector2Operator_t3401148003, ___storeFloatResult_18)); }
	inline FsmFloat_t2883254149 * get_storeFloatResult_18() const { return ___storeFloatResult_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeFloatResult_18() { return &___storeFloatResult_18; }
	inline void set_storeFloatResult_18(FsmFloat_t2883254149 * value)
	{
		___storeFloatResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeFloatResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(Vector2Operator_t3401148003, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2OPERATOR_T3401148003_H
#ifndef VECTOR2PERSECOND_T4028226088_H
#define VECTOR2PERSECOND_T4028226088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2PerSecond
struct  Vector2PerSecond_t4028226088  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2PerSecond::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2PerSecond::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2PerSecond_t4028226088, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector2PerSecond_t4028226088, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PERSECOND_T4028226088_H
#ifndef VECTOR2ROTATETOWARDS_T2909112328_H
#define VECTOR2ROTATETOWARDS_T2909112328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2RotateTowards
struct  Vector2RotateTowards_t2909112328  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2RotateTowards::currentDirection
	FsmVector2_t2965096677 * ___currentDirection_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2RotateTowards::targetDirection
	FsmVector2_t2965096677 * ___targetDirection_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector2RotateTowards::rotateSpeed
	FsmFloat_t2883254149 * ___rotateSpeed_16;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.Vector2RotateTowards::current
	Vector3_t3722313464  ___current_17;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.Vector2RotateTowards::target
	Vector3_t3722313464  ___target_18;

public:
	inline static int32_t get_offset_of_currentDirection_14() { return static_cast<int32_t>(offsetof(Vector2RotateTowards_t2909112328, ___currentDirection_14)); }
	inline FsmVector2_t2965096677 * get_currentDirection_14() const { return ___currentDirection_14; }
	inline FsmVector2_t2965096677 ** get_address_of_currentDirection_14() { return &___currentDirection_14; }
	inline void set_currentDirection_14(FsmVector2_t2965096677 * value)
	{
		___currentDirection_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentDirection_14), value);
	}

	inline static int32_t get_offset_of_targetDirection_15() { return static_cast<int32_t>(offsetof(Vector2RotateTowards_t2909112328, ___targetDirection_15)); }
	inline FsmVector2_t2965096677 * get_targetDirection_15() const { return ___targetDirection_15; }
	inline FsmVector2_t2965096677 ** get_address_of_targetDirection_15() { return &___targetDirection_15; }
	inline void set_targetDirection_15(FsmVector2_t2965096677 * value)
	{
		___targetDirection_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetDirection_15), value);
	}

	inline static int32_t get_offset_of_rotateSpeed_16() { return static_cast<int32_t>(offsetof(Vector2RotateTowards_t2909112328, ___rotateSpeed_16)); }
	inline FsmFloat_t2883254149 * get_rotateSpeed_16() const { return ___rotateSpeed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_rotateSpeed_16() { return &___rotateSpeed_16; }
	inline void set_rotateSpeed_16(FsmFloat_t2883254149 * value)
	{
		___rotateSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&___rotateSpeed_16), value);
	}

	inline static int32_t get_offset_of_current_17() { return static_cast<int32_t>(offsetof(Vector2RotateTowards_t2909112328, ___current_17)); }
	inline Vector3_t3722313464  get_current_17() const { return ___current_17; }
	inline Vector3_t3722313464 * get_address_of_current_17() { return &___current_17; }
	inline void set_current_17(Vector3_t3722313464  value)
	{
		___current_17 = value;
	}

	inline static int32_t get_offset_of_target_18() { return static_cast<int32_t>(offsetof(Vector2RotateTowards_t2909112328, ___target_18)); }
	inline Vector3_t3722313464  get_target_18() const { return ___target_18; }
	inline Vector3_t3722313464 * get_address_of_target_18() { return &___target_18; }
	inline void set_target_18(Vector3_t3722313464  value)
	{
		___target_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2ROTATETOWARDS_T2909112328_H
#ifndef VECTOR2SUBTRACT_T213488385_H
#define VECTOR2SUBTRACT_T213488385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector2Subtract
struct  Vector2Subtract_t213488385  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Subtract::vector2Variable
	FsmVector2_t2965096677 * ___vector2Variable_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector2Subtract::subtractVector
	FsmVector2_t2965096677 * ___subtractVector_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector2Subtract::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector2Variable_14() { return static_cast<int32_t>(offsetof(Vector2Subtract_t213488385, ___vector2Variable_14)); }
	inline FsmVector2_t2965096677 * get_vector2Variable_14() const { return ___vector2Variable_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Variable_14() { return &___vector2Variable_14; }
	inline void set_vector2Variable_14(FsmVector2_t2965096677 * value)
	{
		___vector2Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variable_14), value);
	}

	inline static int32_t get_offset_of_subtractVector_15() { return static_cast<int32_t>(offsetof(Vector2Subtract_t213488385, ___subtractVector_15)); }
	inline FsmVector2_t2965096677 * get_subtractVector_15() const { return ___subtractVector_15; }
	inline FsmVector2_t2965096677 ** get_address_of_subtractVector_15() { return &___subtractVector_15; }
	inline void set_subtractVector_15(FsmVector2_t2965096677 * value)
	{
		___subtractVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___subtractVector_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector2Subtract_t213488385, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2SUBTRACT_T213488385_H
#ifndef VECTOR3ADD_T3269288713_H
#define VECTOR3ADD_T3269288713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Add
struct  Vector3Add_t3269288713  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Add::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Add::addVector
	FsmVector3_t626444517 * ___addVector_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Add::everyFrame
	bool ___everyFrame_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Add::perSecond
	bool ___perSecond_17;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3Add_t3269288713, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_addVector_15() { return static_cast<int32_t>(offsetof(Vector3Add_t3269288713, ___addVector_15)); }
	inline FsmVector3_t626444517 * get_addVector_15() const { return ___addVector_15; }
	inline FsmVector3_t626444517 ** get_address_of_addVector_15() { return &___addVector_15; }
	inline void set_addVector_15(FsmVector3_t626444517 * value)
	{
		___addVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___addVector_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector3Add_t3269288713, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_perSecond_17() { return static_cast<int32_t>(offsetof(Vector3Add_t3269288713, ___perSecond_17)); }
	inline bool get_perSecond_17() const { return ___perSecond_17; }
	inline bool* get_address_of_perSecond_17() { return &___perSecond_17; }
	inline void set_perSecond_17(bool value)
	{
		___perSecond_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ADD_T3269288713_H
#ifndef VECTOR3ADDXYZ_T1772202739_H
#define VECTOR3ADDXYZ_T1772202739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3AddXYZ
struct  Vector3AddXYZ_t1772202739  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3AddXYZ::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3AddXYZ::addX
	FsmFloat_t2883254149 * ___addX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3AddXYZ::addY
	FsmFloat_t2883254149 * ___addY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3AddXYZ::addZ
	FsmFloat_t2883254149 * ___addZ_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3AddXYZ::everyFrame
	bool ___everyFrame_18;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3AddXYZ::perSecond
	bool ___perSecond_19;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3AddXYZ_t1772202739, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_addX_15() { return static_cast<int32_t>(offsetof(Vector3AddXYZ_t1772202739, ___addX_15)); }
	inline FsmFloat_t2883254149 * get_addX_15() const { return ___addX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_addX_15() { return &___addX_15; }
	inline void set_addX_15(FsmFloat_t2883254149 * value)
	{
		___addX_15 = value;
		Il2CppCodeGenWriteBarrier((&___addX_15), value);
	}

	inline static int32_t get_offset_of_addY_16() { return static_cast<int32_t>(offsetof(Vector3AddXYZ_t1772202739, ___addY_16)); }
	inline FsmFloat_t2883254149 * get_addY_16() const { return ___addY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_addY_16() { return &___addY_16; }
	inline void set_addY_16(FsmFloat_t2883254149 * value)
	{
		___addY_16 = value;
		Il2CppCodeGenWriteBarrier((&___addY_16), value);
	}

	inline static int32_t get_offset_of_addZ_17() { return static_cast<int32_t>(offsetof(Vector3AddXYZ_t1772202739, ___addZ_17)); }
	inline FsmFloat_t2883254149 * get_addZ_17() const { return ___addZ_17; }
	inline FsmFloat_t2883254149 ** get_address_of_addZ_17() { return &___addZ_17; }
	inline void set_addZ_17(FsmFloat_t2883254149 * value)
	{
		___addZ_17 = value;
		Il2CppCodeGenWriteBarrier((&___addZ_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(Vector3AddXYZ_t1772202739, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_perSecond_19() { return static_cast<int32_t>(offsetof(Vector3AddXYZ_t1772202739, ___perSecond_19)); }
	inline bool get_perSecond_19() const { return ___perSecond_19; }
	inline bool* get_address_of_perSecond_19() { return &___perSecond_19; }
	inline void set_perSecond_19(bool value)
	{
		___perSecond_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ADDXYZ_T1772202739_H
#ifndef VECTOR3CLAMPMAGNITUDE_T243703331_H
#define VECTOR3CLAMPMAGNITUDE_T243703331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3ClampMagnitude
struct  Vector3ClampMagnitude_t243703331  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::maxLength
	FsmFloat_t2883254149 * ___maxLength_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3ClampMagnitude_t243703331, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_maxLength_15() { return static_cast<int32_t>(offsetof(Vector3ClampMagnitude_t243703331, ___maxLength_15)); }
	inline FsmFloat_t2883254149 * get_maxLength_15() const { return ___maxLength_15; }
	inline FsmFloat_t2883254149 ** get_address_of_maxLength_15() { return &___maxLength_15; }
	inline void set_maxLength_15(FsmFloat_t2883254149 * value)
	{
		___maxLength_15 = value;
		Il2CppCodeGenWriteBarrier((&___maxLength_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector3ClampMagnitude_t243703331, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3CLAMPMAGNITUDE_T243703331_H
#ifndef VECTOR3HIGHPASSFILTER_T3943992817_H
#define VECTOR3HIGHPASSFILTER_T3943992817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3HighPassFilter
struct  Vector3HighPassFilter_t3943992817  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3HighPassFilter::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3HighPassFilter::filteringFactor
	FsmFloat_t2883254149 * ___filteringFactor_15;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.Vector3HighPassFilter::filteredVector
	Vector3_t3722313464  ___filteredVector_16;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3HighPassFilter_t3943992817, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_filteringFactor_15() { return static_cast<int32_t>(offsetof(Vector3HighPassFilter_t3943992817, ___filteringFactor_15)); }
	inline FsmFloat_t2883254149 * get_filteringFactor_15() const { return ___filteringFactor_15; }
	inline FsmFloat_t2883254149 ** get_address_of_filteringFactor_15() { return &___filteringFactor_15; }
	inline void set_filteringFactor_15(FsmFloat_t2883254149 * value)
	{
		___filteringFactor_15 = value;
		Il2CppCodeGenWriteBarrier((&___filteringFactor_15), value);
	}

	inline static int32_t get_offset_of_filteredVector_16() { return static_cast<int32_t>(offsetof(Vector3HighPassFilter_t3943992817, ___filteredVector_16)); }
	inline Vector3_t3722313464  get_filteredVector_16() const { return ___filteredVector_16; }
	inline Vector3_t3722313464 * get_address_of_filteredVector_16() { return &___filteredVector_16; }
	inline void set_filteredVector_16(Vector3_t3722313464  value)
	{
		___filteredVector_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3HIGHPASSFILTER_T3943992817_H
#ifndef VECTOR3INTERPOLATE_T1656171596_H
#define VECTOR3INTERPOLATE_T1656171596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Interpolate
struct  Vector3Interpolate_t1656171596  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.InterpolationType HutongGames.PlayMaker.Actions.Vector3Interpolate::mode
	int32_t ___mode_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Interpolate::fromVector
	FsmVector3_t626444517 * ___fromVector_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Interpolate::toVector
	FsmVector3_t626444517 * ___toVector_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Interpolate::time
	FsmFloat_t2883254149 * ___time_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Interpolate::storeResult
	FsmVector3_t626444517 * ___storeResult_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Vector3Interpolate::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Interpolate::realTime
	bool ___realTime_20;
	// System.Single HutongGames.PlayMaker.Actions.Vector3Interpolate::startTime
	float ___startTime_21;
	// System.Single HutongGames.PlayMaker.Actions.Vector3Interpolate::currentTime
	float ___currentTime_22;

public:
	inline static int32_t get_offset_of_mode_14() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___mode_14)); }
	inline int32_t get_mode_14() const { return ___mode_14; }
	inline int32_t* get_address_of_mode_14() { return &___mode_14; }
	inline void set_mode_14(int32_t value)
	{
		___mode_14 = value;
	}

	inline static int32_t get_offset_of_fromVector_15() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___fromVector_15)); }
	inline FsmVector3_t626444517 * get_fromVector_15() const { return ___fromVector_15; }
	inline FsmVector3_t626444517 ** get_address_of_fromVector_15() { return &___fromVector_15; }
	inline void set_fromVector_15(FsmVector3_t626444517 * value)
	{
		___fromVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromVector_15), value);
	}

	inline static int32_t get_offset_of_toVector_16() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___toVector_16)); }
	inline FsmVector3_t626444517 * get_toVector_16() const { return ___toVector_16; }
	inline FsmVector3_t626444517 ** get_address_of_toVector_16() { return &___toVector_16; }
	inline void set_toVector_16(FsmVector3_t626444517 * value)
	{
		___toVector_16 = value;
		Il2CppCodeGenWriteBarrier((&___toVector_16), value);
	}

	inline static int32_t get_offset_of_time_17() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___time_17)); }
	inline FsmFloat_t2883254149 * get_time_17() const { return ___time_17; }
	inline FsmFloat_t2883254149 ** get_address_of_time_17() { return &___time_17; }
	inline void set_time_17(FsmFloat_t2883254149 * value)
	{
		___time_17 = value;
		Il2CppCodeGenWriteBarrier((&___time_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___storeResult_18)); }
	inline FsmVector3_t626444517 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmVector3_t626444517 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmVector3_t626444517 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___finishEvent_19)); }
	inline FsmEvent_t3736299882 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t3736299882 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_19), value);
	}

	inline static int32_t get_offset_of_realTime_20() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___realTime_20)); }
	inline bool get_realTime_20() const { return ___realTime_20; }
	inline bool* get_address_of_realTime_20() { return &___realTime_20; }
	inline void set_realTime_20(bool value)
	{
		___realTime_20 = value;
	}

	inline static int32_t get_offset_of_startTime_21() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___startTime_21)); }
	inline float get_startTime_21() const { return ___startTime_21; }
	inline float* get_address_of_startTime_21() { return &___startTime_21; }
	inline void set_startTime_21(float value)
	{
		___startTime_21 = value;
	}

	inline static int32_t get_offset_of_currentTime_22() { return static_cast<int32_t>(offsetof(Vector3Interpolate_t1656171596, ___currentTime_22)); }
	inline float get_currentTime_22() const { return ___currentTime_22; }
	inline float* get_address_of_currentTime_22() { return &___currentTime_22; }
	inline void set_currentTime_22(float value)
	{
		___currentTime_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTERPOLATE_T1656171596_H
#ifndef VECTOR3INVERT_T838402458_H
#define VECTOR3INVERT_T838402458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Invert
struct  Vector3Invert_t838402458  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Invert::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Invert::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3Invert_t838402458, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector3Invert_t838402458, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INVERT_T838402458_H
#ifndef VECTOR3LERP_T3977169109_H
#define VECTOR3LERP_T3977169109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Lerp
struct  Vector3Lerp_t3977169109  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Lerp::fromVector
	FsmVector3_t626444517 * ___fromVector_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Lerp::toVector
	FsmVector3_t626444517 * ___toVector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Lerp::amount
	FsmFloat_t2883254149 * ___amount_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Lerp::storeResult
	FsmVector3_t626444517 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Lerp::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_fromVector_14() { return static_cast<int32_t>(offsetof(Vector3Lerp_t3977169109, ___fromVector_14)); }
	inline FsmVector3_t626444517 * get_fromVector_14() const { return ___fromVector_14; }
	inline FsmVector3_t626444517 ** get_address_of_fromVector_14() { return &___fromVector_14; }
	inline void set_fromVector_14(FsmVector3_t626444517 * value)
	{
		___fromVector_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromVector_14), value);
	}

	inline static int32_t get_offset_of_toVector_15() { return static_cast<int32_t>(offsetof(Vector3Lerp_t3977169109, ___toVector_15)); }
	inline FsmVector3_t626444517 * get_toVector_15() const { return ___toVector_15; }
	inline FsmVector3_t626444517 ** get_address_of_toVector_15() { return &___toVector_15; }
	inline void set_toVector_15(FsmVector3_t626444517 * value)
	{
		___toVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___toVector_15), value);
	}

	inline static int32_t get_offset_of_amount_16() { return static_cast<int32_t>(offsetof(Vector3Lerp_t3977169109, ___amount_16)); }
	inline FsmFloat_t2883254149 * get_amount_16() const { return ___amount_16; }
	inline FsmFloat_t2883254149 ** get_address_of_amount_16() { return &___amount_16; }
	inline void set_amount_16(FsmFloat_t2883254149 * value)
	{
		___amount_16 = value;
		Il2CppCodeGenWriteBarrier((&___amount_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(Vector3Lerp_t3977169109, ___storeResult_17)); }
	inline FsmVector3_t626444517 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmVector3_t626444517 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmVector3_t626444517 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(Vector3Lerp_t3977169109, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3LERP_T3977169109_H
#ifndef VECTOR3LOWPASSFILTER_T435448546_H
#define VECTOR3LOWPASSFILTER_T435448546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3LowPassFilter
struct  Vector3LowPassFilter_t435448546  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3LowPassFilter::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3LowPassFilter::filteringFactor
	FsmFloat_t2883254149 * ___filteringFactor_15;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.Vector3LowPassFilter::filteredVector
	Vector3_t3722313464  ___filteredVector_16;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3LowPassFilter_t435448546, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_filteringFactor_15() { return static_cast<int32_t>(offsetof(Vector3LowPassFilter_t435448546, ___filteringFactor_15)); }
	inline FsmFloat_t2883254149 * get_filteringFactor_15() const { return ___filteringFactor_15; }
	inline FsmFloat_t2883254149 ** get_address_of_filteringFactor_15() { return &___filteringFactor_15; }
	inline void set_filteringFactor_15(FsmFloat_t2883254149 * value)
	{
		___filteringFactor_15 = value;
		Il2CppCodeGenWriteBarrier((&___filteringFactor_15), value);
	}

	inline static int32_t get_offset_of_filteredVector_16() { return static_cast<int32_t>(offsetof(Vector3LowPassFilter_t435448546, ___filteredVector_16)); }
	inline Vector3_t3722313464  get_filteredVector_16() const { return ___filteredVector_16; }
	inline Vector3_t3722313464 * get_address_of_filteredVector_16() { return &___filteredVector_16; }
	inline void set_filteredVector_16(Vector3_t3722313464  value)
	{
		___filteredVector_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3LOWPASSFILTER_T435448546_H
#ifndef VECTOR3MULTIPLY_T636803490_H
#define VECTOR3MULTIPLY_T636803490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Multiply
struct  Vector3Multiply_t636803490  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Multiply::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Multiply::multiplyBy
	FsmFloat_t2883254149 * ___multiplyBy_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Multiply::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3Multiply_t636803490, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_multiplyBy_15() { return static_cast<int32_t>(offsetof(Vector3Multiply_t636803490, ___multiplyBy_15)); }
	inline FsmFloat_t2883254149 * get_multiplyBy_15() const { return ___multiplyBy_15; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplyBy_15() { return &___multiplyBy_15; }
	inline void set_multiplyBy_15(FsmFloat_t2883254149 * value)
	{
		___multiplyBy_15 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyBy_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector3Multiply_t636803490, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3MULTIPLY_T636803490_H
#ifndef VECTOR3NORMALIZE_T87642748_H
#define VECTOR3NORMALIZE_T87642748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Normalize
struct  Vector3Normalize_t87642748  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Normalize::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Normalize::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3Normalize_t87642748, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector3Normalize_t87642748, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3NORMALIZE_T87642748_H
#ifndef VECTOR3OPERATOR_T3401148894_H
#define VECTOR3OPERATOR_T3401148894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Operator
struct  Vector3Operator_t3401148894  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Operator::vector1
	FsmVector3_t626444517 * ___vector1_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Operator::vector2
	FsmVector3_t626444517 * ___vector2_15;
	// HutongGames.PlayMaker.Actions.Vector3Operator/Vector3Operation HutongGames.PlayMaker.Actions.Vector3Operator::operation
	int32_t ___operation_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Operator::storeVector3Result
	FsmVector3_t626444517 * ___storeVector3Result_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Operator::storeFloatResult
	FsmFloat_t2883254149 * ___storeFloatResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Operator::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_vector1_14() { return static_cast<int32_t>(offsetof(Vector3Operator_t3401148894, ___vector1_14)); }
	inline FsmVector3_t626444517 * get_vector1_14() const { return ___vector1_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector1_14() { return &___vector1_14; }
	inline void set_vector1_14(FsmVector3_t626444517 * value)
	{
		___vector1_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector1_14), value);
	}

	inline static int32_t get_offset_of_vector2_15() { return static_cast<int32_t>(offsetof(Vector3Operator_t3401148894, ___vector2_15)); }
	inline FsmVector3_t626444517 * get_vector2_15() const { return ___vector2_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector2_15() { return &___vector2_15; }
	inline void set_vector2_15(FsmVector3_t626444517 * value)
	{
		___vector2_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2_15), value);
	}

	inline static int32_t get_offset_of_operation_16() { return static_cast<int32_t>(offsetof(Vector3Operator_t3401148894, ___operation_16)); }
	inline int32_t get_operation_16() const { return ___operation_16; }
	inline int32_t* get_address_of_operation_16() { return &___operation_16; }
	inline void set_operation_16(int32_t value)
	{
		___operation_16 = value;
	}

	inline static int32_t get_offset_of_storeVector3Result_17() { return static_cast<int32_t>(offsetof(Vector3Operator_t3401148894, ___storeVector3Result_17)); }
	inline FsmVector3_t626444517 * get_storeVector3Result_17() const { return ___storeVector3Result_17; }
	inline FsmVector3_t626444517 ** get_address_of_storeVector3Result_17() { return &___storeVector3Result_17; }
	inline void set_storeVector3Result_17(FsmVector3_t626444517 * value)
	{
		___storeVector3Result_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector3Result_17), value);
	}

	inline static int32_t get_offset_of_storeFloatResult_18() { return static_cast<int32_t>(offsetof(Vector3Operator_t3401148894, ___storeFloatResult_18)); }
	inline FsmFloat_t2883254149 * get_storeFloatResult_18() const { return ___storeFloatResult_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeFloatResult_18() { return &___storeFloatResult_18; }
	inline void set_storeFloatResult_18(FsmFloat_t2883254149 * value)
	{
		___storeFloatResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeFloatResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(Vector3Operator_t3401148894, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3OPERATOR_T3401148894_H
#ifndef VECTOR3PERSECOND_T4028224785_H
#define VECTOR3PERSECOND_T4028224785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3PerSecond
struct  Vector3PerSecond_t4028224785  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3PerSecond::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3PerSecond::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3PerSecond_t4028224785, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(Vector3PerSecond_t4028224785, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3PERSECOND_T4028224785_H
#ifndef VECTOR3ROTATETOWARDS_T2909139811_H
#define VECTOR3ROTATETOWARDS_T2909139811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3RotateTowards
struct  Vector3RotateTowards_t2909139811  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3RotateTowards::currentDirection
	FsmVector3_t626444517 * ___currentDirection_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3RotateTowards::targetDirection
	FsmVector3_t626444517 * ___targetDirection_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3RotateTowards::rotateSpeed
	FsmFloat_t2883254149 * ___rotateSpeed_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3RotateTowards::maxMagnitude
	FsmFloat_t2883254149 * ___maxMagnitude_17;

public:
	inline static int32_t get_offset_of_currentDirection_14() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t2909139811, ___currentDirection_14)); }
	inline FsmVector3_t626444517 * get_currentDirection_14() const { return ___currentDirection_14; }
	inline FsmVector3_t626444517 ** get_address_of_currentDirection_14() { return &___currentDirection_14; }
	inline void set_currentDirection_14(FsmVector3_t626444517 * value)
	{
		___currentDirection_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentDirection_14), value);
	}

	inline static int32_t get_offset_of_targetDirection_15() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t2909139811, ___targetDirection_15)); }
	inline FsmVector3_t626444517 * get_targetDirection_15() const { return ___targetDirection_15; }
	inline FsmVector3_t626444517 ** get_address_of_targetDirection_15() { return &___targetDirection_15; }
	inline void set_targetDirection_15(FsmVector3_t626444517 * value)
	{
		___targetDirection_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetDirection_15), value);
	}

	inline static int32_t get_offset_of_rotateSpeed_16() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t2909139811, ___rotateSpeed_16)); }
	inline FsmFloat_t2883254149 * get_rotateSpeed_16() const { return ___rotateSpeed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_rotateSpeed_16() { return &___rotateSpeed_16; }
	inline void set_rotateSpeed_16(FsmFloat_t2883254149 * value)
	{
		___rotateSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&___rotateSpeed_16), value);
	}

	inline static int32_t get_offset_of_maxMagnitude_17() { return static_cast<int32_t>(offsetof(Vector3RotateTowards_t2909139811, ___maxMagnitude_17)); }
	inline FsmFloat_t2883254149 * get_maxMagnitude_17() const { return ___maxMagnitude_17; }
	inline FsmFloat_t2883254149 ** get_address_of_maxMagnitude_17() { return &___maxMagnitude_17; }
	inline void set_maxMagnitude_17(FsmFloat_t2883254149 * value)
	{
		___maxMagnitude_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxMagnitude_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ROTATETOWARDS_T2909139811_H
#ifndef VECTOR3SUBTRACT_T213489640_H
#define VECTOR3SUBTRACT_T213489640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Subtract
struct  Vector3Subtract_t213489640  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Subtract::vector3Variable
	FsmVector3_t626444517 * ___vector3Variable_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Subtract::subtractVector
	FsmVector3_t626444517 * ___subtractVector_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Subtract::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_vector3Variable_14() { return static_cast<int32_t>(offsetof(Vector3Subtract_t213489640, ___vector3Variable_14)); }
	inline FsmVector3_t626444517 * get_vector3Variable_14() const { return ___vector3Variable_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Variable_14() { return &___vector3Variable_14; }
	inline void set_vector3Variable_14(FsmVector3_t626444517 * value)
	{
		___vector3Variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variable_14), value);
	}

	inline static int32_t get_offset_of_subtractVector_15() { return static_cast<int32_t>(offsetof(Vector3Subtract_t213489640, ___subtractVector_15)); }
	inline FsmVector3_t626444517 * get_subtractVector_15() const { return ___subtractVector_15; }
	inline FsmVector3_t626444517 ** get_address_of_subtractVector_15() { return &___subtractVector_15; }
	inline void set_subtractVector_15(FsmVector3_t626444517 * value)
	{
		___subtractVector_15 = value;
		Il2CppCodeGenWriteBarrier((&___subtractVector_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(Vector3Subtract_t213489640, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3SUBTRACT_T213489640_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef UIINPUTFIELDGETTEXTASFLOAT_T3905871643_H
#define UIINPUTFIELDGETTEXTASFLOAT_T3905871643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat
struct  UiInputFieldGetTextAsFloat_t3905871643  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::value
	FsmFloat_t2883254149 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::isFloat
	FsmBool_t163807967 * ___isFloat_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::isFloatEvent
	FsmEvent_t3736299882 * ___isFloatEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::isNotFloatEvent
	FsmEvent_t3736299882 * ___isNotFloatEvent_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::inputField
	InputField_t3762917431 * ___inputField_22;
	// System.Single HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::_value
	float ____value_23;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsFloat::_success
	bool ____success_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_isFloat_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___isFloat_18)); }
	inline FsmBool_t163807967 * get_isFloat_18() const { return ___isFloat_18; }
	inline FsmBool_t163807967 ** get_address_of_isFloat_18() { return &___isFloat_18; }
	inline void set_isFloat_18(FsmBool_t163807967 * value)
	{
		___isFloat_18 = value;
		Il2CppCodeGenWriteBarrier((&___isFloat_18), value);
	}

	inline static int32_t get_offset_of_isFloatEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___isFloatEvent_19)); }
	inline FsmEvent_t3736299882 * get_isFloatEvent_19() const { return ___isFloatEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isFloatEvent_19() { return &___isFloatEvent_19; }
	inline void set_isFloatEvent_19(FsmEvent_t3736299882 * value)
	{
		___isFloatEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isFloatEvent_19), value);
	}

	inline static int32_t get_offset_of_isNotFloatEvent_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___isNotFloatEvent_20)); }
	inline FsmEvent_t3736299882 * get_isNotFloatEvent_20() const { return ___isNotFloatEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotFloatEvent_20() { return &___isNotFloatEvent_20; }
	inline void set_isNotFloatEvent_20(FsmEvent_t3736299882 * value)
	{
		___isNotFloatEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___isNotFloatEvent_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_inputField_22() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ___inputField_22)); }
	inline InputField_t3762917431 * get_inputField_22() const { return ___inputField_22; }
	inline InputField_t3762917431 ** get_address_of_inputField_22() { return &___inputField_22; }
	inline void set_inputField_22(InputField_t3762917431 * value)
	{
		___inputField_22 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_22), value);
	}

	inline static int32_t get_offset_of__value_23() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ____value_23)); }
	inline float get__value_23() const { return ____value_23; }
	inline float* get_address_of__value_23() { return &____value_23; }
	inline void set__value_23(float value)
	{
		____value_23 = value;
	}

	inline static int32_t get_offset_of__success_24() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsFloat_t3905871643, ____success_24)); }
	inline bool get__success_24() const { return ____success_24; }
	inline bool* get_address_of__success_24() { return &____success_24; }
	inline void set__success_24(bool value)
	{
		____success_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETTEXTASFLOAT_T3905871643_H
#ifndef UIINPUTFIELDGETTEXTASINT_T2882175745_H
#define UIINPUTFIELDGETTEXTASINT_T2882175745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt
struct  UiInputFieldGetTextAsInt_t2882175745  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::value
	FsmInt_t874273141 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::isInt
	FsmBool_t163807967 * ___isInt_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::isIntEvent
	FsmEvent_t3736299882 * ___isIntEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::isNotIntEvent
	FsmEvent_t3736299882 * ___isNotIntEvent_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::inputField
	InputField_t3762917431 * ___inputField_22;
	// System.Int32 HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::_value
	int32_t ____value_23;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetTextAsInt::_success
	bool ____success_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___value_17)); }
	inline FsmInt_t874273141 * get_value_17() const { return ___value_17; }
	inline FsmInt_t874273141 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmInt_t874273141 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_isInt_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___isInt_18)); }
	inline FsmBool_t163807967 * get_isInt_18() const { return ___isInt_18; }
	inline FsmBool_t163807967 ** get_address_of_isInt_18() { return &___isInt_18; }
	inline void set_isInt_18(FsmBool_t163807967 * value)
	{
		___isInt_18 = value;
		Il2CppCodeGenWriteBarrier((&___isInt_18), value);
	}

	inline static int32_t get_offset_of_isIntEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___isIntEvent_19)); }
	inline FsmEvent_t3736299882 * get_isIntEvent_19() const { return ___isIntEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isIntEvent_19() { return &___isIntEvent_19; }
	inline void set_isIntEvent_19(FsmEvent_t3736299882 * value)
	{
		___isIntEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isIntEvent_19), value);
	}

	inline static int32_t get_offset_of_isNotIntEvent_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___isNotIntEvent_20)); }
	inline FsmEvent_t3736299882 * get_isNotIntEvent_20() const { return ___isNotIntEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotIntEvent_20() { return &___isNotIntEvent_20; }
	inline void set_isNotIntEvent_20(FsmEvent_t3736299882 * value)
	{
		___isNotIntEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___isNotIntEvent_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_inputField_22() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ___inputField_22)); }
	inline InputField_t3762917431 * get_inputField_22() const { return ___inputField_22; }
	inline InputField_t3762917431 ** get_address_of_inputField_22() { return &___inputField_22; }
	inline void set_inputField_22(InputField_t3762917431 * value)
	{
		___inputField_22 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_22), value);
	}

	inline static int32_t get_offset_of__value_23() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ____value_23)); }
	inline int32_t get__value_23() const { return ____value_23; }
	inline int32_t* get_address_of__value_23() { return &____value_23; }
	inline void set__value_23(int32_t value)
	{
		____value_23 = value;
	}

	inline static int32_t get_offset_of__success_24() { return static_cast<int32_t>(offsetof(UiInputFieldGetTextAsInt_t2882175745, ____success_24)); }
	inline bool get__success_24() const { return ____success_24; }
	inline bool* get_address_of__success_24() { return &____success_24; }
	inline void set__success_24(bool value)
	{
		____success_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETTEXTASINT_T2882175745_H
#ifndef UIINPUTFIELDGETWASCANCELED_T3143818148_H
#define UIINPUTFIELDGETWASCANCELED_T3143818148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetWasCanceled
struct  UiInputFieldGetWasCanceled_t3143818148  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetWasCanceled::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldGetWasCanceled::wasCanceled
	FsmBool_t163807967 * ___wasCanceled_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetWasCanceled::wasCanceledEvent
	FsmEvent_t3736299882 * ___wasCanceledEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetWasCanceled::wasNotCanceledEvent
	FsmEvent_t3736299882 * ___wasNotCanceledEvent_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetWasCanceled::inputField
	InputField_t3762917431 * ___inputField_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetWasCanceled_t3143818148, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_wasCanceled_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetWasCanceled_t3143818148, ___wasCanceled_17)); }
	inline FsmBool_t163807967 * get_wasCanceled_17() const { return ___wasCanceled_17; }
	inline FsmBool_t163807967 ** get_address_of_wasCanceled_17() { return &___wasCanceled_17; }
	inline void set_wasCanceled_17(FsmBool_t163807967 * value)
	{
		___wasCanceled_17 = value;
		Il2CppCodeGenWriteBarrier((&___wasCanceled_17), value);
	}

	inline static int32_t get_offset_of_wasCanceledEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetWasCanceled_t3143818148, ___wasCanceledEvent_18)); }
	inline FsmEvent_t3736299882 * get_wasCanceledEvent_18() const { return ___wasCanceledEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_wasCanceledEvent_18() { return &___wasCanceledEvent_18; }
	inline void set_wasCanceledEvent_18(FsmEvent_t3736299882 * value)
	{
		___wasCanceledEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___wasCanceledEvent_18), value);
	}

	inline static int32_t get_offset_of_wasNotCanceledEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetWasCanceled_t3143818148, ___wasNotCanceledEvent_19)); }
	inline FsmEvent_t3736299882 * get_wasNotCanceledEvent_19() const { return ___wasNotCanceledEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_wasNotCanceledEvent_19() { return &___wasNotCanceledEvent_19; }
	inline void set_wasNotCanceledEvent_19(FsmEvent_t3736299882 * value)
	{
		___wasNotCanceledEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___wasNotCanceledEvent_19), value);
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetWasCanceled_t3143818148, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETWASCANCELED_T3143818148_H
#ifndef UIINPUTFIELDMOVECARETTOTEXTEND_T2789998028_H
#define UIINPUTFIELDMOVECARETTOTEXTEND_T2789998028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextEnd
struct  UiInputFieldMoveCaretToTextEnd_t2789998028  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextEnd::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextEnd::shift
	FsmBool_t163807967 * ___shift_17;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextEnd::inputField
	InputField_t3762917431 * ___inputField_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldMoveCaretToTextEnd_t2789998028, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_shift_17() { return static_cast<int32_t>(offsetof(UiInputFieldMoveCaretToTextEnd_t2789998028, ___shift_17)); }
	inline FsmBool_t163807967 * get_shift_17() const { return ___shift_17; }
	inline FsmBool_t163807967 ** get_address_of_shift_17() { return &___shift_17; }
	inline void set_shift_17(FsmBool_t163807967 * value)
	{
		___shift_17 = value;
		Il2CppCodeGenWriteBarrier((&___shift_17), value);
	}

	inline static int32_t get_offset_of_inputField_18() { return static_cast<int32_t>(offsetof(UiInputFieldMoveCaretToTextEnd_t2789998028, ___inputField_18)); }
	inline InputField_t3762917431 * get_inputField_18() const { return ___inputField_18; }
	inline InputField_t3762917431 ** get_address_of_inputField_18() { return &___inputField_18; }
	inline void set_inputField_18(InputField_t3762917431 * value)
	{
		___inputField_18 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDMOVECARETTOTEXTEND_T2789998028_H
#ifndef UIINPUTFIELDMOVECARETTOTEXTSTART_T554352859_H
#define UIINPUTFIELDMOVECARETTOTEXTSTART_T554352859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextStart
struct  UiInputFieldMoveCaretToTextStart_t554352859  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextStart::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextStart::shift
	FsmBool_t163807967 * ___shift_17;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldMoveCaretToTextStart::inputField
	InputField_t3762917431 * ___inputField_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldMoveCaretToTextStart_t554352859, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_shift_17() { return static_cast<int32_t>(offsetof(UiInputFieldMoveCaretToTextStart_t554352859, ___shift_17)); }
	inline FsmBool_t163807967 * get_shift_17() const { return ___shift_17; }
	inline FsmBool_t163807967 ** get_address_of_shift_17() { return &___shift_17; }
	inline void set_shift_17(FsmBool_t163807967 * value)
	{
		___shift_17 = value;
		Il2CppCodeGenWriteBarrier((&___shift_17), value);
	}

	inline static int32_t get_offset_of_inputField_18() { return static_cast<int32_t>(offsetof(UiInputFieldMoveCaretToTextStart_t554352859, ___inputField_18)); }
	inline InputField_t3762917431 * get_inputField_18() const { return ___inputField_18; }
	inline InputField_t3762917431 ** get_address_of_inputField_18() { return &___inputField_18; }
	inline void set_inputField_18(InputField_t3762917431 * value)
	{
		___inputField_18 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDMOVECARETTOTEXTSTART_T554352859_H
#ifndef UIINPUTFIELDONENDEDITEVENT_T3952452798_H
#define UIINPUTFIELDONENDEDITEVENT_T3952452798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent
struct  UiInputFieldOnEndEditEvent_t3952452798  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent::text
	FsmString_t1785915204 * ___text_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent::wasCanceled
	FsmBool_t163807967 * ___wasCanceled_20;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldOnEndEditEvent::inputField
	InputField_t3762917431 * ___inputField_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldOnEndEditEvent_t3952452798, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiInputFieldOnEndEditEvent_t3952452798, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldOnEndEditEvent_t3952452798, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_text_19() { return static_cast<int32_t>(offsetof(UiInputFieldOnEndEditEvent_t3952452798, ___text_19)); }
	inline FsmString_t1785915204 * get_text_19() const { return ___text_19; }
	inline FsmString_t1785915204 ** get_address_of_text_19() { return &___text_19; }
	inline void set_text_19(FsmString_t1785915204 * value)
	{
		___text_19 = value;
		Il2CppCodeGenWriteBarrier((&___text_19), value);
	}

	inline static int32_t get_offset_of_wasCanceled_20() { return static_cast<int32_t>(offsetof(UiInputFieldOnEndEditEvent_t3952452798, ___wasCanceled_20)); }
	inline FsmBool_t163807967 * get_wasCanceled_20() const { return ___wasCanceled_20; }
	inline FsmBool_t163807967 ** get_address_of_wasCanceled_20() { return &___wasCanceled_20; }
	inline void set_wasCanceled_20(FsmBool_t163807967 * value)
	{
		___wasCanceled_20 = value;
		Il2CppCodeGenWriteBarrier((&___wasCanceled_20), value);
	}

	inline static int32_t get_offset_of_inputField_21() { return static_cast<int32_t>(offsetof(UiInputFieldOnEndEditEvent_t3952452798, ___inputField_21)); }
	inline InputField_t3762917431 * get_inputField_21() const { return ___inputField_21; }
	inline InputField_t3762917431 ** get_address_of_inputField_21() { return &___inputField_21; }
	inline void set_inputField_21(InputField_t3762917431 * value)
	{
		___inputField_21 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDONENDEDITEVENT_T3952452798_H
#ifndef UIINPUTFIELDONSUBMITEVENT_T9451400_H
#define UIINPUTFIELDONSUBMITEVENT_T9451400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldOnSubmitEvent
struct  UiInputFieldOnSubmitEvent_t9451400  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldOnSubmitEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiInputFieldOnSubmitEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldOnSubmitEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiInputFieldOnSubmitEvent::text
	FsmString_t1785915204 * ___text_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldOnSubmitEvent::inputField
	InputField_t3762917431 * ___inputField_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldOnSubmitEvent_t9451400, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiInputFieldOnSubmitEvent_t9451400, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldOnSubmitEvent_t9451400, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_text_19() { return static_cast<int32_t>(offsetof(UiInputFieldOnSubmitEvent_t9451400, ___text_19)); }
	inline FsmString_t1785915204 * get_text_19() const { return ___text_19; }
	inline FsmString_t1785915204 ** get_address_of_text_19() { return &___text_19; }
	inline void set_text_19(FsmString_t1785915204 * value)
	{
		___text_19 = value;
		Il2CppCodeGenWriteBarrier((&___text_19), value);
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldOnSubmitEvent_t9451400, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDONSUBMITEVENT_T9451400_H
#ifndef UIINPUTFIELDONVALUECHANGEEVENT_T2570439459_H
#define UIINPUTFIELDONVALUECHANGEEVENT_T2570439459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldOnValueChangeEvent
struct  UiInputFieldOnValueChangeEvent_t2570439459  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldOnValueChangeEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiInputFieldOnValueChangeEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldOnValueChangeEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiInputFieldOnValueChangeEvent::text
	FsmString_t1785915204 * ___text_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldOnValueChangeEvent::inputField
	InputField_t3762917431 * ___inputField_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldOnValueChangeEvent_t2570439459, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiInputFieldOnValueChangeEvent_t2570439459, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldOnValueChangeEvent_t2570439459, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_text_19() { return static_cast<int32_t>(offsetof(UiInputFieldOnValueChangeEvent_t2570439459, ___text_19)); }
	inline FsmString_t1785915204 * get_text_19() const { return ___text_19; }
	inline FsmString_t1785915204 ** get_address_of_text_19() { return &___text_19; }
	inline void set_text_19(FsmString_t1785915204 * value)
	{
		___text_19 = value;
		Il2CppCodeGenWriteBarrier((&___text_19), value);
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldOnValueChangeEvent_t2570439459, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDONVALUECHANGEEVENT_T2570439459_H
#ifndef UIINPUTFIELDSETASTERIXCHAR_T3822249831_H
#define UIINPUTFIELDSETASTERIXCHAR_T3822249831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar
struct  UiInputFieldSetAsterixChar_t3822249831  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar::asterixChar
	FsmString_t1785915204 * ___asterixChar_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar::inputField
	InputField_t3762917431 * ___inputField_19;
	// System.Char HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar::originalValue
	Il2CppChar ___originalValue_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetAsterixChar_t3822249831, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_asterixChar_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetAsterixChar_t3822249831, ___asterixChar_17)); }
	inline FsmString_t1785915204 * get_asterixChar_17() const { return ___asterixChar_17; }
	inline FsmString_t1785915204 ** get_address_of_asterixChar_17() { return &___asterixChar_17; }
	inline void set_asterixChar_17(FsmString_t1785915204 * value)
	{
		___asterixChar_17 = value;
		Il2CppCodeGenWriteBarrier((&___asterixChar_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetAsterixChar_t3822249831, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_inputField_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetAsterixChar_t3822249831, ___inputField_19)); }
	inline InputField_t3762917431 * get_inputField_19() const { return ___inputField_19; }
	inline InputField_t3762917431 ** get_address_of_inputField_19() { return &___inputField_19; }
	inline void set_inputField_19(InputField_t3762917431 * value)
	{
		___inputField_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_19), value);
	}

	inline static int32_t get_offset_of_originalValue_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetAsterixChar_t3822249831, ___originalValue_20)); }
	inline Il2CppChar get_originalValue_20() const { return ___originalValue_20; }
	inline Il2CppChar* get_address_of_originalValue_20() { return &___originalValue_20; }
	inline void set_originalValue_20(Il2CppChar value)
	{
		___originalValue_20 = value;
	}
};

struct UiInputFieldSetAsterixChar_t3822249831_StaticFields
{
public:
	// System.Char HutongGames.PlayMaker.Actions.UiInputFieldSetAsterixChar::__char__
	Il2CppChar _____char___21;

public:
	inline static int32_t get_offset_of___char___21() { return static_cast<int32_t>(offsetof(UiInputFieldSetAsterixChar_t3822249831_StaticFields, _____char___21)); }
	inline Il2CppChar get___char___21() const { return _____char___21; }
	inline Il2CppChar* get_address_of___char___21() { return &_____char___21; }
	inline void set___char___21(Il2CppChar value)
	{
		_____char___21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETASTERIXCHAR_T3822249831_H
#ifndef UIINPUTFIELDSETCARETBLINKRATE_T1782366990_H
#define UIINPUTFIELDSETCARETBLINKRATE_T1782366990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate
struct  UiInputFieldSetCaretBlinkRate_t1782366990  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate::caretBlinkRate
	FsmInt_t874273141 * ___caretBlinkRate_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate::inputField
	InputField_t3762917431 * ___inputField_20;
	// System.Single HutongGames.PlayMaker.Actions.UiInputFieldSetCaretBlinkRate::originalValue
	float ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetCaretBlinkRate_t1782366990, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_caretBlinkRate_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetCaretBlinkRate_t1782366990, ___caretBlinkRate_17)); }
	inline FsmInt_t874273141 * get_caretBlinkRate_17() const { return ___caretBlinkRate_17; }
	inline FsmInt_t874273141 ** get_address_of_caretBlinkRate_17() { return &___caretBlinkRate_17; }
	inline void set_caretBlinkRate_17(FsmInt_t874273141 * value)
	{
		___caretBlinkRate_17 = value;
		Il2CppCodeGenWriteBarrier((&___caretBlinkRate_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetCaretBlinkRate_t1782366990, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetCaretBlinkRate_t1782366990, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetCaretBlinkRate_t1782366990, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiInputFieldSetCaretBlinkRate_t1782366990, ___originalValue_21)); }
	inline float get_originalValue_21() const { return ___originalValue_21; }
	inline float* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(float value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETCARETBLINKRATE_T1782366990_H
#ifndef UIINPUTFIELDSETCHARACTERLIMIT_T3722784285_H
#define UIINPUTFIELDSETCHARACTERLIMIT_T3722784285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit
struct  UiInputFieldSetCharacterLimit_t3722784285  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit::characterLimit
	FsmInt_t874273141 * ___characterLimit_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit::inputField
	InputField_t3762917431 * ___inputField_20;
	// System.Int32 HutongGames.PlayMaker.Actions.UiInputFieldSetCharacterLimit::originalValue
	int32_t ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetCharacterLimit_t3722784285, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_characterLimit_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetCharacterLimit_t3722784285, ___characterLimit_17)); }
	inline FsmInt_t874273141 * get_characterLimit_17() const { return ___characterLimit_17; }
	inline FsmInt_t874273141 ** get_address_of_characterLimit_17() { return &___characterLimit_17; }
	inline void set_characterLimit_17(FsmInt_t874273141 * value)
	{
		___characterLimit_17 = value;
		Il2CppCodeGenWriteBarrier((&___characterLimit_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetCharacterLimit_t3722784285, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetCharacterLimit_t3722784285, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetCharacterLimit_t3722784285, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiInputFieldSetCharacterLimit_t3722784285, ___originalValue_21)); }
	inline int32_t get_originalValue_21() const { return ___originalValue_21; }
	inline int32_t* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(int32_t value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETCHARACTERLIMIT_T3722784285_H
#ifndef UIINPUTFIELDSETHIDEMOBILEINPUT_T1921728854_H
#define UIINPUTFIELDSETHIDEMOBILEINPUT_T1921728854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetHideMobileInput
struct  UiInputFieldSetHideMobileInput_t1921728854  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetHideMobileInput::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetHideMobileInput::hideMobileInput
	FsmBool_t163807967 * ___hideMobileInput_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetHideMobileInput::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetHideMobileInput::inputField
	InputField_t3762917431 * ___inputField_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldSetHideMobileInput::originalValue
	bool ___originalValue_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetHideMobileInput_t1921728854, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_hideMobileInput_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetHideMobileInput_t1921728854, ___hideMobileInput_17)); }
	inline FsmBool_t163807967 * get_hideMobileInput_17() const { return ___hideMobileInput_17; }
	inline FsmBool_t163807967 ** get_address_of_hideMobileInput_17() { return &___hideMobileInput_17; }
	inline void set_hideMobileInput_17(FsmBool_t163807967 * value)
	{
		___hideMobileInput_17 = value;
		Il2CppCodeGenWriteBarrier((&___hideMobileInput_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetHideMobileInput_t1921728854, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_inputField_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetHideMobileInput_t1921728854, ___inputField_19)); }
	inline InputField_t3762917431 * get_inputField_19() const { return ___inputField_19; }
	inline InputField_t3762917431 ** get_address_of_inputField_19() { return &___inputField_19; }
	inline void set_inputField_19(InputField_t3762917431 * value)
	{
		___inputField_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_19), value);
	}

	inline static int32_t get_offset_of_originalValue_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetHideMobileInput_t1921728854, ___originalValue_20)); }
	inline bool get_originalValue_20() const { return ___originalValue_20; }
	inline bool* get_address_of_originalValue_20() { return &___originalValue_20; }
	inline void set_originalValue_20(bool value)
	{
		___originalValue_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETHIDEMOBILEINPUT_T1921728854_H
#ifndef UIINPUTFIELDSETPLACEHOLDER_T3526529484_H
#define UIINPUTFIELDSETPLACEHOLDER_T3526529484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetPlaceHolder
struct  UiInputFieldSetPlaceHolder_t3526529484  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetPlaceHolder::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiInputFieldSetPlaceHolder::placeholder
	FsmGameObject_t3581898942 * ___placeholder_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetPlaceHolder::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetPlaceHolder::inputField
	InputField_t3762917431 * ___inputField_19;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.UiInputFieldSetPlaceHolder::originalValue
	Graphic_t1660335611 * ___originalValue_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetPlaceHolder_t3526529484, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_placeholder_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetPlaceHolder_t3526529484, ___placeholder_17)); }
	inline FsmGameObject_t3581898942 * get_placeholder_17() const { return ___placeholder_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_placeholder_17() { return &___placeholder_17; }
	inline void set_placeholder_17(FsmGameObject_t3581898942 * value)
	{
		___placeholder_17 = value;
		Il2CppCodeGenWriteBarrier((&___placeholder_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetPlaceHolder_t3526529484, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_inputField_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetPlaceHolder_t3526529484, ___inputField_19)); }
	inline InputField_t3762917431 * get_inputField_19() const { return ___inputField_19; }
	inline InputField_t3762917431 ** get_address_of_inputField_19() { return &___inputField_19; }
	inline void set_inputField_19(InputField_t3762917431 * value)
	{
		___inputField_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_19), value);
	}

	inline static int32_t get_offset_of_originalValue_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetPlaceHolder_t3526529484, ___originalValue_20)); }
	inline Graphic_t1660335611 * get_originalValue_20() const { return ___originalValue_20; }
	inline Graphic_t1660335611 ** get_address_of_originalValue_20() { return &___originalValue_20; }
	inline void set_originalValue_20(Graphic_t1660335611 * value)
	{
		___originalValue_20 = value;
		Il2CppCodeGenWriteBarrier((&___originalValue_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETPLACEHOLDER_T3526529484_H
#ifndef UIINPUTFIELDSETSELECTIONCOLOR_T3194565660_H
#define UIINPUTFIELDSETSELECTIONCOLOR_T3194565660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor
struct  UiInputFieldSetSelectionColor_t3194565660  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor::selectionColor
	FsmColor_t1738900188 * ___selectionColor_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor::inputField
	InputField_t3762917431 * ___inputField_20;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.UiInputFieldSetSelectionColor::originalValue
	Color_t2555686324  ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetSelectionColor_t3194565660, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_selectionColor_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetSelectionColor_t3194565660, ___selectionColor_17)); }
	inline FsmColor_t1738900188 * get_selectionColor_17() const { return ___selectionColor_17; }
	inline FsmColor_t1738900188 ** get_address_of_selectionColor_17() { return &___selectionColor_17; }
	inline void set_selectionColor_17(FsmColor_t1738900188 * value)
	{
		___selectionColor_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectionColor_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetSelectionColor_t3194565660, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetSelectionColor_t3194565660, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetSelectionColor_t3194565660, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiInputFieldSetSelectionColor_t3194565660, ___originalValue_21)); }
	inline Color_t2555686324  get_originalValue_21() const { return ___originalValue_21; }
	inline Color_t2555686324 * get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(Color_t2555686324  value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETSELECTIONCOLOR_T3194565660_H
#ifndef UIINPUTFIELDSETTEXT_T3586869520_H
#define UIINPUTFIELDSETTEXT_T3586869520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldSetText
struct  UiInputFieldSetText_t3586869520  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldSetText::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiInputFieldSetText::text
	FsmString_t1785915204 * ___text_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldSetText::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldSetText::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldSetText::inputField
	InputField_t3762917431 * ___inputField_20;
	// System.String HutongGames.PlayMaker.Actions.UiInputFieldSetText::originalString
	String_t* ___originalString_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldSetText_t3586869520, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(UiInputFieldSetText_t3586869520, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiInputFieldSetText_t3586869520, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiInputFieldSetText_t3586869520, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldSetText_t3586869520, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}

	inline static int32_t get_offset_of_originalString_21() { return static_cast<int32_t>(offsetof(UiInputFieldSetText_t3586869520, ___originalString_21)); }
	inline String_t* get_originalString_21() const { return ___originalString_21; }
	inline String_t** get_address_of_originalString_21() { return &___originalString_21; }
	inline void set_originalString_21(String_t* value)
	{
		___originalString_21 = value;
		Il2CppCodeGenWriteBarrier((&___originalString_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDSETTEXT_T3586869520_H
#ifndef UIRAWIMAGESETTEXTURE_T1884493090_H
#define UIRAWIMAGESETTEXTURE_T1884493090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiRawImageSetTexture
struct  UiRawImageSetTexture_t1884493090  : public ComponentAction_1_t1765238890
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiRawImageSetTexture::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.UiRawImageSetTexture::texture
	FsmTexture_t1738787045 * ___texture_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiRawImageSetTexture::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.RawImage HutongGames.PlayMaker.Actions.UiRawImageSetTexture::_texture
	RawImage_t3182918964 * ____texture_19;
	// UnityEngine.Texture HutongGames.PlayMaker.Actions.UiRawImageSetTexture::_originalTexture
	Texture_t3661962703 * ____originalTexture_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiRawImageSetTexture_t1884493090, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_texture_17() { return static_cast<int32_t>(offsetof(UiRawImageSetTexture_t1884493090, ___texture_17)); }
	inline FsmTexture_t1738787045 * get_texture_17() const { return ___texture_17; }
	inline FsmTexture_t1738787045 ** get_address_of_texture_17() { return &___texture_17; }
	inline void set_texture_17(FsmTexture_t1738787045 * value)
	{
		___texture_17 = value;
		Il2CppCodeGenWriteBarrier((&___texture_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiRawImageSetTexture_t1884493090, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of__texture_19() { return static_cast<int32_t>(offsetof(UiRawImageSetTexture_t1884493090, ____texture_19)); }
	inline RawImage_t3182918964 * get__texture_19() const { return ____texture_19; }
	inline RawImage_t3182918964 ** get_address_of__texture_19() { return &____texture_19; }
	inline void set__texture_19(RawImage_t3182918964 * value)
	{
		____texture_19 = value;
		Il2CppCodeGenWriteBarrier((&____texture_19), value);
	}

	inline static int32_t get_offset_of__originalTexture_20() { return static_cast<int32_t>(offsetof(UiRawImageSetTexture_t1884493090, ____originalTexture_20)); }
	inline Texture_t3661962703 * get__originalTexture_20() const { return ____originalTexture_20; }
	inline Texture_t3661962703 ** get_address_of__originalTexture_20() { return &____originalTexture_20; }
	inline void set__originalTexture_20(Texture_t3661962703 * value)
	{
		____originalTexture_20 = value;
		Il2CppCodeGenWriteBarrier((&____originalTexture_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIRAWIMAGESETTEXTURE_T1884493090_H
#ifndef UIREBUILD_T1491182680_H
#define UIREBUILD_T1491182680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiRebuild
struct  UiRebuild_t1491182680  : public ComponentAction_1_t242655537
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiRebuild::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.UI.CanvasUpdate HutongGames.PlayMaker.Actions.UiRebuild::canvasUpdate
	int32_t ___canvasUpdate_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiRebuild::rebuildOnExit
	bool ___rebuildOnExit_18;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.UiRebuild::graphic
	Graphic_t1660335611 * ___graphic_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiRebuild_t1491182680, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_canvasUpdate_17() { return static_cast<int32_t>(offsetof(UiRebuild_t1491182680, ___canvasUpdate_17)); }
	inline int32_t get_canvasUpdate_17() const { return ___canvasUpdate_17; }
	inline int32_t* get_address_of_canvasUpdate_17() { return &___canvasUpdate_17; }
	inline void set_canvasUpdate_17(int32_t value)
	{
		___canvasUpdate_17 = value;
	}

	inline static int32_t get_offset_of_rebuildOnExit_18() { return static_cast<int32_t>(offsetof(UiRebuild_t1491182680, ___rebuildOnExit_18)); }
	inline bool get_rebuildOnExit_18() const { return ___rebuildOnExit_18; }
	inline bool* get_address_of_rebuildOnExit_18() { return &___rebuildOnExit_18; }
	inline void set_rebuildOnExit_18(bool value)
	{
		___rebuildOnExit_18 = value;
	}

	inline static int32_t get_offset_of_graphic_19() { return static_cast<int32_t>(offsetof(UiRebuild_t1491182680, ___graphic_19)); }
	inline Graphic_t1660335611 * get_graphic_19() const { return ___graphic_19; }
	inline Graphic_t1660335611 ** get_address_of_graphic_19() { return &___graphic_19; }
	inline void set_graphic_19(Graphic_t1660335611 * value)
	{
		___graphic_19 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIREBUILD_T1491182680_H
#ifndef UISCROLLRECTSETHORIZONTAL_T2924575077_H
#define UISCROLLRECTSETHORIZONTAL_T2924575077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal
struct  UiScrollRectSetHorizontal_t2924575077  : public ComponentAction_1_t2720175740
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal::horizontal
	FsmBool_t163807967 * ___horizontal_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.ScrollRect HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollRectSetHorizontal::originalValue
	bool ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollRectSetHorizontal_t2924575077, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_horizontal_17() { return static_cast<int32_t>(offsetof(UiScrollRectSetHorizontal_t2924575077, ___horizontal_17)); }
	inline FsmBool_t163807967 * get_horizontal_17() const { return ___horizontal_17; }
	inline FsmBool_t163807967 ** get_address_of_horizontal_17() { return &___horizontal_17; }
	inline void set_horizontal_17(FsmBool_t163807967 * value)
	{
		___horizontal_17 = value;
		Il2CppCodeGenWriteBarrier((&___horizontal_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiScrollRectSetHorizontal_t2924575077, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiScrollRectSetHorizontal_t2924575077, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_scrollRect_20() { return static_cast<int32_t>(offsetof(UiScrollRectSetHorizontal_t2924575077, ___scrollRect_20)); }
	inline ScrollRect_t4137855814 * get_scrollRect_20() const { return ___scrollRect_20; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_20() { return &___scrollRect_20; }
	inline void set_scrollRect_20(ScrollRect_t4137855814 * value)
	{
		___scrollRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiScrollRectSetHorizontal_t2924575077, ___originalValue_21)); }
	inline bool get_originalValue_21() const { return ___originalValue_21; }
	inline bool* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(bool value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLRECTSETHORIZONTAL_T2924575077_H
#ifndef UISCROLLRECTSETNORMALIZEDPOSITION_T833414855_H
#define UISCROLLRECTSETNORMALIZEDPOSITION_T833414855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition
struct  UiScrollRectSetNormalizedPosition_t833414855  : public ComponentAction_1_t2720175740
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::normalizedPosition
	FsmVector2_t2965096677 * ___normalizedPosition_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::horizontalPosition
	FsmFloat_t2883254149 * ___horizontalPosition_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::verticalPosition
	FsmFloat_t2883254149 * ___verticalPosition_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.UI.ScrollRect HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_22;
	// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.UiScrollRectSetNormalizedPosition::originalValue
	Vector2_t2156229523  ___originalValue_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_normalizedPosition_17() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___normalizedPosition_17)); }
	inline FsmVector2_t2965096677 * get_normalizedPosition_17() const { return ___normalizedPosition_17; }
	inline FsmVector2_t2965096677 ** get_address_of_normalizedPosition_17() { return &___normalizedPosition_17; }
	inline void set_normalizedPosition_17(FsmVector2_t2965096677 * value)
	{
		___normalizedPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedPosition_17), value);
	}

	inline static int32_t get_offset_of_horizontalPosition_18() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___horizontalPosition_18)); }
	inline FsmFloat_t2883254149 * get_horizontalPosition_18() const { return ___horizontalPosition_18; }
	inline FsmFloat_t2883254149 ** get_address_of_horizontalPosition_18() { return &___horizontalPosition_18; }
	inline void set_horizontalPosition_18(FsmFloat_t2883254149 * value)
	{
		___horizontalPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalPosition_18), value);
	}

	inline static int32_t get_offset_of_verticalPosition_19() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___verticalPosition_19)); }
	inline FsmFloat_t2883254149 * get_verticalPosition_19() const { return ___verticalPosition_19; }
	inline FsmFloat_t2883254149 ** get_address_of_verticalPosition_19() { return &___verticalPosition_19; }
	inline void set_verticalPosition_19(FsmFloat_t2883254149 * value)
	{
		___verticalPosition_19 = value;
		Il2CppCodeGenWriteBarrier((&___verticalPosition_19), value);
	}

	inline static int32_t get_offset_of_resetOnExit_20() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___resetOnExit_20)); }
	inline FsmBool_t163807967 * get_resetOnExit_20() const { return ___resetOnExit_20; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_20() { return &___resetOnExit_20; }
	inline void set_resetOnExit_20(FsmBool_t163807967 * value)
	{
		___resetOnExit_20 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_scrollRect_22() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___scrollRect_22)); }
	inline ScrollRect_t4137855814 * get_scrollRect_22() const { return ___scrollRect_22; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_22() { return &___scrollRect_22; }
	inline void set_scrollRect_22(ScrollRect_t4137855814 * value)
	{
		___scrollRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_22), value);
	}

	inline static int32_t get_offset_of_originalValue_23() { return static_cast<int32_t>(offsetof(UiScrollRectSetNormalizedPosition_t833414855, ___originalValue_23)); }
	inline Vector2_t2156229523  get_originalValue_23() const { return ___originalValue_23; }
	inline Vector2_t2156229523 * get_address_of_originalValue_23() { return &___originalValue_23; }
	inline void set_originalValue_23(Vector2_t2156229523  value)
	{
		___originalValue_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLRECTSETNORMALIZEDPOSITION_T833414855_H
#ifndef UISCROLLRECTSETVERTICAL_T893328469_H
#define UISCROLLRECTSETVERTICAL_T893328469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollRectSetVertical
struct  UiScrollRectSetVertical_t893328469  : public ComponentAction_1_t2720175740
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollRectSetVertical::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollRectSetVertical::vertical
	FsmBool_t163807967 * ___vertical_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollRectSetVertical::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollRectSetVertical::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.ScrollRect HutongGames.PlayMaker.Actions.UiScrollRectSetVertical::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollRectSetVertical::originalValue
	bool ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollRectSetVertical_t893328469, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_vertical_17() { return static_cast<int32_t>(offsetof(UiScrollRectSetVertical_t893328469, ___vertical_17)); }
	inline FsmBool_t163807967 * get_vertical_17() const { return ___vertical_17; }
	inline FsmBool_t163807967 ** get_address_of_vertical_17() { return &___vertical_17; }
	inline void set_vertical_17(FsmBool_t163807967 * value)
	{
		___vertical_17 = value;
		Il2CppCodeGenWriteBarrier((&___vertical_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiScrollRectSetVertical_t893328469, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiScrollRectSetVertical_t893328469, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_scrollRect_20() { return static_cast<int32_t>(offsetof(UiScrollRectSetVertical_t893328469, ___scrollRect_20)); }
	inline ScrollRect_t4137855814 * get_scrollRect_20() const { return ___scrollRect_20; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_20() { return &___scrollRect_20; }
	inline void set_scrollRect_20(ScrollRect_t4137855814 * value)
	{
		___scrollRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiScrollRectSetVertical_t893328469, ___originalValue_21)); }
	inline bool get_originalValue_21() const { return ___originalValue_21; }
	inline bool* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(bool value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLRECTSETVERTICAL_T893328469_H
#ifndef UISCROLLBARGETDIRECTION_T1527513757_H
#define UISCROLLBARGETDIRECTION_T1527513757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarGetDirection
struct  UiScrollbarGetDirection_t1527513757  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarGetDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiScrollbarGetDirection::direction
	FsmEnum_t2861764163 * ___direction_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollbarGetDirection::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarGetDirection::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarGetDirection_t1527513757, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_direction_17() { return static_cast<int32_t>(offsetof(UiScrollbarGetDirection_t1527513757, ___direction_17)); }
	inline FsmEnum_t2861764163 * get_direction_17() const { return ___direction_17; }
	inline FsmEnum_t2861764163 ** get_address_of_direction_17() { return &___direction_17; }
	inline void set_direction_17(FsmEnum_t2861764163 * value)
	{
		___direction_17 = value;
		Il2CppCodeGenWriteBarrier((&___direction_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiScrollbarGetDirection_t1527513757, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_scrollbar_19() { return static_cast<int32_t>(offsetof(UiScrollbarGetDirection_t1527513757, ___scrollbar_19)); }
	inline Scrollbar_t1494447233 * get_scrollbar_19() const { return ___scrollbar_19; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_19() { return &___scrollbar_19; }
	inline void set_scrollbar_19(Scrollbar_t1494447233 * value)
	{
		___scrollbar_19 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARGETDIRECTION_T1527513757_H
#ifndef UISCROLLBARGETVALUE_T2957191981_H
#define UISCROLLBARGETVALUE_T2957191981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarGetValue
struct  UiScrollbarGetValue_t2957191981  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarGetValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiScrollbarGetValue::value
	FsmFloat_t2883254149 * ___value_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollbarGetValue::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarGetValue::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarGetValue_t2957191981, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiScrollbarGetValue_t2957191981, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiScrollbarGetValue_t2957191981, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_scrollbar_19() { return static_cast<int32_t>(offsetof(UiScrollbarGetValue_t2957191981, ___scrollbar_19)); }
	inline Scrollbar_t1494447233 * get_scrollbar_19() const { return ___scrollbar_19; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_19() { return &___scrollbar_19; }
	inline void set_scrollbar_19(Scrollbar_t1494447233 * value)
	{
		___scrollbar_19 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARGETVALUE_T2957191981_H
#ifndef UISCROLLBARONVALUECHANGED_T2078301051_H
#define UISCROLLBARONVALUECHANGED_T2078301051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarOnValueChanged
struct  UiScrollbarOnValueChanged_t2078301051  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarOnValueChanged::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiScrollbarOnValueChanged::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiScrollbarOnValueChanged::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiScrollbarOnValueChanged::value
	FsmFloat_t2883254149 * ___value_19;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarOnValueChanged::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarOnValueChanged_t2078301051, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiScrollbarOnValueChanged_t2078301051, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiScrollbarOnValueChanged_t2078301051, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_value_19() { return static_cast<int32_t>(offsetof(UiScrollbarOnValueChanged_t2078301051, ___value_19)); }
	inline FsmFloat_t2883254149 * get_value_19() const { return ___value_19; }
	inline FsmFloat_t2883254149 ** get_address_of_value_19() { return &___value_19; }
	inline void set_value_19(FsmFloat_t2883254149 * value)
	{
		___value_19 = value;
		Il2CppCodeGenWriteBarrier((&___value_19), value);
	}

	inline static int32_t get_offset_of_scrollbar_20() { return static_cast<int32_t>(offsetof(UiScrollbarOnValueChanged_t2078301051, ___scrollbar_20)); }
	inline Scrollbar_t1494447233 * get_scrollbar_20() const { return ___scrollbar_20; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_20() { return &___scrollbar_20; }
	inline void set_scrollbar_20(Scrollbar_t1494447233 * value)
	{
		___scrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARONVALUECHANGED_T2078301051_H
#ifndef UISCROLLBARSETDIRECTION_T2376598163_H
#define UISCROLLBARSETDIRECTION_T2376598163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarSetDirection
struct  UiScrollbarSetDirection_t2376598163  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarSetDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiScrollbarSetDirection::direction
	FsmEnum_t2861764163 * ___direction_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollbarSetDirection::includeRectLayouts
	FsmBool_t163807967 * ___includeRectLayouts_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollbarSetDirection::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_19;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarSetDirection::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_20;
	// UnityEngine.UI.Scrollbar/Direction HutongGames.PlayMaker.Actions.UiScrollbarSetDirection::originalValue
	int32_t ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarSetDirection_t2376598163, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_direction_17() { return static_cast<int32_t>(offsetof(UiScrollbarSetDirection_t2376598163, ___direction_17)); }
	inline FsmEnum_t2861764163 * get_direction_17() const { return ___direction_17; }
	inline FsmEnum_t2861764163 ** get_address_of_direction_17() { return &___direction_17; }
	inline void set_direction_17(FsmEnum_t2861764163 * value)
	{
		___direction_17 = value;
		Il2CppCodeGenWriteBarrier((&___direction_17), value);
	}

	inline static int32_t get_offset_of_includeRectLayouts_18() { return static_cast<int32_t>(offsetof(UiScrollbarSetDirection_t2376598163, ___includeRectLayouts_18)); }
	inline FsmBool_t163807967 * get_includeRectLayouts_18() const { return ___includeRectLayouts_18; }
	inline FsmBool_t163807967 ** get_address_of_includeRectLayouts_18() { return &___includeRectLayouts_18; }
	inline void set_includeRectLayouts_18(FsmBool_t163807967 * value)
	{
		___includeRectLayouts_18 = value;
		Il2CppCodeGenWriteBarrier((&___includeRectLayouts_18), value);
	}

	inline static int32_t get_offset_of_resetOnExit_19() { return static_cast<int32_t>(offsetof(UiScrollbarSetDirection_t2376598163, ___resetOnExit_19)); }
	inline FsmBool_t163807967 * get_resetOnExit_19() const { return ___resetOnExit_19; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_19() { return &___resetOnExit_19; }
	inline void set_resetOnExit_19(FsmBool_t163807967 * value)
	{
		___resetOnExit_19 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_19), value);
	}

	inline static int32_t get_offset_of_scrollbar_20() { return static_cast<int32_t>(offsetof(UiScrollbarSetDirection_t2376598163, ___scrollbar_20)); }
	inline Scrollbar_t1494447233 * get_scrollbar_20() const { return ___scrollbar_20; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_20() { return &___scrollbar_20; }
	inline void set_scrollbar_20(Scrollbar_t1494447233 * value)
	{
		___scrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiScrollbarSetDirection_t2376598163, ___originalValue_21)); }
	inline int32_t get_originalValue_21() const { return ___originalValue_21; }
	inline int32_t* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(int32_t value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARSETDIRECTION_T2376598163_H
#ifndef UISCROLLBARSETNUMBEROFSTEPS_T1614973516_H
#define UISCROLLBARSETNUMBEROFSTEPS_T1614973516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps
struct  UiScrollbarSetNumberOfSteps_t1614973516  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps::value
	FsmInt_t874273141 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_20;
	// System.Int32 HutongGames.PlayMaker.Actions.UiScrollbarSetNumberOfSteps::originalValue
	int32_t ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarSetNumberOfSteps_t1614973516, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiScrollbarSetNumberOfSteps_t1614973516, ___value_17)); }
	inline FsmInt_t874273141 * get_value_17() const { return ___value_17; }
	inline FsmInt_t874273141 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmInt_t874273141 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiScrollbarSetNumberOfSteps_t1614973516, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiScrollbarSetNumberOfSteps_t1614973516, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_scrollbar_20() { return static_cast<int32_t>(offsetof(UiScrollbarSetNumberOfSteps_t1614973516, ___scrollbar_20)); }
	inline Scrollbar_t1494447233 * get_scrollbar_20() const { return ___scrollbar_20; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_20() { return &___scrollbar_20; }
	inline void set_scrollbar_20(Scrollbar_t1494447233 * value)
	{
		___scrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiScrollbarSetNumberOfSteps_t1614973516, ___originalValue_21)); }
	inline int32_t get_originalValue_21() const { return ___originalValue_21; }
	inline int32_t* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(int32_t value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARSETNUMBEROFSTEPS_T1614973516_H
#ifndef UISCROLLBARSETSIZE_T1003663313_H
#define UISCROLLBARSETSIZE_T1003663313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarSetSize
struct  UiScrollbarSetSize_t1003663313  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarSetSize::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiScrollbarSetSize::value
	FsmFloat_t2883254149 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollbarSetSize::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollbarSetSize::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarSetSize::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_20;
	// System.Single HutongGames.PlayMaker.Actions.UiScrollbarSetSize::originalValue
	float ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarSetSize_t1003663313, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiScrollbarSetSize_t1003663313, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiScrollbarSetSize_t1003663313, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiScrollbarSetSize_t1003663313, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_scrollbar_20() { return static_cast<int32_t>(offsetof(UiScrollbarSetSize_t1003663313, ___scrollbar_20)); }
	inline Scrollbar_t1494447233 * get_scrollbar_20() const { return ___scrollbar_20; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_20() { return &___scrollbar_20; }
	inline void set_scrollbar_20(Scrollbar_t1494447233 * value)
	{
		___scrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiScrollbarSetSize_t1003663313, ___originalValue_21)); }
	inline float get_originalValue_21() const { return ___originalValue_21; }
	inline float* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(float value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARSETSIZE_T1003663313_H
#ifndef UISCROLLBARSETVALUE_T4246154029_H
#define UISCROLLBARSETVALUE_T4246154029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiScrollbarSetValue
struct  UiScrollbarSetValue_t4246154029  : public ComponentAction_1_t76767159
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiScrollbarSetValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiScrollbarSetValue::value
	FsmFloat_t2883254149 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiScrollbarSetValue::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiScrollbarSetValue::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.UiScrollbarSetValue::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_20;
	// System.Single HutongGames.PlayMaker.Actions.UiScrollbarSetValue::originalValue
	float ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiScrollbarSetValue_t4246154029, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiScrollbarSetValue_t4246154029, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiScrollbarSetValue_t4246154029, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiScrollbarSetValue_t4246154029, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_scrollbar_20() { return static_cast<int32_t>(offsetof(UiScrollbarSetValue_t4246154029, ___scrollbar_20)); }
	inline Scrollbar_t1494447233 * get_scrollbar_20() const { return ___scrollbar_20; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_20() { return &___scrollbar_20; }
	inline void set_scrollbar_20(Scrollbar_t1494447233 * value)
	{
		___scrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiScrollbarSetValue_t4246154029, ___originalValue_21)); }
	inline float get_originalValue_21() const { return ___originalValue_21; }
	inline float* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(float value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCROLLBARSETVALUE_T4246154029_H
#ifndef UISLIDERGETDIRECTION_T3868044733_H
#define UISLIDERGETDIRECTION_T3868044733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderGetDirection
struct  UiSliderGetDirection_t3868044733  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderGetDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiSliderGetDirection::direction
	FsmEnum_t2861764163 * ___direction_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderGetDirection::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderGetDirection::slider
	Slider_t3903728902 * ___slider_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderGetDirection_t3868044733, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_direction_17() { return static_cast<int32_t>(offsetof(UiSliderGetDirection_t3868044733, ___direction_17)); }
	inline FsmEnum_t2861764163 * get_direction_17() const { return ___direction_17; }
	inline FsmEnum_t2861764163 ** get_address_of_direction_17() { return &___direction_17; }
	inline void set_direction_17(FsmEnum_t2861764163 * value)
	{
		___direction_17 = value;
		Il2CppCodeGenWriteBarrier((&___direction_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiSliderGetDirection_t3868044733, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_slider_19() { return static_cast<int32_t>(offsetof(UiSliderGetDirection_t3868044733, ___slider_19)); }
	inline Slider_t3903728902 * get_slider_19() const { return ___slider_19; }
	inline Slider_t3903728902 ** get_address_of_slider_19() { return &___slider_19; }
	inline void set_slider_19(Slider_t3903728902 * value)
	{
		___slider_19 = value;
		Il2CppCodeGenWriteBarrier((&___slider_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERGETDIRECTION_T3868044733_H
#ifndef UISLIDERGETMINMAX_T1181160557_H
#define UISLIDERGETMINMAX_T1181160557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderGetMinMax
struct  UiSliderGetMinMax_t1181160557  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderGetMinMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderGetMinMax::minValue
	FsmFloat_t2883254149 * ___minValue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderGetMinMax::maxValue
	FsmFloat_t2883254149 * ___maxValue_18;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderGetMinMax::slider
	Slider_t3903728902 * ___slider_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderGetMinMax_t1181160557, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_minValue_17() { return static_cast<int32_t>(offsetof(UiSliderGetMinMax_t1181160557, ___minValue_17)); }
	inline FsmFloat_t2883254149 * get_minValue_17() const { return ___minValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_minValue_17() { return &___minValue_17; }
	inline void set_minValue_17(FsmFloat_t2883254149 * value)
	{
		___minValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___minValue_17), value);
	}

	inline static int32_t get_offset_of_maxValue_18() { return static_cast<int32_t>(offsetof(UiSliderGetMinMax_t1181160557, ___maxValue_18)); }
	inline FsmFloat_t2883254149 * get_maxValue_18() const { return ___maxValue_18; }
	inline FsmFloat_t2883254149 ** get_address_of_maxValue_18() { return &___maxValue_18; }
	inline void set_maxValue_18(FsmFloat_t2883254149 * value)
	{
		___maxValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___maxValue_18), value);
	}

	inline static int32_t get_offset_of_slider_19() { return static_cast<int32_t>(offsetof(UiSliderGetMinMax_t1181160557, ___slider_19)); }
	inline Slider_t3903728902 * get_slider_19() const { return ___slider_19; }
	inline Slider_t3903728902 ** get_address_of_slider_19() { return &___slider_19; }
	inline void set_slider_19(Slider_t3903728902 * value)
	{
		___slider_19 = value;
		Il2CppCodeGenWriteBarrier((&___slider_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERGETMINMAX_T1181160557_H
#ifndef UISLIDERGETNORMALIZEDVALUE_T613760320_H
#define UISLIDERGETNORMALIZEDVALUE_T613760320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderGetNormalizedValue
struct  UiSliderGetNormalizedValue_t613760320  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderGetNormalizedValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderGetNormalizedValue::value
	FsmFloat_t2883254149 * ___value_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderGetNormalizedValue::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderGetNormalizedValue::slider
	Slider_t3903728902 * ___slider_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderGetNormalizedValue_t613760320, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiSliderGetNormalizedValue_t613760320, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiSliderGetNormalizedValue_t613760320, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_slider_19() { return static_cast<int32_t>(offsetof(UiSliderGetNormalizedValue_t613760320, ___slider_19)); }
	inline Slider_t3903728902 * get_slider_19() const { return ___slider_19; }
	inline Slider_t3903728902 ** get_address_of_slider_19() { return &___slider_19; }
	inline void set_slider_19(Slider_t3903728902 * value)
	{
		___slider_19 = value;
		Il2CppCodeGenWriteBarrier((&___slider_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERGETNORMALIZEDVALUE_T613760320_H
#ifndef UISLIDERGETVALUE_T2578351891_H
#define UISLIDERGETVALUE_T2578351891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderGetValue
struct  UiSliderGetValue_t2578351891  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderGetValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderGetValue::value
	FsmFloat_t2883254149 * ___value_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderGetValue::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderGetValue::slider
	Slider_t3903728902 * ___slider_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderGetValue_t2578351891, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiSliderGetValue_t2578351891, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiSliderGetValue_t2578351891, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_slider_19() { return static_cast<int32_t>(offsetof(UiSliderGetValue_t2578351891, ___slider_19)); }
	inline Slider_t3903728902 * get_slider_19() const { return ___slider_19; }
	inline Slider_t3903728902 ** get_address_of_slider_19() { return &___slider_19; }
	inline void set_slider_19(Slider_t3903728902 * value)
	{
		___slider_19 = value;
		Il2CppCodeGenWriteBarrier((&___slider_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERGETVALUE_T2578351891_H
#ifndef UISLIDERGETWHOLENUMBERS_T3953068173_H
#define UISLIDERGETWHOLENUMBERS_T3953068173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderGetWholeNumbers
struct  UiSliderGetWholeNumbers_t3953068173  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderGetWholeNumbers::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderGetWholeNumbers::wholeNumbers
	FsmBool_t163807967 * ___wholeNumbers_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiSliderGetWholeNumbers::isShowingWholeNumbersEvent
	FsmEvent_t3736299882 * ___isShowingWholeNumbersEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiSliderGetWholeNumbers::isNotShowingWholeNumbersEvent
	FsmEvent_t3736299882 * ___isNotShowingWholeNumbersEvent_19;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderGetWholeNumbers::slider
	Slider_t3903728902 * ___slider_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderGetWholeNumbers_t3953068173, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_wholeNumbers_17() { return static_cast<int32_t>(offsetof(UiSliderGetWholeNumbers_t3953068173, ___wholeNumbers_17)); }
	inline FsmBool_t163807967 * get_wholeNumbers_17() const { return ___wholeNumbers_17; }
	inline FsmBool_t163807967 ** get_address_of_wholeNumbers_17() { return &___wholeNumbers_17; }
	inline void set_wholeNumbers_17(FsmBool_t163807967 * value)
	{
		___wholeNumbers_17 = value;
		Il2CppCodeGenWriteBarrier((&___wholeNumbers_17), value);
	}

	inline static int32_t get_offset_of_isShowingWholeNumbersEvent_18() { return static_cast<int32_t>(offsetof(UiSliderGetWholeNumbers_t3953068173, ___isShowingWholeNumbersEvent_18)); }
	inline FsmEvent_t3736299882 * get_isShowingWholeNumbersEvent_18() const { return ___isShowingWholeNumbersEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_isShowingWholeNumbersEvent_18() { return &___isShowingWholeNumbersEvent_18; }
	inline void set_isShowingWholeNumbersEvent_18(FsmEvent_t3736299882 * value)
	{
		___isShowingWholeNumbersEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___isShowingWholeNumbersEvent_18), value);
	}

	inline static int32_t get_offset_of_isNotShowingWholeNumbersEvent_19() { return static_cast<int32_t>(offsetof(UiSliderGetWholeNumbers_t3953068173, ___isNotShowingWholeNumbersEvent_19)); }
	inline FsmEvent_t3736299882 * get_isNotShowingWholeNumbersEvent_19() const { return ___isNotShowingWholeNumbersEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotShowingWholeNumbersEvent_19() { return &___isNotShowingWholeNumbersEvent_19; }
	inline void set_isNotShowingWholeNumbersEvent_19(FsmEvent_t3736299882 * value)
	{
		___isNotShowingWholeNumbersEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isNotShowingWholeNumbersEvent_19), value);
	}

	inline static int32_t get_offset_of_slider_20() { return static_cast<int32_t>(offsetof(UiSliderGetWholeNumbers_t3953068173, ___slider_20)); }
	inline Slider_t3903728902 * get_slider_20() const { return ___slider_20; }
	inline Slider_t3903728902 ** get_address_of_slider_20() { return &___slider_20; }
	inline void set_slider_20(Slider_t3903728902 * value)
	{
		___slider_20 = value;
		Il2CppCodeGenWriteBarrier((&___slider_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERGETWHOLENUMBERS_T3953068173_H
#ifndef UISLIDERONVALUECHANGEDEVENT_T330195802_H
#define UISLIDERONVALUECHANGEDEVENT_T330195802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderOnValueChangedEvent
struct  UiSliderOnValueChangedEvent_t330195802  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderOnValueChangedEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiSliderOnValueChangedEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiSliderOnValueChangedEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderOnValueChangedEvent::value
	FsmFloat_t2883254149 * ___value_19;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderOnValueChangedEvent::slider
	Slider_t3903728902 * ___slider_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderOnValueChangedEvent_t330195802, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiSliderOnValueChangedEvent_t330195802, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiSliderOnValueChangedEvent_t330195802, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_value_19() { return static_cast<int32_t>(offsetof(UiSliderOnValueChangedEvent_t330195802, ___value_19)); }
	inline FsmFloat_t2883254149 * get_value_19() const { return ___value_19; }
	inline FsmFloat_t2883254149 ** get_address_of_value_19() { return &___value_19; }
	inline void set_value_19(FsmFloat_t2883254149 * value)
	{
		___value_19 = value;
		Il2CppCodeGenWriteBarrier((&___value_19), value);
	}

	inline static int32_t get_offset_of_slider_20() { return static_cast<int32_t>(offsetof(UiSliderOnValueChangedEvent_t330195802, ___slider_20)); }
	inline Slider_t3903728902 * get_slider_20() const { return ___slider_20; }
	inline Slider_t3903728902 ** get_address_of_slider_20() { return &___slider_20; }
	inline void set_slider_20(Slider_t3903728902 * value)
	{
		___slider_20 = value;
		Il2CppCodeGenWriteBarrier((&___slider_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERONVALUECHANGEDEVENT_T330195802_H
#ifndef UISLIDERSETDIRECTION_T2440866705_H
#define UISLIDERSETDIRECTION_T2440866705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderSetDirection
struct  UiSliderSetDirection_t2440866705  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderSetDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiSliderSetDirection::direction
	FsmEnum_t2861764163 * ___direction_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetDirection::includeRectLayouts
	FsmBool_t163807967 * ___includeRectLayouts_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetDirection::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_19;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderSetDirection::slider
	Slider_t3903728902 * ___slider_20;
	// UnityEngine.UI.Slider/Direction HutongGames.PlayMaker.Actions.UiSliderSetDirection::originalValue
	int32_t ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderSetDirection_t2440866705, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_direction_17() { return static_cast<int32_t>(offsetof(UiSliderSetDirection_t2440866705, ___direction_17)); }
	inline FsmEnum_t2861764163 * get_direction_17() const { return ___direction_17; }
	inline FsmEnum_t2861764163 ** get_address_of_direction_17() { return &___direction_17; }
	inline void set_direction_17(FsmEnum_t2861764163 * value)
	{
		___direction_17 = value;
		Il2CppCodeGenWriteBarrier((&___direction_17), value);
	}

	inline static int32_t get_offset_of_includeRectLayouts_18() { return static_cast<int32_t>(offsetof(UiSliderSetDirection_t2440866705, ___includeRectLayouts_18)); }
	inline FsmBool_t163807967 * get_includeRectLayouts_18() const { return ___includeRectLayouts_18; }
	inline FsmBool_t163807967 ** get_address_of_includeRectLayouts_18() { return &___includeRectLayouts_18; }
	inline void set_includeRectLayouts_18(FsmBool_t163807967 * value)
	{
		___includeRectLayouts_18 = value;
		Il2CppCodeGenWriteBarrier((&___includeRectLayouts_18), value);
	}

	inline static int32_t get_offset_of_resetOnExit_19() { return static_cast<int32_t>(offsetof(UiSliderSetDirection_t2440866705, ___resetOnExit_19)); }
	inline FsmBool_t163807967 * get_resetOnExit_19() const { return ___resetOnExit_19; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_19() { return &___resetOnExit_19; }
	inline void set_resetOnExit_19(FsmBool_t163807967 * value)
	{
		___resetOnExit_19 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_19), value);
	}

	inline static int32_t get_offset_of_slider_20() { return static_cast<int32_t>(offsetof(UiSliderSetDirection_t2440866705, ___slider_20)); }
	inline Slider_t3903728902 * get_slider_20() const { return ___slider_20; }
	inline Slider_t3903728902 ** get_address_of_slider_20() { return &___slider_20; }
	inline void set_slider_20(Slider_t3903728902 * value)
	{
		___slider_20 = value;
		Il2CppCodeGenWriteBarrier((&___slider_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiSliderSetDirection_t2440866705, ___originalValue_21)); }
	inline int32_t get_originalValue_21() const { return ___originalValue_21; }
	inline int32_t* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(int32_t value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERSETDIRECTION_T2440866705_H
#ifndef UISLIDERSETMINMAX_T110212889_H
#define UISLIDERSETMINMAX_T110212889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderSetMinMax
struct  UiSliderSetMinMax_t110212889  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderSetMinMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderSetMinMax::minValue
	FsmFloat_t2883254149 * ___minValue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderSetMinMax::maxValue
	FsmFloat_t2883254149 * ___maxValue_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetMinMax::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderSetMinMax::everyFrame
	bool ___everyFrame_20;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderSetMinMax::slider
	Slider_t3903728902 * ___slider_21;
	// System.Single HutongGames.PlayMaker.Actions.UiSliderSetMinMax::originalMinValue
	float ___originalMinValue_22;
	// System.Single HutongGames.PlayMaker.Actions.UiSliderSetMinMax::originalMaxValue
	float ___originalMaxValue_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_minValue_17() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___minValue_17)); }
	inline FsmFloat_t2883254149 * get_minValue_17() const { return ___minValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_minValue_17() { return &___minValue_17; }
	inline void set_minValue_17(FsmFloat_t2883254149 * value)
	{
		___minValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___minValue_17), value);
	}

	inline static int32_t get_offset_of_maxValue_18() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___maxValue_18)); }
	inline FsmFloat_t2883254149 * get_maxValue_18() const { return ___maxValue_18; }
	inline FsmFloat_t2883254149 ** get_address_of_maxValue_18() { return &___maxValue_18; }
	inline void set_maxValue_18(FsmFloat_t2883254149 * value)
	{
		___maxValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___maxValue_18), value);
	}

	inline static int32_t get_offset_of_resetOnExit_19() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___resetOnExit_19)); }
	inline FsmBool_t163807967 * get_resetOnExit_19() const { return ___resetOnExit_19; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_19() { return &___resetOnExit_19; }
	inline void set_resetOnExit_19(FsmBool_t163807967 * value)
	{
		___resetOnExit_19 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of_slider_21() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___slider_21)); }
	inline Slider_t3903728902 * get_slider_21() const { return ___slider_21; }
	inline Slider_t3903728902 ** get_address_of_slider_21() { return &___slider_21; }
	inline void set_slider_21(Slider_t3903728902 * value)
	{
		___slider_21 = value;
		Il2CppCodeGenWriteBarrier((&___slider_21), value);
	}

	inline static int32_t get_offset_of_originalMinValue_22() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___originalMinValue_22)); }
	inline float get_originalMinValue_22() const { return ___originalMinValue_22; }
	inline float* get_address_of_originalMinValue_22() { return &___originalMinValue_22; }
	inline void set_originalMinValue_22(float value)
	{
		___originalMinValue_22 = value;
	}

	inline static int32_t get_offset_of_originalMaxValue_23() { return static_cast<int32_t>(offsetof(UiSliderSetMinMax_t110212889, ___originalMaxValue_23)); }
	inline float get_originalMaxValue_23() const { return ___originalMaxValue_23; }
	inline float* get_address_of_originalMaxValue_23() { return &___originalMaxValue_23; }
	inline void set_originalMaxValue_23(float value)
	{
		___originalMaxValue_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERSETMINMAX_T110212889_H
#ifndef UISLIDERSETNORMALIZEDVALUE_T2928092588_H
#define UISLIDERSETNORMALIZEDVALUE_T2928092588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue
struct  UiSliderSetNormalizedValue_t2928092588  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue::value
	FsmFloat_t2883254149 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue::slider
	Slider_t3903728902 * ___slider_20;
	// System.Single HutongGames.PlayMaker.Actions.UiSliderSetNormalizedValue::originalValue
	float ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderSetNormalizedValue_t2928092588, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiSliderSetNormalizedValue_t2928092588, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiSliderSetNormalizedValue_t2928092588, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiSliderSetNormalizedValue_t2928092588, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_slider_20() { return static_cast<int32_t>(offsetof(UiSliderSetNormalizedValue_t2928092588, ___slider_20)); }
	inline Slider_t3903728902 * get_slider_20() const { return ___slider_20; }
	inline Slider_t3903728902 ** get_address_of_slider_20() { return &___slider_20; }
	inline void set_slider_20(Slider_t3903728902 * value)
	{
		___slider_20 = value;
		Il2CppCodeGenWriteBarrier((&___slider_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiSliderSetNormalizedValue_t2928092588, ___originalValue_21)); }
	inline float get_originalValue_21() const { return ___originalValue_21; }
	inline float* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(float value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERSETNORMALIZEDVALUE_T2928092588_H
#ifndef UISLIDERSETVALUE_T2827177487_H
#define UISLIDERSETVALUE_T2827177487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderSetValue
struct  UiSliderSetValue_t2827177487  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderSetValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSliderSetValue::value
	FsmFloat_t2883254149 * ___value_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetValue::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderSetValue::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderSetValue::slider
	Slider_t3903728902 * ___slider_20;
	// System.Single HutongGames.PlayMaker.Actions.UiSliderSetValue::originalValue
	float ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderSetValue_t2827177487, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiSliderSetValue_t2827177487, ___value_17)); }
	inline FsmFloat_t2883254149 * get_value_17() const { return ___value_17; }
	inline FsmFloat_t2883254149 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmFloat_t2883254149 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiSliderSetValue_t2827177487, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiSliderSetValue_t2827177487, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_slider_20() { return static_cast<int32_t>(offsetof(UiSliderSetValue_t2827177487, ___slider_20)); }
	inline Slider_t3903728902 * get_slider_20() const { return ___slider_20; }
	inline Slider_t3903728902 ** get_address_of_slider_20() { return &___slider_20; }
	inline void set_slider_20(Slider_t3903728902 * value)
	{
		___slider_20 = value;
		Il2CppCodeGenWriteBarrier((&___slider_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiSliderSetValue_t2827177487, ___originalValue_21)); }
	inline float get_originalValue_21() const { return ___originalValue_21; }
	inline float* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(float value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERSETVALUE_T2827177487_H
#ifndef UISLIDERSETWHOLENUMBERS_T2461207593_H
#define UISLIDERSETWHOLENUMBERS_T2461207593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSliderSetWholeNumbers
struct  UiSliderSetWholeNumbers_t2461207593  : public ComponentAction_1_t2486048828
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSliderSetWholeNumbers::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetWholeNumbers::wholeNumbers
	FsmBool_t163807967 * ___wholeNumbers_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSliderSetWholeNumbers::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.UiSliderSetWholeNumbers::slider
	Slider_t3903728902 * ___slider_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSliderSetWholeNumbers::originalValue
	bool ___originalValue_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSliderSetWholeNumbers_t2461207593, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_wholeNumbers_17() { return static_cast<int32_t>(offsetof(UiSliderSetWholeNumbers_t2461207593, ___wholeNumbers_17)); }
	inline FsmBool_t163807967 * get_wholeNumbers_17() const { return ___wholeNumbers_17; }
	inline FsmBool_t163807967 ** get_address_of_wholeNumbers_17() { return &___wholeNumbers_17; }
	inline void set_wholeNumbers_17(FsmBool_t163807967 * value)
	{
		___wholeNumbers_17 = value;
		Il2CppCodeGenWriteBarrier((&___wholeNumbers_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiSliderSetWholeNumbers_t2461207593, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_slider_19() { return static_cast<int32_t>(offsetof(UiSliderSetWholeNumbers_t2461207593, ___slider_19)); }
	inline Slider_t3903728902 * get_slider_19() const { return ___slider_19; }
	inline Slider_t3903728902 ** get_address_of_slider_19() { return &___slider_19; }
	inline void set_slider_19(Slider_t3903728902 * value)
	{
		___slider_19 = value;
		Il2CppCodeGenWriteBarrier((&___slider_19), value);
	}

	inline static int32_t get_offset_of_originalValue_20() { return static_cast<int32_t>(offsetof(UiSliderSetWholeNumbers_t2461207593, ___originalValue_20)); }
	inline bool get_originalValue_20() const { return ___originalValue_20; }
	inline bool* get_address_of_originalValue_20() { return &___originalValue_20; }
	inline void set_originalValue_20(bool value)
	{
		___originalValue_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISLIDERSETWHOLENUMBERS_T2461207593_H
#ifndef UITEXTGETTEXT_T1294294173_H
#define UITEXTGETTEXT_T1294294173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiTextGetText
struct  UiTextGetText_t1294294173  : public ComponentAction_1_t484202640
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiTextGetText::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiTextGetText::text
	FsmString_t1785915204 * ___text_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiTextGetText::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Text HutongGames.PlayMaker.Actions.UiTextGetText::uiText
	Text_t1901882714 * ___uiText_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiTextGetText_t1294294173, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(UiTextGetText_t1294294173, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiTextGetText_t1294294173, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_uiText_19() { return static_cast<int32_t>(offsetof(UiTextGetText_t1294294173, ___uiText_19)); }
	inline Text_t1901882714 * get_uiText_19() const { return ___uiText_19; }
	inline Text_t1901882714 ** get_address_of_uiText_19() { return &___uiText_19; }
	inline void set_uiText_19(Text_t1901882714 * value)
	{
		___uiText_19 = value;
		Il2CppCodeGenWriteBarrier((&___uiText_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITEXTGETTEXT_T1294294173_H
#ifndef UITEXTSETTEXT_T1294294577_H
#define UITEXTSETTEXT_T1294294577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiTextSetText
struct  UiTextSetText_t1294294577  : public ComponentAction_1_t484202640
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiTextSetText::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiTextSetText::text
	FsmString_t1785915204 * ___text_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiTextSetText::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiTextSetText::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Text HutongGames.PlayMaker.Actions.UiTextSetText::uiText
	Text_t1901882714 * ___uiText_20;
	// System.String HutongGames.PlayMaker.Actions.UiTextSetText::originalString
	String_t* ___originalString_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiTextSetText_t1294294577, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(UiTextSetText_t1294294577, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiTextSetText_t1294294577, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiTextSetText_t1294294577, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_uiText_20() { return static_cast<int32_t>(offsetof(UiTextSetText_t1294294577, ___uiText_20)); }
	inline Text_t1901882714 * get_uiText_20() const { return ___uiText_20; }
	inline Text_t1901882714 ** get_address_of_uiText_20() { return &___uiText_20; }
	inline void set_uiText_20(Text_t1901882714 * value)
	{
		___uiText_20 = value;
		Il2CppCodeGenWriteBarrier((&___uiText_20), value);
	}

	inline static int32_t get_offset_of_originalString_21() { return static_cast<int32_t>(offsetof(UiTextSetText_t1294294577, ___originalString_21)); }
	inline String_t* get_originalString_21() const { return ___originalString_21; }
	inline String_t** get_address_of_originalString_21() { return &___originalString_21; }
	inline void set_originalString_21(String_t* value)
	{
		___originalString_21 = value;
		Il2CppCodeGenWriteBarrier((&___originalString_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITEXTSETTEXT_T1294294577_H
#ifndef UITOGGLEGETISON_T3709473285_H
#define UITOGGLEGETISON_T3709473285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiToggleGetIsOn
struct  UiToggleGetIsOn_t3709473285  : public ComponentAction_1_t1317696987
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiToggleGetIsOn::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiToggleGetIsOn::value
	FsmBool_t163807967 * ___value_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiToggleGetIsOn::isOnEvent
	FsmEvent_t3736299882 * ___isOnEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiToggleGetIsOn::isOffEvent
	FsmEvent_t3736299882 * ___isOffEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiToggleGetIsOn::everyFrame
	bool ___everyFrame_20;
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.Actions.UiToggleGetIsOn::_toggle
	Toggle_t2735377061 * ____toggle_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiToggleGetIsOn_t3709473285, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiToggleGetIsOn_t3709473285, ___value_17)); }
	inline FsmBool_t163807967 * get_value_17() const { return ___value_17; }
	inline FsmBool_t163807967 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmBool_t163807967 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_isOnEvent_18() { return static_cast<int32_t>(offsetof(UiToggleGetIsOn_t3709473285, ___isOnEvent_18)); }
	inline FsmEvent_t3736299882 * get_isOnEvent_18() const { return ___isOnEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_isOnEvent_18() { return &___isOnEvent_18; }
	inline void set_isOnEvent_18(FsmEvent_t3736299882 * value)
	{
		___isOnEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___isOnEvent_18), value);
	}

	inline static int32_t get_offset_of_isOffEvent_19() { return static_cast<int32_t>(offsetof(UiToggleGetIsOn_t3709473285, ___isOffEvent_19)); }
	inline FsmEvent_t3736299882 * get_isOffEvent_19() const { return ___isOffEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isOffEvent_19() { return &___isOffEvent_19; }
	inline void set_isOffEvent_19(FsmEvent_t3736299882 * value)
	{
		___isOffEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isOffEvent_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(UiToggleGetIsOn_t3709473285, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of__toggle_21() { return static_cast<int32_t>(offsetof(UiToggleGetIsOn_t3709473285, ____toggle_21)); }
	inline Toggle_t2735377061 * get__toggle_21() const { return ____toggle_21; }
	inline Toggle_t2735377061 ** get_address_of__toggle_21() { return &____toggle_21; }
	inline void set__toggle_21(Toggle_t2735377061 * value)
	{
		____toggle_21 = value;
		Il2CppCodeGenWriteBarrier((&____toggle_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEGETISON_T3709473285_H
#ifndef UITOGGLEONVALUECHANGEDEVENT_T2905849127_H
#define UITOGGLEONVALUECHANGEDEVENT_T2905849127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiToggleOnValueChangedEvent
struct  UiToggleOnValueChangedEvent_t2905849127  : public ComponentAction_1_t1317696987
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiToggleOnValueChangedEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiToggleOnValueChangedEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiToggleOnValueChangedEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiToggleOnValueChangedEvent::value
	FsmBool_t163807967 * ___value_19;
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.Actions.UiToggleOnValueChangedEvent::toggle
	Toggle_t2735377061 * ___toggle_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiToggleOnValueChangedEvent_t2905849127, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiToggleOnValueChangedEvent_t2905849127, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiToggleOnValueChangedEvent_t2905849127, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_value_19() { return static_cast<int32_t>(offsetof(UiToggleOnValueChangedEvent_t2905849127, ___value_19)); }
	inline FsmBool_t163807967 * get_value_19() const { return ___value_19; }
	inline FsmBool_t163807967 ** get_address_of_value_19() { return &___value_19; }
	inline void set_value_19(FsmBool_t163807967 * value)
	{
		___value_19 = value;
		Il2CppCodeGenWriteBarrier((&___value_19), value);
	}

	inline static int32_t get_offset_of_toggle_20() { return static_cast<int32_t>(offsetof(UiToggleOnValueChangedEvent_t2905849127, ___toggle_20)); }
	inline Toggle_t2735377061 * get_toggle_20() const { return ___toggle_20; }
	inline Toggle_t2735377061 ** get_address_of_toggle_20() { return &___toggle_20; }
	inline void set_toggle_20(Toggle_t2735377061 * value)
	{
		___toggle_20 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLEONVALUECHANGEDEVENT_T2905849127_H
#ifndef UITOGGLESETISON_T2270738753_H
#define UITOGGLESETISON_T2270738753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiToggleSetIsOn
struct  UiToggleSetIsOn_t2270738753  : public ComponentAction_1_t1317696987
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiToggleSetIsOn::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiToggleSetIsOn::isOn
	FsmBool_t163807967 * ___isOn_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiToggleSetIsOn::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.Actions.UiToggleSetIsOn::_toggle
	Toggle_t2735377061 * ____toggle_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiToggleSetIsOn::_originalValue
	bool ____originalValue_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiToggleSetIsOn_t2270738753, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_isOn_17() { return static_cast<int32_t>(offsetof(UiToggleSetIsOn_t2270738753, ___isOn_17)); }
	inline FsmBool_t163807967 * get_isOn_17() const { return ___isOn_17; }
	inline FsmBool_t163807967 ** get_address_of_isOn_17() { return &___isOn_17; }
	inline void set_isOn_17(FsmBool_t163807967 * value)
	{
		___isOn_17 = value;
		Il2CppCodeGenWriteBarrier((&___isOn_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiToggleSetIsOn_t2270738753, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of__toggle_19() { return static_cast<int32_t>(offsetof(UiToggleSetIsOn_t2270738753, ____toggle_19)); }
	inline Toggle_t2735377061 * get__toggle_19() const { return ____toggle_19; }
	inline Toggle_t2735377061 ** get_address_of__toggle_19() { return &____toggle_19; }
	inline void set__toggle_19(Toggle_t2735377061 * value)
	{
		____toggle_19 = value;
		Il2CppCodeGenWriteBarrier((&____toggle_19), value);
	}

	inline static int32_t get_offset_of__originalValue_20() { return static_cast<int32_t>(offsetof(UiToggleSetIsOn_t2270738753, ____originalValue_20)); }
	inline bool get__originalValue_20() const { return ____originalValue_20; }
	inline bool* get_address_of__originalValue_20() { return &____originalValue_20; }
	inline void set__originalValue_20(bool value)
	{
		____originalValue_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLESETISON_T2270738753_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;
	// UnityEngine.GameObject DefaultTrackableEventHandler::trackingEventCatcher
	GameObject_t1113636619 * ___trackingEventCatcher_5;
	// UnityEngine.Texture2D DefaultTrackableEventHandler::markerTexture
	Texture2D_t3840446185 * ___markerTexture_6;
	// UnityEngine.MeshRenderer DefaultTrackableEventHandler::explodeObjA
	MeshRenderer_t587009260 * ___explodeObjA_7;
	// UnityEngine.MeshRenderer DefaultTrackableEventHandler::explodeObjB
	MeshRenderer_t587009260 * ___explodeObjB_8;
	// UnityEngine.MeshRenderer DefaultTrackableEventHandler::explodeObjC
	MeshRenderer_t587009260 * ___explodeObjC_9;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_trackingEventCatcher_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___trackingEventCatcher_5)); }
	inline GameObject_t1113636619 * get_trackingEventCatcher_5() const { return ___trackingEventCatcher_5; }
	inline GameObject_t1113636619 ** get_address_of_trackingEventCatcher_5() { return &___trackingEventCatcher_5; }
	inline void set_trackingEventCatcher_5(GameObject_t1113636619 * value)
	{
		___trackingEventCatcher_5 = value;
		Il2CppCodeGenWriteBarrier((&___trackingEventCatcher_5), value);
	}

	inline static int32_t get_offset_of_markerTexture_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___markerTexture_6)); }
	inline Texture2D_t3840446185 * get_markerTexture_6() const { return ___markerTexture_6; }
	inline Texture2D_t3840446185 ** get_address_of_markerTexture_6() { return &___markerTexture_6; }
	inline void set_markerTexture_6(Texture2D_t3840446185 * value)
	{
		___markerTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___markerTexture_6), value);
	}

	inline static int32_t get_offset_of_explodeObjA_7() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___explodeObjA_7)); }
	inline MeshRenderer_t587009260 * get_explodeObjA_7() const { return ___explodeObjA_7; }
	inline MeshRenderer_t587009260 ** get_address_of_explodeObjA_7() { return &___explodeObjA_7; }
	inline void set_explodeObjA_7(MeshRenderer_t587009260 * value)
	{
		___explodeObjA_7 = value;
		Il2CppCodeGenWriteBarrier((&___explodeObjA_7), value);
	}

	inline static int32_t get_offset_of_explodeObjB_8() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___explodeObjB_8)); }
	inline MeshRenderer_t587009260 * get_explodeObjB_8() const { return ___explodeObjB_8; }
	inline MeshRenderer_t587009260 ** get_address_of_explodeObjB_8() { return &___explodeObjB_8; }
	inline void set_explodeObjB_8(MeshRenderer_t587009260 * value)
	{
		___explodeObjB_8 = value;
		Il2CppCodeGenWriteBarrier((&___explodeObjB_8), value);
	}

	inline static int32_t get_offset_of_explodeObjC_9() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___explodeObjC_9)); }
	inline MeshRenderer_t587009260 * get_explodeObjC_9() const { return ___explodeObjC_9; }
	inline MeshRenderer_t587009260 ** get_address_of_explodeObjC_9() { return &___explodeObjC_9; }
	inline void set_explodeObjC_9(MeshRenderer_t587009260 * value)
	{
		___explodeObjC_9 = value;
		Il2CppCodeGenWriteBarrier((&___explodeObjC_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_7)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_8)); }
	inline GUIStyle_t3956901511 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t3956901511 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_9)); }
	inline GUIStyle_t3956901511 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t3956901511 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_10)); }
	inline Texture2D_t3840446185 * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_t3840446185 * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_11)); }
	inline Texture2D_t3840446185 * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_t3840446185 * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_12)); }
	inline Texture2D_t3840446185 * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_t3840446185 * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (UiInputFieldGetTextAsFloat_t3905871643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3800[9] = 
{
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_gameObject_16(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_value_17(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_isFloat_18(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_isFloatEvent_19(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_isNotFloatEvent_20(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_everyFrame_21(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of_inputField_22(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of__value_23(),
	UiInputFieldGetTextAsFloat_t3905871643::get_offset_of__success_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (UiInputFieldGetTextAsInt_t2882175745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3801[9] = 
{
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_gameObject_16(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_value_17(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_isInt_18(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_isIntEvent_19(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_isNotIntEvent_20(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_everyFrame_21(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of_inputField_22(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of__value_23(),
	UiInputFieldGetTextAsInt_t2882175745::get_offset_of__success_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (UiInputFieldGetWasCanceled_t3143818148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3802[5] = 
{
	UiInputFieldGetWasCanceled_t3143818148::get_offset_of_gameObject_16(),
	UiInputFieldGetWasCanceled_t3143818148::get_offset_of_wasCanceled_17(),
	UiInputFieldGetWasCanceled_t3143818148::get_offset_of_wasCanceledEvent_18(),
	UiInputFieldGetWasCanceled_t3143818148::get_offset_of_wasNotCanceledEvent_19(),
	UiInputFieldGetWasCanceled_t3143818148::get_offset_of_inputField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (UiInputFieldMoveCaretToTextEnd_t2789998028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3803[3] = 
{
	UiInputFieldMoveCaretToTextEnd_t2789998028::get_offset_of_gameObject_16(),
	UiInputFieldMoveCaretToTextEnd_t2789998028::get_offset_of_shift_17(),
	UiInputFieldMoveCaretToTextEnd_t2789998028::get_offset_of_inputField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (UiInputFieldMoveCaretToTextStart_t554352859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3804[3] = 
{
	UiInputFieldMoveCaretToTextStart_t554352859::get_offset_of_gameObject_16(),
	UiInputFieldMoveCaretToTextStart_t554352859::get_offset_of_shift_17(),
	UiInputFieldMoveCaretToTextStart_t554352859::get_offset_of_inputField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (UiInputFieldOnEndEditEvent_t3952452798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3805[6] = 
{
	UiInputFieldOnEndEditEvent_t3952452798::get_offset_of_gameObject_16(),
	UiInputFieldOnEndEditEvent_t3952452798::get_offset_of_eventTarget_17(),
	UiInputFieldOnEndEditEvent_t3952452798::get_offset_of_sendEvent_18(),
	UiInputFieldOnEndEditEvent_t3952452798::get_offset_of_text_19(),
	UiInputFieldOnEndEditEvent_t3952452798::get_offset_of_wasCanceled_20(),
	UiInputFieldOnEndEditEvent_t3952452798::get_offset_of_inputField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (UiInputFieldOnSubmitEvent_t9451400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3806[5] = 
{
	UiInputFieldOnSubmitEvent_t9451400::get_offset_of_gameObject_16(),
	UiInputFieldOnSubmitEvent_t9451400::get_offset_of_eventTarget_17(),
	UiInputFieldOnSubmitEvent_t9451400::get_offset_of_sendEvent_18(),
	UiInputFieldOnSubmitEvent_t9451400::get_offset_of_text_19(),
	UiInputFieldOnSubmitEvent_t9451400::get_offset_of_inputField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (UiInputFieldOnValueChangeEvent_t2570439459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3807[5] = 
{
	UiInputFieldOnValueChangeEvent_t2570439459::get_offset_of_gameObject_16(),
	UiInputFieldOnValueChangeEvent_t2570439459::get_offset_of_eventTarget_17(),
	UiInputFieldOnValueChangeEvent_t2570439459::get_offset_of_sendEvent_18(),
	UiInputFieldOnValueChangeEvent_t2570439459::get_offset_of_text_19(),
	UiInputFieldOnValueChangeEvent_t2570439459::get_offset_of_inputField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (UiInputFieldSetAsterixChar_t3822249831), -1, sizeof(UiInputFieldSetAsterixChar_t3822249831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3808[6] = 
{
	UiInputFieldSetAsterixChar_t3822249831::get_offset_of_gameObject_16(),
	UiInputFieldSetAsterixChar_t3822249831::get_offset_of_asterixChar_17(),
	UiInputFieldSetAsterixChar_t3822249831::get_offset_of_resetOnExit_18(),
	UiInputFieldSetAsterixChar_t3822249831::get_offset_of_inputField_19(),
	UiInputFieldSetAsterixChar_t3822249831::get_offset_of_originalValue_20(),
	UiInputFieldSetAsterixChar_t3822249831_StaticFields::get_offset_of___char___21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (UiInputFieldSetCaretBlinkRate_t1782366990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3809[6] = 
{
	UiInputFieldSetCaretBlinkRate_t1782366990::get_offset_of_gameObject_16(),
	UiInputFieldSetCaretBlinkRate_t1782366990::get_offset_of_caretBlinkRate_17(),
	UiInputFieldSetCaretBlinkRate_t1782366990::get_offset_of_resetOnExit_18(),
	UiInputFieldSetCaretBlinkRate_t1782366990::get_offset_of_everyFrame_19(),
	UiInputFieldSetCaretBlinkRate_t1782366990::get_offset_of_inputField_20(),
	UiInputFieldSetCaretBlinkRate_t1782366990::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (UiInputFieldSetCharacterLimit_t3722784285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3810[6] = 
{
	UiInputFieldSetCharacterLimit_t3722784285::get_offset_of_gameObject_16(),
	UiInputFieldSetCharacterLimit_t3722784285::get_offset_of_characterLimit_17(),
	UiInputFieldSetCharacterLimit_t3722784285::get_offset_of_resetOnExit_18(),
	UiInputFieldSetCharacterLimit_t3722784285::get_offset_of_everyFrame_19(),
	UiInputFieldSetCharacterLimit_t3722784285::get_offset_of_inputField_20(),
	UiInputFieldSetCharacterLimit_t3722784285::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (UiInputFieldSetHideMobileInput_t1921728854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3811[5] = 
{
	UiInputFieldSetHideMobileInput_t1921728854::get_offset_of_gameObject_16(),
	UiInputFieldSetHideMobileInput_t1921728854::get_offset_of_hideMobileInput_17(),
	UiInputFieldSetHideMobileInput_t1921728854::get_offset_of_resetOnExit_18(),
	UiInputFieldSetHideMobileInput_t1921728854::get_offset_of_inputField_19(),
	UiInputFieldSetHideMobileInput_t1921728854::get_offset_of_originalValue_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (UiInputFieldSetPlaceHolder_t3526529484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3812[5] = 
{
	UiInputFieldSetPlaceHolder_t3526529484::get_offset_of_gameObject_16(),
	UiInputFieldSetPlaceHolder_t3526529484::get_offset_of_placeholder_17(),
	UiInputFieldSetPlaceHolder_t3526529484::get_offset_of_resetOnExit_18(),
	UiInputFieldSetPlaceHolder_t3526529484::get_offset_of_inputField_19(),
	UiInputFieldSetPlaceHolder_t3526529484::get_offset_of_originalValue_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (UiInputFieldSetSelectionColor_t3194565660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3813[6] = 
{
	UiInputFieldSetSelectionColor_t3194565660::get_offset_of_gameObject_16(),
	UiInputFieldSetSelectionColor_t3194565660::get_offset_of_selectionColor_17(),
	UiInputFieldSetSelectionColor_t3194565660::get_offset_of_resetOnExit_18(),
	UiInputFieldSetSelectionColor_t3194565660::get_offset_of_everyFrame_19(),
	UiInputFieldSetSelectionColor_t3194565660::get_offset_of_inputField_20(),
	UiInputFieldSetSelectionColor_t3194565660::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (UiInputFieldSetText_t3586869520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3814[6] = 
{
	UiInputFieldSetText_t3586869520::get_offset_of_gameObject_16(),
	UiInputFieldSetText_t3586869520::get_offset_of_text_17(),
	UiInputFieldSetText_t3586869520::get_offset_of_resetOnExit_18(),
	UiInputFieldSetText_t3586869520::get_offset_of_everyFrame_19(),
	UiInputFieldSetText_t3586869520::get_offset_of_inputField_20(),
	UiInputFieldSetText_t3586869520::get_offset_of_originalString_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (UiRawImageSetTexture_t1884493090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3815[5] = 
{
	UiRawImageSetTexture_t1884493090::get_offset_of_gameObject_16(),
	UiRawImageSetTexture_t1884493090::get_offset_of_texture_17(),
	UiRawImageSetTexture_t1884493090::get_offset_of_resetOnExit_18(),
	UiRawImageSetTexture_t1884493090::get_offset_of__texture_19(),
	UiRawImageSetTexture_t1884493090::get_offset_of__originalTexture_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (UiRebuild_t1491182680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3816[4] = 
{
	UiRebuild_t1491182680::get_offset_of_gameObject_16(),
	UiRebuild_t1491182680::get_offset_of_canvasUpdate_17(),
	UiRebuild_t1491182680::get_offset_of_rebuildOnExit_18(),
	UiRebuild_t1491182680::get_offset_of_graphic_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (UiScrollbarGetDirection_t1527513757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3817[4] = 
{
	UiScrollbarGetDirection_t1527513757::get_offset_of_gameObject_16(),
	UiScrollbarGetDirection_t1527513757::get_offset_of_direction_17(),
	UiScrollbarGetDirection_t1527513757::get_offset_of_everyFrame_18(),
	UiScrollbarGetDirection_t1527513757::get_offset_of_scrollbar_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (UiScrollbarGetValue_t2957191981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3818[4] = 
{
	UiScrollbarGetValue_t2957191981::get_offset_of_gameObject_16(),
	UiScrollbarGetValue_t2957191981::get_offset_of_value_17(),
	UiScrollbarGetValue_t2957191981::get_offset_of_everyFrame_18(),
	UiScrollbarGetValue_t2957191981::get_offset_of_scrollbar_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (UiScrollbarOnValueChanged_t2078301051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[5] = 
{
	UiScrollbarOnValueChanged_t2078301051::get_offset_of_gameObject_16(),
	UiScrollbarOnValueChanged_t2078301051::get_offset_of_eventTarget_17(),
	UiScrollbarOnValueChanged_t2078301051::get_offset_of_sendEvent_18(),
	UiScrollbarOnValueChanged_t2078301051::get_offset_of_value_19(),
	UiScrollbarOnValueChanged_t2078301051::get_offset_of_scrollbar_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (UiScrollbarSetDirection_t2376598163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3820[6] = 
{
	UiScrollbarSetDirection_t2376598163::get_offset_of_gameObject_16(),
	UiScrollbarSetDirection_t2376598163::get_offset_of_direction_17(),
	UiScrollbarSetDirection_t2376598163::get_offset_of_includeRectLayouts_18(),
	UiScrollbarSetDirection_t2376598163::get_offset_of_resetOnExit_19(),
	UiScrollbarSetDirection_t2376598163::get_offset_of_scrollbar_20(),
	UiScrollbarSetDirection_t2376598163::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (UiScrollbarSetNumberOfSteps_t1614973516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3821[6] = 
{
	UiScrollbarSetNumberOfSteps_t1614973516::get_offset_of_gameObject_16(),
	UiScrollbarSetNumberOfSteps_t1614973516::get_offset_of_value_17(),
	UiScrollbarSetNumberOfSteps_t1614973516::get_offset_of_resetOnExit_18(),
	UiScrollbarSetNumberOfSteps_t1614973516::get_offset_of_everyFrame_19(),
	UiScrollbarSetNumberOfSteps_t1614973516::get_offset_of_scrollbar_20(),
	UiScrollbarSetNumberOfSteps_t1614973516::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (UiScrollbarSetSize_t1003663313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3822[6] = 
{
	UiScrollbarSetSize_t1003663313::get_offset_of_gameObject_16(),
	UiScrollbarSetSize_t1003663313::get_offset_of_value_17(),
	UiScrollbarSetSize_t1003663313::get_offset_of_resetOnExit_18(),
	UiScrollbarSetSize_t1003663313::get_offset_of_everyFrame_19(),
	UiScrollbarSetSize_t1003663313::get_offset_of_scrollbar_20(),
	UiScrollbarSetSize_t1003663313::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (UiScrollbarSetValue_t4246154029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3823[6] = 
{
	UiScrollbarSetValue_t4246154029::get_offset_of_gameObject_16(),
	UiScrollbarSetValue_t4246154029::get_offset_of_value_17(),
	UiScrollbarSetValue_t4246154029::get_offset_of_resetOnExit_18(),
	UiScrollbarSetValue_t4246154029::get_offset_of_everyFrame_19(),
	UiScrollbarSetValue_t4246154029::get_offset_of_scrollbar_20(),
	UiScrollbarSetValue_t4246154029::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (UiScrollRectSetHorizontal_t2924575077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[6] = 
{
	UiScrollRectSetHorizontal_t2924575077::get_offset_of_gameObject_16(),
	UiScrollRectSetHorizontal_t2924575077::get_offset_of_horizontal_17(),
	UiScrollRectSetHorizontal_t2924575077::get_offset_of_resetOnExit_18(),
	UiScrollRectSetHorizontal_t2924575077::get_offset_of_everyFrame_19(),
	UiScrollRectSetHorizontal_t2924575077::get_offset_of_scrollRect_20(),
	UiScrollRectSetHorizontal_t2924575077::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (UiScrollRectSetNormalizedPosition_t833414855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3825[8] = 
{
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_gameObject_16(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_normalizedPosition_17(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_horizontalPosition_18(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_verticalPosition_19(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_resetOnExit_20(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_everyFrame_21(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_scrollRect_22(),
	UiScrollRectSetNormalizedPosition_t833414855::get_offset_of_originalValue_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (UiScrollRectSetVertical_t893328469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3826[6] = 
{
	UiScrollRectSetVertical_t893328469::get_offset_of_gameObject_16(),
	UiScrollRectSetVertical_t893328469::get_offset_of_vertical_17(),
	UiScrollRectSetVertical_t893328469::get_offset_of_resetOnExit_18(),
	UiScrollRectSetVertical_t893328469::get_offset_of_everyFrame_19(),
	UiScrollRectSetVertical_t893328469::get_offset_of_scrollRect_20(),
	UiScrollRectSetVertical_t893328469::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (UiSliderGetDirection_t3868044733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3827[4] = 
{
	UiSliderGetDirection_t3868044733::get_offset_of_gameObject_16(),
	UiSliderGetDirection_t3868044733::get_offset_of_direction_17(),
	UiSliderGetDirection_t3868044733::get_offset_of_everyFrame_18(),
	UiSliderGetDirection_t3868044733::get_offset_of_slider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (UiSliderGetMinMax_t1181160557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[4] = 
{
	UiSliderGetMinMax_t1181160557::get_offset_of_gameObject_16(),
	UiSliderGetMinMax_t1181160557::get_offset_of_minValue_17(),
	UiSliderGetMinMax_t1181160557::get_offset_of_maxValue_18(),
	UiSliderGetMinMax_t1181160557::get_offset_of_slider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (UiSliderGetNormalizedValue_t613760320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[4] = 
{
	UiSliderGetNormalizedValue_t613760320::get_offset_of_gameObject_16(),
	UiSliderGetNormalizedValue_t613760320::get_offset_of_value_17(),
	UiSliderGetNormalizedValue_t613760320::get_offset_of_everyFrame_18(),
	UiSliderGetNormalizedValue_t613760320::get_offset_of_slider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (UiSliderGetValue_t2578351891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[4] = 
{
	UiSliderGetValue_t2578351891::get_offset_of_gameObject_16(),
	UiSliderGetValue_t2578351891::get_offset_of_value_17(),
	UiSliderGetValue_t2578351891::get_offset_of_everyFrame_18(),
	UiSliderGetValue_t2578351891::get_offset_of_slider_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (UiSliderGetWholeNumbers_t3953068173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3831[5] = 
{
	UiSliderGetWholeNumbers_t3953068173::get_offset_of_gameObject_16(),
	UiSliderGetWholeNumbers_t3953068173::get_offset_of_wholeNumbers_17(),
	UiSliderGetWholeNumbers_t3953068173::get_offset_of_isShowingWholeNumbersEvent_18(),
	UiSliderGetWholeNumbers_t3953068173::get_offset_of_isNotShowingWholeNumbersEvent_19(),
	UiSliderGetWholeNumbers_t3953068173::get_offset_of_slider_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (UiSliderOnValueChangedEvent_t330195802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3832[5] = 
{
	UiSliderOnValueChangedEvent_t330195802::get_offset_of_gameObject_16(),
	UiSliderOnValueChangedEvent_t330195802::get_offset_of_eventTarget_17(),
	UiSliderOnValueChangedEvent_t330195802::get_offset_of_sendEvent_18(),
	UiSliderOnValueChangedEvent_t330195802::get_offset_of_value_19(),
	UiSliderOnValueChangedEvent_t330195802::get_offset_of_slider_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (UiSliderSetDirection_t2440866705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3833[6] = 
{
	UiSliderSetDirection_t2440866705::get_offset_of_gameObject_16(),
	UiSliderSetDirection_t2440866705::get_offset_of_direction_17(),
	UiSliderSetDirection_t2440866705::get_offset_of_includeRectLayouts_18(),
	UiSliderSetDirection_t2440866705::get_offset_of_resetOnExit_19(),
	UiSliderSetDirection_t2440866705::get_offset_of_slider_20(),
	UiSliderSetDirection_t2440866705::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (UiSliderSetMinMax_t110212889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3834[8] = 
{
	UiSliderSetMinMax_t110212889::get_offset_of_gameObject_16(),
	UiSliderSetMinMax_t110212889::get_offset_of_minValue_17(),
	UiSliderSetMinMax_t110212889::get_offset_of_maxValue_18(),
	UiSliderSetMinMax_t110212889::get_offset_of_resetOnExit_19(),
	UiSliderSetMinMax_t110212889::get_offset_of_everyFrame_20(),
	UiSliderSetMinMax_t110212889::get_offset_of_slider_21(),
	UiSliderSetMinMax_t110212889::get_offset_of_originalMinValue_22(),
	UiSliderSetMinMax_t110212889::get_offset_of_originalMaxValue_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (UiSliderSetNormalizedValue_t2928092588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3835[6] = 
{
	UiSliderSetNormalizedValue_t2928092588::get_offset_of_gameObject_16(),
	UiSliderSetNormalizedValue_t2928092588::get_offset_of_value_17(),
	UiSliderSetNormalizedValue_t2928092588::get_offset_of_resetOnExit_18(),
	UiSliderSetNormalizedValue_t2928092588::get_offset_of_everyFrame_19(),
	UiSliderSetNormalizedValue_t2928092588::get_offset_of_slider_20(),
	UiSliderSetNormalizedValue_t2928092588::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (UiSliderSetValue_t2827177487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3836[6] = 
{
	UiSliderSetValue_t2827177487::get_offset_of_gameObject_16(),
	UiSliderSetValue_t2827177487::get_offset_of_value_17(),
	UiSliderSetValue_t2827177487::get_offset_of_resetOnExit_18(),
	UiSliderSetValue_t2827177487::get_offset_of_everyFrame_19(),
	UiSliderSetValue_t2827177487::get_offset_of_slider_20(),
	UiSliderSetValue_t2827177487::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (UiSliderSetWholeNumbers_t2461207593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3837[5] = 
{
	UiSliderSetWholeNumbers_t2461207593::get_offset_of_gameObject_16(),
	UiSliderSetWholeNumbers_t2461207593::get_offset_of_wholeNumbers_17(),
	UiSliderSetWholeNumbers_t2461207593::get_offset_of_resetOnExit_18(),
	UiSliderSetWholeNumbers_t2461207593::get_offset_of_slider_19(),
	UiSliderSetWholeNumbers_t2461207593::get_offset_of_originalValue_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (UiTextGetText_t1294294173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3838[4] = 
{
	UiTextGetText_t1294294173::get_offset_of_gameObject_16(),
	UiTextGetText_t1294294173::get_offset_of_text_17(),
	UiTextGetText_t1294294173::get_offset_of_everyFrame_18(),
	UiTextGetText_t1294294173::get_offset_of_uiText_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (UiTextSetText_t1294294577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3839[6] = 
{
	UiTextSetText_t1294294577::get_offset_of_gameObject_16(),
	UiTextSetText_t1294294577::get_offset_of_text_17(),
	UiTextSetText_t1294294577::get_offset_of_resetOnExit_18(),
	UiTextSetText_t1294294577::get_offset_of_everyFrame_19(),
	UiTextSetText_t1294294577::get_offset_of_uiText_20(),
	UiTextSetText_t1294294577::get_offset_of_originalString_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (UiToggleGetIsOn_t3709473285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[6] = 
{
	UiToggleGetIsOn_t3709473285::get_offset_of_gameObject_16(),
	UiToggleGetIsOn_t3709473285::get_offset_of_value_17(),
	UiToggleGetIsOn_t3709473285::get_offset_of_isOnEvent_18(),
	UiToggleGetIsOn_t3709473285::get_offset_of_isOffEvent_19(),
	UiToggleGetIsOn_t3709473285::get_offset_of_everyFrame_20(),
	UiToggleGetIsOn_t3709473285::get_offset_of__toggle_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (UiToggleOnValueChangedEvent_t2905849127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[5] = 
{
	UiToggleOnValueChangedEvent_t2905849127::get_offset_of_gameObject_16(),
	UiToggleOnValueChangedEvent_t2905849127::get_offset_of_eventTarget_17(),
	UiToggleOnValueChangedEvent_t2905849127::get_offset_of_sendEvent_18(),
	UiToggleOnValueChangedEvent_t2905849127::get_offset_of_value_19(),
	UiToggleOnValueChangedEvent_t2905849127::get_offset_of_toggle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (UiToggleSetIsOn_t2270738753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3842[5] = 
{
	UiToggleSetIsOn_t2270738753::get_offset_of_gameObject_16(),
	UiToggleSetIsOn_t2270738753::get_offset_of_isOn_17(),
	UiToggleSetIsOn_t2270738753::get_offset_of_resetOnExit_18(),
	UiToggleSetIsOn_t2270738753::get_offset_of__toggle_19(),
	UiToggleSetIsOn_t2270738753::get_offset_of__originalValue_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (GetComponent_t1263127638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3843[3] = 
{
	GetComponent_t1263127638::get_offset_of_gameObject_14(),
	GetComponent_t1263127638::get_offset_of_storeComponent_15(),
	GetComponent_t1263127638::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (GetProperty_t3607958757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[2] = 
{
	GetProperty_t3607958757::get_offset_of_targetProperty_14(),
	GetProperty_t3607958757::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (SetObjectValue_t3915321542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3845[3] = 
{
	SetObjectValue_t3915321542::get_offset_of_objectVariable_14(),
	SetObjectValue_t3915321542::get_offset_of_objectValue_15(),
	SetObjectValue_t3915321542::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (SetProperty_t714161153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3846[2] = 
{
	SetProperty_t714161153::get_offset_of_targetProperty_14(),
	SetProperty_t714161153::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (DebugVector2_t1875071681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3847[2] = 
{
	DebugVector2_t1875071681::get_offset_of_logLevel_14(),
	DebugVector2_t1875071681::get_offset_of_vector2Variable_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (GetVector2Length_t488136250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3848[3] = 
{
	GetVector2Length_t488136250::get_offset_of_vector2_14(),
	GetVector2Length_t488136250::get_offset_of_storeLength_15(),
	GetVector2Length_t488136250::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (GetVector2XY_t4289145362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3849[4] = 
{
	GetVector2XY_t4289145362::get_offset_of_vector2Variable_14(),
	GetVector2XY_t4289145362::get_offset_of_storeX_15(),
	GetVector2XY_t4289145362::get_offset_of_storeY_16(),
	GetVector2XY_t4289145362::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (SelectRandomVector2_t2658517560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3850[3] = 
{
	SelectRandomVector2_t2658517560::get_offset_of_vector2Array_14(),
	SelectRandomVector2_t2658517560::get_offset_of_weights_15(),
	SelectRandomVector2_t2658517560::get_offset_of_storeVector2_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (SetVector2Value_t1101835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3851[3] = 
{
	SetVector2Value_t1101835::get_offset_of_vector2Variable_14(),
	SetVector2Value_t1101835::get_offset_of_vector2Value_15(),
	SetVector2Value_t1101835::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (SetVector2XY_t2797569270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3852[5] = 
{
	SetVector2XY_t2797569270::get_offset_of_vector2Variable_14(),
	SetVector2XY_t2797569270::get_offset_of_vector2Value_15(),
	SetVector2XY_t2797569270::get_offset_of_x_16(),
	SetVector2XY_t2797569270::get_offset_of_y_17(),
	SetVector2XY_t2797569270::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (Vector2Add_t3269288714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[4] = 
{
	Vector2Add_t3269288714::get_offset_of_vector2Variable_14(),
	Vector2Add_t3269288714::get_offset_of_addVector_15(),
	Vector2Add_t3269288714::get_offset_of_everyFrame_16(),
	Vector2Add_t3269288714::get_offset_of_perSecond_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (Vector2AddXY_t1192973363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[5] = 
{
	Vector2AddXY_t1192973363::get_offset_of_vector2Variable_14(),
	Vector2AddXY_t1192973363::get_offset_of_addX_15(),
	Vector2AddXY_t1192973363::get_offset_of_addY_16(),
	Vector2AddXY_t1192973363::get_offset_of_everyFrame_17(),
	Vector2AddXY_t1192973363::get_offset_of_perSecond_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (Vector2ClampMagnitude_t243667646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3855[3] = 
{
	Vector2ClampMagnitude_t243667646::get_offset_of_vector2Variable_14(),
	Vector2ClampMagnitude_t243667646::get_offset_of_maxLength_15(),
	Vector2ClampMagnitude_t243667646::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (Vector2HighPassFilter_t3943959056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[3] = 
{
	Vector2HighPassFilter_t3943959056::get_offset_of_vector2Variable_14(),
	Vector2HighPassFilter_t3943959056::get_offset_of_filteringFactor_15(),
	Vector2HighPassFilter_t3943959056::get_offset_of_filteredVector_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (Vector2Interpolate_t1656170511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3857[9] = 
{
	Vector2Interpolate_t1656170511::get_offset_of_mode_14(),
	Vector2Interpolate_t1656170511::get_offset_of_fromVector_15(),
	Vector2Interpolate_t1656170511::get_offset_of_toVector_16(),
	Vector2Interpolate_t1656170511::get_offset_of_time_17(),
	Vector2Interpolate_t1656170511::get_offset_of_storeResult_18(),
	Vector2Interpolate_t1656170511::get_offset_of_finishEvent_19(),
	Vector2Interpolate_t1656170511::get_offset_of_realTime_20(),
	Vector2Interpolate_t1656170511::get_offset_of_startTime_21(),
	Vector2Interpolate_t1656170511::get_offset_of_currentTime_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (Vector2Invert_t838402421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3858[2] = 
{
	Vector2Invert_t838402421::get_offset_of_vector2Variable_14(),
	Vector2Invert_t838402421::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (Vector2Lerp_t3977169078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3859[5] = 
{
	Vector2Lerp_t3977169078::get_offset_of_fromVector_14(),
	Vector2Lerp_t3977169078::get_offset_of_toVector_15(),
	Vector2Lerp_t3977169078::get_offset_of_amount_16(),
	Vector2Lerp_t3977169078::get_offset_of_storeResult_17(),
	Vector2Lerp_t3977169078::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (Vector2LowPassFilter_t435226625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3860[3] = 
{
	Vector2LowPassFilter_t435226625::get_offset_of_vector2Variable_14(),
	Vector2LowPassFilter_t435226625::get_offset_of_filteringFactor_15(),
	Vector2LowPassFilter_t435226625::get_offset_of_filteredVector_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (Vector2MoveTowards_t306435168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3861[5] = 
{
	Vector2MoveTowards_t306435168::get_offset_of_source_14(),
	Vector2MoveTowards_t306435168::get_offset_of_target_15(),
	Vector2MoveTowards_t306435168::get_offset_of_maxSpeed_16(),
	Vector2MoveTowards_t306435168::get_offset_of_finishDistance_17(),
	Vector2MoveTowards_t306435168::get_offset_of_finishEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (Vector2Multiply_t636802217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[3] = 
{
	Vector2Multiply_t636802217::get_offset_of_vector2Variable_14(),
	Vector2Multiply_t636802217::get_offset_of_multiplyBy_15(),
	Vector2Multiply_t636802217::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (Vector2Normalize_t87641655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3863[2] = 
{
	Vector2Normalize_t87641655::get_offset_of_vector2Variable_14(),
	Vector2Normalize_t87641655::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (Vector2Operator_t3401148003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[6] = 
{
	Vector2Operator_t3401148003::get_offset_of_vector1_14(),
	Vector2Operator_t3401148003::get_offset_of_vector2_15(),
	Vector2Operator_t3401148003::get_offset_of_operation_16(),
	Vector2Operator_t3401148003::get_offset_of_storeVector2Result_17(),
	Vector2Operator_t3401148003::get_offset_of_storeFloatResult_18(),
	Vector2Operator_t3401148003::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (Vector2Operation_t1926650760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3865[10] = 
{
	Vector2Operation_t1926650760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (Vector2PerSecond_t4028226088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3866[2] = 
{
	Vector2PerSecond_t4028226088::get_offset_of_vector2Variable_14(),
	Vector2PerSecond_t4028226088::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (Vector2RotateTowards_t2909112328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[5] = 
{
	Vector2RotateTowards_t2909112328::get_offset_of_currentDirection_14(),
	Vector2RotateTowards_t2909112328::get_offset_of_targetDirection_15(),
	Vector2RotateTowards_t2909112328::get_offset_of_rotateSpeed_16(),
	Vector2RotateTowards_t2909112328::get_offset_of_current_17(),
	Vector2RotateTowards_t2909112328::get_offset_of_target_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (Vector2Subtract_t213488385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3868[3] = 
{
	Vector2Subtract_t213488385::get_offset_of_vector2Variable_14(),
	Vector2Subtract_t213488385::get_offset_of_subtractVector_15(),
	Vector2Subtract_t213488385::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (GetVector3XYZ_t1886187690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3869[5] = 
{
	GetVector3XYZ_t1886187690::get_offset_of_vector3Variable_14(),
	GetVector3XYZ_t1886187690::get_offset_of_storeX_15(),
	GetVector3XYZ_t1886187690::get_offset_of_storeY_16(),
	GetVector3XYZ_t1886187690::get_offset_of_storeZ_17(),
	GetVector3XYZ_t1886187690::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (GetVectorLength_t1339640164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3870[2] = 
{
	GetVectorLength_t1339640164::get_offset_of_vector3_14(),
	GetVectorLength_t1339640164::get_offset_of_storeLength_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (SelectRandomVector3_t2658517559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3871[3] = 
{
	SelectRandomVector3_t2658517559::get_offset_of_vector3Array_14(),
	SelectRandomVector3_t2658517559::get_offset_of_weights_15(),
	SelectRandomVector3_t2658517559::get_offset_of_storeVector3_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (SetVector3Value_t606588939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3872[3] = 
{
	SetVector3Value_t606588939::get_offset_of_vector3Variable_14(),
	SetVector3Value_t606588939::get_offset_of_vector3Value_15(),
	SetVector3Value_t606588939::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (SetVector3XYZ_t1065958254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3873[6] = 
{
	SetVector3XYZ_t1065958254::get_offset_of_vector3Variable_14(),
	SetVector3XYZ_t1065958254::get_offset_of_vector3Value_15(),
	SetVector3XYZ_t1065958254::get_offset_of_x_16(),
	SetVector3XYZ_t1065958254::get_offset_of_y_17(),
	SetVector3XYZ_t1065958254::get_offset_of_z_18(),
	SetVector3XYZ_t1065958254::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (Vector3Add_t3269288713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3874[4] = 
{
	Vector3Add_t3269288713::get_offset_of_vector3Variable_14(),
	Vector3Add_t3269288713::get_offset_of_addVector_15(),
	Vector3Add_t3269288713::get_offset_of_everyFrame_16(),
	Vector3Add_t3269288713::get_offset_of_perSecond_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (Vector3AddXYZ_t1772202739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3875[6] = 
{
	Vector3AddXYZ_t1772202739::get_offset_of_vector3Variable_14(),
	Vector3AddXYZ_t1772202739::get_offset_of_addX_15(),
	Vector3AddXYZ_t1772202739::get_offset_of_addY_16(),
	Vector3AddXYZ_t1772202739::get_offset_of_addZ_17(),
	Vector3AddXYZ_t1772202739::get_offset_of_everyFrame_18(),
	Vector3AddXYZ_t1772202739::get_offset_of_perSecond_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (Vector3ClampMagnitude_t243703331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[3] = 
{
	Vector3ClampMagnitude_t243703331::get_offset_of_vector3Variable_14(),
	Vector3ClampMagnitude_t243703331::get_offset_of_maxLength_15(),
	Vector3ClampMagnitude_t243703331::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (Vector3HighPassFilter_t3943992817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3877[3] = 
{
	Vector3HighPassFilter_t3943992817::get_offset_of_vector3Variable_14(),
	Vector3HighPassFilter_t3943992817::get_offset_of_filteringFactor_15(),
	Vector3HighPassFilter_t3943992817::get_offset_of_filteredVector_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (Vector3Interpolate_t1656171596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3878[9] = 
{
	Vector3Interpolate_t1656171596::get_offset_of_mode_14(),
	Vector3Interpolate_t1656171596::get_offset_of_fromVector_15(),
	Vector3Interpolate_t1656171596::get_offset_of_toVector_16(),
	Vector3Interpolate_t1656171596::get_offset_of_time_17(),
	Vector3Interpolate_t1656171596::get_offset_of_storeResult_18(),
	Vector3Interpolate_t1656171596::get_offset_of_finishEvent_19(),
	Vector3Interpolate_t1656171596::get_offset_of_realTime_20(),
	Vector3Interpolate_t1656171596::get_offset_of_startTime_21(),
	Vector3Interpolate_t1656171596::get_offset_of_currentTime_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (Vector3Invert_t838402458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3879[2] = 
{
	Vector3Invert_t838402458::get_offset_of_vector3Variable_14(),
	Vector3Invert_t838402458::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (Vector3Lerp_t3977169109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3880[5] = 
{
	Vector3Lerp_t3977169109::get_offset_of_fromVector_14(),
	Vector3Lerp_t3977169109::get_offset_of_toVector_15(),
	Vector3Lerp_t3977169109::get_offset_of_amount_16(),
	Vector3Lerp_t3977169109::get_offset_of_storeResult_17(),
	Vector3Lerp_t3977169109::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (Vector3LowPassFilter_t435448546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3881[3] = 
{
	Vector3LowPassFilter_t435448546::get_offset_of_vector3Variable_14(),
	Vector3LowPassFilter_t435448546::get_offset_of_filteringFactor_15(),
	Vector3LowPassFilter_t435448546::get_offset_of_filteredVector_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (Vector3Multiply_t636803490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3882[3] = 
{
	Vector3Multiply_t636803490::get_offset_of_vector3Variable_14(),
	Vector3Multiply_t636803490::get_offset_of_multiplyBy_15(),
	Vector3Multiply_t636803490::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (Vector3Normalize_t87642748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3883[2] = 
{
	Vector3Normalize_t87642748::get_offset_of_vector3Variable_14(),
	Vector3Normalize_t87642748::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (Vector3Operator_t3401148894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[6] = 
{
	Vector3Operator_t3401148894::get_offset_of_vector1_14(),
	Vector3Operator_t3401148894::get_offset_of_vector2_15(),
	Vector3Operator_t3401148894::get_offset_of_operation_16(),
	Vector3Operator_t3401148894::get_offset_of_storeVector3Result_17(),
	Vector3Operator_t3401148894::get_offset_of_storeFloatResult_18(),
	Vector3Operator_t3401148894::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (Vector3Operation_t3413508724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3885[13] = 
{
	Vector3Operation_t3413508724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (Vector3PerSecond_t4028224785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3886[2] = 
{
	Vector3PerSecond_t4028224785::get_offset_of_vector3Variable_14(),
	Vector3PerSecond_t4028224785::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (Vector3RotateTowards_t2909139811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3887[4] = 
{
	Vector3RotateTowards_t2909139811::get_offset_of_currentDirection_14(),
	Vector3RotateTowards_t2909139811::get_offset_of_targetDirection_15(),
	Vector3RotateTowards_t2909139811::get_offset_of_rotateSpeed_16(),
	Vector3RotateTowards_t2909139811::get_offset_of_maxMagnitude_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (Vector3Subtract_t213489640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3888[3] = 
{
	Vector3Subtract_t213489640::get_offset_of_vector3Variable_14(),
	Vector3Subtract_t213489640::get_offset_of_subtractVector_15(),
	Vector3Subtract_t213489640::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (FsmProcessor_t2217570134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (UpdateHelper_t1432531020), -1, sizeof(UpdateHelper_t1432531020_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3890[1] = 
{
	UpdateHelper_t1432531020_StaticFields::get_offset_of_editorPrefLoaded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3891[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3892[6] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_trackingEventCatcher_5(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_markerTexture_6(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_explodeObjA_7(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_explodeObjB_8(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_explodeObjC_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
