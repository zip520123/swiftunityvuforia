﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Actions.UiButtonArray
struct UiButtonArray_t3593418030;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t919699796;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2511618479;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t719957643;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t635502296;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t4259038327;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// HutongGames.PlayMaker.PlayMakerCanvasRaycastFilterProxy
struct PlayMakerCanvasRaycastFilterProxy_t3073240404;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t3085084973;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_t447389798;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1076084509;
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t3344766165;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.Events.UnityAction[]
struct UnityActionU5BU5D_t1672898414;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2297175928;
// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2767979955;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CONENTERU3EC__ANONSTOREY0_T2283252108_H
#define U3CONENTERU3EC__ANONSTOREY0_T2283252108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiButtonArray/<OnEnter>c__AnonStorey0
struct  U3COnEnterU3Ec__AnonStorey0_t2283252108  : public RuntimeObject
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.UiButtonArray/<OnEnter>c__AnonStorey0::index
	int32_t ___index_0;
	// HutongGames.PlayMaker.Actions.UiButtonArray HutongGames.PlayMaker.Actions.UiButtonArray/<OnEnter>c__AnonStorey0::$this
	UiButtonArray_t3593418030 * ___U24this_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3COnEnterU3Ec__AnonStorey0_t2283252108, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnEnterU3Ec__AnonStorey0_t2283252108, ___U24this_1)); }
	inline UiButtonArray_t3593418030 * get_U24this_1() const { return ___U24this_1; }
	inline UiButtonArray_t3593418030 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(UiButtonArray_t3593418030 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONENTERU3EC__ANONSTOREY0_T2283252108_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef ATAN2ENUMAXIS_T207693546_H
#define ATAN2ENUMAXIS_T207693546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAtan2FromVector3/aTan2EnumAxis
struct  aTan2EnumAxis_t207693546 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetAtan2FromVector3/aTan2EnumAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(aTan2EnumAxis_t207693546, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATAN2ENUMAXIS_T207693546_H
#ifndef EVENTHANDLERS_T409295930_H
#define EVENTHANDLERS_T409295930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent/EventHandlers
struct  EventHandlers_t409295930 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent/EventHandlers::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventHandlers_t409295930, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLERS_T409295930_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef COMPONENTACTION_1_T1655560330_H
#define COMPONENTACTION_1_T1655560330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<HutongGames.PlayMaker.PlayMakerCanvasRaycastFilterProxy>
struct  ComponentAction_1_t1655560330  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	PlayMakerCanvasRaycastFilterProxy_t3073240404 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1655560330, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1655560330, ___cachedComponent_15)); }
	inline PlayMakerCanvasRaycastFilterProxy_t3073240404 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline PlayMakerCanvasRaycastFilterProxy_t3073240404 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(PlayMakerCanvasRaycastFilterProxy_t3073240404 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1655560330_H
#ifndef COMPONENTACTION_1_T2665831686_H
#define COMPONENTACTION_1_T2665831686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.CanvasGroup>
struct  ComponentAction_1_t2665831686  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	CanvasGroup_t4083511760 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2665831686, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2665831686, ___cachedComponent_15)); }
	inline CanvasGroup_t4083511760 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline CanvasGroup_t4083511760 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(CanvasGroup_t4083511760 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2665831686_H
#ifndef COMPONENTACTION_1_T3953371731_H
#define COMPONENTACTION_1_T3953371731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.EventSystems.EventTrigger>
struct  ComponentAction_1_t3953371731  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	EventTrigger_t1076084509 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3953371731, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3953371731, ___cachedComponent_15)); }
	inline EventTrigger_t1076084509 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline EventTrigger_t1076084509 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(EventTrigger_t1076084509 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T3953371731_H
#ifndef COMPONENTACTION_1_T2637352395_H
#define COMPONENTACTION_1_T2637352395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Button>
struct  ComponentAction_1_t2637352395  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Button_t4055032469 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2637352395, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2637352395, ___cachedComponent_15)); }
	inline Button_t4055032469 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Button_t4055032469 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Button_t4055032469 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2637352395_H
#ifndef COMPONENTACTION_1_T1350299881_H
#define COMPONENTACTION_1_T1350299881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.CanvasScaler>
struct  ComponentAction_1_t1350299881  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	CanvasScaler_t2767979955 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1350299881, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1350299881, ___cachedComponent_15)); }
	inline CanvasScaler_t2767979955 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline CanvasScaler_t2767979955 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(CanvasScaler_t2767979955 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1350299881_H
#ifndef COMPONENTACTION_1_T856711151_H
#define COMPONENTACTION_1_T856711151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Dropdown>
struct  ComponentAction_1_t856711151  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Dropdown_t2274391225 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t856711151, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t856711151, ___cachedComponent_15)); }
	inline Dropdown_t2274391225 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Dropdown_t2274391225 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Dropdown_t2274391225 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T856711151_H
#ifndef COMPONENTACTION_1_T242655537_H
#define COMPONENTACTION_1_T242655537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Graphic>
struct  ComponentAction_1_t242655537  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Graphic_t1660335611 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t242655537, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t242655537, ___cachedComponent_15)); }
	inline Graphic_t1660335611 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Graphic_t1660335611 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Graphic_t1660335611 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T242655537_H
#ifndef COMPONENTACTION_1_T1252589577_H
#define COMPONENTACTION_1_T1252589577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Image>
struct  ComponentAction_1_t1252589577  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Image_t2670269651 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1252589577, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1252589577, ___cachedComponent_15)); }
	inline Image_t2670269651 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Image_t2670269651 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Image_t2670269651 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1252589577_H
#ifndef COMPONENTACTION_1_T2345237357_H
#define COMPONENTACTION_1_T2345237357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.InputField>
struct  ComponentAction_1_t2345237357  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	InputField_t3762917431 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2345237357, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2345237357, ___cachedComponent_15)); }
	inline InputField_t3762917431 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline InputField_t3762917431 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(InputField_t3762917431 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2345237357_H
#ifndef COMPONENTACTION_1_T367723604_H
#define COMPONENTACTION_1_T367723604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.LayoutElement>
struct  ComponentAction_1_t367723604  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	LayoutElement_t1785403678 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t367723604, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t367723604, ___cachedComponent_15)); }
	inline LayoutElement_t1785403678 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline LayoutElement_t1785403678 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(LayoutElement_t1785403678 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T367723604_H
#ifndef COMPONENTACTION_1_T1832348367_H
#define COMPONENTACTION_1_T1832348367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.UI.Selectable>
struct  ComponentAction_1_t1832348367  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Selectable_t3250028441 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1832348367, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1832348367, ___cachedComponent_15)); }
	inline Selectable_t3250028441 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Selectable_t3250028441 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Selectable_t3250028441 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1832348367_H
#ifndef GETACOSINE_T2060389857_H
#define GETACOSINE_T2060389857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetACosine
struct  GetACosine_t2060389857  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetACosine::Value
	FsmFloat_t2883254149 * ___Value_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetACosine::angle
	FsmFloat_t2883254149 * ___angle_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetACosine::RadToDeg
	FsmBool_t163807967 * ___RadToDeg_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetACosine::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_Value_14() { return static_cast<int32_t>(offsetof(GetACosine_t2060389857, ___Value_14)); }
	inline FsmFloat_t2883254149 * get_Value_14() const { return ___Value_14; }
	inline FsmFloat_t2883254149 ** get_address_of_Value_14() { return &___Value_14; }
	inline void set_Value_14(FsmFloat_t2883254149 * value)
	{
		___Value_14 = value;
		Il2CppCodeGenWriteBarrier((&___Value_14), value);
	}

	inline static int32_t get_offset_of_angle_15() { return static_cast<int32_t>(offsetof(GetACosine_t2060389857, ___angle_15)); }
	inline FsmFloat_t2883254149 * get_angle_15() const { return ___angle_15; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_15() { return &___angle_15; }
	inline void set_angle_15(FsmFloat_t2883254149 * value)
	{
		___angle_15 = value;
		Il2CppCodeGenWriteBarrier((&___angle_15), value);
	}

	inline static int32_t get_offset_of_RadToDeg_16() { return static_cast<int32_t>(offsetof(GetACosine_t2060389857, ___RadToDeg_16)); }
	inline FsmBool_t163807967 * get_RadToDeg_16() const { return ___RadToDeg_16; }
	inline FsmBool_t163807967 ** get_address_of_RadToDeg_16() { return &___RadToDeg_16; }
	inline void set_RadToDeg_16(FsmBool_t163807967 * value)
	{
		___RadToDeg_16 = value;
		Il2CppCodeGenWriteBarrier((&___RadToDeg_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetACosine_t2060389857, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETACOSINE_T2060389857_H
#ifndef GETASINE_T718811337_H
#define GETASINE_T718811337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetASine
struct  GetASine_t718811337  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetASine::Value
	FsmFloat_t2883254149 * ___Value_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetASine::angle
	FsmFloat_t2883254149 * ___angle_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetASine::RadToDeg
	FsmBool_t163807967 * ___RadToDeg_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetASine::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_Value_14() { return static_cast<int32_t>(offsetof(GetASine_t718811337, ___Value_14)); }
	inline FsmFloat_t2883254149 * get_Value_14() const { return ___Value_14; }
	inline FsmFloat_t2883254149 ** get_address_of_Value_14() { return &___Value_14; }
	inline void set_Value_14(FsmFloat_t2883254149 * value)
	{
		___Value_14 = value;
		Il2CppCodeGenWriteBarrier((&___Value_14), value);
	}

	inline static int32_t get_offset_of_angle_15() { return static_cast<int32_t>(offsetof(GetASine_t718811337, ___angle_15)); }
	inline FsmFloat_t2883254149 * get_angle_15() const { return ___angle_15; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_15() { return &___angle_15; }
	inline void set_angle_15(FsmFloat_t2883254149 * value)
	{
		___angle_15 = value;
		Il2CppCodeGenWriteBarrier((&___angle_15), value);
	}

	inline static int32_t get_offset_of_RadToDeg_16() { return static_cast<int32_t>(offsetof(GetASine_t718811337, ___RadToDeg_16)); }
	inline FsmBool_t163807967 * get_RadToDeg_16() const { return ___RadToDeg_16; }
	inline FsmBool_t163807967 ** get_address_of_RadToDeg_16() { return &___RadToDeg_16; }
	inline void set_RadToDeg_16(FsmBool_t163807967 * value)
	{
		___RadToDeg_16 = value;
		Il2CppCodeGenWriteBarrier((&___RadToDeg_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetASine_t718811337, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETASINE_T718811337_H
#ifndef GETATAN_T2743258534_H
#define GETATAN_T2743258534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAtan
struct  GetAtan_t2743258534  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan::Value
	FsmFloat_t2883254149 * ___Value_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan::angle
	FsmFloat_t2883254149 * ___angle_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAtan::RadToDeg
	FsmBool_t163807967 * ___RadToDeg_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAtan::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_Value_14() { return static_cast<int32_t>(offsetof(GetAtan_t2743258534, ___Value_14)); }
	inline FsmFloat_t2883254149 * get_Value_14() const { return ___Value_14; }
	inline FsmFloat_t2883254149 ** get_address_of_Value_14() { return &___Value_14; }
	inline void set_Value_14(FsmFloat_t2883254149 * value)
	{
		___Value_14 = value;
		Il2CppCodeGenWriteBarrier((&___Value_14), value);
	}

	inline static int32_t get_offset_of_angle_15() { return static_cast<int32_t>(offsetof(GetAtan_t2743258534, ___angle_15)); }
	inline FsmFloat_t2883254149 * get_angle_15() const { return ___angle_15; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_15() { return &___angle_15; }
	inline void set_angle_15(FsmFloat_t2883254149 * value)
	{
		___angle_15 = value;
		Il2CppCodeGenWriteBarrier((&___angle_15), value);
	}

	inline static int32_t get_offset_of_RadToDeg_16() { return static_cast<int32_t>(offsetof(GetAtan_t2743258534, ___RadToDeg_16)); }
	inline FsmBool_t163807967 * get_RadToDeg_16() const { return ___RadToDeg_16; }
	inline FsmBool_t163807967 ** get_address_of_RadToDeg_16() { return &___RadToDeg_16; }
	inline void set_RadToDeg_16(FsmBool_t163807967 * value)
	{
		___RadToDeg_16 = value;
		Il2CppCodeGenWriteBarrier((&___RadToDeg_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetAtan_t2743258534, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETATAN_T2743258534_H
#ifndef GETATAN2_T2742078886_H
#define GETATAN2_T2742078886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAtan2
struct  GetAtan2_t2742078886  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan2::xValue
	FsmFloat_t2883254149 * ___xValue_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan2::yValue
	FsmFloat_t2883254149 * ___yValue_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan2::angle
	FsmFloat_t2883254149 * ___angle_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAtan2::RadToDeg
	FsmBool_t163807967 * ___RadToDeg_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAtan2::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_xValue_14() { return static_cast<int32_t>(offsetof(GetAtan2_t2742078886, ___xValue_14)); }
	inline FsmFloat_t2883254149 * get_xValue_14() const { return ___xValue_14; }
	inline FsmFloat_t2883254149 ** get_address_of_xValue_14() { return &___xValue_14; }
	inline void set_xValue_14(FsmFloat_t2883254149 * value)
	{
		___xValue_14 = value;
		Il2CppCodeGenWriteBarrier((&___xValue_14), value);
	}

	inline static int32_t get_offset_of_yValue_15() { return static_cast<int32_t>(offsetof(GetAtan2_t2742078886, ___yValue_15)); }
	inline FsmFloat_t2883254149 * get_yValue_15() const { return ___yValue_15; }
	inline FsmFloat_t2883254149 ** get_address_of_yValue_15() { return &___yValue_15; }
	inline void set_yValue_15(FsmFloat_t2883254149 * value)
	{
		___yValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___yValue_15), value);
	}

	inline static int32_t get_offset_of_angle_16() { return static_cast<int32_t>(offsetof(GetAtan2_t2742078886, ___angle_16)); }
	inline FsmFloat_t2883254149 * get_angle_16() const { return ___angle_16; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_16() { return &___angle_16; }
	inline void set_angle_16(FsmFloat_t2883254149 * value)
	{
		___angle_16 = value;
		Il2CppCodeGenWriteBarrier((&___angle_16), value);
	}

	inline static int32_t get_offset_of_RadToDeg_17() { return static_cast<int32_t>(offsetof(GetAtan2_t2742078886, ___RadToDeg_17)); }
	inline FsmBool_t163807967 * get_RadToDeg_17() const { return ___RadToDeg_17; }
	inline FsmBool_t163807967 ** get_address_of_RadToDeg_17() { return &___RadToDeg_17; }
	inline void set_RadToDeg_17(FsmBool_t163807967 * value)
	{
		___RadToDeg_17 = value;
		Il2CppCodeGenWriteBarrier((&___RadToDeg_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetAtan2_t2742078886, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETATAN2_T2742078886_H
#ifndef GETATAN2FROMVECTOR2_T1932972477_H
#define GETATAN2FROMVECTOR2_T1932972477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAtan2FromVector2
struct  GetAtan2FromVector2_t1932972477  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetAtan2FromVector2::vector2
	FsmVector2_t2965096677 * ___vector2_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan2FromVector2::angle
	FsmFloat_t2883254149 * ___angle_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAtan2FromVector2::RadToDeg
	FsmBool_t163807967 * ___RadToDeg_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAtan2FromVector2::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_vector2_14() { return static_cast<int32_t>(offsetof(GetAtan2FromVector2_t1932972477, ___vector2_14)); }
	inline FsmVector2_t2965096677 * get_vector2_14() const { return ___vector2_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2_14() { return &___vector2_14; }
	inline void set_vector2_14(FsmVector2_t2965096677 * value)
	{
		___vector2_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector2_14), value);
	}

	inline static int32_t get_offset_of_angle_15() { return static_cast<int32_t>(offsetof(GetAtan2FromVector2_t1932972477, ___angle_15)); }
	inline FsmFloat_t2883254149 * get_angle_15() const { return ___angle_15; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_15() { return &___angle_15; }
	inline void set_angle_15(FsmFloat_t2883254149 * value)
	{
		___angle_15 = value;
		Il2CppCodeGenWriteBarrier((&___angle_15), value);
	}

	inline static int32_t get_offset_of_RadToDeg_16() { return static_cast<int32_t>(offsetof(GetAtan2FromVector2_t1932972477, ___RadToDeg_16)); }
	inline FsmBool_t163807967 * get_RadToDeg_16() const { return ___RadToDeg_16; }
	inline FsmBool_t163807967 ** get_address_of_RadToDeg_16() { return &___RadToDeg_16; }
	inline void set_RadToDeg_16(FsmBool_t163807967 * value)
	{
		___RadToDeg_16 = value;
		Il2CppCodeGenWriteBarrier((&___RadToDeg_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetAtan2FromVector2_t1932972477, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETATAN2FROMVECTOR2_T1932972477_H
#ifndef GETATAN2FROMVECTOR3_T1932972476_H
#define GETATAN2FROMVECTOR3_T1932972476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAtan2FromVector3
struct  GetAtan2FromVector3_t1932972476  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAtan2FromVector3::vector3
	FsmVector3_t626444517 * ___vector3_14;
	// HutongGames.PlayMaker.Actions.GetAtan2FromVector3/aTan2EnumAxis HutongGames.PlayMaker.Actions.GetAtan2FromVector3::xAxis
	int32_t ___xAxis_15;
	// HutongGames.PlayMaker.Actions.GetAtan2FromVector3/aTan2EnumAxis HutongGames.PlayMaker.Actions.GetAtan2FromVector3::yAxis
	int32_t ___yAxis_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAtan2FromVector3::angle
	FsmFloat_t2883254149 * ___angle_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAtan2FromVector3::RadToDeg
	FsmBool_t163807967 * ___RadToDeg_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAtan2FromVector3::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_vector3_14() { return static_cast<int32_t>(offsetof(GetAtan2FromVector3_t1932972476, ___vector3_14)); }
	inline FsmVector3_t626444517 * get_vector3_14() const { return ___vector3_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector3_14() { return &___vector3_14; }
	inline void set_vector3_14(FsmVector3_t626444517 * value)
	{
		___vector3_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector3_14), value);
	}

	inline static int32_t get_offset_of_xAxis_15() { return static_cast<int32_t>(offsetof(GetAtan2FromVector3_t1932972476, ___xAxis_15)); }
	inline int32_t get_xAxis_15() const { return ___xAxis_15; }
	inline int32_t* get_address_of_xAxis_15() { return &___xAxis_15; }
	inline void set_xAxis_15(int32_t value)
	{
		___xAxis_15 = value;
	}

	inline static int32_t get_offset_of_yAxis_16() { return static_cast<int32_t>(offsetof(GetAtan2FromVector3_t1932972476, ___yAxis_16)); }
	inline int32_t get_yAxis_16() const { return ___yAxis_16; }
	inline int32_t* get_address_of_yAxis_16() { return &___yAxis_16; }
	inline void set_yAxis_16(int32_t value)
	{
		___yAxis_16 = value;
	}

	inline static int32_t get_offset_of_angle_17() { return static_cast<int32_t>(offsetof(GetAtan2FromVector3_t1932972476, ___angle_17)); }
	inline FsmFloat_t2883254149 * get_angle_17() const { return ___angle_17; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_17() { return &___angle_17; }
	inline void set_angle_17(FsmFloat_t2883254149 * value)
	{
		___angle_17 = value;
		Il2CppCodeGenWriteBarrier((&___angle_17), value);
	}

	inline static int32_t get_offset_of_RadToDeg_18() { return static_cast<int32_t>(offsetof(GetAtan2FromVector3_t1932972476, ___RadToDeg_18)); }
	inline FsmBool_t163807967 * get_RadToDeg_18() const { return ___RadToDeg_18; }
	inline FsmBool_t163807967 ** get_address_of_RadToDeg_18() { return &___RadToDeg_18; }
	inline void set_RadToDeg_18(FsmBool_t163807967 * value)
	{
		___RadToDeg_18 = value;
		Il2CppCodeGenWriteBarrier((&___RadToDeg_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GetAtan2FromVector3_t1932972476, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETATAN2FROMVECTOR3_T1932972476_H
#ifndef GETCOSINE_T2142229402_H
#define GETCOSINE_T2142229402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetCosine
struct  GetCosine_t2142229402  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCosine::angle
	FsmFloat_t2883254149 * ___angle_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetCosine::DegToRad
	FsmBool_t163807967 * ___DegToRad_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCosine::result
	FsmFloat_t2883254149 * ___result_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetCosine::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_angle_14() { return static_cast<int32_t>(offsetof(GetCosine_t2142229402, ___angle_14)); }
	inline FsmFloat_t2883254149 * get_angle_14() const { return ___angle_14; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_14() { return &___angle_14; }
	inline void set_angle_14(FsmFloat_t2883254149 * value)
	{
		___angle_14 = value;
		Il2CppCodeGenWriteBarrier((&___angle_14), value);
	}

	inline static int32_t get_offset_of_DegToRad_15() { return static_cast<int32_t>(offsetof(GetCosine_t2142229402, ___DegToRad_15)); }
	inline FsmBool_t163807967 * get_DegToRad_15() const { return ___DegToRad_15; }
	inline FsmBool_t163807967 ** get_address_of_DegToRad_15() { return &___DegToRad_15; }
	inline void set_DegToRad_15(FsmBool_t163807967 * value)
	{
		___DegToRad_15 = value;
		Il2CppCodeGenWriteBarrier((&___DegToRad_15), value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(GetCosine_t2142229402, ___result_16)); }
	inline FsmFloat_t2883254149 * get_result_16() const { return ___result_16; }
	inline FsmFloat_t2883254149 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmFloat_t2883254149 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier((&___result_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetCosine_t2142229402, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOSINE_T2142229402_H
#ifndef GETSINE_T1694078994_H
#define GETSINE_T1694078994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSine
struct  GetSine_t1694078994  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetSine::angle
	FsmFloat_t2883254149 * ___angle_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSine::DegToRad
	FsmBool_t163807967 * ___DegToRad_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetSine::result
	FsmFloat_t2883254149 * ___result_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSine::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_angle_14() { return static_cast<int32_t>(offsetof(GetSine_t1694078994, ___angle_14)); }
	inline FsmFloat_t2883254149 * get_angle_14() const { return ___angle_14; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_14() { return &___angle_14; }
	inline void set_angle_14(FsmFloat_t2883254149 * value)
	{
		___angle_14 = value;
		Il2CppCodeGenWriteBarrier((&___angle_14), value);
	}

	inline static int32_t get_offset_of_DegToRad_15() { return static_cast<int32_t>(offsetof(GetSine_t1694078994, ___DegToRad_15)); }
	inline FsmBool_t163807967 * get_DegToRad_15() const { return ___DegToRad_15; }
	inline FsmBool_t163807967 ** get_address_of_DegToRad_15() { return &___DegToRad_15; }
	inline void set_DegToRad_15(FsmBool_t163807967 * value)
	{
		___DegToRad_15 = value;
		Il2CppCodeGenWriteBarrier((&___DegToRad_15), value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(GetSine_t1694078994, ___result_16)); }
	inline FsmFloat_t2883254149 * get_result_16() const { return ___result_16; }
	inline FsmFloat_t2883254149 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmFloat_t2883254149 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier((&___result_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetSine_t1694078994, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSINE_T1694078994_H
#ifndef GETTAN_T4257013587_H
#define GETTAN_T4257013587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTan
struct  GetTan_t4257013587  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTan::angle
	FsmFloat_t2883254149 * ___angle_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetTan::DegToRad
	FsmBool_t163807967 * ___DegToRad_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTan::result
	FsmFloat_t2883254149 * ___result_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTan::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_angle_14() { return static_cast<int32_t>(offsetof(GetTan_t4257013587, ___angle_14)); }
	inline FsmFloat_t2883254149 * get_angle_14() const { return ___angle_14; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_14() { return &___angle_14; }
	inline void set_angle_14(FsmFloat_t2883254149 * value)
	{
		___angle_14 = value;
		Il2CppCodeGenWriteBarrier((&___angle_14), value);
	}

	inline static int32_t get_offset_of_DegToRad_15() { return static_cast<int32_t>(offsetof(GetTan_t4257013587, ___DegToRad_15)); }
	inline FsmBool_t163807967 * get_DegToRad_15() const { return ___DegToRad_15; }
	inline FsmBool_t163807967 ** get_address_of_DegToRad_15() { return &___DegToRad_15; }
	inline void set_DegToRad_15(FsmBool_t163807967 * value)
	{
		___DegToRad_15 = value;
		Il2CppCodeGenWriteBarrier((&___DegToRad_15), value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(GetTan_t4257013587, ___result_16)); }
	inline FsmFloat_t2883254149 * get_result_16() const { return ___result_16; }
	inline FsmFloat_t2883254149 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmFloat_t2883254149 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier((&___result_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetTan_t4257013587, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTAN_T4257013587_H
#ifndef SETPOSITION_T795544129_H
#define SETPOSITION_T795544129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetPosition
struct  SetPosition_t795544129  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetPosition::vector
	FsmVector3_t626444517 * ___vector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetPosition::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetPosition::y
	FsmFloat_t2883254149 * ___y_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetPosition::z
	FsmFloat_t2883254149 * ___z_18;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.SetPosition::space
	int32_t ___space_19;
	// System.Boolean HutongGames.PlayMaker.Actions.SetPosition::everyFrame
	bool ___everyFrame_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetPosition::lateUpdate
	bool ___lateUpdate_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector_15() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___vector_15)); }
	inline FsmVector3_t626444517 * get_vector_15() const { return ___vector_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector_15() { return &___vector_15; }
	inline void set_vector_15(FsmVector3_t626444517 * value)
	{
		___vector_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_z_18() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___z_18)); }
	inline FsmFloat_t2883254149 * get_z_18() const { return ___z_18; }
	inline FsmFloat_t2883254149 ** get_address_of_z_18() { return &___z_18; }
	inline void set_z_18(FsmFloat_t2883254149 * value)
	{
		___z_18 = value;
		Il2CppCodeGenWriteBarrier((&___z_18), value);
	}

	inline static int32_t get_offset_of_space_19() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___space_19)); }
	inline int32_t get_space_19() const { return ___space_19; }
	inline int32_t* get_address_of_space_19() { return &___space_19; }
	inline void set_space_19(int32_t value)
	{
		___space_19 = value;
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_21() { return static_cast<int32_t>(offsetof(SetPosition_t795544129, ___lateUpdate_21)); }
	inline bool get_lateUpdate_21() const { return ___lateUpdate_21; }
	inline bool* get_address_of_lateUpdate_21() { return &___lateUpdate_21; }
	inline void set_lateUpdate_21(bool value)
	{
		___lateUpdate_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPOSITION_T795544129_H
#ifndef SETRANDOMROTATION_T2959778138_H
#define SETRANDOMROTATION_T2959778138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRandomRotation
struct  SetRandomRotation_t2959778138  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetRandomRotation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetRandomRotation::x
	FsmBool_t163807967 * ___x_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetRandomRotation::y
	FsmBool_t163807967 * ___y_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetRandomRotation::z
	FsmBool_t163807967 * ___z_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetRandomRotation_t2959778138, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(SetRandomRotation_t2959778138, ___x_15)); }
	inline FsmBool_t163807967 * get_x_15() const { return ___x_15; }
	inline FsmBool_t163807967 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmBool_t163807967 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier((&___x_15), value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(SetRandomRotation_t2959778138, ___y_16)); }
	inline FsmBool_t163807967 * get_y_16() const { return ___y_16; }
	inline FsmBool_t163807967 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmBool_t163807967 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier((&___y_16), value);
	}

	inline static int32_t get_offset_of_z_17() { return static_cast<int32_t>(offsetof(SetRandomRotation_t2959778138, ___z_17)); }
	inline FsmBool_t163807967 * get_z_17() const { return ___z_17; }
	inline FsmBool_t163807967 ** get_address_of_z_17() { return &___z_17; }
	inline void set_z_17(FsmBool_t163807967 * value)
	{
		___z_17 = value;
		Il2CppCodeGenWriteBarrier((&___z_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETRANDOMROTATION_T2959778138_H
#ifndef SETROTATION_T3293972810_H
#define SETROTATION_T3293972810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRotation
struct  SetRotation_t3293972810  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetRotation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetRotation::quaternion
	FsmQuaternion_t4259038327 * ___quaternion_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetRotation::vector
	FsmVector3_t626444517 * ___vector_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRotation::xAngle
	FsmFloat_t2883254149 * ___xAngle_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRotation::yAngle
	FsmFloat_t2883254149 * ___yAngle_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRotation::zAngle
	FsmFloat_t2883254149 * ___zAngle_19;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.SetRotation::space
	int32_t ___space_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRotation::everyFrame
	bool ___everyFrame_21;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRotation::lateUpdate
	bool ___lateUpdate_22;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_quaternion_15() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___quaternion_15)); }
	inline FsmQuaternion_t4259038327 * get_quaternion_15() const { return ___quaternion_15; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternion_15() { return &___quaternion_15; }
	inline void set_quaternion_15(FsmQuaternion_t4259038327 * value)
	{
		___quaternion_15 = value;
		Il2CppCodeGenWriteBarrier((&___quaternion_15), value);
	}

	inline static int32_t get_offset_of_vector_16() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___vector_16)); }
	inline FsmVector3_t626444517 * get_vector_16() const { return ___vector_16; }
	inline FsmVector3_t626444517 ** get_address_of_vector_16() { return &___vector_16; }
	inline void set_vector_16(FsmVector3_t626444517 * value)
	{
		___vector_16 = value;
		Il2CppCodeGenWriteBarrier((&___vector_16), value);
	}

	inline static int32_t get_offset_of_xAngle_17() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___xAngle_17)); }
	inline FsmFloat_t2883254149 * get_xAngle_17() const { return ___xAngle_17; }
	inline FsmFloat_t2883254149 ** get_address_of_xAngle_17() { return &___xAngle_17; }
	inline void set_xAngle_17(FsmFloat_t2883254149 * value)
	{
		___xAngle_17 = value;
		Il2CppCodeGenWriteBarrier((&___xAngle_17), value);
	}

	inline static int32_t get_offset_of_yAngle_18() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___yAngle_18)); }
	inline FsmFloat_t2883254149 * get_yAngle_18() const { return ___yAngle_18; }
	inline FsmFloat_t2883254149 ** get_address_of_yAngle_18() { return &___yAngle_18; }
	inline void set_yAngle_18(FsmFloat_t2883254149 * value)
	{
		___yAngle_18 = value;
		Il2CppCodeGenWriteBarrier((&___yAngle_18), value);
	}

	inline static int32_t get_offset_of_zAngle_19() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___zAngle_19)); }
	inline FsmFloat_t2883254149 * get_zAngle_19() const { return ___zAngle_19; }
	inline FsmFloat_t2883254149 ** get_address_of_zAngle_19() { return &___zAngle_19; }
	inline void set_zAngle_19(FsmFloat_t2883254149 * value)
	{
		___zAngle_19 = value;
		Il2CppCodeGenWriteBarrier((&___zAngle_19), value);
	}

	inline static int32_t get_offset_of_space_20() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___space_20)); }
	inline int32_t get_space_20() const { return ___space_20; }
	inline int32_t* get_address_of_space_20() { return &___space_20; }
	inline void set_space_20(int32_t value)
	{
		___space_20 = value;
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_22() { return static_cast<int32_t>(offsetof(SetRotation_t3293972810, ___lateUpdate_22)); }
	inline bool get_lateUpdate_22() const { return ___lateUpdate_22; }
	inline bool* get_address_of_lateUpdate_22() { return &___lateUpdate_22; }
	inline void set_lateUpdate_22(bool value)
	{
		___lateUpdate_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETROTATION_T3293972810_H
#ifndef SETSCALE_T1165161303_H
#define SETSCALE_T1165161303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetScale
struct  SetScale_t1165161303  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetScale::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetScale::vector
	FsmVector3_t626444517 * ___vector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetScale::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetScale::y
	FsmFloat_t2883254149 * ___y_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetScale::z
	FsmFloat_t2883254149 * ___z_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetScale::everyFrame
	bool ___everyFrame_19;
	// System.Boolean HutongGames.PlayMaker.Actions.SetScale::lateUpdate
	bool ___lateUpdate_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector_15() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___vector_15)); }
	inline FsmVector3_t626444517 * get_vector_15() const { return ___vector_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector_15() { return &___vector_15; }
	inline void set_vector_15(FsmVector3_t626444517 * value)
	{
		___vector_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_z_18() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___z_18)); }
	inline FsmFloat_t2883254149 * get_z_18() const { return ___z_18; }
	inline FsmFloat_t2883254149 ** get_address_of_z_18() { return &___z_18; }
	inline void set_z_18(FsmFloat_t2883254149 * value)
	{
		___z_18 = value;
		Il2CppCodeGenWriteBarrier((&___z_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_20() { return static_cast<int32_t>(offsetof(SetScale_t1165161303, ___lateUpdate_20)); }
	inline bool get_lateUpdate_20() const { return ___lateUpdate_20; }
	inline bool* get_address_of_lateUpdate_20() { return &___lateUpdate_20; }
	inline void set_lateUpdate_20(bool value)
	{
		___lateUpdate_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETSCALE_T1165161303_H
#ifndef SMOOTHFOLLOWACTION_T772972276_H
#define SMOOTHFOLLOWACTION_T772972276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothFollowAction
struct  SmoothFollowAction_t772972276  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothFollowAction::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::distance
	FsmFloat_t2883254149 * ___distance_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::height
	FsmFloat_t2883254149 * ___height_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::heightDamping
	FsmFloat_t2883254149 * ___heightDamping_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothFollowAction::rotationDamping
	FsmFloat_t2883254149 * ___rotationDamping_19;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::cachedObject
	GameObject_t1113636619 * ___cachedObject_20;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SmoothFollowAction::myTransform
	Transform_t3600365921 * ___myTransform_21;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothFollowAction::cachedTarget
	GameObject_t1113636619 * ___cachedTarget_22;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.SmoothFollowAction::targetTransform
	Transform_t3600365921 * ___targetTransform_23;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_distance_16() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___distance_16)); }
	inline FsmFloat_t2883254149 * get_distance_16() const { return ___distance_16; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_16() { return &___distance_16; }
	inline void set_distance_16(FsmFloat_t2883254149 * value)
	{
		___distance_16 = value;
		Il2CppCodeGenWriteBarrier((&___distance_16), value);
	}

	inline static int32_t get_offset_of_height_17() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___height_17)); }
	inline FsmFloat_t2883254149 * get_height_17() const { return ___height_17; }
	inline FsmFloat_t2883254149 ** get_address_of_height_17() { return &___height_17; }
	inline void set_height_17(FsmFloat_t2883254149 * value)
	{
		___height_17 = value;
		Il2CppCodeGenWriteBarrier((&___height_17), value);
	}

	inline static int32_t get_offset_of_heightDamping_18() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___heightDamping_18)); }
	inline FsmFloat_t2883254149 * get_heightDamping_18() const { return ___heightDamping_18; }
	inline FsmFloat_t2883254149 ** get_address_of_heightDamping_18() { return &___heightDamping_18; }
	inline void set_heightDamping_18(FsmFloat_t2883254149 * value)
	{
		___heightDamping_18 = value;
		Il2CppCodeGenWriteBarrier((&___heightDamping_18), value);
	}

	inline static int32_t get_offset_of_rotationDamping_19() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___rotationDamping_19)); }
	inline FsmFloat_t2883254149 * get_rotationDamping_19() const { return ___rotationDamping_19; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationDamping_19() { return &___rotationDamping_19; }
	inline void set_rotationDamping_19(FsmFloat_t2883254149 * value)
	{
		___rotationDamping_19 = value;
		Il2CppCodeGenWriteBarrier((&___rotationDamping_19), value);
	}

	inline static int32_t get_offset_of_cachedObject_20() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___cachedObject_20)); }
	inline GameObject_t1113636619 * get_cachedObject_20() const { return ___cachedObject_20; }
	inline GameObject_t1113636619 ** get_address_of_cachedObject_20() { return &___cachedObject_20; }
	inline void set_cachedObject_20(GameObject_t1113636619 * value)
	{
		___cachedObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___cachedObject_20), value);
	}

	inline static int32_t get_offset_of_myTransform_21() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___myTransform_21)); }
	inline Transform_t3600365921 * get_myTransform_21() const { return ___myTransform_21; }
	inline Transform_t3600365921 ** get_address_of_myTransform_21() { return &___myTransform_21; }
	inline void set_myTransform_21(Transform_t3600365921 * value)
	{
		___myTransform_21 = value;
		Il2CppCodeGenWriteBarrier((&___myTransform_21), value);
	}

	inline static int32_t get_offset_of_cachedTarget_22() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___cachedTarget_22)); }
	inline GameObject_t1113636619 * get_cachedTarget_22() const { return ___cachedTarget_22; }
	inline GameObject_t1113636619 ** get_address_of_cachedTarget_22() { return &___cachedTarget_22; }
	inline void set_cachedTarget_22(GameObject_t1113636619 * value)
	{
		___cachedTarget_22 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTarget_22), value);
	}

	inline static int32_t get_offset_of_targetTransform_23() { return static_cast<int32_t>(offsetof(SmoothFollowAction_t772972276, ___targetTransform_23)); }
	inline Transform_t3600365921 * get_targetTransform_23() const { return ___targetTransform_23; }
	inline Transform_t3600365921 ** get_address_of_targetTransform_23() { return &___targetTransform_23; }
	inline void set_targetTransform_23(Transform_t3600365921 * value)
	{
		___targetTransform_23 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOWACTION_T772972276_H
#ifndef SMOOTHLOOKAT_T3621050583_H
#define SMOOTHLOOKAT_T3621050583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothLookAt
struct  SmoothLookAt_t3621050583  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothLookAt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SmoothLookAt::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAt::targetPosition
	FsmVector3_t626444517 * ___targetPosition_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAt::upVector
	FsmVector3_t626444517 * ___upVector_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SmoothLookAt::keepVertical
	FsmBool_t163807967 * ___keepVertical_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAt::speed
	FsmFloat_t2883254149 * ___speed_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SmoothLookAt::debug
	FsmBool_t163807967 * ___debug_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAt::finishTolerance
	FsmFloat_t2883254149 * ___finishTolerance_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SmoothLookAt::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_22;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothLookAt::previousGo
	GameObject_t1113636619 * ___previousGo_23;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAt::lastRotation
	Quaternion_t2301928331  ___lastRotation_24;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAt::desiredRotation
	Quaternion_t2301928331  ___desiredRotation_25;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_targetPosition_16() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___targetPosition_16)); }
	inline FsmVector3_t626444517 * get_targetPosition_16() const { return ___targetPosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_16() { return &___targetPosition_16; }
	inline void set_targetPosition_16(FsmVector3_t626444517 * value)
	{
		___targetPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_16), value);
	}

	inline static int32_t get_offset_of_upVector_17() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___upVector_17)); }
	inline FsmVector3_t626444517 * get_upVector_17() const { return ___upVector_17; }
	inline FsmVector3_t626444517 ** get_address_of_upVector_17() { return &___upVector_17; }
	inline void set_upVector_17(FsmVector3_t626444517 * value)
	{
		___upVector_17 = value;
		Il2CppCodeGenWriteBarrier((&___upVector_17), value);
	}

	inline static int32_t get_offset_of_keepVertical_18() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___keepVertical_18)); }
	inline FsmBool_t163807967 * get_keepVertical_18() const { return ___keepVertical_18; }
	inline FsmBool_t163807967 ** get_address_of_keepVertical_18() { return &___keepVertical_18; }
	inline void set_keepVertical_18(FsmBool_t163807967 * value)
	{
		___keepVertical_18 = value;
		Il2CppCodeGenWriteBarrier((&___keepVertical_18), value);
	}

	inline static int32_t get_offset_of_speed_19() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___speed_19)); }
	inline FsmFloat_t2883254149 * get_speed_19() const { return ___speed_19; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_19() { return &___speed_19; }
	inline void set_speed_19(FsmFloat_t2883254149 * value)
	{
		___speed_19 = value;
		Il2CppCodeGenWriteBarrier((&___speed_19), value);
	}

	inline static int32_t get_offset_of_debug_20() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___debug_20)); }
	inline FsmBool_t163807967 * get_debug_20() const { return ___debug_20; }
	inline FsmBool_t163807967 ** get_address_of_debug_20() { return &___debug_20; }
	inline void set_debug_20(FsmBool_t163807967 * value)
	{
		___debug_20 = value;
		Il2CppCodeGenWriteBarrier((&___debug_20), value);
	}

	inline static int32_t get_offset_of_finishTolerance_21() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___finishTolerance_21)); }
	inline FsmFloat_t2883254149 * get_finishTolerance_21() const { return ___finishTolerance_21; }
	inline FsmFloat_t2883254149 ** get_address_of_finishTolerance_21() { return &___finishTolerance_21; }
	inline void set_finishTolerance_21(FsmFloat_t2883254149 * value)
	{
		___finishTolerance_21 = value;
		Il2CppCodeGenWriteBarrier((&___finishTolerance_21), value);
	}

	inline static int32_t get_offset_of_finishEvent_22() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___finishEvent_22)); }
	inline FsmEvent_t3736299882 * get_finishEvent_22() const { return ___finishEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_22() { return &___finishEvent_22; }
	inline void set_finishEvent_22(FsmEvent_t3736299882 * value)
	{
		___finishEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_22), value);
	}

	inline static int32_t get_offset_of_previousGo_23() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___previousGo_23)); }
	inline GameObject_t1113636619 * get_previousGo_23() const { return ___previousGo_23; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_23() { return &___previousGo_23; }
	inline void set_previousGo_23(GameObject_t1113636619 * value)
	{
		___previousGo_23 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_23), value);
	}

	inline static int32_t get_offset_of_lastRotation_24() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___lastRotation_24)); }
	inline Quaternion_t2301928331  get_lastRotation_24() const { return ___lastRotation_24; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_24() { return &___lastRotation_24; }
	inline void set_lastRotation_24(Quaternion_t2301928331  value)
	{
		___lastRotation_24 = value;
	}

	inline static int32_t get_offset_of_desiredRotation_25() { return static_cast<int32_t>(offsetof(SmoothLookAt_t3621050583, ___desiredRotation_25)); }
	inline Quaternion_t2301928331  get_desiredRotation_25() const { return ___desiredRotation_25; }
	inline Quaternion_t2301928331 * get_address_of_desiredRotation_25() { return &___desiredRotation_25; }
	inline void set_desiredRotation_25(Quaternion_t2301928331  value)
	{
		___desiredRotation_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHLOOKAT_T3621050583_H
#ifndef SMOOTHLOOKATDIRECTION_T1378160396_H
#define SMOOTHLOOKATDIRECTION_T1378160396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothLookAtDirection
struct  SmoothLookAtDirection_t1378160396  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothLookAtDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAtDirection::targetDirection
	FsmVector3_t626444517 * ___targetDirection_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAtDirection::minMagnitude
	FsmFloat_t2883254149 * ___minMagnitude_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAtDirection::upVector
	FsmVector3_t626444517 * ___upVector_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SmoothLookAtDirection::keepVertical
	FsmBool_t163807967 * ___keepVertical_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAtDirection::speed
	FsmFloat_t2883254149 * ___speed_19;
	// System.Boolean HutongGames.PlayMaker.Actions.SmoothLookAtDirection::lateUpdate
	bool ___lateUpdate_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SmoothLookAtDirection::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SmoothLookAtDirection::finish
	FsmBool_t163807967 * ___finish_22;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothLookAtDirection::previousGo
	GameObject_t1113636619 * ___previousGo_23;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAtDirection::lastRotation
	Quaternion_t2301928331  ___lastRotation_24;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAtDirection::desiredRotation
	Quaternion_t2301928331  ___desiredRotation_25;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetDirection_15() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___targetDirection_15)); }
	inline FsmVector3_t626444517 * get_targetDirection_15() const { return ___targetDirection_15; }
	inline FsmVector3_t626444517 ** get_address_of_targetDirection_15() { return &___targetDirection_15; }
	inline void set_targetDirection_15(FsmVector3_t626444517 * value)
	{
		___targetDirection_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetDirection_15), value);
	}

	inline static int32_t get_offset_of_minMagnitude_16() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___minMagnitude_16)); }
	inline FsmFloat_t2883254149 * get_minMagnitude_16() const { return ___minMagnitude_16; }
	inline FsmFloat_t2883254149 ** get_address_of_minMagnitude_16() { return &___minMagnitude_16; }
	inline void set_minMagnitude_16(FsmFloat_t2883254149 * value)
	{
		___minMagnitude_16 = value;
		Il2CppCodeGenWriteBarrier((&___minMagnitude_16), value);
	}

	inline static int32_t get_offset_of_upVector_17() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___upVector_17)); }
	inline FsmVector3_t626444517 * get_upVector_17() const { return ___upVector_17; }
	inline FsmVector3_t626444517 ** get_address_of_upVector_17() { return &___upVector_17; }
	inline void set_upVector_17(FsmVector3_t626444517 * value)
	{
		___upVector_17 = value;
		Il2CppCodeGenWriteBarrier((&___upVector_17), value);
	}

	inline static int32_t get_offset_of_keepVertical_18() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___keepVertical_18)); }
	inline FsmBool_t163807967 * get_keepVertical_18() const { return ___keepVertical_18; }
	inline FsmBool_t163807967 ** get_address_of_keepVertical_18() { return &___keepVertical_18; }
	inline void set_keepVertical_18(FsmBool_t163807967 * value)
	{
		___keepVertical_18 = value;
		Il2CppCodeGenWriteBarrier((&___keepVertical_18), value);
	}

	inline static int32_t get_offset_of_speed_19() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___speed_19)); }
	inline FsmFloat_t2883254149 * get_speed_19() const { return ___speed_19; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_19() { return &___speed_19; }
	inline void set_speed_19(FsmFloat_t2883254149 * value)
	{
		___speed_19 = value;
		Il2CppCodeGenWriteBarrier((&___speed_19), value);
	}

	inline static int32_t get_offset_of_lateUpdate_20() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___lateUpdate_20)); }
	inline bool get_lateUpdate_20() const { return ___lateUpdate_20; }
	inline bool* get_address_of_lateUpdate_20() { return &___lateUpdate_20; }
	inline void set_lateUpdate_20(bool value)
	{
		___lateUpdate_20 = value;
	}

	inline static int32_t get_offset_of_finishEvent_21() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___finishEvent_21)); }
	inline FsmEvent_t3736299882 * get_finishEvent_21() const { return ___finishEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_21() { return &___finishEvent_21; }
	inline void set_finishEvent_21(FsmEvent_t3736299882 * value)
	{
		___finishEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_21), value);
	}

	inline static int32_t get_offset_of_finish_22() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___finish_22)); }
	inline FsmBool_t163807967 * get_finish_22() const { return ___finish_22; }
	inline FsmBool_t163807967 ** get_address_of_finish_22() { return &___finish_22; }
	inline void set_finish_22(FsmBool_t163807967 * value)
	{
		___finish_22 = value;
		Il2CppCodeGenWriteBarrier((&___finish_22), value);
	}

	inline static int32_t get_offset_of_previousGo_23() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___previousGo_23)); }
	inline GameObject_t1113636619 * get_previousGo_23() const { return ___previousGo_23; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_23() { return &___previousGo_23; }
	inline void set_previousGo_23(GameObject_t1113636619 * value)
	{
		___previousGo_23 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_23), value);
	}

	inline static int32_t get_offset_of_lastRotation_24() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___lastRotation_24)); }
	inline Quaternion_t2301928331  get_lastRotation_24() const { return ___lastRotation_24; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_24() { return &___lastRotation_24; }
	inline void set_lastRotation_24(Quaternion_t2301928331  value)
	{
		___lastRotation_24 = value;
	}

	inline static int32_t get_offset_of_desiredRotation_25() { return static_cast<int32_t>(offsetof(SmoothLookAtDirection_t1378160396, ___desiredRotation_25)); }
	inline Quaternion_t2301928331  get_desiredRotation_25() const { return ___desiredRotation_25; }
	inline Quaternion_t2301928331 * get_address_of_desiredRotation_25() { return &___desiredRotation_25; }
	inline void set_desiredRotation_25(Quaternion_t2301928331  value)
	{
		___desiredRotation_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHLOOKATDIRECTION_T1378160396_H
#ifndef TRANSFORMDIRECTION_T1398199187_H
#define TRANSFORMDIRECTION_T1398199187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TransformDirection
struct  TransformDirection_t1398199187  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TransformDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformDirection::localDirection
	FsmVector3_t626444517 * ___localDirection_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformDirection::storeResult
	FsmVector3_t626444517 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.TransformDirection::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(TransformDirection_t1398199187, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_localDirection_15() { return static_cast<int32_t>(offsetof(TransformDirection_t1398199187, ___localDirection_15)); }
	inline FsmVector3_t626444517 * get_localDirection_15() const { return ___localDirection_15; }
	inline FsmVector3_t626444517 ** get_address_of_localDirection_15() { return &___localDirection_15; }
	inline void set_localDirection_15(FsmVector3_t626444517 * value)
	{
		___localDirection_15 = value;
		Il2CppCodeGenWriteBarrier((&___localDirection_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(TransformDirection_t1398199187, ___storeResult_16)); }
	inline FsmVector3_t626444517 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmVector3_t626444517 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmVector3_t626444517 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(TransformDirection_t1398199187, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMDIRECTION_T1398199187_H
#ifndef TRANSFORMPOINT_T592873837_H
#define TRANSFORMPOINT_T592873837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TransformPoint
struct  TransformPoint_t592873837  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TransformPoint::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformPoint::localPosition
	FsmVector3_t626444517 * ___localPosition_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformPoint::storeResult
	FsmVector3_t626444517 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.TransformPoint::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(TransformPoint_t592873837, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_localPosition_15() { return static_cast<int32_t>(offsetof(TransformPoint_t592873837, ___localPosition_15)); }
	inline FsmVector3_t626444517 * get_localPosition_15() const { return ___localPosition_15; }
	inline FsmVector3_t626444517 ** get_address_of_localPosition_15() { return &___localPosition_15; }
	inline void set_localPosition_15(FsmVector3_t626444517 * value)
	{
		___localPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___localPosition_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(TransformPoint_t592873837, ___storeResult_16)); }
	inline FsmVector3_t626444517 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmVector3_t626444517 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmVector3_t626444517 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(TransformPoint_t592873837, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMPOINT_T592873837_H
#ifndef TRANSLATE_T2146470009_H
#define TRANSLATE_T2146470009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Translate
struct  Translate_t2146470009  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Translate::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Translate::vector
	FsmVector3_t626444517 * ___vector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::y
	FsmFloat_t2883254149 * ___y_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Translate::z
	FsmFloat_t2883254149 * ___z_18;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.Translate::space
	int32_t ___space_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::perSecond
	bool ___perSecond_20;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::everyFrame
	bool ___everyFrame_21;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::lateUpdate
	bool ___lateUpdate_22;
	// System.Boolean HutongGames.PlayMaker.Actions.Translate::fixedUpdate
	bool ___fixedUpdate_23;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector_15() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___vector_15)); }
	inline FsmVector3_t626444517 * get_vector_15() const { return ___vector_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector_15() { return &___vector_15; }
	inline void set_vector_15(FsmVector3_t626444517 * value)
	{
		___vector_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_z_18() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___z_18)); }
	inline FsmFloat_t2883254149 * get_z_18() const { return ___z_18; }
	inline FsmFloat_t2883254149 ** get_address_of_z_18() { return &___z_18; }
	inline void set_z_18(FsmFloat_t2883254149 * value)
	{
		___z_18 = value;
		Il2CppCodeGenWriteBarrier((&___z_18), value);
	}

	inline static int32_t get_offset_of_space_19() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___space_19)); }
	inline int32_t get_space_19() const { return ___space_19; }
	inline int32_t* get_address_of_space_19() { return &___space_19; }
	inline void set_space_19(int32_t value)
	{
		___space_19 = value;
	}

	inline static int32_t get_offset_of_perSecond_20() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___perSecond_20)); }
	inline bool get_perSecond_20() const { return ___perSecond_20; }
	inline bool* get_address_of_perSecond_20() { return &___perSecond_20; }
	inline void set_perSecond_20(bool value)
	{
		___perSecond_20 = value;
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_22() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___lateUpdate_22)); }
	inline bool get_lateUpdate_22() const { return ___lateUpdate_22; }
	inline bool* get_address_of_lateUpdate_22() { return &___lateUpdate_22; }
	inline void set_lateUpdate_22(bool value)
	{
		___lateUpdate_22 = value;
	}

	inline static int32_t get_offset_of_fixedUpdate_23() { return static_cast<int32_t>(offsetof(Translate_t2146470009, ___fixedUpdate_23)); }
	inline bool get_fixedUpdate_23() const { return ___fixedUpdate_23; }
	inline bool* get_address_of_fixedUpdate_23() { return &___fixedUpdate_23; }
	inline void set_fixedUpdate_23(bool value)
	{
		___fixedUpdate_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATE_T2146470009_H
#ifndef UIBUTTONARRAY_T3593418030_H
#define UIBUTTONARRAY_T3593418030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiButtonArray
struct  UiButtonArray_t3593418030  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiButtonArray::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_14;
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.Actions.UiButtonArray::gameObjects
	FsmGameObjectU5BU5D_t719957643* ___gameObjects_15;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.UiButtonArray::clickEvents
	FsmEventU5BU5D_t2511618479* ___clickEvents_16;
	// UnityEngine.UI.Button[] HutongGames.PlayMaker.Actions.UiButtonArray::buttons
	ButtonU5BU5D_t2297175928* ___buttons_17;
	// UnityEngine.GameObject[] HutongGames.PlayMaker.Actions.UiButtonArray::cachedGameObjects
	GameObjectU5BU5D_t3328599146* ___cachedGameObjects_18;
	// UnityEngine.Events.UnityAction[] HutongGames.PlayMaker.Actions.UiButtonArray::actions
	UnityActionU5BU5D_t1672898414* ___actions_19;
	// System.Int32 HutongGames.PlayMaker.Actions.UiButtonArray::clickedButton
	int32_t ___clickedButton_20;

public:
	inline static int32_t get_offset_of_eventTarget_14() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___eventTarget_14)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_14() const { return ___eventTarget_14; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_14() { return &___eventTarget_14; }
	inline void set_eventTarget_14(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_14), value);
	}

	inline static int32_t get_offset_of_gameObjects_15() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___gameObjects_15)); }
	inline FsmGameObjectU5BU5D_t719957643* get_gameObjects_15() const { return ___gameObjects_15; }
	inline FsmGameObjectU5BU5D_t719957643** get_address_of_gameObjects_15() { return &___gameObjects_15; }
	inline void set_gameObjects_15(FsmGameObjectU5BU5D_t719957643* value)
	{
		___gameObjects_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjects_15), value);
	}

	inline static int32_t get_offset_of_clickEvents_16() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___clickEvents_16)); }
	inline FsmEventU5BU5D_t2511618479* get_clickEvents_16() const { return ___clickEvents_16; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_clickEvents_16() { return &___clickEvents_16; }
	inline void set_clickEvents_16(FsmEventU5BU5D_t2511618479* value)
	{
		___clickEvents_16 = value;
		Il2CppCodeGenWriteBarrier((&___clickEvents_16), value);
	}

	inline static int32_t get_offset_of_buttons_17() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___buttons_17)); }
	inline ButtonU5BU5D_t2297175928* get_buttons_17() const { return ___buttons_17; }
	inline ButtonU5BU5D_t2297175928** get_address_of_buttons_17() { return &___buttons_17; }
	inline void set_buttons_17(ButtonU5BU5D_t2297175928* value)
	{
		___buttons_17 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_17), value);
	}

	inline static int32_t get_offset_of_cachedGameObjects_18() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___cachedGameObjects_18)); }
	inline GameObjectU5BU5D_t3328599146* get_cachedGameObjects_18() const { return ___cachedGameObjects_18; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_cachedGameObjects_18() { return &___cachedGameObjects_18; }
	inline void set_cachedGameObjects_18(GameObjectU5BU5D_t3328599146* value)
	{
		___cachedGameObjects_18 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObjects_18), value);
	}

	inline static int32_t get_offset_of_actions_19() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___actions_19)); }
	inline UnityActionU5BU5D_t1672898414* get_actions_19() const { return ___actions_19; }
	inline UnityActionU5BU5D_t1672898414** get_address_of_actions_19() { return &___actions_19; }
	inline void set_actions_19(UnityActionU5BU5D_t1672898414* value)
	{
		___actions_19 = value;
		Il2CppCodeGenWriteBarrier((&___actions_19), value);
	}

	inline static int32_t get_offset_of_clickedButton_20() { return static_cast<int32_t>(offsetof(UiButtonArray_t3593418030, ___clickedButton_20)); }
	inline int32_t get_clickedButton_20() const { return ___clickedButton_20; }
	inline int32_t* get_address_of_clickedButton_20() { return &___clickedButton_20; }
	inline void set_clickedButton_20(int32_t value)
	{
		___clickedButton_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONARRAY_T3593418030_H
#ifndef UICANVASFORCEUPDATECANVASES_T663998497_H
#define UICANVASFORCEUPDATECANVASES_T663998497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiCanvasForceUpdateCanvases
struct  UiCanvasForceUpdateCanvases_t663998497  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASFORCEUPDATECANVASES_T663998497_H
#ifndef UIEVENTSYSTEMCURRENTRAYCASTALL_T2732382018_H
#define UIEVENTSYSTEMCURRENTRAYCASTALL_T2732382018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll
struct  UiEventSystemCurrentRayCastAll_t2732382018  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::screenPosition
	FsmVector3_t626444517 * ___screenPosition_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::orScreenPosition2d
	FsmVector2_t2965096677 * ___orScreenPosition2d_15;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::gameObjectList
	FsmArray_t1756862219 * ___gameObjectList_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::hitCount
	FsmInt_t874273141 * ___hitCount_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.EventSystems.PointerEventData HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::pointer
	PointerEventData_t3807901092 * ___pointer_19;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HutongGames.PlayMaker.Actions.UiEventSystemCurrentRayCastAll::raycastResults
	List_1_t537414295 * ___raycastResults_20;

public:
	inline static int32_t get_offset_of_screenPosition_14() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___screenPosition_14)); }
	inline FsmVector3_t626444517 * get_screenPosition_14() const { return ___screenPosition_14; }
	inline FsmVector3_t626444517 ** get_address_of_screenPosition_14() { return &___screenPosition_14; }
	inline void set_screenPosition_14(FsmVector3_t626444517 * value)
	{
		___screenPosition_14 = value;
		Il2CppCodeGenWriteBarrier((&___screenPosition_14), value);
	}

	inline static int32_t get_offset_of_orScreenPosition2d_15() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___orScreenPosition2d_15)); }
	inline FsmVector2_t2965096677 * get_orScreenPosition2d_15() const { return ___orScreenPosition2d_15; }
	inline FsmVector2_t2965096677 ** get_address_of_orScreenPosition2d_15() { return &___orScreenPosition2d_15; }
	inline void set_orScreenPosition2d_15(FsmVector2_t2965096677 * value)
	{
		___orScreenPosition2d_15 = value;
		Il2CppCodeGenWriteBarrier((&___orScreenPosition2d_15), value);
	}

	inline static int32_t get_offset_of_gameObjectList_16() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___gameObjectList_16)); }
	inline FsmArray_t1756862219 * get_gameObjectList_16() const { return ___gameObjectList_16; }
	inline FsmArray_t1756862219 ** get_address_of_gameObjectList_16() { return &___gameObjectList_16; }
	inline void set_gameObjectList_16(FsmArray_t1756862219 * value)
	{
		___gameObjectList_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectList_16), value);
	}

	inline static int32_t get_offset_of_hitCount_17() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___hitCount_17)); }
	inline FsmInt_t874273141 * get_hitCount_17() const { return ___hitCount_17; }
	inline FsmInt_t874273141 ** get_address_of_hitCount_17() { return &___hitCount_17; }
	inline void set_hitCount_17(FsmInt_t874273141 * value)
	{
		___hitCount_17 = value;
		Il2CppCodeGenWriteBarrier((&___hitCount_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_pointer_19() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___pointer_19)); }
	inline PointerEventData_t3807901092 * get_pointer_19() const { return ___pointer_19; }
	inline PointerEventData_t3807901092 ** get_address_of_pointer_19() { return &___pointer_19; }
	inline void set_pointer_19(PointerEventData_t3807901092 * value)
	{
		___pointer_19 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_19), value);
	}

	inline static int32_t get_offset_of_raycastResults_20() { return static_cast<int32_t>(offsetof(UiEventSystemCurrentRayCastAll_t2732382018, ___raycastResults_20)); }
	inline List_1_t537414295 * get_raycastResults_20() const { return ___raycastResults_20; }
	inline List_1_t537414295 ** get_address_of_raycastResults_20() { return &___raycastResults_20; }
	inline void set_raycastResults_20(List_1_t537414295 * value)
	{
		___raycastResults_20 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTSYSTEMCURRENTRAYCASTALL_T2732382018_H
#ifndef UIEVENTSYSTEMEXECUTEEVENT_T4239363109_H
#define UIEVENTSYSTEMEXECUTEEVENT_T4239363109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent
struct  UiEventSystemExecuteEvent_t4239363109  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent::eventHandler
	FsmEnum_t2861764163 * ___eventHandler_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent::success
	FsmEvent_t3736299882 * ___success_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent::canNotHandleEvent
	FsmEvent_t3736299882 * ___canNotHandleEvent_17;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.UiEventSystemExecuteEvent::go
	GameObject_t1113636619 * ___go_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(UiEventSystemExecuteEvent_t4239363109, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_eventHandler_15() { return static_cast<int32_t>(offsetof(UiEventSystemExecuteEvent_t4239363109, ___eventHandler_15)); }
	inline FsmEnum_t2861764163 * get_eventHandler_15() const { return ___eventHandler_15; }
	inline FsmEnum_t2861764163 ** get_address_of_eventHandler_15() { return &___eventHandler_15; }
	inline void set_eventHandler_15(FsmEnum_t2861764163 * value)
	{
		___eventHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_15), value);
	}

	inline static int32_t get_offset_of_success_16() { return static_cast<int32_t>(offsetof(UiEventSystemExecuteEvent_t4239363109, ___success_16)); }
	inline FsmEvent_t3736299882 * get_success_16() const { return ___success_16; }
	inline FsmEvent_t3736299882 ** get_address_of_success_16() { return &___success_16; }
	inline void set_success_16(FsmEvent_t3736299882 * value)
	{
		___success_16 = value;
		Il2CppCodeGenWriteBarrier((&___success_16), value);
	}

	inline static int32_t get_offset_of_canNotHandleEvent_17() { return static_cast<int32_t>(offsetof(UiEventSystemExecuteEvent_t4239363109, ___canNotHandleEvent_17)); }
	inline FsmEvent_t3736299882 * get_canNotHandleEvent_17() const { return ___canNotHandleEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_canNotHandleEvent_17() { return &___canNotHandleEvent_17; }
	inline void set_canNotHandleEvent_17(FsmEvent_t3736299882 * value)
	{
		___canNotHandleEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___canNotHandleEvent_17), value);
	}

	inline static int32_t get_offset_of_go_18() { return static_cast<int32_t>(offsetof(UiEventSystemExecuteEvent_t4239363109, ___go_18)); }
	inline GameObject_t1113636619 * get_go_18() const { return ___go_18; }
	inline GameObject_t1113636619 ** get_address_of_go_18() { return &___go_18; }
	inline void set_go_18(GameObject_t1113636619 * value)
	{
		___go_18 = value;
		Il2CppCodeGenWriteBarrier((&___go_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTSYSTEMEXECUTEEVENT_T4239363109_H
#ifndef UIGETLASTPOINTERDATAINFO_T95606296_H
#define UIGETLASTPOINTERDATAINFO_T95606296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo
struct  UiGetLastPointerDataInfo_t95606296  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::clickCount
	FsmInt_t874273141 * ___clickCount_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::clickTime
	FsmFloat_t2883254149 * ___clickTime_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::delta
	FsmVector2_t2965096677 * ___delta_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::dragging
	FsmBool_t163807967 * ___dragging_18;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::inputButton
	FsmEnum_t2861764163 * ___inputButton_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::eligibleForClick
	FsmBool_t163807967 * ___eligibleForClick_20;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::enterEventCamera
	FsmGameObject_t3581898942 * ___enterEventCamera_21;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::pressEventCamera
	FsmGameObject_t3581898942 * ___pressEventCamera_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::isPointerMoving
	FsmBool_t163807967 * ___isPointerMoving_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::isScrolling
	FsmBool_t163807967 * ___isScrolling_24;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::lastPress
	FsmGameObject_t3581898942 * ___lastPress_25;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::pointerDrag
	FsmGameObject_t3581898942 * ___pointerDrag_26;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::pointerEnter
	FsmGameObject_t3581898942 * ___pointerEnter_27;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::pointerId
	FsmInt_t874273141 * ___pointerId_28;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::pointerPress
	FsmGameObject_t3581898942 * ___pointerPress_29;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::position
	FsmVector2_t2965096677 * ___position_30;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::pressPosition
	FsmVector2_t2965096677 * ___pressPosition_31;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::rawPointerPress
	FsmGameObject_t3581898942 * ___rawPointerPress_32;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::scrollDelta
	FsmVector2_t2965096677 * ___scrollDelta_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::used
	FsmBool_t163807967 * ___used_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::useDragThreshold
	FsmBool_t163807967 * ___useDragThreshold_35;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::worldNormal
	FsmVector3_t626444517 * ___worldNormal_36;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::worldPosition
	FsmVector3_t626444517 * ___worldPosition_37;

public:
	inline static int32_t get_offset_of_clickCount_15() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___clickCount_15)); }
	inline FsmInt_t874273141 * get_clickCount_15() const { return ___clickCount_15; }
	inline FsmInt_t874273141 ** get_address_of_clickCount_15() { return &___clickCount_15; }
	inline void set_clickCount_15(FsmInt_t874273141 * value)
	{
		___clickCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___clickCount_15), value);
	}

	inline static int32_t get_offset_of_clickTime_16() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___clickTime_16)); }
	inline FsmFloat_t2883254149 * get_clickTime_16() const { return ___clickTime_16; }
	inline FsmFloat_t2883254149 ** get_address_of_clickTime_16() { return &___clickTime_16; }
	inline void set_clickTime_16(FsmFloat_t2883254149 * value)
	{
		___clickTime_16 = value;
		Il2CppCodeGenWriteBarrier((&___clickTime_16), value);
	}

	inline static int32_t get_offset_of_delta_17() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___delta_17)); }
	inline FsmVector2_t2965096677 * get_delta_17() const { return ___delta_17; }
	inline FsmVector2_t2965096677 ** get_address_of_delta_17() { return &___delta_17; }
	inline void set_delta_17(FsmVector2_t2965096677 * value)
	{
		___delta_17 = value;
		Il2CppCodeGenWriteBarrier((&___delta_17), value);
	}

	inline static int32_t get_offset_of_dragging_18() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___dragging_18)); }
	inline FsmBool_t163807967 * get_dragging_18() const { return ___dragging_18; }
	inline FsmBool_t163807967 ** get_address_of_dragging_18() { return &___dragging_18; }
	inline void set_dragging_18(FsmBool_t163807967 * value)
	{
		___dragging_18 = value;
		Il2CppCodeGenWriteBarrier((&___dragging_18), value);
	}

	inline static int32_t get_offset_of_inputButton_19() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___inputButton_19)); }
	inline FsmEnum_t2861764163 * get_inputButton_19() const { return ___inputButton_19; }
	inline FsmEnum_t2861764163 ** get_address_of_inputButton_19() { return &___inputButton_19; }
	inline void set_inputButton_19(FsmEnum_t2861764163 * value)
	{
		___inputButton_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputButton_19), value);
	}

	inline static int32_t get_offset_of_eligibleForClick_20() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___eligibleForClick_20)); }
	inline FsmBool_t163807967 * get_eligibleForClick_20() const { return ___eligibleForClick_20; }
	inline FsmBool_t163807967 ** get_address_of_eligibleForClick_20() { return &___eligibleForClick_20; }
	inline void set_eligibleForClick_20(FsmBool_t163807967 * value)
	{
		___eligibleForClick_20 = value;
		Il2CppCodeGenWriteBarrier((&___eligibleForClick_20), value);
	}

	inline static int32_t get_offset_of_enterEventCamera_21() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___enterEventCamera_21)); }
	inline FsmGameObject_t3581898942 * get_enterEventCamera_21() const { return ___enterEventCamera_21; }
	inline FsmGameObject_t3581898942 ** get_address_of_enterEventCamera_21() { return &___enterEventCamera_21; }
	inline void set_enterEventCamera_21(FsmGameObject_t3581898942 * value)
	{
		___enterEventCamera_21 = value;
		Il2CppCodeGenWriteBarrier((&___enterEventCamera_21), value);
	}

	inline static int32_t get_offset_of_pressEventCamera_22() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___pressEventCamera_22)); }
	inline FsmGameObject_t3581898942 * get_pressEventCamera_22() const { return ___pressEventCamera_22; }
	inline FsmGameObject_t3581898942 ** get_address_of_pressEventCamera_22() { return &___pressEventCamera_22; }
	inline void set_pressEventCamera_22(FsmGameObject_t3581898942 * value)
	{
		___pressEventCamera_22 = value;
		Il2CppCodeGenWriteBarrier((&___pressEventCamera_22), value);
	}

	inline static int32_t get_offset_of_isPointerMoving_23() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___isPointerMoving_23)); }
	inline FsmBool_t163807967 * get_isPointerMoving_23() const { return ___isPointerMoving_23; }
	inline FsmBool_t163807967 ** get_address_of_isPointerMoving_23() { return &___isPointerMoving_23; }
	inline void set_isPointerMoving_23(FsmBool_t163807967 * value)
	{
		___isPointerMoving_23 = value;
		Il2CppCodeGenWriteBarrier((&___isPointerMoving_23), value);
	}

	inline static int32_t get_offset_of_isScrolling_24() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___isScrolling_24)); }
	inline FsmBool_t163807967 * get_isScrolling_24() const { return ___isScrolling_24; }
	inline FsmBool_t163807967 ** get_address_of_isScrolling_24() { return &___isScrolling_24; }
	inline void set_isScrolling_24(FsmBool_t163807967 * value)
	{
		___isScrolling_24 = value;
		Il2CppCodeGenWriteBarrier((&___isScrolling_24), value);
	}

	inline static int32_t get_offset_of_lastPress_25() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___lastPress_25)); }
	inline FsmGameObject_t3581898942 * get_lastPress_25() const { return ___lastPress_25; }
	inline FsmGameObject_t3581898942 ** get_address_of_lastPress_25() { return &___lastPress_25; }
	inline void set_lastPress_25(FsmGameObject_t3581898942 * value)
	{
		___lastPress_25 = value;
		Il2CppCodeGenWriteBarrier((&___lastPress_25), value);
	}

	inline static int32_t get_offset_of_pointerDrag_26() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___pointerDrag_26)); }
	inline FsmGameObject_t3581898942 * get_pointerDrag_26() const { return ___pointerDrag_26; }
	inline FsmGameObject_t3581898942 ** get_address_of_pointerDrag_26() { return &___pointerDrag_26; }
	inline void set_pointerDrag_26(FsmGameObject_t3581898942 * value)
	{
		___pointerDrag_26 = value;
		Il2CppCodeGenWriteBarrier((&___pointerDrag_26), value);
	}

	inline static int32_t get_offset_of_pointerEnter_27() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___pointerEnter_27)); }
	inline FsmGameObject_t3581898942 * get_pointerEnter_27() const { return ___pointerEnter_27; }
	inline FsmGameObject_t3581898942 ** get_address_of_pointerEnter_27() { return &___pointerEnter_27; }
	inline void set_pointerEnter_27(FsmGameObject_t3581898942 * value)
	{
		___pointerEnter_27 = value;
		Il2CppCodeGenWriteBarrier((&___pointerEnter_27), value);
	}

	inline static int32_t get_offset_of_pointerId_28() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___pointerId_28)); }
	inline FsmInt_t874273141 * get_pointerId_28() const { return ___pointerId_28; }
	inline FsmInt_t874273141 ** get_address_of_pointerId_28() { return &___pointerId_28; }
	inline void set_pointerId_28(FsmInt_t874273141 * value)
	{
		___pointerId_28 = value;
		Il2CppCodeGenWriteBarrier((&___pointerId_28), value);
	}

	inline static int32_t get_offset_of_pointerPress_29() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___pointerPress_29)); }
	inline FsmGameObject_t3581898942 * get_pointerPress_29() const { return ___pointerPress_29; }
	inline FsmGameObject_t3581898942 ** get_address_of_pointerPress_29() { return &___pointerPress_29; }
	inline void set_pointerPress_29(FsmGameObject_t3581898942 * value)
	{
		___pointerPress_29 = value;
		Il2CppCodeGenWriteBarrier((&___pointerPress_29), value);
	}

	inline static int32_t get_offset_of_position_30() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___position_30)); }
	inline FsmVector2_t2965096677 * get_position_30() const { return ___position_30; }
	inline FsmVector2_t2965096677 ** get_address_of_position_30() { return &___position_30; }
	inline void set_position_30(FsmVector2_t2965096677 * value)
	{
		___position_30 = value;
		Il2CppCodeGenWriteBarrier((&___position_30), value);
	}

	inline static int32_t get_offset_of_pressPosition_31() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___pressPosition_31)); }
	inline FsmVector2_t2965096677 * get_pressPosition_31() const { return ___pressPosition_31; }
	inline FsmVector2_t2965096677 ** get_address_of_pressPosition_31() { return &___pressPosition_31; }
	inline void set_pressPosition_31(FsmVector2_t2965096677 * value)
	{
		___pressPosition_31 = value;
		Il2CppCodeGenWriteBarrier((&___pressPosition_31), value);
	}

	inline static int32_t get_offset_of_rawPointerPress_32() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___rawPointerPress_32)); }
	inline FsmGameObject_t3581898942 * get_rawPointerPress_32() const { return ___rawPointerPress_32; }
	inline FsmGameObject_t3581898942 ** get_address_of_rawPointerPress_32() { return &___rawPointerPress_32; }
	inline void set_rawPointerPress_32(FsmGameObject_t3581898942 * value)
	{
		___rawPointerPress_32 = value;
		Il2CppCodeGenWriteBarrier((&___rawPointerPress_32), value);
	}

	inline static int32_t get_offset_of_scrollDelta_33() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___scrollDelta_33)); }
	inline FsmVector2_t2965096677 * get_scrollDelta_33() const { return ___scrollDelta_33; }
	inline FsmVector2_t2965096677 ** get_address_of_scrollDelta_33() { return &___scrollDelta_33; }
	inline void set_scrollDelta_33(FsmVector2_t2965096677 * value)
	{
		___scrollDelta_33 = value;
		Il2CppCodeGenWriteBarrier((&___scrollDelta_33), value);
	}

	inline static int32_t get_offset_of_used_34() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___used_34)); }
	inline FsmBool_t163807967 * get_used_34() const { return ___used_34; }
	inline FsmBool_t163807967 ** get_address_of_used_34() { return &___used_34; }
	inline void set_used_34(FsmBool_t163807967 * value)
	{
		___used_34 = value;
		Il2CppCodeGenWriteBarrier((&___used_34), value);
	}

	inline static int32_t get_offset_of_useDragThreshold_35() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___useDragThreshold_35)); }
	inline FsmBool_t163807967 * get_useDragThreshold_35() const { return ___useDragThreshold_35; }
	inline FsmBool_t163807967 ** get_address_of_useDragThreshold_35() { return &___useDragThreshold_35; }
	inline void set_useDragThreshold_35(FsmBool_t163807967 * value)
	{
		___useDragThreshold_35 = value;
		Il2CppCodeGenWriteBarrier((&___useDragThreshold_35), value);
	}

	inline static int32_t get_offset_of_worldNormal_36() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___worldNormal_36)); }
	inline FsmVector3_t626444517 * get_worldNormal_36() const { return ___worldNormal_36; }
	inline FsmVector3_t626444517 ** get_address_of_worldNormal_36() { return &___worldNormal_36; }
	inline void set_worldNormal_36(FsmVector3_t626444517 * value)
	{
		___worldNormal_36 = value;
		Il2CppCodeGenWriteBarrier((&___worldNormal_36), value);
	}

	inline static int32_t get_offset_of_worldPosition_37() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296, ___worldPosition_37)); }
	inline FsmVector3_t626444517 * get_worldPosition_37() const { return ___worldPosition_37; }
	inline FsmVector3_t626444517 ** get_address_of_worldPosition_37() { return &___worldPosition_37; }
	inline void set_worldPosition_37(FsmVector3_t626444517 * value)
	{
		___worldPosition_37 = value;
		Il2CppCodeGenWriteBarrier((&___worldPosition_37), value);
	}
};

struct UiGetLastPointerDataInfo_t95606296_StaticFields
{
public:
	// UnityEngine.EventSystems.PointerEventData HutongGames.PlayMaker.Actions.UiGetLastPointerDataInfo::lastPointerEventData
	PointerEventData_t3807901092 * ___lastPointerEventData_14;

public:
	inline static int32_t get_offset_of_lastPointerEventData_14() { return static_cast<int32_t>(offsetof(UiGetLastPointerDataInfo_t95606296_StaticFields, ___lastPointerEventData_14)); }
	inline PointerEventData_t3807901092 * get_lastPointerEventData_14() const { return ___lastPointerEventData_14; }
	inline PointerEventData_t3807901092 ** get_address_of_lastPointerEventData_14() { return &___lastPointerEventData_14; }
	inline void set_lastPointerEventData_14(PointerEventData_t3807901092 * value)
	{
		___lastPointerEventData_14 = value;
		Il2CppCodeGenWriteBarrier((&___lastPointerEventData_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGETLASTPOINTERDATAINFO_T95606296_H
#ifndef UIGETLASTPOINTEREVENTDATAINPUTBUTTON_T3342924654_H
#define UIGETLASTPOINTEREVENTDATAINPUTBUTTON_T3342924654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGetLastPointerEventDataInputButton
struct  UiGetLastPointerEventDataInputButton_t3342924654  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.UiGetLastPointerEventDataInputButton::inputButton
	FsmEnum_t2861764163 * ___inputButton_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiGetLastPointerEventDataInputButton::leftClick
	FsmEvent_t3736299882 * ___leftClick_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiGetLastPointerEventDataInputButton::middleClick
	FsmEvent_t3736299882 * ___middleClick_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiGetLastPointerEventDataInputButton::rightClick
	FsmEvent_t3736299882 * ___rightClick_17;

public:
	inline static int32_t get_offset_of_inputButton_14() { return static_cast<int32_t>(offsetof(UiGetLastPointerEventDataInputButton_t3342924654, ___inputButton_14)); }
	inline FsmEnum_t2861764163 * get_inputButton_14() const { return ___inputButton_14; }
	inline FsmEnum_t2861764163 ** get_address_of_inputButton_14() { return &___inputButton_14; }
	inline void set_inputButton_14(FsmEnum_t2861764163 * value)
	{
		___inputButton_14 = value;
		Il2CppCodeGenWriteBarrier((&___inputButton_14), value);
	}

	inline static int32_t get_offset_of_leftClick_15() { return static_cast<int32_t>(offsetof(UiGetLastPointerEventDataInputButton_t3342924654, ___leftClick_15)); }
	inline FsmEvent_t3736299882 * get_leftClick_15() const { return ___leftClick_15; }
	inline FsmEvent_t3736299882 ** get_address_of_leftClick_15() { return &___leftClick_15; }
	inline void set_leftClick_15(FsmEvent_t3736299882 * value)
	{
		___leftClick_15 = value;
		Il2CppCodeGenWriteBarrier((&___leftClick_15), value);
	}

	inline static int32_t get_offset_of_middleClick_16() { return static_cast<int32_t>(offsetof(UiGetLastPointerEventDataInputButton_t3342924654, ___middleClick_16)); }
	inline FsmEvent_t3736299882 * get_middleClick_16() const { return ___middleClick_16; }
	inline FsmEvent_t3736299882 ** get_address_of_middleClick_16() { return &___middleClick_16; }
	inline void set_middleClick_16(FsmEvent_t3736299882 * value)
	{
		___middleClick_16 = value;
		Il2CppCodeGenWriteBarrier((&___middleClick_16), value);
	}

	inline static int32_t get_offset_of_rightClick_17() { return static_cast<int32_t>(offsetof(UiGetLastPointerEventDataInputButton_t3342924654, ___rightClick_17)); }
	inline FsmEvent_t3736299882 * get_rightClick_17() const { return ___rightClick_17; }
	inline FsmEvent_t3736299882 ** get_address_of_rightClick_17() { return &___rightClick_17; }
	inline void set_rightClick_17(FsmEvent_t3736299882 * value)
	{
		___rightClick_17 = value;
		Il2CppCodeGenWriteBarrier((&___rightClick_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGETLASTPOINTEREVENTDATAINPUTBUTTON_T3342924654_H
#ifndef UIGETSELECTEDGAMEOBJECT_T2739701632_H
#define UIGETSELECTEDGAMEOBJECT_T2739701632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGetSelectedGameObject
struct  UiGetSelectedGameObject_t2739701632  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiGetSelectedGameObject::StoreGameObject
	FsmGameObject_t3581898942 * ___StoreGameObject_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiGetSelectedGameObject::ObjectChangedEvent
	FsmEvent_t3736299882 * ___ObjectChangedEvent_15;
	// System.Boolean HutongGames.PlayMaker.Actions.UiGetSelectedGameObject::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.UiGetSelectedGameObject::lastGameObject
	GameObject_t1113636619 * ___lastGameObject_17;

public:
	inline static int32_t get_offset_of_StoreGameObject_14() { return static_cast<int32_t>(offsetof(UiGetSelectedGameObject_t2739701632, ___StoreGameObject_14)); }
	inline FsmGameObject_t3581898942 * get_StoreGameObject_14() const { return ___StoreGameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_StoreGameObject_14() { return &___StoreGameObject_14; }
	inline void set_StoreGameObject_14(FsmGameObject_t3581898942 * value)
	{
		___StoreGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___StoreGameObject_14), value);
	}

	inline static int32_t get_offset_of_ObjectChangedEvent_15() { return static_cast<int32_t>(offsetof(UiGetSelectedGameObject_t2739701632, ___ObjectChangedEvent_15)); }
	inline FsmEvent_t3736299882 * get_ObjectChangedEvent_15() const { return ___ObjectChangedEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_ObjectChangedEvent_15() { return &___ObjectChangedEvent_15; }
	inline void set_ObjectChangedEvent_15(FsmEvent_t3736299882 * value)
	{
		___ObjectChangedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectChangedEvent_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(UiGetSelectedGameObject_t2739701632, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_lastGameObject_17() { return static_cast<int32_t>(offsetof(UiGetSelectedGameObject_t2739701632, ___lastGameObject_17)); }
	inline GameObject_t1113636619 * get_lastGameObject_17() const { return ___lastGameObject_17; }
	inline GameObject_t1113636619 ** get_address_of_lastGameObject_17() { return &___lastGameObject_17; }
	inline void set_lastGameObject_17(GameObject_t1113636619 * value)
	{
		___lastGameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___lastGameObject_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGETSELECTEDGAMEOBJECT_T2739701632_H
#ifndef UIISPOINTEROVERUIOBJECT_T3566716892_H
#define UIISPOINTEROVERUIOBJECT_T3566716892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiIsPointerOverUiObject
struct  UiIsPointerOverUiObject_t3566716892  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiIsPointerOverUiObject::pointerId
	FsmInt_t874273141 * ___pointerId_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiIsPointerOverUiObject::pointerOverUI
	FsmEvent_t3736299882 * ___pointerOverUI_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiIsPointerOverUiObject::pointerNotOverUI
	FsmEvent_t3736299882 * ___pointerNotOverUI_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiIsPointerOverUiObject::isPointerOverUI
	FsmBool_t163807967 * ___isPointerOverUI_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiIsPointerOverUiObject::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_pointerId_14() { return static_cast<int32_t>(offsetof(UiIsPointerOverUiObject_t3566716892, ___pointerId_14)); }
	inline FsmInt_t874273141 * get_pointerId_14() const { return ___pointerId_14; }
	inline FsmInt_t874273141 ** get_address_of_pointerId_14() { return &___pointerId_14; }
	inline void set_pointerId_14(FsmInt_t874273141 * value)
	{
		___pointerId_14 = value;
		Il2CppCodeGenWriteBarrier((&___pointerId_14), value);
	}

	inline static int32_t get_offset_of_pointerOverUI_15() { return static_cast<int32_t>(offsetof(UiIsPointerOverUiObject_t3566716892, ___pointerOverUI_15)); }
	inline FsmEvent_t3736299882 * get_pointerOverUI_15() const { return ___pointerOverUI_15; }
	inline FsmEvent_t3736299882 ** get_address_of_pointerOverUI_15() { return &___pointerOverUI_15; }
	inline void set_pointerOverUI_15(FsmEvent_t3736299882 * value)
	{
		___pointerOverUI_15 = value;
		Il2CppCodeGenWriteBarrier((&___pointerOverUI_15), value);
	}

	inline static int32_t get_offset_of_pointerNotOverUI_16() { return static_cast<int32_t>(offsetof(UiIsPointerOverUiObject_t3566716892, ___pointerNotOverUI_16)); }
	inline FsmEvent_t3736299882 * get_pointerNotOverUI_16() const { return ___pointerNotOverUI_16; }
	inline FsmEvent_t3736299882 ** get_address_of_pointerNotOverUI_16() { return &___pointerNotOverUI_16; }
	inline void set_pointerNotOverUI_16(FsmEvent_t3736299882 * value)
	{
		___pointerNotOverUI_16 = value;
		Il2CppCodeGenWriteBarrier((&___pointerNotOverUI_16), value);
	}

	inline static int32_t get_offset_of_isPointerOverUI_17() { return static_cast<int32_t>(offsetof(UiIsPointerOverUiObject_t3566716892, ___isPointerOverUI_17)); }
	inline FsmBool_t163807967 * get_isPointerOverUI_17() const { return ___isPointerOverUI_17; }
	inline FsmBool_t163807967 ** get_address_of_isPointerOverUI_17() { return &___isPointerOverUI_17; }
	inline void set_isPointerOverUI_17(FsmBool_t163807967 * value)
	{
		___isPointerOverUI_17 = value;
		Il2CppCodeGenWriteBarrier((&___isPointerOverUI_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiIsPointerOverUiObject_t3566716892, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIISPOINTEROVERUIOBJECT_T3566716892_H
#ifndef UISETISINTERACTABLE_T3806359819_H
#define UISETISINTERACTABLE_T3806359819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSetIsInteractable
struct  UiSetIsInteractable_t3806359819  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSetIsInteractable::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSetIsInteractable::isInteractable
	FsmBool_t163807967 * ___isInteractable_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSetIsInteractable::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_16;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiSetIsInteractable::_selectable
	Selectable_t3250028441 * ____selectable_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSetIsInteractable::_originalState
	bool ____originalState_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(UiSetIsInteractable_t3806359819, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_isInteractable_15() { return static_cast<int32_t>(offsetof(UiSetIsInteractable_t3806359819, ___isInteractable_15)); }
	inline FsmBool_t163807967 * get_isInteractable_15() const { return ___isInteractable_15; }
	inline FsmBool_t163807967 ** get_address_of_isInteractable_15() { return &___isInteractable_15; }
	inline void set_isInteractable_15(FsmBool_t163807967 * value)
	{
		___isInteractable_15 = value;
		Il2CppCodeGenWriteBarrier((&___isInteractable_15), value);
	}

	inline static int32_t get_offset_of_resetOnExit_16() { return static_cast<int32_t>(offsetof(UiSetIsInteractable_t3806359819, ___resetOnExit_16)); }
	inline FsmBool_t163807967 * get_resetOnExit_16() const { return ___resetOnExit_16; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_16() { return &___resetOnExit_16; }
	inline void set_resetOnExit_16(FsmBool_t163807967 * value)
	{
		___resetOnExit_16 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_16), value);
	}

	inline static int32_t get_offset_of__selectable_17() { return static_cast<int32_t>(offsetof(UiSetIsInteractable_t3806359819, ____selectable_17)); }
	inline Selectable_t3250028441 * get__selectable_17() const { return ____selectable_17; }
	inline Selectable_t3250028441 ** get_address_of__selectable_17() { return &____selectable_17; }
	inline void set__selectable_17(Selectable_t3250028441 * value)
	{
		____selectable_17 = value;
		Il2CppCodeGenWriteBarrier((&____selectable_17), value);
	}

	inline static int32_t get_offset_of__originalState_18() { return static_cast<int32_t>(offsetof(UiSetIsInteractable_t3806359819, ____originalState_18)); }
	inline bool get__originalState_18() const { return ____originalState_18; }
	inline bool* get_address_of__originalState_18() { return &____originalState_18; }
	inline void set__originalState_18(bool value)
	{
		____originalState_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISETISINTERACTABLE_T3806359819_H
#ifndef UISETSELECTEDGAMEOBJECT_T2602666268_H
#define UISETSELECTEDGAMEOBJECT_T2602666268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSetSelectedGameObject
struct  UiSetSelectedGameObject_t2602666268  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiSetSelectedGameObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(UiSetSelectedGameObject_t2602666268, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISETSELECTEDGAMEOBJECT_T2602666268_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef EVENTTRIGGERACTIONBASE_T72041308_H
#define EVENTTRIGGERACTIONBASE_T72041308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EventTriggerActionBase
struct  EventTriggerActionBase_t72041308  : public ComponentAction_1_t3953371731
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EventTriggerActionBase::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.EventTriggerActionBase::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// UnityEngine.EventSystems.EventTrigger HutongGames.PlayMaker.Actions.EventTriggerActionBase::trigger
	EventTrigger_t1076084509 * ___trigger_18;
	// UnityEngine.EventSystems.EventTrigger/Entry HutongGames.PlayMaker.Actions.EventTriggerActionBase::entry
	Entry_t3344766165 * ___entry_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(EventTriggerActionBase_t72041308, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(EventTriggerActionBase_t72041308, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_trigger_18() { return static_cast<int32_t>(offsetof(EventTriggerActionBase_t72041308, ___trigger_18)); }
	inline EventTrigger_t1076084509 * get_trigger_18() const { return ___trigger_18; }
	inline EventTrigger_t1076084509 ** get_address_of_trigger_18() { return &___trigger_18; }
	inline void set_trigger_18(EventTrigger_t1076084509 * value)
	{
		___trigger_18 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_18), value);
	}

	inline static int32_t get_offset_of_entry_19() { return static_cast<int32_t>(offsetof(EventTriggerActionBase_t72041308, ___entry_19)); }
	inline Entry_t3344766165 * get_entry_19() const { return ___entry_19; }
	inline Entry_t3344766165 ** get_address_of_entry_19() { return &___entry_19; }
	inline void set_entry_19(Entry_t3344766165 * value)
	{
		___entry_19 = value;
		Il2CppCodeGenWriteBarrier((&___entry_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGERACTIONBASE_T72041308_H
#ifndef UIBUTTONONCLICKEVENT_T3361402451_H
#define UIBUTTONONCLICKEVENT_T3361402451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiButtonOnClickEvent
struct  UiButtonOnClickEvent_t3361402451  : public ComponentAction_1_t2637352395
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiButtonOnClickEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.UiButtonOnClickEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiButtonOnClickEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_18;
	// UnityEngine.UI.Button HutongGames.PlayMaker.Actions.UiButtonOnClickEvent::button
	Button_t4055032469 * ___button_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiButtonOnClickEvent_t3361402451, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_eventTarget_17() { return static_cast<int32_t>(offsetof(UiButtonOnClickEvent_t3361402451, ___eventTarget_17)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_17() const { return ___eventTarget_17; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_17() { return &___eventTarget_17; }
	inline void set_eventTarget_17(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_17 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_17), value);
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(UiButtonOnClickEvent_t3361402451, ___sendEvent_18)); }
	inline FsmEvent_t3736299882 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t3736299882 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_18), value);
	}

	inline static int32_t get_offset_of_button_19() { return static_cast<int32_t>(offsetof(UiButtonOnClickEvent_t3361402451, ___button_19)); }
	inline Button_t4055032469 * get_button_19() const { return ___button_19; }
	inline Button_t4055032469 ** get_address_of_button_19() { return &___button_19; }
	inline void set_button_19(Button_t4055032469 * value)
	{
		___button_19 = value;
		Il2CppCodeGenWriteBarrier((&___button_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBUTTONONCLICKEVENT_T3361402451_H
#ifndef UICANVASENABLERAYCAST_T3530533341_H
#define UICANVASENABLERAYCAST_T3530533341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast
struct  UiCanvasEnableRaycast_t3530533341  : public ComponentAction_1_t1655560330
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast::enableRaycasting
	FsmBool_t163807967 * ___enableRaycasting_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast::everyFrame
	bool ___everyFrame_19;
	// HutongGames.PlayMaker.PlayMakerCanvasRaycastFilterProxy HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast::raycastFilterProxy
	PlayMakerCanvasRaycastFilterProxy_t3073240404 * ___raycastFilterProxy_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasEnableRaycast::originalValue
	bool ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiCanvasEnableRaycast_t3530533341, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_enableRaycasting_17() { return static_cast<int32_t>(offsetof(UiCanvasEnableRaycast_t3530533341, ___enableRaycasting_17)); }
	inline FsmBool_t163807967 * get_enableRaycasting_17() const { return ___enableRaycasting_17; }
	inline FsmBool_t163807967 ** get_address_of_enableRaycasting_17() { return &___enableRaycasting_17; }
	inline void set_enableRaycasting_17(FsmBool_t163807967 * value)
	{
		___enableRaycasting_17 = value;
		Il2CppCodeGenWriteBarrier((&___enableRaycasting_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiCanvasEnableRaycast_t3530533341, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiCanvasEnableRaycast_t3530533341, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_raycastFilterProxy_20() { return static_cast<int32_t>(offsetof(UiCanvasEnableRaycast_t3530533341, ___raycastFilterProxy_20)); }
	inline PlayMakerCanvasRaycastFilterProxy_t3073240404 * get_raycastFilterProxy_20() const { return ___raycastFilterProxy_20; }
	inline PlayMakerCanvasRaycastFilterProxy_t3073240404 ** get_address_of_raycastFilterProxy_20() { return &___raycastFilterProxy_20; }
	inline void set_raycastFilterProxy_20(PlayMakerCanvasRaycastFilterProxy_t3073240404 * value)
	{
		___raycastFilterProxy_20 = value;
		Il2CppCodeGenWriteBarrier((&___raycastFilterProxy_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiCanvasEnableRaycast_t3530533341, ___originalValue_21)); }
	inline bool get_originalValue_21() const { return ___originalValue_21; }
	inline bool* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(bool value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASENABLERAYCAST_T3530533341_H
#ifndef UICANVASGROUPSETALPHA_T3687891700_H
#define UICANVASGROUPSETALPHA_T3687891700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha
struct  UiCanvasGroupSetAlpha_t3687891700  : public ComponentAction_1_t2665831686
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha::alpha
	FsmFloat_t2883254149 * ___alpha_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.CanvasGroup HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha::component
	CanvasGroup_t4083511760 * ___component_20;
	// System.Single HutongGames.PlayMaker.Actions.UiCanvasGroupSetAlpha::originalValue
	float ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetAlpha_t3687891700, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_alpha_17() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetAlpha_t3687891700, ___alpha_17)); }
	inline FsmFloat_t2883254149 * get_alpha_17() const { return ___alpha_17; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_17() { return &___alpha_17; }
	inline void set_alpha_17(FsmFloat_t2883254149 * value)
	{
		___alpha_17 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetAlpha_t3687891700, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetAlpha_t3687891700, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_component_20() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetAlpha_t3687891700, ___component_20)); }
	inline CanvasGroup_t4083511760 * get_component_20() const { return ___component_20; }
	inline CanvasGroup_t4083511760 ** get_address_of_component_20() { return &___component_20; }
	inline void set_component_20(CanvasGroup_t4083511760 * value)
	{
		___component_20 = value;
		Il2CppCodeGenWriteBarrier((&___component_20), value);
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetAlpha_t3687891700, ___originalValue_21)); }
	inline float get_originalValue_21() const { return ___originalValue_21; }
	inline float* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(float value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASGROUPSETALPHA_T3687891700_H
#ifndef UICANVASGROUPSETPROPERTIES_T1011590200_H
#define UICANVASGROUPSETPROPERTIES_T1011590200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties
struct  UiCanvasGroupSetProperties_t1011590200  : public ComponentAction_1_t2665831686
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::alpha
	FsmFloat_t2883254149 * ___alpha_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::interactable
	FsmBool_t163807967 * ___interactable_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::blocksRaycasts
	FsmBool_t163807967 * ___blocksRaycasts_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::ignoreParentGroup
	FsmBool_t163807967 * ___ignoreParentGroup_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_21;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::everyFrame
	bool ___everyFrame_22;
	// UnityEngine.CanvasGroup HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::component
	CanvasGroup_t4083511760 * ___component_23;
	// System.Single HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::originalAlpha
	float ___originalAlpha_24;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::originalInteractable
	bool ___originalInteractable_25;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::originalBlocksRaycasts
	bool ___originalBlocksRaycasts_26;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasGroupSetProperties::originalIgnoreParentGroup
	bool ___originalIgnoreParentGroup_27;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_alpha_17() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___alpha_17)); }
	inline FsmFloat_t2883254149 * get_alpha_17() const { return ___alpha_17; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_17() { return &___alpha_17; }
	inline void set_alpha_17(FsmFloat_t2883254149 * value)
	{
		___alpha_17 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_17), value);
	}

	inline static int32_t get_offset_of_interactable_18() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___interactable_18)); }
	inline FsmBool_t163807967 * get_interactable_18() const { return ___interactable_18; }
	inline FsmBool_t163807967 ** get_address_of_interactable_18() { return &___interactable_18; }
	inline void set_interactable_18(FsmBool_t163807967 * value)
	{
		___interactable_18 = value;
		Il2CppCodeGenWriteBarrier((&___interactable_18), value);
	}

	inline static int32_t get_offset_of_blocksRaycasts_19() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___blocksRaycasts_19)); }
	inline FsmBool_t163807967 * get_blocksRaycasts_19() const { return ___blocksRaycasts_19; }
	inline FsmBool_t163807967 ** get_address_of_blocksRaycasts_19() { return &___blocksRaycasts_19; }
	inline void set_blocksRaycasts_19(FsmBool_t163807967 * value)
	{
		___blocksRaycasts_19 = value;
		Il2CppCodeGenWriteBarrier((&___blocksRaycasts_19), value);
	}

	inline static int32_t get_offset_of_ignoreParentGroup_20() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___ignoreParentGroup_20)); }
	inline FsmBool_t163807967 * get_ignoreParentGroup_20() const { return ___ignoreParentGroup_20; }
	inline FsmBool_t163807967 ** get_address_of_ignoreParentGroup_20() { return &___ignoreParentGroup_20; }
	inline void set_ignoreParentGroup_20(FsmBool_t163807967 * value)
	{
		___ignoreParentGroup_20 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreParentGroup_20), value);
	}

	inline static int32_t get_offset_of_resetOnExit_21() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___resetOnExit_21)); }
	inline FsmBool_t163807967 * get_resetOnExit_21() const { return ___resetOnExit_21; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_21() { return &___resetOnExit_21; }
	inline void set_resetOnExit_21(FsmBool_t163807967 * value)
	{
		___resetOnExit_21 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}

	inline static int32_t get_offset_of_component_23() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___component_23)); }
	inline CanvasGroup_t4083511760 * get_component_23() const { return ___component_23; }
	inline CanvasGroup_t4083511760 ** get_address_of_component_23() { return &___component_23; }
	inline void set_component_23(CanvasGroup_t4083511760 * value)
	{
		___component_23 = value;
		Il2CppCodeGenWriteBarrier((&___component_23), value);
	}

	inline static int32_t get_offset_of_originalAlpha_24() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___originalAlpha_24)); }
	inline float get_originalAlpha_24() const { return ___originalAlpha_24; }
	inline float* get_address_of_originalAlpha_24() { return &___originalAlpha_24; }
	inline void set_originalAlpha_24(float value)
	{
		___originalAlpha_24 = value;
	}

	inline static int32_t get_offset_of_originalInteractable_25() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___originalInteractable_25)); }
	inline bool get_originalInteractable_25() const { return ___originalInteractable_25; }
	inline bool* get_address_of_originalInteractable_25() { return &___originalInteractable_25; }
	inline void set_originalInteractable_25(bool value)
	{
		___originalInteractable_25 = value;
	}

	inline static int32_t get_offset_of_originalBlocksRaycasts_26() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___originalBlocksRaycasts_26)); }
	inline bool get_originalBlocksRaycasts_26() const { return ___originalBlocksRaycasts_26; }
	inline bool* get_address_of_originalBlocksRaycasts_26() { return &___originalBlocksRaycasts_26; }
	inline void set_originalBlocksRaycasts_26(bool value)
	{
		___originalBlocksRaycasts_26 = value;
	}

	inline static int32_t get_offset_of_originalIgnoreParentGroup_27() { return static_cast<int32_t>(offsetof(UiCanvasGroupSetProperties_t1011590200, ___originalIgnoreParentGroup_27)); }
	inline bool get_originalIgnoreParentGroup_27() const { return ___originalIgnoreParentGroup_27; }
	inline bool* get_address_of_originalIgnoreParentGroup_27() { return &___originalIgnoreParentGroup_27; }
	inline void set_originalIgnoreParentGroup_27(bool value)
	{
		___originalIgnoreParentGroup_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASGROUPSETPROPERTIES_T1011590200_H
#ifndef UICANVASSCALERGETSCALEFACTOR_T2797552755_H
#define UICANVASSCALERGETSCALEFACTOR_T2797552755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiCanvasScalerGetScaleFactor
struct  UiCanvasScalerGetScaleFactor_t2797552755  : public ComponentAction_1_t1350299881
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiCanvasScalerGetScaleFactor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiCanvasScalerGetScaleFactor::scaleFactor
	FsmFloat_t2883254149 * ___scaleFactor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasScalerGetScaleFactor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.CanvasScaler HutongGames.PlayMaker.Actions.UiCanvasScalerGetScaleFactor::component
	CanvasScaler_t2767979955 * ___component_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiCanvasScalerGetScaleFactor_t2797552755, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_scaleFactor_17() { return static_cast<int32_t>(offsetof(UiCanvasScalerGetScaleFactor_t2797552755, ___scaleFactor_17)); }
	inline FsmFloat_t2883254149 * get_scaleFactor_17() const { return ___scaleFactor_17; }
	inline FsmFloat_t2883254149 ** get_address_of_scaleFactor_17() { return &___scaleFactor_17; }
	inline void set_scaleFactor_17(FsmFloat_t2883254149 * value)
	{
		___scaleFactor_17 = value;
		Il2CppCodeGenWriteBarrier((&___scaleFactor_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiCanvasScalerGetScaleFactor_t2797552755, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_component_19() { return static_cast<int32_t>(offsetof(UiCanvasScalerGetScaleFactor_t2797552755, ___component_19)); }
	inline CanvasScaler_t2767979955 * get_component_19() const { return ___component_19; }
	inline CanvasScaler_t2767979955 ** get_address_of_component_19() { return &___component_19; }
	inline void set_component_19(CanvasScaler_t2767979955 * value)
	{
		___component_19 = value;
		Il2CppCodeGenWriteBarrier((&___component_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASSCALERGETSCALEFACTOR_T2797552755_H
#ifndef UICANVASSCALERSETSCALEFACTOR_T2796641695_H
#define UICANVASSCALERSETSCALEFACTOR_T2796641695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiCanvasScalerSetScaleFactor
struct  UiCanvasScalerSetScaleFactor_t2796641695  : public ComponentAction_1_t1350299881
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiCanvasScalerSetScaleFactor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiCanvasScalerSetScaleFactor::scaleFactor
	FsmFloat_t2883254149 * ___scaleFactor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiCanvasScalerSetScaleFactor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.CanvasScaler HutongGames.PlayMaker.Actions.UiCanvasScalerSetScaleFactor::component
	CanvasScaler_t2767979955 * ___component_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiCanvasScalerSetScaleFactor_t2796641695, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_scaleFactor_17() { return static_cast<int32_t>(offsetof(UiCanvasScalerSetScaleFactor_t2796641695, ___scaleFactor_17)); }
	inline FsmFloat_t2883254149 * get_scaleFactor_17() const { return ___scaleFactor_17; }
	inline FsmFloat_t2883254149 ** get_address_of_scaleFactor_17() { return &___scaleFactor_17; }
	inline void set_scaleFactor_17(FsmFloat_t2883254149 * value)
	{
		___scaleFactor_17 = value;
		Il2CppCodeGenWriteBarrier((&___scaleFactor_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiCanvasScalerSetScaleFactor_t2796641695, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_component_19() { return static_cast<int32_t>(offsetof(UiCanvasScalerSetScaleFactor_t2796641695, ___component_19)); }
	inline CanvasScaler_t2767979955 * get_component_19() const { return ___component_19; }
	inline CanvasScaler_t2767979955 ** get_address_of_component_19() { return &___component_19; }
	inline void set_component_19(CanvasScaler_t2767979955 * value)
	{
		___component_19 = value;
		Il2CppCodeGenWriteBarrier((&___component_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICANVASSCALERSETSCALEFACTOR_T2796641695_H
#ifndef UIDROPDOWNADDOPTIONS_T3285147849_H
#define UIDROPDOWNADDOPTIONS_T3285147849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiDropDownAddOptions
struct  UiDropDownAddOptions_t3285147849  : public ComponentAction_1_t856711151
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiDropDownAddOptions::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.UiDropDownAddOptions::optionText
	FsmStringU5BU5D_t252501805* ___optionText_17;
	// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.Actions.UiDropDownAddOptions::optionImage
	FsmObjectU5BU5D_t635502296* ___optionImage_18;
	// UnityEngine.UI.Dropdown HutongGames.PlayMaker.Actions.UiDropDownAddOptions::dropDown
	Dropdown_t2274391225 * ___dropDown_19;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> HutongGames.PlayMaker.Actions.UiDropDownAddOptions::options
	List_1_t447389798 * ___options_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiDropDownAddOptions_t3285147849, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_optionText_17() { return static_cast<int32_t>(offsetof(UiDropDownAddOptions_t3285147849, ___optionText_17)); }
	inline FsmStringU5BU5D_t252501805* get_optionText_17() const { return ___optionText_17; }
	inline FsmStringU5BU5D_t252501805** get_address_of_optionText_17() { return &___optionText_17; }
	inline void set_optionText_17(FsmStringU5BU5D_t252501805* value)
	{
		___optionText_17 = value;
		Il2CppCodeGenWriteBarrier((&___optionText_17), value);
	}

	inline static int32_t get_offset_of_optionImage_18() { return static_cast<int32_t>(offsetof(UiDropDownAddOptions_t3285147849, ___optionImage_18)); }
	inline FsmObjectU5BU5D_t635502296* get_optionImage_18() const { return ___optionImage_18; }
	inline FsmObjectU5BU5D_t635502296** get_address_of_optionImage_18() { return &___optionImage_18; }
	inline void set_optionImage_18(FsmObjectU5BU5D_t635502296* value)
	{
		___optionImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___optionImage_18), value);
	}

	inline static int32_t get_offset_of_dropDown_19() { return static_cast<int32_t>(offsetof(UiDropDownAddOptions_t3285147849, ___dropDown_19)); }
	inline Dropdown_t2274391225 * get_dropDown_19() const { return ___dropDown_19; }
	inline Dropdown_t2274391225 ** get_address_of_dropDown_19() { return &___dropDown_19; }
	inline void set_dropDown_19(Dropdown_t2274391225 * value)
	{
		___dropDown_19 = value;
		Il2CppCodeGenWriteBarrier((&___dropDown_19), value);
	}

	inline static int32_t get_offset_of_options_20() { return static_cast<int32_t>(offsetof(UiDropDownAddOptions_t3285147849, ___options_20)); }
	inline List_1_t447389798 * get_options_20() const { return ___options_20; }
	inline List_1_t447389798 ** get_address_of_options_20() { return &___options_20; }
	inline void set_options_20(List_1_t447389798 * value)
	{
		___options_20 = value;
		Il2CppCodeGenWriteBarrier((&___options_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDROPDOWNADDOPTIONS_T3285147849_H
#ifndef UIDROPDOWNCLEAROPTIONS_T3929997459_H
#define UIDROPDOWNCLEAROPTIONS_T3929997459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiDropDownClearOptions
struct  UiDropDownClearOptions_t3929997459  : public ComponentAction_1_t856711151
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiDropDownClearOptions::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.UI.Dropdown HutongGames.PlayMaker.Actions.UiDropDownClearOptions::dropDown
	Dropdown_t2274391225 * ___dropDown_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiDropDownClearOptions_t3929997459, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_dropDown_17() { return static_cast<int32_t>(offsetof(UiDropDownClearOptions_t3929997459, ___dropDown_17)); }
	inline Dropdown_t2274391225 * get_dropDown_17() const { return ___dropDown_17; }
	inline Dropdown_t2274391225 ** get_address_of_dropDown_17() { return &___dropDown_17; }
	inline void set_dropDown_17(Dropdown_t2274391225 * value)
	{
		___dropDown_17 = value;
		Il2CppCodeGenWriteBarrier((&___dropDown_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDROPDOWNCLEAROPTIONS_T3929997459_H
#ifndef UIDROPDOWNGETSELECTEDDATA_T4113433095_H
#define UIDROPDOWNGETSELECTEDDATA_T4113433095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData
struct  UiDropDownGetSelectedData_t4113433095  : public ComponentAction_1_t856711151
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData::index
	FsmInt_t874273141 * ___index_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData::getText
	FsmString_t1785915204 * ___getText_18;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData::getImage
	FsmObject_t2606870197 * ___getImage_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData::everyFrame
	bool ___everyFrame_20;
	// UnityEngine.UI.Dropdown HutongGames.PlayMaker.Actions.UiDropDownGetSelectedData::dropDown
	Dropdown_t2274391225 * ___dropDown_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiDropDownGetSelectedData_t4113433095, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_index_17() { return static_cast<int32_t>(offsetof(UiDropDownGetSelectedData_t4113433095, ___index_17)); }
	inline FsmInt_t874273141 * get_index_17() const { return ___index_17; }
	inline FsmInt_t874273141 ** get_address_of_index_17() { return &___index_17; }
	inline void set_index_17(FsmInt_t874273141 * value)
	{
		___index_17 = value;
		Il2CppCodeGenWriteBarrier((&___index_17), value);
	}

	inline static int32_t get_offset_of_getText_18() { return static_cast<int32_t>(offsetof(UiDropDownGetSelectedData_t4113433095, ___getText_18)); }
	inline FsmString_t1785915204 * get_getText_18() const { return ___getText_18; }
	inline FsmString_t1785915204 ** get_address_of_getText_18() { return &___getText_18; }
	inline void set_getText_18(FsmString_t1785915204 * value)
	{
		___getText_18 = value;
		Il2CppCodeGenWriteBarrier((&___getText_18), value);
	}

	inline static int32_t get_offset_of_getImage_19() { return static_cast<int32_t>(offsetof(UiDropDownGetSelectedData_t4113433095, ___getImage_19)); }
	inline FsmObject_t2606870197 * get_getImage_19() const { return ___getImage_19; }
	inline FsmObject_t2606870197 ** get_address_of_getImage_19() { return &___getImage_19; }
	inline void set_getImage_19(FsmObject_t2606870197 * value)
	{
		___getImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___getImage_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(UiDropDownGetSelectedData_t4113433095, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of_dropDown_21() { return static_cast<int32_t>(offsetof(UiDropDownGetSelectedData_t4113433095, ___dropDown_21)); }
	inline Dropdown_t2274391225 * get_dropDown_21() const { return ___dropDown_21; }
	inline Dropdown_t2274391225 ** get_address_of_dropDown_21() { return &___dropDown_21; }
	inline void set_dropDown_21(Dropdown_t2274391225 * value)
	{
		___dropDown_21 = value;
		Il2CppCodeGenWriteBarrier((&___dropDown_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDROPDOWNGETSELECTEDDATA_T4113433095_H
#ifndef UIDROPDOWNSETVALUE_T1878988810_H
#define UIDROPDOWNSETVALUE_T1878988810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiDropDownSetValue
struct  UiDropDownSetValue_t1878988810  : public ComponentAction_1_t856711151
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiDropDownSetValue::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiDropDownSetValue::value
	FsmInt_t874273141 * ___value_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiDropDownSetValue::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Dropdown HutongGames.PlayMaker.Actions.UiDropDownSetValue::dropDown
	Dropdown_t2274391225 * ___dropDown_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiDropDownSetValue_t1878988810, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_value_17() { return static_cast<int32_t>(offsetof(UiDropDownSetValue_t1878988810, ___value_17)); }
	inline FsmInt_t874273141 * get_value_17() const { return ___value_17; }
	inline FsmInt_t874273141 ** get_address_of_value_17() { return &___value_17; }
	inline void set_value_17(FsmInt_t874273141 * value)
	{
		___value_17 = value;
		Il2CppCodeGenWriteBarrier((&___value_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiDropDownSetValue_t1878988810, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_dropDown_19() { return static_cast<int32_t>(offsetof(UiDropDownSetValue_t1878988810, ___dropDown_19)); }
	inline Dropdown_t2274391225 * get_dropDown_19() const { return ___dropDown_19; }
	inline Dropdown_t2274391225 ** get_address_of_dropDown_19() { return &___dropDown_19; }
	inline void set_dropDown_19(Dropdown_t2274391225 * value)
	{
		___dropDown_19 = value;
		Il2CppCodeGenWriteBarrier((&___dropDown_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDROPDOWNSETVALUE_T1878988810_H
#ifndef UIGETCOLORBLOCK_T2505395705_H
#define UIGETCOLORBLOCK_T2505395705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGetColorBlock
struct  UiGetColorBlock_t2505395705  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiGetColorBlock::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGetColorBlock::fadeDuration
	FsmFloat_t2883254149 * ___fadeDuration_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGetColorBlock::colorMultiplier
	FsmFloat_t2883254149 * ___colorMultiplier_18;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGetColorBlock::normalColor
	FsmColor_t1738900188 * ___normalColor_19;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGetColorBlock::pressedColor
	FsmColor_t1738900188 * ___pressedColor_20;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGetColorBlock::highlightedColor
	FsmColor_t1738900188 * ___highlightedColor_21;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGetColorBlock::disabledColor
	FsmColor_t1738900188 * ___disabledColor_22;
	// System.Boolean HutongGames.PlayMaker.Actions.UiGetColorBlock::everyFrame
	bool ___everyFrame_23;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiGetColorBlock::selectable
	Selectable_t3250028441 * ___selectable_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_fadeDuration_17() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___fadeDuration_17)); }
	inline FsmFloat_t2883254149 * get_fadeDuration_17() const { return ___fadeDuration_17; }
	inline FsmFloat_t2883254149 ** get_address_of_fadeDuration_17() { return &___fadeDuration_17; }
	inline void set_fadeDuration_17(FsmFloat_t2883254149 * value)
	{
		___fadeDuration_17 = value;
		Il2CppCodeGenWriteBarrier((&___fadeDuration_17), value);
	}

	inline static int32_t get_offset_of_colorMultiplier_18() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___colorMultiplier_18)); }
	inline FsmFloat_t2883254149 * get_colorMultiplier_18() const { return ___colorMultiplier_18; }
	inline FsmFloat_t2883254149 ** get_address_of_colorMultiplier_18() { return &___colorMultiplier_18; }
	inline void set_colorMultiplier_18(FsmFloat_t2883254149 * value)
	{
		___colorMultiplier_18 = value;
		Il2CppCodeGenWriteBarrier((&___colorMultiplier_18), value);
	}

	inline static int32_t get_offset_of_normalColor_19() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___normalColor_19)); }
	inline FsmColor_t1738900188 * get_normalColor_19() const { return ___normalColor_19; }
	inline FsmColor_t1738900188 ** get_address_of_normalColor_19() { return &___normalColor_19; }
	inline void set_normalColor_19(FsmColor_t1738900188 * value)
	{
		___normalColor_19 = value;
		Il2CppCodeGenWriteBarrier((&___normalColor_19), value);
	}

	inline static int32_t get_offset_of_pressedColor_20() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___pressedColor_20)); }
	inline FsmColor_t1738900188 * get_pressedColor_20() const { return ___pressedColor_20; }
	inline FsmColor_t1738900188 ** get_address_of_pressedColor_20() { return &___pressedColor_20; }
	inline void set_pressedColor_20(FsmColor_t1738900188 * value)
	{
		___pressedColor_20 = value;
		Il2CppCodeGenWriteBarrier((&___pressedColor_20), value);
	}

	inline static int32_t get_offset_of_highlightedColor_21() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___highlightedColor_21)); }
	inline FsmColor_t1738900188 * get_highlightedColor_21() const { return ___highlightedColor_21; }
	inline FsmColor_t1738900188 ** get_address_of_highlightedColor_21() { return &___highlightedColor_21; }
	inline void set_highlightedColor_21(FsmColor_t1738900188 * value)
	{
		___highlightedColor_21 = value;
		Il2CppCodeGenWriteBarrier((&___highlightedColor_21), value);
	}

	inline static int32_t get_offset_of_disabledColor_22() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___disabledColor_22)); }
	inline FsmColor_t1738900188 * get_disabledColor_22() const { return ___disabledColor_22; }
	inline FsmColor_t1738900188 ** get_address_of_disabledColor_22() { return &___disabledColor_22; }
	inline void set_disabledColor_22(FsmColor_t1738900188 * value)
	{
		___disabledColor_22 = value;
		Il2CppCodeGenWriteBarrier((&___disabledColor_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}

	inline static int32_t get_offset_of_selectable_24() { return static_cast<int32_t>(offsetof(UiGetColorBlock_t2505395705, ___selectable_24)); }
	inline Selectable_t3250028441 * get_selectable_24() const { return ___selectable_24; }
	inline Selectable_t3250028441 ** get_address_of_selectable_24() { return &___selectable_24; }
	inline void set_selectable_24(Selectable_t3250028441 * value)
	{
		___selectable_24 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGETCOLORBLOCK_T2505395705_H
#ifndef UIGETISINTERACTABLE_T3788033727_H
#define UIGETISINTERACTABLE_T3788033727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGetIsInteractable
struct  UiGetIsInteractable_t3788033727  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiGetIsInteractable::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGetIsInteractable::isInteractable
	FsmBool_t163807967 * ___isInteractable_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiGetIsInteractable::isInteractableEvent
	FsmEvent_t3736299882 * ___isInteractableEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiGetIsInteractable::isNotInteractableEvent
	FsmEvent_t3736299882 * ___isNotInteractableEvent_19;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiGetIsInteractable::selectable
	Selectable_t3250028441 * ___selectable_20;
	// System.Boolean HutongGames.PlayMaker.Actions.UiGetIsInteractable::originalState
	bool ___originalState_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiGetIsInteractable_t3788033727, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_isInteractable_17() { return static_cast<int32_t>(offsetof(UiGetIsInteractable_t3788033727, ___isInteractable_17)); }
	inline FsmBool_t163807967 * get_isInteractable_17() const { return ___isInteractable_17; }
	inline FsmBool_t163807967 ** get_address_of_isInteractable_17() { return &___isInteractable_17; }
	inline void set_isInteractable_17(FsmBool_t163807967 * value)
	{
		___isInteractable_17 = value;
		Il2CppCodeGenWriteBarrier((&___isInteractable_17), value);
	}

	inline static int32_t get_offset_of_isInteractableEvent_18() { return static_cast<int32_t>(offsetof(UiGetIsInteractable_t3788033727, ___isInteractableEvent_18)); }
	inline FsmEvent_t3736299882 * get_isInteractableEvent_18() const { return ___isInteractableEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_isInteractableEvent_18() { return &___isInteractableEvent_18; }
	inline void set_isInteractableEvent_18(FsmEvent_t3736299882 * value)
	{
		___isInteractableEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___isInteractableEvent_18), value);
	}

	inline static int32_t get_offset_of_isNotInteractableEvent_19() { return static_cast<int32_t>(offsetof(UiGetIsInteractable_t3788033727, ___isNotInteractableEvent_19)); }
	inline FsmEvent_t3736299882 * get_isNotInteractableEvent_19() const { return ___isNotInteractableEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotInteractableEvent_19() { return &___isNotInteractableEvent_19; }
	inline void set_isNotInteractableEvent_19(FsmEvent_t3736299882 * value)
	{
		___isNotInteractableEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isNotInteractableEvent_19), value);
	}

	inline static int32_t get_offset_of_selectable_20() { return static_cast<int32_t>(offsetof(UiGetIsInteractable_t3788033727, ___selectable_20)); }
	inline Selectable_t3250028441 * get_selectable_20() const { return ___selectable_20; }
	inline Selectable_t3250028441 ** get_address_of_selectable_20() { return &___selectable_20; }
	inline void set_selectable_20(Selectable_t3250028441 * value)
	{
		___selectable_20 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_20), value);
	}

	inline static int32_t get_offset_of_originalState_21() { return static_cast<int32_t>(offsetof(UiGetIsInteractable_t3788033727, ___originalState_21)); }
	inline bool get_originalState_21() const { return ___originalState_21; }
	inline bool* get_address_of_originalState_21() { return &___originalState_21; }
	inline void set_originalState_21(bool value)
	{
		___originalState_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGETISINTERACTABLE_T3788033727_H
#ifndef UIGRAPHICCROSSFADEALPHA_T1243273689_H
#define UIGRAPHICCROSSFADEALPHA_T1243273689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGraphicCrossFadeAlpha
struct  UiGraphicCrossFadeAlpha_t1243273689  : public ComponentAction_1_t242655537
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiGraphicCrossFadeAlpha::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeAlpha::alpha
	FsmFloat_t2883254149 * ___alpha_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeAlpha::duration
	FsmFloat_t2883254149 * ___duration_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGraphicCrossFadeAlpha::ignoreTimeScale
	FsmBool_t163807967 * ___ignoreTimeScale_19;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.UiGraphicCrossFadeAlpha::uiComponent
	Graphic_t1660335611 * ___uiComponent_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeAlpha_t1243273689, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_alpha_17() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeAlpha_t1243273689, ___alpha_17)); }
	inline FsmFloat_t2883254149 * get_alpha_17() const { return ___alpha_17; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_17() { return &___alpha_17; }
	inline void set_alpha_17(FsmFloat_t2883254149 * value)
	{
		___alpha_17 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_17), value);
	}

	inline static int32_t get_offset_of_duration_18() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeAlpha_t1243273689, ___duration_18)); }
	inline FsmFloat_t2883254149 * get_duration_18() const { return ___duration_18; }
	inline FsmFloat_t2883254149 ** get_address_of_duration_18() { return &___duration_18; }
	inline void set_duration_18(FsmFloat_t2883254149 * value)
	{
		___duration_18 = value;
		Il2CppCodeGenWriteBarrier((&___duration_18), value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_19() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeAlpha_t1243273689, ___ignoreTimeScale_19)); }
	inline FsmBool_t163807967 * get_ignoreTimeScale_19() const { return ___ignoreTimeScale_19; }
	inline FsmBool_t163807967 ** get_address_of_ignoreTimeScale_19() { return &___ignoreTimeScale_19; }
	inline void set_ignoreTimeScale_19(FsmBool_t163807967 * value)
	{
		___ignoreTimeScale_19 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreTimeScale_19), value);
	}

	inline static int32_t get_offset_of_uiComponent_20() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeAlpha_t1243273689, ___uiComponent_20)); }
	inline Graphic_t1660335611 * get_uiComponent_20() const { return ___uiComponent_20; }
	inline Graphic_t1660335611 ** get_address_of_uiComponent_20() { return &___uiComponent_20; }
	inline void set_uiComponent_20(Graphic_t1660335611 * value)
	{
		___uiComponent_20 = value;
		Il2CppCodeGenWriteBarrier((&___uiComponent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRAPHICCROSSFADEALPHA_T1243273689_H
#ifndef UIGRAPHICCROSSFADECOLOR_T1236708496_H
#define UIGRAPHICCROSSFADECOLOR_T1236708496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor
struct  UiGraphicCrossFadeColor_t1236708496  : public ComponentAction_1_t242655537
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::color
	FsmColor_t1738900188 * ___color_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::red
	FsmFloat_t2883254149 * ___red_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::green
	FsmFloat_t2883254149 * ___green_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::blue
	FsmFloat_t2883254149 * ___blue_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::alpha
	FsmFloat_t2883254149 * ___alpha_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::duration
	FsmFloat_t2883254149 * ___duration_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::ignoreTimeScale
	FsmBool_t163807967 * ___ignoreTimeScale_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::useAlpha
	FsmBool_t163807967 * ___useAlpha_24;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.UiGraphicCrossFadeColor::uiComponent
	Graphic_t1660335611 * ___uiComponent_25;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_color_17() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___color_17)); }
	inline FsmColor_t1738900188 * get_color_17() const { return ___color_17; }
	inline FsmColor_t1738900188 ** get_address_of_color_17() { return &___color_17; }
	inline void set_color_17(FsmColor_t1738900188 * value)
	{
		___color_17 = value;
		Il2CppCodeGenWriteBarrier((&___color_17), value);
	}

	inline static int32_t get_offset_of_red_18() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___red_18)); }
	inline FsmFloat_t2883254149 * get_red_18() const { return ___red_18; }
	inline FsmFloat_t2883254149 ** get_address_of_red_18() { return &___red_18; }
	inline void set_red_18(FsmFloat_t2883254149 * value)
	{
		___red_18 = value;
		Il2CppCodeGenWriteBarrier((&___red_18), value);
	}

	inline static int32_t get_offset_of_green_19() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___green_19)); }
	inline FsmFloat_t2883254149 * get_green_19() const { return ___green_19; }
	inline FsmFloat_t2883254149 ** get_address_of_green_19() { return &___green_19; }
	inline void set_green_19(FsmFloat_t2883254149 * value)
	{
		___green_19 = value;
		Il2CppCodeGenWriteBarrier((&___green_19), value);
	}

	inline static int32_t get_offset_of_blue_20() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___blue_20)); }
	inline FsmFloat_t2883254149 * get_blue_20() const { return ___blue_20; }
	inline FsmFloat_t2883254149 ** get_address_of_blue_20() { return &___blue_20; }
	inline void set_blue_20(FsmFloat_t2883254149 * value)
	{
		___blue_20 = value;
		Il2CppCodeGenWriteBarrier((&___blue_20), value);
	}

	inline static int32_t get_offset_of_alpha_21() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___alpha_21)); }
	inline FsmFloat_t2883254149 * get_alpha_21() const { return ___alpha_21; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_21() { return &___alpha_21; }
	inline void set_alpha_21(FsmFloat_t2883254149 * value)
	{
		___alpha_21 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_21), value);
	}

	inline static int32_t get_offset_of_duration_22() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___duration_22)); }
	inline FsmFloat_t2883254149 * get_duration_22() const { return ___duration_22; }
	inline FsmFloat_t2883254149 ** get_address_of_duration_22() { return &___duration_22; }
	inline void set_duration_22(FsmFloat_t2883254149 * value)
	{
		___duration_22 = value;
		Il2CppCodeGenWriteBarrier((&___duration_22), value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_23() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___ignoreTimeScale_23)); }
	inline FsmBool_t163807967 * get_ignoreTimeScale_23() const { return ___ignoreTimeScale_23; }
	inline FsmBool_t163807967 ** get_address_of_ignoreTimeScale_23() { return &___ignoreTimeScale_23; }
	inline void set_ignoreTimeScale_23(FsmBool_t163807967 * value)
	{
		___ignoreTimeScale_23 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreTimeScale_23), value);
	}

	inline static int32_t get_offset_of_useAlpha_24() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___useAlpha_24)); }
	inline FsmBool_t163807967 * get_useAlpha_24() const { return ___useAlpha_24; }
	inline FsmBool_t163807967 ** get_address_of_useAlpha_24() { return &___useAlpha_24; }
	inline void set_useAlpha_24(FsmBool_t163807967 * value)
	{
		___useAlpha_24 = value;
		Il2CppCodeGenWriteBarrier((&___useAlpha_24), value);
	}

	inline static int32_t get_offset_of_uiComponent_25() { return static_cast<int32_t>(offsetof(UiGraphicCrossFadeColor_t1236708496, ___uiComponent_25)); }
	inline Graphic_t1660335611 * get_uiComponent_25() const { return ___uiComponent_25; }
	inline Graphic_t1660335611 ** get_address_of_uiComponent_25() { return &___uiComponent_25; }
	inline void set_uiComponent_25(Graphic_t1660335611 * value)
	{
		___uiComponent_25 = value;
		Il2CppCodeGenWriteBarrier((&___uiComponent_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRAPHICCROSSFADECOLOR_T1236708496_H
#ifndef UIGRAPHICGETCOLOR_T798376921_H
#define UIGRAPHICGETCOLOR_T798376921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGraphicGetColor
struct  UiGraphicGetColor_t798376921  : public ComponentAction_1_t242655537
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiGraphicGetColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGraphicGetColor::color
	FsmColor_t1738900188 * ___color_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiGraphicGetColor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.UiGraphicGetColor::uiComponent
	Graphic_t1660335611 * ___uiComponent_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiGraphicGetColor_t798376921, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_color_17() { return static_cast<int32_t>(offsetof(UiGraphicGetColor_t798376921, ___color_17)); }
	inline FsmColor_t1738900188 * get_color_17() const { return ___color_17; }
	inline FsmColor_t1738900188 ** get_address_of_color_17() { return &___color_17; }
	inline void set_color_17(FsmColor_t1738900188 * value)
	{
		___color_17 = value;
		Il2CppCodeGenWriteBarrier((&___color_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiGraphicGetColor_t798376921, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_uiComponent_19() { return static_cast<int32_t>(offsetof(UiGraphicGetColor_t798376921, ___uiComponent_19)); }
	inline Graphic_t1660335611 * get_uiComponent_19() const { return ___uiComponent_19; }
	inline Graphic_t1660335611 ** get_address_of_uiComponent_19() { return &___uiComponent_19; }
	inline void set_uiComponent_19(Graphic_t1660335611 * value)
	{
		___uiComponent_19 = value;
		Il2CppCodeGenWriteBarrier((&___uiComponent_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRAPHICGETCOLOR_T798376921_H
#ifndef UIGRAPHICSETCOLOR_T3371320281_H
#define UIGRAPHICSETCOLOR_T3371320281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiGraphicSetColor
struct  UiGraphicSetColor_t3371320281  : public ComponentAction_1_t242655537
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiGraphicSetColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiGraphicSetColor::color
	FsmColor_t1738900188 * ___color_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicSetColor::red
	FsmFloat_t2883254149 * ___red_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicSetColor::green
	FsmFloat_t2883254149 * ___green_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicSetColor::blue
	FsmFloat_t2883254149 * ___blue_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiGraphicSetColor::alpha
	FsmFloat_t2883254149 * ___alpha_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiGraphicSetColor::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_22;
	// System.Boolean HutongGames.PlayMaker.Actions.UiGraphicSetColor::everyFrame
	bool ___everyFrame_23;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.UiGraphicSetColor::uiComponent
	Graphic_t1660335611 * ___uiComponent_24;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.UiGraphicSetColor::originalColor
	Color_t2555686324  ___originalColor_25;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_color_17() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___color_17)); }
	inline FsmColor_t1738900188 * get_color_17() const { return ___color_17; }
	inline FsmColor_t1738900188 ** get_address_of_color_17() { return &___color_17; }
	inline void set_color_17(FsmColor_t1738900188 * value)
	{
		___color_17 = value;
		Il2CppCodeGenWriteBarrier((&___color_17), value);
	}

	inline static int32_t get_offset_of_red_18() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___red_18)); }
	inline FsmFloat_t2883254149 * get_red_18() const { return ___red_18; }
	inline FsmFloat_t2883254149 ** get_address_of_red_18() { return &___red_18; }
	inline void set_red_18(FsmFloat_t2883254149 * value)
	{
		___red_18 = value;
		Il2CppCodeGenWriteBarrier((&___red_18), value);
	}

	inline static int32_t get_offset_of_green_19() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___green_19)); }
	inline FsmFloat_t2883254149 * get_green_19() const { return ___green_19; }
	inline FsmFloat_t2883254149 ** get_address_of_green_19() { return &___green_19; }
	inline void set_green_19(FsmFloat_t2883254149 * value)
	{
		___green_19 = value;
		Il2CppCodeGenWriteBarrier((&___green_19), value);
	}

	inline static int32_t get_offset_of_blue_20() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___blue_20)); }
	inline FsmFloat_t2883254149 * get_blue_20() const { return ___blue_20; }
	inline FsmFloat_t2883254149 ** get_address_of_blue_20() { return &___blue_20; }
	inline void set_blue_20(FsmFloat_t2883254149 * value)
	{
		___blue_20 = value;
		Il2CppCodeGenWriteBarrier((&___blue_20), value);
	}

	inline static int32_t get_offset_of_alpha_21() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___alpha_21)); }
	inline FsmFloat_t2883254149 * get_alpha_21() const { return ___alpha_21; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_21() { return &___alpha_21; }
	inline void set_alpha_21(FsmFloat_t2883254149 * value)
	{
		___alpha_21 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_21), value);
	}

	inline static int32_t get_offset_of_resetOnExit_22() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___resetOnExit_22)); }
	inline FsmBool_t163807967 * get_resetOnExit_22() const { return ___resetOnExit_22; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_22() { return &___resetOnExit_22; }
	inline void set_resetOnExit_22(FsmBool_t163807967 * value)
	{
		___resetOnExit_22 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}

	inline static int32_t get_offset_of_uiComponent_24() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___uiComponent_24)); }
	inline Graphic_t1660335611 * get_uiComponent_24() const { return ___uiComponent_24; }
	inline Graphic_t1660335611 ** get_address_of_uiComponent_24() { return &___uiComponent_24; }
	inline void set_uiComponent_24(Graphic_t1660335611 * value)
	{
		___uiComponent_24 = value;
		Il2CppCodeGenWriteBarrier((&___uiComponent_24), value);
	}

	inline static int32_t get_offset_of_originalColor_25() { return static_cast<int32_t>(offsetof(UiGraphicSetColor_t3371320281, ___originalColor_25)); }
	inline Color_t2555686324  get_originalColor_25() const { return ___originalColor_25; }
	inline Color_t2555686324 * get_address_of_originalColor_25() { return &___originalColor_25; }
	inline void set_originalColor_25(Color_t2555686324  value)
	{
		___originalColor_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIGRAPHICSETCOLOR_T3371320281_H
#ifndef UIIMAGEGETFILLAMOUNT_T3656243199_H
#define UIIMAGEGETFILLAMOUNT_T3656243199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiImageGetFillAmount
struct  UiImageGetFillAmount_t3656243199  : public ComponentAction_1_t1252589577
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiImageGetFillAmount::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiImageGetFillAmount::ImageFillAmount
	FsmFloat_t2883254149 * ___ImageFillAmount_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiImageGetFillAmount::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Image HutongGames.PlayMaker.Actions.UiImageGetFillAmount::image
	Image_t2670269651 * ___image_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiImageGetFillAmount_t3656243199, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_ImageFillAmount_17() { return static_cast<int32_t>(offsetof(UiImageGetFillAmount_t3656243199, ___ImageFillAmount_17)); }
	inline FsmFloat_t2883254149 * get_ImageFillAmount_17() const { return ___ImageFillAmount_17; }
	inline FsmFloat_t2883254149 ** get_address_of_ImageFillAmount_17() { return &___ImageFillAmount_17; }
	inline void set_ImageFillAmount_17(FsmFloat_t2883254149 * value)
	{
		___ImageFillAmount_17 = value;
		Il2CppCodeGenWriteBarrier((&___ImageFillAmount_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiImageGetFillAmount_t3656243199, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_image_19() { return static_cast<int32_t>(offsetof(UiImageGetFillAmount_t3656243199, ___image_19)); }
	inline Image_t2670269651 * get_image_19() const { return ___image_19; }
	inline Image_t2670269651 ** get_address_of_image_19() { return &___image_19; }
	inline void set_image_19(Image_t2670269651 * value)
	{
		___image_19 = value;
		Il2CppCodeGenWriteBarrier((&___image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGEGETFILLAMOUNT_T3656243199_H
#ifndef UIIMAGEGETSPRITE_T1070759130_H
#define UIIMAGEGETSPRITE_T1070759130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiImageGetSprite
struct  UiImageGetSprite_t1070759130  : public ComponentAction_1_t1252589577
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiImageGetSprite::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.UiImageGetSprite::sprite
	FsmObject_t2606870197 * ___sprite_17;
	// UnityEngine.UI.Image HutongGames.PlayMaker.Actions.UiImageGetSprite::image
	Image_t2670269651 * ___image_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiImageGetSprite_t1070759130, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_sprite_17() { return static_cast<int32_t>(offsetof(UiImageGetSprite_t1070759130, ___sprite_17)); }
	inline FsmObject_t2606870197 * get_sprite_17() const { return ___sprite_17; }
	inline FsmObject_t2606870197 ** get_address_of_sprite_17() { return &___sprite_17; }
	inline void set_sprite_17(FsmObject_t2606870197 * value)
	{
		___sprite_17 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_17), value);
	}

	inline static int32_t get_offset_of_image_18() { return static_cast<int32_t>(offsetof(UiImageGetSprite_t1070759130, ___image_18)); }
	inline Image_t2670269651 * get_image_18() const { return ___image_18; }
	inline Image_t2670269651 ** get_address_of_image_18() { return &___image_18; }
	inline void set_image_18(Image_t2670269651 * value)
	{
		___image_18 = value;
		Il2CppCodeGenWriteBarrier((&___image_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGEGETSPRITE_T1070759130_H
#ifndef UIIMAGESETFILLAMOUNT_T140630020_H
#define UIIMAGESETFILLAMOUNT_T140630020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiImageSetFillAmount
struct  UiImageSetFillAmount_t140630020  : public ComponentAction_1_t1252589577
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiImageSetFillAmount::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiImageSetFillAmount::ImageFillAmount
	FsmFloat_t2883254149 * ___ImageFillAmount_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiImageSetFillAmount::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Image HutongGames.PlayMaker.Actions.UiImageSetFillAmount::image
	Image_t2670269651 * ___image_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiImageSetFillAmount_t140630020, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_ImageFillAmount_17() { return static_cast<int32_t>(offsetof(UiImageSetFillAmount_t140630020, ___ImageFillAmount_17)); }
	inline FsmFloat_t2883254149 * get_ImageFillAmount_17() const { return ___ImageFillAmount_17; }
	inline FsmFloat_t2883254149 ** get_address_of_ImageFillAmount_17() { return &___ImageFillAmount_17; }
	inline void set_ImageFillAmount_17(FsmFloat_t2883254149 * value)
	{
		___ImageFillAmount_17 = value;
		Il2CppCodeGenWriteBarrier((&___ImageFillAmount_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiImageSetFillAmount_t140630020, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_image_19() { return static_cast<int32_t>(offsetof(UiImageSetFillAmount_t140630020, ___image_19)); }
	inline Image_t2670269651 * get_image_19() const { return ___image_19; }
	inline Image_t2670269651 ** get_address_of_image_19() { return &___image_19; }
	inline void set_image_19(Image_t2670269651 * value)
	{
		___image_19 = value;
		Il2CppCodeGenWriteBarrier((&___image_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGESETFILLAMOUNT_T140630020_H
#ifndef UIIMAGESETSPRITE_T278822106_H
#define UIIMAGESETSPRITE_T278822106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiImageSetSprite
struct  UiImageSetSprite_t278822106  : public ComponentAction_1_t1252589577
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiImageSetSprite::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.UiImageSetSprite::sprite
	FsmObject_t2606870197 * ___sprite_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiImageSetSprite::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.Image HutongGames.PlayMaker.Actions.UiImageSetSprite::image
	Image_t2670269651 * ___image_19;
	// UnityEngine.Sprite HutongGames.PlayMaker.Actions.UiImageSetSprite::originalSprite
	Sprite_t280657092 * ___originalSprite_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiImageSetSprite_t278822106, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_sprite_17() { return static_cast<int32_t>(offsetof(UiImageSetSprite_t278822106, ___sprite_17)); }
	inline FsmObject_t2606870197 * get_sprite_17() const { return ___sprite_17; }
	inline FsmObject_t2606870197 ** get_address_of_sprite_17() { return &___sprite_17; }
	inline void set_sprite_17(FsmObject_t2606870197 * value)
	{
		___sprite_17 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiImageSetSprite_t278822106, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_image_19() { return static_cast<int32_t>(offsetof(UiImageSetSprite_t278822106, ___image_19)); }
	inline Image_t2670269651 * get_image_19() const { return ___image_19; }
	inline Image_t2670269651 ** get_address_of_image_19() { return &___image_19; }
	inline void set_image_19(Image_t2670269651 * value)
	{
		___image_19 = value;
		Il2CppCodeGenWriteBarrier((&___image_19), value);
	}

	inline static int32_t get_offset_of_originalSprite_20() { return static_cast<int32_t>(offsetof(UiImageSetSprite_t278822106, ___originalSprite_20)); }
	inline Sprite_t280657092 * get_originalSprite_20() const { return ___originalSprite_20; }
	inline Sprite_t280657092 ** get_address_of_originalSprite_20() { return &___originalSprite_20; }
	inline void set_originalSprite_20(Sprite_t280657092 * value)
	{
		___originalSprite_20 = value;
		Il2CppCodeGenWriteBarrier((&___originalSprite_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGESETSPRITE_T278822106_H
#ifndef UIINPUTFIELDACTIVATE_T1212826059_H
#define UIINPUTFIELDACTIVATE_T1212826059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldActivate
struct  UiInputFieldActivate_t1212826059  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldActivate::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldActivate::deactivateOnExit
	FsmBool_t163807967 * ___deactivateOnExit_17;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldActivate::inputField
	InputField_t3762917431 * ___inputField_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldActivate_t1212826059, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_deactivateOnExit_17() { return static_cast<int32_t>(offsetof(UiInputFieldActivate_t1212826059, ___deactivateOnExit_17)); }
	inline FsmBool_t163807967 * get_deactivateOnExit_17() const { return ___deactivateOnExit_17; }
	inline FsmBool_t163807967 ** get_address_of_deactivateOnExit_17() { return &___deactivateOnExit_17; }
	inline void set_deactivateOnExit_17(FsmBool_t163807967 * value)
	{
		___deactivateOnExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___deactivateOnExit_17), value);
	}

	inline static int32_t get_offset_of_inputField_18() { return static_cast<int32_t>(offsetof(UiInputFieldActivate_t1212826059, ___inputField_18)); }
	inline InputField_t3762917431 * get_inputField_18() const { return ___inputField_18; }
	inline InputField_t3762917431 ** get_address_of_inputField_18() { return &___inputField_18; }
	inline void set_inputField_18(InputField_t3762917431 * value)
	{
		___inputField_18 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDACTIVATE_T1212826059_H
#ifndef UIINPUTFIELDDEACTIVATE_T990877000_H
#define UIINPUTFIELDDEACTIVATE_T990877000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldDeactivate
struct  UiInputFieldDeactivate_t990877000  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldDeactivate::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldDeactivate::activateOnExit
	FsmBool_t163807967 * ___activateOnExit_17;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldDeactivate::inputField
	InputField_t3762917431 * ___inputField_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldDeactivate_t990877000, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_activateOnExit_17() { return static_cast<int32_t>(offsetof(UiInputFieldDeactivate_t990877000, ___activateOnExit_17)); }
	inline FsmBool_t163807967 * get_activateOnExit_17() const { return ___activateOnExit_17; }
	inline FsmBool_t163807967 ** get_address_of_activateOnExit_17() { return &___activateOnExit_17; }
	inline void set_activateOnExit_17(FsmBool_t163807967 * value)
	{
		___activateOnExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___activateOnExit_17), value);
	}

	inline static int32_t get_offset_of_inputField_18() { return static_cast<int32_t>(offsetof(UiInputFieldDeactivate_t990877000, ___inputField_18)); }
	inline InputField_t3762917431 * get_inputField_18() const { return ___inputField_18; }
	inline InputField_t3762917431 ** get_address_of_inputField_18() { return &___inputField_18; }
	inline void set_inputField_18(InputField_t3762917431 * value)
	{
		___inputField_18 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDDEACTIVATE_T990877000_H
#ifndef UIINPUTFIELDGETCARETBLINKRATE_T2635913202_H
#define UIINPUTFIELDGETCARETBLINKRATE_T2635913202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetCaretBlinkRate
struct  UiInputFieldGetCaretBlinkRate_t2635913202  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetCaretBlinkRate::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiInputFieldGetCaretBlinkRate::caretBlinkRate
	FsmFloat_t2883254149 * ___caretBlinkRate_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetCaretBlinkRate::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetCaretBlinkRate::inputField
	InputField_t3762917431 * ___inputField_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetCaretBlinkRate_t2635913202, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_caretBlinkRate_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetCaretBlinkRate_t2635913202, ___caretBlinkRate_17)); }
	inline FsmFloat_t2883254149 * get_caretBlinkRate_17() const { return ___caretBlinkRate_17; }
	inline FsmFloat_t2883254149 ** get_address_of_caretBlinkRate_17() { return &___caretBlinkRate_17; }
	inline void set_caretBlinkRate_17(FsmFloat_t2883254149 * value)
	{
		___caretBlinkRate_17 = value;
		Il2CppCodeGenWriteBarrier((&___caretBlinkRate_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetCaretBlinkRate_t2635913202, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_inputField_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetCaretBlinkRate_t2635913202, ___inputField_19)); }
	inline InputField_t3762917431 * get_inputField_19() const { return ___inputField_19; }
	inline InputField_t3762917431 ** get_address_of_inputField_19() { return &___inputField_19; }
	inline void set_inputField_19(InputField_t3762917431 * value)
	{
		___inputField_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETCARETBLINKRATE_T2635913202_H
#ifndef UIINPUTFIELDGETCHARACTERLIMIT_T617759529_H
#define UIINPUTFIELDGETCHARACTERLIMIT_T617759529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit
struct  UiInputFieldGetCharacterLimit_t617759529  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit::characterLimit
	FsmInt_t874273141 * ___characterLimit_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit::hasNoLimitEvent
	FsmEvent_t3736299882 * ___hasNoLimitEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit::isLimitedEvent
	FsmEvent_t3736299882 * ___isLimitedEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit::everyFrame
	bool ___everyFrame_20;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetCharacterLimit::inputField
	InputField_t3762917431 * ___inputField_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetCharacterLimit_t617759529, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_characterLimit_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetCharacterLimit_t617759529, ___characterLimit_17)); }
	inline FsmInt_t874273141 * get_characterLimit_17() const { return ___characterLimit_17; }
	inline FsmInt_t874273141 ** get_address_of_characterLimit_17() { return &___characterLimit_17; }
	inline void set_characterLimit_17(FsmInt_t874273141 * value)
	{
		___characterLimit_17 = value;
		Il2CppCodeGenWriteBarrier((&___characterLimit_17), value);
	}

	inline static int32_t get_offset_of_hasNoLimitEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetCharacterLimit_t617759529, ___hasNoLimitEvent_18)); }
	inline FsmEvent_t3736299882 * get_hasNoLimitEvent_18() const { return ___hasNoLimitEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_hasNoLimitEvent_18() { return &___hasNoLimitEvent_18; }
	inline void set_hasNoLimitEvent_18(FsmEvent_t3736299882 * value)
	{
		___hasNoLimitEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___hasNoLimitEvent_18), value);
	}

	inline static int32_t get_offset_of_isLimitedEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetCharacterLimit_t617759529, ___isLimitedEvent_19)); }
	inline FsmEvent_t3736299882 * get_isLimitedEvent_19() const { return ___isLimitedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isLimitedEvent_19() { return &___isLimitedEvent_19; }
	inline void set_isLimitedEvent_19(FsmEvent_t3736299882 * value)
	{
		___isLimitedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isLimitedEvent_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetCharacterLimit_t617759529, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of_inputField_21() { return static_cast<int32_t>(offsetof(UiInputFieldGetCharacterLimit_t617759529, ___inputField_21)); }
	inline InputField_t3762917431 * get_inputField_21() const { return ___inputField_21; }
	inline InputField_t3762917431 ** get_address_of_inputField_21() { return &___inputField_21; }
	inline void set_inputField_21(InputField_t3762917431 * value)
	{
		___inputField_21 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETCHARACTERLIMIT_T617759529_H
#ifndef UIINPUTFIELDGETHIDEMOBILEINPUT_T1704494218_H
#define UIINPUTFIELDGETHIDEMOBILEINPUT_T1704494218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetHideMobileInput
struct  UiInputFieldGetHideMobileInput_t1704494218  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetHideMobileInput::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldGetHideMobileInput::hideMobileInput
	FsmBool_t163807967 * ___hideMobileInput_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetHideMobileInput::mobileInputHiddenEvent
	FsmEvent_t3736299882 * ___mobileInputHiddenEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetHideMobileInput::mobileInputShownEvent
	FsmEvent_t3736299882 * ___mobileInputShownEvent_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetHideMobileInput::inputField
	InputField_t3762917431 * ___inputField_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetHideMobileInput_t1704494218, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_hideMobileInput_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetHideMobileInput_t1704494218, ___hideMobileInput_17)); }
	inline FsmBool_t163807967 * get_hideMobileInput_17() const { return ___hideMobileInput_17; }
	inline FsmBool_t163807967 ** get_address_of_hideMobileInput_17() { return &___hideMobileInput_17; }
	inline void set_hideMobileInput_17(FsmBool_t163807967 * value)
	{
		___hideMobileInput_17 = value;
		Il2CppCodeGenWriteBarrier((&___hideMobileInput_17), value);
	}

	inline static int32_t get_offset_of_mobileInputHiddenEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetHideMobileInput_t1704494218, ___mobileInputHiddenEvent_18)); }
	inline FsmEvent_t3736299882 * get_mobileInputHiddenEvent_18() const { return ___mobileInputHiddenEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_mobileInputHiddenEvent_18() { return &___mobileInputHiddenEvent_18; }
	inline void set_mobileInputHiddenEvent_18(FsmEvent_t3736299882 * value)
	{
		___mobileInputHiddenEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___mobileInputHiddenEvent_18), value);
	}

	inline static int32_t get_offset_of_mobileInputShownEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetHideMobileInput_t1704494218, ___mobileInputShownEvent_19)); }
	inline FsmEvent_t3736299882 * get_mobileInputShownEvent_19() const { return ___mobileInputShownEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_mobileInputShownEvent_19() { return &___mobileInputShownEvent_19; }
	inline void set_mobileInputShownEvent_19(FsmEvent_t3736299882 * value)
	{
		___mobileInputShownEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___mobileInputShownEvent_19), value);
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetHideMobileInput_t1704494218, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETHIDEMOBILEINPUT_T1704494218_H
#ifndef UIINPUTFIELDGETISFOCUSED_T1382970275_H
#define UIINPUTFIELDGETISFOCUSED_T1382970275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetIsFocused
struct  UiInputFieldGetIsFocused_t1382970275  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetIsFocused::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldGetIsFocused::isFocused
	FsmBool_t163807967 * ___isFocused_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetIsFocused::isfocusedEvent
	FsmEvent_t3736299882 * ___isfocusedEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetIsFocused::isNotFocusedEvent
	FsmEvent_t3736299882 * ___isNotFocusedEvent_19;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetIsFocused::inputField
	InputField_t3762917431 * ___inputField_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetIsFocused_t1382970275, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_isFocused_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetIsFocused_t1382970275, ___isFocused_17)); }
	inline FsmBool_t163807967 * get_isFocused_17() const { return ___isFocused_17; }
	inline FsmBool_t163807967 ** get_address_of_isFocused_17() { return &___isFocused_17; }
	inline void set_isFocused_17(FsmBool_t163807967 * value)
	{
		___isFocused_17 = value;
		Il2CppCodeGenWriteBarrier((&___isFocused_17), value);
	}

	inline static int32_t get_offset_of_isfocusedEvent_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetIsFocused_t1382970275, ___isfocusedEvent_18)); }
	inline FsmEvent_t3736299882 * get_isfocusedEvent_18() const { return ___isfocusedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_isfocusedEvent_18() { return &___isfocusedEvent_18; }
	inline void set_isfocusedEvent_18(FsmEvent_t3736299882 * value)
	{
		___isfocusedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___isfocusedEvent_18), value);
	}

	inline static int32_t get_offset_of_isNotFocusedEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetIsFocused_t1382970275, ___isNotFocusedEvent_19)); }
	inline FsmEvent_t3736299882 * get_isNotFocusedEvent_19() const { return ___isNotFocusedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotFocusedEvent_19() { return &___isNotFocusedEvent_19; }
	inline void set_isNotFocusedEvent_19(FsmEvent_t3736299882 * value)
	{
		___isNotFocusedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___isNotFocusedEvent_19), value);
	}

	inline static int32_t get_offset_of_inputField_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetIsFocused_t1382970275, ___inputField_20)); }
	inline InputField_t3762917431 * get_inputField_20() const { return ___inputField_20; }
	inline InputField_t3762917431 ** get_address_of_inputField_20() { return &___inputField_20; }
	inline void set_inputField_20(InputField_t3762917431 * value)
	{
		___inputField_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETISFOCUSED_T1382970275_H
#ifndef UIINPUTFIELDGETPLACEHOLDER_T898934888_H
#define UIINPUTFIELDGETPLACEHOLDER_T898934888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder
struct  UiInputFieldGetPlaceHolder_t898934888  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder::placeHolder
	FsmGameObject_t3581898942 * ___placeHolder_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder::placeHolderDefined
	FsmBool_t163807967 * ___placeHolderDefined_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder::foundEvent
	FsmEvent_t3736299882 * ___foundEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder::notFoundEvent
	FsmEvent_t3736299882 * ___notFoundEvent_20;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetPlaceHolder::inputField
	InputField_t3762917431 * ___inputField_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetPlaceHolder_t898934888, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_placeHolder_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetPlaceHolder_t898934888, ___placeHolder_17)); }
	inline FsmGameObject_t3581898942 * get_placeHolder_17() const { return ___placeHolder_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_placeHolder_17() { return &___placeHolder_17; }
	inline void set_placeHolder_17(FsmGameObject_t3581898942 * value)
	{
		___placeHolder_17 = value;
		Il2CppCodeGenWriteBarrier((&___placeHolder_17), value);
	}

	inline static int32_t get_offset_of_placeHolderDefined_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetPlaceHolder_t898934888, ___placeHolderDefined_18)); }
	inline FsmBool_t163807967 * get_placeHolderDefined_18() const { return ___placeHolderDefined_18; }
	inline FsmBool_t163807967 ** get_address_of_placeHolderDefined_18() { return &___placeHolderDefined_18; }
	inline void set_placeHolderDefined_18(FsmBool_t163807967 * value)
	{
		___placeHolderDefined_18 = value;
		Il2CppCodeGenWriteBarrier((&___placeHolderDefined_18), value);
	}

	inline static int32_t get_offset_of_foundEvent_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetPlaceHolder_t898934888, ___foundEvent_19)); }
	inline FsmEvent_t3736299882 * get_foundEvent_19() const { return ___foundEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_foundEvent_19() { return &___foundEvent_19; }
	inline void set_foundEvent_19(FsmEvent_t3736299882 * value)
	{
		___foundEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___foundEvent_19), value);
	}

	inline static int32_t get_offset_of_notFoundEvent_20() { return static_cast<int32_t>(offsetof(UiInputFieldGetPlaceHolder_t898934888, ___notFoundEvent_20)); }
	inline FsmEvent_t3736299882 * get_notFoundEvent_20() const { return ___notFoundEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_notFoundEvent_20() { return &___notFoundEvent_20; }
	inline void set_notFoundEvent_20(FsmEvent_t3736299882 * value)
	{
		___notFoundEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___notFoundEvent_20), value);
	}

	inline static int32_t get_offset_of_inputField_21() { return static_cast<int32_t>(offsetof(UiInputFieldGetPlaceHolder_t898934888, ___inputField_21)); }
	inline InputField_t3762917431 * get_inputField_21() const { return ___inputField_21; }
	inline InputField_t3762917431 ** get_address_of_inputField_21() { return &___inputField_21; }
	inline void set_inputField_21(InputField_t3762917431 * value)
	{
		___inputField_21 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETPLACEHOLDER_T898934888_H
#ifndef UIINPUTFIELDGETSELECTIONCOLOR_T1814258712_H
#define UIINPUTFIELDGETSELECTIONCOLOR_T1814258712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetSelectionColor
struct  UiInputFieldGetSelectionColor_t1814258712  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetSelectionColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiInputFieldGetSelectionColor::selectionColor
	FsmColor_t1738900188 * ___selectionColor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetSelectionColor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetSelectionColor::inputField
	InputField_t3762917431 * ___inputField_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetSelectionColor_t1814258712, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_selectionColor_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetSelectionColor_t1814258712, ___selectionColor_17)); }
	inline FsmColor_t1738900188 * get_selectionColor_17() const { return ___selectionColor_17; }
	inline FsmColor_t1738900188 ** get_address_of_selectionColor_17() { return &___selectionColor_17; }
	inline void set_selectionColor_17(FsmColor_t1738900188 * value)
	{
		___selectionColor_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectionColor_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetSelectionColor_t1814258712, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_inputField_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetSelectionColor_t1814258712, ___inputField_19)); }
	inline InputField_t3762917431 * get_inputField_19() const { return ___inputField_19; }
	inline InputField_t3762917431 ** get_address_of_inputField_19() { return &___inputField_19; }
	inline void set_inputField_19(InputField_t3762917431 * value)
	{
		___inputField_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETSELECTIONCOLOR_T1814258712_H
#ifndef UIINPUTFIELDGETTEXT_T3479464756_H
#define UIINPUTFIELDGETTEXT_T3479464756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiInputFieldGetText
struct  UiInputFieldGetText_t3479464756  : public ComponentAction_1_t2345237357
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiInputFieldGetText::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiInputFieldGetText::text
	FsmString_t1785915204 * ___text_17;
	// System.Boolean HutongGames.PlayMaker.Actions.UiInputFieldGetText::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.UiInputFieldGetText::inputField
	InputField_t3762917431 * ___inputField_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiInputFieldGetText_t3479464756, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(UiInputFieldGetText_t3479464756, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(UiInputFieldGetText_t3479464756, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_inputField_19() { return static_cast<int32_t>(offsetof(UiInputFieldGetText_t3479464756, ___inputField_19)); }
	inline InputField_t3762917431 * get_inputField_19() const { return ___inputField_19; }
	inline InputField_t3762917431 ** get_address_of_inputField_19() { return &___inputField_19; }
	inline void set_inputField_19(InputField_t3762917431 * value)
	{
		___inputField_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTFIELDGETTEXT_T3479464756_H
#ifndef UILAYOUTELEMENTGETVALUES_T128213014_H
#define UILAYOUTELEMENTGETVALUES_T128213014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiLayoutElementGetValues
struct  UiLayoutElementGetValues_t128213014  : public ComponentAction_1_t367723604
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::ignoreLayout
	FsmBool_t163807967 * ___ignoreLayout_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::minWidthEnabled
	FsmBool_t163807967 * ___minWidthEnabled_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::minWidth
	FsmFloat_t2883254149 * ___minWidth_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::minHeightEnabled
	FsmBool_t163807967 * ___minHeightEnabled_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::minHeight
	FsmFloat_t2883254149 * ___minHeight_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::preferredWidthEnabled
	FsmBool_t163807967 * ___preferredWidthEnabled_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::preferredWidth
	FsmFloat_t2883254149 * ___preferredWidth_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::preferredHeightEnabled
	FsmBool_t163807967 * ___preferredHeightEnabled_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::preferredHeight
	FsmFloat_t2883254149 * ___preferredHeight_25;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::flexibleWidthEnabled
	FsmBool_t163807967 * ___flexibleWidthEnabled_26;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::flexibleWidth
	FsmFloat_t2883254149 * ___flexibleWidth_27;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::flexibleHeightEnabled
	FsmBool_t163807967 * ___flexibleHeightEnabled_28;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::flexibleHeight
	FsmFloat_t2883254149 * ___flexibleHeight_29;
	// System.Boolean HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::everyFrame
	bool ___everyFrame_30;
	// UnityEngine.UI.LayoutElement HutongGames.PlayMaker.Actions.UiLayoutElementGetValues::layoutElement
	LayoutElement_t1785403678 * ___layoutElement_31;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_ignoreLayout_17() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___ignoreLayout_17)); }
	inline FsmBool_t163807967 * get_ignoreLayout_17() const { return ___ignoreLayout_17; }
	inline FsmBool_t163807967 ** get_address_of_ignoreLayout_17() { return &___ignoreLayout_17; }
	inline void set_ignoreLayout_17(FsmBool_t163807967 * value)
	{
		___ignoreLayout_17 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreLayout_17), value);
	}

	inline static int32_t get_offset_of_minWidthEnabled_18() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___minWidthEnabled_18)); }
	inline FsmBool_t163807967 * get_minWidthEnabled_18() const { return ___minWidthEnabled_18; }
	inline FsmBool_t163807967 ** get_address_of_minWidthEnabled_18() { return &___minWidthEnabled_18; }
	inline void set_minWidthEnabled_18(FsmBool_t163807967 * value)
	{
		___minWidthEnabled_18 = value;
		Il2CppCodeGenWriteBarrier((&___minWidthEnabled_18), value);
	}

	inline static int32_t get_offset_of_minWidth_19() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___minWidth_19)); }
	inline FsmFloat_t2883254149 * get_minWidth_19() const { return ___minWidth_19; }
	inline FsmFloat_t2883254149 ** get_address_of_minWidth_19() { return &___minWidth_19; }
	inline void set_minWidth_19(FsmFloat_t2883254149 * value)
	{
		___minWidth_19 = value;
		Il2CppCodeGenWriteBarrier((&___minWidth_19), value);
	}

	inline static int32_t get_offset_of_minHeightEnabled_20() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___minHeightEnabled_20)); }
	inline FsmBool_t163807967 * get_minHeightEnabled_20() const { return ___minHeightEnabled_20; }
	inline FsmBool_t163807967 ** get_address_of_minHeightEnabled_20() { return &___minHeightEnabled_20; }
	inline void set_minHeightEnabled_20(FsmBool_t163807967 * value)
	{
		___minHeightEnabled_20 = value;
		Il2CppCodeGenWriteBarrier((&___minHeightEnabled_20), value);
	}

	inline static int32_t get_offset_of_minHeight_21() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___minHeight_21)); }
	inline FsmFloat_t2883254149 * get_minHeight_21() const { return ___minHeight_21; }
	inline FsmFloat_t2883254149 ** get_address_of_minHeight_21() { return &___minHeight_21; }
	inline void set_minHeight_21(FsmFloat_t2883254149 * value)
	{
		___minHeight_21 = value;
		Il2CppCodeGenWriteBarrier((&___minHeight_21), value);
	}

	inline static int32_t get_offset_of_preferredWidthEnabled_22() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___preferredWidthEnabled_22)); }
	inline FsmBool_t163807967 * get_preferredWidthEnabled_22() const { return ___preferredWidthEnabled_22; }
	inline FsmBool_t163807967 ** get_address_of_preferredWidthEnabled_22() { return &___preferredWidthEnabled_22; }
	inline void set_preferredWidthEnabled_22(FsmBool_t163807967 * value)
	{
		___preferredWidthEnabled_22 = value;
		Il2CppCodeGenWriteBarrier((&___preferredWidthEnabled_22), value);
	}

	inline static int32_t get_offset_of_preferredWidth_23() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___preferredWidth_23)); }
	inline FsmFloat_t2883254149 * get_preferredWidth_23() const { return ___preferredWidth_23; }
	inline FsmFloat_t2883254149 ** get_address_of_preferredWidth_23() { return &___preferredWidth_23; }
	inline void set_preferredWidth_23(FsmFloat_t2883254149 * value)
	{
		___preferredWidth_23 = value;
		Il2CppCodeGenWriteBarrier((&___preferredWidth_23), value);
	}

	inline static int32_t get_offset_of_preferredHeightEnabled_24() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___preferredHeightEnabled_24)); }
	inline FsmBool_t163807967 * get_preferredHeightEnabled_24() const { return ___preferredHeightEnabled_24; }
	inline FsmBool_t163807967 ** get_address_of_preferredHeightEnabled_24() { return &___preferredHeightEnabled_24; }
	inline void set_preferredHeightEnabled_24(FsmBool_t163807967 * value)
	{
		___preferredHeightEnabled_24 = value;
		Il2CppCodeGenWriteBarrier((&___preferredHeightEnabled_24), value);
	}

	inline static int32_t get_offset_of_preferredHeight_25() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___preferredHeight_25)); }
	inline FsmFloat_t2883254149 * get_preferredHeight_25() const { return ___preferredHeight_25; }
	inline FsmFloat_t2883254149 ** get_address_of_preferredHeight_25() { return &___preferredHeight_25; }
	inline void set_preferredHeight_25(FsmFloat_t2883254149 * value)
	{
		___preferredHeight_25 = value;
		Il2CppCodeGenWriteBarrier((&___preferredHeight_25), value);
	}

	inline static int32_t get_offset_of_flexibleWidthEnabled_26() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___flexibleWidthEnabled_26)); }
	inline FsmBool_t163807967 * get_flexibleWidthEnabled_26() const { return ___flexibleWidthEnabled_26; }
	inline FsmBool_t163807967 ** get_address_of_flexibleWidthEnabled_26() { return &___flexibleWidthEnabled_26; }
	inline void set_flexibleWidthEnabled_26(FsmBool_t163807967 * value)
	{
		___flexibleWidthEnabled_26 = value;
		Il2CppCodeGenWriteBarrier((&___flexibleWidthEnabled_26), value);
	}

	inline static int32_t get_offset_of_flexibleWidth_27() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___flexibleWidth_27)); }
	inline FsmFloat_t2883254149 * get_flexibleWidth_27() const { return ___flexibleWidth_27; }
	inline FsmFloat_t2883254149 ** get_address_of_flexibleWidth_27() { return &___flexibleWidth_27; }
	inline void set_flexibleWidth_27(FsmFloat_t2883254149 * value)
	{
		___flexibleWidth_27 = value;
		Il2CppCodeGenWriteBarrier((&___flexibleWidth_27), value);
	}

	inline static int32_t get_offset_of_flexibleHeightEnabled_28() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___flexibleHeightEnabled_28)); }
	inline FsmBool_t163807967 * get_flexibleHeightEnabled_28() const { return ___flexibleHeightEnabled_28; }
	inline FsmBool_t163807967 ** get_address_of_flexibleHeightEnabled_28() { return &___flexibleHeightEnabled_28; }
	inline void set_flexibleHeightEnabled_28(FsmBool_t163807967 * value)
	{
		___flexibleHeightEnabled_28 = value;
		Il2CppCodeGenWriteBarrier((&___flexibleHeightEnabled_28), value);
	}

	inline static int32_t get_offset_of_flexibleHeight_29() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___flexibleHeight_29)); }
	inline FsmFloat_t2883254149 * get_flexibleHeight_29() const { return ___flexibleHeight_29; }
	inline FsmFloat_t2883254149 ** get_address_of_flexibleHeight_29() { return &___flexibleHeight_29; }
	inline void set_flexibleHeight_29(FsmFloat_t2883254149 * value)
	{
		___flexibleHeight_29 = value;
		Il2CppCodeGenWriteBarrier((&___flexibleHeight_29), value);
	}

	inline static int32_t get_offset_of_everyFrame_30() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___everyFrame_30)); }
	inline bool get_everyFrame_30() const { return ___everyFrame_30; }
	inline bool* get_address_of_everyFrame_30() { return &___everyFrame_30; }
	inline void set_everyFrame_30(bool value)
	{
		___everyFrame_30 = value;
	}

	inline static int32_t get_offset_of_layoutElement_31() { return static_cast<int32_t>(offsetof(UiLayoutElementGetValues_t128213014, ___layoutElement_31)); }
	inline LayoutElement_t1785403678 * get_layoutElement_31() const { return ___layoutElement_31; }
	inline LayoutElement_t1785403678 ** get_address_of_layoutElement_31() { return &___layoutElement_31; }
	inline void set_layoutElement_31(LayoutElement_t1785403678 * value)
	{
		___layoutElement_31 = value;
		Il2CppCodeGenWriteBarrier((&___layoutElement_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILAYOUTELEMENTGETVALUES_T128213014_H
#ifndef UILAYOUTELEMENTSETVALUES_T982540310_H
#define UILAYOUTELEMENTSETVALUES_T982540310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiLayoutElementSetValues
struct  UiLayoutElementSetValues_t982540310  : public ComponentAction_1_t367723604
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::minWidth
	FsmFloat_t2883254149 * ___minWidth_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::minHeight
	FsmFloat_t2883254149 * ___minHeight_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::preferredWidth
	FsmFloat_t2883254149 * ___preferredWidth_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::preferredHeight
	FsmFloat_t2883254149 * ___preferredHeight_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::flexibleWidth
	FsmFloat_t2883254149 * ___flexibleWidth_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::flexibleHeight
	FsmFloat_t2883254149 * ___flexibleHeight_22;
	// System.Boolean HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::everyFrame
	bool ___everyFrame_23;
	// UnityEngine.UI.LayoutElement HutongGames.PlayMaker.Actions.UiLayoutElementSetValues::layoutElement
	LayoutElement_t1785403678 * ___layoutElement_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_minWidth_17() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___minWidth_17)); }
	inline FsmFloat_t2883254149 * get_minWidth_17() const { return ___minWidth_17; }
	inline FsmFloat_t2883254149 ** get_address_of_minWidth_17() { return &___minWidth_17; }
	inline void set_minWidth_17(FsmFloat_t2883254149 * value)
	{
		___minWidth_17 = value;
		Il2CppCodeGenWriteBarrier((&___minWidth_17), value);
	}

	inline static int32_t get_offset_of_minHeight_18() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___minHeight_18)); }
	inline FsmFloat_t2883254149 * get_minHeight_18() const { return ___minHeight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_minHeight_18() { return &___minHeight_18; }
	inline void set_minHeight_18(FsmFloat_t2883254149 * value)
	{
		___minHeight_18 = value;
		Il2CppCodeGenWriteBarrier((&___minHeight_18), value);
	}

	inline static int32_t get_offset_of_preferredWidth_19() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___preferredWidth_19)); }
	inline FsmFloat_t2883254149 * get_preferredWidth_19() const { return ___preferredWidth_19; }
	inline FsmFloat_t2883254149 ** get_address_of_preferredWidth_19() { return &___preferredWidth_19; }
	inline void set_preferredWidth_19(FsmFloat_t2883254149 * value)
	{
		___preferredWidth_19 = value;
		Il2CppCodeGenWriteBarrier((&___preferredWidth_19), value);
	}

	inline static int32_t get_offset_of_preferredHeight_20() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___preferredHeight_20)); }
	inline FsmFloat_t2883254149 * get_preferredHeight_20() const { return ___preferredHeight_20; }
	inline FsmFloat_t2883254149 ** get_address_of_preferredHeight_20() { return &___preferredHeight_20; }
	inline void set_preferredHeight_20(FsmFloat_t2883254149 * value)
	{
		___preferredHeight_20 = value;
		Il2CppCodeGenWriteBarrier((&___preferredHeight_20), value);
	}

	inline static int32_t get_offset_of_flexibleWidth_21() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___flexibleWidth_21)); }
	inline FsmFloat_t2883254149 * get_flexibleWidth_21() const { return ___flexibleWidth_21; }
	inline FsmFloat_t2883254149 ** get_address_of_flexibleWidth_21() { return &___flexibleWidth_21; }
	inline void set_flexibleWidth_21(FsmFloat_t2883254149 * value)
	{
		___flexibleWidth_21 = value;
		Il2CppCodeGenWriteBarrier((&___flexibleWidth_21), value);
	}

	inline static int32_t get_offset_of_flexibleHeight_22() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___flexibleHeight_22)); }
	inline FsmFloat_t2883254149 * get_flexibleHeight_22() const { return ___flexibleHeight_22; }
	inline FsmFloat_t2883254149 ** get_address_of_flexibleHeight_22() { return &___flexibleHeight_22; }
	inline void set_flexibleHeight_22(FsmFloat_t2883254149 * value)
	{
		___flexibleHeight_22 = value;
		Il2CppCodeGenWriteBarrier((&___flexibleHeight_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}

	inline static int32_t get_offset_of_layoutElement_24() { return static_cast<int32_t>(offsetof(UiLayoutElementSetValues_t982540310, ___layoutElement_24)); }
	inline LayoutElement_t1785403678 * get_layoutElement_24() const { return ___layoutElement_24; }
	inline LayoutElement_t1785403678 ** get_address_of_layoutElement_24() { return &___layoutElement_24; }
	inline void set_layoutElement_24(LayoutElement_t1785403678 * value)
	{
		___layoutElement_24 = value;
		Il2CppCodeGenWriteBarrier((&___layoutElement_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILAYOUTELEMENTSETVALUES_T982540310_H
#ifndef UINAVIGATIONEXPLICITGETPROPERTIES_T4213199546_H
#define UINAVIGATIONEXPLICITGETPROPERTIES_T4213199546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties
struct  UiNavigationExplicitGetProperties_t4213199546  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties::selectOnDown
	FsmGameObject_t3581898942 * ___selectOnDown_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties::selectOnUp
	FsmGameObject_t3581898942 * ___selectOnUp_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties::selectOnLeft
	FsmGameObject_t3581898942 * ___selectOnLeft_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties::selectOnRight
	FsmGameObject_t3581898942 * ___selectOnRight_20;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiNavigationExplicitGetProperties::_selectable
	Selectable_t3250028441 * ____selectable_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiNavigationExplicitGetProperties_t4213199546, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_selectOnDown_17() { return static_cast<int32_t>(offsetof(UiNavigationExplicitGetProperties_t4213199546, ___selectOnDown_17)); }
	inline FsmGameObject_t3581898942 * get_selectOnDown_17() const { return ___selectOnDown_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnDown_17() { return &___selectOnDown_17; }
	inline void set_selectOnDown_17(FsmGameObject_t3581898942 * value)
	{
		___selectOnDown_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnDown_17), value);
	}

	inline static int32_t get_offset_of_selectOnUp_18() { return static_cast<int32_t>(offsetof(UiNavigationExplicitGetProperties_t4213199546, ___selectOnUp_18)); }
	inline FsmGameObject_t3581898942 * get_selectOnUp_18() const { return ___selectOnUp_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnUp_18() { return &___selectOnUp_18; }
	inline void set_selectOnUp_18(FsmGameObject_t3581898942 * value)
	{
		___selectOnUp_18 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnUp_18), value);
	}

	inline static int32_t get_offset_of_selectOnLeft_19() { return static_cast<int32_t>(offsetof(UiNavigationExplicitGetProperties_t4213199546, ___selectOnLeft_19)); }
	inline FsmGameObject_t3581898942 * get_selectOnLeft_19() const { return ___selectOnLeft_19; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnLeft_19() { return &___selectOnLeft_19; }
	inline void set_selectOnLeft_19(FsmGameObject_t3581898942 * value)
	{
		___selectOnLeft_19 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnLeft_19), value);
	}

	inline static int32_t get_offset_of_selectOnRight_20() { return static_cast<int32_t>(offsetof(UiNavigationExplicitGetProperties_t4213199546, ___selectOnRight_20)); }
	inline FsmGameObject_t3581898942 * get_selectOnRight_20() const { return ___selectOnRight_20; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnRight_20() { return &___selectOnRight_20; }
	inline void set_selectOnRight_20(FsmGameObject_t3581898942 * value)
	{
		___selectOnRight_20 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnRight_20), value);
	}

	inline static int32_t get_offset_of__selectable_21() { return static_cast<int32_t>(offsetof(UiNavigationExplicitGetProperties_t4213199546, ____selectable_21)); }
	inline Selectable_t3250028441 * get__selectable_21() const { return ____selectable_21; }
	inline Selectable_t3250028441 ** get_address_of__selectable_21() { return &____selectable_21; }
	inline void set__selectable_21(Selectable_t3250028441 * value)
	{
		____selectable_21 = value;
		Il2CppCodeGenWriteBarrier((&____selectable_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINAVIGATIONEXPLICITGETPROPERTIES_T4213199546_H
#ifndef UINAVIGATIONEXPLICITSETPROPERTIES_T3567312374_H
#define UINAVIGATIONEXPLICITSETPROPERTIES_T3567312374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties
struct  UiNavigationExplicitSetProperties_t3567312374  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::selectOnDown
	FsmGameObject_t3581898942 * ___selectOnDown_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::selectOnUp
	FsmGameObject_t3581898942 * ___selectOnUp_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::selectOnLeft
	FsmGameObject_t3581898942 * ___selectOnLeft_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::selectOnRight
	FsmGameObject_t3581898942 * ___selectOnRight_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_21;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::selectable
	Selectable_t3250028441 * ___selectable_22;
	// UnityEngine.UI.Navigation HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::navigation
	Navigation_t3049316579  ___navigation_23;
	// UnityEngine.UI.Navigation HutongGames.PlayMaker.Actions.UiNavigationExplicitSetProperties::originalState
	Navigation_t3049316579  ___originalState_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_selectOnDown_17() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___selectOnDown_17)); }
	inline FsmGameObject_t3581898942 * get_selectOnDown_17() const { return ___selectOnDown_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnDown_17() { return &___selectOnDown_17; }
	inline void set_selectOnDown_17(FsmGameObject_t3581898942 * value)
	{
		___selectOnDown_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnDown_17), value);
	}

	inline static int32_t get_offset_of_selectOnUp_18() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___selectOnUp_18)); }
	inline FsmGameObject_t3581898942 * get_selectOnUp_18() const { return ___selectOnUp_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnUp_18() { return &___selectOnUp_18; }
	inline void set_selectOnUp_18(FsmGameObject_t3581898942 * value)
	{
		___selectOnUp_18 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnUp_18), value);
	}

	inline static int32_t get_offset_of_selectOnLeft_19() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___selectOnLeft_19)); }
	inline FsmGameObject_t3581898942 * get_selectOnLeft_19() const { return ___selectOnLeft_19; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnLeft_19() { return &___selectOnLeft_19; }
	inline void set_selectOnLeft_19(FsmGameObject_t3581898942 * value)
	{
		___selectOnLeft_19 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnLeft_19), value);
	}

	inline static int32_t get_offset_of_selectOnRight_20() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___selectOnRight_20)); }
	inline FsmGameObject_t3581898942 * get_selectOnRight_20() const { return ___selectOnRight_20; }
	inline FsmGameObject_t3581898942 ** get_address_of_selectOnRight_20() { return &___selectOnRight_20; }
	inline void set_selectOnRight_20(FsmGameObject_t3581898942 * value)
	{
		___selectOnRight_20 = value;
		Il2CppCodeGenWriteBarrier((&___selectOnRight_20), value);
	}

	inline static int32_t get_offset_of_resetOnExit_21() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___resetOnExit_21)); }
	inline FsmBool_t163807967 * get_resetOnExit_21() const { return ___resetOnExit_21; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_21() { return &___resetOnExit_21; }
	inline void set_resetOnExit_21(FsmBool_t163807967 * value)
	{
		___resetOnExit_21 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_21), value);
	}

	inline static int32_t get_offset_of_selectable_22() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___selectable_22)); }
	inline Selectable_t3250028441 * get_selectable_22() const { return ___selectable_22; }
	inline Selectable_t3250028441 ** get_address_of_selectable_22() { return &___selectable_22; }
	inline void set_selectable_22(Selectable_t3250028441 * value)
	{
		___selectable_22 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_22), value);
	}

	inline static int32_t get_offset_of_navigation_23() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___navigation_23)); }
	inline Navigation_t3049316579  get_navigation_23() const { return ___navigation_23; }
	inline Navigation_t3049316579 * get_address_of_navigation_23() { return &___navigation_23; }
	inline void set_navigation_23(Navigation_t3049316579  value)
	{
		___navigation_23 = value;
	}

	inline static int32_t get_offset_of_originalState_24() { return static_cast<int32_t>(offsetof(UiNavigationExplicitSetProperties_t3567312374, ___originalState_24)); }
	inline Navigation_t3049316579  get_originalState_24() const { return ___originalState_24; }
	inline Navigation_t3049316579 * get_address_of_originalState_24() { return &___originalState_24; }
	inline void set_originalState_24(Navigation_t3049316579  value)
	{
		___originalState_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINAVIGATIONEXPLICITSETPROPERTIES_T3567312374_H
#ifndef UINAVIGATIONGETMODE_T2431581720_H
#define UINAVIGATIONGETMODE_T2431581720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiNavigationGetMode
struct  UiNavigationGetMode_t2431581720  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiNavigationGetMode::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiNavigationGetMode::navigationMode
	FsmString_t1785915204 * ___navigationMode_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiNavigationGetMode::automaticEvent
	FsmEvent_t3736299882 * ___automaticEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiNavigationGetMode::horizontalEvent
	FsmEvent_t3736299882 * ___horizontalEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiNavigationGetMode::verticalEvent
	FsmEvent_t3736299882 * ___verticalEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiNavigationGetMode::explicitEvent
	FsmEvent_t3736299882 * ___explicitEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiNavigationGetMode::noNavigationEvent
	FsmEvent_t3736299882 * ___noNavigationEvent_22;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiNavigationGetMode::selectable
	Selectable_t3250028441 * ___selectable_23;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.UiNavigationGetMode::originalTransition
	int32_t ___originalTransition_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_navigationMode_17() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___navigationMode_17)); }
	inline FsmString_t1785915204 * get_navigationMode_17() const { return ___navigationMode_17; }
	inline FsmString_t1785915204 ** get_address_of_navigationMode_17() { return &___navigationMode_17; }
	inline void set_navigationMode_17(FsmString_t1785915204 * value)
	{
		___navigationMode_17 = value;
		Il2CppCodeGenWriteBarrier((&___navigationMode_17), value);
	}

	inline static int32_t get_offset_of_automaticEvent_18() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___automaticEvent_18)); }
	inline FsmEvent_t3736299882 * get_automaticEvent_18() const { return ___automaticEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_automaticEvent_18() { return &___automaticEvent_18; }
	inline void set_automaticEvent_18(FsmEvent_t3736299882 * value)
	{
		___automaticEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___automaticEvent_18), value);
	}

	inline static int32_t get_offset_of_horizontalEvent_19() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___horizontalEvent_19)); }
	inline FsmEvent_t3736299882 * get_horizontalEvent_19() const { return ___horizontalEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_horizontalEvent_19() { return &___horizontalEvent_19; }
	inline void set_horizontalEvent_19(FsmEvent_t3736299882 * value)
	{
		___horizontalEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalEvent_19), value);
	}

	inline static int32_t get_offset_of_verticalEvent_20() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___verticalEvent_20)); }
	inline FsmEvent_t3736299882 * get_verticalEvent_20() const { return ___verticalEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_verticalEvent_20() { return &___verticalEvent_20; }
	inline void set_verticalEvent_20(FsmEvent_t3736299882 * value)
	{
		___verticalEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___verticalEvent_20), value);
	}

	inline static int32_t get_offset_of_explicitEvent_21() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___explicitEvent_21)); }
	inline FsmEvent_t3736299882 * get_explicitEvent_21() const { return ___explicitEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_explicitEvent_21() { return &___explicitEvent_21; }
	inline void set_explicitEvent_21(FsmEvent_t3736299882 * value)
	{
		___explicitEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___explicitEvent_21), value);
	}

	inline static int32_t get_offset_of_noNavigationEvent_22() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___noNavigationEvent_22)); }
	inline FsmEvent_t3736299882 * get_noNavigationEvent_22() const { return ___noNavigationEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_noNavigationEvent_22() { return &___noNavigationEvent_22; }
	inline void set_noNavigationEvent_22(FsmEvent_t3736299882 * value)
	{
		___noNavigationEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___noNavigationEvent_22), value);
	}

	inline static int32_t get_offset_of_selectable_23() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___selectable_23)); }
	inline Selectable_t3250028441 * get_selectable_23() const { return ___selectable_23; }
	inline Selectable_t3250028441 ** get_address_of_selectable_23() { return &___selectable_23; }
	inline void set_selectable_23(Selectable_t3250028441 * value)
	{
		___selectable_23 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_23), value);
	}

	inline static int32_t get_offset_of_originalTransition_24() { return static_cast<int32_t>(offsetof(UiNavigationGetMode_t2431581720, ___originalTransition_24)); }
	inline int32_t get_originalTransition_24() const { return ___originalTransition_24; }
	inline int32_t* get_address_of_originalTransition_24() { return &___originalTransition_24; }
	inline void set_originalTransition_24(int32_t value)
	{
		___originalTransition_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINAVIGATIONGETMODE_T2431581720_H
#ifndef UINAVIGATIONSETMODE_T1356500756_H
#define UINAVIGATIONSETMODE_T1356500756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiNavigationSetMode
struct  UiNavigationSetMode_t1356500756  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiNavigationSetMode::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.UI.Navigation/Mode HutongGames.PlayMaker.Actions.UiNavigationSetMode::navigationMode
	int32_t ___navigationMode_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiNavigationSetMode::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiNavigationSetMode::selectable
	Selectable_t3250028441 * ___selectable_19;
	// UnityEngine.UI.Navigation HutongGames.PlayMaker.Actions.UiNavigationSetMode::_navigation
	Navigation_t3049316579  ____navigation_20;
	// UnityEngine.UI.Navigation/Mode HutongGames.PlayMaker.Actions.UiNavigationSetMode::originalValue
	int32_t ___originalValue_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiNavigationSetMode_t1356500756, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_navigationMode_17() { return static_cast<int32_t>(offsetof(UiNavigationSetMode_t1356500756, ___navigationMode_17)); }
	inline int32_t get_navigationMode_17() const { return ___navigationMode_17; }
	inline int32_t* get_address_of_navigationMode_17() { return &___navigationMode_17; }
	inline void set_navigationMode_17(int32_t value)
	{
		___navigationMode_17 = value;
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiNavigationSetMode_t1356500756, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_selectable_19() { return static_cast<int32_t>(offsetof(UiNavigationSetMode_t1356500756, ___selectable_19)); }
	inline Selectable_t3250028441 * get_selectable_19() const { return ___selectable_19; }
	inline Selectable_t3250028441 ** get_address_of_selectable_19() { return &___selectable_19; }
	inline void set_selectable_19(Selectable_t3250028441 * value)
	{
		___selectable_19 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_19), value);
	}

	inline static int32_t get_offset_of__navigation_20() { return static_cast<int32_t>(offsetof(UiNavigationSetMode_t1356500756, ____navigation_20)); }
	inline Navigation_t3049316579  get__navigation_20() const { return ____navigation_20; }
	inline Navigation_t3049316579 * get_address_of__navigation_20() { return &____navigation_20; }
	inline void set__navigation_20(Navigation_t3049316579  value)
	{
		____navigation_20 = value;
	}

	inline static int32_t get_offset_of_originalValue_21() { return static_cast<int32_t>(offsetof(UiNavigationSetMode_t1356500756, ___originalValue_21)); }
	inline int32_t get_originalValue_21() const { return ___originalValue_21; }
	inline int32_t* get_address_of_originalValue_21() { return &___originalValue_21; }
	inline void set_originalValue_21(int32_t value)
	{
		___originalValue_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINAVIGATIONSETMODE_T1356500756_H
#ifndef UISETANIMATIONTRIGGERS_T493328399_H
#define UISETANIMATIONTRIGGERS_T493328399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSetAnimationTriggers
struct  UiSetAnimationTriggers_t493328399  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::normalTrigger
	FsmString_t1785915204 * ___normalTrigger_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::highlightedTrigger
	FsmString_t1785915204 * ___highlightedTrigger_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::pressedTrigger
	FsmString_t1785915204 * ___pressedTrigger_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::disabledTrigger
	FsmString_t1785915204 * ___disabledTrigger_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_21;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::selectable
	Selectable_t3250028441 * ___selectable_22;
	// UnityEngine.UI.AnimationTriggers HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::_animationTriggers
	AnimationTriggers_t2532145056 * ____animationTriggers_23;
	// UnityEngine.UI.AnimationTriggers HutongGames.PlayMaker.Actions.UiSetAnimationTriggers::originalAnimationTriggers
	AnimationTriggers_t2532145056 * ___originalAnimationTriggers_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_normalTrigger_17() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___normalTrigger_17)); }
	inline FsmString_t1785915204 * get_normalTrigger_17() const { return ___normalTrigger_17; }
	inline FsmString_t1785915204 ** get_address_of_normalTrigger_17() { return &___normalTrigger_17; }
	inline void set_normalTrigger_17(FsmString_t1785915204 * value)
	{
		___normalTrigger_17 = value;
		Il2CppCodeGenWriteBarrier((&___normalTrigger_17), value);
	}

	inline static int32_t get_offset_of_highlightedTrigger_18() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___highlightedTrigger_18)); }
	inline FsmString_t1785915204 * get_highlightedTrigger_18() const { return ___highlightedTrigger_18; }
	inline FsmString_t1785915204 ** get_address_of_highlightedTrigger_18() { return &___highlightedTrigger_18; }
	inline void set_highlightedTrigger_18(FsmString_t1785915204 * value)
	{
		___highlightedTrigger_18 = value;
		Il2CppCodeGenWriteBarrier((&___highlightedTrigger_18), value);
	}

	inline static int32_t get_offset_of_pressedTrigger_19() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___pressedTrigger_19)); }
	inline FsmString_t1785915204 * get_pressedTrigger_19() const { return ___pressedTrigger_19; }
	inline FsmString_t1785915204 ** get_address_of_pressedTrigger_19() { return &___pressedTrigger_19; }
	inline void set_pressedTrigger_19(FsmString_t1785915204 * value)
	{
		___pressedTrigger_19 = value;
		Il2CppCodeGenWriteBarrier((&___pressedTrigger_19), value);
	}

	inline static int32_t get_offset_of_disabledTrigger_20() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___disabledTrigger_20)); }
	inline FsmString_t1785915204 * get_disabledTrigger_20() const { return ___disabledTrigger_20; }
	inline FsmString_t1785915204 ** get_address_of_disabledTrigger_20() { return &___disabledTrigger_20; }
	inline void set_disabledTrigger_20(FsmString_t1785915204 * value)
	{
		___disabledTrigger_20 = value;
		Il2CppCodeGenWriteBarrier((&___disabledTrigger_20), value);
	}

	inline static int32_t get_offset_of_resetOnExit_21() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___resetOnExit_21)); }
	inline FsmBool_t163807967 * get_resetOnExit_21() const { return ___resetOnExit_21; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_21() { return &___resetOnExit_21; }
	inline void set_resetOnExit_21(FsmBool_t163807967 * value)
	{
		___resetOnExit_21 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_21), value);
	}

	inline static int32_t get_offset_of_selectable_22() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___selectable_22)); }
	inline Selectable_t3250028441 * get_selectable_22() const { return ___selectable_22; }
	inline Selectable_t3250028441 ** get_address_of_selectable_22() { return &___selectable_22; }
	inline void set_selectable_22(Selectable_t3250028441 * value)
	{
		___selectable_22 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_22), value);
	}

	inline static int32_t get_offset_of__animationTriggers_23() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ____animationTriggers_23)); }
	inline AnimationTriggers_t2532145056 * get__animationTriggers_23() const { return ____animationTriggers_23; }
	inline AnimationTriggers_t2532145056 ** get_address_of__animationTriggers_23() { return &____animationTriggers_23; }
	inline void set__animationTriggers_23(AnimationTriggers_t2532145056 * value)
	{
		____animationTriggers_23 = value;
		Il2CppCodeGenWriteBarrier((&____animationTriggers_23), value);
	}

	inline static int32_t get_offset_of_originalAnimationTriggers_24() { return static_cast<int32_t>(offsetof(UiSetAnimationTriggers_t493328399, ___originalAnimationTriggers_24)); }
	inline AnimationTriggers_t2532145056 * get_originalAnimationTriggers_24() const { return ___originalAnimationTriggers_24; }
	inline AnimationTriggers_t2532145056 ** get_address_of_originalAnimationTriggers_24() { return &___originalAnimationTriggers_24; }
	inline void set_originalAnimationTriggers_24(AnimationTriggers_t2532145056 * value)
	{
		___originalAnimationTriggers_24 = value;
		Il2CppCodeGenWriteBarrier((&___originalAnimationTriggers_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISETANIMATIONTRIGGERS_T493328399_H
#ifndef UISETCOLORBLOCK_T2506035845_H
#define UISETCOLORBLOCK_T2506035845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiSetColorBlock
struct  UiSetColorBlock_t2506035845  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiSetColorBlock::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSetColorBlock::fadeDuration
	FsmFloat_t2883254149 * ___fadeDuration_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UiSetColorBlock::colorMultiplier
	FsmFloat_t2883254149 * ___colorMultiplier_18;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiSetColorBlock::normalColor
	FsmColor_t1738900188 * ___normalColor_19;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiSetColorBlock::pressedColor
	FsmColor_t1738900188 * ___pressedColor_20;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiSetColorBlock::highlightedColor
	FsmColor_t1738900188 * ___highlightedColor_21;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.UiSetColorBlock::disabledColor
	FsmColor_t1738900188 * ___disabledColor_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiSetColorBlock::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_23;
	// System.Boolean HutongGames.PlayMaker.Actions.UiSetColorBlock::everyFrame
	bool ___everyFrame_24;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiSetColorBlock::selectable
	Selectable_t3250028441 * ___selectable_25;
	// UnityEngine.UI.ColorBlock HutongGames.PlayMaker.Actions.UiSetColorBlock::_colorBlock
	ColorBlock_t2139031574  ____colorBlock_26;
	// UnityEngine.UI.ColorBlock HutongGames.PlayMaker.Actions.UiSetColorBlock::originalColorBlock
	ColorBlock_t2139031574  ___originalColorBlock_27;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_fadeDuration_17() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___fadeDuration_17)); }
	inline FsmFloat_t2883254149 * get_fadeDuration_17() const { return ___fadeDuration_17; }
	inline FsmFloat_t2883254149 ** get_address_of_fadeDuration_17() { return &___fadeDuration_17; }
	inline void set_fadeDuration_17(FsmFloat_t2883254149 * value)
	{
		___fadeDuration_17 = value;
		Il2CppCodeGenWriteBarrier((&___fadeDuration_17), value);
	}

	inline static int32_t get_offset_of_colorMultiplier_18() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___colorMultiplier_18)); }
	inline FsmFloat_t2883254149 * get_colorMultiplier_18() const { return ___colorMultiplier_18; }
	inline FsmFloat_t2883254149 ** get_address_of_colorMultiplier_18() { return &___colorMultiplier_18; }
	inline void set_colorMultiplier_18(FsmFloat_t2883254149 * value)
	{
		___colorMultiplier_18 = value;
		Il2CppCodeGenWriteBarrier((&___colorMultiplier_18), value);
	}

	inline static int32_t get_offset_of_normalColor_19() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___normalColor_19)); }
	inline FsmColor_t1738900188 * get_normalColor_19() const { return ___normalColor_19; }
	inline FsmColor_t1738900188 ** get_address_of_normalColor_19() { return &___normalColor_19; }
	inline void set_normalColor_19(FsmColor_t1738900188 * value)
	{
		___normalColor_19 = value;
		Il2CppCodeGenWriteBarrier((&___normalColor_19), value);
	}

	inline static int32_t get_offset_of_pressedColor_20() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___pressedColor_20)); }
	inline FsmColor_t1738900188 * get_pressedColor_20() const { return ___pressedColor_20; }
	inline FsmColor_t1738900188 ** get_address_of_pressedColor_20() { return &___pressedColor_20; }
	inline void set_pressedColor_20(FsmColor_t1738900188 * value)
	{
		___pressedColor_20 = value;
		Il2CppCodeGenWriteBarrier((&___pressedColor_20), value);
	}

	inline static int32_t get_offset_of_highlightedColor_21() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___highlightedColor_21)); }
	inline FsmColor_t1738900188 * get_highlightedColor_21() const { return ___highlightedColor_21; }
	inline FsmColor_t1738900188 ** get_address_of_highlightedColor_21() { return &___highlightedColor_21; }
	inline void set_highlightedColor_21(FsmColor_t1738900188 * value)
	{
		___highlightedColor_21 = value;
		Il2CppCodeGenWriteBarrier((&___highlightedColor_21), value);
	}

	inline static int32_t get_offset_of_disabledColor_22() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___disabledColor_22)); }
	inline FsmColor_t1738900188 * get_disabledColor_22() const { return ___disabledColor_22; }
	inline FsmColor_t1738900188 ** get_address_of_disabledColor_22() { return &___disabledColor_22; }
	inline void set_disabledColor_22(FsmColor_t1738900188 * value)
	{
		___disabledColor_22 = value;
		Il2CppCodeGenWriteBarrier((&___disabledColor_22), value);
	}

	inline static int32_t get_offset_of_resetOnExit_23() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___resetOnExit_23)); }
	inline FsmBool_t163807967 * get_resetOnExit_23() const { return ___resetOnExit_23; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_23() { return &___resetOnExit_23; }
	inline void set_resetOnExit_23(FsmBool_t163807967 * value)
	{
		___resetOnExit_23 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_23), value);
	}

	inline static int32_t get_offset_of_everyFrame_24() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___everyFrame_24)); }
	inline bool get_everyFrame_24() const { return ___everyFrame_24; }
	inline bool* get_address_of_everyFrame_24() { return &___everyFrame_24; }
	inline void set_everyFrame_24(bool value)
	{
		___everyFrame_24 = value;
	}

	inline static int32_t get_offset_of_selectable_25() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___selectable_25)); }
	inline Selectable_t3250028441 * get_selectable_25() const { return ___selectable_25; }
	inline Selectable_t3250028441 ** get_address_of_selectable_25() { return &___selectable_25; }
	inline void set_selectable_25(Selectable_t3250028441 * value)
	{
		___selectable_25 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_25), value);
	}

	inline static int32_t get_offset_of__colorBlock_26() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ____colorBlock_26)); }
	inline ColorBlock_t2139031574  get__colorBlock_26() const { return ____colorBlock_26; }
	inline ColorBlock_t2139031574 * get_address_of__colorBlock_26() { return &____colorBlock_26; }
	inline void set__colorBlock_26(ColorBlock_t2139031574  value)
	{
		____colorBlock_26 = value;
	}

	inline static int32_t get_offset_of_originalColorBlock_27() { return static_cast<int32_t>(offsetof(UiSetColorBlock_t2506035845, ___originalColorBlock_27)); }
	inline ColorBlock_t2139031574  get_originalColorBlock_27() const { return ___originalColorBlock_27; }
	inline ColorBlock_t2139031574 * get_address_of_originalColorBlock_27() { return &___originalColorBlock_27; }
	inline void set_originalColorBlock_27(ColorBlock_t2139031574  value)
	{
		___originalColorBlock_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISETCOLORBLOCK_T2506035845_H
#ifndef UITRANSITIONGETTYPE_T845750375_H
#define UITRANSITIONGETTYPE_T845750375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiTransitionGetType
struct  UiTransitionGetType_t845750375  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiTransitionGetType::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UiTransitionGetType::transition
	FsmString_t1785915204 * ___transition_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiTransitionGetType::colorTintEvent
	FsmEvent_t3736299882 * ___colorTintEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiTransitionGetType::spriteSwapEvent
	FsmEvent_t3736299882 * ___spriteSwapEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiTransitionGetType::animationEvent
	FsmEvent_t3736299882 * ___animationEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiTransitionGetType::noTransitionEvent
	FsmEvent_t3736299882 * ___noTransitionEvent_21;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiTransitionGetType::selectable
	Selectable_t3250028441 * ___selectable_22;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.UiTransitionGetType::originalTransition
	int32_t ___originalTransition_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_transition_17() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___transition_17)); }
	inline FsmString_t1785915204 * get_transition_17() const { return ___transition_17; }
	inline FsmString_t1785915204 ** get_address_of_transition_17() { return &___transition_17; }
	inline void set_transition_17(FsmString_t1785915204 * value)
	{
		___transition_17 = value;
		Il2CppCodeGenWriteBarrier((&___transition_17), value);
	}

	inline static int32_t get_offset_of_colorTintEvent_18() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___colorTintEvent_18)); }
	inline FsmEvent_t3736299882 * get_colorTintEvent_18() const { return ___colorTintEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_colorTintEvent_18() { return &___colorTintEvent_18; }
	inline void set_colorTintEvent_18(FsmEvent_t3736299882 * value)
	{
		___colorTintEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___colorTintEvent_18), value);
	}

	inline static int32_t get_offset_of_spriteSwapEvent_19() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___spriteSwapEvent_19)); }
	inline FsmEvent_t3736299882 * get_spriteSwapEvent_19() const { return ___spriteSwapEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_spriteSwapEvent_19() { return &___spriteSwapEvent_19; }
	inline void set_spriteSwapEvent_19(FsmEvent_t3736299882 * value)
	{
		___spriteSwapEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___spriteSwapEvent_19), value);
	}

	inline static int32_t get_offset_of_animationEvent_20() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___animationEvent_20)); }
	inline FsmEvent_t3736299882 * get_animationEvent_20() const { return ___animationEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_animationEvent_20() { return &___animationEvent_20; }
	inline void set_animationEvent_20(FsmEvent_t3736299882 * value)
	{
		___animationEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___animationEvent_20), value);
	}

	inline static int32_t get_offset_of_noTransitionEvent_21() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___noTransitionEvent_21)); }
	inline FsmEvent_t3736299882 * get_noTransitionEvent_21() const { return ___noTransitionEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_noTransitionEvent_21() { return &___noTransitionEvent_21; }
	inline void set_noTransitionEvent_21(FsmEvent_t3736299882 * value)
	{
		___noTransitionEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___noTransitionEvent_21), value);
	}

	inline static int32_t get_offset_of_selectable_22() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___selectable_22)); }
	inline Selectable_t3250028441 * get_selectable_22() const { return ___selectable_22; }
	inline Selectable_t3250028441 ** get_address_of_selectable_22() { return &___selectable_22; }
	inline void set_selectable_22(Selectable_t3250028441 * value)
	{
		___selectable_22 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_22), value);
	}

	inline static int32_t get_offset_of_originalTransition_23() { return static_cast<int32_t>(offsetof(UiTransitionGetType_t845750375, ___originalTransition_23)); }
	inline int32_t get_originalTransition_23() const { return ___originalTransition_23; }
	inline int32_t* get_address_of_originalTransition_23() { return &___originalTransition_23; }
	inline void set_originalTransition_23(int32_t value)
	{
		___originalTransition_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITRANSITIONGETTYPE_T845750375_H
#ifndef UITRANSITIONSETTYPE_T798954539_H
#define UITRANSITIONSETTYPE_T798954539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiTransitionSetType
struct  UiTransitionSetType_t798954539  : public ComponentAction_1_t1832348367
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UiTransitionSetType::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.UiTransitionSetType::transition
	int32_t ___transition_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UiTransitionSetType::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.UiTransitionSetType::selectable
	Selectable_t3250028441 * ___selectable_19;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.UiTransitionSetType::originalTransition
	int32_t ___originalTransition_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UiTransitionSetType_t798954539, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_transition_17() { return static_cast<int32_t>(offsetof(UiTransitionSetType_t798954539, ___transition_17)); }
	inline int32_t get_transition_17() const { return ___transition_17; }
	inline int32_t* get_address_of_transition_17() { return &___transition_17; }
	inline void set_transition_17(int32_t value)
	{
		___transition_17 = value;
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(UiTransitionSetType_t798954539, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_selectable_19() { return static_cast<int32_t>(offsetof(UiTransitionSetType_t798954539, ___selectable_19)); }
	inline Selectable_t3250028441 * get_selectable_19() const { return ___selectable_19; }
	inline Selectable_t3250028441 ** get_address_of_selectable_19() { return &___selectable_19; }
	inline void set_selectable_19(Selectable_t3250028441 * value)
	{
		___selectable_19 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_19), value);
	}

	inline static int32_t get_offset_of_originalTransition_20() { return static_cast<int32_t>(offsetof(UiTransitionSetType_t798954539, ___originalTransition_20)); }
	inline int32_t get_originalTransition_20() const { return ___originalTransition_20; }
	inline int32_t* get_address_of_originalTransition_20() { return &___originalTransition_20; }
	inline void set_originalTransition_20(int32_t value)
	{
		___originalTransition_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITRANSITIONSETTYPE_T798954539_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef UIONBEGINDRAGEVENT_T3639786799_H
#define UIONBEGINDRAGEVENT_T3639786799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnBeginDragEvent
struct  UiOnBeginDragEvent_t3639786799  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnBeginDragEvent::onBeginDragEvent
	FsmEvent_t3736299882 * ___onBeginDragEvent_20;

public:
	inline static int32_t get_offset_of_onBeginDragEvent_20() { return static_cast<int32_t>(offsetof(UiOnBeginDragEvent_t3639786799, ___onBeginDragEvent_20)); }
	inline FsmEvent_t3736299882 * get_onBeginDragEvent_20() const { return ___onBeginDragEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onBeginDragEvent_20() { return &___onBeginDragEvent_20; }
	inline void set_onBeginDragEvent_20(FsmEvent_t3736299882 * value)
	{
		___onBeginDragEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginDragEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONBEGINDRAGEVENT_T3639786799_H
#ifndef UIONCANCELEVENT_T3485713958_H
#define UIONCANCELEVENT_T3485713958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnCancelEvent
struct  UiOnCancelEvent_t3485713958  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnCancelEvent::onCancelEvent
	FsmEvent_t3736299882 * ___onCancelEvent_20;

public:
	inline static int32_t get_offset_of_onCancelEvent_20() { return static_cast<int32_t>(offsetof(UiOnCancelEvent_t3485713958, ___onCancelEvent_20)); }
	inline FsmEvent_t3736299882 * get_onCancelEvent_20() const { return ___onCancelEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onCancelEvent_20() { return &___onCancelEvent_20; }
	inline void set_onCancelEvent_20(FsmEvent_t3736299882 * value)
	{
		___onCancelEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onCancelEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONCANCELEVENT_T3485713958_H
#ifndef UIONDESELECTEVENT_T3391393454_H
#define UIONDESELECTEVENT_T3391393454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnDeselectEvent
struct  UiOnDeselectEvent_t3391393454  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnDeselectEvent::onDeselectEvent
	FsmEvent_t3736299882 * ___onDeselectEvent_20;

public:
	inline static int32_t get_offset_of_onDeselectEvent_20() { return static_cast<int32_t>(offsetof(UiOnDeselectEvent_t3391393454, ___onDeselectEvent_20)); }
	inline FsmEvent_t3736299882 * get_onDeselectEvent_20() const { return ___onDeselectEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onDeselectEvent_20() { return &___onDeselectEvent_20; }
	inline void set_onDeselectEvent_20(FsmEvent_t3736299882 * value)
	{
		___onDeselectEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onDeselectEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONDESELECTEVENT_T3391393454_H
#ifndef UIONDRAGEVENT_T2141365689_H
#define UIONDRAGEVENT_T2141365689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnDragEvent
struct  UiOnDragEvent_t2141365689  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnDragEvent::onDragEvent
	FsmEvent_t3736299882 * ___onDragEvent_20;

public:
	inline static int32_t get_offset_of_onDragEvent_20() { return static_cast<int32_t>(offsetof(UiOnDragEvent_t2141365689, ___onDragEvent_20)); }
	inline FsmEvent_t3736299882 * get_onDragEvent_20() const { return ___onDragEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onDragEvent_20() { return &___onDragEvent_20; }
	inline void set_onDragEvent_20(FsmEvent_t3736299882 * value)
	{
		___onDragEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onDragEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONDRAGEVENT_T2141365689_H
#ifndef UIONDROPEVENT_T2090312951_H
#define UIONDROPEVENT_T2090312951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnDropEvent
struct  UiOnDropEvent_t2090312951  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnDropEvent::onDropEvent
	FsmEvent_t3736299882 * ___onDropEvent_20;

public:
	inline static int32_t get_offset_of_onDropEvent_20() { return static_cast<int32_t>(offsetof(UiOnDropEvent_t2090312951, ___onDropEvent_20)); }
	inline FsmEvent_t3736299882 * get_onDropEvent_20() const { return ___onDropEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onDropEvent_20() { return &___onDropEvent_20; }
	inline void set_onDropEvent_20(FsmEvent_t3736299882 * value)
	{
		___onDropEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onDropEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONDROPEVENT_T2090312951_H
#ifndef UIONENDDRAGEVENT_T1898429554_H
#define UIONENDDRAGEVENT_T1898429554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnEndDragEvent
struct  UiOnEndDragEvent_t1898429554  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnEndDragEvent::onEndDragEvent
	FsmEvent_t3736299882 * ___onEndDragEvent_20;

public:
	inline static int32_t get_offset_of_onEndDragEvent_20() { return static_cast<int32_t>(offsetof(UiOnEndDragEvent_t1898429554, ___onEndDragEvent_20)); }
	inline FsmEvent_t3736299882 * get_onEndDragEvent_20() const { return ___onEndDragEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onEndDragEvent_20() { return &___onEndDragEvent_20; }
	inline void set_onEndDragEvent_20(FsmEvent_t3736299882 * value)
	{
		___onEndDragEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onEndDragEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONENDDRAGEVENT_T1898429554_H
#ifndef UIONINITIALIZEPOTENTIALDRAGEVENT_T1975688602_H
#define UIONINITIALIZEPOTENTIALDRAGEVENT_T1975688602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnInitializePotentialDragEvent
struct  UiOnInitializePotentialDragEvent_t1975688602  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnInitializePotentialDragEvent::onInitializePotentialDragEvent
	FsmEvent_t3736299882 * ___onInitializePotentialDragEvent_20;

public:
	inline static int32_t get_offset_of_onInitializePotentialDragEvent_20() { return static_cast<int32_t>(offsetof(UiOnInitializePotentialDragEvent_t1975688602, ___onInitializePotentialDragEvent_20)); }
	inline FsmEvent_t3736299882 * get_onInitializePotentialDragEvent_20() const { return ___onInitializePotentialDragEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onInitializePotentialDragEvent_20() { return &___onInitializePotentialDragEvent_20; }
	inline void set_onInitializePotentialDragEvent_20(FsmEvent_t3736299882 * value)
	{
		___onInitializePotentialDragEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onInitializePotentialDragEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONINITIALIZEPOTENTIALDRAGEVENT_T1975688602_H
#ifndef UIONMOVEEVENT_T3747541532_H
#define UIONMOVEEVENT_T3747541532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnMoveEvent
struct  UiOnMoveEvent_t3747541532  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnMoveEvent::onMoveEvent
	FsmEvent_t3736299882 * ___onMoveEvent_20;

public:
	inline static int32_t get_offset_of_onMoveEvent_20() { return static_cast<int32_t>(offsetof(UiOnMoveEvent_t3747541532, ___onMoveEvent_20)); }
	inline FsmEvent_t3736299882 * get_onMoveEvent_20() const { return ___onMoveEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onMoveEvent_20() { return &___onMoveEvent_20; }
	inline void set_onMoveEvent_20(FsmEvent_t3736299882 * value)
	{
		___onMoveEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onMoveEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONMOVEEVENT_T3747541532_H
#ifndef UIONPOINTERCLICKEVENT_T696272602_H
#define UIONPOINTERCLICKEVENT_T696272602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnPointerClickEvent
struct  UiOnPointerClickEvent_t696272602  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnPointerClickEvent::onPointerClickEvent
	FsmEvent_t3736299882 * ___onPointerClickEvent_20;

public:
	inline static int32_t get_offset_of_onPointerClickEvent_20() { return static_cast<int32_t>(offsetof(UiOnPointerClickEvent_t696272602, ___onPointerClickEvent_20)); }
	inline FsmEvent_t3736299882 * get_onPointerClickEvent_20() const { return ___onPointerClickEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onPointerClickEvent_20() { return &___onPointerClickEvent_20; }
	inline void set_onPointerClickEvent_20(FsmEvent_t3736299882 * value)
	{
		___onPointerClickEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerClickEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONPOINTERCLICKEVENT_T696272602_H
#ifndef UIONPOINTERDOWNEVENT_T1426364170_H
#define UIONPOINTERDOWNEVENT_T1426364170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnPointerDownEvent
struct  UiOnPointerDownEvent_t1426364170  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnPointerDownEvent::onPointerDownEvent
	FsmEvent_t3736299882 * ___onPointerDownEvent_20;

public:
	inline static int32_t get_offset_of_onPointerDownEvent_20() { return static_cast<int32_t>(offsetof(UiOnPointerDownEvent_t1426364170, ___onPointerDownEvent_20)); }
	inline FsmEvent_t3736299882 * get_onPointerDownEvent_20() const { return ___onPointerDownEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onPointerDownEvent_20() { return &___onPointerDownEvent_20; }
	inline void set_onPointerDownEvent_20(FsmEvent_t3736299882 * value)
	{
		___onPointerDownEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerDownEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONPOINTERDOWNEVENT_T1426364170_H
#ifndef UIONPOINTERENTEREVENT_T3050051842_H
#define UIONPOINTERENTEREVENT_T3050051842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnPointerEnterEvent
struct  UiOnPointerEnterEvent_t3050051842  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnPointerEnterEvent::onPointerEnterEvent
	FsmEvent_t3736299882 * ___onPointerEnterEvent_20;

public:
	inline static int32_t get_offset_of_onPointerEnterEvent_20() { return static_cast<int32_t>(offsetof(UiOnPointerEnterEvent_t3050051842, ___onPointerEnterEvent_20)); }
	inline FsmEvent_t3736299882 * get_onPointerEnterEvent_20() const { return ___onPointerEnterEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onPointerEnterEvent_20() { return &___onPointerEnterEvent_20; }
	inline void set_onPointerEnterEvent_20(FsmEvent_t3736299882 * value)
	{
		___onPointerEnterEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerEnterEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONPOINTERENTEREVENT_T3050051842_H
#ifndef UIONPOINTEREXITEVENT_T2854665363_H
#define UIONPOINTEREXITEVENT_T2854665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnPointerExitEvent
struct  UiOnPointerExitEvent_t2854665363  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnPointerExitEvent::onPointerExitEvent
	FsmEvent_t3736299882 * ___onPointerExitEvent_20;

public:
	inline static int32_t get_offset_of_onPointerExitEvent_20() { return static_cast<int32_t>(offsetof(UiOnPointerExitEvent_t2854665363, ___onPointerExitEvent_20)); }
	inline FsmEvent_t3736299882 * get_onPointerExitEvent_20() const { return ___onPointerExitEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onPointerExitEvent_20() { return &___onPointerExitEvent_20; }
	inline void set_onPointerExitEvent_20(FsmEvent_t3736299882 * value)
	{
		___onPointerExitEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerExitEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONPOINTEREXITEVENT_T2854665363_H
#ifndef UIONPOINTERUPEVENT_T2144733083_H
#define UIONPOINTERUPEVENT_T2144733083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnPointerUpEvent
struct  UiOnPointerUpEvent_t2144733083  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnPointerUpEvent::onPointerUpEvent
	FsmEvent_t3736299882 * ___onPointerUpEvent_20;

public:
	inline static int32_t get_offset_of_onPointerUpEvent_20() { return static_cast<int32_t>(offsetof(UiOnPointerUpEvent_t2144733083, ___onPointerUpEvent_20)); }
	inline FsmEvent_t3736299882 * get_onPointerUpEvent_20() const { return ___onPointerUpEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onPointerUpEvent_20() { return &___onPointerUpEvent_20; }
	inline void set_onPointerUpEvent_20(FsmEvent_t3736299882 * value)
	{
		___onPointerUpEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerUpEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONPOINTERUPEVENT_T2144733083_H
#ifndef UIONSCROLLEVENT_T3739468943_H
#define UIONSCROLLEVENT_T3739468943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnScrollEvent
struct  UiOnScrollEvent_t3739468943  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnScrollEvent::onScrollEvent
	FsmEvent_t3736299882 * ___onScrollEvent_20;

public:
	inline static int32_t get_offset_of_onScrollEvent_20() { return static_cast<int32_t>(offsetof(UiOnScrollEvent_t3739468943, ___onScrollEvent_20)); }
	inline FsmEvent_t3736299882 * get_onScrollEvent_20() const { return ___onScrollEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onScrollEvent_20() { return &___onScrollEvent_20; }
	inline void set_onScrollEvent_20(FsmEvent_t3736299882 * value)
	{
		___onScrollEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onScrollEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONSCROLLEVENT_T3739468943_H
#ifndef UIONSELECTEVENT_T3633769758_H
#define UIONSELECTEVENT_T3633769758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnSelectEvent
struct  UiOnSelectEvent_t3633769758  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnSelectEvent::onSelectEvent
	FsmEvent_t3736299882 * ___onSelectEvent_20;

public:
	inline static int32_t get_offset_of_onSelectEvent_20() { return static_cast<int32_t>(offsetof(UiOnSelectEvent_t3633769758, ___onSelectEvent_20)); }
	inline FsmEvent_t3736299882 * get_onSelectEvent_20() const { return ___onSelectEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onSelectEvent_20() { return &___onSelectEvent_20; }
	inline void set_onSelectEvent_20(FsmEvent_t3736299882 * value)
	{
		___onSelectEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onSelectEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONSELECTEVENT_T3633769758_H
#ifndef UIONSUBMITEVENT_T1986591334_H
#define UIONSUBMITEVENT_T1986591334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnSubmitEvent
struct  UiOnSubmitEvent_t1986591334  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnSubmitEvent::onSubmitEvent
	FsmEvent_t3736299882 * ___onSubmitEvent_20;

public:
	inline static int32_t get_offset_of_onSubmitEvent_20() { return static_cast<int32_t>(offsetof(UiOnSubmitEvent_t1986591334, ___onSubmitEvent_20)); }
	inline FsmEvent_t3736299882 * get_onSubmitEvent_20() const { return ___onSubmitEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onSubmitEvent_20() { return &___onSubmitEvent_20; }
	inline void set_onSubmitEvent_20(FsmEvent_t3736299882 * value)
	{
		___onSubmitEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmitEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONSUBMITEVENT_T1986591334_H
#ifndef UIONUPDATESELECTEDEVENT_T543813073_H
#define UIONUPDATESELECTEDEVENT_T543813073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UiOnUpdateSelectedEvent
struct  UiOnUpdateSelectedEvent_t543813073  : public EventTriggerActionBase_t72041308
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UiOnUpdateSelectedEvent::onUpdateSelectedEvent
	FsmEvent_t3736299882 * ___onUpdateSelectedEvent_20;

public:
	inline static int32_t get_offset_of_onUpdateSelectedEvent_20() { return static_cast<int32_t>(offsetof(UiOnUpdateSelectedEvent_t543813073, ___onUpdateSelectedEvent_20)); }
	inline FsmEvent_t3736299882 * get_onUpdateSelectedEvent_20() const { return ___onUpdateSelectedEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_onUpdateSelectedEvent_20() { return &___onUpdateSelectedEvent_20; }
	inline void set_onUpdateSelectedEvent_20(FsmEvent_t3736299882 * value)
	{
		___onUpdateSelectedEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateSelectedEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIONUPDATESELECTEDEVENT_T543813073_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PLAYMAKERCANVASRAYCASTFILTERPROXY_T3073240404_H
#define PLAYMAKERCANVASRAYCASTFILTERPROXY_T3073240404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerCanvasRaycastFilterProxy
struct  PlayMakerCanvasRaycastFilterProxy_t3073240404  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean HutongGames.PlayMaker.PlayMakerCanvasRaycastFilterProxy::RayCastingEnabled
	bool ___RayCastingEnabled_4;

public:
	inline static int32_t get_offset_of_RayCastingEnabled_4() { return static_cast<int32_t>(offsetof(PlayMakerCanvasRaycastFilterProxy_t3073240404, ___RayCastingEnabled_4)); }
	inline bool get_RayCastingEnabled_4() const { return ___RayCastingEnabled_4; }
	inline bool* get_address_of_RayCastingEnabled_4() { return &___RayCastingEnabled_4; }
	inline void set_RayCastingEnabled_4(bool value)
	{
		___RayCastingEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCANVASRAYCASTFILTERPROXY_T3073240404_H
#ifndef PLAYMAKERUIEVENTBASE_T1707648030_H
#define PLAYMAKERUIEVENTBASE_T1707648030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiEventBase
struct  PlayMakerUiEventBase_t1707648030  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> HutongGames.PlayMaker.PlayMakerUiEventBase::targetFsms
	List_1_t3085084973 * ___targetFsms_4;
	// System.Boolean HutongGames.PlayMaker.PlayMakerUiEventBase::initialized
	bool ___initialized_5;

public:
	inline static int32_t get_offset_of_targetFsms_4() { return static_cast<int32_t>(offsetof(PlayMakerUiEventBase_t1707648030, ___targetFsms_4)); }
	inline List_1_t3085084973 * get_targetFsms_4() const { return ___targetFsms_4; }
	inline List_1_t3085084973 ** get_address_of_targetFsms_4() { return &___targetFsms_4; }
	inline void set_targetFsms_4(List_1_t3085084973 * value)
	{
		___targetFsms_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetFsms_4), value);
	}

	inline static int32_t get_offset_of_initialized_5() { return static_cast<int32_t>(offsetof(PlayMakerUiEventBase_t1707648030, ___initialized_5)); }
	inline bool get_initialized_5() const { return ___initialized_5; }
	inline bool* get_address_of_initialized_5() { return &___initialized_5; }
	inline void set_initialized_5(bool value)
	{
		___initialized_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIEVENTBASE_T1707648030_H
#ifndef PLAYMAKERUIBOOLVALUECHANGEDEVENT_T793166467_H
#define PLAYMAKERUIBOOLVALUECHANGEDEVENT_T793166467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiBoolValueChangedEvent
struct  PlayMakerUiBoolValueChangedEvent_t793166467  : public PlayMakerUiEventBase_t1707648030
{
public:
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.PlayMakerUiBoolValueChangedEvent::toggle
	Toggle_t2735377061 * ___toggle_6;

public:
	inline static int32_t get_offset_of_toggle_6() { return static_cast<int32_t>(offsetof(PlayMakerUiBoolValueChangedEvent_t793166467, ___toggle_6)); }
	inline Toggle_t2735377061 * get_toggle_6() const { return ___toggle_6; }
	inline Toggle_t2735377061 ** get_address_of_toggle_6() { return &___toggle_6; }
	inline void set_toggle_6(Toggle_t2735377061 * value)
	{
		___toggle_6 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIBOOLVALUECHANGEDEVENT_T793166467_H
#ifndef PLAYMAKERUICLICKEVENT_T2847625480_H
#define PLAYMAKERUICLICKEVENT_T2847625480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiClickEvent
struct  PlayMakerUiClickEvent_t2847625480  : public PlayMakerUiEventBase_t1707648030
{
public:
	// UnityEngine.UI.Button HutongGames.PlayMaker.PlayMakerUiClickEvent::button
	Button_t4055032469 * ___button_6;

public:
	inline static int32_t get_offset_of_button_6() { return static_cast<int32_t>(offsetof(PlayMakerUiClickEvent_t2847625480, ___button_6)); }
	inline Button_t4055032469 * get_button_6() const { return ___button_6; }
	inline Button_t4055032469 ** get_address_of_button_6() { return &___button_6; }
	inline void set_button_6(Button_t4055032469 * value)
	{
		___button_6 = value;
		Il2CppCodeGenWriteBarrier((&___button_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUICLICKEVENT_T2847625480_H
#ifndef PLAYMAKERUIDRAGEVENTS_T799663687_H
#define PLAYMAKERUIDRAGEVENTS_T799663687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiDragEvents
struct  PlayMakerUiDragEvents_t799663687  : public PlayMakerUiEventBase_t1707648030
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIDRAGEVENTS_T799663687_H
#ifndef PLAYMAKERUIDROPEVENT_T709928077_H
#define PLAYMAKERUIDROPEVENT_T709928077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiDropEvent
struct  PlayMakerUiDropEvent_t709928077  : public PlayMakerUiEventBase_t1707648030
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIDROPEVENT_T709928077_H
#ifndef PLAYMAKERUIENDEDITEVENT_T2584568241_H
#define PLAYMAKERUIENDEDITEVENT_T2584568241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiEndEditEvent
struct  PlayMakerUiEndEditEvent_t2584568241  : public PlayMakerUiEventBase_t1707648030
{
public:
	// UnityEngine.UI.InputField HutongGames.PlayMaker.PlayMakerUiEndEditEvent::inputField
	InputField_t3762917431 * ___inputField_6;

public:
	inline static int32_t get_offset_of_inputField_6() { return static_cast<int32_t>(offsetof(PlayMakerUiEndEditEvent_t2584568241, ___inputField_6)); }
	inline InputField_t3762917431 * get_inputField_6() const { return ___inputField_6; }
	inline InputField_t3762917431 ** get_address_of_inputField_6() { return &___inputField_6; }
	inline void set_inputField_6(InputField_t3762917431 * value)
	{
		___inputField_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIENDEDITEVENT_T2584568241_H
#ifndef PLAYMAKERUIFLOATVALUECHANGEDEVENT_T1964756808_H
#define PLAYMAKERUIFLOATVALUECHANGEDEVENT_T1964756808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiFloatValueChangedEvent
struct  PlayMakerUiFloatValueChangedEvent_t1964756808  : public PlayMakerUiEventBase_t1707648030
{
public:
	// UnityEngine.UI.Slider HutongGames.PlayMaker.PlayMakerUiFloatValueChangedEvent::slider
	Slider_t3903728902 * ___slider_6;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.PlayMakerUiFloatValueChangedEvent::scrollbar
	Scrollbar_t1494447233 * ___scrollbar_7;

public:
	inline static int32_t get_offset_of_slider_6() { return static_cast<int32_t>(offsetof(PlayMakerUiFloatValueChangedEvent_t1964756808, ___slider_6)); }
	inline Slider_t3903728902 * get_slider_6() const { return ___slider_6; }
	inline Slider_t3903728902 ** get_address_of_slider_6() { return &___slider_6; }
	inline void set_slider_6(Slider_t3903728902 * value)
	{
		___slider_6 = value;
		Il2CppCodeGenWriteBarrier((&___slider_6), value);
	}

	inline static int32_t get_offset_of_scrollbar_7() { return static_cast<int32_t>(offsetof(PlayMakerUiFloatValueChangedEvent_t1964756808, ___scrollbar_7)); }
	inline Scrollbar_t1494447233 * get_scrollbar_7() const { return ___scrollbar_7; }
	inline Scrollbar_t1494447233 ** get_address_of_scrollbar_7() { return &___scrollbar_7; }
	inline void set_scrollbar_7(Scrollbar_t1494447233 * value)
	{
		___scrollbar_7 = value;
		Il2CppCodeGenWriteBarrier((&___scrollbar_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIFLOATVALUECHANGEDEVENT_T1964756808_H
#ifndef PLAYMAKERUIINTVALUECHANGEDEVENT_T3490394599_H
#define PLAYMAKERUIINTVALUECHANGEDEVENT_T3490394599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiIntValueChangedEvent
struct  PlayMakerUiIntValueChangedEvent_t3490394599  : public PlayMakerUiEventBase_t1707648030
{
public:
	// UnityEngine.UI.Dropdown HutongGames.PlayMaker.PlayMakerUiIntValueChangedEvent::dropdown
	Dropdown_t2274391225 * ___dropdown_6;

public:
	inline static int32_t get_offset_of_dropdown_6() { return static_cast<int32_t>(offsetof(PlayMakerUiIntValueChangedEvent_t3490394599, ___dropdown_6)); }
	inline Dropdown_t2274391225 * get_dropdown_6() const { return ___dropdown_6; }
	inline Dropdown_t2274391225 ** get_address_of_dropdown_6() { return &___dropdown_6; }
	inline void set_dropdown_6(Dropdown_t2274391225 * value)
	{
		___dropdown_6 = value;
		Il2CppCodeGenWriteBarrier((&___dropdown_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIINTVALUECHANGEDEVENT_T3490394599_H
#ifndef PLAYMAKERUIPOINTEREVENTS_T3538587227_H
#define PLAYMAKERUIPOINTEREVENTS_T3538587227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiPointerEvents
struct  PlayMakerUiPointerEvents_t3538587227  : public PlayMakerUiEventBase_t1707648030
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIPOINTEREVENTS_T3538587227_H
#ifndef PLAYMAKERUIVECTOR2VALUECHANGEDEVENT_T132838590_H
#define PLAYMAKERUIVECTOR2VALUECHANGEDEVENT_T132838590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PlayMakerUiVector2ValueChangedEvent
struct  PlayMakerUiVector2ValueChangedEvent_t132838590  : public PlayMakerUiEventBase_t1707648030
{
public:
	// UnityEngine.UI.ScrollRect HutongGames.PlayMaker.PlayMakerUiVector2ValueChangedEvent::scrollRect
	ScrollRect_t4137855814 * ___scrollRect_6;

public:
	inline static int32_t get_offset_of_scrollRect_6() { return static_cast<int32_t>(offsetof(PlayMakerUiVector2ValueChangedEvent_t132838590, ___scrollRect_6)); }
	inline ScrollRect_t4137855814 * get_scrollRect_6() const { return ___scrollRect_6; }
	inline ScrollRect_t4137855814 ** get_address_of_scrollRect_6() { return &___scrollRect_6; }
	inline void set_scrollRect_6(ScrollRect_t4137855814 * value)
	{
		___scrollRect_6 = value;
		Il2CppCodeGenWriteBarrier((&___scrollRect_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERUIVECTOR2VALUECHANGEDEVENT_T132838590_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3700 = { sizeof (SetPosition_t795544129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3700[8] = 
{
	SetPosition_t795544129::get_offset_of_gameObject_14(),
	SetPosition_t795544129::get_offset_of_vector_15(),
	SetPosition_t795544129::get_offset_of_x_16(),
	SetPosition_t795544129::get_offset_of_y_17(),
	SetPosition_t795544129::get_offset_of_z_18(),
	SetPosition_t795544129::get_offset_of_space_19(),
	SetPosition_t795544129::get_offset_of_everyFrame_20(),
	SetPosition_t795544129::get_offset_of_lateUpdate_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3701 = { sizeof (SetRandomRotation_t2959778138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3701[4] = 
{
	SetRandomRotation_t2959778138::get_offset_of_gameObject_14(),
	SetRandomRotation_t2959778138::get_offset_of_x_15(),
	SetRandomRotation_t2959778138::get_offset_of_y_16(),
	SetRandomRotation_t2959778138::get_offset_of_z_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3702 = { sizeof (SetRotation_t3293972810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3702[9] = 
{
	SetRotation_t3293972810::get_offset_of_gameObject_14(),
	SetRotation_t3293972810::get_offset_of_quaternion_15(),
	SetRotation_t3293972810::get_offset_of_vector_16(),
	SetRotation_t3293972810::get_offset_of_xAngle_17(),
	SetRotation_t3293972810::get_offset_of_yAngle_18(),
	SetRotation_t3293972810::get_offset_of_zAngle_19(),
	SetRotation_t3293972810::get_offset_of_space_20(),
	SetRotation_t3293972810::get_offset_of_everyFrame_21(),
	SetRotation_t3293972810::get_offset_of_lateUpdate_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3703 = { sizeof (SetScale_t1165161303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3703[7] = 
{
	SetScale_t1165161303::get_offset_of_gameObject_14(),
	SetScale_t1165161303::get_offset_of_vector_15(),
	SetScale_t1165161303::get_offset_of_x_16(),
	SetScale_t1165161303::get_offset_of_y_17(),
	SetScale_t1165161303::get_offset_of_z_18(),
	SetScale_t1165161303::get_offset_of_everyFrame_19(),
	SetScale_t1165161303::get_offset_of_lateUpdate_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3704 = { sizeof (SmoothFollowAction_t772972276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3704[10] = 
{
	SmoothFollowAction_t772972276::get_offset_of_gameObject_14(),
	SmoothFollowAction_t772972276::get_offset_of_targetObject_15(),
	SmoothFollowAction_t772972276::get_offset_of_distance_16(),
	SmoothFollowAction_t772972276::get_offset_of_height_17(),
	SmoothFollowAction_t772972276::get_offset_of_heightDamping_18(),
	SmoothFollowAction_t772972276::get_offset_of_rotationDamping_19(),
	SmoothFollowAction_t772972276::get_offset_of_cachedObject_20(),
	SmoothFollowAction_t772972276::get_offset_of_myTransform_21(),
	SmoothFollowAction_t772972276::get_offset_of_cachedTarget_22(),
	SmoothFollowAction_t772972276::get_offset_of_targetTransform_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3705 = { sizeof (SmoothLookAt_t3621050583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3705[12] = 
{
	SmoothLookAt_t3621050583::get_offset_of_gameObject_14(),
	SmoothLookAt_t3621050583::get_offset_of_targetObject_15(),
	SmoothLookAt_t3621050583::get_offset_of_targetPosition_16(),
	SmoothLookAt_t3621050583::get_offset_of_upVector_17(),
	SmoothLookAt_t3621050583::get_offset_of_keepVertical_18(),
	SmoothLookAt_t3621050583::get_offset_of_speed_19(),
	SmoothLookAt_t3621050583::get_offset_of_debug_20(),
	SmoothLookAt_t3621050583::get_offset_of_finishTolerance_21(),
	SmoothLookAt_t3621050583::get_offset_of_finishEvent_22(),
	SmoothLookAt_t3621050583::get_offset_of_previousGo_23(),
	SmoothLookAt_t3621050583::get_offset_of_lastRotation_24(),
	SmoothLookAt_t3621050583::get_offset_of_desiredRotation_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3706 = { sizeof (SmoothLookAtDirection_t1378160396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3706[12] = 
{
	SmoothLookAtDirection_t1378160396::get_offset_of_gameObject_14(),
	SmoothLookAtDirection_t1378160396::get_offset_of_targetDirection_15(),
	SmoothLookAtDirection_t1378160396::get_offset_of_minMagnitude_16(),
	SmoothLookAtDirection_t1378160396::get_offset_of_upVector_17(),
	SmoothLookAtDirection_t1378160396::get_offset_of_keepVertical_18(),
	SmoothLookAtDirection_t1378160396::get_offset_of_speed_19(),
	SmoothLookAtDirection_t1378160396::get_offset_of_lateUpdate_20(),
	SmoothLookAtDirection_t1378160396::get_offset_of_finishEvent_21(),
	SmoothLookAtDirection_t1378160396::get_offset_of_finish_22(),
	SmoothLookAtDirection_t1378160396::get_offset_of_previousGo_23(),
	SmoothLookAtDirection_t1378160396::get_offset_of_lastRotation_24(),
	SmoothLookAtDirection_t1378160396::get_offset_of_desiredRotation_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3707 = { sizeof (TransformDirection_t1398199187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3707[4] = 
{
	TransformDirection_t1398199187::get_offset_of_gameObject_14(),
	TransformDirection_t1398199187::get_offset_of_localDirection_15(),
	TransformDirection_t1398199187::get_offset_of_storeResult_16(),
	TransformDirection_t1398199187::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3708 = { sizeof (TransformPoint_t592873837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3708[4] = 
{
	TransformPoint_t592873837::get_offset_of_gameObject_14(),
	TransformPoint_t592873837::get_offset_of_localPosition_15(),
	TransformPoint_t592873837::get_offset_of_storeResult_16(),
	TransformPoint_t592873837::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3709 = { sizeof (Translate_t2146470009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3709[10] = 
{
	Translate_t2146470009::get_offset_of_gameObject_14(),
	Translate_t2146470009::get_offset_of_vector_15(),
	Translate_t2146470009::get_offset_of_x_16(),
	Translate_t2146470009::get_offset_of_y_17(),
	Translate_t2146470009::get_offset_of_z_18(),
	Translate_t2146470009::get_offset_of_space_19(),
	Translate_t2146470009::get_offset_of_perSecond_20(),
	Translate_t2146470009::get_offset_of_everyFrame_21(),
	Translate_t2146470009::get_offset_of_lateUpdate_22(),
	Translate_t2146470009::get_offset_of_fixedUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3710 = { sizeof (GetACosine_t2060389857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3710[4] = 
{
	GetACosine_t2060389857::get_offset_of_Value_14(),
	GetACosine_t2060389857::get_offset_of_angle_15(),
	GetACosine_t2060389857::get_offset_of_RadToDeg_16(),
	GetACosine_t2060389857::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3711 = { sizeof (GetASine_t718811337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3711[4] = 
{
	GetASine_t718811337::get_offset_of_Value_14(),
	GetASine_t718811337::get_offset_of_angle_15(),
	GetASine_t718811337::get_offset_of_RadToDeg_16(),
	GetASine_t718811337::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3712 = { sizeof (GetAtan_t2743258534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3712[4] = 
{
	GetAtan_t2743258534::get_offset_of_Value_14(),
	GetAtan_t2743258534::get_offset_of_angle_15(),
	GetAtan_t2743258534::get_offset_of_RadToDeg_16(),
	GetAtan_t2743258534::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3713 = { sizeof (GetAtan2_t2742078886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3713[5] = 
{
	GetAtan2_t2742078886::get_offset_of_xValue_14(),
	GetAtan2_t2742078886::get_offset_of_yValue_15(),
	GetAtan2_t2742078886::get_offset_of_angle_16(),
	GetAtan2_t2742078886::get_offset_of_RadToDeg_17(),
	GetAtan2_t2742078886::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3714 = { sizeof (GetAtan2FromVector2_t1932972477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3714[4] = 
{
	GetAtan2FromVector2_t1932972477::get_offset_of_vector2_14(),
	GetAtan2FromVector2_t1932972477::get_offset_of_angle_15(),
	GetAtan2FromVector2_t1932972477::get_offset_of_RadToDeg_16(),
	GetAtan2FromVector2_t1932972477::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3715 = { sizeof (GetAtan2FromVector3_t1932972476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3715[6] = 
{
	GetAtan2FromVector3_t1932972476::get_offset_of_vector3_14(),
	GetAtan2FromVector3_t1932972476::get_offset_of_xAxis_15(),
	GetAtan2FromVector3_t1932972476::get_offset_of_yAxis_16(),
	GetAtan2FromVector3_t1932972476::get_offset_of_angle_17(),
	GetAtan2FromVector3_t1932972476::get_offset_of_RadToDeg_18(),
	GetAtan2FromVector3_t1932972476::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3716 = { sizeof (aTan2EnumAxis_t207693546)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3716[4] = 
{
	aTan2EnumAxis_t207693546::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3717 = { sizeof (GetCosine_t2142229402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3717[4] = 
{
	GetCosine_t2142229402::get_offset_of_angle_14(),
	GetCosine_t2142229402::get_offset_of_DegToRad_15(),
	GetCosine_t2142229402::get_offset_of_result_16(),
	GetCosine_t2142229402::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3718 = { sizeof (GetSine_t1694078994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3718[4] = 
{
	GetSine_t1694078994::get_offset_of_angle_14(),
	GetSine_t1694078994::get_offset_of_DegToRad_15(),
	GetSine_t1694078994::get_offset_of_result_16(),
	GetSine_t1694078994::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3719 = { sizeof (GetTan_t4257013587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3719[4] = 
{
	GetTan_t4257013587::get_offset_of_angle_14(),
	GetTan_t4257013587::get_offset_of_DegToRad_15(),
	GetTan_t4257013587::get_offset_of_result_16(),
	GetTan_t4257013587::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3720 = { sizeof (UiCanvasEnableRaycast_t3530533341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3720[6] = 
{
	UiCanvasEnableRaycast_t3530533341::get_offset_of_gameObject_16(),
	UiCanvasEnableRaycast_t3530533341::get_offset_of_enableRaycasting_17(),
	UiCanvasEnableRaycast_t3530533341::get_offset_of_resetOnExit_18(),
	UiCanvasEnableRaycast_t3530533341::get_offset_of_everyFrame_19(),
	UiCanvasEnableRaycast_t3530533341::get_offset_of_raycastFilterProxy_20(),
	UiCanvasEnableRaycast_t3530533341::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3721 = { sizeof (UiCanvasForceUpdateCanvases_t663998497), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3722 = { sizeof (UiCanvasGroupSetAlpha_t3687891700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3722[6] = 
{
	UiCanvasGroupSetAlpha_t3687891700::get_offset_of_gameObject_16(),
	UiCanvasGroupSetAlpha_t3687891700::get_offset_of_alpha_17(),
	UiCanvasGroupSetAlpha_t3687891700::get_offset_of_resetOnExit_18(),
	UiCanvasGroupSetAlpha_t3687891700::get_offset_of_everyFrame_19(),
	UiCanvasGroupSetAlpha_t3687891700::get_offset_of_component_20(),
	UiCanvasGroupSetAlpha_t3687891700::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3723 = { sizeof (UiCanvasGroupSetProperties_t1011590200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3723[12] = 
{
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_gameObject_16(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_alpha_17(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_interactable_18(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_blocksRaycasts_19(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_ignoreParentGroup_20(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_resetOnExit_21(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_everyFrame_22(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_component_23(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_originalAlpha_24(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_originalInteractable_25(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_originalBlocksRaycasts_26(),
	UiCanvasGroupSetProperties_t1011590200::get_offset_of_originalIgnoreParentGroup_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3724 = { sizeof (UiCanvasScalerGetScaleFactor_t2797552755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3724[4] = 
{
	UiCanvasScalerGetScaleFactor_t2797552755::get_offset_of_gameObject_16(),
	UiCanvasScalerGetScaleFactor_t2797552755::get_offset_of_scaleFactor_17(),
	UiCanvasScalerGetScaleFactor_t2797552755::get_offset_of_everyFrame_18(),
	UiCanvasScalerGetScaleFactor_t2797552755::get_offset_of_component_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3725 = { sizeof (UiCanvasScalerSetScaleFactor_t2796641695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3725[4] = 
{
	UiCanvasScalerSetScaleFactor_t2796641695::get_offset_of_gameObject_16(),
	UiCanvasScalerSetScaleFactor_t2796641695::get_offset_of_scaleFactor_17(),
	UiCanvasScalerSetScaleFactor_t2796641695::get_offset_of_everyFrame_18(),
	UiCanvasScalerSetScaleFactor_t2796641695::get_offset_of_component_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3726 = { sizeof (PlayMakerCanvasRaycastFilterProxy_t3073240404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3726[1] = 
{
	PlayMakerCanvasRaycastFilterProxy_t3073240404::get_offset_of_RayCastingEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3727 = { sizeof (PlayMakerUiBoolValueChangedEvent_t793166467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3727[1] = 
{
	PlayMakerUiBoolValueChangedEvent_t793166467::get_offset_of_toggle_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3728 = { sizeof (PlayMakerUiClickEvent_t2847625480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3728[1] = 
{
	PlayMakerUiClickEvent_t2847625480::get_offset_of_button_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3729 = { sizeof (PlayMakerUiDragEvents_t799663687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3730 = { sizeof (PlayMakerUiDropEvent_t709928077), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3731 = { sizeof (PlayMakerUiEndEditEvent_t2584568241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3731[1] = 
{
	PlayMakerUiEndEditEvent_t2584568241::get_offset_of_inputField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3732 = { sizeof (PlayMakerUiEventBase_t1707648030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3732[2] = 
{
	PlayMakerUiEventBase_t1707648030::get_offset_of_targetFsms_4(),
	PlayMakerUiEventBase_t1707648030::get_offset_of_initialized_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3733 = { sizeof (PlayMakerUiFloatValueChangedEvent_t1964756808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3733[2] = 
{
	PlayMakerUiFloatValueChangedEvent_t1964756808::get_offset_of_slider_6(),
	PlayMakerUiFloatValueChangedEvent_t1964756808::get_offset_of_scrollbar_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3734 = { sizeof (PlayMakerUiIntValueChangedEvent_t3490394599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3734[1] = 
{
	PlayMakerUiIntValueChangedEvent_t3490394599::get_offset_of_dropdown_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3735 = { sizeof (PlayMakerUiPointerEvents_t3538587227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3736 = { sizeof (PlayMakerUiVector2ValueChangedEvent_t132838590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3736[1] = 
{
	PlayMakerUiVector2ValueChangedEvent_t132838590::get_offset_of_scrollRect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3737 = { sizeof (EventTriggerActionBase_t72041308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3737[4] = 
{
	EventTriggerActionBase_t72041308::get_offset_of_gameObject_16(),
	EventTriggerActionBase_t72041308::get_offset_of_eventTarget_17(),
	EventTriggerActionBase_t72041308::get_offset_of_trigger_18(),
	EventTriggerActionBase_t72041308::get_offset_of_entry_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3738 = { sizeof (UiEventSystemCurrentRayCastAll_t2732382018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3738[7] = 
{
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_screenPosition_14(),
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_orScreenPosition2d_15(),
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_gameObjectList_16(),
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_hitCount_17(),
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_everyFrame_18(),
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_pointer_19(),
	UiEventSystemCurrentRayCastAll_t2732382018::get_offset_of_raycastResults_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3739 = { sizeof (UiEventSystemExecuteEvent_t4239363109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3739[5] = 
{
	UiEventSystemExecuteEvent_t4239363109::get_offset_of_gameObject_14(),
	UiEventSystemExecuteEvent_t4239363109::get_offset_of_eventHandler_15(),
	UiEventSystemExecuteEvent_t4239363109::get_offset_of_success_16(),
	UiEventSystemExecuteEvent_t4239363109::get_offset_of_canNotHandleEvent_17(),
	UiEventSystemExecuteEvent_t4239363109::get_offset_of_go_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3740 = { sizeof (EventHandlers_t409295930)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3740[17] = 
{
	EventHandlers_t409295930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3741 = { sizeof (UiGetLastPointerDataInfo_t95606296), -1, sizeof(UiGetLastPointerDataInfo_t95606296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3741[24] = 
{
	UiGetLastPointerDataInfo_t95606296_StaticFields::get_offset_of_lastPointerEventData_14(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_clickCount_15(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_clickTime_16(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_delta_17(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_dragging_18(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_inputButton_19(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_eligibleForClick_20(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_enterEventCamera_21(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_pressEventCamera_22(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_isPointerMoving_23(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_isScrolling_24(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_lastPress_25(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_pointerDrag_26(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_pointerEnter_27(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_pointerId_28(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_pointerPress_29(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_position_30(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_pressPosition_31(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_rawPointerPress_32(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_scrollDelta_33(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_used_34(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_useDragThreshold_35(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_worldNormal_36(),
	UiGetLastPointerDataInfo_t95606296::get_offset_of_worldPosition_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3742 = { sizeof (UiGetLastPointerEventDataInputButton_t3342924654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3742[4] = 
{
	UiGetLastPointerEventDataInputButton_t3342924654::get_offset_of_inputButton_14(),
	UiGetLastPointerEventDataInputButton_t3342924654::get_offset_of_leftClick_15(),
	UiGetLastPointerEventDataInputButton_t3342924654::get_offset_of_middleClick_16(),
	UiGetLastPointerEventDataInputButton_t3342924654::get_offset_of_rightClick_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3743 = { sizeof (UiGetSelectedGameObject_t2739701632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3743[4] = 
{
	UiGetSelectedGameObject_t2739701632::get_offset_of_StoreGameObject_14(),
	UiGetSelectedGameObject_t2739701632::get_offset_of_ObjectChangedEvent_15(),
	UiGetSelectedGameObject_t2739701632::get_offset_of_everyFrame_16(),
	UiGetSelectedGameObject_t2739701632::get_offset_of_lastGameObject_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3744 = { sizeof (UiIsPointerOverUiObject_t3566716892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3744[5] = 
{
	UiIsPointerOverUiObject_t3566716892::get_offset_of_pointerId_14(),
	UiIsPointerOverUiObject_t3566716892::get_offset_of_pointerOverUI_15(),
	UiIsPointerOverUiObject_t3566716892::get_offset_of_pointerNotOverUI_16(),
	UiIsPointerOverUiObject_t3566716892::get_offset_of_isPointerOverUI_17(),
	UiIsPointerOverUiObject_t3566716892::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3745 = { sizeof (UiOnBeginDragEvent_t3639786799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3745[1] = 
{
	UiOnBeginDragEvent_t3639786799::get_offset_of_onBeginDragEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3746 = { sizeof (UiOnCancelEvent_t3485713958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3746[1] = 
{
	UiOnCancelEvent_t3485713958::get_offset_of_onCancelEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3747 = { sizeof (UiOnDeselectEvent_t3391393454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3747[1] = 
{
	UiOnDeselectEvent_t3391393454::get_offset_of_onDeselectEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3748 = { sizeof (UiOnDragEvent_t2141365689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3748[1] = 
{
	UiOnDragEvent_t2141365689::get_offset_of_onDragEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3749 = { sizeof (UiOnDropEvent_t2090312951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3749[1] = 
{
	UiOnDropEvent_t2090312951::get_offset_of_onDropEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3750 = { sizeof (UiOnEndDragEvent_t1898429554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3750[1] = 
{
	UiOnEndDragEvent_t1898429554::get_offset_of_onEndDragEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3751 = { sizeof (UiOnInitializePotentialDragEvent_t1975688602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3751[1] = 
{
	UiOnInitializePotentialDragEvent_t1975688602::get_offset_of_onInitializePotentialDragEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3752 = { sizeof (UiOnMoveEvent_t3747541532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3752[1] = 
{
	UiOnMoveEvent_t3747541532::get_offset_of_onMoveEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3753 = { sizeof (UiOnPointerClickEvent_t696272602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3753[1] = 
{
	UiOnPointerClickEvent_t696272602::get_offset_of_onPointerClickEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3754 = { sizeof (UiOnPointerDownEvent_t1426364170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3754[1] = 
{
	UiOnPointerDownEvent_t1426364170::get_offset_of_onPointerDownEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3755 = { sizeof (UiOnPointerEnterEvent_t3050051842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3755[1] = 
{
	UiOnPointerEnterEvent_t3050051842::get_offset_of_onPointerEnterEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3756 = { sizeof (UiOnPointerExitEvent_t2854665363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3756[1] = 
{
	UiOnPointerExitEvent_t2854665363::get_offset_of_onPointerExitEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3757 = { sizeof (UiOnPointerUpEvent_t2144733083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3757[1] = 
{
	UiOnPointerUpEvent_t2144733083::get_offset_of_onPointerUpEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3758 = { sizeof (UiOnScrollEvent_t3739468943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3758[1] = 
{
	UiOnScrollEvent_t3739468943::get_offset_of_onScrollEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3759 = { sizeof (UiOnSelectEvent_t3633769758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3759[1] = 
{
	UiOnSelectEvent_t3633769758::get_offset_of_onSelectEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3760 = { sizeof (UiOnSubmitEvent_t1986591334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3760[1] = 
{
	UiOnSubmitEvent_t1986591334::get_offset_of_onSubmitEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3761 = { sizeof (UiOnUpdateSelectedEvent_t543813073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3761[1] = 
{
	UiOnUpdateSelectedEvent_t543813073::get_offset_of_onUpdateSelectedEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3762 = { sizeof (UiSetSelectedGameObject_t2602666268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3762[1] = 
{
	UiSetSelectedGameObject_t2602666268::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3763 = { sizeof (UiLayoutElementGetValues_t128213014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3763[16] = 
{
	UiLayoutElementGetValues_t128213014::get_offset_of_gameObject_16(),
	UiLayoutElementGetValues_t128213014::get_offset_of_ignoreLayout_17(),
	UiLayoutElementGetValues_t128213014::get_offset_of_minWidthEnabled_18(),
	UiLayoutElementGetValues_t128213014::get_offset_of_minWidth_19(),
	UiLayoutElementGetValues_t128213014::get_offset_of_minHeightEnabled_20(),
	UiLayoutElementGetValues_t128213014::get_offset_of_minHeight_21(),
	UiLayoutElementGetValues_t128213014::get_offset_of_preferredWidthEnabled_22(),
	UiLayoutElementGetValues_t128213014::get_offset_of_preferredWidth_23(),
	UiLayoutElementGetValues_t128213014::get_offset_of_preferredHeightEnabled_24(),
	UiLayoutElementGetValues_t128213014::get_offset_of_preferredHeight_25(),
	UiLayoutElementGetValues_t128213014::get_offset_of_flexibleWidthEnabled_26(),
	UiLayoutElementGetValues_t128213014::get_offset_of_flexibleWidth_27(),
	UiLayoutElementGetValues_t128213014::get_offset_of_flexibleHeightEnabled_28(),
	UiLayoutElementGetValues_t128213014::get_offset_of_flexibleHeight_29(),
	UiLayoutElementGetValues_t128213014::get_offset_of_everyFrame_30(),
	UiLayoutElementGetValues_t128213014::get_offset_of_layoutElement_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3764 = { sizeof (UiLayoutElementSetValues_t982540310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3764[9] = 
{
	UiLayoutElementSetValues_t982540310::get_offset_of_gameObject_16(),
	UiLayoutElementSetValues_t982540310::get_offset_of_minWidth_17(),
	UiLayoutElementSetValues_t982540310::get_offset_of_minHeight_18(),
	UiLayoutElementSetValues_t982540310::get_offset_of_preferredWidth_19(),
	UiLayoutElementSetValues_t982540310::get_offset_of_preferredHeight_20(),
	UiLayoutElementSetValues_t982540310::get_offset_of_flexibleWidth_21(),
	UiLayoutElementSetValues_t982540310::get_offset_of_flexibleHeight_22(),
	UiLayoutElementSetValues_t982540310::get_offset_of_everyFrame_23(),
	UiLayoutElementSetValues_t982540310::get_offset_of_layoutElement_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3765 = { sizeof (UiNavigationExplicitGetProperties_t4213199546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3765[6] = 
{
	UiNavigationExplicitGetProperties_t4213199546::get_offset_of_gameObject_16(),
	UiNavigationExplicitGetProperties_t4213199546::get_offset_of_selectOnDown_17(),
	UiNavigationExplicitGetProperties_t4213199546::get_offset_of_selectOnUp_18(),
	UiNavigationExplicitGetProperties_t4213199546::get_offset_of_selectOnLeft_19(),
	UiNavigationExplicitGetProperties_t4213199546::get_offset_of_selectOnRight_20(),
	UiNavigationExplicitGetProperties_t4213199546::get_offset_of__selectable_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3766 = { sizeof (UiNavigationExplicitSetProperties_t3567312374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3766[9] = 
{
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_gameObject_16(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_selectOnDown_17(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_selectOnUp_18(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_selectOnLeft_19(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_selectOnRight_20(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_resetOnExit_21(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_selectable_22(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_navigation_23(),
	UiNavigationExplicitSetProperties_t3567312374::get_offset_of_originalState_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3767 = { sizeof (UiGetColorBlock_t2505395705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3767[9] = 
{
	UiGetColorBlock_t2505395705::get_offset_of_gameObject_16(),
	UiGetColorBlock_t2505395705::get_offset_of_fadeDuration_17(),
	UiGetColorBlock_t2505395705::get_offset_of_colorMultiplier_18(),
	UiGetColorBlock_t2505395705::get_offset_of_normalColor_19(),
	UiGetColorBlock_t2505395705::get_offset_of_pressedColor_20(),
	UiGetColorBlock_t2505395705::get_offset_of_highlightedColor_21(),
	UiGetColorBlock_t2505395705::get_offset_of_disabledColor_22(),
	UiGetColorBlock_t2505395705::get_offset_of_everyFrame_23(),
	UiGetColorBlock_t2505395705::get_offset_of_selectable_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3768 = { sizeof (UiGetIsInteractable_t3788033727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3768[6] = 
{
	UiGetIsInteractable_t3788033727::get_offset_of_gameObject_16(),
	UiGetIsInteractable_t3788033727::get_offset_of_isInteractable_17(),
	UiGetIsInteractable_t3788033727::get_offset_of_isInteractableEvent_18(),
	UiGetIsInteractable_t3788033727::get_offset_of_isNotInteractableEvent_19(),
	UiGetIsInteractable_t3788033727::get_offset_of_selectable_20(),
	UiGetIsInteractable_t3788033727::get_offset_of_originalState_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3769 = { sizeof (UiNavigationGetMode_t2431581720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3769[9] = 
{
	UiNavigationGetMode_t2431581720::get_offset_of_gameObject_16(),
	UiNavigationGetMode_t2431581720::get_offset_of_navigationMode_17(),
	UiNavigationGetMode_t2431581720::get_offset_of_automaticEvent_18(),
	UiNavigationGetMode_t2431581720::get_offset_of_horizontalEvent_19(),
	UiNavigationGetMode_t2431581720::get_offset_of_verticalEvent_20(),
	UiNavigationGetMode_t2431581720::get_offset_of_explicitEvent_21(),
	UiNavigationGetMode_t2431581720::get_offset_of_noNavigationEvent_22(),
	UiNavigationGetMode_t2431581720::get_offset_of_selectable_23(),
	UiNavigationGetMode_t2431581720::get_offset_of_originalTransition_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3770 = { sizeof (UiNavigationSetMode_t1356500756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3770[6] = 
{
	UiNavigationSetMode_t1356500756::get_offset_of_gameObject_16(),
	UiNavigationSetMode_t1356500756::get_offset_of_navigationMode_17(),
	UiNavigationSetMode_t1356500756::get_offset_of_resetOnExit_18(),
	UiNavigationSetMode_t1356500756::get_offset_of_selectable_19(),
	UiNavigationSetMode_t1356500756::get_offset_of__navigation_20(),
	UiNavigationSetMode_t1356500756::get_offset_of_originalValue_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3771 = { sizeof (UiSetAnimationTriggers_t493328399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3771[9] = 
{
	UiSetAnimationTriggers_t493328399::get_offset_of_gameObject_16(),
	UiSetAnimationTriggers_t493328399::get_offset_of_normalTrigger_17(),
	UiSetAnimationTriggers_t493328399::get_offset_of_highlightedTrigger_18(),
	UiSetAnimationTriggers_t493328399::get_offset_of_pressedTrigger_19(),
	UiSetAnimationTriggers_t493328399::get_offset_of_disabledTrigger_20(),
	UiSetAnimationTriggers_t493328399::get_offset_of_resetOnExit_21(),
	UiSetAnimationTriggers_t493328399::get_offset_of_selectable_22(),
	UiSetAnimationTriggers_t493328399::get_offset_of__animationTriggers_23(),
	UiSetAnimationTriggers_t493328399::get_offset_of_originalAnimationTriggers_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3772 = { sizeof (UiSetColorBlock_t2506035845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3772[12] = 
{
	UiSetColorBlock_t2506035845::get_offset_of_gameObject_16(),
	UiSetColorBlock_t2506035845::get_offset_of_fadeDuration_17(),
	UiSetColorBlock_t2506035845::get_offset_of_colorMultiplier_18(),
	UiSetColorBlock_t2506035845::get_offset_of_normalColor_19(),
	UiSetColorBlock_t2506035845::get_offset_of_pressedColor_20(),
	UiSetColorBlock_t2506035845::get_offset_of_highlightedColor_21(),
	UiSetColorBlock_t2506035845::get_offset_of_disabledColor_22(),
	UiSetColorBlock_t2506035845::get_offset_of_resetOnExit_23(),
	UiSetColorBlock_t2506035845::get_offset_of_everyFrame_24(),
	UiSetColorBlock_t2506035845::get_offset_of_selectable_25(),
	UiSetColorBlock_t2506035845::get_offset_of__colorBlock_26(),
	UiSetColorBlock_t2506035845::get_offset_of_originalColorBlock_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3773 = { sizeof (UiSetIsInteractable_t3806359819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3773[5] = 
{
	UiSetIsInteractable_t3806359819::get_offset_of_gameObject_14(),
	UiSetIsInteractable_t3806359819::get_offset_of_isInteractable_15(),
	UiSetIsInteractable_t3806359819::get_offset_of_resetOnExit_16(),
	UiSetIsInteractable_t3806359819::get_offset_of__selectable_17(),
	UiSetIsInteractable_t3806359819::get_offset_of__originalState_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3774 = { sizeof (UiTransitionGetType_t845750375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3774[8] = 
{
	UiTransitionGetType_t845750375::get_offset_of_gameObject_16(),
	UiTransitionGetType_t845750375::get_offset_of_transition_17(),
	UiTransitionGetType_t845750375::get_offset_of_colorTintEvent_18(),
	UiTransitionGetType_t845750375::get_offset_of_spriteSwapEvent_19(),
	UiTransitionGetType_t845750375::get_offset_of_animationEvent_20(),
	UiTransitionGetType_t845750375::get_offset_of_noTransitionEvent_21(),
	UiTransitionGetType_t845750375::get_offset_of_selectable_22(),
	UiTransitionGetType_t845750375::get_offset_of_originalTransition_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3775 = { sizeof (UiTransitionSetType_t798954539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3775[5] = 
{
	UiTransitionSetType_t798954539::get_offset_of_gameObject_16(),
	UiTransitionSetType_t798954539::get_offset_of_transition_17(),
	UiTransitionSetType_t798954539::get_offset_of_resetOnExit_18(),
	UiTransitionSetType_t798954539::get_offset_of_selectable_19(),
	UiTransitionSetType_t798954539::get_offset_of_originalTransition_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3776 = { sizeof (UiButtonArray_t3593418030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3776[7] = 
{
	UiButtonArray_t3593418030::get_offset_of_eventTarget_14(),
	UiButtonArray_t3593418030::get_offset_of_gameObjects_15(),
	UiButtonArray_t3593418030::get_offset_of_clickEvents_16(),
	UiButtonArray_t3593418030::get_offset_of_buttons_17(),
	UiButtonArray_t3593418030::get_offset_of_cachedGameObjects_18(),
	UiButtonArray_t3593418030::get_offset_of_actions_19(),
	UiButtonArray_t3593418030::get_offset_of_clickedButton_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3777 = { sizeof (U3COnEnterU3Ec__AnonStorey0_t2283252108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3777[2] = 
{
	U3COnEnterU3Ec__AnonStorey0_t2283252108::get_offset_of_index_0(),
	U3COnEnterU3Ec__AnonStorey0_t2283252108::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3778 = { sizeof (UiButtonOnClickEvent_t3361402451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3778[4] = 
{
	UiButtonOnClickEvent_t3361402451::get_offset_of_gameObject_16(),
	UiButtonOnClickEvent_t3361402451::get_offset_of_eventTarget_17(),
	UiButtonOnClickEvent_t3361402451::get_offset_of_sendEvent_18(),
	UiButtonOnClickEvent_t3361402451::get_offset_of_button_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3779 = { sizeof (UiDropDownAddOptions_t3285147849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3779[5] = 
{
	UiDropDownAddOptions_t3285147849::get_offset_of_gameObject_16(),
	UiDropDownAddOptions_t3285147849::get_offset_of_optionText_17(),
	UiDropDownAddOptions_t3285147849::get_offset_of_optionImage_18(),
	UiDropDownAddOptions_t3285147849::get_offset_of_dropDown_19(),
	UiDropDownAddOptions_t3285147849::get_offset_of_options_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3780 = { sizeof (UiDropDownClearOptions_t3929997459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3780[2] = 
{
	UiDropDownClearOptions_t3929997459::get_offset_of_gameObject_16(),
	UiDropDownClearOptions_t3929997459::get_offset_of_dropDown_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3781 = { sizeof (UiDropDownGetSelectedData_t4113433095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3781[6] = 
{
	UiDropDownGetSelectedData_t4113433095::get_offset_of_gameObject_16(),
	UiDropDownGetSelectedData_t4113433095::get_offset_of_index_17(),
	UiDropDownGetSelectedData_t4113433095::get_offset_of_getText_18(),
	UiDropDownGetSelectedData_t4113433095::get_offset_of_getImage_19(),
	UiDropDownGetSelectedData_t4113433095::get_offset_of_everyFrame_20(),
	UiDropDownGetSelectedData_t4113433095::get_offset_of_dropDown_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3782 = { sizeof (UiDropDownSetValue_t1878988810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3782[4] = 
{
	UiDropDownSetValue_t1878988810::get_offset_of_gameObject_16(),
	UiDropDownSetValue_t1878988810::get_offset_of_value_17(),
	UiDropDownSetValue_t1878988810::get_offset_of_everyFrame_18(),
	UiDropDownSetValue_t1878988810::get_offset_of_dropDown_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3783 = { sizeof (UiGraphicCrossFadeAlpha_t1243273689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3783[5] = 
{
	UiGraphicCrossFadeAlpha_t1243273689::get_offset_of_gameObject_16(),
	UiGraphicCrossFadeAlpha_t1243273689::get_offset_of_alpha_17(),
	UiGraphicCrossFadeAlpha_t1243273689::get_offset_of_duration_18(),
	UiGraphicCrossFadeAlpha_t1243273689::get_offset_of_ignoreTimeScale_19(),
	UiGraphicCrossFadeAlpha_t1243273689::get_offset_of_uiComponent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3784 = { sizeof (UiGraphicCrossFadeColor_t1236708496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3784[10] = 
{
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_gameObject_16(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_color_17(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_red_18(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_green_19(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_blue_20(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_alpha_21(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_duration_22(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_ignoreTimeScale_23(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_useAlpha_24(),
	UiGraphicCrossFadeColor_t1236708496::get_offset_of_uiComponent_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3785 = { sizeof (UiGraphicGetColor_t798376921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3785[4] = 
{
	UiGraphicGetColor_t798376921::get_offset_of_gameObject_16(),
	UiGraphicGetColor_t798376921::get_offset_of_color_17(),
	UiGraphicGetColor_t798376921::get_offset_of_everyFrame_18(),
	UiGraphicGetColor_t798376921::get_offset_of_uiComponent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3786 = { sizeof (UiGraphicSetColor_t3371320281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3786[10] = 
{
	UiGraphicSetColor_t3371320281::get_offset_of_gameObject_16(),
	UiGraphicSetColor_t3371320281::get_offset_of_color_17(),
	UiGraphicSetColor_t3371320281::get_offset_of_red_18(),
	UiGraphicSetColor_t3371320281::get_offset_of_green_19(),
	UiGraphicSetColor_t3371320281::get_offset_of_blue_20(),
	UiGraphicSetColor_t3371320281::get_offset_of_alpha_21(),
	UiGraphicSetColor_t3371320281::get_offset_of_resetOnExit_22(),
	UiGraphicSetColor_t3371320281::get_offset_of_everyFrame_23(),
	UiGraphicSetColor_t3371320281::get_offset_of_uiComponent_24(),
	UiGraphicSetColor_t3371320281::get_offset_of_originalColor_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3787 = { sizeof (UiImageGetFillAmount_t3656243199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3787[4] = 
{
	UiImageGetFillAmount_t3656243199::get_offset_of_gameObject_16(),
	UiImageGetFillAmount_t3656243199::get_offset_of_ImageFillAmount_17(),
	UiImageGetFillAmount_t3656243199::get_offset_of_everyFrame_18(),
	UiImageGetFillAmount_t3656243199::get_offset_of_image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3788 = { sizeof (UiImageGetSprite_t1070759130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3788[3] = 
{
	UiImageGetSprite_t1070759130::get_offset_of_gameObject_16(),
	UiImageGetSprite_t1070759130::get_offset_of_sprite_17(),
	UiImageGetSprite_t1070759130::get_offset_of_image_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3789 = { sizeof (UiImageSetFillAmount_t140630020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3789[4] = 
{
	UiImageSetFillAmount_t140630020::get_offset_of_gameObject_16(),
	UiImageSetFillAmount_t140630020::get_offset_of_ImageFillAmount_17(),
	UiImageSetFillAmount_t140630020::get_offset_of_everyFrame_18(),
	UiImageSetFillAmount_t140630020::get_offset_of_image_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3790 = { sizeof (UiImageSetSprite_t278822106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3790[5] = 
{
	UiImageSetSprite_t278822106::get_offset_of_gameObject_16(),
	UiImageSetSprite_t278822106::get_offset_of_sprite_17(),
	UiImageSetSprite_t278822106::get_offset_of_resetOnExit_18(),
	UiImageSetSprite_t278822106::get_offset_of_image_19(),
	UiImageSetSprite_t278822106::get_offset_of_originalSprite_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3791 = { sizeof (UiInputFieldActivate_t1212826059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3791[3] = 
{
	UiInputFieldActivate_t1212826059::get_offset_of_gameObject_16(),
	UiInputFieldActivate_t1212826059::get_offset_of_deactivateOnExit_17(),
	UiInputFieldActivate_t1212826059::get_offset_of_inputField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3792 = { sizeof (UiInputFieldDeactivate_t990877000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3792[3] = 
{
	UiInputFieldDeactivate_t990877000::get_offset_of_gameObject_16(),
	UiInputFieldDeactivate_t990877000::get_offset_of_activateOnExit_17(),
	UiInputFieldDeactivate_t990877000::get_offset_of_inputField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3793 = { sizeof (UiInputFieldGetCaretBlinkRate_t2635913202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3793[4] = 
{
	UiInputFieldGetCaretBlinkRate_t2635913202::get_offset_of_gameObject_16(),
	UiInputFieldGetCaretBlinkRate_t2635913202::get_offset_of_caretBlinkRate_17(),
	UiInputFieldGetCaretBlinkRate_t2635913202::get_offset_of_everyFrame_18(),
	UiInputFieldGetCaretBlinkRate_t2635913202::get_offset_of_inputField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3794 = { sizeof (UiInputFieldGetCharacterLimit_t617759529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3794[6] = 
{
	UiInputFieldGetCharacterLimit_t617759529::get_offset_of_gameObject_16(),
	UiInputFieldGetCharacterLimit_t617759529::get_offset_of_characterLimit_17(),
	UiInputFieldGetCharacterLimit_t617759529::get_offset_of_hasNoLimitEvent_18(),
	UiInputFieldGetCharacterLimit_t617759529::get_offset_of_isLimitedEvent_19(),
	UiInputFieldGetCharacterLimit_t617759529::get_offset_of_everyFrame_20(),
	UiInputFieldGetCharacterLimit_t617759529::get_offset_of_inputField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3795 = { sizeof (UiInputFieldGetHideMobileInput_t1704494218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3795[5] = 
{
	UiInputFieldGetHideMobileInput_t1704494218::get_offset_of_gameObject_16(),
	UiInputFieldGetHideMobileInput_t1704494218::get_offset_of_hideMobileInput_17(),
	UiInputFieldGetHideMobileInput_t1704494218::get_offset_of_mobileInputHiddenEvent_18(),
	UiInputFieldGetHideMobileInput_t1704494218::get_offset_of_mobileInputShownEvent_19(),
	UiInputFieldGetHideMobileInput_t1704494218::get_offset_of_inputField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3796 = { sizeof (UiInputFieldGetIsFocused_t1382970275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3796[5] = 
{
	UiInputFieldGetIsFocused_t1382970275::get_offset_of_gameObject_16(),
	UiInputFieldGetIsFocused_t1382970275::get_offset_of_isFocused_17(),
	UiInputFieldGetIsFocused_t1382970275::get_offset_of_isfocusedEvent_18(),
	UiInputFieldGetIsFocused_t1382970275::get_offset_of_isNotFocusedEvent_19(),
	UiInputFieldGetIsFocused_t1382970275::get_offset_of_inputField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3797 = { sizeof (UiInputFieldGetPlaceHolder_t898934888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3797[6] = 
{
	UiInputFieldGetPlaceHolder_t898934888::get_offset_of_gameObject_16(),
	UiInputFieldGetPlaceHolder_t898934888::get_offset_of_placeHolder_17(),
	UiInputFieldGetPlaceHolder_t898934888::get_offset_of_placeHolderDefined_18(),
	UiInputFieldGetPlaceHolder_t898934888::get_offset_of_foundEvent_19(),
	UiInputFieldGetPlaceHolder_t898934888::get_offset_of_notFoundEvent_20(),
	UiInputFieldGetPlaceHolder_t898934888::get_offset_of_inputField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3798 = { sizeof (UiInputFieldGetSelectionColor_t1814258712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3798[4] = 
{
	UiInputFieldGetSelectionColor_t1814258712::get_offset_of_gameObject_16(),
	UiInputFieldGetSelectionColor_t1814258712::get_offset_of_selectionColor_17(),
	UiInputFieldGetSelectionColor_t1814258712::get_offset_of_everyFrame_18(),
	UiInputFieldGetSelectionColor_t1814258712::get_offset_of_inputField_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3799 = { sizeof (UiInputFieldGetText_t3479464756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3799[4] = 
{
	UiInputFieldGetText_t3479464756::get_offset_of_gameObject_16(),
	UiInputFieldGetText_t3479464756::get_offset_of_text_17(),
	UiInputFieldGetText_t3479464756::get_offset_of_everyFrame_18(),
	UiInputFieldGetText_t3479464756::get_offset_of_inputField_19(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
