﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.EasingFunction/Function
struct Function_t3898704989;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t719957643;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t3649179877;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t1738787045;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// HutongGames.PlayMaker.LayoutOption[]
struct LayoutOptionU5BU5D_t80899288;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GUIContent
struct GUIContent_t3050628031;
// UnityEngine.GUIElement
struct GUIElement_t3567083079;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.GUITexture
struct GUITexture_t951903601;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Renderer
struct Renderer_t2627027031;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EASINGFUNCTION_T3263762189_H
#define EASINGFUNCTION_T3263762189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.EasingFunction
struct  EasingFunction_t3263762189  : public RuntimeObject
{
public:

public:
};

struct EasingFunction_t3263762189_StaticFields
{
public:
	// UnityEngine.AnimationCurve HutongGames.EasingFunction::AnimationCurve
	AnimationCurve_t3046754366 * ___AnimationCurve_1;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache0
	Function_t3898704989 * ___U3CU3Ef__mgU24cache0_2;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1_3;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2_4;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3_5;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache4
	Function_t3898704989 * ___U3CU3Ef__mgU24cache4_6;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache5
	Function_t3898704989 * ___U3CU3Ef__mgU24cache5_7;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache6
	Function_t3898704989 * ___U3CU3Ef__mgU24cache6_8;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache7
	Function_t3898704989 * ___U3CU3Ef__mgU24cache7_9;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache8
	Function_t3898704989 * ___U3CU3Ef__mgU24cache8_10;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache9
	Function_t3898704989 * ___U3CU3Ef__mgU24cache9_11;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cacheA
	Function_t3898704989 * ___U3CU3Ef__mgU24cacheA_12;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cacheB
	Function_t3898704989 * ___U3CU3Ef__mgU24cacheB_13;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cacheC
	Function_t3898704989 * ___U3CU3Ef__mgU24cacheC_14;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cacheD
	Function_t3898704989 * ___U3CU3Ef__mgU24cacheD_15;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cacheE
	Function_t3898704989 * ___U3CU3Ef__mgU24cacheE_16;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cacheF
	Function_t3898704989 * ___U3CU3Ef__mgU24cacheF_17;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache10
	Function_t3898704989 * ___U3CU3Ef__mgU24cache10_18;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache11
	Function_t3898704989 * ___U3CU3Ef__mgU24cache11_19;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache12
	Function_t3898704989 * ___U3CU3Ef__mgU24cache12_20;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache13
	Function_t3898704989 * ___U3CU3Ef__mgU24cache13_21;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache14
	Function_t3898704989 * ___U3CU3Ef__mgU24cache14_22;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache15
	Function_t3898704989 * ___U3CU3Ef__mgU24cache15_23;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache16
	Function_t3898704989 * ___U3CU3Ef__mgU24cache16_24;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache17
	Function_t3898704989 * ___U3CU3Ef__mgU24cache17_25;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache18
	Function_t3898704989 * ___U3CU3Ef__mgU24cache18_26;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache19
	Function_t3898704989 * ___U3CU3Ef__mgU24cache19_27;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1A
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1A_28;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1B
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1B_29;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1C
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1C_30;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1D
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1D_31;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1E
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1E_32;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache1F
	Function_t3898704989 * ___U3CU3Ef__mgU24cache1F_33;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache20
	Function_t3898704989 * ___U3CU3Ef__mgU24cache20_34;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache21
	Function_t3898704989 * ___U3CU3Ef__mgU24cache21_35;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache22
	Function_t3898704989 * ___U3CU3Ef__mgU24cache22_36;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache23
	Function_t3898704989 * ___U3CU3Ef__mgU24cache23_37;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache24
	Function_t3898704989 * ___U3CU3Ef__mgU24cache24_38;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache25
	Function_t3898704989 * ___U3CU3Ef__mgU24cache25_39;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache26
	Function_t3898704989 * ___U3CU3Ef__mgU24cache26_40;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache27
	Function_t3898704989 * ___U3CU3Ef__mgU24cache27_41;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache28
	Function_t3898704989 * ___U3CU3Ef__mgU24cache28_42;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache29
	Function_t3898704989 * ___U3CU3Ef__mgU24cache29_43;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2A
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2A_44;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2B
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2B_45;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2C
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2C_46;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2D
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2D_47;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2E
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2E_48;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache2F
	Function_t3898704989 * ___U3CU3Ef__mgU24cache2F_49;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache30
	Function_t3898704989 * ___U3CU3Ef__mgU24cache30_50;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache31
	Function_t3898704989 * ___U3CU3Ef__mgU24cache31_51;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache32
	Function_t3898704989 * ___U3CU3Ef__mgU24cache32_52;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache33
	Function_t3898704989 * ___U3CU3Ef__mgU24cache33_53;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache34
	Function_t3898704989 * ___U3CU3Ef__mgU24cache34_54;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache35
	Function_t3898704989 * ___U3CU3Ef__mgU24cache35_55;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache36
	Function_t3898704989 * ___U3CU3Ef__mgU24cache36_56;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache37
	Function_t3898704989 * ___U3CU3Ef__mgU24cache37_57;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache38
	Function_t3898704989 * ___U3CU3Ef__mgU24cache38_58;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache39
	Function_t3898704989 * ___U3CU3Ef__mgU24cache39_59;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3A
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3A_60;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3B
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3B_61;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3C
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3C_62;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3D
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3D_63;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3E
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3E_64;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache3F
	Function_t3898704989 * ___U3CU3Ef__mgU24cache3F_65;
	// HutongGames.EasingFunction/Function HutongGames.EasingFunction::<>f__mg$cache40
	Function_t3898704989 * ___U3CU3Ef__mgU24cache40_66;

public:
	inline static int32_t get_offset_of_AnimationCurve_1() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___AnimationCurve_1)); }
	inline AnimationCurve_t3046754366 * get_AnimationCurve_1() const { return ___AnimationCurve_1; }
	inline AnimationCurve_t3046754366 ** get_address_of_AnimationCurve_1() { return &___AnimationCurve_1; }
	inline void set_AnimationCurve_1(AnimationCurve_t3046754366 * value)
	{
		___AnimationCurve_1 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationCurve_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_3() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1_3)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1_3() const { return ___U3CU3Ef__mgU24cache1_3; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1_3() { return &___U3CU3Ef__mgU24cache1_3; }
	inline void set_U3CU3Ef__mgU24cache1_3(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_4() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2_4)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2_4() const { return ___U3CU3Ef__mgU24cache2_4; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2_4() { return &___U3CU3Ef__mgU24cache2_4; }
	inline void set_U3CU3Ef__mgU24cache2_4(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_5() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3_5)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3_5() const { return ___U3CU3Ef__mgU24cache3_5; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3_5() { return &___U3CU3Ef__mgU24cache3_5; }
	inline void set_U3CU3Ef__mgU24cache3_5(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_6() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache4_6)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache4_6() const { return ___U3CU3Ef__mgU24cache4_6; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache4_6() { return &___U3CU3Ef__mgU24cache4_6; }
	inline void set_U3CU3Ef__mgU24cache4_6(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_7() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache5_7)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache5_7() const { return ___U3CU3Ef__mgU24cache5_7; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache5_7() { return &___U3CU3Ef__mgU24cache5_7; }
	inline void set_U3CU3Ef__mgU24cache5_7(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_8() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache6_8)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache6_8() const { return ___U3CU3Ef__mgU24cache6_8; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache6_8() { return &___U3CU3Ef__mgU24cache6_8; }
	inline void set_U3CU3Ef__mgU24cache6_8(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_9() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache7_9)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache7_9() const { return ___U3CU3Ef__mgU24cache7_9; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache7_9() { return &___U3CU3Ef__mgU24cache7_9; }
	inline void set_U3CU3Ef__mgU24cache7_9(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_10() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache8_10)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache8_10() const { return ___U3CU3Ef__mgU24cache8_10; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache8_10() { return &___U3CU3Ef__mgU24cache8_10; }
	inline void set_U3CU3Ef__mgU24cache8_10(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_11() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache9_11)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache9_11() const { return ___U3CU3Ef__mgU24cache9_11; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache9_11() { return &___U3CU3Ef__mgU24cache9_11; }
	inline void set_U3CU3Ef__mgU24cache9_11(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_12() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cacheA_12)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cacheA_12() const { return ___U3CU3Ef__mgU24cacheA_12; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cacheA_12() { return &___U3CU3Ef__mgU24cacheA_12; }
	inline void set_U3CU3Ef__mgU24cacheA_12(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cacheA_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_13() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cacheB_13)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cacheB_13() const { return ___U3CU3Ef__mgU24cacheB_13; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cacheB_13() { return &___U3CU3Ef__mgU24cacheB_13; }
	inline void set_U3CU3Ef__mgU24cacheB_13(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cacheB_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_14() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cacheC_14)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cacheC_14() const { return ___U3CU3Ef__mgU24cacheC_14; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cacheC_14() { return &___U3CU3Ef__mgU24cacheC_14; }
	inline void set_U3CU3Ef__mgU24cacheC_14(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cacheC_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_15() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cacheD_15)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cacheD_15() const { return ___U3CU3Ef__mgU24cacheD_15; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cacheD_15() { return &___U3CU3Ef__mgU24cacheD_15; }
	inline void set_U3CU3Ef__mgU24cacheD_15(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cacheD_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_16() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cacheE_16)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cacheE_16() const { return ___U3CU3Ef__mgU24cacheE_16; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cacheE_16() { return &___U3CU3Ef__mgU24cacheE_16; }
	inline void set_U3CU3Ef__mgU24cacheE_16(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cacheE_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_17() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cacheF_17)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cacheF_17() const { return ___U3CU3Ef__mgU24cacheF_17; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cacheF_17() { return &___U3CU3Ef__mgU24cacheF_17; }
	inline void set_U3CU3Ef__mgU24cacheF_17(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cacheF_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_18() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache10_18)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache10_18() const { return ___U3CU3Ef__mgU24cache10_18; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache10_18() { return &___U3CU3Ef__mgU24cache10_18; }
	inline void set_U3CU3Ef__mgU24cache10_18(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache10_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_19() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache11_19)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache11_19() const { return ___U3CU3Ef__mgU24cache11_19; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache11_19() { return &___U3CU3Ef__mgU24cache11_19; }
	inline void set_U3CU3Ef__mgU24cache11_19(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache11_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_20() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache12_20)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache12_20() const { return ___U3CU3Ef__mgU24cache12_20; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache12_20() { return &___U3CU3Ef__mgU24cache12_20; }
	inline void set_U3CU3Ef__mgU24cache12_20(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache12_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache13_21() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache13_21)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache13_21() const { return ___U3CU3Ef__mgU24cache13_21; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache13_21() { return &___U3CU3Ef__mgU24cache13_21; }
	inline void set_U3CU3Ef__mgU24cache13_21(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache13_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache13_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache14_22() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache14_22)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache14_22() const { return ___U3CU3Ef__mgU24cache14_22; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache14_22() { return &___U3CU3Ef__mgU24cache14_22; }
	inline void set_U3CU3Ef__mgU24cache14_22(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache14_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache14_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache15_23() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache15_23)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache15_23() const { return ___U3CU3Ef__mgU24cache15_23; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache15_23() { return &___U3CU3Ef__mgU24cache15_23; }
	inline void set_U3CU3Ef__mgU24cache15_23(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache15_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache15_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache16_24() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache16_24)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache16_24() const { return ___U3CU3Ef__mgU24cache16_24; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache16_24() { return &___U3CU3Ef__mgU24cache16_24; }
	inline void set_U3CU3Ef__mgU24cache16_24(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache16_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache16_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache17_25() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache17_25)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache17_25() const { return ___U3CU3Ef__mgU24cache17_25; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache17_25() { return &___U3CU3Ef__mgU24cache17_25; }
	inline void set_U3CU3Ef__mgU24cache17_25(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache17_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache17_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache18_26() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache18_26)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache18_26() const { return ___U3CU3Ef__mgU24cache18_26; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache18_26() { return &___U3CU3Ef__mgU24cache18_26; }
	inline void set_U3CU3Ef__mgU24cache18_26(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache18_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache18_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache19_27() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache19_27)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache19_27() const { return ___U3CU3Ef__mgU24cache19_27; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache19_27() { return &___U3CU3Ef__mgU24cache19_27; }
	inline void set_U3CU3Ef__mgU24cache19_27(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache19_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache19_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1A_28() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1A_28)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1A_28() const { return ___U3CU3Ef__mgU24cache1A_28; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1A_28() { return &___U3CU3Ef__mgU24cache1A_28; }
	inline void set_U3CU3Ef__mgU24cache1A_28(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1A_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1A_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1B_29() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1B_29)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1B_29() const { return ___U3CU3Ef__mgU24cache1B_29; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1B_29() { return &___U3CU3Ef__mgU24cache1B_29; }
	inline void set_U3CU3Ef__mgU24cache1B_29(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1B_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1B_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1C_30() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1C_30)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1C_30() const { return ___U3CU3Ef__mgU24cache1C_30; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1C_30() { return &___U3CU3Ef__mgU24cache1C_30; }
	inline void set_U3CU3Ef__mgU24cache1C_30(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1C_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1C_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1D_31() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1D_31)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1D_31() const { return ___U3CU3Ef__mgU24cache1D_31; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1D_31() { return &___U3CU3Ef__mgU24cache1D_31; }
	inline void set_U3CU3Ef__mgU24cache1D_31(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1D_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1D_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1E_32() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1E_32)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1E_32() const { return ___U3CU3Ef__mgU24cache1E_32; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1E_32() { return &___U3CU3Ef__mgU24cache1E_32; }
	inline void set_U3CU3Ef__mgU24cache1E_32(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1E_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1E_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1F_33() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache1F_33)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache1F_33() const { return ___U3CU3Ef__mgU24cache1F_33; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache1F_33() { return &___U3CU3Ef__mgU24cache1F_33; }
	inline void set_U3CU3Ef__mgU24cache1F_33(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache1F_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1F_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache20_34() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache20_34)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache20_34() const { return ___U3CU3Ef__mgU24cache20_34; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache20_34() { return &___U3CU3Ef__mgU24cache20_34; }
	inline void set_U3CU3Ef__mgU24cache20_34(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache20_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache20_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache21_35() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache21_35)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache21_35() const { return ___U3CU3Ef__mgU24cache21_35; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache21_35() { return &___U3CU3Ef__mgU24cache21_35; }
	inline void set_U3CU3Ef__mgU24cache21_35(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache21_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache21_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache22_36() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache22_36)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache22_36() const { return ___U3CU3Ef__mgU24cache22_36; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache22_36() { return &___U3CU3Ef__mgU24cache22_36; }
	inline void set_U3CU3Ef__mgU24cache22_36(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache22_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache22_36), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache23_37() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache23_37)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache23_37() const { return ___U3CU3Ef__mgU24cache23_37; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache23_37() { return &___U3CU3Ef__mgU24cache23_37; }
	inline void set_U3CU3Ef__mgU24cache23_37(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache23_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache23_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache24_38() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache24_38)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache24_38() const { return ___U3CU3Ef__mgU24cache24_38; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache24_38() { return &___U3CU3Ef__mgU24cache24_38; }
	inline void set_U3CU3Ef__mgU24cache24_38(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache24_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache24_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache25_39() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache25_39)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache25_39() const { return ___U3CU3Ef__mgU24cache25_39; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache25_39() { return &___U3CU3Ef__mgU24cache25_39; }
	inline void set_U3CU3Ef__mgU24cache25_39(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache25_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache25_39), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache26_40() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache26_40)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache26_40() const { return ___U3CU3Ef__mgU24cache26_40; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache26_40() { return &___U3CU3Ef__mgU24cache26_40; }
	inline void set_U3CU3Ef__mgU24cache26_40(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache26_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache26_40), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache27_41() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache27_41)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache27_41() const { return ___U3CU3Ef__mgU24cache27_41; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache27_41() { return &___U3CU3Ef__mgU24cache27_41; }
	inline void set_U3CU3Ef__mgU24cache27_41(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache27_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache27_41), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache28_42() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache28_42)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache28_42() const { return ___U3CU3Ef__mgU24cache28_42; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache28_42() { return &___U3CU3Ef__mgU24cache28_42; }
	inline void set_U3CU3Ef__mgU24cache28_42(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache28_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache28_42), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache29_43() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache29_43)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache29_43() const { return ___U3CU3Ef__mgU24cache29_43; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache29_43() { return &___U3CU3Ef__mgU24cache29_43; }
	inline void set_U3CU3Ef__mgU24cache29_43(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache29_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache29_43), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2A_44() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2A_44)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2A_44() const { return ___U3CU3Ef__mgU24cache2A_44; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2A_44() { return &___U3CU3Ef__mgU24cache2A_44; }
	inline void set_U3CU3Ef__mgU24cache2A_44(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2A_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2A_44), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2B_45() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2B_45)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2B_45() const { return ___U3CU3Ef__mgU24cache2B_45; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2B_45() { return &___U3CU3Ef__mgU24cache2B_45; }
	inline void set_U3CU3Ef__mgU24cache2B_45(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2B_45 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2B_45), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2C_46() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2C_46)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2C_46() const { return ___U3CU3Ef__mgU24cache2C_46; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2C_46() { return &___U3CU3Ef__mgU24cache2C_46; }
	inline void set_U3CU3Ef__mgU24cache2C_46(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2C_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2C_46), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2D_47() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2D_47)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2D_47() const { return ___U3CU3Ef__mgU24cache2D_47; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2D_47() { return &___U3CU3Ef__mgU24cache2D_47; }
	inline void set_U3CU3Ef__mgU24cache2D_47(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2D_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2D_47), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2E_48() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2E_48)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2E_48() const { return ___U3CU3Ef__mgU24cache2E_48; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2E_48() { return &___U3CU3Ef__mgU24cache2E_48; }
	inline void set_U3CU3Ef__mgU24cache2E_48(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2E_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2E_48), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2F_49() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache2F_49)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache2F_49() const { return ___U3CU3Ef__mgU24cache2F_49; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache2F_49() { return &___U3CU3Ef__mgU24cache2F_49; }
	inline void set_U3CU3Ef__mgU24cache2F_49(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache2F_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2F_49), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache30_50() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache30_50)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache30_50() const { return ___U3CU3Ef__mgU24cache30_50; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache30_50() { return &___U3CU3Ef__mgU24cache30_50; }
	inline void set_U3CU3Ef__mgU24cache30_50(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache30_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache30_50), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache31_51() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache31_51)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache31_51() const { return ___U3CU3Ef__mgU24cache31_51; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache31_51() { return &___U3CU3Ef__mgU24cache31_51; }
	inline void set_U3CU3Ef__mgU24cache31_51(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache31_51 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache31_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache32_52() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache32_52)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache32_52() const { return ___U3CU3Ef__mgU24cache32_52; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache32_52() { return &___U3CU3Ef__mgU24cache32_52; }
	inline void set_U3CU3Ef__mgU24cache32_52(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache32_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache32_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache33_53() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache33_53)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache33_53() const { return ___U3CU3Ef__mgU24cache33_53; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache33_53() { return &___U3CU3Ef__mgU24cache33_53; }
	inline void set_U3CU3Ef__mgU24cache33_53(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache33_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache33_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache34_54() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache34_54)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache34_54() const { return ___U3CU3Ef__mgU24cache34_54; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache34_54() { return &___U3CU3Ef__mgU24cache34_54; }
	inline void set_U3CU3Ef__mgU24cache34_54(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache34_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache34_54), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache35_55() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache35_55)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache35_55() const { return ___U3CU3Ef__mgU24cache35_55; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache35_55() { return &___U3CU3Ef__mgU24cache35_55; }
	inline void set_U3CU3Ef__mgU24cache35_55(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache35_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache35_55), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache36_56() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache36_56)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache36_56() const { return ___U3CU3Ef__mgU24cache36_56; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache36_56() { return &___U3CU3Ef__mgU24cache36_56; }
	inline void set_U3CU3Ef__mgU24cache36_56(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache36_56 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache36_56), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache37_57() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache37_57)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache37_57() const { return ___U3CU3Ef__mgU24cache37_57; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache37_57() { return &___U3CU3Ef__mgU24cache37_57; }
	inline void set_U3CU3Ef__mgU24cache37_57(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache37_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache37_57), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache38_58() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache38_58)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache38_58() const { return ___U3CU3Ef__mgU24cache38_58; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache38_58() { return &___U3CU3Ef__mgU24cache38_58; }
	inline void set_U3CU3Ef__mgU24cache38_58(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache38_58 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache38_58), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache39_59() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache39_59)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache39_59() const { return ___U3CU3Ef__mgU24cache39_59; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache39_59() { return &___U3CU3Ef__mgU24cache39_59; }
	inline void set_U3CU3Ef__mgU24cache39_59(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache39_59 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache39_59), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3A_60() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3A_60)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3A_60() const { return ___U3CU3Ef__mgU24cache3A_60; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3A_60() { return &___U3CU3Ef__mgU24cache3A_60; }
	inline void set_U3CU3Ef__mgU24cache3A_60(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3A_60 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3A_60), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3B_61() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3B_61)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3B_61() const { return ___U3CU3Ef__mgU24cache3B_61; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3B_61() { return &___U3CU3Ef__mgU24cache3B_61; }
	inline void set_U3CU3Ef__mgU24cache3B_61(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3B_61 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3B_61), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3C_62() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3C_62)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3C_62() const { return ___U3CU3Ef__mgU24cache3C_62; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3C_62() { return &___U3CU3Ef__mgU24cache3C_62; }
	inline void set_U3CU3Ef__mgU24cache3C_62(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3C_62 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3C_62), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3D_63() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3D_63)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3D_63() const { return ___U3CU3Ef__mgU24cache3D_63; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3D_63() { return &___U3CU3Ef__mgU24cache3D_63; }
	inline void set_U3CU3Ef__mgU24cache3D_63(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3D_63 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3D_63), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3E_64() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3E_64)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3E_64() const { return ___U3CU3Ef__mgU24cache3E_64; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3E_64() { return &___U3CU3Ef__mgU24cache3E_64; }
	inline void set_U3CU3Ef__mgU24cache3E_64(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3E_64 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3E_64), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3F_65() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache3F_65)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache3F_65() const { return ___U3CU3Ef__mgU24cache3F_65; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache3F_65() { return &___U3CU3Ef__mgU24cache3F_65; }
	inline void set_U3CU3Ef__mgU24cache3F_65(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache3F_65 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3F_65), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache40_66() { return static_cast<int32_t>(offsetof(EasingFunction_t3263762189_StaticFields, ___U3CU3Ef__mgU24cache40_66)); }
	inline Function_t3898704989 * get_U3CU3Ef__mgU24cache40_66() const { return ___U3CU3Ef__mgU24cache40_66; }
	inline Function_t3898704989 ** get_address_of_U3CU3Ef__mgU24cache40_66() { return &___U3CU3Ef__mgU24cache40_66; }
	inline void set_U3CU3Ef__mgU24cache40_66(Function_t3898704989 * value)
	{
		___U3CU3Ef__mgU24cache40_66 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache40_66), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASINGFUNCTION_T3263762189_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef EASE_T2364754935_H
#define EASE_T2364754935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.EasingFunction/Ease
struct  Ease_t2364754935 
{
public:
	// System.Int32 HutongGames.EasingFunction/Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t2364754935, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T2364754935_H
#ifndef BASEORIENTATION_T2907697253_H
#define BASEORIENTATION_T2907697253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetDeviceRoll/BaseOrientation
struct  BaseOrientation_t2907697253 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetDeviceRoll/BaseOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BaseOrientation_t2907697253, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEORIENTATION_T2907697253_H
#ifndef MAPPROJECTION_T2129394632_H
#define MAPPROJECTION_T2129394632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ProjectLocationToMap/MapProjection
struct  MapProjection_t2129394632 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.ProjectLocationToMap/MapProjection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MapProjection_t2129394632, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPROJECTION_T2129394632_H
#ifndef OFFSETOPTIONS_T1111682770_H
#define OFFSETOPTIONS_T1111682770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchGUIEvent/OffsetOptions
struct  OffsetOptions_t1111682770 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.TouchGUIEvent/OffsetOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OffsetOptions_t1111682770, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OFFSETOPTIONS_T1111682770_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef FULLSCREENMOVIECONTROLMODE_T3573826744_H
#define FULLSCREENMOVIECONTROLMODE_T3573826744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FullScreenMovieControlMode
struct  FullScreenMovieControlMode_t3573826744 
{
public:
	// System.Int32 UnityEngine.FullScreenMovieControlMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FullScreenMovieControlMode_t3573826744, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLSCREENMOVIECONTROLMODE_T3573826744_H
#ifndef FULLSCREENMOVIESCALINGMODE_T695529244_H
#define FULLSCREENMOVIESCALINGMODE_T695529244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FullScreenMovieScalingMode
struct  FullScreenMovieScalingMode_t695529244 
{
public:
	// System.Int32 UnityEngine.FullScreenMovieScalingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FullScreenMovieScalingMode_t695529244, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLSCREENMOVIESCALINGMODE_T695529244_H
#ifndef SCALEMODE_T2341947364_H
#define SCALEMODE_T2341947364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScaleMode
struct  ScaleMode_t2341947364 
{
public:
	// System.Int32 UnityEngine.ScaleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleMode_t2341947364, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T2341947364_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef ACTIVATEGAMEOBJECT_T2035484578_H
#define ACTIVATEGAMEOBJECT_T2035484578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ActivateGameObject
struct  ActivateGameObject_t2035484578  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ActivateGameObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ActivateGameObject::activate
	FsmBool_t163807967 * ___activate_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ActivateGameObject::recursive
	FsmBool_t163807967 * ___recursive_16;
	// System.Boolean HutongGames.PlayMaker.Actions.ActivateGameObject::resetOnExit
	bool ___resetOnExit_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ActivateGameObject::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ActivateGameObject::activatedGameObject
	GameObject_t1113636619 * ___activatedGameObject_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(ActivateGameObject_t2035484578, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_activate_15() { return static_cast<int32_t>(offsetof(ActivateGameObject_t2035484578, ___activate_15)); }
	inline FsmBool_t163807967 * get_activate_15() const { return ___activate_15; }
	inline FsmBool_t163807967 ** get_address_of_activate_15() { return &___activate_15; }
	inline void set_activate_15(FsmBool_t163807967 * value)
	{
		___activate_15 = value;
		Il2CppCodeGenWriteBarrier((&___activate_15), value);
	}

	inline static int32_t get_offset_of_recursive_16() { return static_cast<int32_t>(offsetof(ActivateGameObject_t2035484578, ___recursive_16)); }
	inline FsmBool_t163807967 * get_recursive_16() const { return ___recursive_16; }
	inline FsmBool_t163807967 ** get_address_of_recursive_16() { return &___recursive_16; }
	inline void set_recursive_16(FsmBool_t163807967 * value)
	{
		___recursive_16 = value;
		Il2CppCodeGenWriteBarrier((&___recursive_16), value);
	}

	inline static int32_t get_offset_of_resetOnExit_17() { return static_cast<int32_t>(offsetof(ActivateGameObject_t2035484578, ___resetOnExit_17)); }
	inline bool get_resetOnExit_17() const { return ___resetOnExit_17; }
	inline bool* get_address_of_resetOnExit_17() { return &___resetOnExit_17; }
	inline void set_resetOnExit_17(bool value)
	{
		___resetOnExit_17 = value;
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(ActivateGameObject_t2035484578, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_activatedGameObject_19() { return static_cast<int32_t>(offsetof(ActivateGameObject_t2035484578, ___activatedGameObject_19)); }
	inline GameObject_t1113636619 * get_activatedGameObject_19() const { return ___activatedGameObject_19; }
	inline GameObject_t1113636619 ** get_address_of_activatedGameObject_19() { return &___activatedGameObject_19; }
	inline void set_activatedGameObject_19(GameObject_t1113636619 * value)
	{
		___activatedGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___activatedGameObject_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATEGAMEOBJECT_T2035484578_H
#ifndef ADDCOMPONENT_T2682014428_H
#define ADDCOMPONENT_T2682014428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddComponent
struct  AddComponent_t2682014428  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddComponent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddComponent::component
	FsmString_t1785915204 * ___component_15;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.AddComponent::storeComponent
	FsmObject_t2606870197 * ___storeComponent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddComponent::removeOnExit
	FsmBool_t163807967 * ___removeOnExit_17;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.AddComponent::addedComponent
	Component_t1923634451 * ___addedComponent_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AddComponent_t2682014428, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_component_15() { return static_cast<int32_t>(offsetof(AddComponent_t2682014428, ___component_15)); }
	inline FsmString_t1785915204 * get_component_15() const { return ___component_15; }
	inline FsmString_t1785915204 ** get_address_of_component_15() { return &___component_15; }
	inline void set_component_15(FsmString_t1785915204 * value)
	{
		___component_15 = value;
		Il2CppCodeGenWriteBarrier((&___component_15), value);
	}

	inline static int32_t get_offset_of_storeComponent_16() { return static_cast<int32_t>(offsetof(AddComponent_t2682014428, ___storeComponent_16)); }
	inline FsmObject_t2606870197 * get_storeComponent_16() const { return ___storeComponent_16; }
	inline FsmObject_t2606870197 ** get_address_of_storeComponent_16() { return &___storeComponent_16; }
	inline void set_storeComponent_16(FsmObject_t2606870197 * value)
	{
		___storeComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeComponent_16), value);
	}

	inline static int32_t get_offset_of_removeOnExit_17() { return static_cast<int32_t>(offsetof(AddComponent_t2682014428, ___removeOnExit_17)); }
	inline FsmBool_t163807967 * get_removeOnExit_17() const { return ___removeOnExit_17; }
	inline FsmBool_t163807967 ** get_address_of_removeOnExit_17() { return &___removeOnExit_17; }
	inline void set_removeOnExit_17(FsmBool_t163807967 * value)
	{
		___removeOnExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___removeOnExit_17), value);
	}

	inline static int32_t get_offset_of_addedComponent_18() { return static_cast<int32_t>(offsetof(AddComponent_t2682014428, ___addedComponent_18)); }
	inline Component_t1923634451 * get_addedComponent_18() const { return ___addedComponent_18; }
	inline Component_t1923634451 ** get_address_of_addedComponent_18() { return &___addedComponent_18; }
	inline void set_addedComponent_18(Component_t1923634451 * value)
	{
		___addedComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___addedComponent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDCOMPONENT_T2682014428_H
#ifndef COMPONENTACTION_1_T3279520548_H
#define COMPONENTACTION_1_T3279520548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>
struct  ComponentAction_1_t3279520548  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	GUIText_t402233326 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3279520548, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3279520548, ___cachedComponent_15)); }
	inline GUIText_t402233326 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline GUIText_t402233326 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(GUIText_t402233326 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T3279520548_H
#ifndef COMPONENTACTION_1_T3829190823_H
#define COMPONENTACTION_1_T3829190823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUITexture>
struct  ComponentAction_1_t3829190823  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	GUITexture_t951903601 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3829190823, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3829190823, ___cachedComponent_15)); }
	inline GUITexture_t951903601 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline GUITexture_t951903601 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(GUITexture_t951903601 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T3829190823_H
#ifndef COMPONENTACTION_1_T1209346957_H
#define COMPONENTACTION_1_T1209346957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Renderer>
struct  ComponentAction_1_t1209346957  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Renderer_t2627027031 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1209346957, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1209346957, ___cachedComponent_15)); }
	inline Renderer_t2627027031 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Renderer_t2627027031 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Renderer_t2627027031 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1209346957_H
#ifndef CREATEEMPTYOBJECT_T4245439205_H
#define CREATEEMPTYOBJECT_T4245439205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CreateEmptyObject
struct  CreateEmptyObject_t4245439205  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateEmptyObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateEmptyObject::spawnPoint
	FsmGameObject_t3581898942 * ___spawnPoint_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateEmptyObject::position
	FsmVector3_t626444517 * ___position_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateEmptyObject::rotation
	FsmVector3_t626444517 * ___rotation_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateEmptyObject::storeObject
	FsmGameObject_t3581898942 * ___storeObject_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t4245439205, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_spawnPoint_15() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t4245439205, ___spawnPoint_15)); }
	inline FsmGameObject_t3581898942 * get_spawnPoint_15() const { return ___spawnPoint_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_spawnPoint_15() { return &___spawnPoint_15; }
	inline void set_spawnPoint_15(FsmGameObject_t3581898942 * value)
	{
		___spawnPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPoint_15), value);
	}

	inline static int32_t get_offset_of_position_16() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t4245439205, ___position_16)); }
	inline FsmVector3_t626444517 * get_position_16() const { return ___position_16; }
	inline FsmVector3_t626444517 ** get_address_of_position_16() { return &___position_16; }
	inline void set_position_16(FsmVector3_t626444517 * value)
	{
		___position_16 = value;
		Il2CppCodeGenWriteBarrier((&___position_16), value);
	}

	inline static int32_t get_offset_of_rotation_17() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t4245439205, ___rotation_17)); }
	inline FsmVector3_t626444517 * get_rotation_17() const { return ___rotation_17; }
	inline FsmVector3_t626444517 ** get_address_of_rotation_17() { return &___rotation_17; }
	inline void set_rotation_17(FsmVector3_t626444517 * value)
	{
		___rotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_17), value);
	}

	inline static int32_t get_offset_of_storeObject_18() { return static_cast<int32_t>(offsetof(CreateEmptyObject_t4245439205, ___storeObject_18)); }
	inline FsmGameObject_t3581898942 * get_storeObject_18() const { return ___storeObject_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeObject_18() { return &___storeObject_18; }
	inline void set_storeObject_18(FsmGameObject_t3581898942 * value)
	{
		___storeObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeObject_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEEMPTYOBJECT_T4245439205_H
#ifndef CREATEOBJECT_T2822511132_H
#define CREATEOBJECT_T2822511132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CreateObject
struct  CreateObject_t2822511132  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::spawnPoint
	FsmGameObject_t3581898942 * ___spawnPoint_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateObject::position
	FsmVector3_t626444517 * ___position_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.CreateObject::rotation
	FsmVector3_t626444517 * ___rotation_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CreateObject::storeObject
	FsmGameObject_t3581898942 * ___storeObject_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(CreateObject_t2822511132, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_spawnPoint_15() { return static_cast<int32_t>(offsetof(CreateObject_t2822511132, ___spawnPoint_15)); }
	inline FsmGameObject_t3581898942 * get_spawnPoint_15() const { return ___spawnPoint_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_spawnPoint_15() { return &___spawnPoint_15; }
	inline void set_spawnPoint_15(FsmGameObject_t3581898942 * value)
	{
		___spawnPoint_15 = value;
		Il2CppCodeGenWriteBarrier((&___spawnPoint_15), value);
	}

	inline static int32_t get_offset_of_position_16() { return static_cast<int32_t>(offsetof(CreateObject_t2822511132, ___position_16)); }
	inline FsmVector3_t626444517 * get_position_16() const { return ___position_16; }
	inline FsmVector3_t626444517 ** get_address_of_position_16() { return &___position_16; }
	inline void set_position_16(FsmVector3_t626444517 * value)
	{
		___position_16 = value;
		Il2CppCodeGenWriteBarrier((&___position_16), value);
	}

	inline static int32_t get_offset_of_rotation_17() { return static_cast<int32_t>(offsetof(CreateObject_t2822511132, ___rotation_17)); }
	inline FsmVector3_t626444517 * get_rotation_17() const { return ___rotation_17; }
	inline FsmVector3_t626444517 ** get_address_of_rotation_17() { return &___rotation_17; }
	inline void set_rotation_17(FsmVector3_t626444517 * value)
	{
		___rotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_17), value);
	}

	inline static int32_t get_offset_of_storeObject_18() { return static_cast<int32_t>(offsetof(CreateObject_t2822511132, ___storeObject_18)); }
	inline FsmGameObject_t3581898942 * get_storeObject_18() const { return ___storeObject_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeObject_18() { return &___storeObject_18; }
	inline void set_storeObject_18(FsmGameObject_t3581898942 * value)
	{
		___storeObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeObject_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEOBJECT_T2822511132_H
#ifndef DESTROYCOMPONENT_T2397952545_H
#define DESTROYCOMPONENT_T2397952545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyComponent
struct  DestroyComponent_t2397952545  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DestroyComponent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DestroyComponent::component
	FsmString_t1785915204 * ___component_15;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.DestroyComponent::aComponent
	Component_t1923634451 * ___aComponent_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(DestroyComponent_t2397952545, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_component_15() { return static_cast<int32_t>(offsetof(DestroyComponent_t2397952545, ___component_15)); }
	inline FsmString_t1785915204 * get_component_15() const { return ___component_15; }
	inline FsmString_t1785915204 ** get_address_of_component_15() { return &___component_15; }
	inline void set_component_15(FsmString_t1785915204 * value)
	{
		___component_15 = value;
		Il2CppCodeGenWriteBarrier((&___component_15), value);
	}

	inline static int32_t get_offset_of_aComponent_16() { return static_cast<int32_t>(offsetof(DestroyComponent_t2397952545, ___aComponent_16)); }
	inline Component_t1923634451 * get_aComponent_16() const { return ___aComponent_16; }
	inline Component_t1923634451 ** get_address_of_aComponent_16() { return &___aComponent_16; }
	inline void set_aComponent_16(Component_t1923634451 * value)
	{
		___aComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___aComponent_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYCOMPONENT_T2397952545_H
#ifndef DESTROYOBJECT_T4173337614_H
#define DESTROYOBJECT_T4173337614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyObject
struct  DestroyObject_t4173337614  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DestroyObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DestroyObject::delay
	FsmFloat_t2883254149 * ___delay_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DestroyObject::detachChildren
	FsmBool_t163807967 * ___detachChildren_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(DestroyObject_t4173337614, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_delay_15() { return static_cast<int32_t>(offsetof(DestroyObject_t4173337614, ___delay_15)); }
	inline FsmFloat_t2883254149 * get_delay_15() const { return ___delay_15; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_15() { return &___delay_15; }
	inline void set_delay_15(FsmFloat_t2883254149 * value)
	{
		___delay_15 = value;
		Il2CppCodeGenWriteBarrier((&___delay_15), value);
	}

	inline static int32_t get_offset_of_detachChildren_16() { return static_cast<int32_t>(offsetof(DestroyObject_t4173337614, ___detachChildren_16)); }
	inline FsmBool_t163807967 * get_detachChildren_16() const { return ___detachChildren_16; }
	inline FsmBool_t163807967 ** get_address_of_detachChildren_16() { return &___detachChildren_16; }
	inline void set_detachChildren_16(FsmBool_t163807967 * value)
	{
		___detachChildren_16 = value;
		Il2CppCodeGenWriteBarrier((&___detachChildren_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYOBJECT_T4173337614_H
#ifndef DESTROYOBJECTS_T2663322638_H
#define DESTROYOBJECTS_T2663322638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyObjects
struct  DestroyObjects_t2663322638  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.DestroyObjects::gameObjects
	FsmArray_t1756862219 * ___gameObjects_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DestroyObjects::delay
	FsmFloat_t2883254149 * ___delay_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DestroyObjects::detachChildren
	FsmBool_t163807967 * ___detachChildren_16;

public:
	inline static int32_t get_offset_of_gameObjects_14() { return static_cast<int32_t>(offsetof(DestroyObjects_t2663322638, ___gameObjects_14)); }
	inline FsmArray_t1756862219 * get_gameObjects_14() const { return ___gameObjects_14; }
	inline FsmArray_t1756862219 ** get_address_of_gameObjects_14() { return &___gameObjects_14; }
	inline void set_gameObjects_14(FsmArray_t1756862219 * value)
	{
		___gameObjects_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjects_14), value);
	}

	inline static int32_t get_offset_of_delay_15() { return static_cast<int32_t>(offsetof(DestroyObjects_t2663322638, ___delay_15)); }
	inline FsmFloat_t2883254149 * get_delay_15() const { return ___delay_15; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_15() { return &___delay_15; }
	inline void set_delay_15(FsmFloat_t2883254149 * value)
	{
		___delay_15 = value;
		Il2CppCodeGenWriteBarrier((&___delay_15), value);
	}

	inline static int32_t get_offset_of_detachChildren_16() { return static_cast<int32_t>(offsetof(DestroyObjects_t2663322638, ___detachChildren_16)); }
	inline FsmBool_t163807967 * get_detachChildren_16() const { return ___detachChildren_16; }
	inline FsmBool_t163807967 ** get_address_of_detachChildren_16() { return &___detachChildren_16; }
	inline void set_detachChildren_16(FsmBool_t163807967 * value)
	{
		___detachChildren_16 = value;
		Il2CppCodeGenWriteBarrier((&___detachChildren_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYOBJECTS_T2663322638_H
#ifndef DESTROYSELF_T235920970_H
#define DESTROYSELF_T235920970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroySelf
struct  DestroySelf_t235920970  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DestroySelf::detachChildren
	FsmBool_t163807967 * ___detachChildren_14;

public:
	inline static int32_t get_offset_of_detachChildren_14() { return static_cast<int32_t>(offsetof(DestroySelf_t235920970, ___detachChildren_14)); }
	inline FsmBool_t163807967 * get_detachChildren_14() const { return ___detachChildren_14; }
	inline FsmBool_t163807967 ** get_address_of_detachChildren_14() { return &___detachChildren_14; }
	inline void set_detachChildren_14(FsmBool_t163807967 * value)
	{
		___detachChildren_14 = value;
		Il2CppCodeGenWriteBarrier((&___detachChildren_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYSELF_T235920970_H
#ifndef DETACHCHILDREN_T1056900145_H
#define DETACHCHILDREN_T1056900145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DetachChildren
struct  DetachChildren_t1056900145  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DetachChildren::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(DetachChildren_t1056900145, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETACHCHILDREN_T1056900145_H
#ifndef DEVICEPLAYFULLSCREENMOVIE_T1747172878_H
#define DEVICEPLAYFULLSCREENMOVIE_T1747172878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie
struct  DevicePlayFullScreenMovie_t1747172878  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::moviePath
	FsmString_t1785915204 * ___moviePath_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::fadeColor
	FsmColor_t1738900188 * ___fadeColor_15;
	// UnityEngine.FullScreenMovieControlMode HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::movieControlMode
	int32_t ___movieControlMode_16;
	// UnityEngine.FullScreenMovieScalingMode HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::movieScalingMode
	int32_t ___movieScalingMode_17;

public:
	inline static int32_t get_offset_of_moviePath_14() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t1747172878, ___moviePath_14)); }
	inline FsmString_t1785915204 * get_moviePath_14() const { return ___moviePath_14; }
	inline FsmString_t1785915204 ** get_address_of_moviePath_14() { return &___moviePath_14; }
	inline void set_moviePath_14(FsmString_t1785915204 * value)
	{
		___moviePath_14 = value;
		Il2CppCodeGenWriteBarrier((&___moviePath_14), value);
	}

	inline static int32_t get_offset_of_fadeColor_15() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t1747172878, ___fadeColor_15)); }
	inline FsmColor_t1738900188 * get_fadeColor_15() const { return ___fadeColor_15; }
	inline FsmColor_t1738900188 ** get_address_of_fadeColor_15() { return &___fadeColor_15; }
	inline void set_fadeColor_15(FsmColor_t1738900188 * value)
	{
		___fadeColor_15 = value;
		Il2CppCodeGenWriteBarrier((&___fadeColor_15), value);
	}

	inline static int32_t get_offset_of_movieControlMode_16() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t1747172878, ___movieControlMode_16)); }
	inline int32_t get_movieControlMode_16() const { return ___movieControlMode_16; }
	inline int32_t* get_address_of_movieControlMode_16() { return &___movieControlMode_16; }
	inline void set_movieControlMode_16(int32_t value)
	{
		___movieControlMode_16 = value;
	}

	inline static int32_t get_offset_of_movieScalingMode_17() { return static_cast<int32_t>(offsetof(DevicePlayFullScreenMovie_t1747172878, ___movieScalingMode_17)); }
	inline int32_t get_movieScalingMode_17() const { return ___movieScalingMode_17; }
	inline int32_t* get_address_of_movieScalingMode_17() { return &___movieScalingMode_17; }
	inline void set_movieScalingMode_17(int32_t value)
	{
		___movieScalingMode_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEPLAYFULLSCREENMOVIE_T1747172878_H
#ifndef DEVICESHAKEEVENT_T415947583_H
#define DEVICESHAKEEVENT_T415947583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DeviceShakeEvent
struct  DeviceShakeEvent_t415947583  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DeviceShakeEvent::shakeThreshold
	FsmFloat_t2883254149 * ___shakeThreshold_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DeviceShakeEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;

public:
	inline static int32_t get_offset_of_shakeThreshold_14() { return static_cast<int32_t>(offsetof(DeviceShakeEvent_t415947583, ___shakeThreshold_14)); }
	inline FsmFloat_t2883254149 * get_shakeThreshold_14() const { return ___shakeThreshold_14; }
	inline FsmFloat_t2883254149 ** get_address_of_shakeThreshold_14() { return &___shakeThreshold_14; }
	inline void set_shakeThreshold_14(FsmFloat_t2883254149 * value)
	{
		___shakeThreshold_14 = value;
		Il2CppCodeGenWriteBarrier((&___shakeThreshold_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(DeviceShakeEvent_t415947583, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICESHAKEEVENT_T415947583_H
#ifndef DEVICEVIBRATE_T3177149157_H
#define DEVICEVIBRATE_T3177149157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DeviceVibrate
struct  DeviceVibrate_t3177149157  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICEVIBRATE_T3177149157_H
#ifndef DRAWFULLSCREENCOLOR_T1684922187_H
#define DRAWFULLSCREENCOLOR_T1684922187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawFullscreenColor
struct  DrawFullscreenColor_t1684922187  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DrawFullscreenColor::color
	FsmColor_t1738900188 * ___color_14;

public:
	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(DrawFullscreenColor_t1684922187, ___color_14)); }
	inline FsmColor_t1738900188 * get_color_14() const { return ___color_14; }
	inline FsmColor_t1738900188 ** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(FsmColor_t1738900188 * value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWFULLSCREENCOLOR_T1684922187_H
#ifndef DRAWTEXTURE_T1719845717_H
#define DRAWTEXTURE_T1719845717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DrawTexture
struct  DrawTexture_t1719845717  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.DrawTexture::texture
	FsmTexture_t1738787045 * ___texture_14;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.DrawTexture::screenRect
	FsmRect_t3649179877 * ___screenRect_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::left
	FsmFloat_t2883254149 * ___left_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::top
	FsmFloat_t2883254149 * ___top_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::width
	FsmFloat_t2883254149 * ___width_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::height
	FsmFloat_t2883254149 * ___height_19;
	// UnityEngine.ScaleMode HutongGames.PlayMaker.Actions.DrawTexture::scaleMode
	int32_t ___scaleMode_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DrawTexture::alphaBlend
	FsmBool_t163807967 * ___alphaBlend_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DrawTexture::imageAspect
	FsmFloat_t2883254149 * ___imageAspect_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DrawTexture::normalized
	FsmBool_t163807967 * ___normalized_23;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.DrawTexture::rect
	Rect_t2360479859  ___rect_24;

public:
	inline static int32_t get_offset_of_texture_14() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___texture_14)); }
	inline FsmTexture_t1738787045 * get_texture_14() const { return ___texture_14; }
	inline FsmTexture_t1738787045 ** get_address_of_texture_14() { return &___texture_14; }
	inline void set_texture_14(FsmTexture_t1738787045 * value)
	{
		___texture_14 = value;
		Il2CppCodeGenWriteBarrier((&___texture_14), value);
	}

	inline static int32_t get_offset_of_screenRect_15() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___screenRect_15)); }
	inline FsmRect_t3649179877 * get_screenRect_15() const { return ___screenRect_15; }
	inline FsmRect_t3649179877 ** get_address_of_screenRect_15() { return &___screenRect_15; }
	inline void set_screenRect_15(FsmRect_t3649179877 * value)
	{
		___screenRect_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenRect_15), value);
	}

	inline static int32_t get_offset_of_left_16() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___left_16)); }
	inline FsmFloat_t2883254149 * get_left_16() const { return ___left_16; }
	inline FsmFloat_t2883254149 ** get_address_of_left_16() { return &___left_16; }
	inline void set_left_16(FsmFloat_t2883254149 * value)
	{
		___left_16 = value;
		Il2CppCodeGenWriteBarrier((&___left_16), value);
	}

	inline static int32_t get_offset_of_top_17() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___top_17)); }
	inline FsmFloat_t2883254149 * get_top_17() const { return ___top_17; }
	inline FsmFloat_t2883254149 ** get_address_of_top_17() { return &___top_17; }
	inline void set_top_17(FsmFloat_t2883254149 * value)
	{
		___top_17 = value;
		Il2CppCodeGenWriteBarrier((&___top_17), value);
	}

	inline static int32_t get_offset_of_width_18() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___width_18)); }
	inline FsmFloat_t2883254149 * get_width_18() const { return ___width_18; }
	inline FsmFloat_t2883254149 ** get_address_of_width_18() { return &___width_18; }
	inline void set_width_18(FsmFloat_t2883254149 * value)
	{
		___width_18 = value;
		Il2CppCodeGenWriteBarrier((&___width_18), value);
	}

	inline static int32_t get_offset_of_height_19() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___height_19)); }
	inline FsmFloat_t2883254149 * get_height_19() const { return ___height_19; }
	inline FsmFloat_t2883254149 ** get_address_of_height_19() { return &___height_19; }
	inline void set_height_19(FsmFloat_t2883254149 * value)
	{
		___height_19 = value;
		Il2CppCodeGenWriteBarrier((&___height_19), value);
	}

	inline static int32_t get_offset_of_scaleMode_20() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___scaleMode_20)); }
	inline int32_t get_scaleMode_20() const { return ___scaleMode_20; }
	inline int32_t* get_address_of_scaleMode_20() { return &___scaleMode_20; }
	inline void set_scaleMode_20(int32_t value)
	{
		___scaleMode_20 = value;
	}

	inline static int32_t get_offset_of_alphaBlend_21() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___alphaBlend_21)); }
	inline FsmBool_t163807967 * get_alphaBlend_21() const { return ___alphaBlend_21; }
	inline FsmBool_t163807967 ** get_address_of_alphaBlend_21() { return &___alphaBlend_21; }
	inline void set_alphaBlend_21(FsmBool_t163807967 * value)
	{
		___alphaBlend_21 = value;
		Il2CppCodeGenWriteBarrier((&___alphaBlend_21), value);
	}

	inline static int32_t get_offset_of_imageAspect_22() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___imageAspect_22)); }
	inline FsmFloat_t2883254149 * get_imageAspect_22() const { return ___imageAspect_22; }
	inline FsmFloat_t2883254149 ** get_address_of_imageAspect_22() { return &___imageAspect_22; }
	inline void set_imageAspect_22(FsmFloat_t2883254149 * value)
	{
		___imageAspect_22 = value;
		Il2CppCodeGenWriteBarrier((&___imageAspect_22), value);
	}

	inline static int32_t get_offset_of_normalized_23() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___normalized_23)); }
	inline FsmBool_t163807967 * get_normalized_23() const { return ___normalized_23; }
	inline FsmBool_t163807967 ** get_address_of_normalized_23() { return &___normalized_23; }
	inline void set_normalized_23(FsmBool_t163807967 * value)
	{
		___normalized_23 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_23), value);
	}

	inline static int32_t get_offset_of_rect_24() { return static_cast<int32_t>(offsetof(DrawTexture_t1719845717, ___rect_24)); }
	inline Rect_t2360479859  get_rect_24() const { return ___rect_24; }
	inline Rect_t2360479859 * get_address_of_rect_24() { return &___rect_24; }
	inline void set_rect_24(Rect_t2360479859  value)
	{
		___rect_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWTEXTURE_T1719845717_H
#ifndef ENABLEGUI_T1143397964_H
#define ENABLEGUI_T1143397964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableGUI
struct  EnableGUI_t1143397964  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableGUI::enableGUI
	FsmBool_t163807967 * ___enableGUI_14;

public:
	inline static int32_t get_offset_of_enableGUI_14() { return static_cast<int32_t>(offsetof(EnableGUI_t1143397964, ___enableGUI_14)); }
	inline FsmBool_t163807967 * get_enableGUI_14() const { return ___enableGUI_14; }
	inline FsmBool_t163807967 ** get_address_of_enableGUI_14() { return &___enableGUI_14; }
	inline void set_enableGUI_14(FsmBool_t163807967 * value)
	{
		___enableGUI_14 = value;
		Il2CppCodeGenWriteBarrier((&___enableGUI_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEGUI_T1143397964_H
#ifndef FINDCHILD_T142528656_H
#define FINDCHILD_T142528656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindChild
struct  FindChild_t142528656  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.FindChild::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindChild::childName
	FsmString_t1785915204 * ___childName_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FindChild::storeResult
	FsmGameObject_t3581898942 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(FindChild_t142528656, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_childName_15() { return static_cast<int32_t>(offsetof(FindChild_t142528656, ___childName_15)); }
	inline FsmString_t1785915204 * get_childName_15() const { return ___childName_15; }
	inline FsmString_t1785915204 ** get_address_of_childName_15() { return &___childName_15; }
	inline void set_childName_15(FsmString_t1785915204 * value)
	{
		___childName_15 = value;
		Il2CppCodeGenWriteBarrier((&___childName_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(FindChild_t142528656, ___storeResult_16)); }
	inline FsmGameObject_t3581898942 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmGameObject_t3581898942 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDCHILD_T142528656_H
#ifndef FINDCLOSEST_T375870294_H
#define FINDCLOSEST_T375870294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindClosest
struct  FindClosest_t375870294  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.FindClosest::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindClosest::withTag
	FsmString_t1785915204 * ___withTag_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FindClosest::ignoreOwner
	FsmBool_t163807967 * ___ignoreOwner_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FindClosest::mustBeVisible
	FsmBool_t163807967 * ___mustBeVisible_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FindClosest::storeObject
	FsmGameObject_t3581898942 * ___storeObject_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FindClosest::storeDistance
	FsmFloat_t2883254149 * ___storeDistance_19;
	// System.Boolean HutongGames.PlayMaker.Actions.FindClosest::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_withTag_15() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___withTag_15)); }
	inline FsmString_t1785915204 * get_withTag_15() const { return ___withTag_15; }
	inline FsmString_t1785915204 ** get_address_of_withTag_15() { return &___withTag_15; }
	inline void set_withTag_15(FsmString_t1785915204 * value)
	{
		___withTag_15 = value;
		Il2CppCodeGenWriteBarrier((&___withTag_15), value);
	}

	inline static int32_t get_offset_of_ignoreOwner_16() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___ignoreOwner_16)); }
	inline FsmBool_t163807967 * get_ignoreOwner_16() const { return ___ignoreOwner_16; }
	inline FsmBool_t163807967 ** get_address_of_ignoreOwner_16() { return &___ignoreOwner_16; }
	inline void set_ignoreOwner_16(FsmBool_t163807967 * value)
	{
		___ignoreOwner_16 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreOwner_16), value);
	}

	inline static int32_t get_offset_of_mustBeVisible_17() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___mustBeVisible_17)); }
	inline FsmBool_t163807967 * get_mustBeVisible_17() const { return ___mustBeVisible_17; }
	inline FsmBool_t163807967 ** get_address_of_mustBeVisible_17() { return &___mustBeVisible_17; }
	inline void set_mustBeVisible_17(FsmBool_t163807967 * value)
	{
		___mustBeVisible_17 = value;
		Il2CppCodeGenWriteBarrier((&___mustBeVisible_17), value);
	}

	inline static int32_t get_offset_of_storeObject_18() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___storeObject_18)); }
	inline FsmGameObject_t3581898942 * get_storeObject_18() const { return ___storeObject_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeObject_18() { return &___storeObject_18; }
	inline void set_storeObject_18(FsmGameObject_t3581898942 * value)
	{
		___storeObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeObject_18), value);
	}

	inline static int32_t get_offset_of_storeDistance_19() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___storeDistance_19)); }
	inline FsmFloat_t2883254149 * get_storeDistance_19() const { return ___storeDistance_19; }
	inline FsmFloat_t2883254149 ** get_address_of_storeDistance_19() { return &___storeDistance_19; }
	inline void set_storeDistance_19(FsmFloat_t2883254149 * value)
	{
		___storeDistance_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeDistance_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(FindClosest_t375870294, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDCLOSEST_T375870294_H
#ifndef FINDGAMEOBJECT_T3470023239_H
#define FINDGAMEOBJECT_T3470023239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindGameObject
struct  FindGameObject_t3470023239  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindGameObject::objectName
	FsmString_t1785915204 * ___objectName_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindGameObject::withTag
	FsmString_t1785915204 * ___withTag_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FindGameObject::store
	FsmGameObject_t3581898942 * ___store_16;

public:
	inline static int32_t get_offset_of_objectName_14() { return static_cast<int32_t>(offsetof(FindGameObject_t3470023239, ___objectName_14)); }
	inline FsmString_t1785915204 * get_objectName_14() const { return ___objectName_14; }
	inline FsmString_t1785915204 ** get_address_of_objectName_14() { return &___objectName_14; }
	inline void set_objectName_14(FsmString_t1785915204 * value)
	{
		___objectName_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectName_14), value);
	}

	inline static int32_t get_offset_of_withTag_15() { return static_cast<int32_t>(offsetof(FindGameObject_t3470023239, ___withTag_15)); }
	inline FsmString_t1785915204 * get_withTag_15() const { return ___withTag_15; }
	inline FsmString_t1785915204 ** get_address_of_withTag_15() { return &___withTag_15; }
	inline void set_withTag_15(FsmString_t1785915204 * value)
	{
		___withTag_15 = value;
		Il2CppCodeGenWriteBarrier((&___withTag_15), value);
	}

	inline static int32_t get_offset_of_store_16() { return static_cast<int32_t>(offsetof(FindGameObject_t3470023239, ___store_16)); }
	inline FsmGameObject_t3581898942 * get_store_16() const { return ___store_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_store_16() { return &___store_16; }
	inline void set_store_16(FsmGameObject_t3581898942 * value)
	{
		___store_16 = value;
		Il2CppCodeGenWriteBarrier((&___store_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDGAMEOBJECT_T3470023239_H
#ifndef GUIACTION_T3750965478_H
#define GUIACTION_T3750965478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIAction
struct  GUIAction_t3750965478  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GUIAction::screenRect
	FsmRect_t3649179877 * ___screenRect_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIAction::left
	FsmFloat_t2883254149 * ___left_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIAction::top
	FsmFloat_t2883254149 * ___top_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIAction::width
	FsmFloat_t2883254149 * ___width_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIAction::height
	FsmFloat_t2883254149 * ___height_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIAction::normalized
	FsmBool_t163807967 * ___normalized_19;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.GUIAction::rect
	Rect_t2360479859  ___rect_20;

public:
	inline static int32_t get_offset_of_screenRect_14() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___screenRect_14)); }
	inline FsmRect_t3649179877 * get_screenRect_14() const { return ___screenRect_14; }
	inline FsmRect_t3649179877 ** get_address_of_screenRect_14() { return &___screenRect_14; }
	inline void set_screenRect_14(FsmRect_t3649179877 * value)
	{
		___screenRect_14 = value;
		Il2CppCodeGenWriteBarrier((&___screenRect_14), value);
	}

	inline static int32_t get_offset_of_left_15() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___left_15)); }
	inline FsmFloat_t2883254149 * get_left_15() const { return ___left_15; }
	inline FsmFloat_t2883254149 ** get_address_of_left_15() { return &___left_15; }
	inline void set_left_15(FsmFloat_t2883254149 * value)
	{
		___left_15 = value;
		Il2CppCodeGenWriteBarrier((&___left_15), value);
	}

	inline static int32_t get_offset_of_top_16() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___top_16)); }
	inline FsmFloat_t2883254149 * get_top_16() const { return ___top_16; }
	inline FsmFloat_t2883254149 ** get_address_of_top_16() { return &___top_16; }
	inline void set_top_16(FsmFloat_t2883254149 * value)
	{
		___top_16 = value;
		Il2CppCodeGenWriteBarrier((&___top_16), value);
	}

	inline static int32_t get_offset_of_width_17() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___width_17)); }
	inline FsmFloat_t2883254149 * get_width_17() const { return ___width_17; }
	inline FsmFloat_t2883254149 ** get_address_of_width_17() { return &___width_17; }
	inline void set_width_17(FsmFloat_t2883254149 * value)
	{
		___width_17 = value;
		Il2CppCodeGenWriteBarrier((&___width_17), value);
	}

	inline static int32_t get_offset_of_height_18() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___height_18)); }
	inline FsmFloat_t2883254149 * get_height_18() const { return ___height_18; }
	inline FsmFloat_t2883254149 ** get_address_of_height_18() { return &___height_18; }
	inline void set_height_18(FsmFloat_t2883254149 * value)
	{
		___height_18 = value;
		Il2CppCodeGenWriteBarrier((&___height_18), value);
	}

	inline static int32_t get_offset_of_normalized_19() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___normalized_19)); }
	inline FsmBool_t163807967 * get_normalized_19() const { return ___normalized_19; }
	inline FsmBool_t163807967 ** get_address_of_normalized_19() { return &___normalized_19; }
	inline void set_normalized_19(FsmBool_t163807967 * value)
	{
		___normalized_19 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_19), value);
	}

	inline static int32_t get_offset_of_rect_20() { return static_cast<int32_t>(offsetof(GUIAction_t3750965478, ___rect_20)); }
	inline Rect_t2360479859  get_rect_20() const { return ___rect_20; }
	inline Rect_t2360479859 * get_address_of_rect_20() { return &___rect_20; }
	inline void set_rect_20(Rect_t2360479859  value)
	{
		___rect_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIACTION_T3750965478_H
#ifndef GUIELEMENTHITTEST_T2786187436_H
#define GUIELEMENTHITTEST_T2786187436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIElementHitTest
struct  GUIElementHitTest_t2786187436  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GUIElementHitTest::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.GUIElementHitTest::camera
	Camera_t4157153871 * ___camera_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GUIElementHitTest::screenPoint
	FsmVector3_t626444517 * ___screenPoint_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIElementHitTest::screenX
	FsmFloat_t2883254149 * ___screenX_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIElementHitTest::screenY
	FsmFloat_t2883254149 * ___screenY_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIElementHitTest::normalized
	FsmBool_t163807967 * ___normalized_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUIElementHitTest::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIElementHitTest::storeResult
	FsmBool_t163807967 * ___storeResult_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIElementHitTest::everyFrame
	FsmBool_t163807967 * ___everyFrame_22;
	// UnityEngine.GUIElement HutongGames.PlayMaker.Actions.GUIElementHitTest::guiElement
	GUIElement_t3567083079 * ___guiElement_23;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GUIElementHitTest::gameObjectCached
	GameObject_t1113636619 * ___gameObjectCached_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_camera_15() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___camera_15)); }
	inline Camera_t4157153871 * get_camera_15() const { return ___camera_15; }
	inline Camera_t4157153871 ** get_address_of_camera_15() { return &___camera_15; }
	inline void set_camera_15(Camera_t4157153871 * value)
	{
		___camera_15 = value;
		Il2CppCodeGenWriteBarrier((&___camera_15), value);
	}

	inline static int32_t get_offset_of_screenPoint_16() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___screenPoint_16)); }
	inline FsmVector3_t626444517 * get_screenPoint_16() const { return ___screenPoint_16; }
	inline FsmVector3_t626444517 ** get_address_of_screenPoint_16() { return &___screenPoint_16; }
	inline void set_screenPoint_16(FsmVector3_t626444517 * value)
	{
		___screenPoint_16 = value;
		Il2CppCodeGenWriteBarrier((&___screenPoint_16), value);
	}

	inline static int32_t get_offset_of_screenX_17() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___screenX_17)); }
	inline FsmFloat_t2883254149 * get_screenX_17() const { return ___screenX_17; }
	inline FsmFloat_t2883254149 ** get_address_of_screenX_17() { return &___screenX_17; }
	inline void set_screenX_17(FsmFloat_t2883254149 * value)
	{
		___screenX_17 = value;
		Il2CppCodeGenWriteBarrier((&___screenX_17), value);
	}

	inline static int32_t get_offset_of_screenY_18() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___screenY_18)); }
	inline FsmFloat_t2883254149 * get_screenY_18() const { return ___screenY_18; }
	inline FsmFloat_t2883254149 ** get_address_of_screenY_18() { return &___screenY_18; }
	inline void set_screenY_18(FsmFloat_t2883254149 * value)
	{
		___screenY_18 = value;
		Il2CppCodeGenWriteBarrier((&___screenY_18), value);
	}

	inline static int32_t get_offset_of_normalized_19() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___normalized_19)); }
	inline FsmBool_t163807967 * get_normalized_19() const { return ___normalized_19; }
	inline FsmBool_t163807967 ** get_address_of_normalized_19() { return &___normalized_19; }
	inline void set_normalized_19(FsmBool_t163807967 * value)
	{
		___normalized_19 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_19), value);
	}

	inline static int32_t get_offset_of_hitEvent_20() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___hitEvent_20)); }
	inline FsmEvent_t3736299882 * get_hitEvent_20() const { return ___hitEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_20() { return &___hitEvent_20; }
	inline void set_hitEvent_20(FsmEvent_t3736299882 * value)
	{
		___hitEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_20), value);
	}

	inline static int32_t get_offset_of_storeResult_21() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___storeResult_21)); }
	inline FsmBool_t163807967 * get_storeResult_21() const { return ___storeResult_21; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_21() { return &___storeResult_21; }
	inline void set_storeResult_21(FsmBool_t163807967 * value)
	{
		___storeResult_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___everyFrame_22)); }
	inline FsmBool_t163807967 * get_everyFrame_22() const { return ___everyFrame_22; }
	inline FsmBool_t163807967 ** get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(FsmBool_t163807967 * value)
	{
		___everyFrame_22 = value;
		Il2CppCodeGenWriteBarrier((&___everyFrame_22), value);
	}

	inline static int32_t get_offset_of_guiElement_23() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___guiElement_23)); }
	inline GUIElement_t3567083079 * get_guiElement_23() const { return ___guiElement_23; }
	inline GUIElement_t3567083079 ** get_address_of_guiElement_23() { return &___guiElement_23; }
	inline void set_guiElement_23(GUIElement_t3567083079 * value)
	{
		___guiElement_23 = value;
		Il2CppCodeGenWriteBarrier((&___guiElement_23), value);
	}

	inline static int32_t get_offset_of_gameObjectCached_24() { return static_cast<int32_t>(offsetof(GUIElementHitTest_t2786187436, ___gameObjectCached_24)); }
	inline GameObject_t1113636619 * get_gameObjectCached_24() const { return ___gameObjectCached_24; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectCached_24() { return &___gameObjectCached_24; }
	inline void set_gameObjectCached_24(GameObject_t1113636619 * value)
	{
		___gameObjectCached_24 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectCached_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENTHITTEST_T2786187436_H
#ifndef GUILAYOUTACTION_T1272787235_H
#define GUILAYOUTACTION_T1272787235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutAction
struct  GUILayoutAction_t1272787235  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.LayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::layoutOptions
	LayoutOptionU5BU5D_t80899288* ___layoutOptions_14;
	// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::options
	GUILayoutOptionU5BU5D_t2510215842* ___options_15;

public:
	inline static int32_t get_offset_of_layoutOptions_14() { return static_cast<int32_t>(offsetof(GUILayoutAction_t1272787235, ___layoutOptions_14)); }
	inline LayoutOptionU5BU5D_t80899288* get_layoutOptions_14() const { return ___layoutOptions_14; }
	inline LayoutOptionU5BU5D_t80899288** get_address_of_layoutOptions_14() { return &___layoutOptions_14; }
	inline void set_layoutOptions_14(LayoutOptionU5BU5D_t80899288* value)
	{
		___layoutOptions_14 = value;
		Il2CppCodeGenWriteBarrier((&___layoutOptions_14), value);
	}

	inline static int32_t get_offset_of_options_15() { return static_cast<int32_t>(offsetof(GUILayoutAction_t1272787235, ___options_15)); }
	inline GUILayoutOptionU5BU5D_t2510215842* get_options_15() const { return ___options_15; }
	inline GUILayoutOptionU5BU5D_t2510215842** get_address_of_options_15() { return &___options_15; }
	inline void set_options_15(GUILayoutOptionU5BU5D_t2510215842* value)
	{
		___options_15 = value;
		Il2CppCodeGenWriteBarrier((&___options_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTACTION_T1272787235_H
#ifndef GUILAYOUTBEGINAREA_T1161236718_H
#define GUILAYOUTBEGINAREA_T1161236718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginArea
struct  GUILayoutBeginArea_t1161236718  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GUILayoutBeginArea::screenRect
	FsmRect_t3649179877 * ___screenRect_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::left
	FsmFloat_t2883254149 * ___left_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::top
	FsmFloat_t2883254149 * ___top_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::width
	FsmFloat_t2883254149 * ___width_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginArea::height
	FsmFloat_t2883254149 * ___height_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginArea::normalized
	FsmBool_t163807967 * ___normalized_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginArea::style
	FsmString_t1785915204 * ___style_20;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.GUILayoutBeginArea::rect
	Rect_t2360479859  ___rect_21;

public:
	inline static int32_t get_offset_of_screenRect_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___screenRect_14)); }
	inline FsmRect_t3649179877 * get_screenRect_14() const { return ___screenRect_14; }
	inline FsmRect_t3649179877 ** get_address_of_screenRect_14() { return &___screenRect_14; }
	inline void set_screenRect_14(FsmRect_t3649179877 * value)
	{
		___screenRect_14 = value;
		Il2CppCodeGenWriteBarrier((&___screenRect_14), value);
	}

	inline static int32_t get_offset_of_left_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___left_15)); }
	inline FsmFloat_t2883254149 * get_left_15() const { return ___left_15; }
	inline FsmFloat_t2883254149 ** get_address_of_left_15() { return &___left_15; }
	inline void set_left_15(FsmFloat_t2883254149 * value)
	{
		___left_15 = value;
		Il2CppCodeGenWriteBarrier((&___left_15), value);
	}

	inline static int32_t get_offset_of_top_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___top_16)); }
	inline FsmFloat_t2883254149 * get_top_16() const { return ___top_16; }
	inline FsmFloat_t2883254149 ** get_address_of_top_16() { return &___top_16; }
	inline void set_top_16(FsmFloat_t2883254149 * value)
	{
		___top_16 = value;
		Il2CppCodeGenWriteBarrier((&___top_16), value);
	}

	inline static int32_t get_offset_of_width_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___width_17)); }
	inline FsmFloat_t2883254149 * get_width_17() const { return ___width_17; }
	inline FsmFloat_t2883254149 ** get_address_of_width_17() { return &___width_17; }
	inline void set_width_17(FsmFloat_t2883254149 * value)
	{
		___width_17 = value;
		Il2CppCodeGenWriteBarrier((&___width_17), value);
	}

	inline static int32_t get_offset_of_height_18() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___height_18)); }
	inline FsmFloat_t2883254149 * get_height_18() const { return ___height_18; }
	inline FsmFloat_t2883254149 ** get_address_of_height_18() { return &___height_18; }
	inline void set_height_18(FsmFloat_t2883254149 * value)
	{
		___height_18 = value;
		Il2CppCodeGenWriteBarrier((&___height_18), value);
	}

	inline static int32_t get_offset_of_normalized_19() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___normalized_19)); }
	inline FsmBool_t163807967 * get_normalized_19() const { return ___normalized_19; }
	inline FsmBool_t163807967 ** get_address_of_normalized_19() { return &___normalized_19; }
	inline void set_normalized_19(FsmBool_t163807967 * value)
	{
		___normalized_19 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_19), value);
	}

	inline static int32_t get_offset_of_style_20() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___style_20)); }
	inline FsmString_t1785915204 * get_style_20() const { return ___style_20; }
	inline FsmString_t1785915204 ** get_address_of_style_20() { return &___style_20; }
	inline void set_style_20(FsmString_t1785915204 * value)
	{
		___style_20 = value;
		Il2CppCodeGenWriteBarrier((&___style_20), value);
	}

	inline static int32_t get_offset_of_rect_21() { return static_cast<int32_t>(offsetof(GUILayoutBeginArea_t1161236718, ___rect_21)); }
	inline Rect_t2360479859  get_rect_21() const { return ___rect_21; }
	inline Rect_t2360479859 * get_address_of_rect_21() { return &___rect_21; }
	inline void set_rect_21(Rect_t2360479859  value)
	{
		___rect_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBEGINAREA_T1161236718_H
#ifndef GUILAYOUTBEGINAREAFOLLOWOBJECT_T2928844525_H
#define GUILAYOUTBEGINAREAFOLLOWOBJECT_T2928844525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject
struct  GUILayoutBeginAreaFollowObject_t2928844525  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::offsetLeft
	FsmFloat_t2883254149 * ___offsetLeft_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::offsetTop
	FsmFloat_t2883254149 * ___offsetTop_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::width
	FsmFloat_t2883254149 * ___width_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::height
	FsmFloat_t2883254149 * ___height_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::normalized
	FsmBool_t163807967 * ___normalized_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::style
	FsmString_t1785915204 * ___style_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_offsetLeft_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___offsetLeft_15)); }
	inline FsmFloat_t2883254149 * get_offsetLeft_15() const { return ___offsetLeft_15; }
	inline FsmFloat_t2883254149 ** get_address_of_offsetLeft_15() { return &___offsetLeft_15; }
	inline void set_offsetLeft_15(FsmFloat_t2883254149 * value)
	{
		___offsetLeft_15 = value;
		Il2CppCodeGenWriteBarrier((&___offsetLeft_15), value);
	}

	inline static int32_t get_offset_of_offsetTop_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___offsetTop_16)); }
	inline FsmFloat_t2883254149 * get_offsetTop_16() const { return ___offsetTop_16; }
	inline FsmFloat_t2883254149 ** get_address_of_offsetTop_16() { return &___offsetTop_16; }
	inline void set_offsetTop_16(FsmFloat_t2883254149 * value)
	{
		___offsetTop_16 = value;
		Il2CppCodeGenWriteBarrier((&___offsetTop_16), value);
	}

	inline static int32_t get_offset_of_width_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___width_17)); }
	inline FsmFloat_t2883254149 * get_width_17() const { return ___width_17; }
	inline FsmFloat_t2883254149 ** get_address_of_width_17() { return &___width_17; }
	inline void set_width_17(FsmFloat_t2883254149 * value)
	{
		___width_17 = value;
		Il2CppCodeGenWriteBarrier((&___width_17), value);
	}

	inline static int32_t get_offset_of_height_18() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___height_18)); }
	inline FsmFloat_t2883254149 * get_height_18() const { return ___height_18; }
	inline FsmFloat_t2883254149 ** get_address_of_height_18() { return &___height_18; }
	inline void set_height_18(FsmFloat_t2883254149 * value)
	{
		___height_18 = value;
		Il2CppCodeGenWriteBarrier((&___height_18), value);
	}

	inline static int32_t get_offset_of_normalized_19() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___normalized_19)); }
	inline FsmBool_t163807967 * get_normalized_19() const { return ___normalized_19; }
	inline FsmBool_t163807967 ** get_address_of_normalized_19() { return &___normalized_19; }
	inline void set_normalized_19(FsmBool_t163807967 * value)
	{
		___normalized_19 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_19), value);
	}

	inline static int32_t get_offset_of_style_20() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2928844525, ___style_20)); }
	inline FsmString_t1785915204 * get_style_20() const { return ___style_20; }
	inline FsmString_t1785915204 ** get_address_of_style_20() { return &___style_20; }
	inline void set_style_20(FsmString_t1785915204 * value)
	{
		___style_20 = value;
		Il2CppCodeGenWriteBarrier((&___style_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBEGINAREAFOLLOWOBJECT_T2928844525_H
#ifndef GUILAYOUTBEGINCENTERED_T2525719869_H
#define GUILAYOUTBEGINCENTERED_T2525719869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginCentered
struct  GUILayoutBeginCentered_t2525719869  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBEGINCENTERED_T2525719869_H
#ifndef GUILAYOUTENDAREA_T1298492153_H
#define GUILAYOUTENDAREA_T1298492153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEndArea
struct  GUILayoutEndArea_t1298492153  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTENDAREA_T1298492153_H
#ifndef GUILAYOUTENDCENTERED_T1066408831_H
#define GUILAYOUTENDCENTERED_T1066408831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEndCentered
struct  GUILayoutEndCentered_t1066408831  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTENDCENTERED_T1066408831_H
#ifndef GUILAYOUTENDHORIZONTAL_T880464906_H
#define GUILAYOUTENDHORIZONTAL_T880464906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEndHorizontal
struct  GUILayoutEndHorizontal_t880464906  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTENDHORIZONTAL_T880464906_H
#ifndef GUITOOLTIP_T2214156286_H
#define GUITOOLTIP_T2214156286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUITooltip
struct  GUITooltip_t2214156286  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUITooltip::storeTooltip
	FsmString_t1785915204 * ___storeTooltip_14;

public:
	inline static int32_t get_offset_of_storeTooltip_14() { return static_cast<int32_t>(offsetof(GUITooltip_t2214156286, ___storeTooltip_14)); }
	inline FsmString_t1785915204 * get_storeTooltip_14() const { return ___storeTooltip_14; }
	inline FsmString_t1785915204 ** get_address_of_storeTooltip_14() { return &___storeTooltip_14; }
	inline void set_storeTooltip_14(FsmString_t1785915204 * value)
	{
		___storeTooltip_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeTooltip_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITOOLTIP_T2214156286_H
#ifndef GETCHILD_T2112359604_H
#define GETCHILD_T2112359604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetChild
struct  GetChild_t2112359604  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetChild::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetChild::childName
	FsmString_t1785915204 * ___childName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetChild::withTag
	FsmString_t1785915204 * ___withTag_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetChild::storeResult
	FsmGameObject_t3581898942 * ___storeResult_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetChild_t2112359604, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_childName_15() { return static_cast<int32_t>(offsetof(GetChild_t2112359604, ___childName_15)); }
	inline FsmString_t1785915204 * get_childName_15() const { return ___childName_15; }
	inline FsmString_t1785915204 ** get_address_of_childName_15() { return &___childName_15; }
	inline void set_childName_15(FsmString_t1785915204 * value)
	{
		___childName_15 = value;
		Il2CppCodeGenWriteBarrier((&___childName_15), value);
	}

	inline static int32_t get_offset_of_withTag_16() { return static_cast<int32_t>(offsetof(GetChild_t2112359604, ___withTag_16)); }
	inline FsmString_t1785915204 * get_withTag_16() const { return ___withTag_16; }
	inline FsmString_t1785915204 ** get_address_of_withTag_16() { return &___withTag_16; }
	inline void set_withTag_16(FsmString_t1785915204 * value)
	{
		___withTag_16 = value;
		Il2CppCodeGenWriteBarrier((&___withTag_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetChild_t2112359604, ___storeResult_17)); }
	inline FsmGameObject_t3581898942 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmGameObject_t3581898942 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCHILD_T2112359604_H
#ifndef GETCHILDCOUNT_T3095803499_H
#define GETCHILDCOUNT_T3095803499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetChildCount
struct  GetChildCount_t3095803499  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetChildCount::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetChildCount::storeResult
	FsmInt_t874273141 * ___storeResult_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetChildCount_t3095803499, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetChildCount_t3095803499, ___storeResult_15)); }
	inline FsmInt_t874273141 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmInt_t874273141 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmInt_t874273141 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCHILDCOUNT_T3095803499_H
#ifndef GETCHILDNUM_T3674298174_H
#define GETCHILDNUM_T3674298174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetChildNum
struct  GetChildNum_t3674298174  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetChildNum::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetChildNum::childIndex
	FsmInt_t874273141 * ___childIndex_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetChildNum::store
	FsmGameObject_t3581898942 * ___store_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetChildNum_t3674298174, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_childIndex_15() { return static_cast<int32_t>(offsetof(GetChildNum_t3674298174, ___childIndex_15)); }
	inline FsmInt_t874273141 * get_childIndex_15() const { return ___childIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_childIndex_15() { return &___childIndex_15; }
	inline void set_childIndex_15(FsmInt_t874273141 * value)
	{
		___childIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___childIndex_15), value);
	}

	inline static int32_t get_offset_of_store_16() { return static_cast<int32_t>(offsetof(GetChildNum_t3674298174, ___store_16)); }
	inline FsmGameObject_t3581898942 * get_store_16() const { return ___store_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_store_16() { return &___store_16; }
	inline void set_store_16(FsmGameObject_t3581898942 * value)
	{
		___store_16 = value;
		Il2CppCodeGenWriteBarrier((&___store_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCHILDNUM_T3674298174_H
#ifndef GETDEVICEACCELERATION_T4085971106_H
#define GETDEVICEACCELERATION_T4085971106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetDeviceAcceleration
struct  GetDeviceAcceleration_t4085971106  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeVector
	FsmVector3_t626444517 * ___storeVector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeX
	FsmFloat_t2883254149 * ___storeX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeY
	FsmFloat_t2883254149 * ___storeY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::storeZ
	FsmFloat_t2883254149 * ___storeZ_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceAcceleration::multiplier
	FsmFloat_t2883254149 * ___multiplier_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetDeviceAcceleration::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_storeVector_14() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t4085971106, ___storeVector_14)); }
	inline FsmVector3_t626444517 * get_storeVector_14() const { return ___storeVector_14; }
	inline FsmVector3_t626444517 ** get_address_of_storeVector_14() { return &___storeVector_14; }
	inline void set_storeVector_14(FsmVector3_t626444517 * value)
	{
		___storeVector_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector_14), value);
	}

	inline static int32_t get_offset_of_storeX_15() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t4085971106, ___storeX_15)); }
	inline FsmFloat_t2883254149 * get_storeX_15() const { return ___storeX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeX_15() { return &___storeX_15; }
	inline void set_storeX_15(FsmFloat_t2883254149 * value)
	{
		___storeX_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeX_15), value);
	}

	inline static int32_t get_offset_of_storeY_16() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t4085971106, ___storeY_16)); }
	inline FsmFloat_t2883254149 * get_storeY_16() const { return ___storeY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeY_16() { return &___storeY_16; }
	inline void set_storeY_16(FsmFloat_t2883254149 * value)
	{
		___storeY_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeY_16), value);
	}

	inline static int32_t get_offset_of_storeZ_17() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t4085971106, ___storeZ_17)); }
	inline FsmFloat_t2883254149 * get_storeZ_17() const { return ___storeZ_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeZ_17() { return &___storeZ_17; }
	inline void set_storeZ_17(FsmFloat_t2883254149 * value)
	{
		___storeZ_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeZ_17), value);
	}

	inline static int32_t get_offset_of_multiplier_18() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t4085971106, ___multiplier_18)); }
	inline FsmFloat_t2883254149 * get_multiplier_18() const { return ___multiplier_18; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplier_18() { return &___multiplier_18; }
	inline void set_multiplier_18(FsmFloat_t2883254149 * value)
	{
		___multiplier_18 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GetDeviceAcceleration_t4085971106, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDEVICEACCELERATION_T4085971106_H
#ifndef GETDEVICEROLL_T308638562_H
#define GETDEVICEROLL_T308638562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetDeviceRoll
struct  GetDeviceRoll_t308638562  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.GetDeviceRoll/BaseOrientation HutongGames.PlayMaker.Actions.GetDeviceRoll::baseOrientation
	int32_t ___baseOrientation_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceRoll::storeAngle
	FsmFloat_t2883254149 * ___storeAngle_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceRoll::limitAngle
	FsmFloat_t2883254149 * ___limitAngle_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDeviceRoll::smoothing
	FsmFloat_t2883254149 * ___smoothing_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetDeviceRoll::everyFrame
	bool ___everyFrame_18;
	// System.Single HutongGames.PlayMaker.Actions.GetDeviceRoll::lastZAngle
	float ___lastZAngle_19;

public:
	inline static int32_t get_offset_of_baseOrientation_14() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t308638562, ___baseOrientation_14)); }
	inline int32_t get_baseOrientation_14() const { return ___baseOrientation_14; }
	inline int32_t* get_address_of_baseOrientation_14() { return &___baseOrientation_14; }
	inline void set_baseOrientation_14(int32_t value)
	{
		___baseOrientation_14 = value;
	}

	inline static int32_t get_offset_of_storeAngle_15() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t308638562, ___storeAngle_15)); }
	inline FsmFloat_t2883254149 * get_storeAngle_15() const { return ___storeAngle_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeAngle_15() { return &___storeAngle_15; }
	inline void set_storeAngle_15(FsmFloat_t2883254149 * value)
	{
		___storeAngle_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeAngle_15), value);
	}

	inline static int32_t get_offset_of_limitAngle_16() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t308638562, ___limitAngle_16)); }
	inline FsmFloat_t2883254149 * get_limitAngle_16() const { return ___limitAngle_16; }
	inline FsmFloat_t2883254149 ** get_address_of_limitAngle_16() { return &___limitAngle_16; }
	inline void set_limitAngle_16(FsmFloat_t2883254149 * value)
	{
		___limitAngle_16 = value;
		Il2CppCodeGenWriteBarrier((&___limitAngle_16), value);
	}

	inline static int32_t get_offset_of_smoothing_17() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t308638562, ___smoothing_17)); }
	inline FsmFloat_t2883254149 * get_smoothing_17() const { return ___smoothing_17; }
	inline FsmFloat_t2883254149 ** get_address_of_smoothing_17() { return &___smoothing_17; }
	inline void set_smoothing_17(FsmFloat_t2883254149 * value)
	{
		___smoothing_17 = value;
		Il2CppCodeGenWriteBarrier((&___smoothing_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t308638562, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_lastZAngle_19() { return static_cast<int32_t>(offsetof(GetDeviceRoll_t308638562, ___lastZAngle_19)); }
	inline float get_lastZAngle_19() const { return ___lastZAngle_19; }
	inline float* get_address_of_lastZAngle_19() { return &___lastZAngle_19; }
	inline void set_lastZAngle_19(float value)
	{
		___lastZAngle_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDEVICEROLL_T308638562_H
#ifndef GETDISTANCE_T3017425359_H
#define GETDISTANCE_T3017425359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetDistance
struct  GetDistance_t3017425359  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetDistance::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetDistance::target
	FsmGameObject_t3581898942 * ___target_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetDistance::storeResult
	FsmFloat_t2883254149 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetDistance::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetDistance_t3017425359, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_target_15() { return static_cast<int32_t>(offsetof(GetDistance_t3017425359, ___target_15)); }
	inline FsmGameObject_t3581898942 * get_target_15() const { return ___target_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_target_15() { return &___target_15; }
	inline void set_target_15(FsmGameObject_t3581898942 * value)
	{
		___target_15 = value;
		Il2CppCodeGenWriteBarrier((&___target_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetDistance_t3017425359, ___storeResult_16)); }
	inline FsmFloat_t2883254149 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmFloat_t2883254149 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetDistance_t3017425359, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDISTANCE_T3017425359_H
#ifndef GETIPHONESETTINGS_T35316684_H
#define GETIPHONESETTINGS_T35316684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetIPhoneSettings
struct  GetIPhoneSettings_t35316684  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetIPhoneSettings::getScreenCanDarken
	FsmBool_t163807967 * ___getScreenCanDarken_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetIPhoneSettings::getUniqueIdentifier
	FsmString_t1785915204 * ___getUniqueIdentifier_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetIPhoneSettings::getName
	FsmString_t1785915204 * ___getName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetIPhoneSettings::getModel
	FsmString_t1785915204 * ___getModel_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetIPhoneSettings::getSystemName
	FsmString_t1785915204 * ___getSystemName_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetIPhoneSettings::getGeneration
	FsmString_t1785915204 * ___getGeneration_19;

public:
	inline static int32_t get_offset_of_getScreenCanDarken_14() { return static_cast<int32_t>(offsetof(GetIPhoneSettings_t35316684, ___getScreenCanDarken_14)); }
	inline FsmBool_t163807967 * get_getScreenCanDarken_14() const { return ___getScreenCanDarken_14; }
	inline FsmBool_t163807967 ** get_address_of_getScreenCanDarken_14() { return &___getScreenCanDarken_14; }
	inline void set_getScreenCanDarken_14(FsmBool_t163807967 * value)
	{
		___getScreenCanDarken_14 = value;
		Il2CppCodeGenWriteBarrier((&___getScreenCanDarken_14), value);
	}

	inline static int32_t get_offset_of_getUniqueIdentifier_15() { return static_cast<int32_t>(offsetof(GetIPhoneSettings_t35316684, ___getUniqueIdentifier_15)); }
	inline FsmString_t1785915204 * get_getUniqueIdentifier_15() const { return ___getUniqueIdentifier_15; }
	inline FsmString_t1785915204 ** get_address_of_getUniqueIdentifier_15() { return &___getUniqueIdentifier_15; }
	inline void set_getUniqueIdentifier_15(FsmString_t1785915204 * value)
	{
		___getUniqueIdentifier_15 = value;
		Il2CppCodeGenWriteBarrier((&___getUniqueIdentifier_15), value);
	}

	inline static int32_t get_offset_of_getName_16() { return static_cast<int32_t>(offsetof(GetIPhoneSettings_t35316684, ___getName_16)); }
	inline FsmString_t1785915204 * get_getName_16() const { return ___getName_16; }
	inline FsmString_t1785915204 ** get_address_of_getName_16() { return &___getName_16; }
	inline void set_getName_16(FsmString_t1785915204 * value)
	{
		___getName_16 = value;
		Il2CppCodeGenWriteBarrier((&___getName_16), value);
	}

	inline static int32_t get_offset_of_getModel_17() { return static_cast<int32_t>(offsetof(GetIPhoneSettings_t35316684, ___getModel_17)); }
	inline FsmString_t1785915204 * get_getModel_17() const { return ___getModel_17; }
	inline FsmString_t1785915204 ** get_address_of_getModel_17() { return &___getModel_17; }
	inline void set_getModel_17(FsmString_t1785915204 * value)
	{
		___getModel_17 = value;
		Il2CppCodeGenWriteBarrier((&___getModel_17), value);
	}

	inline static int32_t get_offset_of_getSystemName_18() { return static_cast<int32_t>(offsetof(GetIPhoneSettings_t35316684, ___getSystemName_18)); }
	inline FsmString_t1785915204 * get_getSystemName_18() const { return ___getSystemName_18; }
	inline FsmString_t1785915204 ** get_address_of_getSystemName_18() { return &___getSystemName_18; }
	inline void set_getSystemName_18(FsmString_t1785915204 * value)
	{
		___getSystemName_18 = value;
		Il2CppCodeGenWriteBarrier((&___getSystemName_18), value);
	}

	inline static int32_t get_offset_of_getGeneration_19() { return static_cast<int32_t>(offsetof(GetIPhoneSettings_t35316684, ___getGeneration_19)); }
	inline FsmString_t1785915204 * get_getGeneration_19() const { return ___getGeneration_19; }
	inline FsmString_t1785915204 ** get_address_of_getGeneration_19() { return &___getGeneration_19; }
	inline void set_getGeneration_19(FsmString_t1785915204 * value)
	{
		___getGeneration_19 = value;
		Il2CppCodeGenWriteBarrier((&___getGeneration_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETIPHONESETTINGS_T35316684_H
#ifndef GETLAYER_T2829532394_H
#define GETLAYER_T2829532394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetLayer
struct  GetLayer_t2829532394  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLayer::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetLayer::storeResult
	FsmInt_t874273141 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetLayer::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetLayer_t2829532394, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetLayer_t2829532394, ___storeResult_15)); }
	inline FsmInt_t874273141 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmInt_t874273141 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmInt_t874273141 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetLayer_t2829532394, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLAYER_T2829532394_H
#ifndef GETLOCATIONINFO_T933323761_H
#define GETLOCATIONINFO_T933323761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetLocationInfo
struct  GetLocationInfo_t933323761  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetLocationInfo::vectorPosition
	FsmVector3_t626444517 * ___vectorPosition_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetLocationInfo::longitude
	FsmFloat_t2883254149 * ___longitude_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetLocationInfo::latitude
	FsmFloat_t2883254149 * ___latitude_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetLocationInfo::altitude
	FsmFloat_t2883254149 * ___altitude_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetLocationInfo::horizontalAccuracy
	FsmFloat_t2883254149 * ___horizontalAccuracy_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetLocationInfo::verticalAccuracy
	FsmFloat_t2883254149 * ___verticalAccuracy_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetLocationInfo::errorEvent
	FsmEvent_t3736299882 * ___errorEvent_20;

public:
	inline static int32_t get_offset_of_vectorPosition_14() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___vectorPosition_14)); }
	inline FsmVector3_t626444517 * get_vectorPosition_14() const { return ___vectorPosition_14; }
	inline FsmVector3_t626444517 ** get_address_of_vectorPosition_14() { return &___vectorPosition_14; }
	inline void set_vectorPosition_14(FsmVector3_t626444517 * value)
	{
		___vectorPosition_14 = value;
		Il2CppCodeGenWriteBarrier((&___vectorPosition_14), value);
	}

	inline static int32_t get_offset_of_longitude_15() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___longitude_15)); }
	inline FsmFloat_t2883254149 * get_longitude_15() const { return ___longitude_15; }
	inline FsmFloat_t2883254149 ** get_address_of_longitude_15() { return &___longitude_15; }
	inline void set_longitude_15(FsmFloat_t2883254149 * value)
	{
		___longitude_15 = value;
		Il2CppCodeGenWriteBarrier((&___longitude_15), value);
	}

	inline static int32_t get_offset_of_latitude_16() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___latitude_16)); }
	inline FsmFloat_t2883254149 * get_latitude_16() const { return ___latitude_16; }
	inline FsmFloat_t2883254149 ** get_address_of_latitude_16() { return &___latitude_16; }
	inline void set_latitude_16(FsmFloat_t2883254149 * value)
	{
		___latitude_16 = value;
		Il2CppCodeGenWriteBarrier((&___latitude_16), value);
	}

	inline static int32_t get_offset_of_altitude_17() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___altitude_17)); }
	inline FsmFloat_t2883254149 * get_altitude_17() const { return ___altitude_17; }
	inline FsmFloat_t2883254149 ** get_address_of_altitude_17() { return &___altitude_17; }
	inline void set_altitude_17(FsmFloat_t2883254149 * value)
	{
		___altitude_17 = value;
		Il2CppCodeGenWriteBarrier((&___altitude_17), value);
	}

	inline static int32_t get_offset_of_horizontalAccuracy_18() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___horizontalAccuracy_18)); }
	inline FsmFloat_t2883254149 * get_horizontalAccuracy_18() const { return ___horizontalAccuracy_18; }
	inline FsmFloat_t2883254149 ** get_address_of_horizontalAccuracy_18() { return &___horizontalAccuracy_18; }
	inline void set_horizontalAccuracy_18(FsmFloat_t2883254149 * value)
	{
		___horizontalAccuracy_18 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAccuracy_18), value);
	}

	inline static int32_t get_offset_of_verticalAccuracy_19() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___verticalAccuracy_19)); }
	inline FsmFloat_t2883254149 * get_verticalAccuracy_19() const { return ___verticalAccuracy_19; }
	inline FsmFloat_t2883254149 ** get_address_of_verticalAccuracy_19() { return &___verticalAccuracy_19; }
	inline void set_verticalAccuracy_19(FsmFloat_t2883254149 * value)
	{
		___verticalAccuracy_19 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAccuracy_19), value);
	}

	inline static int32_t get_offset_of_errorEvent_20() { return static_cast<int32_t>(offsetof(GetLocationInfo_t933323761, ___errorEvent_20)); }
	inline FsmEvent_t3736299882 * get_errorEvent_20() const { return ___errorEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_errorEvent_20() { return &___errorEvent_20; }
	inline void set_errorEvent_20(FsmEvent_t3736299882 * value)
	{
		___errorEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___errorEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLOCATIONINFO_T933323761_H
#ifndef GETNAME_T534723818_H
#define GETNAME_T534723818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetName
struct  GetName_t534723818  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetName::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetName::storeName
	FsmString_t1785915204 * ___storeName_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetName::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetName_t534723818, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeName_15() { return static_cast<int32_t>(offsetof(GetName_t534723818, ___storeName_15)); }
	inline FsmString_t1785915204 * get_storeName_15() const { return ___storeName_15; }
	inline FsmString_t1785915204 ** get_address_of_storeName_15() { return &___storeName_15; }
	inline void set_storeName_15(FsmString_t1785915204 * value)
	{
		___storeName_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeName_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetName_t534723818, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNAME_T534723818_H
#ifndef GETNEXTCHILD_T3737268120_H
#define GETNEXTCHILD_T3737268120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextChild
struct  GetNextChild_t3737268120  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextChild::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextChild::storeNextChild
	FsmGameObject_t3581898942 * ___storeNextChild_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextChild::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextChild::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextChild::resetFlag
	FsmBool_t163807967 * ___resetFlag_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetNextChild::go
	GameObject_t1113636619 * ___go_19;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextChild::nextChildIndex
	int32_t ___nextChildIndex_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeNextChild_15() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___storeNextChild_15)); }
	inline FsmGameObject_t3581898942 * get_storeNextChild_15() const { return ___storeNextChild_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeNextChild_15() { return &___storeNextChild_15; }
	inline void set_storeNextChild_15(FsmGameObject_t3581898942 * value)
	{
		___storeNextChild_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextChild_15), value);
	}

	inline static int32_t get_offset_of_loopEvent_16() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___loopEvent_16)); }
	inline FsmEvent_t3736299882 * get_loopEvent_16() const { return ___loopEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_16() { return &___loopEvent_16; }
	inline void set_loopEvent_16(FsmEvent_t3736299882 * value)
	{
		___loopEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_16), value);
	}

	inline static int32_t get_offset_of_finishedEvent_17() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___finishedEvent_17)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_17() const { return ___finishedEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_17() { return &___finishedEvent_17; }
	inline void set_finishedEvent_17(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_17), value);
	}

	inline static int32_t get_offset_of_resetFlag_18() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___resetFlag_18)); }
	inline FsmBool_t163807967 * get_resetFlag_18() const { return ___resetFlag_18; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_18() { return &___resetFlag_18; }
	inline void set_resetFlag_18(FsmBool_t163807967 * value)
	{
		___resetFlag_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_18), value);
	}

	inline static int32_t get_offset_of_go_19() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___go_19)); }
	inline GameObject_t1113636619 * get_go_19() const { return ___go_19; }
	inline GameObject_t1113636619 ** get_address_of_go_19() { return &___go_19; }
	inline void set_go_19(GameObject_t1113636619 * value)
	{
		___go_19 = value;
		Il2CppCodeGenWriteBarrier((&___go_19), value);
	}

	inline static int32_t get_offset_of_nextChildIndex_20() { return static_cast<int32_t>(offsetof(GetNextChild_t3737268120, ___nextChildIndex_20)); }
	inline int32_t get_nextChildIndex_20() const { return ___nextChildIndex_20; }
	inline int32_t* get_address_of_nextChildIndex_20() { return &___nextChildIndex_20; }
	inline void set_nextChildIndex_20(int32_t value)
	{
		___nextChildIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEXTCHILD_T3737268120_H
#ifndef GETOWNER_T3978915792_H
#define GETOWNER_T3978915792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetOwner
struct  GetOwner_t3978915792  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetOwner::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_14;

public:
	inline static int32_t get_offset_of_storeGameObject_14() { return static_cast<int32_t>(offsetof(GetOwner_t3978915792, ___storeGameObject_14)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_14() const { return ___storeGameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_14() { return &___storeGameObject_14; }
	inline void set_storeGameObject_14(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETOWNER_T3978915792_H
#ifndef GETPARENT_T2000019683_H
#define GETPARENT_T2000019683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetParent
struct  GetParent_t2000019683  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetParent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetParent::storeResult
	FsmGameObject_t3581898942 * ___storeResult_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetParent_t2000019683, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetParent_t2000019683, ___storeResult_15)); }
	inline FsmGameObject_t3581898942 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmGameObject_t3581898942 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPARENT_T2000019683_H
#ifndef GETRANDOMCHILD_T2866006007_H
#define GETRANDOMCHILD_T2866006007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRandomChild
struct  GetRandomChild_t2866006007  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetRandomChild::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRandomChild::storeResult
	FsmGameObject_t3581898942 * ___storeResult_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetRandomChild_t2866006007, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetRandomChild_t2866006007, ___storeResult_15)); }
	inline FsmGameObject_t3581898942 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmGameObject_t3581898942 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRANDOMCHILD_T2866006007_H
#ifndef GETRANDOMOBJECT_T296617571_H
#define GETRANDOMOBJECT_T296617571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRandomObject
struct  GetRandomObject_t296617571  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetRandomObject::withTag
	FsmString_t1785915204 * ___withTag_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRandomObject::storeResult
	FsmGameObject_t3581898942 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRandomObject::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_withTag_14() { return static_cast<int32_t>(offsetof(GetRandomObject_t296617571, ___withTag_14)); }
	inline FsmString_t1785915204 * get_withTag_14() const { return ___withTag_14; }
	inline FsmString_t1785915204 ** get_address_of_withTag_14() { return &___withTag_14; }
	inline void set_withTag_14(FsmString_t1785915204 * value)
	{
		___withTag_14 = value;
		Il2CppCodeGenWriteBarrier((&___withTag_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetRandomObject_t296617571, ___storeResult_15)); }
	inline FsmGameObject_t3581898942 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmGameObject_t3581898942 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetRandomObject_t296617571, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRANDOMOBJECT_T296617571_H
#ifndef GETROOT_T898400599_H
#define GETROOT_T898400599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRoot
struct  GetRoot_t898400599  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetRoot::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRoot::storeRoot
	FsmGameObject_t3581898942 * ___storeRoot_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetRoot_t898400599, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeRoot_15() { return static_cast<int32_t>(offsetof(GetRoot_t898400599, ___storeRoot_15)); }
	inline FsmGameObject_t3581898942 * get_storeRoot_15() const { return ___storeRoot_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeRoot_15() { return &___storeRoot_15; }
	inline void set_storeRoot_15(FsmGameObject_t3581898942 * value)
	{
		___storeRoot_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeRoot_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETROOT_T898400599_H
#ifndef GETTAG_T3830046547_H
#define GETTAG_T3830046547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTag
struct  GetTag_t3830046547  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetTag::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetTag::storeResult
	FsmString_t1785915204 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTag::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetTag_t3830046547, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetTag_t3830046547, ___storeResult_15)); }
	inline FsmString_t1785915204 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmString_t1785915204 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetTag_t3830046547, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTAG_T3830046547_H
#ifndef GETTAGCOUNT_T1999077496_H
#define GETTAGCOUNT_T1999077496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTagCount
struct  GetTagCount_t1999077496  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetTagCount::tag
	FsmString_t1785915204 * ___tag_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTagCount::storeResult
	FsmInt_t874273141 * ___storeResult_15;

public:
	inline static int32_t get_offset_of_tag_14() { return static_cast<int32_t>(offsetof(GetTagCount_t1999077496, ___tag_14)); }
	inline FsmString_t1785915204 * get_tag_14() const { return ___tag_14; }
	inline FsmString_t1785915204 ** get_address_of_tag_14() { return &___tag_14; }
	inline void set_tag_14(FsmString_t1785915204 * value)
	{
		___tag_14 = value;
		Il2CppCodeGenWriteBarrier((&___tag_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetTagCount_t1999077496, ___storeResult_15)); }
	inline FsmInt_t874273141 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmInt_t874273141 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmInt_t874273141 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTAGCOUNT_T1999077496_H
#ifndef GETTOUCHCOUNT_T3256006630_H
#define GETTOUCHCOUNT_T3256006630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTouchCount
struct  GetTouchCount_t3256006630  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTouchCount::storeCount
	FsmInt_t874273141 * ___storeCount_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTouchCount::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_storeCount_14() { return static_cast<int32_t>(offsetof(GetTouchCount_t3256006630, ___storeCount_14)); }
	inline FsmInt_t874273141 * get_storeCount_14() const { return ___storeCount_14; }
	inline FsmInt_t874273141 ** get_address_of_storeCount_14() { return &___storeCount_14; }
	inline void set_storeCount_14(FsmInt_t874273141 * value)
	{
		___storeCount_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeCount_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(GetTouchCount_t3256006630, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTOUCHCOUNT_T3256006630_H
#ifndef GETTOUCHINFO_T3534618608_H
#define GETTOUCHINFO_T3534618608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTouchInfo
struct  GetTouchInfo_t3534618608  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTouchInfo::fingerId
	FsmInt_t874273141 * ___fingerId_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetTouchInfo::normalize
	FsmBool_t163807967 * ___normalize_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetTouchInfo::storePosition
	FsmVector3_t626444517 * ___storePosition_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeX
	FsmFloat_t2883254149 * ___storeX_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeY
	FsmFloat_t2883254149 * ___storeY_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaPosition
	FsmVector3_t626444517 * ___storeDeltaPosition_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaX
	FsmFloat_t2883254149 * ___storeDeltaX_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaY
	FsmFloat_t2883254149 * ___storeDeltaY_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTouchInfo::storeDeltaTime
	FsmFloat_t2883254149 * ___storeDeltaTime_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTouchInfo::storeTapCount
	FsmInt_t874273141 * ___storeTapCount_23;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTouchInfo::everyFrame
	bool ___everyFrame_24;
	// System.Single HutongGames.PlayMaker.Actions.GetTouchInfo::screenWidth
	float ___screenWidth_25;
	// System.Single HutongGames.PlayMaker.Actions.GetTouchInfo::screenHeight
	float ___screenHeight_26;

public:
	inline static int32_t get_offset_of_fingerId_14() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___fingerId_14)); }
	inline FsmInt_t874273141 * get_fingerId_14() const { return ___fingerId_14; }
	inline FsmInt_t874273141 ** get_address_of_fingerId_14() { return &___fingerId_14; }
	inline void set_fingerId_14(FsmInt_t874273141 * value)
	{
		___fingerId_14 = value;
		Il2CppCodeGenWriteBarrier((&___fingerId_14), value);
	}

	inline static int32_t get_offset_of_normalize_15() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___normalize_15)); }
	inline FsmBool_t163807967 * get_normalize_15() const { return ___normalize_15; }
	inline FsmBool_t163807967 ** get_address_of_normalize_15() { return &___normalize_15; }
	inline void set_normalize_15(FsmBool_t163807967 * value)
	{
		___normalize_15 = value;
		Il2CppCodeGenWriteBarrier((&___normalize_15), value);
	}

	inline static int32_t get_offset_of_storePosition_16() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storePosition_16)); }
	inline FsmVector3_t626444517 * get_storePosition_16() const { return ___storePosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_storePosition_16() { return &___storePosition_16; }
	inline void set_storePosition_16(FsmVector3_t626444517 * value)
	{
		___storePosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___storePosition_16), value);
	}

	inline static int32_t get_offset_of_storeX_17() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeX_17)); }
	inline FsmFloat_t2883254149 * get_storeX_17() const { return ___storeX_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeX_17() { return &___storeX_17; }
	inline void set_storeX_17(FsmFloat_t2883254149 * value)
	{
		___storeX_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeX_17), value);
	}

	inline static int32_t get_offset_of_storeY_18() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeY_18)); }
	inline FsmFloat_t2883254149 * get_storeY_18() const { return ___storeY_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeY_18() { return &___storeY_18; }
	inline void set_storeY_18(FsmFloat_t2883254149 * value)
	{
		___storeY_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeY_18), value);
	}

	inline static int32_t get_offset_of_storeDeltaPosition_19() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeDeltaPosition_19)); }
	inline FsmVector3_t626444517 * get_storeDeltaPosition_19() const { return ___storeDeltaPosition_19; }
	inline FsmVector3_t626444517 ** get_address_of_storeDeltaPosition_19() { return &___storeDeltaPosition_19; }
	inline void set_storeDeltaPosition_19(FsmVector3_t626444517 * value)
	{
		___storeDeltaPosition_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeDeltaPosition_19), value);
	}

	inline static int32_t get_offset_of_storeDeltaX_20() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeDeltaX_20)); }
	inline FsmFloat_t2883254149 * get_storeDeltaX_20() const { return ___storeDeltaX_20; }
	inline FsmFloat_t2883254149 ** get_address_of_storeDeltaX_20() { return &___storeDeltaX_20; }
	inline void set_storeDeltaX_20(FsmFloat_t2883254149 * value)
	{
		___storeDeltaX_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeDeltaX_20), value);
	}

	inline static int32_t get_offset_of_storeDeltaY_21() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeDeltaY_21)); }
	inline FsmFloat_t2883254149 * get_storeDeltaY_21() const { return ___storeDeltaY_21; }
	inline FsmFloat_t2883254149 ** get_address_of_storeDeltaY_21() { return &___storeDeltaY_21; }
	inline void set_storeDeltaY_21(FsmFloat_t2883254149 * value)
	{
		___storeDeltaY_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeDeltaY_21), value);
	}

	inline static int32_t get_offset_of_storeDeltaTime_22() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeDeltaTime_22)); }
	inline FsmFloat_t2883254149 * get_storeDeltaTime_22() const { return ___storeDeltaTime_22; }
	inline FsmFloat_t2883254149 ** get_address_of_storeDeltaTime_22() { return &___storeDeltaTime_22; }
	inline void set_storeDeltaTime_22(FsmFloat_t2883254149 * value)
	{
		___storeDeltaTime_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeDeltaTime_22), value);
	}

	inline static int32_t get_offset_of_storeTapCount_23() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___storeTapCount_23)); }
	inline FsmInt_t874273141 * get_storeTapCount_23() const { return ___storeTapCount_23; }
	inline FsmInt_t874273141 ** get_address_of_storeTapCount_23() { return &___storeTapCount_23; }
	inline void set_storeTapCount_23(FsmInt_t874273141 * value)
	{
		___storeTapCount_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeTapCount_23), value);
	}

	inline static int32_t get_offset_of_everyFrame_24() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___everyFrame_24)); }
	inline bool get_everyFrame_24() const { return ___everyFrame_24; }
	inline bool* get_address_of_everyFrame_24() { return &___everyFrame_24; }
	inline void set_everyFrame_24(bool value)
	{
		___everyFrame_24 = value;
	}

	inline static int32_t get_offset_of_screenWidth_25() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___screenWidth_25)); }
	inline float get_screenWidth_25() const { return ___screenWidth_25; }
	inline float* get_address_of_screenWidth_25() { return &___screenWidth_25; }
	inline void set_screenWidth_25(float value)
	{
		___screenWidth_25 = value;
	}

	inline static int32_t get_offset_of_screenHeight_26() { return static_cast<int32_t>(offsetof(GetTouchInfo_t3534618608, ___screenHeight_26)); }
	inline float get_screenHeight_26() const { return ___screenHeight_26; }
	inline float* get_address_of_screenHeight_26() { return &___screenHeight_26; }
	inline void set_screenHeight_26(float value)
	{
		___screenHeight_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTOUCHINFO_T3534618608_H
#ifndef GETTRANSFORM_T1873649011_H
#define GETTRANSFORM_T1873649011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTransform
struct  GetTransform_t1873649011  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetTransform::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetTransform::storeTransform
	FsmObject_t2606870197 * ___storeTransform_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTransform::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetTransform_t1873649011, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeTransform_15() { return static_cast<int32_t>(offsetof(GetTransform_t1873649011, ___storeTransform_15)); }
	inline FsmObject_t2606870197 * get_storeTransform_15() const { return ___storeTransform_15; }
	inline FsmObject_t2606870197 ** get_address_of_storeTransform_15() { return &___storeTransform_15; }
	inline void set_storeTransform_15(FsmObject_t2606870197 * value)
	{
		___storeTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeTransform_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetTransform_t1873649011, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTRANSFORM_T1873649011_H
#ifndef HASCOMPONENT_T1059837092_H
#define HASCOMPONENT_T1059837092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HasComponent
struct  HasComponent_t1059837092  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HasComponent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HasComponent::component
	FsmString_t1785915204 * ___component_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.HasComponent::removeOnExit
	FsmBool_t163807967 * ___removeOnExit_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HasComponent::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HasComponent::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.HasComponent::store
	FsmBool_t163807967 * ___store_19;
	// System.Boolean HutongGames.PlayMaker.Actions.HasComponent::everyFrame
	bool ___everyFrame_20;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.HasComponent::aComponent
	Component_t1923634451 * ___aComponent_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_component_15() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___component_15)); }
	inline FsmString_t1785915204 * get_component_15() const { return ___component_15; }
	inline FsmString_t1785915204 ** get_address_of_component_15() { return &___component_15; }
	inline void set_component_15(FsmString_t1785915204 * value)
	{
		___component_15 = value;
		Il2CppCodeGenWriteBarrier((&___component_15), value);
	}

	inline static int32_t get_offset_of_removeOnExit_16() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___removeOnExit_16)); }
	inline FsmBool_t163807967 * get_removeOnExit_16() const { return ___removeOnExit_16; }
	inline FsmBool_t163807967 ** get_address_of_removeOnExit_16() { return &___removeOnExit_16; }
	inline void set_removeOnExit_16(FsmBool_t163807967 * value)
	{
		___removeOnExit_16 = value;
		Il2CppCodeGenWriteBarrier((&___removeOnExit_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_store_19() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___store_19)); }
	inline FsmBool_t163807967 * get_store_19() const { return ___store_19; }
	inline FsmBool_t163807967 ** get_address_of_store_19() { return &___store_19; }
	inline void set_store_19(FsmBool_t163807967 * value)
	{
		___store_19 = value;
		Il2CppCodeGenWriteBarrier((&___store_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of_aComponent_21() { return static_cast<int32_t>(offsetof(HasComponent_t1059837092, ___aComponent_21)); }
	inline Component_t1923634451 * get_aComponent_21() const { return ___aComponent_21; }
	inline Component_t1923634451 ** get_address_of_aComponent_21() { return &___aComponent_21; }
	inline void set_aComponent_21(Component_t1923634451 * value)
	{
		___aComponent_21 = value;
		Il2CppCodeGenWriteBarrier((&___aComponent_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASCOMPONENT_T1059837092_H
#ifndef PROJECTLOCATIONTOMAP_T1440892226_H
#define PROJECTLOCATIONTOMAP_T1440892226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ProjectLocationToMap
struct  ProjectLocationToMap_t1440892226  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ProjectLocationToMap::GPSLocation
	FsmVector3_t626444517 * ___GPSLocation_14;
	// HutongGames.PlayMaker.Actions.ProjectLocationToMap/MapProjection HutongGames.PlayMaker.Actions.ProjectLocationToMap::mapProjection
	int32_t ___mapProjection_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minLongitude
	FsmFloat_t2883254149 * ___minLongitude_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::maxLongitude
	FsmFloat_t2883254149 * ___maxLongitude_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minLatitude
	FsmFloat_t2883254149 * ___minLatitude_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::maxLatitude
	FsmFloat_t2883254149 * ___maxLatitude_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minX
	FsmFloat_t2883254149 * ___minX_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::minY
	FsmFloat_t2883254149 * ___minY_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::width
	FsmFloat_t2883254149 * ___width_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::height
	FsmFloat_t2883254149 * ___height_23;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::projectedX
	FsmFloat_t2883254149 * ___projectedX_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ProjectLocationToMap::projectedY
	FsmFloat_t2883254149 * ___projectedY_25;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ProjectLocationToMap::normalized
	FsmBool_t163807967 * ___normalized_26;
	// System.Boolean HutongGames.PlayMaker.Actions.ProjectLocationToMap::everyFrame
	bool ___everyFrame_27;
	// System.Single HutongGames.PlayMaker.Actions.ProjectLocationToMap::x
	float ___x_28;
	// System.Single HutongGames.PlayMaker.Actions.ProjectLocationToMap::y
	float ___y_29;

public:
	inline static int32_t get_offset_of_GPSLocation_14() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___GPSLocation_14)); }
	inline FsmVector3_t626444517 * get_GPSLocation_14() const { return ___GPSLocation_14; }
	inline FsmVector3_t626444517 ** get_address_of_GPSLocation_14() { return &___GPSLocation_14; }
	inline void set_GPSLocation_14(FsmVector3_t626444517 * value)
	{
		___GPSLocation_14 = value;
		Il2CppCodeGenWriteBarrier((&___GPSLocation_14), value);
	}

	inline static int32_t get_offset_of_mapProjection_15() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___mapProjection_15)); }
	inline int32_t get_mapProjection_15() const { return ___mapProjection_15; }
	inline int32_t* get_address_of_mapProjection_15() { return &___mapProjection_15; }
	inline void set_mapProjection_15(int32_t value)
	{
		___mapProjection_15 = value;
	}

	inline static int32_t get_offset_of_minLongitude_16() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___minLongitude_16)); }
	inline FsmFloat_t2883254149 * get_minLongitude_16() const { return ___minLongitude_16; }
	inline FsmFloat_t2883254149 ** get_address_of_minLongitude_16() { return &___minLongitude_16; }
	inline void set_minLongitude_16(FsmFloat_t2883254149 * value)
	{
		___minLongitude_16 = value;
		Il2CppCodeGenWriteBarrier((&___minLongitude_16), value);
	}

	inline static int32_t get_offset_of_maxLongitude_17() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___maxLongitude_17)); }
	inline FsmFloat_t2883254149 * get_maxLongitude_17() const { return ___maxLongitude_17; }
	inline FsmFloat_t2883254149 ** get_address_of_maxLongitude_17() { return &___maxLongitude_17; }
	inline void set_maxLongitude_17(FsmFloat_t2883254149 * value)
	{
		___maxLongitude_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxLongitude_17), value);
	}

	inline static int32_t get_offset_of_minLatitude_18() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___minLatitude_18)); }
	inline FsmFloat_t2883254149 * get_minLatitude_18() const { return ___minLatitude_18; }
	inline FsmFloat_t2883254149 ** get_address_of_minLatitude_18() { return &___minLatitude_18; }
	inline void set_minLatitude_18(FsmFloat_t2883254149 * value)
	{
		___minLatitude_18 = value;
		Il2CppCodeGenWriteBarrier((&___minLatitude_18), value);
	}

	inline static int32_t get_offset_of_maxLatitude_19() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___maxLatitude_19)); }
	inline FsmFloat_t2883254149 * get_maxLatitude_19() const { return ___maxLatitude_19; }
	inline FsmFloat_t2883254149 ** get_address_of_maxLatitude_19() { return &___maxLatitude_19; }
	inline void set_maxLatitude_19(FsmFloat_t2883254149 * value)
	{
		___maxLatitude_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxLatitude_19), value);
	}

	inline static int32_t get_offset_of_minX_20() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___minX_20)); }
	inline FsmFloat_t2883254149 * get_minX_20() const { return ___minX_20; }
	inline FsmFloat_t2883254149 ** get_address_of_minX_20() { return &___minX_20; }
	inline void set_minX_20(FsmFloat_t2883254149 * value)
	{
		___minX_20 = value;
		Il2CppCodeGenWriteBarrier((&___minX_20), value);
	}

	inline static int32_t get_offset_of_minY_21() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___minY_21)); }
	inline FsmFloat_t2883254149 * get_minY_21() const { return ___minY_21; }
	inline FsmFloat_t2883254149 ** get_address_of_minY_21() { return &___minY_21; }
	inline void set_minY_21(FsmFloat_t2883254149 * value)
	{
		___minY_21 = value;
		Il2CppCodeGenWriteBarrier((&___minY_21), value);
	}

	inline static int32_t get_offset_of_width_22() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___width_22)); }
	inline FsmFloat_t2883254149 * get_width_22() const { return ___width_22; }
	inline FsmFloat_t2883254149 ** get_address_of_width_22() { return &___width_22; }
	inline void set_width_22(FsmFloat_t2883254149 * value)
	{
		___width_22 = value;
		Il2CppCodeGenWriteBarrier((&___width_22), value);
	}

	inline static int32_t get_offset_of_height_23() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___height_23)); }
	inline FsmFloat_t2883254149 * get_height_23() const { return ___height_23; }
	inline FsmFloat_t2883254149 ** get_address_of_height_23() { return &___height_23; }
	inline void set_height_23(FsmFloat_t2883254149 * value)
	{
		___height_23 = value;
		Il2CppCodeGenWriteBarrier((&___height_23), value);
	}

	inline static int32_t get_offset_of_projectedX_24() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___projectedX_24)); }
	inline FsmFloat_t2883254149 * get_projectedX_24() const { return ___projectedX_24; }
	inline FsmFloat_t2883254149 ** get_address_of_projectedX_24() { return &___projectedX_24; }
	inline void set_projectedX_24(FsmFloat_t2883254149 * value)
	{
		___projectedX_24 = value;
		Il2CppCodeGenWriteBarrier((&___projectedX_24), value);
	}

	inline static int32_t get_offset_of_projectedY_25() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___projectedY_25)); }
	inline FsmFloat_t2883254149 * get_projectedY_25() const { return ___projectedY_25; }
	inline FsmFloat_t2883254149 ** get_address_of_projectedY_25() { return &___projectedY_25; }
	inline void set_projectedY_25(FsmFloat_t2883254149 * value)
	{
		___projectedY_25 = value;
		Il2CppCodeGenWriteBarrier((&___projectedY_25), value);
	}

	inline static int32_t get_offset_of_normalized_26() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___normalized_26)); }
	inline FsmBool_t163807967 * get_normalized_26() const { return ___normalized_26; }
	inline FsmBool_t163807967 ** get_address_of_normalized_26() { return &___normalized_26; }
	inline void set_normalized_26(FsmBool_t163807967 * value)
	{
		___normalized_26 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_26), value);
	}

	inline static int32_t get_offset_of_everyFrame_27() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___everyFrame_27)); }
	inline bool get_everyFrame_27() const { return ___everyFrame_27; }
	inline bool* get_address_of_everyFrame_27() { return &___everyFrame_27; }
	inline void set_everyFrame_27(bool value)
	{
		___everyFrame_27 = value;
	}

	inline static int32_t get_offset_of_x_28() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___x_28)); }
	inline float get_x_28() const { return ___x_28; }
	inline float* get_address_of_x_28() { return &___x_28; }
	inline void set_x_28(float value)
	{
		___x_28 = value;
	}

	inline static int32_t get_offset_of_y_29() { return static_cast<int32_t>(offsetof(ProjectLocationToMap_t1440892226, ___y_29)); }
	inline float get_y_29() const { return ___y_29; }
	inline float* get_address_of_y_29() { return &___y_29; }
	inline void set_y_29(float value)
	{
		___y_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTLOCATIONTOMAP_T1440892226_H
#ifndef RESETGUIMATRIX_T582177292_H
#define RESETGUIMATRIX_T582177292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ResetGUIMatrix
struct  ResetGUIMatrix_t582177292  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETGUIMATRIX_T582177292_H
#ifndef ROTATEGUI_T3863613888_H
#define ROTATEGUI_T3863613888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RotateGUI
struct  RotateGUI_t3863613888  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RotateGUI::angle
	FsmFloat_t2883254149 * ___angle_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RotateGUI::pivotX
	FsmFloat_t2883254149 * ___pivotX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RotateGUI::pivotY
	FsmFloat_t2883254149 * ___pivotY_16;
	// System.Boolean HutongGames.PlayMaker.Actions.RotateGUI::normalized
	bool ___normalized_17;
	// System.Boolean HutongGames.PlayMaker.Actions.RotateGUI::applyGlobally
	bool ___applyGlobally_18;
	// System.Boolean HutongGames.PlayMaker.Actions.RotateGUI::applied
	bool ___applied_19;

public:
	inline static int32_t get_offset_of_angle_14() { return static_cast<int32_t>(offsetof(RotateGUI_t3863613888, ___angle_14)); }
	inline FsmFloat_t2883254149 * get_angle_14() const { return ___angle_14; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_14() { return &___angle_14; }
	inline void set_angle_14(FsmFloat_t2883254149 * value)
	{
		___angle_14 = value;
		Il2CppCodeGenWriteBarrier((&___angle_14), value);
	}

	inline static int32_t get_offset_of_pivotX_15() { return static_cast<int32_t>(offsetof(RotateGUI_t3863613888, ___pivotX_15)); }
	inline FsmFloat_t2883254149 * get_pivotX_15() const { return ___pivotX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_pivotX_15() { return &___pivotX_15; }
	inline void set_pivotX_15(FsmFloat_t2883254149 * value)
	{
		___pivotX_15 = value;
		Il2CppCodeGenWriteBarrier((&___pivotX_15), value);
	}

	inline static int32_t get_offset_of_pivotY_16() { return static_cast<int32_t>(offsetof(RotateGUI_t3863613888, ___pivotY_16)); }
	inline FsmFloat_t2883254149 * get_pivotY_16() const { return ___pivotY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_pivotY_16() { return &___pivotY_16; }
	inline void set_pivotY_16(FsmFloat_t2883254149 * value)
	{
		___pivotY_16 = value;
		Il2CppCodeGenWriteBarrier((&___pivotY_16), value);
	}

	inline static int32_t get_offset_of_normalized_17() { return static_cast<int32_t>(offsetof(RotateGUI_t3863613888, ___normalized_17)); }
	inline bool get_normalized_17() const { return ___normalized_17; }
	inline bool* get_address_of_normalized_17() { return &___normalized_17; }
	inline void set_normalized_17(bool value)
	{
		___normalized_17 = value;
	}

	inline static int32_t get_offset_of_applyGlobally_18() { return static_cast<int32_t>(offsetof(RotateGUI_t3863613888, ___applyGlobally_18)); }
	inline bool get_applyGlobally_18() const { return ___applyGlobally_18; }
	inline bool* get_address_of_applyGlobally_18() { return &___applyGlobally_18; }
	inline void set_applyGlobally_18(bool value)
	{
		___applyGlobally_18 = value;
	}

	inline static int32_t get_offset_of_applied_19() { return static_cast<int32_t>(offsetof(RotateGUI_t3863613888, ___applied_19)); }
	inline bool get_applied_19() const { return ___applied_19; }
	inline bool* get_address_of_applied_19() { return &___applied_19; }
	inline void set_applied_19(bool value)
	{
		___applied_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEGUI_T3863613888_H
#ifndef SCALEGUI_T3223259317_H
#define SCALEGUI_T3223259317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScaleGUI
struct  ScaleGUI_t3223259317  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScaleGUI::scaleX
	FsmFloat_t2883254149 * ___scaleX_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScaleGUI::scaleY
	FsmFloat_t2883254149 * ___scaleY_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScaleGUI::pivotX
	FsmFloat_t2883254149 * ___pivotX_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScaleGUI::pivotY
	FsmFloat_t2883254149 * ___pivotY_17;
	// System.Boolean HutongGames.PlayMaker.Actions.ScaleGUI::normalized
	bool ___normalized_18;
	// System.Boolean HutongGames.PlayMaker.Actions.ScaleGUI::applyGlobally
	bool ___applyGlobally_19;
	// System.Boolean HutongGames.PlayMaker.Actions.ScaleGUI::applied
	bool ___applied_20;

public:
	inline static int32_t get_offset_of_scaleX_14() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___scaleX_14)); }
	inline FsmFloat_t2883254149 * get_scaleX_14() const { return ___scaleX_14; }
	inline FsmFloat_t2883254149 ** get_address_of_scaleX_14() { return &___scaleX_14; }
	inline void set_scaleX_14(FsmFloat_t2883254149 * value)
	{
		___scaleX_14 = value;
		Il2CppCodeGenWriteBarrier((&___scaleX_14), value);
	}

	inline static int32_t get_offset_of_scaleY_15() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___scaleY_15)); }
	inline FsmFloat_t2883254149 * get_scaleY_15() const { return ___scaleY_15; }
	inline FsmFloat_t2883254149 ** get_address_of_scaleY_15() { return &___scaleY_15; }
	inline void set_scaleY_15(FsmFloat_t2883254149 * value)
	{
		___scaleY_15 = value;
		Il2CppCodeGenWriteBarrier((&___scaleY_15), value);
	}

	inline static int32_t get_offset_of_pivotX_16() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___pivotX_16)); }
	inline FsmFloat_t2883254149 * get_pivotX_16() const { return ___pivotX_16; }
	inline FsmFloat_t2883254149 ** get_address_of_pivotX_16() { return &___pivotX_16; }
	inline void set_pivotX_16(FsmFloat_t2883254149 * value)
	{
		___pivotX_16 = value;
		Il2CppCodeGenWriteBarrier((&___pivotX_16), value);
	}

	inline static int32_t get_offset_of_pivotY_17() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___pivotY_17)); }
	inline FsmFloat_t2883254149 * get_pivotY_17() const { return ___pivotY_17; }
	inline FsmFloat_t2883254149 ** get_address_of_pivotY_17() { return &___pivotY_17; }
	inline void set_pivotY_17(FsmFloat_t2883254149 * value)
	{
		___pivotY_17 = value;
		Il2CppCodeGenWriteBarrier((&___pivotY_17), value);
	}

	inline static int32_t get_offset_of_normalized_18() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___normalized_18)); }
	inline bool get_normalized_18() const { return ___normalized_18; }
	inline bool* get_address_of_normalized_18() { return &___normalized_18; }
	inline void set_normalized_18(bool value)
	{
		___normalized_18 = value;
	}

	inline static int32_t get_offset_of_applyGlobally_19() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___applyGlobally_19)); }
	inline bool get_applyGlobally_19() const { return ___applyGlobally_19; }
	inline bool* get_address_of_applyGlobally_19() { return &___applyGlobally_19; }
	inline void set_applyGlobally_19(bool value)
	{
		___applyGlobally_19 = value;
	}

	inline static int32_t get_offset_of_applied_20() { return static_cast<int32_t>(offsetof(ScaleGUI_t3223259317, ___applied_20)); }
	inline bool get_applied_20() const { return ___applied_20; }
	inline bool* get_address_of_applied_20() { return &___applied_20; }
	inline void set_applied_20(bool value)
	{
		___applied_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEGUI_T3223259317_H
#ifndef SELECTRANDOMGAMEOBJECT_T2588744226_H
#define SELECTRANDOMGAMEOBJECT_T2588744226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomGameObject
struct  SelectRandomGameObject_t2588744226  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.Actions.SelectRandomGameObject::gameObjects
	FsmGameObjectU5BU5D_t719957643* ___gameObjects_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomGameObject::weights
	FsmFloatU5BU5D_t3637897416* ___weights_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SelectRandomGameObject::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_16;

public:
	inline static int32_t get_offset_of_gameObjects_14() { return static_cast<int32_t>(offsetof(SelectRandomGameObject_t2588744226, ___gameObjects_14)); }
	inline FsmGameObjectU5BU5D_t719957643* get_gameObjects_14() const { return ___gameObjects_14; }
	inline FsmGameObjectU5BU5D_t719957643** get_address_of_gameObjects_14() { return &___gameObjects_14; }
	inline void set_gameObjects_14(FsmGameObjectU5BU5D_t719957643* value)
	{
		___gameObjects_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjects_14), value);
	}

	inline static int32_t get_offset_of_weights_15() { return static_cast<int32_t>(offsetof(SelectRandomGameObject_t2588744226, ___weights_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_15() const { return ___weights_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_15() { return &___weights_15; }
	inline void set_weights_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_15 = value;
		Il2CppCodeGenWriteBarrier((&___weights_15), value);
	}

	inline static int32_t get_offset_of_storeGameObject_16() { return static_cast<int32_t>(offsetof(SelectRandomGameObject_t2588744226, ___storeGameObject_16)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_16() const { return ___storeGameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_16() { return &___storeGameObject_16; }
	inline void set_storeGameObject_16(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTRANDOMGAMEOBJECT_T2588744226_H
#ifndef SETENUMVALUE_T2351312988_H
#define SETENUMVALUE_T2351312988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetEnumValue
struct  SetEnumValue_t2351312988  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.SetEnumValue::enumVariable
	FsmEnum_t2861764163 * ___enumVariable_14;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.SetEnumValue::enumValue
	FsmEnum_t2861764163 * ___enumValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetEnumValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_enumVariable_14() { return static_cast<int32_t>(offsetof(SetEnumValue_t2351312988, ___enumVariable_14)); }
	inline FsmEnum_t2861764163 * get_enumVariable_14() const { return ___enumVariable_14; }
	inline FsmEnum_t2861764163 ** get_address_of_enumVariable_14() { return &___enumVariable_14; }
	inline void set_enumVariable_14(FsmEnum_t2861764163 * value)
	{
		___enumVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariable_14), value);
	}

	inline static int32_t get_offset_of_enumValue_15() { return static_cast<int32_t>(offsetof(SetEnumValue_t2351312988, ___enumValue_15)); }
	inline FsmEnum_t2861764163 * get_enumValue_15() const { return ___enumValue_15; }
	inline FsmEnum_t2861764163 ** get_address_of_enumValue_15() { return &___enumValue_15; }
	inline void set_enumValue_15(FsmEnum_t2861764163 * value)
	{
		___enumValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___enumValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetEnumValue_t2351312988, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETENUMVALUE_T2351312988_H
#ifndef SETGUIALPHA_T2212989556_H
#define SETGUIALPHA_T2212989556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIAlpha
struct  SetGUIAlpha_t2212989556  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGUIAlpha::alpha
	FsmFloat_t2883254149 * ___alpha_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUIAlpha::applyGlobally
	FsmBool_t163807967 * ___applyGlobally_15;

public:
	inline static int32_t get_offset_of_alpha_14() { return static_cast<int32_t>(offsetof(SetGUIAlpha_t2212989556, ___alpha_14)); }
	inline FsmFloat_t2883254149 * get_alpha_14() const { return ___alpha_14; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_14() { return &___alpha_14; }
	inline void set_alpha_14(FsmFloat_t2883254149 * value)
	{
		___alpha_14 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_14), value);
	}

	inline static int32_t get_offset_of_applyGlobally_15() { return static_cast<int32_t>(offsetof(SetGUIAlpha_t2212989556, ___applyGlobally_15)); }
	inline FsmBool_t163807967 * get_applyGlobally_15() const { return ___applyGlobally_15; }
	inline FsmBool_t163807967 ** get_address_of_applyGlobally_15() { return &___applyGlobally_15; }
	inline void set_applyGlobally_15(FsmBool_t163807967 * value)
	{
		___applyGlobally_15 = value;
		Il2CppCodeGenWriteBarrier((&___applyGlobally_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUIALPHA_T2212989556_H
#ifndef SETGUIBACKGROUNDCOLOR_T544783107_H
#define SETGUIBACKGROUNDCOLOR_T544783107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIBackgroundColor
struct  SetGUIBackgroundColor_t544783107  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::backgroundColor
	FsmColor_t1738900188 * ___backgroundColor_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUIBackgroundColor::applyGlobally
	FsmBool_t163807967 * ___applyGlobally_15;

public:
	inline static int32_t get_offset_of_backgroundColor_14() { return static_cast<int32_t>(offsetof(SetGUIBackgroundColor_t544783107, ___backgroundColor_14)); }
	inline FsmColor_t1738900188 * get_backgroundColor_14() const { return ___backgroundColor_14; }
	inline FsmColor_t1738900188 ** get_address_of_backgroundColor_14() { return &___backgroundColor_14; }
	inline void set_backgroundColor_14(FsmColor_t1738900188 * value)
	{
		___backgroundColor_14 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundColor_14), value);
	}

	inline static int32_t get_offset_of_applyGlobally_15() { return static_cast<int32_t>(offsetof(SetGUIBackgroundColor_t544783107, ___applyGlobally_15)); }
	inline FsmBool_t163807967 * get_applyGlobally_15() const { return ___applyGlobally_15; }
	inline FsmBool_t163807967 ** get_address_of_applyGlobally_15() { return &___applyGlobally_15; }
	inline void set_applyGlobally_15(FsmBool_t163807967 * value)
	{
		___applyGlobally_15 = value;
		Il2CppCodeGenWriteBarrier((&___applyGlobally_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUIBACKGROUNDCOLOR_T544783107_H
#ifndef SETGUICOLOR_T505170613_H
#define SETGUICOLOR_T505170613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIColor
struct  SetGUIColor_t505170613  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetGUIColor::color
	FsmColor_t1738900188 * ___color_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUIColor::applyGlobally
	FsmBool_t163807967 * ___applyGlobally_15;

public:
	inline static int32_t get_offset_of_color_14() { return static_cast<int32_t>(offsetof(SetGUIColor_t505170613, ___color_14)); }
	inline FsmColor_t1738900188 * get_color_14() const { return ___color_14; }
	inline FsmColor_t1738900188 ** get_address_of_color_14() { return &___color_14; }
	inline void set_color_14(FsmColor_t1738900188 * value)
	{
		___color_14 = value;
		Il2CppCodeGenWriteBarrier((&___color_14), value);
	}

	inline static int32_t get_offset_of_applyGlobally_15() { return static_cast<int32_t>(offsetof(SetGUIColor_t505170613, ___applyGlobally_15)); }
	inline FsmBool_t163807967 * get_applyGlobally_15() const { return ___applyGlobally_15; }
	inline FsmBool_t163807967 ** get_address_of_applyGlobally_15() { return &___applyGlobally_15; }
	inline void set_applyGlobally_15(FsmBool_t163807967 * value)
	{
		___applyGlobally_15 = value;
		Il2CppCodeGenWriteBarrier((&___applyGlobally_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUICOLOR_T505170613_H
#ifndef SETGUICONTENTCOLOR_T3963444964_H
#define SETGUICONTENTCOLOR_T3963444964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIContentColor
struct  SetGUIContentColor_t3963444964  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetGUIContentColor::contentColor
	FsmColor_t1738900188 * ___contentColor_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUIContentColor::applyGlobally
	FsmBool_t163807967 * ___applyGlobally_15;

public:
	inline static int32_t get_offset_of_contentColor_14() { return static_cast<int32_t>(offsetof(SetGUIContentColor_t3963444964, ___contentColor_14)); }
	inline FsmColor_t1738900188 * get_contentColor_14() const { return ___contentColor_14; }
	inline FsmColor_t1738900188 ** get_address_of_contentColor_14() { return &___contentColor_14; }
	inline void set_contentColor_14(FsmColor_t1738900188 * value)
	{
		___contentColor_14 = value;
		Il2CppCodeGenWriteBarrier((&___contentColor_14), value);
	}

	inline static int32_t get_offset_of_applyGlobally_15() { return static_cast<int32_t>(offsetof(SetGUIContentColor_t3963444964, ___applyGlobally_15)); }
	inline FsmBool_t163807967 * get_applyGlobally_15() const { return ___applyGlobally_15; }
	inline FsmBool_t163807967 ** get_address_of_applyGlobally_15() { return &___applyGlobally_15; }
	inline void set_applyGlobally_15(FsmBool_t163807967 * value)
	{
		___applyGlobally_15 = value;
		Il2CppCodeGenWriteBarrier((&___applyGlobally_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUICONTENTCOLOR_T3963444964_H
#ifndef SETGUIDEPTH_T192580112_H
#define SETGUIDEPTH_T192580112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIDepth
struct  SetGUIDepth_t192580112  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetGUIDepth::depth
	FsmInt_t874273141 * ___depth_14;

public:
	inline static int32_t get_offset_of_depth_14() { return static_cast<int32_t>(offsetof(SetGUIDepth_t192580112, ___depth_14)); }
	inline FsmInt_t874273141 * get_depth_14() const { return ___depth_14; }
	inline FsmInt_t874273141 ** get_address_of_depth_14() { return &___depth_14; }
	inline void set_depth_14(FsmInt_t874273141 * value)
	{
		___depth_14 = value;
		Il2CppCodeGenWriteBarrier((&___depth_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUIDEPTH_T192580112_H
#ifndef SETGUISKIN_T296284078_H
#define SETGUISKIN_T296284078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUISkin
struct  SetGUISkin_t296284078  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GUISkin HutongGames.PlayMaker.Actions.SetGUISkin::skin
	GUISkin_t1244372282 * ___skin_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetGUISkin::applyGlobally
	FsmBool_t163807967 * ___applyGlobally_15;

public:
	inline static int32_t get_offset_of_skin_14() { return static_cast<int32_t>(offsetof(SetGUISkin_t296284078, ___skin_14)); }
	inline GUISkin_t1244372282 * get_skin_14() const { return ___skin_14; }
	inline GUISkin_t1244372282 ** get_address_of_skin_14() { return &___skin_14; }
	inline void set_skin_14(GUISkin_t1244372282 * value)
	{
		___skin_14 = value;
		Il2CppCodeGenWriteBarrier((&___skin_14), value);
	}

	inline static int32_t get_offset_of_applyGlobally_15() { return static_cast<int32_t>(offsetof(SetGUISkin_t296284078, ___applyGlobally_15)); }
	inline FsmBool_t163807967 * get_applyGlobally_15() const { return ___applyGlobally_15; }
	inline FsmBool_t163807967 ** get_address_of_applyGlobally_15() { return &___applyGlobally_15; }
	inline void set_applyGlobally_15(FsmBool_t163807967 * value)
	{
		___applyGlobally_15 = value;
		Il2CppCodeGenWriteBarrier((&___applyGlobally_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUISKIN_T296284078_H
#ifndef SETGAMEOBJECT_T597754492_H
#define SETGAMEOBJECT_T597754492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGameObject
struct  SetGameObject_t597754492  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetGameObject::variable
	FsmGameObject_t3581898942 * ___variable_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetGameObject::gameObject
	FsmGameObject_t3581898942 * ___gameObject_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGameObject::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_variable_14() { return static_cast<int32_t>(offsetof(SetGameObject_t597754492, ___variable_14)); }
	inline FsmGameObject_t3581898942 * get_variable_14() const { return ___variable_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_variable_14() { return &___variable_14; }
	inline void set_variable_14(FsmGameObject_t3581898942 * value)
	{
		___variable_14 = value;
		Il2CppCodeGenWriteBarrier((&___variable_14), value);
	}

	inline static int32_t get_offset_of_gameObject_15() { return static_cast<int32_t>(offsetof(SetGameObject_t597754492, ___gameObject_15)); }
	inline FsmGameObject_t3581898942 * get_gameObject_15() const { return ___gameObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_15() { return &___gameObject_15; }
	inline void set_gameObject_15(FsmGameObject_t3581898942 * value)
	{
		___gameObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetGameObject_t597754492, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGAMEOBJECT_T597754492_H
#ifndef SETLAYER_T3918912646_H
#define SETLAYER_T3918912646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLayer
struct  SetLayer_t3918912646  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLayer::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// System.Int32 HutongGames.PlayMaker.Actions.SetLayer::layer
	int32_t ___layer_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetLayer_t3918912646, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_layer_15() { return static_cast<int32_t>(offsetof(SetLayer_t3918912646, ___layer_15)); }
	inline int32_t get_layer_15() const { return ___layer_15; }
	inline int32_t* get_address_of_layer_15() { return &___layer_15; }
	inline void set_layer_15(int32_t value)
	{
		___layer_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLAYER_T3918912646_H
#ifndef SETMOUSECURSOR_T3988608720_H
#define SETMOUSECURSOR_T3988608720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMouseCursor
struct  SetMouseCursor_t3988608720  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetMouseCursor::cursorTexture
	FsmTexture_t1738787045 * ___cursorTexture_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetMouseCursor::hideCursor
	FsmBool_t163807967 * ___hideCursor_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetMouseCursor::lockCursor
	FsmBool_t163807967 * ___lockCursor_16;

public:
	inline static int32_t get_offset_of_cursorTexture_14() { return static_cast<int32_t>(offsetof(SetMouseCursor_t3988608720, ___cursorTexture_14)); }
	inline FsmTexture_t1738787045 * get_cursorTexture_14() const { return ___cursorTexture_14; }
	inline FsmTexture_t1738787045 ** get_address_of_cursorTexture_14() { return &___cursorTexture_14; }
	inline void set_cursorTexture_14(FsmTexture_t1738787045 * value)
	{
		___cursorTexture_14 = value;
		Il2CppCodeGenWriteBarrier((&___cursorTexture_14), value);
	}

	inline static int32_t get_offset_of_hideCursor_15() { return static_cast<int32_t>(offsetof(SetMouseCursor_t3988608720, ___hideCursor_15)); }
	inline FsmBool_t163807967 * get_hideCursor_15() const { return ___hideCursor_15; }
	inline FsmBool_t163807967 ** get_address_of_hideCursor_15() { return &___hideCursor_15; }
	inline void set_hideCursor_15(FsmBool_t163807967 * value)
	{
		___hideCursor_15 = value;
		Il2CppCodeGenWriteBarrier((&___hideCursor_15), value);
	}

	inline static int32_t get_offset_of_lockCursor_16() { return static_cast<int32_t>(offsetof(SetMouseCursor_t3988608720, ___lockCursor_16)); }
	inline FsmBool_t163807967 * get_lockCursor_16() const { return ___lockCursor_16; }
	inline FsmBool_t163807967 ** get_address_of_lockCursor_16() { return &___lockCursor_16; }
	inline void set_lockCursor_16(FsmBool_t163807967 * value)
	{
		___lockCursor_16 = value;
		Il2CppCodeGenWriteBarrier((&___lockCursor_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMOUSECURSOR_T3988608720_H
#ifndef SETNAME_T1624104070_H
#define SETNAME_T1624104070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetName
struct  SetName_t1624104070  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetName::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetName::name
	FsmString_t1785915204 * ___name_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetName_t1624104070, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_name_15() { return static_cast<int32_t>(offsetof(SetName_t1624104070, ___name_15)); }
	inline FsmString_t1785915204 * get_name_15() const { return ___name_15; }
	inline FsmString_t1785915204 ** get_address_of_name_15() { return &___name_15; }
	inline void set_name_15(FsmString_t1785915204 * value)
	{
		___name_15 = value;
		Il2CppCodeGenWriteBarrier((&___name_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETNAME_T1624104070_H
#ifndef SETPARENT_T3814774471_H
#define SETPARENT_T3814774471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetParent
struct  SetParent_t3814774471  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetParent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetParent::parent
	FsmGameObject_t3581898942 * ___parent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetParent::resetLocalPosition
	FsmBool_t163807967 * ___resetLocalPosition_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetParent::resetLocalRotation
	FsmBool_t163807967 * ___resetLocalRotation_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetParent_t3814774471, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_parent_15() { return static_cast<int32_t>(offsetof(SetParent_t3814774471, ___parent_15)); }
	inline FsmGameObject_t3581898942 * get_parent_15() const { return ___parent_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_parent_15() { return &___parent_15; }
	inline void set_parent_15(FsmGameObject_t3581898942 * value)
	{
		___parent_15 = value;
		Il2CppCodeGenWriteBarrier((&___parent_15), value);
	}

	inline static int32_t get_offset_of_resetLocalPosition_16() { return static_cast<int32_t>(offsetof(SetParent_t3814774471, ___resetLocalPosition_16)); }
	inline FsmBool_t163807967 * get_resetLocalPosition_16() const { return ___resetLocalPosition_16; }
	inline FsmBool_t163807967 ** get_address_of_resetLocalPosition_16() { return &___resetLocalPosition_16; }
	inline void set_resetLocalPosition_16(FsmBool_t163807967 * value)
	{
		___resetLocalPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___resetLocalPosition_16), value);
	}

	inline static int32_t get_offset_of_resetLocalRotation_17() { return static_cast<int32_t>(offsetof(SetParent_t3814774471, ___resetLocalRotation_17)); }
	inline FsmBool_t163807967 * get_resetLocalRotation_17() const { return ___resetLocalRotation_17; }
	inline FsmBool_t163807967 ** get_address_of_resetLocalRotation_17() { return &___resetLocalRotation_17; }
	inline void set_resetLocalRotation_17(FsmBool_t163807967 * value)
	{
		___resetLocalRotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___resetLocalRotation_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPARENT_T3814774471_H
#ifndef SETTAG_T1389133551_H
#define SETTAG_T1389133551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetTag
struct  SetTag_t1389133551  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetTag::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetTag::tag
	FsmString_t1785915204 * ___tag_15;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetTag_t1389133551, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_tag_15() { return static_cast<int32_t>(offsetof(SetTag_t1389133551, ___tag_15)); }
	inline FsmString_t1785915204 * get_tag_15() const { return ___tag_15; }
	inline FsmString_t1785915204 ** get_address_of_tag_15() { return &___tag_15; }
	inline void set_tag_15(FsmString_t1785915204 * value)
	{
		___tag_15 = value;
		Il2CppCodeGenWriteBarrier((&___tag_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTAG_T1389133551_H
#ifndef SETTAGSONCHILDREN_T4128407550_H
#define SETTAGSONCHILDREN_T4128407550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetTagsOnChildren
struct  SetTagsOnChildren_t4128407550  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetTagsOnChildren::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetTagsOnChildren::tag
	FsmString_t1785915204 * ___tag_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetTagsOnChildren::filterByComponent
	FsmString_t1785915204 * ___filterByComponent_16;
	// System.Type HutongGames.PlayMaker.Actions.SetTagsOnChildren::componentFilter
	Type_t * ___componentFilter_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetTagsOnChildren_t4128407550, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_tag_15() { return static_cast<int32_t>(offsetof(SetTagsOnChildren_t4128407550, ___tag_15)); }
	inline FsmString_t1785915204 * get_tag_15() const { return ___tag_15; }
	inline FsmString_t1785915204 ** get_address_of_tag_15() { return &___tag_15; }
	inline void set_tag_15(FsmString_t1785915204 * value)
	{
		___tag_15 = value;
		Il2CppCodeGenWriteBarrier((&___tag_15), value);
	}

	inline static int32_t get_offset_of_filterByComponent_16() { return static_cast<int32_t>(offsetof(SetTagsOnChildren_t4128407550, ___filterByComponent_16)); }
	inline FsmString_t1785915204 * get_filterByComponent_16() const { return ___filterByComponent_16; }
	inline FsmString_t1785915204 ** get_address_of_filterByComponent_16() { return &___filterByComponent_16; }
	inline void set_filterByComponent_16(FsmString_t1785915204 * value)
	{
		___filterByComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___filterByComponent_16), value);
	}

	inline static int32_t get_offset_of_componentFilter_17() { return static_cast<int32_t>(offsetof(SetTagsOnChildren_t4128407550, ___componentFilter_17)); }
	inline Type_t * get_componentFilter_17() const { return ___componentFilter_17; }
	inline Type_t ** get_address_of_componentFilter_17() { return &___componentFilter_17; }
	inline void set_componentFilter_17(Type_t * value)
	{
		___componentFilter_17 = value;
		Il2CppCodeGenWriteBarrier((&___componentFilter_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTAGSONCHILDREN_T4128407550_H
#ifndef STARTLOCATIONSERVICEUPDATES_T3395549688_H
#define STARTLOCATIONSERVICEUPDATES_T3395549688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StartLocationServiceUpdates
struct  StartLocationServiceUpdates_t3395549688  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::maxWait
	FsmFloat_t2883254149 * ___maxWait_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::desiredAccuracy
	FsmFloat_t2883254149 * ___desiredAccuracy_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::updateDistance
	FsmFloat_t2883254149 * ___updateDistance_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::successEvent
	FsmEvent_t3736299882 * ___successEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::failedEvent
	FsmEvent_t3736299882 * ___failedEvent_18;
	// System.Single HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::startTime
	float ___startTime_19;

public:
	inline static int32_t get_offset_of_maxWait_14() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t3395549688, ___maxWait_14)); }
	inline FsmFloat_t2883254149 * get_maxWait_14() const { return ___maxWait_14; }
	inline FsmFloat_t2883254149 ** get_address_of_maxWait_14() { return &___maxWait_14; }
	inline void set_maxWait_14(FsmFloat_t2883254149 * value)
	{
		___maxWait_14 = value;
		Il2CppCodeGenWriteBarrier((&___maxWait_14), value);
	}

	inline static int32_t get_offset_of_desiredAccuracy_15() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t3395549688, ___desiredAccuracy_15)); }
	inline FsmFloat_t2883254149 * get_desiredAccuracy_15() const { return ___desiredAccuracy_15; }
	inline FsmFloat_t2883254149 ** get_address_of_desiredAccuracy_15() { return &___desiredAccuracy_15; }
	inline void set_desiredAccuracy_15(FsmFloat_t2883254149 * value)
	{
		___desiredAccuracy_15 = value;
		Il2CppCodeGenWriteBarrier((&___desiredAccuracy_15), value);
	}

	inline static int32_t get_offset_of_updateDistance_16() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t3395549688, ___updateDistance_16)); }
	inline FsmFloat_t2883254149 * get_updateDistance_16() const { return ___updateDistance_16; }
	inline FsmFloat_t2883254149 ** get_address_of_updateDistance_16() { return &___updateDistance_16; }
	inline void set_updateDistance_16(FsmFloat_t2883254149 * value)
	{
		___updateDistance_16 = value;
		Il2CppCodeGenWriteBarrier((&___updateDistance_16), value);
	}

	inline static int32_t get_offset_of_successEvent_17() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t3395549688, ___successEvent_17)); }
	inline FsmEvent_t3736299882 * get_successEvent_17() const { return ___successEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_successEvent_17() { return &___successEvent_17; }
	inline void set_successEvent_17(FsmEvent_t3736299882 * value)
	{
		___successEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___successEvent_17), value);
	}

	inline static int32_t get_offset_of_failedEvent_18() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t3395549688, ___failedEvent_18)); }
	inline FsmEvent_t3736299882 * get_failedEvent_18() const { return ___failedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_failedEvent_18() { return &___failedEvent_18; }
	inline void set_failedEvent_18(FsmEvent_t3736299882 * value)
	{
		___failedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___failedEvent_18), value);
	}

	inline static int32_t get_offset_of_startTime_19() { return static_cast<int32_t>(offsetof(StartLocationServiceUpdates_t3395549688, ___startTime_19)); }
	inline float get_startTime_19() const { return ___startTime_19; }
	inline float* get_address_of_startTime_19() { return &___startTime_19; }
	inline void set_startTime_19(float value)
	{
		___startTime_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTLOCATIONSERVICEUPDATES_T3395549688_H
#ifndef STOPLOCATIONSERVICEUPDATES_T1376535429_H
#define STOPLOCATIONSERVICEUPDATES_T1376535429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StopLocationServiceUpdates
struct  StopLocationServiceUpdates_t1376535429  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPLOCATIONSERVICEUPDATES_T1376535429_H
#ifndef SWIPEGESTUREEVENT_T1778425824_H
#define SWIPEGESTUREEVENT_T1778425824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SwipeGestureEvent
struct  SwipeGestureEvent_t1778425824  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SwipeGestureEvent::minSwipeDistance
	FsmFloat_t2883254149 * ___minSwipeDistance_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeLeftEvent
	FsmEvent_t3736299882 * ___swipeLeftEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeRightEvent
	FsmEvent_t3736299882 * ___swipeRightEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeUpEvent
	FsmEvent_t3736299882 * ___swipeUpEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SwipeGestureEvent::swipeDownEvent
	FsmEvent_t3736299882 * ___swipeDownEvent_18;
	// System.Single HutongGames.PlayMaker.Actions.SwipeGestureEvent::screenDiagonalSize
	float ___screenDiagonalSize_19;
	// System.Single HutongGames.PlayMaker.Actions.SwipeGestureEvent::minSwipeDistancePixels
	float ___minSwipeDistancePixels_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SwipeGestureEvent::touchStarted
	bool ___touchStarted_21;
	// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.SwipeGestureEvent::touchStartPos
	Vector2_t2156229523  ___touchStartPos_22;

public:
	inline static int32_t get_offset_of_minSwipeDistance_14() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___minSwipeDistance_14)); }
	inline FsmFloat_t2883254149 * get_minSwipeDistance_14() const { return ___minSwipeDistance_14; }
	inline FsmFloat_t2883254149 ** get_address_of_minSwipeDistance_14() { return &___minSwipeDistance_14; }
	inline void set_minSwipeDistance_14(FsmFloat_t2883254149 * value)
	{
		___minSwipeDistance_14 = value;
		Il2CppCodeGenWriteBarrier((&___minSwipeDistance_14), value);
	}

	inline static int32_t get_offset_of_swipeLeftEvent_15() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___swipeLeftEvent_15)); }
	inline FsmEvent_t3736299882 * get_swipeLeftEvent_15() const { return ___swipeLeftEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_swipeLeftEvent_15() { return &___swipeLeftEvent_15; }
	inline void set_swipeLeftEvent_15(FsmEvent_t3736299882 * value)
	{
		___swipeLeftEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___swipeLeftEvent_15), value);
	}

	inline static int32_t get_offset_of_swipeRightEvent_16() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___swipeRightEvent_16)); }
	inline FsmEvent_t3736299882 * get_swipeRightEvent_16() const { return ___swipeRightEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_swipeRightEvent_16() { return &___swipeRightEvent_16; }
	inline void set_swipeRightEvent_16(FsmEvent_t3736299882 * value)
	{
		___swipeRightEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___swipeRightEvent_16), value);
	}

	inline static int32_t get_offset_of_swipeUpEvent_17() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___swipeUpEvent_17)); }
	inline FsmEvent_t3736299882 * get_swipeUpEvent_17() const { return ___swipeUpEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_swipeUpEvent_17() { return &___swipeUpEvent_17; }
	inline void set_swipeUpEvent_17(FsmEvent_t3736299882 * value)
	{
		___swipeUpEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___swipeUpEvent_17), value);
	}

	inline static int32_t get_offset_of_swipeDownEvent_18() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___swipeDownEvent_18)); }
	inline FsmEvent_t3736299882 * get_swipeDownEvent_18() const { return ___swipeDownEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_swipeDownEvent_18() { return &___swipeDownEvent_18; }
	inline void set_swipeDownEvent_18(FsmEvent_t3736299882 * value)
	{
		___swipeDownEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___swipeDownEvent_18), value);
	}

	inline static int32_t get_offset_of_screenDiagonalSize_19() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___screenDiagonalSize_19)); }
	inline float get_screenDiagonalSize_19() const { return ___screenDiagonalSize_19; }
	inline float* get_address_of_screenDiagonalSize_19() { return &___screenDiagonalSize_19; }
	inline void set_screenDiagonalSize_19(float value)
	{
		___screenDiagonalSize_19 = value;
	}

	inline static int32_t get_offset_of_minSwipeDistancePixels_20() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___minSwipeDistancePixels_20)); }
	inline float get_minSwipeDistancePixels_20() const { return ___minSwipeDistancePixels_20; }
	inline float* get_address_of_minSwipeDistancePixels_20() { return &___minSwipeDistancePixels_20; }
	inline void set_minSwipeDistancePixels_20(float value)
	{
		___minSwipeDistancePixels_20 = value;
	}

	inline static int32_t get_offset_of_touchStarted_21() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___touchStarted_21)); }
	inline bool get_touchStarted_21() const { return ___touchStarted_21; }
	inline bool* get_address_of_touchStarted_21() { return &___touchStarted_21; }
	inline void set_touchStarted_21(bool value)
	{
		___touchStarted_21 = value;
	}

	inline static int32_t get_offset_of_touchStartPos_22() { return static_cast<int32_t>(offsetof(SwipeGestureEvent_t1778425824, ___touchStartPos_22)); }
	inline Vector2_t2156229523  get_touchStartPos_22() const { return ___touchStartPos_22; }
	inline Vector2_t2156229523 * get_address_of_touchStartPos_22() { return &___touchStartPos_22; }
	inline void set_touchStartPos_22(Vector2_t2156229523  value)
	{
		___touchStartPos_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEGESTUREEVENT_T1778425824_H
#ifndef TOUCHEVENT_T3122665815_H
#define TOUCHEVENT_T3122665815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchEvent
struct  TouchEvent_t3122665815  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchEvent::fingerId
	FsmInt_t874273141 * ___fingerId_14;
	// UnityEngine.TouchPhase HutongGames.PlayMaker.Actions.TouchEvent::touchPhase
	int32_t ___touchPhase_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchEvent::storeFingerId
	FsmInt_t874273141 * ___storeFingerId_17;

public:
	inline static int32_t get_offset_of_fingerId_14() { return static_cast<int32_t>(offsetof(TouchEvent_t3122665815, ___fingerId_14)); }
	inline FsmInt_t874273141 * get_fingerId_14() const { return ___fingerId_14; }
	inline FsmInt_t874273141 ** get_address_of_fingerId_14() { return &___fingerId_14; }
	inline void set_fingerId_14(FsmInt_t874273141 * value)
	{
		___fingerId_14 = value;
		Il2CppCodeGenWriteBarrier((&___fingerId_14), value);
	}

	inline static int32_t get_offset_of_touchPhase_15() { return static_cast<int32_t>(offsetof(TouchEvent_t3122665815, ___touchPhase_15)); }
	inline int32_t get_touchPhase_15() const { return ___touchPhase_15; }
	inline int32_t* get_address_of_touchPhase_15() { return &___touchPhase_15; }
	inline void set_touchPhase_15(int32_t value)
	{
		___touchPhase_15 = value;
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(TouchEvent_t3122665815, ___sendEvent_16)); }
	inline FsmEvent_t3736299882 * get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEvent_t3736299882 * value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_storeFingerId_17() { return static_cast<int32_t>(offsetof(TouchEvent_t3122665815, ___storeFingerId_17)); }
	inline FsmInt_t874273141 * get_storeFingerId_17() const { return ___storeFingerId_17; }
	inline FsmInt_t874273141 ** get_address_of_storeFingerId_17() { return &___storeFingerId_17; }
	inline void set_storeFingerId_17(FsmInt_t874273141 * value)
	{
		___storeFingerId_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeFingerId_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENT_T3122665815_H
#ifndef TOUCHGUIEVENT_T3504819662_H
#define TOUCHGUIEVENT_T3504819662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchGUIEvent
struct  TouchGUIEvent_t3504819662  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TouchGUIEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchGUIEvent::fingerId
	FsmInt_t874273141 * ___fingerId_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchBegan
	FsmEvent_t3736299882 * ___touchBegan_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchMoved
	FsmEvent_t3736299882 * ___touchMoved_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchStationary
	FsmEvent_t3736299882 * ___touchStationary_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchEnded
	FsmEvent_t3736299882 * ___touchEnded_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::touchCanceled
	FsmEvent_t3736299882 * ___touchCanceled_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchGUIEvent::notTouching
	FsmEvent_t3736299882 * ___notTouching_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchGUIEvent::storeFingerId
	FsmInt_t874273141 * ___storeFingerId_22;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchGUIEvent::storeHitPoint
	FsmVector3_t626444517 * ___storeHitPoint_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.TouchGUIEvent::normalizeHitPoint
	FsmBool_t163807967 * ___normalizeHitPoint_24;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchGUIEvent::storeOffset
	FsmVector3_t626444517 * ___storeOffset_25;
	// HutongGames.PlayMaker.Actions.TouchGUIEvent/OffsetOptions HutongGames.PlayMaker.Actions.TouchGUIEvent::relativeTo
	int32_t ___relativeTo_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.TouchGUIEvent::normalizeOffset
	FsmBool_t163807967 * ___normalizeOffset_27;
	// System.Boolean HutongGames.PlayMaker.Actions.TouchGUIEvent::everyFrame
	bool ___everyFrame_28;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.TouchGUIEvent::touchStartPos
	Vector3_t3722313464  ___touchStartPos_29;
	// UnityEngine.GUIElement HutongGames.PlayMaker.Actions.TouchGUIEvent::guiElement
	GUIElement_t3567083079 * ___guiElement_30;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fingerId_15() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___fingerId_15)); }
	inline FsmInt_t874273141 * get_fingerId_15() const { return ___fingerId_15; }
	inline FsmInt_t874273141 ** get_address_of_fingerId_15() { return &___fingerId_15; }
	inline void set_fingerId_15(FsmInt_t874273141 * value)
	{
		___fingerId_15 = value;
		Il2CppCodeGenWriteBarrier((&___fingerId_15), value);
	}

	inline static int32_t get_offset_of_touchBegan_16() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___touchBegan_16)); }
	inline FsmEvent_t3736299882 * get_touchBegan_16() const { return ___touchBegan_16; }
	inline FsmEvent_t3736299882 ** get_address_of_touchBegan_16() { return &___touchBegan_16; }
	inline void set_touchBegan_16(FsmEvent_t3736299882 * value)
	{
		___touchBegan_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchBegan_16), value);
	}

	inline static int32_t get_offset_of_touchMoved_17() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___touchMoved_17)); }
	inline FsmEvent_t3736299882 * get_touchMoved_17() const { return ___touchMoved_17; }
	inline FsmEvent_t3736299882 ** get_address_of_touchMoved_17() { return &___touchMoved_17; }
	inline void set_touchMoved_17(FsmEvent_t3736299882 * value)
	{
		___touchMoved_17 = value;
		Il2CppCodeGenWriteBarrier((&___touchMoved_17), value);
	}

	inline static int32_t get_offset_of_touchStationary_18() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___touchStationary_18)); }
	inline FsmEvent_t3736299882 * get_touchStationary_18() const { return ___touchStationary_18; }
	inline FsmEvent_t3736299882 ** get_address_of_touchStationary_18() { return &___touchStationary_18; }
	inline void set_touchStationary_18(FsmEvent_t3736299882 * value)
	{
		___touchStationary_18 = value;
		Il2CppCodeGenWriteBarrier((&___touchStationary_18), value);
	}

	inline static int32_t get_offset_of_touchEnded_19() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___touchEnded_19)); }
	inline FsmEvent_t3736299882 * get_touchEnded_19() const { return ___touchEnded_19; }
	inline FsmEvent_t3736299882 ** get_address_of_touchEnded_19() { return &___touchEnded_19; }
	inline void set_touchEnded_19(FsmEvent_t3736299882 * value)
	{
		___touchEnded_19 = value;
		Il2CppCodeGenWriteBarrier((&___touchEnded_19), value);
	}

	inline static int32_t get_offset_of_touchCanceled_20() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___touchCanceled_20)); }
	inline FsmEvent_t3736299882 * get_touchCanceled_20() const { return ___touchCanceled_20; }
	inline FsmEvent_t3736299882 ** get_address_of_touchCanceled_20() { return &___touchCanceled_20; }
	inline void set_touchCanceled_20(FsmEvent_t3736299882 * value)
	{
		___touchCanceled_20 = value;
		Il2CppCodeGenWriteBarrier((&___touchCanceled_20), value);
	}

	inline static int32_t get_offset_of_notTouching_21() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___notTouching_21)); }
	inline FsmEvent_t3736299882 * get_notTouching_21() const { return ___notTouching_21; }
	inline FsmEvent_t3736299882 ** get_address_of_notTouching_21() { return &___notTouching_21; }
	inline void set_notTouching_21(FsmEvent_t3736299882 * value)
	{
		___notTouching_21 = value;
		Il2CppCodeGenWriteBarrier((&___notTouching_21), value);
	}

	inline static int32_t get_offset_of_storeFingerId_22() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___storeFingerId_22)); }
	inline FsmInt_t874273141 * get_storeFingerId_22() const { return ___storeFingerId_22; }
	inline FsmInt_t874273141 ** get_address_of_storeFingerId_22() { return &___storeFingerId_22; }
	inline void set_storeFingerId_22(FsmInt_t874273141 * value)
	{
		___storeFingerId_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeFingerId_22), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_23() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___storeHitPoint_23)); }
	inline FsmVector3_t626444517 * get_storeHitPoint_23() const { return ___storeHitPoint_23; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitPoint_23() { return &___storeHitPoint_23; }
	inline void set_storeHitPoint_23(FsmVector3_t626444517 * value)
	{
		___storeHitPoint_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_23), value);
	}

	inline static int32_t get_offset_of_normalizeHitPoint_24() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___normalizeHitPoint_24)); }
	inline FsmBool_t163807967 * get_normalizeHitPoint_24() const { return ___normalizeHitPoint_24; }
	inline FsmBool_t163807967 ** get_address_of_normalizeHitPoint_24() { return &___normalizeHitPoint_24; }
	inline void set_normalizeHitPoint_24(FsmBool_t163807967 * value)
	{
		___normalizeHitPoint_24 = value;
		Il2CppCodeGenWriteBarrier((&___normalizeHitPoint_24), value);
	}

	inline static int32_t get_offset_of_storeOffset_25() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___storeOffset_25)); }
	inline FsmVector3_t626444517 * get_storeOffset_25() const { return ___storeOffset_25; }
	inline FsmVector3_t626444517 ** get_address_of_storeOffset_25() { return &___storeOffset_25; }
	inline void set_storeOffset_25(FsmVector3_t626444517 * value)
	{
		___storeOffset_25 = value;
		Il2CppCodeGenWriteBarrier((&___storeOffset_25), value);
	}

	inline static int32_t get_offset_of_relativeTo_26() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___relativeTo_26)); }
	inline int32_t get_relativeTo_26() const { return ___relativeTo_26; }
	inline int32_t* get_address_of_relativeTo_26() { return &___relativeTo_26; }
	inline void set_relativeTo_26(int32_t value)
	{
		___relativeTo_26 = value;
	}

	inline static int32_t get_offset_of_normalizeOffset_27() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___normalizeOffset_27)); }
	inline FsmBool_t163807967 * get_normalizeOffset_27() const { return ___normalizeOffset_27; }
	inline FsmBool_t163807967 ** get_address_of_normalizeOffset_27() { return &___normalizeOffset_27; }
	inline void set_normalizeOffset_27(FsmBool_t163807967 * value)
	{
		___normalizeOffset_27 = value;
		Il2CppCodeGenWriteBarrier((&___normalizeOffset_27), value);
	}

	inline static int32_t get_offset_of_everyFrame_28() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___everyFrame_28)); }
	inline bool get_everyFrame_28() const { return ___everyFrame_28; }
	inline bool* get_address_of_everyFrame_28() { return &___everyFrame_28; }
	inline void set_everyFrame_28(bool value)
	{
		___everyFrame_28 = value;
	}

	inline static int32_t get_offset_of_touchStartPos_29() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___touchStartPos_29)); }
	inline Vector3_t3722313464  get_touchStartPos_29() const { return ___touchStartPos_29; }
	inline Vector3_t3722313464 * get_address_of_touchStartPos_29() { return &___touchStartPos_29; }
	inline void set_touchStartPos_29(Vector3_t3722313464  value)
	{
		___touchStartPos_29 = value;
	}

	inline static int32_t get_offset_of_guiElement_30() { return static_cast<int32_t>(offsetof(TouchGUIEvent_t3504819662, ___guiElement_30)); }
	inline GUIElement_t3567083079 * get_guiElement_30() const { return ___guiElement_30; }
	inline GUIElement_t3567083079 ** get_address_of_guiElement_30() { return &___guiElement_30; }
	inline void set_guiElement_30(GUIElement_t3567083079 * value)
	{
		___guiElement_30 = value;
		Il2CppCodeGenWriteBarrier((&___guiElement_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHGUIEVENT_T3504819662_H
#ifndef TOUCHOBJECTEVENT_T938568464_H
#define TOUCHOBJECTEVENT_T938568464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchObjectEvent
struct  TouchObjectEvent_t938568464  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TouchObjectEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.TouchObjectEvent::pickDistance
	FsmFloat_t2883254149 * ___pickDistance_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchObjectEvent::fingerId
	FsmInt_t874273141 * ___fingerId_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchBegan
	FsmEvent_t3736299882 * ___touchBegan_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchMoved
	FsmEvent_t3736299882 * ___touchMoved_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchStationary
	FsmEvent_t3736299882 * ___touchStationary_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchEnded
	FsmEvent_t3736299882 * ___touchEnded_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObjectEvent::touchCanceled
	FsmEvent_t3736299882 * ___touchCanceled_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchObjectEvent::storeFingerId
	FsmInt_t874273141 * ___storeFingerId_22;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchObjectEvent::storeHitPoint
	FsmVector3_t626444517 * ___storeHitPoint_23;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TouchObjectEvent::storeHitNormal
	FsmVector3_t626444517 * ___storeHitNormal_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_pickDistance_15() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___pickDistance_15)); }
	inline FsmFloat_t2883254149 * get_pickDistance_15() const { return ___pickDistance_15; }
	inline FsmFloat_t2883254149 ** get_address_of_pickDistance_15() { return &___pickDistance_15; }
	inline void set_pickDistance_15(FsmFloat_t2883254149 * value)
	{
		___pickDistance_15 = value;
		Il2CppCodeGenWriteBarrier((&___pickDistance_15), value);
	}

	inline static int32_t get_offset_of_fingerId_16() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___fingerId_16)); }
	inline FsmInt_t874273141 * get_fingerId_16() const { return ___fingerId_16; }
	inline FsmInt_t874273141 ** get_address_of_fingerId_16() { return &___fingerId_16; }
	inline void set_fingerId_16(FsmInt_t874273141 * value)
	{
		___fingerId_16 = value;
		Il2CppCodeGenWriteBarrier((&___fingerId_16), value);
	}

	inline static int32_t get_offset_of_touchBegan_17() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___touchBegan_17)); }
	inline FsmEvent_t3736299882 * get_touchBegan_17() const { return ___touchBegan_17; }
	inline FsmEvent_t3736299882 ** get_address_of_touchBegan_17() { return &___touchBegan_17; }
	inline void set_touchBegan_17(FsmEvent_t3736299882 * value)
	{
		___touchBegan_17 = value;
		Il2CppCodeGenWriteBarrier((&___touchBegan_17), value);
	}

	inline static int32_t get_offset_of_touchMoved_18() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___touchMoved_18)); }
	inline FsmEvent_t3736299882 * get_touchMoved_18() const { return ___touchMoved_18; }
	inline FsmEvent_t3736299882 ** get_address_of_touchMoved_18() { return &___touchMoved_18; }
	inline void set_touchMoved_18(FsmEvent_t3736299882 * value)
	{
		___touchMoved_18 = value;
		Il2CppCodeGenWriteBarrier((&___touchMoved_18), value);
	}

	inline static int32_t get_offset_of_touchStationary_19() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___touchStationary_19)); }
	inline FsmEvent_t3736299882 * get_touchStationary_19() const { return ___touchStationary_19; }
	inline FsmEvent_t3736299882 ** get_address_of_touchStationary_19() { return &___touchStationary_19; }
	inline void set_touchStationary_19(FsmEvent_t3736299882 * value)
	{
		___touchStationary_19 = value;
		Il2CppCodeGenWriteBarrier((&___touchStationary_19), value);
	}

	inline static int32_t get_offset_of_touchEnded_20() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___touchEnded_20)); }
	inline FsmEvent_t3736299882 * get_touchEnded_20() const { return ___touchEnded_20; }
	inline FsmEvent_t3736299882 ** get_address_of_touchEnded_20() { return &___touchEnded_20; }
	inline void set_touchEnded_20(FsmEvent_t3736299882 * value)
	{
		___touchEnded_20 = value;
		Il2CppCodeGenWriteBarrier((&___touchEnded_20), value);
	}

	inline static int32_t get_offset_of_touchCanceled_21() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___touchCanceled_21)); }
	inline FsmEvent_t3736299882 * get_touchCanceled_21() const { return ___touchCanceled_21; }
	inline FsmEvent_t3736299882 ** get_address_of_touchCanceled_21() { return &___touchCanceled_21; }
	inline void set_touchCanceled_21(FsmEvent_t3736299882 * value)
	{
		___touchCanceled_21 = value;
		Il2CppCodeGenWriteBarrier((&___touchCanceled_21), value);
	}

	inline static int32_t get_offset_of_storeFingerId_22() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___storeFingerId_22)); }
	inline FsmInt_t874273141 * get_storeFingerId_22() const { return ___storeFingerId_22; }
	inline FsmInt_t874273141 ** get_address_of_storeFingerId_22() { return &___storeFingerId_22; }
	inline void set_storeFingerId_22(FsmInt_t874273141 * value)
	{
		___storeFingerId_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeFingerId_22), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_23() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___storeHitPoint_23)); }
	inline FsmVector3_t626444517 * get_storeHitPoint_23() const { return ___storeHitPoint_23; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitPoint_23() { return &___storeHitPoint_23; }
	inline void set_storeHitPoint_23(FsmVector3_t626444517 * value)
	{
		___storeHitPoint_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_23), value);
	}

	inline static int32_t get_offset_of_storeHitNormal_24() { return static_cast<int32_t>(offsetof(TouchObjectEvent_t938568464, ___storeHitNormal_24)); }
	inline FsmVector3_t626444517 * get_storeHitNormal_24() const { return ___storeHitNormal_24; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitNormal_24() { return &___storeHitNormal_24; }
	inline void set_storeHitNormal_24(FsmVector3_t626444517 * value)
	{
		___storeHitNormal_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitNormal_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHOBJECTEVENT_T938568464_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef FUNCTION_T3898704989_H
#define FUNCTION_T3898704989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.EasingFunction/Function
struct  Function_t3898704989  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTION_T3898704989_H
#ifndef BLINK_T2426860219_H
#define BLINK_T2426860219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Blink
struct  Blink_t2426860219  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Blink::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Blink::timeOff
	FsmFloat_t2883254149 * ___timeOff_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Blink::timeOn
	FsmFloat_t2883254149 * ___timeOn_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Blink::startOn
	FsmBool_t163807967 * ___startOn_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Blink::rendererOnly
	bool ___rendererOnly_20;
	// System.Boolean HutongGames.PlayMaker.Actions.Blink::realTime
	bool ___realTime_21;
	// System.Single HutongGames.PlayMaker.Actions.Blink::startTime
	float ___startTime_22;
	// System.Single HutongGames.PlayMaker.Actions.Blink::timer
	float ___timer_23;
	// System.Boolean HutongGames.PlayMaker.Actions.Blink::blinkOn
	bool ___blinkOn_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_timeOff_17() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___timeOff_17)); }
	inline FsmFloat_t2883254149 * get_timeOff_17() const { return ___timeOff_17; }
	inline FsmFloat_t2883254149 ** get_address_of_timeOff_17() { return &___timeOff_17; }
	inline void set_timeOff_17(FsmFloat_t2883254149 * value)
	{
		___timeOff_17 = value;
		Il2CppCodeGenWriteBarrier((&___timeOff_17), value);
	}

	inline static int32_t get_offset_of_timeOn_18() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___timeOn_18)); }
	inline FsmFloat_t2883254149 * get_timeOn_18() const { return ___timeOn_18; }
	inline FsmFloat_t2883254149 ** get_address_of_timeOn_18() { return &___timeOn_18; }
	inline void set_timeOn_18(FsmFloat_t2883254149 * value)
	{
		___timeOn_18 = value;
		Il2CppCodeGenWriteBarrier((&___timeOn_18), value);
	}

	inline static int32_t get_offset_of_startOn_19() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___startOn_19)); }
	inline FsmBool_t163807967 * get_startOn_19() const { return ___startOn_19; }
	inline FsmBool_t163807967 ** get_address_of_startOn_19() { return &___startOn_19; }
	inline void set_startOn_19(FsmBool_t163807967 * value)
	{
		___startOn_19 = value;
		Il2CppCodeGenWriteBarrier((&___startOn_19), value);
	}

	inline static int32_t get_offset_of_rendererOnly_20() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___rendererOnly_20)); }
	inline bool get_rendererOnly_20() const { return ___rendererOnly_20; }
	inline bool* get_address_of_rendererOnly_20() { return &___rendererOnly_20; }
	inline void set_rendererOnly_20(bool value)
	{
		___rendererOnly_20 = value;
	}

	inline static int32_t get_offset_of_realTime_21() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___realTime_21)); }
	inline bool get_realTime_21() const { return ___realTime_21; }
	inline bool* get_address_of_realTime_21() { return &___realTime_21; }
	inline void set_realTime_21(bool value)
	{
		___realTime_21 = value;
	}

	inline static int32_t get_offset_of_startTime_22() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___startTime_22)); }
	inline float get_startTime_22() const { return ___startTime_22; }
	inline float* get_address_of_startTime_22() { return &___startTime_22; }
	inline void set_startTime_22(float value)
	{
		___startTime_22 = value;
	}

	inline static int32_t get_offset_of_timer_23() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___timer_23)); }
	inline float get_timer_23() const { return ___timer_23; }
	inline float* get_address_of_timer_23() { return &___timer_23; }
	inline void set_timer_23(float value)
	{
		___timer_23 = value;
	}

	inline static int32_t get_offset_of_blinkOn_24() { return static_cast<int32_t>(offsetof(Blink_t2426860219, ___blinkOn_24)); }
	inline bool get_blinkOn_24() const { return ___blinkOn_24; }
	inline bool* get_address_of_blinkOn_24() { return &___blinkOn_24; }
	inline void set_blinkOn_24(bool value)
	{
		___blinkOn_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLINK_T2426860219_H
#ifndef FLICKER_T18802239_H
#define FLICKER_T18802239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Flicker
struct  Flicker_t18802239  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Flicker::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Flicker::frequency
	FsmFloat_t2883254149 * ___frequency_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Flicker::amountOn
	FsmFloat_t2883254149 * ___amountOn_18;
	// System.Boolean HutongGames.PlayMaker.Actions.Flicker::rendererOnly
	bool ___rendererOnly_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Flicker::realTime
	bool ___realTime_20;
	// System.Single HutongGames.PlayMaker.Actions.Flicker::startTime
	float ___startTime_21;
	// System.Single HutongGames.PlayMaker.Actions.Flicker::timer
	float ___timer_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_frequency_17() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___frequency_17)); }
	inline FsmFloat_t2883254149 * get_frequency_17() const { return ___frequency_17; }
	inline FsmFloat_t2883254149 ** get_address_of_frequency_17() { return &___frequency_17; }
	inline void set_frequency_17(FsmFloat_t2883254149 * value)
	{
		___frequency_17 = value;
		Il2CppCodeGenWriteBarrier((&___frequency_17), value);
	}

	inline static int32_t get_offset_of_amountOn_18() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___amountOn_18)); }
	inline FsmFloat_t2883254149 * get_amountOn_18() const { return ___amountOn_18; }
	inline FsmFloat_t2883254149 ** get_address_of_amountOn_18() { return &___amountOn_18; }
	inline void set_amountOn_18(FsmFloat_t2883254149 * value)
	{
		___amountOn_18 = value;
		Il2CppCodeGenWriteBarrier((&___amountOn_18), value);
	}

	inline static int32_t get_offset_of_rendererOnly_19() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___rendererOnly_19)); }
	inline bool get_rendererOnly_19() const { return ___rendererOnly_19; }
	inline bool* get_address_of_rendererOnly_19() { return &___rendererOnly_19; }
	inline void set_rendererOnly_19(bool value)
	{
		___rendererOnly_19 = value;
	}

	inline static int32_t get_offset_of_realTime_20() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___realTime_20)); }
	inline bool get_realTime_20() const { return ___realTime_20; }
	inline bool* get_address_of_realTime_20() { return &___realTime_20; }
	inline void set_realTime_20(bool value)
	{
		___realTime_20 = value;
	}

	inline static int32_t get_offset_of_startTime_21() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___startTime_21)); }
	inline float get_startTime_21() const { return ___startTime_21; }
	inline float* get_address_of_startTime_21() { return &___startTime_21; }
	inline void set_startTime_21(float value)
	{
		___startTime_21 = value;
	}

	inline static int32_t get_offset_of_timer_22() { return static_cast<int32_t>(offsetof(Flicker_t18802239, ___timer_22)); }
	inline float get_timer_22() const { return ___timer_22; }
	inline float* get_address_of_timer_22() { return &___timer_22; }
	inline void set_timer_22(float value)
	{
		___timer_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLICKER_T18802239_H
#ifndef GUICONTENTACTION_T3694365960_H
#define GUICONTENTACTION_T3694365960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIContentAction
struct  GUIContentAction_t3694365960  : public GUIAction_t3750965478
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUIContentAction::image
	FsmTexture_t1738787045 * ___image_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIContentAction::text
	FsmString_t1785915204 * ___text_22;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIContentAction::tooltip
	FsmString_t1785915204 * ___tooltip_23;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIContentAction::style
	FsmString_t1785915204 * ___style_24;
	// UnityEngine.GUIContent HutongGames.PlayMaker.Actions.GUIContentAction::content
	GUIContent_t3050628031 * ___content_25;

public:
	inline static int32_t get_offset_of_image_21() { return static_cast<int32_t>(offsetof(GUIContentAction_t3694365960, ___image_21)); }
	inline FsmTexture_t1738787045 * get_image_21() const { return ___image_21; }
	inline FsmTexture_t1738787045 ** get_address_of_image_21() { return &___image_21; }
	inline void set_image_21(FsmTexture_t1738787045 * value)
	{
		___image_21 = value;
		Il2CppCodeGenWriteBarrier((&___image_21), value);
	}

	inline static int32_t get_offset_of_text_22() { return static_cast<int32_t>(offsetof(GUIContentAction_t3694365960, ___text_22)); }
	inline FsmString_t1785915204 * get_text_22() const { return ___text_22; }
	inline FsmString_t1785915204 ** get_address_of_text_22() { return &___text_22; }
	inline void set_text_22(FsmString_t1785915204 * value)
	{
		___text_22 = value;
		Il2CppCodeGenWriteBarrier((&___text_22), value);
	}

	inline static int32_t get_offset_of_tooltip_23() { return static_cast<int32_t>(offsetof(GUIContentAction_t3694365960, ___tooltip_23)); }
	inline FsmString_t1785915204 * get_tooltip_23() const { return ___tooltip_23; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_23() { return &___tooltip_23; }
	inline void set_tooltip_23(FsmString_t1785915204 * value)
	{
		___tooltip_23 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_23), value);
	}

	inline static int32_t get_offset_of_style_24() { return static_cast<int32_t>(offsetof(GUIContentAction_t3694365960, ___style_24)); }
	inline FsmString_t1785915204 * get_style_24() const { return ___style_24; }
	inline FsmString_t1785915204 ** get_address_of_style_24() { return &___style_24; }
	inline void set_style_24(FsmString_t1785915204 * value)
	{
		___style_24 = value;
		Il2CppCodeGenWriteBarrier((&___style_24), value);
	}

	inline static int32_t get_offset_of_content_25() { return static_cast<int32_t>(offsetof(GUIContentAction_t3694365960, ___content_25)); }
	inline GUIContent_t3050628031 * get_content_25() const { return ___content_25; }
	inline GUIContent_t3050628031 ** get_address_of_content_25() { return &___content_25; }
	inline void set_content_25(GUIContent_t3050628031 * value)
	{
		___content_25 = value;
		Il2CppCodeGenWriteBarrier((&___content_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUICONTENTACTION_T3694365960_H
#ifndef GUIHORIZONTALSLIDER_T1379811325_H
#define GUIHORIZONTALSLIDER_T1379811325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIHorizontalSlider
struct  GUIHorizontalSlider_t1379811325  : public GUIAction_t3750965478
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIHorizontalSlider::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIHorizontalSlider::leftValue
	FsmFloat_t2883254149 * ___leftValue_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIHorizontalSlider::rightValue
	FsmFloat_t2883254149 * ___rightValue_23;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIHorizontalSlider::sliderStyle
	FsmString_t1785915204 * ___sliderStyle_24;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIHorizontalSlider::thumbStyle
	FsmString_t1785915204 * ___thumbStyle_25;

public:
	inline static int32_t get_offset_of_floatVariable_21() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t1379811325, ___floatVariable_21)); }
	inline FsmFloat_t2883254149 * get_floatVariable_21() const { return ___floatVariable_21; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_21() { return &___floatVariable_21; }
	inline void set_floatVariable_21(FsmFloat_t2883254149 * value)
	{
		___floatVariable_21 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_21), value);
	}

	inline static int32_t get_offset_of_leftValue_22() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t1379811325, ___leftValue_22)); }
	inline FsmFloat_t2883254149 * get_leftValue_22() const { return ___leftValue_22; }
	inline FsmFloat_t2883254149 ** get_address_of_leftValue_22() { return &___leftValue_22; }
	inline void set_leftValue_22(FsmFloat_t2883254149 * value)
	{
		___leftValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___leftValue_22), value);
	}

	inline static int32_t get_offset_of_rightValue_23() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t1379811325, ___rightValue_23)); }
	inline FsmFloat_t2883254149 * get_rightValue_23() const { return ___rightValue_23; }
	inline FsmFloat_t2883254149 ** get_address_of_rightValue_23() { return &___rightValue_23; }
	inline void set_rightValue_23(FsmFloat_t2883254149 * value)
	{
		___rightValue_23 = value;
		Il2CppCodeGenWriteBarrier((&___rightValue_23), value);
	}

	inline static int32_t get_offset_of_sliderStyle_24() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t1379811325, ___sliderStyle_24)); }
	inline FsmString_t1785915204 * get_sliderStyle_24() const { return ___sliderStyle_24; }
	inline FsmString_t1785915204 ** get_address_of_sliderStyle_24() { return &___sliderStyle_24; }
	inline void set_sliderStyle_24(FsmString_t1785915204 * value)
	{
		___sliderStyle_24 = value;
		Il2CppCodeGenWriteBarrier((&___sliderStyle_24), value);
	}

	inline static int32_t get_offset_of_thumbStyle_25() { return static_cast<int32_t>(offsetof(GUIHorizontalSlider_t1379811325, ___thumbStyle_25)); }
	inline FsmString_t1785915204 * get_thumbStyle_25() const { return ___thumbStyle_25; }
	inline FsmString_t1785915204 ** get_address_of_thumbStyle_25() { return &___thumbStyle_25; }
	inline void set_thumbStyle_25(FsmString_t1785915204 * value)
	{
		___thumbStyle_25 = value;
		Il2CppCodeGenWriteBarrier((&___thumbStyle_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIHORIZONTALSLIDER_T1379811325_H
#ifndef GUILAYOUTBEGINHORIZONTAL_T2612542000_H
#define GUILAYOUTBEGINHORIZONTAL_T2612542000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal
struct  GUILayoutBeginHorizontal_t2612542000  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::image
	FsmTexture_t1738787045 * ___image_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::text
	FsmString_t1785915204 * ___text_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::tooltip
	FsmString_t1785915204 * ___tooltip_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::style
	FsmString_t1785915204 * ___style_19;

public:
	inline static int32_t get_offset_of_image_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2612542000, ___image_16)); }
	inline FsmTexture_t1738787045 * get_image_16() const { return ___image_16; }
	inline FsmTexture_t1738787045 ** get_address_of_image_16() { return &___image_16; }
	inline void set_image_16(FsmTexture_t1738787045 * value)
	{
		___image_16 = value;
		Il2CppCodeGenWriteBarrier((&___image_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2612542000, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_tooltip_18() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2612542000, ___tooltip_18)); }
	inline FsmString_t1785915204 * get_tooltip_18() const { return ___tooltip_18; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_18() { return &___tooltip_18; }
	inline void set_tooltip_18(FsmString_t1785915204 * value)
	{
		___tooltip_18 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_18), value);
	}

	inline static int32_t get_offset_of_style_19() { return static_cast<int32_t>(offsetof(GUILayoutBeginHorizontal_t2612542000, ___style_19)); }
	inline FsmString_t1785915204 * get_style_19() const { return ___style_19; }
	inline FsmString_t1785915204 ** get_address_of_style_19() { return &___style_19; }
	inline void set_style_19(FsmString_t1785915204 * value)
	{
		___style_19 = value;
		Il2CppCodeGenWriteBarrier((&___style_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBEGINHORIZONTAL_T2612542000_H
#ifndef GUILAYOUTBEGINSCROLLVIEW_T2032397505_H
#define GUILAYOUTBEGINSCROLLVIEW_T2032397505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
struct  GUILayoutBeginScrollView_t2032397505  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::scrollPosition
	FsmVector2_t2965096677 * ___scrollPosition_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::horizontalScrollbar
	FsmBool_t163807967 * ___horizontalScrollbar_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::verticalScrollbar
	FsmBool_t163807967 * ___verticalScrollbar_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::useCustomStyle
	FsmBool_t163807967 * ___useCustomStyle_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::horizontalStyle
	FsmString_t1785915204 * ___horizontalStyle_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::verticalStyle
	FsmString_t1785915204 * ___verticalStyle_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::backgroundStyle
	FsmString_t1785915204 * ___backgroundStyle_22;

public:
	inline static int32_t get_offset_of_scrollPosition_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___scrollPosition_16)); }
	inline FsmVector2_t2965096677 * get_scrollPosition_16() const { return ___scrollPosition_16; }
	inline FsmVector2_t2965096677 ** get_address_of_scrollPosition_16() { return &___scrollPosition_16; }
	inline void set_scrollPosition_16(FsmVector2_t2965096677 * value)
	{
		___scrollPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___scrollPosition_16), value);
	}

	inline static int32_t get_offset_of_horizontalScrollbar_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___horizontalScrollbar_17)); }
	inline FsmBool_t163807967 * get_horizontalScrollbar_17() const { return ___horizontalScrollbar_17; }
	inline FsmBool_t163807967 ** get_address_of_horizontalScrollbar_17() { return &___horizontalScrollbar_17; }
	inline void set_horizontalScrollbar_17(FsmBool_t163807967 * value)
	{
		___horizontalScrollbar_17 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalScrollbar_17), value);
	}

	inline static int32_t get_offset_of_verticalScrollbar_18() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___verticalScrollbar_18)); }
	inline FsmBool_t163807967 * get_verticalScrollbar_18() const { return ___verticalScrollbar_18; }
	inline FsmBool_t163807967 ** get_address_of_verticalScrollbar_18() { return &___verticalScrollbar_18; }
	inline void set_verticalScrollbar_18(FsmBool_t163807967 * value)
	{
		___verticalScrollbar_18 = value;
		Il2CppCodeGenWriteBarrier((&___verticalScrollbar_18), value);
	}

	inline static int32_t get_offset_of_useCustomStyle_19() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___useCustomStyle_19)); }
	inline FsmBool_t163807967 * get_useCustomStyle_19() const { return ___useCustomStyle_19; }
	inline FsmBool_t163807967 ** get_address_of_useCustomStyle_19() { return &___useCustomStyle_19; }
	inline void set_useCustomStyle_19(FsmBool_t163807967 * value)
	{
		___useCustomStyle_19 = value;
		Il2CppCodeGenWriteBarrier((&___useCustomStyle_19), value);
	}

	inline static int32_t get_offset_of_horizontalStyle_20() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___horizontalStyle_20)); }
	inline FsmString_t1785915204 * get_horizontalStyle_20() const { return ___horizontalStyle_20; }
	inline FsmString_t1785915204 ** get_address_of_horizontalStyle_20() { return &___horizontalStyle_20; }
	inline void set_horizontalStyle_20(FsmString_t1785915204 * value)
	{
		___horizontalStyle_20 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalStyle_20), value);
	}

	inline static int32_t get_offset_of_verticalStyle_21() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___verticalStyle_21)); }
	inline FsmString_t1785915204 * get_verticalStyle_21() const { return ___verticalStyle_21; }
	inline FsmString_t1785915204 ** get_address_of_verticalStyle_21() { return &___verticalStyle_21; }
	inline void set_verticalStyle_21(FsmString_t1785915204 * value)
	{
		___verticalStyle_21 = value;
		Il2CppCodeGenWriteBarrier((&___verticalStyle_21), value);
	}

	inline static int32_t get_offset_of_backgroundStyle_22() { return static_cast<int32_t>(offsetof(GUILayoutBeginScrollView_t2032397505, ___backgroundStyle_22)); }
	inline FsmString_t1785915204 * get_backgroundStyle_22() const { return ___backgroundStyle_22; }
	inline FsmString_t1785915204 ** get_address_of_backgroundStyle_22() { return &___backgroundStyle_22; }
	inline void set_backgroundStyle_22(FsmString_t1785915204 * value)
	{
		___backgroundStyle_22 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundStyle_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBEGINSCROLLVIEW_T2032397505_H
#ifndef GUILAYOUTBEGINVERTICAL_T1479530004_H
#define GUILAYOUTBEGINVERTICAL_T1479530004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginVertical
struct  GUILayoutBeginVertical_t1479530004  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::image
	FsmTexture_t1738787045 * ___image_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::text
	FsmString_t1785915204 * ___text_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::tooltip
	FsmString_t1785915204 * ___tooltip_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginVertical::style
	FsmString_t1785915204 * ___style_19;

public:
	inline static int32_t get_offset_of_image_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginVertical_t1479530004, ___image_16)); }
	inline FsmTexture_t1738787045 * get_image_16() const { return ___image_16; }
	inline FsmTexture_t1738787045 ** get_address_of_image_16() { return &___image_16; }
	inline void set_image_16(FsmTexture_t1738787045 * value)
	{
		___image_16 = value;
		Il2CppCodeGenWriteBarrier((&___image_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginVertical_t1479530004, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_tooltip_18() { return static_cast<int32_t>(offsetof(GUILayoutBeginVertical_t1479530004, ___tooltip_18)); }
	inline FsmString_t1785915204 * get_tooltip_18() const { return ___tooltip_18; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_18() { return &___tooltip_18; }
	inline void set_tooltip_18(FsmString_t1785915204 * value)
	{
		___tooltip_18 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_18), value);
	}

	inline static int32_t get_offset_of_style_19() { return static_cast<int32_t>(offsetof(GUILayoutBeginVertical_t1479530004, ___style_19)); }
	inline FsmString_t1785915204 * get_style_19() const { return ___style_19; }
	inline FsmString_t1785915204 ** get_address_of_style_19() { return &___style_19; }
	inline void set_style_19(FsmString_t1785915204 * value)
	{
		___style_19 = value;
		Il2CppCodeGenWriteBarrier((&___style_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBEGINVERTICAL_T1479530004_H
#ifndef GUILAYOUTBOX_T2785960633_H
#define GUILAYOUTBOX_T2785960633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBox
struct  GUILayoutBox_t2785960633  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutBox::image
	FsmTexture_t1738787045 * ___image_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBox::text
	FsmString_t1785915204 * ___text_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBox::tooltip
	FsmString_t1785915204 * ___tooltip_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBox::style
	FsmString_t1785915204 * ___style_19;

public:
	inline static int32_t get_offset_of_image_16() { return static_cast<int32_t>(offsetof(GUILayoutBox_t2785960633, ___image_16)); }
	inline FsmTexture_t1738787045 * get_image_16() const { return ___image_16; }
	inline FsmTexture_t1738787045 ** get_address_of_image_16() { return &___image_16; }
	inline void set_image_16(FsmTexture_t1738787045 * value)
	{
		___image_16 = value;
		Il2CppCodeGenWriteBarrier((&___image_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(GUILayoutBox_t2785960633, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_tooltip_18() { return static_cast<int32_t>(offsetof(GUILayoutBox_t2785960633, ___tooltip_18)); }
	inline FsmString_t1785915204 * get_tooltip_18() const { return ___tooltip_18; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_18() { return &___tooltip_18; }
	inline void set_tooltip_18(FsmString_t1785915204 * value)
	{
		___tooltip_18 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_18), value);
	}

	inline static int32_t get_offset_of_style_19() { return static_cast<int32_t>(offsetof(GUILayoutBox_t2785960633, ___style_19)); }
	inline FsmString_t1785915204 * get_style_19() const { return ___style_19; }
	inline FsmString_t1785915204 ** get_address_of_style_19() { return &___style_19; }
	inline void set_style_19(FsmString_t1785915204 * value)
	{
		___style_19 = value;
		Il2CppCodeGenWriteBarrier((&___style_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBOX_T2785960633_H
#ifndef GUILAYOUTBUTTON_T379382150_H
#define GUILAYOUTBUTTON_T379382150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutButton
struct  GUILayoutButton_t379382150  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutButton::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutButton::storeButtonState
	FsmBool_t163807967 * ___storeButtonState_17;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutButton::image
	FsmTexture_t1738787045 * ___image_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutButton::text
	FsmString_t1785915204 * ___text_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutButton::tooltip
	FsmString_t1785915204 * ___tooltip_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutButton::style
	FsmString_t1785915204 * ___style_21;

public:
	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutButton_t379382150, ___sendEvent_16)); }
	inline FsmEvent_t3736299882 * get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEvent_t3736299882 * value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_storeButtonState_17() { return static_cast<int32_t>(offsetof(GUILayoutButton_t379382150, ___storeButtonState_17)); }
	inline FsmBool_t163807967 * get_storeButtonState_17() const { return ___storeButtonState_17; }
	inline FsmBool_t163807967 ** get_address_of_storeButtonState_17() { return &___storeButtonState_17; }
	inline void set_storeButtonState_17(FsmBool_t163807967 * value)
	{
		___storeButtonState_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeButtonState_17), value);
	}

	inline static int32_t get_offset_of_image_18() { return static_cast<int32_t>(offsetof(GUILayoutButton_t379382150, ___image_18)); }
	inline FsmTexture_t1738787045 * get_image_18() const { return ___image_18; }
	inline FsmTexture_t1738787045 ** get_address_of_image_18() { return &___image_18; }
	inline void set_image_18(FsmTexture_t1738787045 * value)
	{
		___image_18 = value;
		Il2CppCodeGenWriteBarrier((&___image_18), value);
	}

	inline static int32_t get_offset_of_text_19() { return static_cast<int32_t>(offsetof(GUILayoutButton_t379382150, ___text_19)); }
	inline FsmString_t1785915204 * get_text_19() const { return ___text_19; }
	inline FsmString_t1785915204 ** get_address_of_text_19() { return &___text_19; }
	inline void set_text_19(FsmString_t1785915204 * value)
	{
		___text_19 = value;
		Il2CppCodeGenWriteBarrier((&___text_19), value);
	}

	inline static int32_t get_offset_of_tooltip_20() { return static_cast<int32_t>(offsetof(GUILayoutButton_t379382150, ___tooltip_20)); }
	inline FsmString_t1785915204 * get_tooltip_20() const { return ___tooltip_20; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_20() { return &___tooltip_20; }
	inline void set_tooltip_20(FsmString_t1785915204 * value)
	{
		___tooltip_20 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_20), value);
	}

	inline static int32_t get_offset_of_style_21() { return static_cast<int32_t>(offsetof(GUILayoutButton_t379382150, ___style_21)); }
	inline FsmString_t1785915204 * get_style_21() const { return ___style_21; }
	inline FsmString_t1785915204 ** get_address_of_style_21() { return &___style_21; }
	inline void set_style_21(FsmString_t1785915204 * value)
	{
		___style_21 = value;
		Il2CppCodeGenWriteBarrier((&___style_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTBUTTON_T379382150_H
#ifndef GUILAYOUTCONFIRMPASSWORDFIELD_T3437180288_H
#define GUILAYOUTCONFIRMPASSWORDFIELD_T3437180288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField
struct  GUILayoutConfirmPasswordField_t3437180288  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::text
	FsmString_t1785915204 * ___text_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::maxLength
	FsmInt_t874273141 * ___maxLength_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::style
	FsmString_t1785915204 * ___style_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::mask
	FsmString_t1785915204 * ___mask_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::confirm
	FsmBool_t163807967 * ___confirm_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::password
	FsmString_t1785915204 * ___password_22;

public:
	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___text_16)); }
	inline FsmString_t1785915204 * get_text_16() const { return ___text_16; }
	inline FsmString_t1785915204 ** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(FsmString_t1785915204 * value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}

	inline static int32_t get_offset_of_maxLength_17() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___maxLength_17)); }
	inline FsmInt_t874273141 * get_maxLength_17() const { return ___maxLength_17; }
	inline FsmInt_t874273141 ** get_address_of_maxLength_17() { return &___maxLength_17; }
	inline void set_maxLength_17(FsmInt_t874273141 * value)
	{
		___maxLength_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxLength_17), value);
	}

	inline static int32_t get_offset_of_style_18() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___style_18)); }
	inline FsmString_t1785915204 * get_style_18() const { return ___style_18; }
	inline FsmString_t1785915204 ** get_address_of_style_18() { return &___style_18; }
	inline void set_style_18(FsmString_t1785915204 * value)
	{
		___style_18 = value;
		Il2CppCodeGenWriteBarrier((&___style_18), value);
	}

	inline static int32_t get_offset_of_changedEvent_19() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___changedEvent_19)); }
	inline FsmEvent_t3736299882 * get_changedEvent_19() const { return ___changedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_19() { return &___changedEvent_19; }
	inline void set_changedEvent_19(FsmEvent_t3736299882 * value)
	{
		___changedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_19), value);
	}

	inline static int32_t get_offset_of_mask_20() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___mask_20)); }
	inline FsmString_t1785915204 * get_mask_20() const { return ___mask_20; }
	inline FsmString_t1785915204 ** get_address_of_mask_20() { return &___mask_20; }
	inline void set_mask_20(FsmString_t1785915204 * value)
	{
		___mask_20 = value;
		Il2CppCodeGenWriteBarrier((&___mask_20), value);
	}

	inline static int32_t get_offset_of_confirm_21() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___confirm_21)); }
	inline FsmBool_t163807967 * get_confirm_21() const { return ___confirm_21; }
	inline FsmBool_t163807967 ** get_address_of_confirm_21() { return &___confirm_21; }
	inline void set_confirm_21(FsmBool_t163807967 * value)
	{
		___confirm_21 = value;
		Il2CppCodeGenWriteBarrier((&___confirm_21), value);
	}

	inline static int32_t get_offset_of_password_22() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3437180288, ___password_22)); }
	inline FsmString_t1785915204 * get_password_22() const { return ___password_22; }
	inline FsmString_t1785915204 ** get_address_of_password_22() { return &___password_22; }
	inline void set_password_22(FsmString_t1785915204 * value)
	{
		___password_22 = value;
		Il2CppCodeGenWriteBarrier((&___password_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTCONFIRMPASSWORDFIELD_T3437180288_H
#ifndef GUILAYOUTEMAILFIELD_T3678593203_H
#define GUILAYOUTEMAILFIELD_T3678593203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEmailField
struct  GUILayoutEmailField_t3678593203  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutEmailField::text
	FsmString_t1785915204 * ___text_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutEmailField::maxLength
	FsmInt_t874273141 * ___maxLength_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutEmailField::style
	FsmString_t1785915204 * ___style_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutEmailField::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutEmailField::valid
	FsmBool_t163807967 * ___valid_20;

public:
	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t3678593203, ___text_16)); }
	inline FsmString_t1785915204 * get_text_16() const { return ___text_16; }
	inline FsmString_t1785915204 ** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(FsmString_t1785915204 * value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}

	inline static int32_t get_offset_of_maxLength_17() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t3678593203, ___maxLength_17)); }
	inline FsmInt_t874273141 * get_maxLength_17() const { return ___maxLength_17; }
	inline FsmInt_t874273141 ** get_address_of_maxLength_17() { return &___maxLength_17; }
	inline void set_maxLength_17(FsmInt_t874273141 * value)
	{
		___maxLength_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxLength_17), value);
	}

	inline static int32_t get_offset_of_style_18() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t3678593203, ___style_18)); }
	inline FsmString_t1785915204 * get_style_18() const { return ___style_18; }
	inline FsmString_t1785915204 ** get_address_of_style_18() { return &___style_18; }
	inline void set_style_18(FsmString_t1785915204 * value)
	{
		___style_18 = value;
		Il2CppCodeGenWriteBarrier((&___style_18), value);
	}

	inline static int32_t get_offset_of_changedEvent_19() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t3678593203, ___changedEvent_19)); }
	inline FsmEvent_t3736299882 * get_changedEvent_19() const { return ___changedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_19() { return &___changedEvent_19; }
	inline void set_changedEvent_19(FsmEvent_t3736299882 * value)
	{
		___changedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_19), value);
	}

	inline static int32_t get_offset_of_valid_20() { return static_cast<int32_t>(offsetof(GUILayoutEmailField_t3678593203, ___valid_20)); }
	inline FsmBool_t163807967 * get_valid_20() const { return ___valid_20; }
	inline FsmBool_t163807967 ** get_address_of_valid_20() { return &___valid_20; }
	inline void set_valid_20(FsmBool_t163807967 * value)
	{
		___valid_20 = value;
		Il2CppCodeGenWriteBarrier((&___valid_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTEMAILFIELD_T3678593203_H
#ifndef GUIVERTICALSLIDER_T3464442948_H
#define GUIVERTICALSLIDER_T3464442948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIVerticalSlider
struct  GUIVerticalSlider_t3464442948  : public GUIAction_t3750965478
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIVerticalSlider::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIVerticalSlider::topValue
	FsmFloat_t2883254149 * ___topValue_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUIVerticalSlider::bottomValue
	FsmFloat_t2883254149 * ___bottomValue_23;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIVerticalSlider::sliderStyle
	FsmString_t1785915204 * ___sliderStyle_24;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUIVerticalSlider::thumbStyle
	FsmString_t1785915204 * ___thumbStyle_25;

public:
	inline static int32_t get_offset_of_floatVariable_21() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t3464442948, ___floatVariable_21)); }
	inline FsmFloat_t2883254149 * get_floatVariable_21() const { return ___floatVariable_21; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_21() { return &___floatVariable_21; }
	inline void set_floatVariable_21(FsmFloat_t2883254149 * value)
	{
		___floatVariable_21 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_21), value);
	}

	inline static int32_t get_offset_of_topValue_22() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t3464442948, ___topValue_22)); }
	inline FsmFloat_t2883254149 * get_topValue_22() const { return ___topValue_22; }
	inline FsmFloat_t2883254149 ** get_address_of_topValue_22() { return &___topValue_22; }
	inline void set_topValue_22(FsmFloat_t2883254149 * value)
	{
		___topValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___topValue_22), value);
	}

	inline static int32_t get_offset_of_bottomValue_23() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t3464442948, ___bottomValue_23)); }
	inline FsmFloat_t2883254149 * get_bottomValue_23() const { return ___bottomValue_23; }
	inline FsmFloat_t2883254149 ** get_address_of_bottomValue_23() { return &___bottomValue_23; }
	inline void set_bottomValue_23(FsmFloat_t2883254149 * value)
	{
		___bottomValue_23 = value;
		Il2CppCodeGenWriteBarrier((&___bottomValue_23), value);
	}

	inline static int32_t get_offset_of_sliderStyle_24() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t3464442948, ___sliderStyle_24)); }
	inline FsmString_t1785915204 * get_sliderStyle_24() const { return ___sliderStyle_24; }
	inline FsmString_t1785915204 ** get_address_of_sliderStyle_24() { return &___sliderStyle_24; }
	inline void set_sliderStyle_24(FsmString_t1785915204 * value)
	{
		___sliderStyle_24 = value;
		Il2CppCodeGenWriteBarrier((&___sliderStyle_24), value);
	}

	inline static int32_t get_offset_of_thumbStyle_25() { return static_cast<int32_t>(offsetof(GUIVerticalSlider_t3464442948, ___thumbStyle_25)); }
	inline FsmString_t1785915204 * get_thumbStyle_25() const { return ___thumbStyle_25; }
	inline FsmString_t1785915204 ** get_address_of_thumbStyle_25() { return &___thumbStyle_25; }
	inline void set_thumbStyle_25(FsmString_t1785915204 * value)
	{
		___thumbStyle_25 = value;
		Il2CppCodeGenWriteBarrier((&___thumbStyle_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIVERTICALSLIDER_T3464442948_H
#ifndef SETGUITEXT_T3459324574_H
#define SETGUITEXT_T3459324574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUIText
struct  SetGUIText_t3459324574  : public ComponentAction_1_t3279520548
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetGUIText::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetGUIText::text
	FsmString_t1785915204 * ___text_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGUIText::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetGUIText_t3459324574, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(SetGUIText_t3459324574, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetGUIText_t3459324574, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUITEXT_T3459324574_H
#ifndef SETGUITEXTURE_T3532892195_H
#define SETGUITEXTURE_T3532892195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUITexture
struct  SetGUITexture_t3532892195  : public ComponentAction_1_t3829190823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetGUITexture::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetGUITexture::texture
	FsmTexture_t1738787045 * ___texture_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetGUITexture_t3532892195, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_texture_17() { return static_cast<int32_t>(offsetof(SetGUITexture_t3532892195, ___texture_17)); }
	inline FsmTexture_t1738787045 * get_texture_17() const { return ___texture_17; }
	inline FsmTexture_t1738787045 ** get_address_of_texture_17() { return &___texture_17; }
	inline void set_texture_17(FsmTexture_t1738787045 * value)
	{
		___texture_17 = value;
		Il2CppCodeGenWriteBarrier((&___texture_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUITEXTURE_T3532892195_H
#ifndef SETGUITEXTUREALPHA_T394349438_H
#define SETGUITEXTUREALPHA_T394349438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUITextureAlpha
struct  SetGUITextureAlpha_t394349438  : public ComponentAction_1_t3829190823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetGUITextureAlpha::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGUITextureAlpha::alpha
	FsmFloat_t2883254149 * ___alpha_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGUITextureAlpha::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetGUITextureAlpha_t394349438, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_alpha_17() { return static_cast<int32_t>(offsetof(SetGUITextureAlpha_t394349438, ___alpha_17)); }
	inline FsmFloat_t2883254149 * get_alpha_17() const { return ___alpha_17; }
	inline FsmFloat_t2883254149 ** get_address_of_alpha_17() { return &___alpha_17; }
	inline void set_alpha_17(FsmFloat_t2883254149 * value)
	{
		___alpha_17 = value;
		Il2CppCodeGenWriteBarrier((&___alpha_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetGUITextureAlpha_t394349438, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUITEXTUREALPHA_T394349438_H
#ifndef SETGUITEXTURECOLOR_T337696484_H
#define SETGUITEXTURECOLOR_T337696484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGUITextureColor
struct  SetGUITextureColor_t337696484  : public ComponentAction_1_t3829190823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetGUITextureColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetGUITextureColor::color
	FsmColor_t1738900188 * ___color_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGUITextureColor::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetGUITextureColor_t337696484, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_color_17() { return static_cast<int32_t>(offsetof(SetGUITextureColor_t337696484, ___color_17)); }
	inline FsmColor_t1738900188 * get_color_17() const { return ___color_17; }
	inline FsmColor_t1738900188 ** get_address_of_color_17() { return &___color_17; }
	inline void set_color_17(FsmColor_t1738900188 * value)
	{
		___color_17 = value;
		Il2CppCodeGenWriteBarrier((&___color_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetGUITextureColor_t337696484, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGUITEXTURECOLOR_T337696484_H
#ifndef GUIBOX_T3115590542_H
#define GUIBOX_T3115590542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIBox
struct  GUIBox_t3115590542  : public GUIContentAction_t3694365960
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIBOX_T3115590542_H
#ifndef GUIBUTTON_T2236162889_H
#define GUIBUTTON_T2236162889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUIButton
struct  GUIButton_t2236162889  : public GUIContentAction_t3694365960
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUIButton::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUIButton::storeButtonState
	FsmBool_t163807967 * ___storeButtonState_27;

public:
	inline static int32_t get_offset_of_sendEvent_26() { return static_cast<int32_t>(offsetof(GUIButton_t2236162889, ___sendEvent_26)); }
	inline FsmEvent_t3736299882 * get_sendEvent_26() const { return ___sendEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_26() { return &___sendEvent_26; }
	inline void set_sendEvent_26(FsmEvent_t3736299882 * value)
	{
		___sendEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_26), value);
	}

	inline static int32_t get_offset_of_storeButtonState_27() { return static_cast<int32_t>(offsetof(GUIButton_t2236162889, ___storeButtonState_27)); }
	inline FsmBool_t163807967 * get_storeButtonState_27() const { return ___storeButtonState_27; }
	inline FsmBool_t163807967 ** get_address_of_storeButtonState_27() { return &___storeButtonState_27; }
	inline void set_storeButtonState_27(FsmBool_t163807967 * value)
	{
		___storeButtonState_27 = value;
		Il2CppCodeGenWriteBarrier((&___storeButtonState_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIBUTTON_T2236162889_H
#ifndef GUILABEL_T72759453_H
#define GUILABEL_T72759453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILabel
struct  GUILabel_t72759453  : public GUIContentAction_t3694365960
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILABEL_T72759453_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (DevicePlayFullScreenMovie_t1747172878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[4] = 
{
	DevicePlayFullScreenMovie_t1747172878::get_offset_of_moviePath_14(),
	DevicePlayFullScreenMovie_t1747172878::get_offset_of_fadeColor_15(),
	DevicePlayFullScreenMovie_t1747172878::get_offset_of_movieControlMode_16(),
	DevicePlayFullScreenMovie_t1747172878::get_offset_of_movieScalingMode_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (DeviceShakeEvent_t415947583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[2] = 
{
	DeviceShakeEvent_t415947583::get_offset_of_shakeThreshold_14(),
	DeviceShakeEvent_t415947583::get_offset_of_sendEvent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (DeviceVibrate_t3177149157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (GetDeviceAcceleration_t4085971106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[6] = 
{
	GetDeviceAcceleration_t4085971106::get_offset_of_storeVector_14(),
	GetDeviceAcceleration_t4085971106::get_offset_of_storeX_15(),
	GetDeviceAcceleration_t4085971106::get_offset_of_storeY_16(),
	GetDeviceAcceleration_t4085971106::get_offset_of_storeZ_17(),
	GetDeviceAcceleration_t4085971106::get_offset_of_multiplier_18(),
	GetDeviceAcceleration_t4085971106::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (GetDeviceRoll_t308638562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[6] = 
{
	GetDeviceRoll_t308638562::get_offset_of_baseOrientation_14(),
	GetDeviceRoll_t308638562::get_offset_of_storeAngle_15(),
	GetDeviceRoll_t308638562::get_offset_of_limitAngle_16(),
	GetDeviceRoll_t308638562::get_offset_of_smoothing_17(),
	GetDeviceRoll_t308638562::get_offset_of_everyFrame_18(),
	GetDeviceRoll_t308638562::get_offset_of_lastZAngle_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (BaseOrientation_t2907697253)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3205[4] = 
{
	BaseOrientation_t2907697253::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (GetIPhoneSettings_t35316684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3206[6] = 
{
	GetIPhoneSettings_t35316684::get_offset_of_getScreenCanDarken_14(),
	GetIPhoneSettings_t35316684::get_offset_of_getUniqueIdentifier_15(),
	GetIPhoneSettings_t35316684::get_offset_of_getName_16(),
	GetIPhoneSettings_t35316684::get_offset_of_getModel_17(),
	GetIPhoneSettings_t35316684::get_offset_of_getSystemName_18(),
	GetIPhoneSettings_t35316684::get_offset_of_getGeneration_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (GetLocationInfo_t933323761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[7] = 
{
	GetLocationInfo_t933323761::get_offset_of_vectorPosition_14(),
	GetLocationInfo_t933323761::get_offset_of_longitude_15(),
	GetLocationInfo_t933323761::get_offset_of_latitude_16(),
	GetLocationInfo_t933323761::get_offset_of_altitude_17(),
	GetLocationInfo_t933323761::get_offset_of_horizontalAccuracy_18(),
	GetLocationInfo_t933323761::get_offset_of_verticalAccuracy_19(),
	GetLocationInfo_t933323761::get_offset_of_errorEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (GetTouchCount_t3256006630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3208[2] = 
{
	GetTouchCount_t3256006630::get_offset_of_storeCount_14(),
	GetTouchCount_t3256006630::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (GetTouchInfo_t3534618608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[13] = 
{
	GetTouchInfo_t3534618608::get_offset_of_fingerId_14(),
	GetTouchInfo_t3534618608::get_offset_of_normalize_15(),
	GetTouchInfo_t3534618608::get_offset_of_storePosition_16(),
	GetTouchInfo_t3534618608::get_offset_of_storeX_17(),
	GetTouchInfo_t3534618608::get_offset_of_storeY_18(),
	GetTouchInfo_t3534618608::get_offset_of_storeDeltaPosition_19(),
	GetTouchInfo_t3534618608::get_offset_of_storeDeltaX_20(),
	GetTouchInfo_t3534618608::get_offset_of_storeDeltaY_21(),
	GetTouchInfo_t3534618608::get_offset_of_storeDeltaTime_22(),
	GetTouchInfo_t3534618608::get_offset_of_storeTapCount_23(),
	GetTouchInfo_t3534618608::get_offset_of_everyFrame_24(),
	GetTouchInfo_t3534618608::get_offset_of_screenWidth_25(),
	GetTouchInfo_t3534618608::get_offset_of_screenHeight_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (ProjectLocationToMap_t1440892226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3210[16] = 
{
	ProjectLocationToMap_t1440892226::get_offset_of_GPSLocation_14(),
	ProjectLocationToMap_t1440892226::get_offset_of_mapProjection_15(),
	ProjectLocationToMap_t1440892226::get_offset_of_minLongitude_16(),
	ProjectLocationToMap_t1440892226::get_offset_of_maxLongitude_17(),
	ProjectLocationToMap_t1440892226::get_offset_of_minLatitude_18(),
	ProjectLocationToMap_t1440892226::get_offset_of_maxLatitude_19(),
	ProjectLocationToMap_t1440892226::get_offset_of_minX_20(),
	ProjectLocationToMap_t1440892226::get_offset_of_minY_21(),
	ProjectLocationToMap_t1440892226::get_offset_of_width_22(),
	ProjectLocationToMap_t1440892226::get_offset_of_height_23(),
	ProjectLocationToMap_t1440892226::get_offset_of_projectedX_24(),
	ProjectLocationToMap_t1440892226::get_offset_of_projectedY_25(),
	ProjectLocationToMap_t1440892226::get_offset_of_normalized_26(),
	ProjectLocationToMap_t1440892226::get_offset_of_everyFrame_27(),
	ProjectLocationToMap_t1440892226::get_offset_of_x_28(),
	ProjectLocationToMap_t1440892226::get_offset_of_y_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (MapProjection_t2129394632)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3211[3] = 
{
	MapProjection_t2129394632::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (StartLocationServiceUpdates_t3395549688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[6] = 
{
	StartLocationServiceUpdates_t3395549688::get_offset_of_maxWait_14(),
	StartLocationServiceUpdates_t3395549688::get_offset_of_desiredAccuracy_15(),
	StartLocationServiceUpdates_t3395549688::get_offset_of_updateDistance_16(),
	StartLocationServiceUpdates_t3395549688::get_offset_of_successEvent_17(),
	StartLocationServiceUpdates_t3395549688::get_offset_of_failedEvent_18(),
	StartLocationServiceUpdates_t3395549688::get_offset_of_startTime_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (StopLocationServiceUpdates_t1376535429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (SwipeGestureEvent_t1778425824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[9] = 
{
	SwipeGestureEvent_t1778425824::get_offset_of_minSwipeDistance_14(),
	SwipeGestureEvent_t1778425824::get_offset_of_swipeLeftEvent_15(),
	SwipeGestureEvent_t1778425824::get_offset_of_swipeRightEvent_16(),
	SwipeGestureEvent_t1778425824::get_offset_of_swipeUpEvent_17(),
	SwipeGestureEvent_t1778425824::get_offset_of_swipeDownEvent_18(),
	SwipeGestureEvent_t1778425824::get_offset_of_screenDiagonalSize_19(),
	SwipeGestureEvent_t1778425824::get_offset_of_minSwipeDistancePixels_20(),
	SwipeGestureEvent_t1778425824::get_offset_of_touchStarted_21(),
	SwipeGestureEvent_t1778425824::get_offset_of_touchStartPos_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (TouchEvent_t3122665815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3215[4] = 
{
	TouchEvent_t3122665815::get_offset_of_fingerId_14(),
	TouchEvent_t3122665815::get_offset_of_touchPhase_15(),
	TouchEvent_t3122665815::get_offset_of_sendEvent_16(),
	TouchEvent_t3122665815::get_offset_of_storeFingerId_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (TouchGUIEvent_t3504819662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[17] = 
{
	TouchGUIEvent_t3504819662::get_offset_of_gameObject_14(),
	TouchGUIEvent_t3504819662::get_offset_of_fingerId_15(),
	TouchGUIEvent_t3504819662::get_offset_of_touchBegan_16(),
	TouchGUIEvent_t3504819662::get_offset_of_touchMoved_17(),
	TouchGUIEvent_t3504819662::get_offset_of_touchStationary_18(),
	TouchGUIEvent_t3504819662::get_offset_of_touchEnded_19(),
	TouchGUIEvent_t3504819662::get_offset_of_touchCanceled_20(),
	TouchGUIEvent_t3504819662::get_offset_of_notTouching_21(),
	TouchGUIEvent_t3504819662::get_offset_of_storeFingerId_22(),
	TouchGUIEvent_t3504819662::get_offset_of_storeHitPoint_23(),
	TouchGUIEvent_t3504819662::get_offset_of_normalizeHitPoint_24(),
	TouchGUIEvent_t3504819662::get_offset_of_storeOffset_25(),
	TouchGUIEvent_t3504819662::get_offset_of_relativeTo_26(),
	TouchGUIEvent_t3504819662::get_offset_of_normalizeOffset_27(),
	TouchGUIEvent_t3504819662::get_offset_of_everyFrame_28(),
	TouchGUIEvent_t3504819662::get_offset_of_touchStartPos_29(),
	TouchGUIEvent_t3504819662::get_offset_of_guiElement_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (OffsetOptions_t1111682770)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3217[4] = 
{
	OffsetOptions_t1111682770::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (TouchObjectEvent_t938568464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[11] = 
{
	TouchObjectEvent_t938568464::get_offset_of_gameObject_14(),
	TouchObjectEvent_t938568464::get_offset_of_pickDistance_15(),
	TouchObjectEvent_t938568464::get_offset_of_fingerId_16(),
	TouchObjectEvent_t938568464::get_offset_of_touchBegan_17(),
	TouchObjectEvent_t938568464::get_offset_of_touchMoved_18(),
	TouchObjectEvent_t938568464::get_offset_of_touchStationary_19(),
	TouchObjectEvent_t938568464::get_offset_of_touchEnded_20(),
	TouchObjectEvent_t938568464::get_offset_of_touchCanceled_21(),
	TouchObjectEvent_t938568464::get_offset_of_storeFingerId_22(),
	TouchObjectEvent_t938568464::get_offset_of_storeHitPoint_23(),
	TouchObjectEvent_t938568464::get_offset_of_storeHitNormal_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (EasingFunction_t3263762189), -1, sizeof(EasingFunction_t3263762189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3219[67] = 
{
	0,
	EasingFunction_t3263762189_StaticFields::get_offset_of_AnimationCurve_1(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_3(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_4(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_5(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_6(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_7(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_8(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_9(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_10(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_11(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_12(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_13(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_14(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_15(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_16(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_17(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_18(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_19(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_20(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_21(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache14_22(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache15_23(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache16_24(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache17_25(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache18_26(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache19_27(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1A_28(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1B_29(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1C_30(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1D_31(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1E_32(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1F_33(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache20_34(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache21_35(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache22_36(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache23_37(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache24_38(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache25_39(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache26_40(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache27_41(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache28_42(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache29_43(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2A_44(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2B_45(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2C_46(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2D_47(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2E_48(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2F_49(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache30_50(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache31_51(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache32_52(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache33_53(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache34_54(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache35_55(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache36_56(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache37_57(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache38_58(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache39_59(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3A_60(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3B_61(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3C_62(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3D_63(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3E_64(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3F_65(),
	EasingFunction_t3263762189_StaticFields::get_offset_of_U3CU3Ef__mgU24cache40_66(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (Ease_t2364754935)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3220[34] = 
{
	Ease_t2364754935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (Function_t3898704989), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (Blink_t2426860219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3222[9] = 
{
	Blink_t2426860219::get_offset_of_gameObject_16(),
	Blink_t2426860219::get_offset_of_timeOff_17(),
	Blink_t2426860219::get_offset_of_timeOn_18(),
	Blink_t2426860219::get_offset_of_startOn_19(),
	Blink_t2426860219::get_offset_of_rendererOnly_20(),
	Blink_t2426860219::get_offset_of_realTime_21(),
	Blink_t2426860219::get_offset_of_startTime_22(),
	Blink_t2426860219::get_offset_of_timer_23(),
	Blink_t2426860219::get_offset_of_blinkOn_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (Flicker_t18802239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3223[7] = 
{
	Flicker_t18802239::get_offset_of_gameObject_16(),
	Flicker_t18802239::get_offset_of_frequency_17(),
	Flicker_t18802239::get_offset_of_amountOn_18(),
	Flicker_t18802239::get_offset_of_rendererOnly_19(),
	Flicker_t18802239::get_offset_of_realTime_20(),
	Flicker_t18802239::get_offset_of_startTime_21(),
	Flicker_t18802239::get_offset_of_timer_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (SetEnumValue_t2351312988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3224[3] = 
{
	SetEnumValue_t2351312988::get_offset_of_enumVariable_14(),
	SetEnumValue_t2351312988::get_offset_of_enumValue_15(),
	SetEnumValue_t2351312988::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (ActivateGameObject_t2035484578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[6] = 
{
	ActivateGameObject_t2035484578::get_offset_of_gameObject_14(),
	ActivateGameObject_t2035484578::get_offset_of_activate_15(),
	ActivateGameObject_t2035484578::get_offset_of_recursive_16(),
	ActivateGameObject_t2035484578::get_offset_of_resetOnExit_17(),
	ActivateGameObject_t2035484578::get_offset_of_everyFrame_18(),
	ActivateGameObject_t2035484578::get_offset_of_activatedGameObject_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (AddComponent_t2682014428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[5] = 
{
	AddComponent_t2682014428::get_offset_of_gameObject_14(),
	AddComponent_t2682014428::get_offset_of_component_15(),
	AddComponent_t2682014428::get_offset_of_storeComponent_16(),
	AddComponent_t2682014428::get_offset_of_removeOnExit_17(),
	AddComponent_t2682014428::get_offset_of_addedComponent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (CreateEmptyObject_t4245439205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[5] = 
{
	CreateEmptyObject_t4245439205::get_offset_of_gameObject_14(),
	CreateEmptyObject_t4245439205::get_offset_of_spawnPoint_15(),
	CreateEmptyObject_t4245439205::get_offset_of_position_16(),
	CreateEmptyObject_t4245439205::get_offset_of_rotation_17(),
	CreateEmptyObject_t4245439205::get_offset_of_storeObject_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (CreateObject_t2822511132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[5] = 
{
	CreateObject_t2822511132::get_offset_of_gameObject_14(),
	CreateObject_t2822511132::get_offset_of_spawnPoint_15(),
	CreateObject_t2822511132::get_offset_of_position_16(),
	CreateObject_t2822511132::get_offset_of_rotation_17(),
	CreateObject_t2822511132::get_offset_of_storeObject_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (DestroyComponent_t2397952545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3229[3] = 
{
	DestroyComponent_t2397952545::get_offset_of_gameObject_14(),
	DestroyComponent_t2397952545::get_offset_of_component_15(),
	DestroyComponent_t2397952545::get_offset_of_aComponent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (DestroyObject_t4173337614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[3] = 
{
	DestroyObject_t4173337614::get_offset_of_gameObject_14(),
	DestroyObject_t4173337614::get_offset_of_delay_15(),
	DestroyObject_t4173337614::get_offset_of_detachChildren_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (DestroyObjects_t2663322638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[3] = 
{
	DestroyObjects_t2663322638::get_offset_of_gameObjects_14(),
	DestroyObjects_t2663322638::get_offset_of_delay_15(),
	DestroyObjects_t2663322638::get_offset_of_detachChildren_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (DestroySelf_t235920970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[1] = 
{
	DestroySelf_t235920970::get_offset_of_detachChildren_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (DetachChildren_t1056900145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3233[1] = 
{
	DetachChildren_t1056900145::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (FindChild_t142528656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[3] = 
{
	FindChild_t142528656::get_offset_of_gameObject_14(),
	FindChild_t142528656::get_offset_of_childName_15(),
	FindChild_t142528656::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (FindClosest_t375870294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3235[7] = 
{
	FindClosest_t375870294::get_offset_of_gameObject_14(),
	FindClosest_t375870294::get_offset_of_withTag_15(),
	FindClosest_t375870294::get_offset_of_ignoreOwner_16(),
	FindClosest_t375870294::get_offset_of_mustBeVisible_17(),
	FindClosest_t375870294::get_offset_of_storeObject_18(),
	FindClosest_t375870294::get_offset_of_storeDistance_19(),
	FindClosest_t375870294::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (FindGameObject_t3470023239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3236[3] = 
{
	FindGameObject_t3470023239::get_offset_of_objectName_14(),
	FindGameObject_t3470023239::get_offset_of_withTag_15(),
	FindGameObject_t3470023239::get_offset_of_store_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (GetChild_t2112359604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[4] = 
{
	GetChild_t2112359604::get_offset_of_gameObject_14(),
	GetChild_t2112359604::get_offset_of_childName_15(),
	GetChild_t2112359604::get_offset_of_withTag_16(),
	GetChild_t2112359604::get_offset_of_storeResult_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (GetChildCount_t3095803499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3238[2] = 
{
	GetChildCount_t3095803499::get_offset_of_gameObject_14(),
	GetChildCount_t3095803499::get_offset_of_storeResult_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (GetChildNum_t3674298174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[3] = 
{
	GetChildNum_t3674298174::get_offset_of_gameObject_14(),
	GetChildNum_t3674298174::get_offset_of_childIndex_15(),
	GetChildNum_t3674298174::get_offset_of_store_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (GetDistance_t3017425359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[4] = 
{
	GetDistance_t3017425359::get_offset_of_gameObject_14(),
	GetDistance_t3017425359::get_offset_of_target_15(),
	GetDistance_t3017425359::get_offset_of_storeResult_16(),
	GetDistance_t3017425359::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (GetLayer_t2829532394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[3] = 
{
	GetLayer_t2829532394::get_offset_of_gameObject_14(),
	GetLayer_t2829532394::get_offset_of_storeResult_15(),
	GetLayer_t2829532394::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (GetName_t534723818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[3] = 
{
	GetName_t534723818::get_offset_of_gameObject_14(),
	GetName_t534723818::get_offset_of_storeName_15(),
	GetName_t534723818::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (GetNextChild_t3737268120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[7] = 
{
	GetNextChild_t3737268120::get_offset_of_gameObject_14(),
	GetNextChild_t3737268120::get_offset_of_storeNextChild_15(),
	GetNextChild_t3737268120::get_offset_of_loopEvent_16(),
	GetNextChild_t3737268120::get_offset_of_finishedEvent_17(),
	GetNextChild_t3737268120::get_offset_of_resetFlag_18(),
	GetNextChild_t3737268120::get_offset_of_go_19(),
	GetNextChild_t3737268120::get_offset_of_nextChildIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (GetOwner_t3978915792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[1] = 
{
	GetOwner_t3978915792::get_offset_of_storeGameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (GetParent_t2000019683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[2] = 
{
	GetParent_t2000019683::get_offset_of_gameObject_14(),
	GetParent_t2000019683::get_offset_of_storeResult_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (GetRandomChild_t2866006007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3246[2] = 
{
	GetRandomChild_t2866006007::get_offset_of_gameObject_14(),
	GetRandomChild_t2866006007::get_offset_of_storeResult_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (GetRandomObject_t296617571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[3] = 
{
	GetRandomObject_t296617571::get_offset_of_withTag_14(),
	GetRandomObject_t296617571::get_offset_of_storeResult_15(),
	GetRandomObject_t296617571::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (GetRoot_t898400599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[2] = 
{
	GetRoot_t898400599::get_offset_of_gameObject_14(),
	GetRoot_t898400599::get_offset_of_storeRoot_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (GetTag_t3830046547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[3] = 
{
	GetTag_t3830046547::get_offset_of_gameObject_14(),
	GetTag_t3830046547::get_offset_of_storeResult_15(),
	GetTag_t3830046547::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (GetTagCount_t1999077496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3250[2] = 
{
	GetTagCount_t1999077496::get_offset_of_tag_14(),
	GetTagCount_t1999077496::get_offset_of_storeResult_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (GetTransform_t1873649011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[3] = 
{
	GetTransform_t1873649011::get_offset_of_gameObject_14(),
	GetTransform_t1873649011::get_offset_of_storeTransform_15(),
	GetTransform_t1873649011::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (HasComponent_t1059837092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[8] = 
{
	HasComponent_t1059837092::get_offset_of_gameObject_14(),
	HasComponent_t1059837092::get_offset_of_component_15(),
	HasComponent_t1059837092::get_offset_of_removeOnExit_16(),
	HasComponent_t1059837092::get_offset_of_trueEvent_17(),
	HasComponent_t1059837092::get_offset_of_falseEvent_18(),
	HasComponent_t1059837092::get_offset_of_store_19(),
	HasComponent_t1059837092::get_offset_of_everyFrame_20(),
	HasComponent_t1059837092::get_offset_of_aComponent_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (SelectRandomGameObject_t2588744226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[3] = 
{
	SelectRandomGameObject_t2588744226::get_offset_of_gameObjects_14(),
	SelectRandomGameObject_t2588744226::get_offset_of_weights_15(),
	SelectRandomGameObject_t2588744226::get_offset_of_storeGameObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (SetGameObject_t597754492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[3] = 
{
	SetGameObject_t597754492::get_offset_of_variable_14(),
	SetGameObject_t597754492::get_offset_of_gameObject_15(),
	SetGameObject_t597754492::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (SetLayer_t3918912646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[2] = 
{
	SetLayer_t3918912646::get_offset_of_gameObject_14(),
	SetLayer_t3918912646::get_offset_of_layer_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (SetName_t1624104070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[2] = 
{
	SetName_t1624104070::get_offset_of_gameObject_14(),
	SetName_t1624104070::get_offset_of_name_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (SetParent_t3814774471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3257[4] = 
{
	SetParent_t3814774471::get_offset_of_gameObject_14(),
	SetParent_t3814774471::get_offset_of_parent_15(),
	SetParent_t3814774471::get_offset_of_resetLocalPosition_16(),
	SetParent_t3814774471::get_offset_of_resetLocalRotation_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (SetTag_t1389133551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[2] = 
{
	SetTag_t1389133551::get_offset_of_gameObject_14(),
	SetTag_t1389133551::get_offset_of_tag_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (SetTagsOnChildren_t4128407550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[4] = 
{
	SetTagsOnChildren_t4128407550::get_offset_of_gameObject_14(),
	SetTagsOnChildren_t4128407550::get_offset_of_tag_15(),
	SetTagsOnChildren_t4128407550::get_offset_of_filterByComponent_16(),
	SetTagsOnChildren_t4128407550::get_offset_of_componentFilter_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (DrawFullscreenColor_t1684922187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3260[1] = 
{
	DrawFullscreenColor_t1684922187::get_offset_of_color_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (DrawTexture_t1719845717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3261[11] = 
{
	DrawTexture_t1719845717::get_offset_of_texture_14(),
	DrawTexture_t1719845717::get_offset_of_screenRect_15(),
	DrawTexture_t1719845717::get_offset_of_left_16(),
	DrawTexture_t1719845717::get_offset_of_top_17(),
	DrawTexture_t1719845717::get_offset_of_width_18(),
	DrawTexture_t1719845717::get_offset_of_height_19(),
	DrawTexture_t1719845717::get_offset_of_scaleMode_20(),
	DrawTexture_t1719845717::get_offset_of_alphaBlend_21(),
	DrawTexture_t1719845717::get_offset_of_imageAspect_22(),
	DrawTexture_t1719845717::get_offset_of_normalized_23(),
	DrawTexture_t1719845717::get_offset_of_rect_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (EnableGUI_t1143397964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[1] = 
{
	EnableGUI_t1143397964::get_offset_of_enableGUI_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (GUIAction_t3750965478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[7] = 
{
	GUIAction_t3750965478::get_offset_of_screenRect_14(),
	GUIAction_t3750965478::get_offset_of_left_15(),
	GUIAction_t3750965478::get_offset_of_top_16(),
	GUIAction_t3750965478::get_offset_of_width_17(),
	GUIAction_t3750965478::get_offset_of_height_18(),
	GUIAction_t3750965478::get_offset_of_normalized_19(),
	GUIAction_t3750965478::get_offset_of_rect_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (GUIBox_t3115590542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (GUIButton_t2236162889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3265[2] = 
{
	GUIButton_t2236162889::get_offset_of_sendEvent_26(),
	GUIButton_t2236162889::get_offset_of_storeButtonState_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (GUIContentAction_t3694365960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3266[5] = 
{
	GUIContentAction_t3694365960::get_offset_of_image_21(),
	GUIContentAction_t3694365960::get_offset_of_text_22(),
	GUIContentAction_t3694365960::get_offset_of_tooltip_23(),
	GUIContentAction_t3694365960::get_offset_of_style_24(),
	GUIContentAction_t3694365960::get_offset_of_content_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (GUIElementHitTest_t2786187436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[11] = 
{
	GUIElementHitTest_t2786187436::get_offset_of_gameObject_14(),
	GUIElementHitTest_t2786187436::get_offset_of_camera_15(),
	GUIElementHitTest_t2786187436::get_offset_of_screenPoint_16(),
	GUIElementHitTest_t2786187436::get_offset_of_screenX_17(),
	GUIElementHitTest_t2786187436::get_offset_of_screenY_18(),
	GUIElementHitTest_t2786187436::get_offset_of_normalized_19(),
	GUIElementHitTest_t2786187436::get_offset_of_hitEvent_20(),
	GUIElementHitTest_t2786187436::get_offset_of_storeResult_21(),
	GUIElementHitTest_t2786187436::get_offset_of_everyFrame_22(),
	GUIElementHitTest_t2786187436::get_offset_of_guiElement_23(),
	GUIElementHitTest_t2786187436::get_offset_of_gameObjectCached_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (GUIHorizontalSlider_t1379811325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[5] = 
{
	GUIHorizontalSlider_t1379811325::get_offset_of_floatVariable_21(),
	GUIHorizontalSlider_t1379811325::get_offset_of_leftValue_22(),
	GUIHorizontalSlider_t1379811325::get_offset_of_rightValue_23(),
	GUIHorizontalSlider_t1379811325::get_offset_of_sliderStyle_24(),
	GUIHorizontalSlider_t1379811325::get_offset_of_thumbStyle_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (GUILabel_t72759453), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (GUITooltip_t2214156286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3270[1] = 
{
	GUITooltip_t2214156286::get_offset_of_storeTooltip_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (GUIVerticalSlider_t3464442948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3271[5] = 
{
	GUIVerticalSlider_t3464442948::get_offset_of_floatVariable_21(),
	GUIVerticalSlider_t3464442948::get_offset_of_topValue_22(),
	GUIVerticalSlider_t3464442948::get_offset_of_bottomValue_23(),
	GUIVerticalSlider_t3464442948::get_offset_of_sliderStyle_24(),
	GUIVerticalSlider_t3464442948::get_offset_of_thumbStyle_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (ResetGUIMatrix_t582177292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (RotateGUI_t3863613888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[6] = 
{
	RotateGUI_t3863613888::get_offset_of_angle_14(),
	RotateGUI_t3863613888::get_offset_of_pivotX_15(),
	RotateGUI_t3863613888::get_offset_of_pivotY_16(),
	RotateGUI_t3863613888::get_offset_of_normalized_17(),
	RotateGUI_t3863613888::get_offset_of_applyGlobally_18(),
	RotateGUI_t3863613888::get_offset_of_applied_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (ScaleGUI_t3223259317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3274[7] = 
{
	ScaleGUI_t3223259317::get_offset_of_scaleX_14(),
	ScaleGUI_t3223259317::get_offset_of_scaleY_15(),
	ScaleGUI_t3223259317::get_offset_of_pivotX_16(),
	ScaleGUI_t3223259317::get_offset_of_pivotY_17(),
	ScaleGUI_t3223259317::get_offset_of_normalized_18(),
	ScaleGUI_t3223259317::get_offset_of_applyGlobally_19(),
	ScaleGUI_t3223259317::get_offset_of_applied_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (SetGUIAlpha_t2212989556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3275[2] = 
{
	SetGUIAlpha_t2212989556::get_offset_of_alpha_14(),
	SetGUIAlpha_t2212989556::get_offset_of_applyGlobally_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (SetGUIBackgroundColor_t544783107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[2] = 
{
	SetGUIBackgroundColor_t544783107::get_offset_of_backgroundColor_14(),
	SetGUIBackgroundColor_t544783107::get_offset_of_applyGlobally_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (SetGUIColor_t505170613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3277[2] = 
{
	SetGUIColor_t505170613::get_offset_of_color_14(),
	SetGUIColor_t505170613::get_offset_of_applyGlobally_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (SetGUIContentColor_t3963444964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[2] = 
{
	SetGUIContentColor_t3963444964::get_offset_of_contentColor_14(),
	SetGUIContentColor_t3963444964::get_offset_of_applyGlobally_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (SetGUIDepth_t192580112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3279[1] = 
{
	SetGUIDepth_t192580112::get_offset_of_depth_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (SetGUISkin_t296284078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3280[2] = 
{
	SetGUISkin_t296284078::get_offset_of_skin_14(),
	SetGUISkin_t296284078::get_offset_of_applyGlobally_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (SetMouseCursor_t3988608720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3281[3] = 
{
	SetMouseCursor_t3988608720::get_offset_of_cursorTexture_14(),
	SetMouseCursor_t3988608720::get_offset_of_hideCursor_15(),
	SetMouseCursor_t3988608720::get_offset_of_lockCursor_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (SetGUIText_t3459324574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3282[3] = 
{
	SetGUIText_t3459324574::get_offset_of_gameObject_16(),
	SetGUIText_t3459324574::get_offset_of_text_17(),
	SetGUIText_t3459324574::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (SetGUITexture_t3532892195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[2] = 
{
	SetGUITexture_t3532892195::get_offset_of_gameObject_16(),
	SetGUITexture_t3532892195::get_offset_of_texture_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (SetGUITextureAlpha_t394349438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[3] = 
{
	SetGUITextureAlpha_t394349438::get_offset_of_gameObject_16(),
	SetGUITextureAlpha_t394349438::get_offset_of_alpha_17(),
	SetGUITextureAlpha_t394349438::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (SetGUITextureColor_t337696484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3285[3] = 
{
	SetGUITextureColor_t337696484::get_offset_of_gameObject_16(),
	SetGUITextureColor_t337696484::get_offset_of_color_17(),
	SetGUITextureColor_t337696484::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (GUILayoutAction_t1272787235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[2] = 
{
	GUILayoutAction_t1272787235::get_offset_of_layoutOptions_14(),
	GUILayoutAction_t1272787235::get_offset_of_options_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (GUILayoutBeginArea_t1161236718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3287[8] = 
{
	GUILayoutBeginArea_t1161236718::get_offset_of_screenRect_14(),
	GUILayoutBeginArea_t1161236718::get_offset_of_left_15(),
	GUILayoutBeginArea_t1161236718::get_offset_of_top_16(),
	GUILayoutBeginArea_t1161236718::get_offset_of_width_17(),
	GUILayoutBeginArea_t1161236718::get_offset_of_height_18(),
	GUILayoutBeginArea_t1161236718::get_offset_of_normalized_19(),
	GUILayoutBeginArea_t1161236718::get_offset_of_style_20(),
	GUILayoutBeginArea_t1161236718::get_offset_of_rect_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (GUILayoutBeginAreaFollowObject_t2928844525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3288[7] = 
{
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_gameObject_14(),
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_offsetLeft_15(),
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_offsetTop_16(),
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_width_17(),
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_height_18(),
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_normalized_19(),
	GUILayoutBeginAreaFollowObject_t2928844525::get_offset_of_style_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (GUILayoutBeginCentered_t2525719869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (GUILayoutBeginHorizontal_t2612542000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[4] = 
{
	GUILayoutBeginHorizontal_t2612542000::get_offset_of_image_16(),
	GUILayoutBeginHorizontal_t2612542000::get_offset_of_text_17(),
	GUILayoutBeginHorizontal_t2612542000::get_offset_of_tooltip_18(),
	GUILayoutBeginHorizontal_t2612542000::get_offset_of_style_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (GUILayoutBeginScrollView_t2032397505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[7] = 
{
	GUILayoutBeginScrollView_t2032397505::get_offset_of_scrollPosition_16(),
	GUILayoutBeginScrollView_t2032397505::get_offset_of_horizontalScrollbar_17(),
	GUILayoutBeginScrollView_t2032397505::get_offset_of_verticalScrollbar_18(),
	GUILayoutBeginScrollView_t2032397505::get_offset_of_useCustomStyle_19(),
	GUILayoutBeginScrollView_t2032397505::get_offset_of_horizontalStyle_20(),
	GUILayoutBeginScrollView_t2032397505::get_offset_of_verticalStyle_21(),
	GUILayoutBeginScrollView_t2032397505::get_offset_of_backgroundStyle_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (GUILayoutBeginVertical_t1479530004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[4] = 
{
	GUILayoutBeginVertical_t1479530004::get_offset_of_image_16(),
	GUILayoutBeginVertical_t1479530004::get_offset_of_text_17(),
	GUILayoutBeginVertical_t1479530004::get_offset_of_tooltip_18(),
	GUILayoutBeginVertical_t1479530004::get_offset_of_style_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (GUILayoutBox_t2785960633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[4] = 
{
	GUILayoutBox_t2785960633::get_offset_of_image_16(),
	GUILayoutBox_t2785960633::get_offset_of_text_17(),
	GUILayoutBox_t2785960633::get_offset_of_tooltip_18(),
	GUILayoutBox_t2785960633::get_offset_of_style_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (GUILayoutButton_t379382150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[6] = 
{
	GUILayoutButton_t379382150::get_offset_of_sendEvent_16(),
	GUILayoutButton_t379382150::get_offset_of_storeButtonState_17(),
	GUILayoutButton_t379382150::get_offset_of_image_18(),
	GUILayoutButton_t379382150::get_offset_of_text_19(),
	GUILayoutButton_t379382150::get_offset_of_tooltip_20(),
	GUILayoutButton_t379382150::get_offset_of_style_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (GUILayoutConfirmPasswordField_t3437180288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3295[7] = 
{
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_text_16(),
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_maxLength_17(),
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_style_18(),
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_changedEvent_19(),
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_mask_20(),
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_confirm_21(),
	GUILayoutConfirmPasswordField_t3437180288::get_offset_of_password_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (GUILayoutEmailField_t3678593203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[5] = 
{
	GUILayoutEmailField_t3678593203::get_offset_of_text_16(),
	GUILayoutEmailField_t3678593203::get_offset_of_maxLength_17(),
	GUILayoutEmailField_t3678593203::get_offset_of_style_18(),
	GUILayoutEmailField_t3678593203::get_offset_of_changedEvent_19(),
	GUILayoutEmailField_t3678593203::get_offset_of_valid_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (GUILayoutEndArea_t1298492153), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (GUILayoutEndCentered_t1066408831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (GUILayoutEndHorizontal_t880464906), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
