﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t3432711236;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2904461592;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// PlayMakerProxyBase
struct PlayMakerProxyBase_t90512809;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.HingeJoint2D
struct HingeJoint2D_t3442948838;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t3308997185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.WheelJoint2D
struct WheelJoint2D_t1756757149;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef JOINTANGLELIMITS2D_T972279075_H
#define JOINTANGLELIMITS2D_T972279075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointAngleLimits2D
struct  JointAngleLimits2D_t972279075 
{
public:
	// System.Single UnityEngine.JointAngleLimits2D::m_LowerAngle
	float ___m_LowerAngle_0;
	// System.Single UnityEngine.JointAngleLimits2D::m_UpperAngle
	float ___m_UpperAngle_1;

public:
	inline static int32_t get_offset_of_m_LowerAngle_0() { return static_cast<int32_t>(offsetof(JointAngleLimits2D_t972279075, ___m_LowerAngle_0)); }
	inline float get_m_LowerAngle_0() const { return ___m_LowerAngle_0; }
	inline float* get_address_of_m_LowerAngle_0() { return &___m_LowerAngle_0; }
	inline void set_m_LowerAngle_0(float value)
	{
		___m_LowerAngle_0 = value;
	}

	inline static int32_t get_offset_of_m_UpperAngle_1() { return static_cast<int32_t>(offsetof(JointAngleLimits2D_t972279075, ___m_UpperAngle_1)); }
	inline float get_m_UpperAngle_1() const { return ___m_UpperAngle_1; }
	inline float* get_address_of_m_UpperAngle_1() { return &___m_UpperAngle_1; }
	inline void set_m_UpperAngle_1(float value)
	{
		___m_UpperAngle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTANGLELIMITS2D_T972279075_H
#ifndef JOINTMOTOR2D_T142461323_H
#define JOINTMOTOR2D_T142461323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointMotor2D
struct  JointMotor2D_t142461323 
{
public:
	// System.Single UnityEngine.JointMotor2D::m_MotorSpeed
	float ___m_MotorSpeed_0;
	// System.Single UnityEngine.JointMotor2D::m_MaximumMotorTorque
	float ___m_MaximumMotorTorque_1;

public:
	inline static int32_t get_offset_of_m_MotorSpeed_0() { return static_cast<int32_t>(offsetof(JointMotor2D_t142461323, ___m_MotorSpeed_0)); }
	inline float get_m_MotorSpeed_0() const { return ___m_MotorSpeed_0; }
	inline float* get_address_of_m_MotorSpeed_0() { return &___m_MotorSpeed_0; }
	inline void set_m_MotorSpeed_0(float value)
	{
		___m_MotorSpeed_0 = value;
	}

	inline static int32_t get_offset_of_m_MaximumMotorTorque_1() { return static_cast<int32_t>(offsetof(JointMotor2D_t142461323, ___m_MaximumMotorTorque_1)); }
	inline float get_m_MaximumMotorTorque_1() const { return ___m_MaximumMotorTorque_1; }
	inline float* get_address_of_m_MaximumMotorTorque_1() { return &___m_MaximumMotorTorque_1; }
	inline void set_m_MaximumMotorTorque_1(float value)
	{
		___m_MaximumMotorTorque_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTMOTOR2D_T142461323_H
#ifndef JOINTSUSPENSION2D_T3350541332_H
#define JOINTSUSPENSION2D_T3350541332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointSuspension2D
struct  JointSuspension2D_t3350541332 
{
public:
	// System.Single UnityEngine.JointSuspension2D::m_DampingRatio
	float ___m_DampingRatio_0;
	// System.Single UnityEngine.JointSuspension2D::m_Frequency
	float ___m_Frequency_1;
	// System.Single UnityEngine.JointSuspension2D::m_Angle
	float ___m_Angle_2;

public:
	inline static int32_t get_offset_of_m_DampingRatio_0() { return static_cast<int32_t>(offsetof(JointSuspension2D_t3350541332, ___m_DampingRatio_0)); }
	inline float get_m_DampingRatio_0() const { return ___m_DampingRatio_0; }
	inline float* get_address_of_m_DampingRatio_0() { return &___m_DampingRatio_0; }
	inline void set_m_DampingRatio_0(float value)
	{
		___m_DampingRatio_0 = value;
	}

	inline static int32_t get_offset_of_m_Frequency_1() { return static_cast<int32_t>(offsetof(JointSuspension2D_t3350541332, ___m_Frequency_1)); }
	inline float get_m_Frequency_1() const { return ___m_Frequency_1; }
	inline float* get_address_of_m_Frequency_1() { return &___m_Frequency_1; }
	inline void set_m_Frequency_1(float value)
	{
		___m_Frequency_1 = value;
	}

	inline static int32_t get_offset_of_m_Angle_2() { return static_cast<int32_t>(offsetof(JointSuspension2D_t3350541332, ___m_Angle_2)); }
	inline float get_m_Angle_2() const { return ___m_Angle_2; }
	inline float* get_address_of_m_Angle_2() { return &___m_Angle_2; }
	inline void set_m_Angle_2(float value)
	{
		___m_Angle_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTSUSPENSION2D_T3350541332_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef OPERATION_T79835483_H
#define OPERATION_T79835483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatOperator/Operation
struct  Operation_t79835483 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.FloatOperator/Operation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Operation_t79835483, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATION_T79835483_H
#ifndef OPERATION_T3934176782_H
#define OPERATION_T3934176782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntOperator/Operation
struct  Operation_t3934176782 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.IntOperator/Operation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Operation_t3934176782, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATION_T3934176782_H
#ifndef COLLISION2DTYPE_T450510585_H
#define COLLISION2DTYPE_T450510585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Collision2DType
struct  Collision2DType_t450510585 
{
public:
	// System.Int32 HutongGames.PlayMaker.Collision2DType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Collision2DType_t450510585, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISION2DTYPE_T450510585_H
#ifndef COLLISIONTYPE_T4059948830_H
#define COLLISIONTYPE_T4059948830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.CollisionType
struct  CollisionType_t4059948830 
{
public:
	// System.Int32 HutongGames.PlayMaker.CollisionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionType_t4059948830, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONTYPE_T4059948830_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef INTERPOLATIONTYPE_T358821704_H
#define INTERPOLATIONTYPE_T358821704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.InterpolationType
struct  InterpolationType_t358821704 
{
public:
	// System.Int32 HutongGames.PlayMaker.InterpolationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InterpolationType_t358821704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONTYPE_T358821704_H
#ifndef TRIGGER2DTYPE_T3885175869_H
#define TRIGGER2DTYPE_T3885175869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Trigger2DType
struct  Trigger2DType_t3885175869 
{
public:
	// System.Int32 HutongGames.PlayMaker.Trigger2DType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger2DType_t3885175869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER2DTYPE_T3885175869_H
#ifndef TRIGGERTYPE_T1545777084_H
#define TRIGGERTYPE_T1545777084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TriggerType
struct  TriggerType_t1545777084 
{
public:
	// System.Int32 HutongGames.PlayMaker.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t1545777084, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T1545777084_H
#ifndef FORCEMODE_T3656391766_H
#define FORCEMODE_T3656391766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode
struct  ForceMode_t3656391766 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode_t3656391766, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE_T3656391766_H
#ifndef FORCEMODE2D_T255358695_H
#define FORCEMODE2D_T255358695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode2D
struct  ForceMode2D_t255358695 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode2D_t255358695, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE2D_T255358695_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef COLLISION2DEVENT_T2872455664_H
#define COLLISION2DEVENT_T2872455664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Collision2dEvent
struct  Collision2dEvent_t2872455664  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Collision2dEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.Collision2DType HutongGames.PlayMaker.Actions.Collision2dEvent::collision
	int32_t ___collision_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.Collision2dEvent::collideTag
	FsmString_t1785915204 * ___collideTag_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Collision2dEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.Collision2dEvent::storeCollider
	FsmGameObject_t3581898942 * ___storeCollider_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Collision2dEvent::storeForce
	FsmFloat_t2883254149 * ___storeForce_19;
	// PlayMakerProxyBase HutongGames.PlayMaker.Actions.Collision2dEvent::cachedProxy
	PlayMakerProxyBase_t90512809 * ___cachedProxy_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_collision_15() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___collision_15)); }
	inline int32_t get_collision_15() const { return ___collision_15; }
	inline int32_t* get_address_of_collision_15() { return &___collision_15; }
	inline void set_collision_15(int32_t value)
	{
		___collision_15 = value;
	}

	inline static int32_t get_offset_of_collideTag_16() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___collideTag_16)); }
	inline FsmString_t1785915204 * get_collideTag_16() const { return ___collideTag_16; }
	inline FsmString_t1785915204 ** get_address_of_collideTag_16() { return &___collideTag_16; }
	inline void set_collideTag_16(FsmString_t1785915204 * value)
	{
		___collideTag_16 = value;
		Il2CppCodeGenWriteBarrier((&___collideTag_16), value);
	}

	inline static int32_t get_offset_of_sendEvent_17() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___sendEvent_17)); }
	inline FsmEvent_t3736299882 * get_sendEvent_17() const { return ___sendEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_17() { return &___sendEvent_17; }
	inline void set_sendEvent_17(FsmEvent_t3736299882 * value)
	{
		___sendEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_17), value);
	}

	inline static int32_t get_offset_of_storeCollider_18() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___storeCollider_18)); }
	inline FsmGameObject_t3581898942 * get_storeCollider_18() const { return ___storeCollider_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeCollider_18() { return &___storeCollider_18; }
	inline void set_storeCollider_18(FsmGameObject_t3581898942 * value)
	{
		___storeCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeCollider_18), value);
	}

	inline static int32_t get_offset_of_storeForce_19() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___storeForce_19)); }
	inline FsmFloat_t2883254149 * get_storeForce_19() const { return ___storeForce_19; }
	inline FsmFloat_t2883254149 ** get_address_of_storeForce_19() { return &___storeForce_19; }
	inline void set_storeForce_19(FsmFloat_t2883254149 * value)
	{
		___storeForce_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeForce_19), value);
	}

	inline static int32_t get_offset_of_cachedProxy_20() { return static_cast<int32_t>(offsetof(Collision2dEvent_t2872455664, ___cachedProxy_20)); }
	inline PlayMakerProxyBase_t90512809 * get_cachedProxy_20() const { return ___cachedProxy_20; }
	inline PlayMakerProxyBase_t90512809 ** get_address_of_cachedProxy_20() { return &___cachedProxy_20; }
	inline void set_cachedProxy_20(PlayMakerProxyBase_t90512809 * value)
	{
		___cachedProxy_20 = value;
		Il2CppCodeGenWriteBarrier((&___cachedProxy_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISION2DEVENT_T2872455664_H
#ifndef COLLISIONEVENT_T3345175258_H
#define COLLISIONEVENT_T3345175258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CollisionEvent
struct  CollisionEvent_t3345175258  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.CollisionEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.CollisionType HutongGames.PlayMaker.Actions.CollisionEvent::collision
	int32_t ___collision_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CollisionEvent::collideTag
	FsmString_t1785915204 * ___collideTag_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.CollisionEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.CollisionEvent::storeCollider
	FsmGameObject_t3581898942 * ___storeCollider_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.CollisionEvent::storeForce
	FsmFloat_t2883254149 * ___storeForce_19;
	// PlayMakerProxyBase HutongGames.PlayMaker.Actions.CollisionEvent::cachedProxy
	PlayMakerProxyBase_t90512809 * ___cachedProxy_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_collision_15() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___collision_15)); }
	inline int32_t get_collision_15() const { return ___collision_15; }
	inline int32_t* get_address_of_collision_15() { return &___collision_15; }
	inline void set_collision_15(int32_t value)
	{
		___collision_15 = value;
	}

	inline static int32_t get_offset_of_collideTag_16() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___collideTag_16)); }
	inline FsmString_t1785915204 * get_collideTag_16() const { return ___collideTag_16; }
	inline FsmString_t1785915204 ** get_address_of_collideTag_16() { return &___collideTag_16; }
	inline void set_collideTag_16(FsmString_t1785915204 * value)
	{
		___collideTag_16 = value;
		Il2CppCodeGenWriteBarrier((&___collideTag_16), value);
	}

	inline static int32_t get_offset_of_sendEvent_17() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___sendEvent_17)); }
	inline FsmEvent_t3736299882 * get_sendEvent_17() const { return ___sendEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_17() { return &___sendEvent_17; }
	inline void set_sendEvent_17(FsmEvent_t3736299882 * value)
	{
		___sendEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_17), value);
	}

	inline static int32_t get_offset_of_storeCollider_18() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___storeCollider_18)); }
	inline FsmGameObject_t3581898942 * get_storeCollider_18() const { return ___storeCollider_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeCollider_18() { return &___storeCollider_18; }
	inline void set_storeCollider_18(FsmGameObject_t3581898942 * value)
	{
		___storeCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeCollider_18), value);
	}

	inline static int32_t get_offset_of_storeForce_19() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___storeForce_19)); }
	inline FsmFloat_t2883254149 * get_storeForce_19() const { return ___storeForce_19; }
	inline FsmFloat_t2883254149 ** get_address_of_storeForce_19() { return &___storeForce_19; }
	inline void set_storeForce_19(FsmFloat_t2883254149 * value)
	{
		___storeForce_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeForce_19), value);
	}

	inline static int32_t get_offset_of_cachedProxy_20() { return static_cast<int32_t>(offsetof(CollisionEvent_t3345175258, ___cachedProxy_20)); }
	inline PlayMakerProxyBase_t90512809 * get_cachedProxy_20() const { return ___cachedProxy_20; }
	inline PlayMakerProxyBase_t90512809 ** get_address_of_cachedProxy_20() { return &___cachedProxy_20; }
	inline void set_cachedProxy_20(PlayMakerProxyBase_t90512809 * value)
	{
		___cachedProxy_20 = value;
		Il2CppCodeGenWriteBarrier((&___cachedProxy_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONEVENT_T3345175258_H
#ifndef COMPONENTACTION_1_T3816781823_H
#define COMPONENTACTION_1_T3816781823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Rigidbody2D>
struct  ComponentAction_1_t3816781823  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Rigidbody2D_t939494601 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3816781823, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t3816781823, ___cachedComponent_15)); }
	inline Rigidbody2D_t939494601 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Rigidbody2D_t939494601 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Rigidbody2D_t939494601 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T3816781823_H
#ifndef COMPONENTACTION_1_T2499100150_H
#define COMPONENTACTION_1_T2499100150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Rigidbody>
struct  ComponentAction_1_t2499100150  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Rigidbody_t3916780224 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2499100150, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2499100150, ___cachedComponent_15)); }
	inline Rigidbody_t3916780224 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Rigidbody_t3916780224 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Rigidbody_t3916780224 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2499100150_H
#ifndef EXPLOSION_T2931029749_H
#define EXPLOSION_T2931029749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Explosion
struct  Explosion_t2931029749  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Explosion::center
	FsmVector3_t626444517 * ___center_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Explosion::force
	FsmFloat_t2883254149 * ___force_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Explosion::radius
	FsmFloat_t2883254149 * ___radius_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Explosion::upwardsModifier
	FsmFloat_t2883254149 * ___upwardsModifier_17;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.Explosion::forceMode
	int32_t ___forceMode_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Explosion::layer
	FsmInt_t874273141 * ___layer_19;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.Explosion::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Explosion::invertMask
	FsmBool_t163807967 * ___invertMask_21;
	// System.Boolean HutongGames.PlayMaker.Actions.Explosion::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_center_14() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___center_14)); }
	inline FsmVector3_t626444517 * get_center_14() const { return ___center_14; }
	inline FsmVector3_t626444517 ** get_address_of_center_14() { return &___center_14; }
	inline void set_center_14(FsmVector3_t626444517 * value)
	{
		___center_14 = value;
		Il2CppCodeGenWriteBarrier((&___center_14), value);
	}

	inline static int32_t get_offset_of_force_15() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___force_15)); }
	inline FsmFloat_t2883254149 * get_force_15() const { return ___force_15; }
	inline FsmFloat_t2883254149 ** get_address_of_force_15() { return &___force_15; }
	inline void set_force_15(FsmFloat_t2883254149 * value)
	{
		___force_15 = value;
		Il2CppCodeGenWriteBarrier((&___force_15), value);
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___radius_16)); }
	inline FsmFloat_t2883254149 * get_radius_16() const { return ___radius_16; }
	inline FsmFloat_t2883254149 ** get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(FsmFloat_t2883254149 * value)
	{
		___radius_16 = value;
		Il2CppCodeGenWriteBarrier((&___radius_16), value);
	}

	inline static int32_t get_offset_of_upwardsModifier_17() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___upwardsModifier_17)); }
	inline FsmFloat_t2883254149 * get_upwardsModifier_17() const { return ___upwardsModifier_17; }
	inline FsmFloat_t2883254149 ** get_address_of_upwardsModifier_17() { return &___upwardsModifier_17; }
	inline void set_upwardsModifier_17(FsmFloat_t2883254149 * value)
	{
		___upwardsModifier_17 = value;
		Il2CppCodeGenWriteBarrier((&___upwardsModifier_17), value);
	}

	inline static int32_t get_offset_of_forceMode_18() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___forceMode_18)); }
	inline int32_t get_forceMode_18() const { return ___forceMode_18; }
	inline int32_t* get_address_of_forceMode_18() { return &___forceMode_18; }
	inline void set_forceMode_18(int32_t value)
	{
		___forceMode_18 = value;
	}

	inline static int32_t get_offset_of_layer_19() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___layer_19)); }
	inline FsmInt_t874273141 * get_layer_19() const { return ___layer_19; }
	inline FsmInt_t874273141 ** get_address_of_layer_19() { return &___layer_19; }
	inline void set_layer_19(FsmInt_t874273141 * value)
	{
		___layer_19 = value;
		Il2CppCodeGenWriteBarrier((&___layer_19), value);
	}

	inline static int32_t get_offset_of_layerMask_20() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___layerMask_20)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_20() const { return ___layerMask_20; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_20() { return &___layerMask_20; }
	inline void set_layerMask_20(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_20 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_20), value);
	}

	inline static int32_t get_offset_of_invertMask_21() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___invertMask_21)); }
	inline FsmBool_t163807967 * get_invertMask_21() const { return ___invertMask_21; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_21() { return &___invertMask_21; }
	inline void set_invertMask_21(FsmBool_t163807967 * value)
	{
		___invertMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(Explosion_t2931029749, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSION_T2931029749_H
#ifndef FLOATADDMULTIPLE_T200444465_H
#define FLOATADDMULTIPLE_T200444465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatAddMultiple
struct  FloatAddMultiple_t200444465  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.FloatAddMultiple::floatVariables
	FsmFloatU5BU5D_t3637897416* ___floatVariables_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatAddMultiple::addTo
	FsmFloat_t2883254149 * ___addTo_15;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatAddMultiple::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_floatVariables_14() { return static_cast<int32_t>(offsetof(FloatAddMultiple_t200444465, ___floatVariables_14)); }
	inline FsmFloatU5BU5D_t3637897416* get_floatVariables_14() const { return ___floatVariables_14; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_floatVariables_14() { return &___floatVariables_14; }
	inline void set_floatVariables_14(FsmFloatU5BU5D_t3637897416* value)
	{
		___floatVariables_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariables_14), value);
	}

	inline static int32_t get_offset_of_addTo_15() { return static_cast<int32_t>(offsetof(FloatAddMultiple_t200444465, ___addTo_15)); }
	inline FsmFloat_t2883254149 * get_addTo_15() const { return ___addTo_15; }
	inline FsmFloat_t2883254149 ** get_address_of_addTo_15() { return &___addTo_15; }
	inline void set_addTo_15(FsmFloat_t2883254149 * value)
	{
		___addTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___addTo_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(FloatAddMultiple_t200444465, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATADDMULTIPLE_T200444465_H
#ifndef FLOATCLAMP_T3804449708_H
#define FLOATCLAMP_T3804449708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatClamp
struct  FloatClamp_t3804449708  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatClamp::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatClamp::minValue
	FsmFloat_t2883254149 * ___minValue_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatClamp::maxValue
	FsmFloat_t2883254149 * ___maxValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatClamp::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatClamp_t3804449708, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_minValue_15() { return static_cast<int32_t>(offsetof(FloatClamp_t3804449708, ___minValue_15)); }
	inline FsmFloat_t2883254149 * get_minValue_15() const { return ___minValue_15; }
	inline FsmFloat_t2883254149 ** get_address_of_minValue_15() { return &___minValue_15; }
	inline void set_minValue_15(FsmFloat_t2883254149 * value)
	{
		___minValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___minValue_15), value);
	}

	inline static int32_t get_offset_of_maxValue_16() { return static_cast<int32_t>(offsetof(FloatClamp_t3804449708, ___maxValue_16)); }
	inline FsmFloat_t2883254149 * get_maxValue_16() const { return ___maxValue_16; }
	inline FsmFloat_t2883254149 ** get_address_of_maxValue_16() { return &___maxValue_16; }
	inline void set_maxValue_16(FsmFloat_t2883254149 * value)
	{
		___maxValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___maxValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(FloatClamp_t3804449708, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATCLAMP_T3804449708_H
#ifndef FLOATDIVIDE_T2012883063_H
#define FLOATDIVIDE_T2012883063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatDivide
struct  FloatDivide_t2012883063  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatDivide::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatDivide::divideBy
	FsmFloat_t2883254149 * ___divideBy_15;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatDivide::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatDivide_t2012883063, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_divideBy_15() { return static_cast<int32_t>(offsetof(FloatDivide_t2012883063, ___divideBy_15)); }
	inline FsmFloat_t2883254149 * get_divideBy_15() const { return ___divideBy_15; }
	inline FsmFloat_t2883254149 ** get_address_of_divideBy_15() { return &___divideBy_15; }
	inline void set_divideBy_15(FsmFloat_t2883254149 * value)
	{
		___divideBy_15 = value;
		Il2CppCodeGenWriteBarrier((&___divideBy_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(FloatDivide_t2012883063, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATDIVIDE_T2012883063_H
#ifndef FLOATINTERPOLATE_T1692993408_H
#define FLOATINTERPOLATE_T1692993408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatInterpolate
struct  FloatInterpolate_t1692993408  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.InterpolationType HutongGames.PlayMaker.Actions.FloatInterpolate::mode
	int32_t ___mode_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::fromFloat
	FsmFloat_t2883254149 * ___fromFloat_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::toFloat
	FsmFloat_t2883254149 * ___toFloat_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::time
	FsmFloat_t2883254149 * ___time_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatInterpolate::storeResult
	FsmFloat_t2883254149 * ___storeResult_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatInterpolate::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatInterpolate::realTime
	bool ___realTime_20;
	// System.Single HutongGames.PlayMaker.Actions.FloatInterpolate::startTime
	float ___startTime_21;
	// System.Single HutongGames.PlayMaker.Actions.FloatInterpolate::currentTime
	float ___currentTime_22;

public:
	inline static int32_t get_offset_of_mode_14() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___mode_14)); }
	inline int32_t get_mode_14() const { return ___mode_14; }
	inline int32_t* get_address_of_mode_14() { return &___mode_14; }
	inline void set_mode_14(int32_t value)
	{
		___mode_14 = value;
	}

	inline static int32_t get_offset_of_fromFloat_15() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___fromFloat_15)); }
	inline FsmFloat_t2883254149 * get_fromFloat_15() const { return ___fromFloat_15; }
	inline FsmFloat_t2883254149 ** get_address_of_fromFloat_15() { return &___fromFloat_15; }
	inline void set_fromFloat_15(FsmFloat_t2883254149 * value)
	{
		___fromFloat_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromFloat_15), value);
	}

	inline static int32_t get_offset_of_toFloat_16() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___toFloat_16)); }
	inline FsmFloat_t2883254149 * get_toFloat_16() const { return ___toFloat_16; }
	inline FsmFloat_t2883254149 ** get_address_of_toFloat_16() { return &___toFloat_16; }
	inline void set_toFloat_16(FsmFloat_t2883254149 * value)
	{
		___toFloat_16 = value;
		Il2CppCodeGenWriteBarrier((&___toFloat_16), value);
	}

	inline static int32_t get_offset_of_time_17() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___time_17)); }
	inline FsmFloat_t2883254149 * get_time_17() const { return ___time_17; }
	inline FsmFloat_t2883254149 ** get_address_of_time_17() { return &___time_17; }
	inline void set_time_17(FsmFloat_t2883254149 * value)
	{
		___time_17 = value;
		Il2CppCodeGenWriteBarrier((&___time_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___storeResult_18)); }
	inline FsmFloat_t2883254149 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmFloat_t2883254149 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___finishEvent_19)); }
	inline FsmEvent_t3736299882 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t3736299882 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_19), value);
	}

	inline static int32_t get_offset_of_realTime_20() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___realTime_20)); }
	inline bool get_realTime_20() const { return ___realTime_20; }
	inline bool* get_address_of_realTime_20() { return &___realTime_20; }
	inline void set_realTime_20(bool value)
	{
		___realTime_20 = value;
	}

	inline static int32_t get_offset_of_startTime_21() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___startTime_21)); }
	inline float get_startTime_21() const { return ___startTime_21; }
	inline float* get_address_of_startTime_21() { return &___startTime_21; }
	inline void set_startTime_21(float value)
	{
		___startTime_21 = value;
	}

	inline static int32_t get_offset_of_currentTime_22() { return static_cast<int32_t>(offsetof(FloatInterpolate_t1692993408, ___currentTime_22)); }
	inline float get_currentTime_22() const { return ___currentTime_22; }
	inline float* get_address_of_currentTime_22() { return &___currentTime_22; }
	inline void set_currentTime_22(float value)
	{
		___currentTime_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATINTERPOLATE_T1692993408_H
#ifndef FLOATMULTIPLY_T1744521690_H
#define FLOATMULTIPLY_T1744521690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatMultiply
struct  FloatMultiply_t1744521690  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatMultiply::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatMultiply::multiplyBy
	FsmFloat_t2883254149 * ___multiplyBy_15;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatMultiply::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatMultiply_t1744521690, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_multiplyBy_15() { return static_cast<int32_t>(offsetof(FloatMultiply_t1744521690, ___multiplyBy_15)); }
	inline FsmFloat_t2883254149 * get_multiplyBy_15() const { return ___multiplyBy_15; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplyBy_15() { return &___multiplyBy_15; }
	inline void set_multiplyBy_15(FsmFloat_t2883254149 * value)
	{
		___multiplyBy_15 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyBy_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(FloatMultiply_t1744521690, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATMULTIPLY_T1744521690_H
#ifndef FLOATOPERATOR_T3450066926_H
#define FLOATOPERATOR_T3450066926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatOperator
struct  FloatOperator_t3450066926  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatOperator::float1
	FsmFloat_t2883254149 * ___float1_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatOperator::float2
	FsmFloat_t2883254149 * ___float2_15;
	// HutongGames.PlayMaker.Actions.FloatOperator/Operation HutongGames.PlayMaker.Actions.FloatOperator::operation
	int32_t ___operation_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatOperator::storeResult
	FsmFloat_t2883254149 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatOperator::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_float1_14() { return static_cast<int32_t>(offsetof(FloatOperator_t3450066926, ___float1_14)); }
	inline FsmFloat_t2883254149 * get_float1_14() const { return ___float1_14; }
	inline FsmFloat_t2883254149 ** get_address_of_float1_14() { return &___float1_14; }
	inline void set_float1_14(FsmFloat_t2883254149 * value)
	{
		___float1_14 = value;
		Il2CppCodeGenWriteBarrier((&___float1_14), value);
	}

	inline static int32_t get_offset_of_float2_15() { return static_cast<int32_t>(offsetof(FloatOperator_t3450066926, ___float2_15)); }
	inline FsmFloat_t2883254149 * get_float2_15() const { return ___float2_15; }
	inline FsmFloat_t2883254149 ** get_address_of_float2_15() { return &___float2_15; }
	inline void set_float2_15(FsmFloat_t2883254149 * value)
	{
		___float2_15 = value;
		Il2CppCodeGenWriteBarrier((&___float2_15), value);
	}

	inline static int32_t get_offset_of_operation_16() { return static_cast<int32_t>(offsetof(FloatOperator_t3450066926, ___operation_16)); }
	inline int32_t get_operation_16() const { return ___operation_16; }
	inline int32_t* get_address_of_operation_16() { return &___operation_16; }
	inline void set_operation_16(int32_t value)
	{
		___operation_16 = value;
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(FloatOperator_t3450066926, ___storeResult_17)); }
	inline FsmFloat_t2883254149 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmFloat_t2883254149 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(FloatOperator_t3450066926, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATOPERATOR_T3450066926_H
#ifndef FLOATSUBTRACT_T2940113104_H
#define FLOATSUBTRACT_T2940113104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatSubtract
struct  FloatSubtract_t2940113104  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSubtract::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSubtract::subtract
	FsmFloat_t2883254149 * ___subtract_15;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSubtract::everyFrame
	bool ___everyFrame_16;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSubtract::perSecond
	bool ___perSecond_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatSubtract_t2940113104, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_subtract_15() { return static_cast<int32_t>(offsetof(FloatSubtract_t2940113104, ___subtract_15)); }
	inline FsmFloat_t2883254149 * get_subtract_15() const { return ___subtract_15; }
	inline FsmFloat_t2883254149 ** get_address_of_subtract_15() { return &___subtract_15; }
	inline void set_subtract_15(FsmFloat_t2883254149 * value)
	{
		___subtract_15 = value;
		Il2CppCodeGenWriteBarrier((&___subtract_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(FloatSubtract_t2940113104, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_perSecond_17() { return static_cast<int32_t>(offsetof(FloatSubtract_t2940113104, ___perSecond_17)); }
	inline bool get_perSecond_17() const { return ___perSecond_17; }
	inline bool* get_address_of_perSecond_17() { return &___perSecond_17; }
	inline void set_perSecond_17(bool value)
	{
		___perSecond_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATSUBTRACT_T2940113104_H
#ifndef GETCOLLISION2DINFO_T1725384043_H
#define GETCOLLISION2DINFO_T1725384043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetCollision2dInfo
struct  GetCollision2dInfo_t1725384043  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetCollision2dInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollision2dInfo::relativeVelocity
	FsmVector3_t626444517 * ___relativeVelocity_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCollision2dInfo::relativeSpeed
	FsmFloat_t2883254149 * ___relativeSpeed_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollision2dInfo::contactPoint
	FsmVector3_t626444517 * ___contactPoint_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollision2dInfo::contactNormal
	FsmVector3_t626444517 * ___contactNormal_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetCollision2dInfo::shapeCount
	FsmInt_t874273141 * ___shapeCount_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetCollision2dInfo::physics2dMaterialName
	FsmString_t1785915204 * ___physics2dMaterialName_20;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_relativeVelocity_15() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___relativeVelocity_15)); }
	inline FsmVector3_t626444517 * get_relativeVelocity_15() const { return ___relativeVelocity_15; }
	inline FsmVector3_t626444517 ** get_address_of_relativeVelocity_15() { return &___relativeVelocity_15; }
	inline void set_relativeVelocity_15(FsmVector3_t626444517 * value)
	{
		___relativeVelocity_15 = value;
		Il2CppCodeGenWriteBarrier((&___relativeVelocity_15), value);
	}

	inline static int32_t get_offset_of_relativeSpeed_16() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___relativeSpeed_16)); }
	inline FsmFloat_t2883254149 * get_relativeSpeed_16() const { return ___relativeSpeed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_relativeSpeed_16() { return &___relativeSpeed_16; }
	inline void set_relativeSpeed_16(FsmFloat_t2883254149 * value)
	{
		___relativeSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&___relativeSpeed_16), value);
	}

	inline static int32_t get_offset_of_contactPoint_17() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___contactPoint_17)); }
	inline FsmVector3_t626444517 * get_contactPoint_17() const { return ___contactPoint_17; }
	inline FsmVector3_t626444517 ** get_address_of_contactPoint_17() { return &___contactPoint_17; }
	inline void set_contactPoint_17(FsmVector3_t626444517 * value)
	{
		___contactPoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___contactPoint_17), value);
	}

	inline static int32_t get_offset_of_contactNormal_18() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___contactNormal_18)); }
	inline FsmVector3_t626444517 * get_contactNormal_18() const { return ___contactNormal_18; }
	inline FsmVector3_t626444517 ** get_address_of_contactNormal_18() { return &___contactNormal_18; }
	inline void set_contactNormal_18(FsmVector3_t626444517 * value)
	{
		___contactNormal_18 = value;
		Il2CppCodeGenWriteBarrier((&___contactNormal_18), value);
	}

	inline static int32_t get_offset_of_shapeCount_19() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___shapeCount_19)); }
	inline FsmInt_t874273141 * get_shapeCount_19() const { return ___shapeCount_19; }
	inline FsmInt_t874273141 ** get_address_of_shapeCount_19() { return &___shapeCount_19; }
	inline void set_shapeCount_19(FsmInt_t874273141 * value)
	{
		___shapeCount_19 = value;
		Il2CppCodeGenWriteBarrier((&___shapeCount_19), value);
	}

	inline static int32_t get_offset_of_physics2dMaterialName_20() { return static_cast<int32_t>(offsetof(GetCollision2dInfo_t1725384043, ___physics2dMaterialName_20)); }
	inline FsmString_t1785915204 * get_physics2dMaterialName_20() const { return ___physics2dMaterialName_20; }
	inline FsmString_t1785915204 ** get_address_of_physics2dMaterialName_20() { return &___physics2dMaterialName_20; }
	inline void set_physics2dMaterialName_20(FsmString_t1785915204 * value)
	{
		___physics2dMaterialName_20 = value;
		Il2CppCodeGenWriteBarrier((&___physics2dMaterialName_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLISION2DINFO_T1725384043_H
#ifndef GETCOLLISIONINFO_T3436222585_H
#define GETCOLLISIONINFO_T3436222585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetCollisionInfo
struct  GetCollisionInfo_t3436222585  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetCollisionInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollisionInfo::relativeVelocity
	FsmVector3_t626444517 * ___relativeVelocity_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCollisionInfo::relativeSpeed
	FsmFloat_t2883254149 * ___relativeSpeed_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollisionInfo::contactPoint
	FsmVector3_t626444517 * ___contactPoint_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCollisionInfo::contactNormal
	FsmVector3_t626444517 * ___contactNormal_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetCollisionInfo::physicsMaterialName
	FsmString_t1785915204 * ___physicsMaterialName_19;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3436222585, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_relativeVelocity_15() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3436222585, ___relativeVelocity_15)); }
	inline FsmVector3_t626444517 * get_relativeVelocity_15() const { return ___relativeVelocity_15; }
	inline FsmVector3_t626444517 ** get_address_of_relativeVelocity_15() { return &___relativeVelocity_15; }
	inline void set_relativeVelocity_15(FsmVector3_t626444517 * value)
	{
		___relativeVelocity_15 = value;
		Il2CppCodeGenWriteBarrier((&___relativeVelocity_15), value);
	}

	inline static int32_t get_offset_of_relativeSpeed_16() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3436222585, ___relativeSpeed_16)); }
	inline FsmFloat_t2883254149 * get_relativeSpeed_16() const { return ___relativeSpeed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_relativeSpeed_16() { return &___relativeSpeed_16; }
	inline void set_relativeSpeed_16(FsmFloat_t2883254149 * value)
	{
		___relativeSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&___relativeSpeed_16), value);
	}

	inline static int32_t get_offset_of_contactPoint_17() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3436222585, ___contactPoint_17)); }
	inline FsmVector3_t626444517 * get_contactPoint_17() const { return ___contactPoint_17; }
	inline FsmVector3_t626444517 ** get_address_of_contactPoint_17() { return &___contactPoint_17; }
	inline void set_contactPoint_17(FsmVector3_t626444517 * value)
	{
		___contactPoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___contactPoint_17), value);
	}

	inline static int32_t get_offset_of_contactNormal_18() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3436222585, ___contactNormal_18)); }
	inline FsmVector3_t626444517 * get_contactNormal_18() const { return ___contactNormal_18; }
	inline FsmVector3_t626444517 ** get_address_of_contactNormal_18() { return &___contactNormal_18; }
	inline void set_contactNormal_18(FsmVector3_t626444517 * value)
	{
		___contactNormal_18 = value;
		Il2CppCodeGenWriteBarrier((&___contactNormal_18), value);
	}

	inline static int32_t get_offset_of_physicsMaterialName_19() { return static_cast<int32_t>(offsetof(GetCollisionInfo_t3436222585, ___physicsMaterialName_19)); }
	inline FsmString_t1785915204 * get_physicsMaterialName_19() const { return ___physicsMaterialName_19; }
	inline FsmString_t1785915204 ** get_address_of_physicsMaterialName_19() { return &___physicsMaterialName_19; }
	inline void set_physicsMaterialName_19(FsmString_t1785915204 * value)
	{
		___physicsMaterialName_19 = value;
		Il2CppCodeGenWriteBarrier((&___physicsMaterialName_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETCOLLISIONINFO_T3436222585_H
#ifndef GETJOINTBREAK2DINFO_T519263708_H
#define GETJOINTBREAK2DINFO_T519263708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetJointBreak2dInfo
struct  GetJointBreak2dInfo_t519263708  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::brokenJoint
	FsmObject_t2606870197 * ___brokenJoint_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::reactionForce
	FsmVector2_t2965096677 * ___reactionForce_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::reactionForceMagnitude
	FsmFloat_t2883254149 * ___reactionForceMagnitude_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::reactionTorque
	FsmFloat_t2883254149 * ___reactionTorque_17;

public:
	inline static int32_t get_offset_of_brokenJoint_14() { return static_cast<int32_t>(offsetof(GetJointBreak2dInfo_t519263708, ___brokenJoint_14)); }
	inline FsmObject_t2606870197 * get_brokenJoint_14() const { return ___brokenJoint_14; }
	inline FsmObject_t2606870197 ** get_address_of_brokenJoint_14() { return &___brokenJoint_14; }
	inline void set_brokenJoint_14(FsmObject_t2606870197 * value)
	{
		___brokenJoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___brokenJoint_14), value);
	}

	inline static int32_t get_offset_of_reactionForce_15() { return static_cast<int32_t>(offsetof(GetJointBreak2dInfo_t519263708, ___reactionForce_15)); }
	inline FsmVector2_t2965096677 * get_reactionForce_15() const { return ___reactionForce_15; }
	inline FsmVector2_t2965096677 ** get_address_of_reactionForce_15() { return &___reactionForce_15; }
	inline void set_reactionForce_15(FsmVector2_t2965096677 * value)
	{
		___reactionForce_15 = value;
		Il2CppCodeGenWriteBarrier((&___reactionForce_15), value);
	}

	inline static int32_t get_offset_of_reactionForceMagnitude_16() { return static_cast<int32_t>(offsetof(GetJointBreak2dInfo_t519263708, ___reactionForceMagnitude_16)); }
	inline FsmFloat_t2883254149 * get_reactionForceMagnitude_16() const { return ___reactionForceMagnitude_16; }
	inline FsmFloat_t2883254149 ** get_address_of_reactionForceMagnitude_16() { return &___reactionForceMagnitude_16; }
	inline void set_reactionForceMagnitude_16(FsmFloat_t2883254149 * value)
	{
		___reactionForceMagnitude_16 = value;
		Il2CppCodeGenWriteBarrier((&___reactionForceMagnitude_16), value);
	}

	inline static int32_t get_offset_of_reactionTorque_17() { return static_cast<int32_t>(offsetof(GetJointBreak2dInfo_t519263708, ___reactionTorque_17)); }
	inline FsmFloat_t2883254149 * get_reactionTorque_17() const { return ___reactionTorque_17; }
	inline FsmFloat_t2883254149 ** get_address_of_reactionTorque_17() { return &___reactionTorque_17; }
	inline void set_reactionTorque_17(FsmFloat_t2883254149 * value)
	{
		___reactionTorque_17 = value;
		Il2CppCodeGenWriteBarrier((&___reactionTorque_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETJOINTBREAK2DINFO_T519263708_H
#ifndef GETJOINTBREAKINFO_T181654311_H
#define GETJOINTBREAKINFO_T181654311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetJointBreakInfo
struct  GetJointBreakInfo_t181654311  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetJointBreakInfo::breakForce
	FsmFloat_t2883254149 * ___breakForce_14;

public:
	inline static int32_t get_offset_of_breakForce_14() { return static_cast<int32_t>(offsetof(GetJointBreakInfo_t181654311, ___breakForce_14)); }
	inline FsmFloat_t2883254149 * get_breakForce_14() const { return ___breakForce_14; }
	inline FsmFloat_t2883254149 ** get_address_of_breakForce_14() { return &___breakForce_14; }
	inline void set_breakForce_14(FsmFloat_t2883254149 * value)
	{
		___breakForce_14 = value;
		Il2CppCodeGenWriteBarrier((&___breakForce_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETJOINTBREAKINFO_T181654311_H
#ifndef GETNEXTLINECAST2D_T4255660837_H
#define GETNEXTLINECAST2D_T4255660837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextLineCast2d
struct  GetNextLineCast2d_t4255660837  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextLineCast2d::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextLineCast2d::fromPosition
	FsmVector2_t2965096677 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextLineCast2d::toGameObject
	FsmGameObject_t3581898942 * ___toGameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextLineCast2d::toPosition
	FsmVector2_t2965096677 * ___toPosition_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextLineCast2d::minDepth
	FsmInt_t874273141 * ___minDepth_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextLineCast2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextLineCast2d::resetFlag
	FsmBool_t163807967 * ___resetFlag_20;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.GetNextLineCast2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextLineCast2d::invertMask
	FsmBool_t163807967 * ___invertMask_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextLineCast2d::collidersCount
	FsmInt_t874273141 * ___collidersCount_23;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextLineCast2d::storeNextCollider
	FsmGameObject_t3581898942 * ___storeNextCollider_24;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextLineCast2d::storeNextHitPoint
	FsmVector2_t2965096677 * ___storeNextHitPoint_25;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextLineCast2d::storeNextHitNormal
	FsmVector2_t2965096677 * ___storeNextHitNormal_26;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetNextLineCast2d::storeNextHitDistance
	FsmFloat_t2883254149 * ___storeNextHitDistance_27;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextLineCast2d::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_28;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextLineCast2d::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_29;
	// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextLineCast2d::hits
	RaycastHit2DU5BU5D_t4286651560* ___hits_30;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextLineCast2d::colliderCount
	int32_t ___colliderCount_31;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextLineCast2d::nextColliderIndex
	int32_t ___nextColliderIndex_32;

public:
	inline static int32_t get_offset_of_fromGameObject_14() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___fromGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_14() const { return ___fromGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_14() { return &___fromGameObject_14; }
	inline void set_fromGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___fromPosition_15)); }
	inline FsmVector2_t2965096677 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector2_t2965096677 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector2_t2965096677 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_toGameObject_16() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___toGameObject_16)); }
	inline FsmGameObject_t3581898942 * get_toGameObject_16() const { return ___toGameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_toGameObject_16() { return &___toGameObject_16; }
	inline void set_toGameObject_16(FsmGameObject_t3581898942 * value)
	{
		___toGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___toGameObject_16), value);
	}

	inline static int32_t get_offset_of_toPosition_17() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___toPosition_17)); }
	inline FsmVector2_t2965096677 * get_toPosition_17() const { return ___toPosition_17; }
	inline FsmVector2_t2965096677 ** get_address_of_toPosition_17() { return &___toPosition_17; }
	inline void set_toPosition_17(FsmVector2_t2965096677 * value)
	{
		___toPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___toPosition_17), value);
	}

	inline static int32_t get_offset_of_minDepth_18() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___minDepth_18)); }
	inline FsmInt_t874273141 * get_minDepth_18() const { return ___minDepth_18; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_18() { return &___minDepth_18; }
	inline void set_minDepth_18(FsmInt_t874273141 * value)
	{
		___minDepth_18 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_18), value);
	}

	inline static int32_t get_offset_of_maxDepth_19() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___maxDepth_19)); }
	inline FsmInt_t874273141 * get_maxDepth_19() const { return ___maxDepth_19; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_19() { return &___maxDepth_19; }
	inline void set_maxDepth_19(FsmInt_t874273141 * value)
	{
		___maxDepth_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_19), value);
	}

	inline static int32_t get_offset_of_resetFlag_20() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___resetFlag_20)); }
	inline FsmBool_t163807967 * get_resetFlag_20() const { return ___resetFlag_20; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_20() { return &___resetFlag_20; }
	inline void set_resetFlag_20(FsmBool_t163807967 * value)
	{
		___resetFlag_20 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_20), value);
	}

	inline static int32_t get_offset_of_layerMask_21() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___layerMask_21)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_21() const { return ___layerMask_21; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_21() { return &___layerMask_21; }
	inline void set_layerMask_21(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_21), value);
	}

	inline static int32_t get_offset_of_invertMask_22() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___invertMask_22)); }
	inline FsmBool_t163807967 * get_invertMask_22() const { return ___invertMask_22; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_22() { return &___invertMask_22; }
	inline void set_invertMask_22(FsmBool_t163807967 * value)
	{
		___invertMask_22 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_22), value);
	}

	inline static int32_t get_offset_of_collidersCount_23() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___collidersCount_23)); }
	inline FsmInt_t874273141 * get_collidersCount_23() const { return ___collidersCount_23; }
	inline FsmInt_t874273141 ** get_address_of_collidersCount_23() { return &___collidersCount_23; }
	inline void set_collidersCount_23(FsmInt_t874273141 * value)
	{
		___collidersCount_23 = value;
		Il2CppCodeGenWriteBarrier((&___collidersCount_23), value);
	}

	inline static int32_t get_offset_of_storeNextCollider_24() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___storeNextCollider_24)); }
	inline FsmGameObject_t3581898942 * get_storeNextCollider_24() const { return ___storeNextCollider_24; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeNextCollider_24() { return &___storeNextCollider_24; }
	inline void set_storeNextCollider_24(FsmGameObject_t3581898942 * value)
	{
		___storeNextCollider_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextCollider_24), value);
	}

	inline static int32_t get_offset_of_storeNextHitPoint_25() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___storeNextHitPoint_25)); }
	inline FsmVector2_t2965096677 * get_storeNextHitPoint_25() const { return ___storeNextHitPoint_25; }
	inline FsmVector2_t2965096677 ** get_address_of_storeNextHitPoint_25() { return &___storeNextHitPoint_25; }
	inline void set_storeNextHitPoint_25(FsmVector2_t2965096677 * value)
	{
		___storeNextHitPoint_25 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitPoint_25), value);
	}

	inline static int32_t get_offset_of_storeNextHitNormal_26() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___storeNextHitNormal_26)); }
	inline FsmVector2_t2965096677 * get_storeNextHitNormal_26() const { return ___storeNextHitNormal_26; }
	inline FsmVector2_t2965096677 ** get_address_of_storeNextHitNormal_26() { return &___storeNextHitNormal_26; }
	inline void set_storeNextHitNormal_26(FsmVector2_t2965096677 * value)
	{
		___storeNextHitNormal_26 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitNormal_26), value);
	}

	inline static int32_t get_offset_of_storeNextHitDistance_27() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___storeNextHitDistance_27)); }
	inline FsmFloat_t2883254149 * get_storeNextHitDistance_27() const { return ___storeNextHitDistance_27; }
	inline FsmFloat_t2883254149 ** get_address_of_storeNextHitDistance_27() { return &___storeNextHitDistance_27; }
	inline void set_storeNextHitDistance_27(FsmFloat_t2883254149 * value)
	{
		___storeNextHitDistance_27 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitDistance_27), value);
	}

	inline static int32_t get_offset_of_loopEvent_28() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___loopEvent_28)); }
	inline FsmEvent_t3736299882 * get_loopEvent_28() const { return ___loopEvent_28; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_28() { return &___loopEvent_28; }
	inline void set_loopEvent_28(FsmEvent_t3736299882 * value)
	{
		___loopEvent_28 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_28), value);
	}

	inline static int32_t get_offset_of_finishedEvent_29() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___finishedEvent_29)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_29() const { return ___finishedEvent_29; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_29() { return &___finishedEvent_29; }
	inline void set_finishedEvent_29(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_29 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_29), value);
	}

	inline static int32_t get_offset_of_hits_30() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___hits_30)); }
	inline RaycastHit2DU5BU5D_t4286651560* get_hits_30() const { return ___hits_30; }
	inline RaycastHit2DU5BU5D_t4286651560** get_address_of_hits_30() { return &___hits_30; }
	inline void set_hits_30(RaycastHit2DU5BU5D_t4286651560* value)
	{
		___hits_30 = value;
		Il2CppCodeGenWriteBarrier((&___hits_30), value);
	}

	inline static int32_t get_offset_of_colliderCount_31() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___colliderCount_31)); }
	inline int32_t get_colliderCount_31() const { return ___colliderCount_31; }
	inline int32_t* get_address_of_colliderCount_31() { return &___colliderCount_31; }
	inline void set_colliderCount_31(int32_t value)
	{
		___colliderCount_31 = value;
	}

	inline static int32_t get_offset_of_nextColliderIndex_32() { return static_cast<int32_t>(offsetof(GetNextLineCast2d_t4255660837, ___nextColliderIndex_32)); }
	inline int32_t get_nextColliderIndex_32() const { return ___nextColliderIndex_32; }
	inline int32_t* get_address_of_nextColliderIndex_32() { return &___nextColliderIndex_32; }
	inline void set_nextColliderIndex_32(int32_t value)
	{
		___nextColliderIndex_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEXTLINECAST2D_T4255660837_H
#ifndef GETNEXTOVERLAPAREA2D_T3488004213_H
#define GETNEXTOVERLAPAREA2D_T3488004213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextOverlapArea2d
struct  GetNextOverlapArea2d_t3488004213  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::firstCornerGameObject
	FsmOwnerDefault_t3590610434 * ___firstCornerGameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::firstCornerPosition
	FsmVector2_t2965096677 * ___firstCornerPosition_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::secondCornerGameObject
	FsmGameObject_t3581898942 * ___secondCornerGameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::secondCornerPosition
	FsmVector2_t2965096677 * ___secondCornerPosition_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::minDepth
	FsmInt_t874273141 * ___minDepth_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::resetFlag
	FsmBool_t163807967 * ___resetFlag_20;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::invertMask
	FsmBool_t163807967 * ___invertMask_22;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::collidersCount
	FsmInt_t874273141 * ___collidersCount_23;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::storeNextCollider
	FsmGameObject_t3581898942 * ___storeNextCollider_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_26;
	// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::colliders
	Collider2DU5BU5D_t1693969295* ___colliders_27;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::colliderCount
	int32_t ___colliderCount_28;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::nextColliderIndex
	int32_t ___nextColliderIndex_29;

public:
	inline static int32_t get_offset_of_firstCornerGameObject_14() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___firstCornerGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_firstCornerGameObject_14() const { return ___firstCornerGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_firstCornerGameObject_14() { return &___firstCornerGameObject_14; }
	inline void set_firstCornerGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___firstCornerGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___firstCornerGameObject_14), value);
	}

	inline static int32_t get_offset_of_firstCornerPosition_15() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___firstCornerPosition_15)); }
	inline FsmVector2_t2965096677 * get_firstCornerPosition_15() const { return ___firstCornerPosition_15; }
	inline FsmVector2_t2965096677 ** get_address_of_firstCornerPosition_15() { return &___firstCornerPosition_15; }
	inline void set_firstCornerPosition_15(FsmVector2_t2965096677 * value)
	{
		___firstCornerPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___firstCornerPosition_15), value);
	}

	inline static int32_t get_offset_of_secondCornerGameObject_16() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___secondCornerGameObject_16)); }
	inline FsmGameObject_t3581898942 * get_secondCornerGameObject_16() const { return ___secondCornerGameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_secondCornerGameObject_16() { return &___secondCornerGameObject_16; }
	inline void set_secondCornerGameObject_16(FsmGameObject_t3581898942 * value)
	{
		___secondCornerGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___secondCornerGameObject_16), value);
	}

	inline static int32_t get_offset_of_secondCornerPosition_17() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___secondCornerPosition_17)); }
	inline FsmVector2_t2965096677 * get_secondCornerPosition_17() const { return ___secondCornerPosition_17; }
	inline FsmVector2_t2965096677 ** get_address_of_secondCornerPosition_17() { return &___secondCornerPosition_17; }
	inline void set_secondCornerPosition_17(FsmVector2_t2965096677 * value)
	{
		___secondCornerPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___secondCornerPosition_17), value);
	}

	inline static int32_t get_offset_of_minDepth_18() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___minDepth_18)); }
	inline FsmInt_t874273141 * get_minDepth_18() const { return ___minDepth_18; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_18() { return &___minDepth_18; }
	inline void set_minDepth_18(FsmInt_t874273141 * value)
	{
		___minDepth_18 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_18), value);
	}

	inline static int32_t get_offset_of_maxDepth_19() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___maxDepth_19)); }
	inline FsmInt_t874273141 * get_maxDepth_19() const { return ___maxDepth_19; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_19() { return &___maxDepth_19; }
	inline void set_maxDepth_19(FsmInt_t874273141 * value)
	{
		___maxDepth_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_19), value);
	}

	inline static int32_t get_offset_of_resetFlag_20() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___resetFlag_20)); }
	inline FsmBool_t163807967 * get_resetFlag_20() const { return ___resetFlag_20; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_20() { return &___resetFlag_20; }
	inline void set_resetFlag_20(FsmBool_t163807967 * value)
	{
		___resetFlag_20 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_20), value);
	}

	inline static int32_t get_offset_of_layerMask_21() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___layerMask_21)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_21() const { return ___layerMask_21; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_21() { return &___layerMask_21; }
	inline void set_layerMask_21(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_21), value);
	}

	inline static int32_t get_offset_of_invertMask_22() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___invertMask_22)); }
	inline FsmBool_t163807967 * get_invertMask_22() const { return ___invertMask_22; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_22() { return &___invertMask_22; }
	inline void set_invertMask_22(FsmBool_t163807967 * value)
	{
		___invertMask_22 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_22), value);
	}

	inline static int32_t get_offset_of_collidersCount_23() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___collidersCount_23)); }
	inline FsmInt_t874273141 * get_collidersCount_23() const { return ___collidersCount_23; }
	inline FsmInt_t874273141 ** get_address_of_collidersCount_23() { return &___collidersCount_23; }
	inline void set_collidersCount_23(FsmInt_t874273141 * value)
	{
		___collidersCount_23 = value;
		Il2CppCodeGenWriteBarrier((&___collidersCount_23), value);
	}

	inline static int32_t get_offset_of_storeNextCollider_24() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___storeNextCollider_24)); }
	inline FsmGameObject_t3581898942 * get_storeNextCollider_24() const { return ___storeNextCollider_24; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeNextCollider_24() { return &___storeNextCollider_24; }
	inline void set_storeNextCollider_24(FsmGameObject_t3581898942 * value)
	{
		___storeNextCollider_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextCollider_24), value);
	}

	inline static int32_t get_offset_of_loopEvent_25() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___loopEvent_25)); }
	inline FsmEvent_t3736299882 * get_loopEvent_25() const { return ___loopEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_25() { return &___loopEvent_25; }
	inline void set_loopEvent_25(FsmEvent_t3736299882 * value)
	{
		___loopEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_25), value);
	}

	inline static int32_t get_offset_of_finishedEvent_26() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___finishedEvent_26)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_26() const { return ___finishedEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_26() { return &___finishedEvent_26; }
	inline void set_finishedEvent_26(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_26), value);
	}

	inline static int32_t get_offset_of_colliders_27() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___colliders_27)); }
	inline Collider2DU5BU5D_t1693969295* get_colliders_27() const { return ___colliders_27; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_colliders_27() { return &___colliders_27; }
	inline void set_colliders_27(Collider2DU5BU5D_t1693969295* value)
	{
		___colliders_27 = value;
		Il2CppCodeGenWriteBarrier((&___colliders_27), value);
	}

	inline static int32_t get_offset_of_colliderCount_28() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___colliderCount_28)); }
	inline int32_t get_colliderCount_28() const { return ___colliderCount_28; }
	inline int32_t* get_address_of_colliderCount_28() { return &___colliderCount_28; }
	inline void set_colliderCount_28(int32_t value)
	{
		___colliderCount_28 = value;
	}

	inline static int32_t get_offset_of_nextColliderIndex_29() { return static_cast<int32_t>(offsetof(GetNextOverlapArea2d_t3488004213, ___nextColliderIndex_29)); }
	inline int32_t get_nextColliderIndex_29() const { return ___nextColliderIndex_29; }
	inline int32_t* get_address_of_nextColliderIndex_29() { return &___nextColliderIndex_29; }
	inline void set_nextColliderIndex_29(int32_t value)
	{
		___nextColliderIndex_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEXTOVERLAPAREA2D_T3488004213_H
#ifndef GETNEXTOVERLAPCIRCLE2D_T2630547077_H
#define GETNEXTOVERLAPCIRCLE2D_T2630547077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d
struct  GetNextOverlapCircle2d_t2630547077  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::fromPosition
	FsmVector2_t2965096677 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::radius
	FsmFloat_t2883254149 * ___radius_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::minDepth
	FsmInt_t874273141 * ___minDepth_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::resetFlag
	FsmBool_t163807967 * ___resetFlag_19;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::invertMask
	FsmBool_t163807967 * ___invertMask_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::collidersCount
	FsmInt_t874273141 * ___collidersCount_22;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::storeNextCollider
	FsmGameObject_t3581898942 * ___storeNextCollider_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_25;
	// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::colliders
	Collider2DU5BU5D_t1693969295* ___colliders_26;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::colliderCount
	int32_t ___colliderCount_27;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::nextColliderIndex
	int32_t ___nextColliderIndex_28;

public:
	inline static int32_t get_offset_of_fromGameObject_14() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___fromGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_14() const { return ___fromGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_14() { return &___fromGameObject_14; }
	inline void set_fromGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___fromPosition_15)); }
	inline FsmVector2_t2965096677 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector2_t2965096677 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector2_t2965096677 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___radius_16)); }
	inline FsmFloat_t2883254149 * get_radius_16() const { return ___radius_16; }
	inline FsmFloat_t2883254149 ** get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(FsmFloat_t2883254149 * value)
	{
		___radius_16 = value;
		Il2CppCodeGenWriteBarrier((&___radius_16), value);
	}

	inline static int32_t get_offset_of_minDepth_17() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___minDepth_17)); }
	inline FsmInt_t874273141 * get_minDepth_17() const { return ___minDepth_17; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_17() { return &___minDepth_17; }
	inline void set_minDepth_17(FsmInt_t874273141 * value)
	{
		___minDepth_17 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_17), value);
	}

	inline static int32_t get_offset_of_maxDepth_18() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___maxDepth_18)); }
	inline FsmInt_t874273141 * get_maxDepth_18() const { return ___maxDepth_18; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_18() { return &___maxDepth_18; }
	inline void set_maxDepth_18(FsmInt_t874273141 * value)
	{
		___maxDepth_18 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_18), value);
	}

	inline static int32_t get_offset_of_resetFlag_19() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___resetFlag_19)); }
	inline FsmBool_t163807967 * get_resetFlag_19() const { return ___resetFlag_19; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_19() { return &___resetFlag_19; }
	inline void set_resetFlag_19(FsmBool_t163807967 * value)
	{
		___resetFlag_19 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_19), value);
	}

	inline static int32_t get_offset_of_layerMask_20() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___layerMask_20)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_20() const { return ___layerMask_20; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_20() { return &___layerMask_20; }
	inline void set_layerMask_20(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_20 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_20), value);
	}

	inline static int32_t get_offset_of_invertMask_21() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___invertMask_21)); }
	inline FsmBool_t163807967 * get_invertMask_21() const { return ___invertMask_21; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_21() { return &___invertMask_21; }
	inline void set_invertMask_21(FsmBool_t163807967 * value)
	{
		___invertMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_21), value);
	}

	inline static int32_t get_offset_of_collidersCount_22() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___collidersCount_22)); }
	inline FsmInt_t874273141 * get_collidersCount_22() const { return ___collidersCount_22; }
	inline FsmInt_t874273141 ** get_address_of_collidersCount_22() { return &___collidersCount_22; }
	inline void set_collidersCount_22(FsmInt_t874273141 * value)
	{
		___collidersCount_22 = value;
		Il2CppCodeGenWriteBarrier((&___collidersCount_22), value);
	}

	inline static int32_t get_offset_of_storeNextCollider_23() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___storeNextCollider_23)); }
	inline FsmGameObject_t3581898942 * get_storeNextCollider_23() const { return ___storeNextCollider_23; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeNextCollider_23() { return &___storeNextCollider_23; }
	inline void set_storeNextCollider_23(FsmGameObject_t3581898942 * value)
	{
		___storeNextCollider_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextCollider_23), value);
	}

	inline static int32_t get_offset_of_loopEvent_24() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___loopEvent_24)); }
	inline FsmEvent_t3736299882 * get_loopEvent_24() const { return ___loopEvent_24; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_24() { return &___loopEvent_24; }
	inline void set_loopEvent_24(FsmEvent_t3736299882 * value)
	{
		___loopEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_24), value);
	}

	inline static int32_t get_offset_of_finishedEvent_25() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___finishedEvent_25)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_25() const { return ___finishedEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_25() { return &___finishedEvent_25; }
	inline void set_finishedEvent_25(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_25), value);
	}

	inline static int32_t get_offset_of_colliders_26() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___colliders_26)); }
	inline Collider2DU5BU5D_t1693969295* get_colliders_26() const { return ___colliders_26; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_colliders_26() { return &___colliders_26; }
	inline void set_colliders_26(Collider2DU5BU5D_t1693969295* value)
	{
		___colliders_26 = value;
		Il2CppCodeGenWriteBarrier((&___colliders_26), value);
	}

	inline static int32_t get_offset_of_colliderCount_27() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___colliderCount_27)); }
	inline int32_t get_colliderCount_27() const { return ___colliderCount_27; }
	inline int32_t* get_address_of_colliderCount_27() { return &___colliderCount_27; }
	inline void set_colliderCount_27(int32_t value)
	{
		___colliderCount_27 = value;
	}

	inline static int32_t get_offset_of_nextColliderIndex_28() { return static_cast<int32_t>(offsetof(GetNextOverlapCircle2d_t2630547077, ___nextColliderIndex_28)); }
	inline int32_t get_nextColliderIndex_28() const { return ___nextColliderIndex_28; }
	inline int32_t* get_address_of_nextColliderIndex_28() { return &___nextColliderIndex_28; }
	inline void set_nextColliderIndex_28(int32_t value)
	{
		___nextColliderIndex_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEXTOVERLAPCIRCLE2D_T2630547077_H
#ifndef GETNEXTOVERLAPPOINT2D_T3527264863_H
#define GETNEXTOVERLAPPOINT2D_T3527264863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d
struct  GetNextOverlapPoint2d_t3527264863  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::position
	FsmVector2_t2965096677 * ___position_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::minDepth
	FsmInt_t874273141 * ___minDepth_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::resetFlag
	FsmBool_t163807967 * ___resetFlag_18;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::invertMask
	FsmBool_t163807967 * ___invertMask_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::collidersCount
	FsmInt_t874273141 * ___collidersCount_21;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::storeNextCollider
	FsmGameObject_t3581898942 * ___storeNextCollider_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_24;
	// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::colliders
	Collider2DU5BU5D_t1693969295* ___colliders_25;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::colliderCount
	int32_t ___colliderCount_26;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::nextColliderIndex
	int32_t ___nextColliderIndex_27;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___position_15)); }
	inline FsmVector2_t2965096677 * get_position_15() const { return ___position_15; }
	inline FsmVector2_t2965096677 ** get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(FsmVector2_t2965096677 * value)
	{
		___position_15 = value;
		Il2CppCodeGenWriteBarrier((&___position_15), value);
	}

	inline static int32_t get_offset_of_minDepth_16() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___minDepth_16)); }
	inline FsmInt_t874273141 * get_minDepth_16() const { return ___minDepth_16; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_16() { return &___minDepth_16; }
	inline void set_minDepth_16(FsmInt_t874273141 * value)
	{
		___minDepth_16 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_16), value);
	}

	inline static int32_t get_offset_of_maxDepth_17() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___maxDepth_17)); }
	inline FsmInt_t874273141 * get_maxDepth_17() const { return ___maxDepth_17; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_17() { return &___maxDepth_17; }
	inline void set_maxDepth_17(FsmInt_t874273141 * value)
	{
		___maxDepth_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_17), value);
	}

	inline static int32_t get_offset_of_resetFlag_18() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___resetFlag_18)); }
	inline FsmBool_t163807967 * get_resetFlag_18() const { return ___resetFlag_18; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_18() { return &___resetFlag_18; }
	inline void set_resetFlag_18(FsmBool_t163807967 * value)
	{
		___resetFlag_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_18), value);
	}

	inline static int32_t get_offset_of_layerMask_19() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___layerMask_19)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_19() const { return ___layerMask_19; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_19() { return &___layerMask_19; }
	inline void set_layerMask_19(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_19 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_19), value);
	}

	inline static int32_t get_offset_of_invertMask_20() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___invertMask_20)); }
	inline FsmBool_t163807967 * get_invertMask_20() const { return ___invertMask_20; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_20() { return &___invertMask_20; }
	inline void set_invertMask_20(FsmBool_t163807967 * value)
	{
		___invertMask_20 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_20), value);
	}

	inline static int32_t get_offset_of_collidersCount_21() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___collidersCount_21)); }
	inline FsmInt_t874273141 * get_collidersCount_21() const { return ___collidersCount_21; }
	inline FsmInt_t874273141 ** get_address_of_collidersCount_21() { return &___collidersCount_21; }
	inline void set_collidersCount_21(FsmInt_t874273141 * value)
	{
		___collidersCount_21 = value;
		Il2CppCodeGenWriteBarrier((&___collidersCount_21), value);
	}

	inline static int32_t get_offset_of_storeNextCollider_22() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___storeNextCollider_22)); }
	inline FsmGameObject_t3581898942 * get_storeNextCollider_22() const { return ___storeNextCollider_22; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeNextCollider_22() { return &___storeNextCollider_22; }
	inline void set_storeNextCollider_22(FsmGameObject_t3581898942 * value)
	{
		___storeNextCollider_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextCollider_22), value);
	}

	inline static int32_t get_offset_of_loopEvent_23() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___loopEvent_23)); }
	inline FsmEvent_t3736299882 * get_loopEvent_23() const { return ___loopEvent_23; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_23() { return &___loopEvent_23; }
	inline void set_loopEvent_23(FsmEvent_t3736299882 * value)
	{
		___loopEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_23), value);
	}

	inline static int32_t get_offset_of_finishedEvent_24() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___finishedEvent_24)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_24() const { return ___finishedEvent_24; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_24() { return &___finishedEvent_24; }
	inline void set_finishedEvent_24(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_24), value);
	}

	inline static int32_t get_offset_of_colliders_25() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___colliders_25)); }
	inline Collider2DU5BU5D_t1693969295* get_colliders_25() const { return ___colliders_25; }
	inline Collider2DU5BU5D_t1693969295** get_address_of_colliders_25() { return &___colliders_25; }
	inline void set_colliders_25(Collider2DU5BU5D_t1693969295* value)
	{
		___colliders_25 = value;
		Il2CppCodeGenWriteBarrier((&___colliders_25), value);
	}

	inline static int32_t get_offset_of_colliderCount_26() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___colliderCount_26)); }
	inline int32_t get_colliderCount_26() const { return ___colliderCount_26; }
	inline int32_t* get_address_of_colliderCount_26() { return &___colliderCount_26; }
	inline void set_colliderCount_26(int32_t value)
	{
		___colliderCount_26 = value;
	}

	inline static int32_t get_offset_of_nextColliderIndex_27() { return static_cast<int32_t>(offsetof(GetNextOverlapPoint2d_t3527264863, ___nextColliderIndex_27)); }
	inline int32_t get_nextColliderIndex_27() const { return ___nextColliderIndex_27; }
	inline int32_t* get_address_of_nextColliderIndex_27() { return &___nextColliderIndex_27; }
	inline void set_nextColliderIndex_27(int32_t value)
	{
		___nextColliderIndex_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEXTOVERLAPPOINT2D_T3527264863_H
#ifndef GETNEXTRAYCAST2D_T2379810548_H
#define GETNEXTRAYCAST2D_T2379810548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetNextRayCast2d
struct  GetNextRayCast2d_t2379810548  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetNextRayCast2d::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextRayCast2d::fromPosition
	FsmVector2_t2965096677 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextRayCast2d::direction
	FsmVector2_t2965096677 * ___direction_16;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetNextRayCast2d::space
	int32_t ___space_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetNextRayCast2d::distance
	FsmFloat_t2883254149 * ___distance_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextRayCast2d::minDepth
	FsmInt_t874273141 * ___minDepth_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextRayCast2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextRayCast2d::resetFlag
	FsmBool_t163807967 * ___resetFlag_21;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.GetNextRayCast2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetNextRayCast2d::invertMask
	FsmBool_t163807967 * ___invertMask_23;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetNextRayCast2d::collidersCount
	FsmInt_t874273141 * ___collidersCount_24;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetNextRayCast2d::storeNextCollider
	FsmGameObject_t3581898942 * ___storeNextCollider_25;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextRayCast2d::storeNextHitPoint
	FsmVector2_t2965096677 * ___storeNextHitPoint_26;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetNextRayCast2d::storeNextHitNormal
	FsmVector2_t2965096677 * ___storeNextHitNormal_27;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetNextRayCast2d::storeNextHitDistance
	FsmFloat_t2883254149 * ___storeNextHitDistance_28;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetNextRayCast2d::storeNextHitFraction
	FsmFloat_t2883254149 * ___storeNextHitFraction_29;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextRayCast2d::loopEvent
	FsmEvent_t3736299882 * ___loopEvent_30;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetNextRayCast2d::finishedEvent
	FsmEvent_t3736299882 * ___finishedEvent_31;
	// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextRayCast2d::hits
	RaycastHit2DU5BU5D_t4286651560* ___hits_32;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextRayCast2d::colliderCount
	int32_t ___colliderCount_33;
	// System.Int32 HutongGames.PlayMaker.Actions.GetNextRayCast2d::nextColliderIndex
	int32_t ___nextColliderIndex_34;

public:
	inline static int32_t get_offset_of_fromGameObject_14() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___fromGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_14() const { return ___fromGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_14() { return &___fromGameObject_14; }
	inline void set_fromGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___fromPosition_15)); }
	inline FsmVector2_t2965096677 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector2_t2965096677 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector2_t2965096677 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___direction_16)); }
	inline FsmVector2_t2965096677 * get_direction_16() const { return ___direction_16; }
	inline FsmVector2_t2965096677 ** get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(FsmVector2_t2965096677 * value)
	{
		___direction_16 = value;
		Il2CppCodeGenWriteBarrier((&___direction_16), value);
	}

	inline static int32_t get_offset_of_space_17() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___space_17)); }
	inline int32_t get_space_17() const { return ___space_17; }
	inline int32_t* get_address_of_space_17() { return &___space_17; }
	inline void set_space_17(int32_t value)
	{
		___space_17 = value;
	}

	inline static int32_t get_offset_of_distance_18() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___distance_18)); }
	inline FsmFloat_t2883254149 * get_distance_18() const { return ___distance_18; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_18() { return &___distance_18; }
	inline void set_distance_18(FsmFloat_t2883254149 * value)
	{
		___distance_18 = value;
		Il2CppCodeGenWriteBarrier((&___distance_18), value);
	}

	inline static int32_t get_offset_of_minDepth_19() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___minDepth_19)); }
	inline FsmInt_t874273141 * get_minDepth_19() const { return ___minDepth_19; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_19() { return &___minDepth_19; }
	inline void set_minDepth_19(FsmInt_t874273141 * value)
	{
		___minDepth_19 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_19), value);
	}

	inline static int32_t get_offset_of_maxDepth_20() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___maxDepth_20)); }
	inline FsmInt_t874273141 * get_maxDepth_20() const { return ___maxDepth_20; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_20() { return &___maxDepth_20; }
	inline void set_maxDepth_20(FsmInt_t874273141 * value)
	{
		___maxDepth_20 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_20), value);
	}

	inline static int32_t get_offset_of_resetFlag_21() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___resetFlag_21)); }
	inline FsmBool_t163807967 * get_resetFlag_21() const { return ___resetFlag_21; }
	inline FsmBool_t163807967 ** get_address_of_resetFlag_21() { return &___resetFlag_21; }
	inline void set_resetFlag_21(FsmBool_t163807967 * value)
	{
		___resetFlag_21 = value;
		Il2CppCodeGenWriteBarrier((&___resetFlag_21), value);
	}

	inline static int32_t get_offset_of_layerMask_22() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___layerMask_22)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_22() const { return ___layerMask_22; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_22() { return &___layerMask_22; }
	inline void set_layerMask_22(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_22 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_22), value);
	}

	inline static int32_t get_offset_of_invertMask_23() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___invertMask_23)); }
	inline FsmBool_t163807967 * get_invertMask_23() const { return ___invertMask_23; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_23() { return &___invertMask_23; }
	inline void set_invertMask_23(FsmBool_t163807967 * value)
	{
		___invertMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_23), value);
	}

	inline static int32_t get_offset_of_collidersCount_24() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___collidersCount_24)); }
	inline FsmInt_t874273141 * get_collidersCount_24() const { return ___collidersCount_24; }
	inline FsmInt_t874273141 ** get_address_of_collidersCount_24() { return &___collidersCount_24; }
	inline void set_collidersCount_24(FsmInt_t874273141 * value)
	{
		___collidersCount_24 = value;
		Il2CppCodeGenWriteBarrier((&___collidersCount_24), value);
	}

	inline static int32_t get_offset_of_storeNextCollider_25() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___storeNextCollider_25)); }
	inline FsmGameObject_t3581898942 * get_storeNextCollider_25() const { return ___storeNextCollider_25; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeNextCollider_25() { return &___storeNextCollider_25; }
	inline void set_storeNextCollider_25(FsmGameObject_t3581898942 * value)
	{
		___storeNextCollider_25 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextCollider_25), value);
	}

	inline static int32_t get_offset_of_storeNextHitPoint_26() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___storeNextHitPoint_26)); }
	inline FsmVector2_t2965096677 * get_storeNextHitPoint_26() const { return ___storeNextHitPoint_26; }
	inline FsmVector2_t2965096677 ** get_address_of_storeNextHitPoint_26() { return &___storeNextHitPoint_26; }
	inline void set_storeNextHitPoint_26(FsmVector2_t2965096677 * value)
	{
		___storeNextHitPoint_26 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitPoint_26), value);
	}

	inline static int32_t get_offset_of_storeNextHitNormal_27() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___storeNextHitNormal_27)); }
	inline FsmVector2_t2965096677 * get_storeNextHitNormal_27() const { return ___storeNextHitNormal_27; }
	inline FsmVector2_t2965096677 ** get_address_of_storeNextHitNormal_27() { return &___storeNextHitNormal_27; }
	inline void set_storeNextHitNormal_27(FsmVector2_t2965096677 * value)
	{
		___storeNextHitNormal_27 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitNormal_27), value);
	}

	inline static int32_t get_offset_of_storeNextHitDistance_28() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___storeNextHitDistance_28)); }
	inline FsmFloat_t2883254149 * get_storeNextHitDistance_28() const { return ___storeNextHitDistance_28; }
	inline FsmFloat_t2883254149 ** get_address_of_storeNextHitDistance_28() { return &___storeNextHitDistance_28; }
	inline void set_storeNextHitDistance_28(FsmFloat_t2883254149 * value)
	{
		___storeNextHitDistance_28 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitDistance_28), value);
	}

	inline static int32_t get_offset_of_storeNextHitFraction_29() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___storeNextHitFraction_29)); }
	inline FsmFloat_t2883254149 * get_storeNextHitFraction_29() const { return ___storeNextHitFraction_29; }
	inline FsmFloat_t2883254149 ** get_address_of_storeNextHitFraction_29() { return &___storeNextHitFraction_29; }
	inline void set_storeNextHitFraction_29(FsmFloat_t2883254149 * value)
	{
		___storeNextHitFraction_29 = value;
		Il2CppCodeGenWriteBarrier((&___storeNextHitFraction_29), value);
	}

	inline static int32_t get_offset_of_loopEvent_30() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___loopEvent_30)); }
	inline FsmEvent_t3736299882 * get_loopEvent_30() const { return ___loopEvent_30; }
	inline FsmEvent_t3736299882 ** get_address_of_loopEvent_30() { return &___loopEvent_30; }
	inline void set_loopEvent_30(FsmEvent_t3736299882 * value)
	{
		___loopEvent_30 = value;
		Il2CppCodeGenWriteBarrier((&___loopEvent_30), value);
	}

	inline static int32_t get_offset_of_finishedEvent_31() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___finishedEvent_31)); }
	inline FsmEvent_t3736299882 * get_finishedEvent_31() const { return ___finishedEvent_31; }
	inline FsmEvent_t3736299882 ** get_address_of_finishedEvent_31() { return &___finishedEvent_31; }
	inline void set_finishedEvent_31(FsmEvent_t3736299882 * value)
	{
		___finishedEvent_31 = value;
		Il2CppCodeGenWriteBarrier((&___finishedEvent_31), value);
	}

	inline static int32_t get_offset_of_hits_32() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___hits_32)); }
	inline RaycastHit2DU5BU5D_t4286651560* get_hits_32() const { return ___hits_32; }
	inline RaycastHit2DU5BU5D_t4286651560** get_address_of_hits_32() { return &___hits_32; }
	inline void set_hits_32(RaycastHit2DU5BU5D_t4286651560* value)
	{
		___hits_32 = value;
		Il2CppCodeGenWriteBarrier((&___hits_32), value);
	}

	inline static int32_t get_offset_of_colliderCount_33() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___colliderCount_33)); }
	inline int32_t get_colliderCount_33() const { return ___colliderCount_33; }
	inline int32_t* get_address_of_colliderCount_33() { return &___colliderCount_33; }
	inline void set_colliderCount_33(int32_t value)
	{
		___colliderCount_33 = value;
	}

	inline static int32_t get_offset_of_nextColliderIndex_34() { return static_cast<int32_t>(offsetof(GetNextRayCast2d_t2379810548, ___nextColliderIndex_34)); }
	inline int32_t get_nextColliderIndex_34() const { return ___nextColliderIndex_34; }
	inline int32_t* get_address_of_nextColliderIndex_34() { return &___nextColliderIndex_34; }
	inline void set_nextColliderIndex_34(int32_t value)
	{
		___nextColliderIndex_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETNEXTRAYCAST2D_T2379810548_H
#ifndef GETPARTICLECOLLISIONINFO_T297522531_H
#define GETPARTICLECOLLISIONINFO_T297522531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetParticleCollisionInfo
struct  GetParticleCollisionInfo_t297522531  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetParticleCollisionInfo_t297522531, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPARTICLECOLLISIONINFO_T297522531_H
#ifndef GETRAYCASTHIT2DINFO_T543528974_H
#define GETRAYCASTHIT2DINFO_T543528974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo
struct  GetRayCastHit2dInfo_t543528974  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::point
	FsmVector2_t2965096677 * ___point_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::normal
	FsmVector3_t626444517 * ___normal_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::distance
	FsmFloat_t2883254149 * ___distance_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetRayCastHit2dInfo_t543528974, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_point_15() { return static_cast<int32_t>(offsetof(GetRayCastHit2dInfo_t543528974, ___point_15)); }
	inline FsmVector2_t2965096677 * get_point_15() const { return ___point_15; }
	inline FsmVector2_t2965096677 ** get_address_of_point_15() { return &___point_15; }
	inline void set_point_15(FsmVector2_t2965096677 * value)
	{
		___point_15 = value;
		Il2CppCodeGenWriteBarrier((&___point_15), value);
	}

	inline static int32_t get_offset_of_normal_16() { return static_cast<int32_t>(offsetof(GetRayCastHit2dInfo_t543528974, ___normal_16)); }
	inline FsmVector3_t626444517 * get_normal_16() const { return ___normal_16; }
	inline FsmVector3_t626444517 ** get_address_of_normal_16() { return &___normal_16; }
	inline void set_normal_16(FsmVector3_t626444517 * value)
	{
		___normal_16 = value;
		Il2CppCodeGenWriteBarrier((&___normal_16), value);
	}

	inline static int32_t get_offset_of_distance_17() { return static_cast<int32_t>(offsetof(GetRayCastHit2dInfo_t543528974, ___distance_17)); }
	inline FsmFloat_t2883254149 * get_distance_17() const { return ___distance_17; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_17() { return &___distance_17; }
	inline void set_distance_17(FsmFloat_t2883254149 * value)
	{
		___distance_17 = value;
		Il2CppCodeGenWriteBarrier((&___distance_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetRayCastHit2dInfo_t543528974, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTHIT2DINFO_T543528974_H
#ifndef GETRAYCASTALLINFO_T2362054518_H
#define GETRAYCASTALLINFO_T2362054518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRaycastAllInfo
struct  GetRaycastAllInfo_t2362054518  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetRaycastAllInfo::storeHitObjects
	FsmArray_t1756862219 * ___storeHitObjects_14;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetRaycastAllInfo::points
	FsmArray_t1756862219 * ___points_15;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetRaycastAllInfo::normals
	FsmArray_t1756862219 * ___normals_16;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetRaycastAllInfo::distances
	FsmArray_t1756862219 * ___distances_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRaycastAllInfo::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_storeHitObjects_14() { return static_cast<int32_t>(offsetof(GetRaycastAllInfo_t2362054518, ___storeHitObjects_14)); }
	inline FsmArray_t1756862219 * get_storeHitObjects_14() const { return ___storeHitObjects_14; }
	inline FsmArray_t1756862219 ** get_address_of_storeHitObjects_14() { return &___storeHitObjects_14; }
	inline void set_storeHitObjects_14(FsmArray_t1756862219 * value)
	{
		___storeHitObjects_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitObjects_14), value);
	}

	inline static int32_t get_offset_of_points_15() { return static_cast<int32_t>(offsetof(GetRaycastAllInfo_t2362054518, ___points_15)); }
	inline FsmArray_t1756862219 * get_points_15() const { return ___points_15; }
	inline FsmArray_t1756862219 ** get_address_of_points_15() { return &___points_15; }
	inline void set_points_15(FsmArray_t1756862219 * value)
	{
		___points_15 = value;
		Il2CppCodeGenWriteBarrier((&___points_15), value);
	}

	inline static int32_t get_offset_of_normals_16() { return static_cast<int32_t>(offsetof(GetRaycastAllInfo_t2362054518, ___normals_16)); }
	inline FsmArray_t1756862219 * get_normals_16() const { return ___normals_16; }
	inline FsmArray_t1756862219 ** get_address_of_normals_16() { return &___normals_16; }
	inline void set_normals_16(FsmArray_t1756862219 * value)
	{
		___normals_16 = value;
		Il2CppCodeGenWriteBarrier((&___normals_16), value);
	}

	inline static int32_t get_offset_of_distances_17() { return static_cast<int32_t>(offsetof(GetRaycastAllInfo_t2362054518, ___distances_17)); }
	inline FsmArray_t1756862219 * get_distances_17() const { return ___distances_17; }
	inline FsmArray_t1756862219 ** get_address_of_distances_17() { return &___distances_17; }
	inline void set_distances_17(FsmArray_t1756862219 * value)
	{
		___distances_17 = value;
		Il2CppCodeGenWriteBarrier((&___distances_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetRaycastAllInfo_t2362054518, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTALLINFO_T2362054518_H
#ifndef GETRAYCASTHITINFO_T1121319619_H
#define GETRAYCASTHITINFO_T1121319619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRaycastHitInfo
struct  GetRaycastHitInfo_t1121319619  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetRaycastHitInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetRaycastHitInfo::point
	FsmVector3_t626444517 * ___point_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetRaycastHitInfo::normal
	FsmVector3_t626444517 * ___normal_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRaycastHitInfo::distance
	FsmFloat_t2883254149 * ___distance_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRaycastHitInfo::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1121319619, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_point_15() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1121319619, ___point_15)); }
	inline FsmVector3_t626444517 * get_point_15() const { return ___point_15; }
	inline FsmVector3_t626444517 ** get_address_of_point_15() { return &___point_15; }
	inline void set_point_15(FsmVector3_t626444517 * value)
	{
		___point_15 = value;
		Il2CppCodeGenWriteBarrier((&___point_15), value);
	}

	inline static int32_t get_offset_of_normal_16() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1121319619, ___normal_16)); }
	inline FsmVector3_t626444517 * get_normal_16() const { return ___normal_16; }
	inline FsmVector3_t626444517 ** get_address_of_normal_16() { return &___normal_16; }
	inline void set_normal_16(FsmVector3_t626444517 * value)
	{
		___normal_16 = value;
		Il2CppCodeGenWriteBarrier((&___normal_16), value);
	}

	inline static int32_t get_offset_of_distance_17() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1121319619, ___distance_17)); }
	inline FsmFloat_t2883254149 * get_distance_17() const { return ___distance_17; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_17() { return &___distance_17; }
	inline void set_distance_17(FsmFloat_t2883254149 * value)
	{
		___distance_17 = value;
		Il2CppCodeGenWriteBarrier((&___distance_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetRaycastHitInfo_t1121319619, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTHITINFO_T1121319619_H
#ifndef GETTRIGGER2DINFO_T2591660132_H
#define GETTRIGGER2DINFO_T2591660132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTrigger2dInfo
struct  GetTrigger2dInfo_t2591660132  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetTrigger2dInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetTrigger2dInfo::shapeCount
	FsmInt_t874273141 * ___shapeCount_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetTrigger2dInfo::physics2dMaterialName
	FsmString_t1785915204 * ___physics2dMaterialName_16;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetTrigger2dInfo_t2591660132, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_shapeCount_15() { return static_cast<int32_t>(offsetof(GetTrigger2dInfo_t2591660132, ___shapeCount_15)); }
	inline FsmInt_t874273141 * get_shapeCount_15() const { return ___shapeCount_15; }
	inline FsmInt_t874273141 ** get_address_of_shapeCount_15() { return &___shapeCount_15; }
	inline void set_shapeCount_15(FsmInt_t874273141 * value)
	{
		___shapeCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___shapeCount_15), value);
	}

	inline static int32_t get_offset_of_physics2dMaterialName_16() { return static_cast<int32_t>(offsetof(GetTrigger2dInfo_t2591660132, ___physics2dMaterialName_16)); }
	inline FsmString_t1785915204 * get_physics2dMaterialName_16() const { return ___physics2dMaterialName_16; }
	inline FsmString_t1785915204 ** get_address_of_physics2dMaterialName_16() { return &___physics2dMaterialName_16; }
	inline void set_physics2dMaterialName_16(FsmString_t1785915204 * value)
	{
		___physics2dMaterialName_16 = value;
		Il2CppCodeGenWriteBarrier((&___physics2dMaterialName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTRIGGER2DINFO_T2591660132_H
#ifndef GETTRIGGERINFO_T3792290394_H
#define GETTRIGGERINFO_T3792290394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTriggerInfo
struct  GetTriggerInfo_t3792290394  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetTriggerInfo::gameObjectHit
	FsmGameObject_t3581898942 * ___gameObjectHit_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetTriggerInfo::physicsMaterialName
	FsmString_t1785915204 * ___physicsMaterialName_15;

public:
	inline static int32_t get_offset_of_gameObjectHit_14() { return static_cast<int32_t>(offsetof(GetTriggerInfo_t3792290394, ___gameObjectHit_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectHit_14() const { return ___gameObjectHit_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectHit_14() { return &___gameObjectHit_14; }
	inline void set_gameObjectHit_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectHit_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectHit_14), value);
	}

	inline static int32_t get_offset_of_physicsMaterialName_15() { return static_cast<int32_t>(offsetof(GetTriggerInfo_t3792290394, ___physicsMaterialName_15)); }
	inline FsmString_t1785915204 * get_physicsMaterialName_15() const { return ___physicsMaterialName_15; }
	inline FsmString_t1785915204 ** get_address_of_physicsMaterialName_15() { return &___physicsMaterialName_15; }
	inline void set_physicsMaterialName_15(FsmString_t1785915204 * value)
	{
		___physicsMaterialName_15 = value;
		Il2CppCodeGenWriteBarrier((&___physicsMaterialName_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTRIGGERINFO_T3792290394_H
#ifndef GETVERTEXCOUNT_T1911616008_H
#define GETVERTEXCOUNT_T1911616008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVertexCount
struct  GetVertexCount_t1911616008  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetVertexCount::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetVertexCount::storeCount
	FsmInt_t874273141 * ___storeCount_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVertexCount::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetVertexCount_t1911616008, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_storeCount_15() { return static_cast<int32_t>(offsetof(GetVertexCount_t1911616008, ___storeCount_15)); }
	inline FsmInt_t874273141 * get_storeCount_15() const { return ___storeCount_15; }
	inline FsmInt_t874273141 ** get_address_of_storeCount_15() { return &___storeCount_15; }
	inline void set_storeCount_15(FsmInt_t874273141 * value)
	{
		___storeCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeCount_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetVertexCount_t1911616008, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVERTEXCOUNT_T1911616008_H
#ifndef GETVERTEXPOSITION_T1214669549_H
#define GETVERTEXPOSITION_T1214669549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVertexPosition
struct  GetVertexPosition_t1214669549  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetVertexPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetVertexPosition::vertexIndex
	FsmInt_t874273141 * ___vertexIndex_15;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetVertexPosition::space
	int32_t ___space_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetVertexPosition::storePosition
	FsmVector3_t626444517 * ___storePosition_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVertexPosition::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetVertexPosition_t1214669549, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vertexIndex_15() { return static_cast<int32_t>(offsetof(GetVertexPosition_t1214669549, ___vertexIndex_15)); }
	inline FsmInt_t874273141 * get_vertexIndex_15() const { return ___vertexIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_vertexIndex_15() { return &___vertexIndex_15; }
	inline void set_vertexIndex_15(FsmInt_t874273141 * value)
	{
		___vertexIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___vertexIndex_15), value);
	}

	inline static int32_t get_offset_of_space_16() { return static_cast<int32_t>(offsetof(GetVertexPosition_t1214669549, ___space_16)); }
	inline int32_t get_space_16() const { return ___space_16; }
	inline int32_t* get_address_of_space_16() { return &___space_16; }
	inline void set_space_16(int32_t value)
	{
		___space_16 = value;
	}

	inline static int32_t get_offset_of_storePosition_17() { return static_cast<int32_t>(offsetof(GetVertexPosition_t1214669549, ___storePosition_17)); }
	inline FsmVector3_t626444517 * get_storePosition_17() const { return ___storePosition_17; }
	inline FsmVector3_t626444517 ** get_address_of_storePosition_17() { return &___storePosition_17; }
	inline void set_storePosition_17(FsmVector3_t626444517 * value)
	{
		___storePosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___storePosition_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetVertexPosition_t1214669549, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVERTEXPOSITION_T1214669549_H
#ifndef INTADD_T2214403792_H
#define INTADD_T2214403792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntAdd
struct  IntAdd_t2214403792  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAdd::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAdd::add
	FsmInt_t874273141 * ___add_15;
	// System.Boolean HutongGames.PlayMaker.Actions.IntAdd::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(IntAdd_t2214403792, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_add_15() { return static_cast<int32_t>(offsetof(IntAdd_t2214403792, ___add_15)); }
	inline FsmInt_t874273141 * get_add_15() const { return ___add_15; }
	inline FsmInt_t874273141 ** get_address_of_add_15() { return &___add_15; }
	inline void set_add_15(FsmInt_t874273141 * value)
	{
		___add_15 = value;
		Il2CppCodeGenWriteBarrier((&___add_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(IntAdd_t2214403792, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTADD_T2214403792_H
#ifndef INTCLAMP_T3495764295_H
#define INTCLAMP_T3495764295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntClamp
struct  IntClamp_t3495764295  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntClamp::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntClamp::minValue
	FsmInt_t874273141 * ___minValue_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntClamp::maxValue
	FsmInt_t874273141 * ___maxValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.IntClamp::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(IntClamp_t3495764295, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_minValue_15() { return static_cast<int32_t>(offsetof(IntClamp_t3495764295, ___minValue_15)); }
	inline FsmInt_t874273141 * get_minValue_15() const { return ___minValue_15; }
	inline FsmInt_t874273141 ** get_address_of_minValue_15() { return &___minValue_15; }
	inline void set_minValue_15(FsmInt_t874273141 * value)
	{
		___minValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___minValue_15), value);
	}

	inline static int32_t get_offset_of_maxValue_16() { return static_cast<int32_t>(offsetof(IntClamp_t3495764295, ___maxValue_16)); }
	inline FsmInt_t874273141 * get_maxValue_16() const { return ___maxValue_16; }
	inline FsmInt_t874273141 ** get_address_of_maxValue_16() { return &___maxValue_16; }
	inline void set_maxValue_16(FsmInt_t874273141 * value)
	{
		___maxValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___maxValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(IntClamp_t3495764295, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTCLAMP_T3495764295_H
#ifndef INTOPERATOR_T945431901_H
#define INTOPERATOR_T945431901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntOperator
struct  IntOperator_t945431901  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntOperator::integer1
	FsmInt_t874273141 * ___integer1_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntOperator::integer2
	FsmInt_t874273141 * ___integer2_15;
	// HutongGames.PlayMaker.Actions.IntOperator/Operation HutongGames.PlayMaker.Actions.IntOperator::operation
	int32_t ___operation_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntOperator::storeResult
	FsmInt_t874273141 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.IntOperator::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_integer1_14() { return static_cast<int32_t>(offsetof(IntOperator_t945431901, ___integer1_14)); }
	inline FsmInt_t874273141 * get_integer1_14() const { return ___integer1_14; }
	inline FsmInt_t874273141 ** get_address_of_integer1_14() { return &___integer1_14; }
	inline void set_integer1_14(FsmInt_t874273141 * value)
	{
		___integer1_14 = value;
		Il2CppCodeGenWriteBarrier((&___integer1_14), value);
	}

	inline static int32_t get_offset_of_integer2_15() { return static_cast<int32_t>(offsetof(IntOperator_t945431901, ___integer2_15)); }
	inline FsmInt_t874273141 * get_integer2_15() const { return ___integer2_15; }
	inline FsmInt_t874273141 ** get_address_of_integer2_15() { return &___integer2_15; }
	inline void set_integer2_15(FsmInt_t874273141 * value)
	{
		___integer2_15 = value;
		Il2CppCodeGenWriteBarrier((&___integer2_15), value);
	}

	inline static int32_t get_offset_of_operation_16() { return static_cast<int32_t>(offsetof(IntOperator_t945431901, ___operation_16)); }
	inline int32_t get_operation_16() const { return ___operation_16; }
	inline int32_t* get_address_of_operation_16() { return &___operation_16; }
	inline void set_operation_16(int32_t value)
	{
		___operation_16 = value;
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(IntOperator_t945431901, ___storeResult_17)); }
	inline FsmInt_t874273141 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmInt_t874273141 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmInt_t874273141 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(IntOperator_t945431901, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTOPERATOR_T945431901_H
#ifndef LINECAST2D_T30898162_H
#define LINECAST2D_T30898162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LineCast2d
struct  LineCast2d_t30898162  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.LineCast2d::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::fromPosition
	FsmVector2_t2965096677 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LineCast2d::toGameObject
	FsmGameObject_t3581898942 * ___toGameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::toPosition
	FsmVector2_t2965096677 * ___toPosition_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LineCast2d::minDepth
	FsmInt_t874273141 * ___minDepth_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LineCast2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LineCast2d::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LineCast2d::storeDidHit
	FsmBool_t163807967 * ___storeDidHit_21;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LineCast2d::storeHitObject
	FsmGameObject_t3581898942 * ___storeHitObject_22;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::storeHitPoint
	FsmVector2_t2965096677 * ___storeHitPoint_23;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LineCast2d::storeHitNormal
	FsmVector2_t2965096677 * ___storeHitNormal_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.LineCast2d::storeHitDistance
	FsmFloat_t2883254149 * ___storeHitDistance_25;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LineCast2d::repeatInterval
	FsmInt_t874273141 * ___repeatInterval_26;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.LineCast2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_27;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LineCast2d::invertMask
	FsmBool_t163807967 * ___invertMask_28;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.LineCast2d::debugColor
	FsmColor_t1738900188 * ___debugColor_29;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LineCast2d::debug
	FsmBool_t163807967 * ___debug_30;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.LineCast2d::_fromTrans
	Transform_t3600365921 * ____fromTrans_31;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.LineCast2d::_toTrans
	Transform_t3600365921 * ____toTrans_32;
	// System.Int32 HutongGames.PlayMaker.Actions.LineCast2d::repeat
	int32_t ___repeat_33;

public:
	inline static int32_t get_offset_of_fromGameObject_14() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___fromGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_14() const { return ___fromGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_14() { return &___fromGameObject_14; }
	inline void set_fromGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___fromPosition_15)); }
	inline FsmVector2_t2965096677 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector2_t2965096677 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector2_t2965096677 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_toGameObject_16() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___toGameObject_16)); }
	inline FsmGameObject_t3581898942 * get_toGameObject_16() const { return ___toGameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_toGameObject_16() { return &___toGameObject_16; }
	inline void set_toGameObject_16(FsmGameObject_t3581898942 * value)
	{
		___toGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___toGameObject_16), value);
	}

	inline static int32_t get_offset_of_toPosition_17() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___toPosition_17)); }
	inline FsmVector2_t2965096677 * get_toPosition_17() const { return ___toPosition_17; }
	inline FsmVector2_t2965096677 ** get_address_of_toPosition_17() { return &___toPosition_17; }
	inline void set_toPosition_17(FsmVector2_t2965096677 * value)
	{
		___toPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___toPosition_17), value);
	}

	inline static int32_t get_offset_of_minDepth_18() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___minDepth_18)); }
	inline FsmInt_t874273141 * get_minDepth_18() const { return ___minDepth_18; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_18() { return &___minDepth_18; }
	inline void set_minDepth_18(FsmInt_t874273141 * value)
	{
		___minDepth_18 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_18), value);
	}

	inline static int32_t get_offset_of_maxDepth_19() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___maxDepth_19)); }
	inline FsmInt_t874273141 * get_maxDepth_19() const { return ___maxDepth_19; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_19() { return &___maxDepth_19; }
	inline void set_maxDepth_19(FsmInt_t874273141 * value)
	{
		___maxDepth_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_19), value);
	}

	inline static int32_t get_offset_of_hitEvent_20() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___hitEvent_20)); }
	inline FsmEvent_t3736299882 * get_hitEvent_20() const { return ___hitEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_20() { return &___hitEvent_20; }
	inline void set_hitEvent_20(FsmEvent_t3736299882 * value)
	{
		___hitEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_20), value);
	}

	inline static int32_t get_offset_of_storeDidHit_21() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___storeDidHit_21)); }
	inline FsmBool_t163807967 * get_storeDidHit_21() const { return ___storeDidHit_21; }
	inline FsmBool_t163807967 ** get_address_of_storeDidHit_21() { return &___storeDidHit_21; }
	inline void set_storeDidHit_21(FsmBool_t163807967 * value)
	{
		___storeDidHit_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidHit_21), value);
	}

	inline static int32_t get_offset_of_storeHitObject_22() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___storeHitObject_22)); }
	inline FsmGameObject_t3581898942 * get_storeHitObject_22() const { return ___storeHitObject_22; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeHitObject_22() { return &___storeHitObject_22; }
	inline void set_storeHitObject_22(FsmGameObject_t3581898942 * value)
	{
		___storeHitObject_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitObject_22), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_23() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___storeHitPoint_23)); }
	inline FsmVector2_t2965096677 * get_storeHitPoint_23() const { return ___storeHitPoint_23; }
	inline FsmVector2_t2965096677 ** get_address_of_storeHitPoint_23() { return &___storeHitPoint_23; }
	inline void set_storeHitPoint_23(FsmVector2_t2965096677 * value)
	{
		___storeHitPoint_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_23), value);
	}

	inline static int32_t get_offset_of_storeHitNormal_24() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___storeHitNormal_24)); }
	inline FsmVector2_t2965096677 * get_storeHitNormal_24() const { return ___storeHitNormal_24; }
	inline FsmVector2_t2965096677 ** get_address_of_storeHitNormal_24() { return &___storeHitNormal_24; }
	inline void set_storeHitNormal_24(FsmVector2_t2965096677 * value)
	{
		___storeHitNormal_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitNormal_24), value);
	}

	inline static int32_t get_offset_of_storeHitDistance_25() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___storeHitDistance_25)); }
	inline FsmFloat_t2883254149 * get_storeHitDistance_25() const { return ___storeHitDistance_25; }
	inline FsmFloat_t2883254149 ** get_address_of_storeHitDistance_25() { return &___storeHitDistance_25; }
	inline void set_storeHitDistance_25(FsmFloat_t2883254149 * value)
	{
		___storeHitDistance_25 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitDistance_25), value);
	}

	inline static int32_t get_offset_of_repeatInterval_26() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___repeatInterval_26)); }
	inline FsmInt_t874273141 * get_repeatInterval_26() const { return ___repeatInterval_26; }
	inline FsmInt_t874273141 ** get_address_of_repeatInterval_26() { return &___repeatInterval_26; }
	inline void set_repeatInterval_26(FsmInt_t874273141 * value)
	{
		___repeatInterval_26 = value;
		Il2CppCodeGenWriteBarrier((&___repeatInterval_26), value);
	}

	inline static int32_t get_offset_of_layerMask_27() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___layerMask_27)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_27() const { return ___layerMask_27; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_27() { return &___layerMask_27; }
	inline void set_layerMask_27(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_27), value);
	}

	inline static int32_t get_offset_of_invertMask_28() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___invertMask_28)); }
	inline FsmBool_t163807967 * get_invertMask_28() const { return ___invertMask_28; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_28() { return &___invertMask_28; }
	inline void set_invertMask_28(FsmBool_t163807967 * value)
	{
		___invertMask_28 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_28), value);
	}

	inline static int32_t get_offset_of_debugColor_29() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___debugColor_29)); }
	inline FsmColor_t1738900188 * get_debugColor_29() const { return ___debugColor_29; }
	inline FsmColor_t1738900188 ** get_address_of_debugColor_29() { return &___debugColor_29; }
	inline void set_debugColor_29(FsmColor_t1738900188 * value)
	{
		___debugColor_29 = value;
		Il2CppCodeGenWriteBarrier((&___debugColor_29), value);
	}

	inline static int32_t get_offset_of_debug_30() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___debug_30)); }
	inline FsmBool_t163807967 * get_debug_30() const { return ___debug_30; }
	inline FsmBool_t163807967 ** get_address_of_debug_30() { return &___debug_30; }
	inline void set_debug_30(FsmBool_t163807967 * value)
	{
		___debug_30 = value;
		Il2CppCodeGenWriteBarrier((&___debug_30), value);
	}

	inline static int32_t get_offset_of__fromTrans_31() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ____fromTrans_31)); }
	inline Transform_t3600365921 * get__fromTrans_31() const { return ____fromTrans_31; }
	inline Transform_t3600365921 ** get_address_of__fromTrans_31() { return &____fromTrans_31; }
	inline void set__fromTrans_31(Transform_t3600365921 * value)
	{
		____fromTrans_31 = value;
		Il2CppCodeGenWriteBarrier((&____fromTrans_31), value);
	}

	inline static int32_t get_offset_of__toTrans_32() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ____toTrans_32)); }
	inline Transform_t3600365921 * get__toTrans_32() const { return ____toTrans_32; }
	inline Transform_t3600365921 ** get_address_of__toTrans_32() { return &____toTrans_32; }
	inline void set__toTrans_32(Transform_t3600365921 * value)
	{
		____toTrans_32 = value;
		Il2CppCodeGenWriteBarrier((&____toTrans_32), value);
	}

	inline static int32_t get_offset_of_repeat_33() { return static_cast<int32_t>(offsetof(LineCast2d_t30898162, ___repeat_33)); }
	inline int32_t get_repeat_33() const { return ___repeat_33; }
	inline int32_t* get_address_of_repeat_33() { return &___repeat_33; }
	inline void set_repeat_33(int32_t value)
	{
		___repeat_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINECAST2D_T30898162_H
#ifndef LOOKAT2D_T2942890213_H
#define LOOKAT2D_T2942890213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LookAt2d
struct  LookAt2d_t2942890213  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.LookAt2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.LookAt2d::vector2Target
	FsmVector2_t2965096677 * ___vector2Target_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.LookAt2d::vector3Target
	FsmVector3_t626444517 * ___vector3Target_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.LookAt2d::rotationOffset
	FsmFloat_t2883254149 * ___rotationOffset_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LookAt2d::debug
	FsmBool_t163807967 * ___debug_18;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.LookAt2d::debugLineColor
	FsmColor_t1738900188 * ___debugLineColor_19;
	// System.Boolean HutongGames.PlayMaker.Actions.LookAt2d::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector2Target_15() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___vector2Target_15)); }
	inline FsmVector2_t2965096677 * get_vector2Target_15() const { return ___vector2Target_15; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Target_15() { return &___vector2Target_15; }
	inline void set_vector2Target_15(FsmVector2_t2965096677 * value)
	{
		___vector2Target_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Target_15), value);
	}

	inline static int32_t get_offset_of_vector3Target_16() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___vector3Target_16)); }
	inline FsmVector3_t626444517 * get_vector3Target_16() const { return ___vector3Target_16; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Target_16() { return &___vector3Target_16; }
	inline void set_vector3Target_16(FsmVector3_t626444517 * value)
	{
		___vector3Target_16 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Target_16), value);
	}

	inline static int32_t get_offset_of_rotationOffset_17() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___rotationOffset_17)); }
	inline FsmFloat_t2883254149 * get_rotationOffset_17() const { return ___rotationOffset_17; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationOffset_17() { return &___rotationOffset_17; }
	inline void set_rotationOffset_17(FsmFloat_t2883254149 * value)
	{
		___rotationOffset_17 = value;
		Il2CppCodeGenWriteBarrier((&___rotationOffset_17), value);
	}

	inline static int32_t get_offset_of_debug_18() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___debug_18)); }
	inline FsmBool_t163807967 * get_debug_18() const { return ___debug_18; }
	inline FsmBool_t163807967 ** get_address_of_debug_18() { return &___debug_18; }
	inline void set_debug_18(FsmBool_t163807967 * value)
	{
		___debug_18 = value;
		Il2CppCodeGenWriteBarrier((&___debug_18), value);
	}

	inline static int32_t get_offset_of_debugLineColor_19() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___debugLineColor_19)); }
	inline FsmColor_t1738900188 * get_debugLineColor_19() const { return ___debugLineColor_19; }
	inline FsmColor_t1738900188 ** get_address_of_debugLineColor_19() { return &___debugLineColor_19; }
	inline void set_debugLineColor_19(FsmColor_t1738900188 * value)
	{
		___debugLineColor_19 = value;
		Il2CppCodeGenWriteBarrier((&___debugLineColor_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(LookAt2d_t2942890213, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKAT2D_T2942890213_H
#ifndef LOOKAT2DGAMEOBJECT_T1764891540_H
#define LOOKAT2DGAMEOBJECT_T1764891540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LookAt2dGameObject
struct  LookAt2dGameObject_t1764891540  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.LookAt2dGameObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LookAt2dGameObject::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.LookAt2dGameObject::rotationOffset
	FsmFloat_t2883254149 * ___rotationOffset_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LookAt2dGameObject::debug
	FsmBool_t163807967 * ___debug_17;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.LookAt2dGameObject::debugLineColor
	FsmColor_t1738900188 * ___debugLineColor_18;
	// System.Boolean HutongGames.PlayMaker.Actions.LookAt2dGameObject::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.LookAt2dGameObject::go
	GameObject_t1113636619 * ___go_20;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.LookAt2dGameObject::goTarget
	GameObject_t1113636619 * ___goTarget_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_rotationOffset_16() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___rotationOffset_16)); }
	inline FsmFloat_t2883254149 * get_rotationOffset_16() const { return ___rotationOffset_16; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationOffset_16() { return &___rotationOffset_16; }
	inline void set_rotationOffset_16(FsmFloat_t2883254149 * value)
	{
		___rotationOffset_16 = value;
		Il2CppCodeGenWriteBarrier((&___rotationOffset_16), value);
	}

	inline static int32_t get_offset_of_debug_17() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___debug_17)); }
	inline FsmBool_t163807967 * get_debug_17() const { return ___debug_17; }
	inline FsmBool_t163807967 ** get_address_of_debug_17() { return &___debug_17; }
	inline void set_debug_17(FsmBool_t163807967 * value)
	{
		___debug_17 = value;
		Il2CppCodeGenWriteBarrier((&___debug_17), value);
	}

	inline static int32_t get_offset_of_debugLineColor_18() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___debugLineColor_18)); }
	inline FsmColor_t1738900188 * get_debugLineColor_18() const { return ___debugLineColor_18; }
	inline FsmColor_t1738900188 ** get_address_of_debugLineColor_18() { return &___debugLineColor_18; }
	inline void set_debugLineColor_18(FsmColor_t1738900188 * value)
	{
		___debugLineColor_18 = value;
		Il2CppCodeGenWriteBarrier((&___debugLineColor_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_go_20() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___go_20)); }
	inline GameObject_t1113636619 * get_go_20() const { return ___go_20; }
	inline GameObject_t1113636619 ** get_address_of_go_20() { return &___go_20; }
	inline void set_go_20(GameObject_t1113636619 * value)
	{
		___go_20 = value;
		Il2CppCodeGenWriteBarrier((&___go_20), value);
	}

	inline static int32_t get_offset_of_goTarget_21() { return static_cast<int32_t>(offsetof(LookAt2dGameObject_t1764891540, ___goTarget_21)); }
	inline GameObject_t1113636619 * get_goTarget_21() const { return ___goTarget_21; }
	inline GameObject_t1113636619 ** get_address_of_goTarget_21() { return &___goTarget_21; }
	inline void set_goTarget_21(GameObject_t1113636619 * value)
	{
		___goTarget_21 = value;
		Il2CppCodeGenWriteBarrier((&___goTarget_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKAT2DGAMEOBJECT_T1764891540_H
#ifndef MOUSEPICK2D_T219204948_H
#define MOUSEPICK2D_T219204948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MousePick2d
struct  MousePick2d_t219204948  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick2d::storeDidPickObject
	FsmBool_t163807967 * ___storeDidPickObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.MousePick2d::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.MousePick2d::storePoint
	FsmVector2_t2965096677 * ___storePoint_16;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.MousePick2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick2d::invertMask
	FsmBool_t163807967 * ___invertMask_18;
	// System.Boolean HutongGames.PlayMaker.Actions.MousePick2d::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_storeDidPickObject_14() { return static_cast<int32_t>(offsetof(MousePick2d_t219204948, ___storeDidPickObject_14)); }
	inline FsmBool_t163807967 * get_storeDidPickObject_14() const { return ___storeDidPickObject_14; }
	inline FsmBool_t163807967 ** get_address_of_storeDidPickObject_14() { return &___storeDidPickObject_14; }
	inline void set_storeDidPickObject_14(FsmBool_t163807967 * value)
	{
		___storeDidPickObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidPickObject_14), value);
	}

	inline static int32_t get_offset_of_storeGameObject_15() { return static_cast<int32_t>(offsetof(MousePick2d_t219204948, ___storeGameObject_15)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_15() const { return ___storeGameObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_15() { return &___storeGameObject_15; }
	inline void set_storeGameObject_15(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_15), value);
	}

	inline static int32_t get_offset_of_storePoint_16() { return static_cast<int32_t>(offsetof(MousePick2d_t219204948, ___storePoint_16)); }
	inline FsmVector2_t2965096677 * get_storePoint_16() const { return ___storePoint_16; }
	inline FsmVector2_t2965096677 ** get_address_of_storePoint_16() { return &___storePoint_16; }
	inline void set_storePoint_16(FsmVector2_t2965096677 * value)
	{
		___storePoint_16 = value;
		Il2CppCodeGenWriteBarrier((&___storePoint_16), value);
	}

	inline static int32_t get_offset_of_layerMask_17() { return static_cast<int32_t>(offsetof(MousePick2d_t219204948, ___layerMask_17)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_17() const { return ___layerMask_17; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_17() { return &___layerMask_17; }
	inline void set_layerMask_17(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_17 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_17), value);
	}

	inline static int32_t get_offset_of_invertMask_18() { return static_cast<int32_t>(offsetof(MousePick2d_t219204948, ___invertMask_18)); }
	inline FsmBool_t163807967 * get_invertMask_18() const { return ___invertMask_18; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_18() { return &___invertMask_18; }
	inline void set_invertMask_18(FsmBool_t163807967 * value)
	{
		___invertMask_18 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(MousePick2d_t219204948, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEPICK2D_T219204948_H
#ifndef MOUSEPICK2DEVENT_T4132667874_H
#define MOUSEPICK2DEVENT_T4132667874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MousePick2dEvent
struct  MousePick2dEvent_t4132667874  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MousePick2dEvent::GameObject
	FsmOwnerDefault_t3590610434 * ___GameObject_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePick2dEvent::mouseOver
	FsmEvent_t3736299882 * ___mouseOver_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePick2dEvent::mouseDown
	FsmEvent_t3736299882 * ___mouseDown_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePick2dEvent::mouseUp
	FsmEvent_t3736299882 * ___mouseUp_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePick2dEvent::mouseOff
	FsmEvent_t3736299882 * ___mouseOff_18;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.MousePick2dEvent::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick2dEvent::invertMask
	FsmBool_t163807967 * ___invertMask_20;
	// System.Boolean HutongGames.PlayMaker.Actions.MousePick2dEvent::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_GameObject_14() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___GameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_GameObject_14() const { return ___GameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_GameObject_14() { return &___GameObject_14; }
	inline void set_GameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___GameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_14), value);
	}

	inline static int32_t get_offset_of_mouseOver_15() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___mouseOver_15)); }
	inline FsmEvent_t3736299882 * get_mouseOver_15() const { return ___mouseOver_15; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseOver_15() { return &___mouseOver_15; }
	inline void set_mouseOver_15(FsmEvent_t3736299882 * value)
	{
		___mouseOver_15 = value;
		Il2CppCodeGenWriteBarrier((&___mouseOver_15), value);
	}

	inline static int32_t get_offset_of_mouseDown_16() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___mouseDown_16)); }
	inline FsmEvent_t3736299882 * get_mouseDown_16() const { return ___mouseDown_16; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseDown_16() { return &___mouseDown_16; }
	inline void set_mouseDown_16(FsmEvent_t3736299882 * value)
	{
		___mouseDown_16 = value;
		Il2CppCodeGenWriteBarrier((&___mouseDown_16), value);
	}

	inline static int32_t get_offset_of_mouseUp_17() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___mouseUp_17)); }
	inline FsmEvent_t3736299882 * get_mouseUp_17() const { return ___mouseUp_17; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseUp_17() { return &___mouseUp_17; }
	inline void set_mouseUp_17(FsmEvent_t3736299882 * value)
	{
		___mouseUp_17 = value;
		Il2CppCodeGenWriteBarrier((&___mouseUp_17), value);
	}

	inline static int32_t get_offset_of_mouseOff_18() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___mouseOff_18)); }
	inline FsmEvent_t3736299882 * get_mouseOff_18() const { return ___mouseOff_18; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseOff_18() { return &___mouseOff_18; }
	inline void set_mouseOff_18(FsmEvent_t3736299882 * value)
	{
		___mouseOff_18 = value;
		Il2CppCodeGenWriteBarrier((&___mouseOff_18), value);
	}

	inline static int32_t get_offset_of_layerMask_19() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___layerMask_19)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_19() const { return ___layerMask_19; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_19() { return &___layerMask_19; }
	inline void set_layerMask_19(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_19 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_19), value);
	}

	inline static int32_t get_offset_of_invertMask_20() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___invertMask_20)); }
	inline FsmBool_t163807967 * get_invertMask_20() const { return ___invertMask_20; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_20() { return &___invertMask_20; }
	inline void set_invertMask_20(FsmBool_t163807967 * value)
	{
		___invertMask_20 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(MousePick2dEvent_t4132667874, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEPICK2DEVENT_T4132667874_H
#ifndef PLAYERPREFSDELETEALL_T724049092_H
#define PLAYERPREFSDELETEALL_T724049092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll
struct  PlayerPrefsDeleteAll_t724049092  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSDELETEALL_T724049092_H
#ifndef PLAYERPREFSDELETEKEY_T1869683915_H
#define PLAYERPREFSDELETEKEY_T1869683915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey
struct  PlayerPrefsDeleteKey_t1869683915  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey::key
	FsmString_t1785915204 * ___key_14;

public:
	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(PlayerPrefsDeleteKey_t1869683915, ___key_14)); }
	inline FsmString_t1785915204 * get_key_14() const { return ___key_14; }
	inline FsmString_t1785915204 ** get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(FsmString_t1785915204 * value)
	{
		___key_14 = value;
		Il2CppCodeGenWriteBarrier((&___key_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSDELETEKEY_T1869683915_H
#ifndef PLAYERPREFSGETFLOAT_T2326825523_H
#define PLAYERPREFSGETFLOAT_T2326825523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat
struct  PlayerPrefsGetFloat_t2326825523  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::keys
	FsmStringU5BU5D_t252501805* ___keys_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::variables
	FsmFloatU5BU5D_t3637897416* ___variables_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(PlayerPrefsGetFloat_t2326825523, ___keys_14)); }
	inline FsmStringU5BU5D_t252501805* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t252501805* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(PlayerPrefsGetFloat_t2326825523, ___variables_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_variables_15() const { return ___variables_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSGETFLOAT_T2326825523_H
#ifndef PLAYERPREFSGETINT_T3361394777_H
#define PLAYERPREFSGETINT_T3361394777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsGetInt
struct  PlayerPrefsGetInt_t3361394777  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::keys
	FsmStringU5BU5D_t252501805* ___keys_14;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::variables
	FsmIntU5BU5D_t2904461592* ___variables_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(PlayerPrefsGetInt_t3361394777, ___keys_14)); }
	inline FsmStringU5BU5D_t252501805* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t252501805* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(PlayerPrefsGetInt_t3361394777, ___variables_15)); }
	inline FsmIntU5BU5D_t2904461592* get_variables_15() const { return ___variables_15; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmIntU5BU5D_t2904461592* value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSGETINT_T3361394777_H
#ifndef PLAYERPREFSGETSTRING_T1146956221_H
#define PLAYERPREFSGETSTRING_T1146956221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsGetString
struct  PlayerPrefsGetString_t1146956221  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetString::keys
	FsmStringU5BU5D_t252501805* ___keys_14;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsGetString::variables
	FsmStringU5BU5D_t252501805* ___variables_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(PlayerPrefsGetString_t1146956221, ___keys_14)); }
	inline FsmStringU5BU5D_t252501805* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t252501805* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(PlayerPrefsGetString_t1146956221, ___variables_15)); }
	inline FsmStringU5BU5D_t252501805* get_variables_15() const { return ___variables_15; }
	inline FsmStringU5BU5D_t252501805** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmStringU5BU5D_t252501805* value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSGETSTRING_T1146956221_H
#ifndef PLAYERPREFSHASKEY_T4143475044_H
#define PLAYERPREFSHASKEY_T4143475044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsHasKey
struct  PlayerPrefsHasKey_t4143475044  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::key
	FsmString_t1785915204 * ___key_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::variable
	FsmBool_t163807967 * ___variable_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_17;

public:
	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(PlayerPrefsHasKey_t4143475044, ___key_14)); }
	inline FsmString_t1785915204 * get_key_14() const { return ___key_14; }
	inline FsmString_t1785915204 ** get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(FsmString_t1785915204 * value)
	{
		___key_14 = value;
		Il2CppCodeGenWriteBarrier((&___key_14), value);
	}

	inline static int32_t get_offset_of_variable_15() { return static_cast<int32_t>(offsetof(PlayerPrefsHasKey_t4143475044, ___variable_15)); }
	inline FsmBool_t163807967 * get_variable_15() const { return ___variable_15; }
	inline FsmBool_t163807967 ** get_address_of_variable_15() { return &___variable_15; }
	inline void set_variable_15(FsmBool_t163807967 * value)
	{
		___variable_15 = value;
		Il2CppCodeGenWriteBarrier((&___variable_15), value);
	}

	inline static int32_t get_offset_of_trueEvent_16() { return static_cast<int32_t>(offsetof(PlayerPrefsHasKey_t4143475044, ___trueEvent_16)); }
	inline FsmEvent_t3736299882 * get_trueEvent_16() const { return ___trueEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_16() { return &___trueEvent_16; }
	inline void set_trueEvent_16(FsmEvent_t3736299882 * value)
	{
		___trueEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_16), value);
	}

	inline static int32_t get_offset_of_falseEvent_17() { return static_cast<int32_t>(offsetof(PlayerPrefsHasKey_t4143475044, ___falseEvent_17)); }
	inline FsmEvent_t3736299882 * get_falseEvent_17() const { return ___falseEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_17() { return &___falseEvent_17; }
	inline void set_falseEvent_17(FsmEvent_t3736299882 * value)
	{
		___falseEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSHASKEY_T4143475044_H
#ifndef PLAYERPREFSSETFLOAT_T1453099571_H
#define PLAYERPREFSSETFLOAT_T1453099571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat
struct  PlayerPrefsSetFloat_t1453099571  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::keys
	FsmStringU5BU5D_t252501805* ___keys_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::values
	FsmFloatU5BU5D_t3637897416* ___values_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(PlayerPrefsSetFloat_t1453099571, ___keys_14)); }
	inline FsmStringU5BU5D_t252501805* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t252501805* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(PlayerPrefsSetFloat_t1453099571, ___values_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_values_15() const { return ___values_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier((&___values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSSETFLOAT_T1453099571_H
#ifndef PLAYERPREFSSETINT_T3334918233_H
#define PLAYERPREFSSETINT_T3334918233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsSetInt
struct  PlayerPrefsSetInt_t3334918233  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsSetInt::keys
	FsmStringU5BU5D_t252501805* ___keys_14;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.PlayerPrefsSetInt::values
	FsmIntU5BU5D_t2904461592* ___values_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(PlayerPrefsSetInt_t3334918233, ___keys_14)); }
	inline FsmStringU5BU5D_t252501805* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t252501805* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(PlayerPrefsSetInt_t3334918233, ___values_15)); }
	inline FsmIntU5BU5D_t2904461592* get_values_15() const { return ___values_15; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(FsmIntU5BU5D_t2904461592* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier((&___values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSSETINT_T3334918233_H
#ifndef RANDOMBOOL_T566293558_H
#define RANDOMBOOL_T566293558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RandomBool
struct  RandomBool_t566293558  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RandomBool::storeResult
	FsmBool_t163807967 * ___storeResult_14;

public:
	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(RandomBool_t566293558, ___storeResult_14)); }
	inline FsmBool_t163807967 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmBool_t163807967 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMBOOL_T566293558_H
#ifndef RANDOMFLOAT_T3836739527_H
#define RANDOMFLOAT_T3836739527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RandomFloat
struct  RandomFloat_t3836739527  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RandomFloat::min
	FsmFloat_t2883254149 * ___min_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RandomFloat::max
	FsmFloat_t2883254149 * ___max_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RandomFloat::storeResult
	FsmFloat_t2883254149 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_min_14() { return static_cast<int32_t>(offsetof(RandomFloat_t3836739527, ___min_14)); }
	inline FsmFloat_t2883254149 * get_min_14() const { return ___min_14; }
	inline FsmFloat_t2883254149 ** get_address_of_min_14() { return &___min_14; }
	inline void set_min_14(FsmFloat_t2883254149 * value)
	{
		___min_14 = value;
		Il2CppCodeGenWriteBarrier((&___min_14), value);
	}

	inline static int32_t get_offset_of_max_15() { return static_cast<int32_t>(offsetof(RandomFloat_t3836739527, ___max_15)); }
	inline FsmFloat_t2883254149 * get_max_15() const { return ___max_15; }
	inline FsmFloat_t2883254149 ** get_address_of_max_15() { return &___max_15; }
	inline void set_max_15(FsmFloat_t2883254149 * value)
	{
		___max_15 = value;
		Il2CppCodeGenWriteBarrier((&___max_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(RandomFloat_t3836739527, ___storeResult_16)); }
	inline FsmFloat_t2883254149 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmFloat_t2883254149 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMFLOAT_T3836739527_H
#ifndef RANDOMINT_T1104706112_H
#define RANDOMINT_T1104706112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RandomInt
struct  RandomInt_t1104706112  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RandomInt::min
	FsmInt_t874273141 * ___min_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RandomInt::max
	FsmInt_t874273141 * ___max_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RandomInt::storeResult
	FsmInt_t874273141 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.RandomInt::inclusiveMax
	bool ___inclusiveMax_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RandomInt::noRepeat
	FsmBool_t163807967 * ___noRepeat_18;
	// System.Int32 HutongGames.PlayMaker.Actions.RandomInt::randomIndex
	int32_t ___randomIndex_19;
	// System.Int32 HutongGames.PlayMaker.Actions.RandomInt::lastIndex
	int32_t ___lastIndex_20;

public:
	inline static int32_t get_offset_of_min_14() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___min_14)); }
	inline FsmInt_t874273141 * get_min_14() const { return ___min_14; }
	inline FsmInt_t874273141 ** get_address_of_min_14() { return &___min_14; }
	inline void set_min_14(FsmInt_t874273141 * value)
	{
		___min_14 = value;
		Il2CppCodeGenWriteBarrier((&___min_14), value);
	}

	inline static int32_t get_offset_of_max_15() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___max_15)); }
	inline FsmInt_t874273141 * get_max_15() const { return ___max_15; }
	inline FsmInt_t874273141 ** get_address_of_max_15() { return &___max_15; }
	inline void set_max_15(FsmInt_t874273141 * value)
	{
		___max_15 = value;
		Il2CppCodeGenWriteBarrier((&___max_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___storeResult_16)); }
	inline FsmInt_t874273141 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmInt_t874273141 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmInt_t874273141 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_inclusiveMax_17() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___inclusiveMax_17)); }
	inline bool get_inclusiveMax_17() const { return ___inclusiveMax_17; }
	inline bool* get_address_of_inclusiveMax_17() { return &___inclusiveMax_17; }
	inline void set_inclusiveMax_17(bool value)
	{
		___inclusiveMax_17 = value;
	}

	inline static int32_t get_offset_of_noRepeat_18() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___noRepeat_18)); }
	inline FsmBool_t163807967 * get_noRepeat_18() const { return ___noRepeat_18; }
	inline FsmBool_t163807967 ** get_address_of_noRepeat_18() { return &___noRepeat_18; }
	inline void set_noRepeat_18(FsmBool_t163807967 * value)
	{
		___noRepeat_18 = value;
		Il2CppCodeGenWriteBarrier((&___noRepeat_18), value);
	}

	inline static int32_t get_offset_of_randomIndex_19() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___randomIndex_19)); }
	inline int32_t get_randomIndex_19() const { return ___randomIndex_19; }
	inline int32_t* get_address_of_randomIndex_19() { return &___randomIndex_19; }
	inline void set_randomIndex_19(int32_t value)
	{
		___randomIndex_19 = value;
	}

	inline static int32_t get_offset_of_lastIndex_20() { return static_cast<int32_t>(offsetof(RandomInt_t1104706112, ___lastIndex_20)); }
	inline int32_t get_lastIndex_20() const { return ___lastIndex_20; }
	inline int32_t* get_address_of_lastIndex_20() { return &___lastIndex_20; }
	inline void set_lastIndex_20(int32_t value)
	{
		___lastIndex_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMINT_T1104706112_H
#ifndef RAYCAST2D_T1611024692_H
#define RAYCAST2D_T1611024692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RayCast2d
struct  RayCast2d_t1611024692  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RayCast2d::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RayCast2d::fromPosition
	FsmVector2_t2965096677 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RayCast2d::direction
	FsmVector2_t2965096677 * ___direction_16;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.RayCast2d::space
	int32_t ___space_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RayCast2d::distance
	FsmFloat_t2883254149 * ___distance_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RayCast2d::minDepth
	FsmInt_t874273141 * ___minDepth_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RayCast2d::maxDepth
	FsmInt_t874273141 * ___maxDepth_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RayCast2d::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RayCast2d::storeDidHit
	FsmBool_t163807967 * ___storeDidHit_22;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RayCast2d::storeHitObject
	FsmGameObject_t3581898942 * ___storeHitObject_23;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RayCast2d::storeHitPoint
	FsmVector2_t2965096677 * ___storeHitPoint_24;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RayCast2d::storeHitNormal
	FsmVector2_t2965096677 * ___storeHitNormal_25;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RayCast2d::storeHitDistance
	FsmFloat_t2883254149 * ___storeHitDistance_26;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RayCast2d::storeHitFraction
	FsmFloat_t2883254149 * ___storeHitFraction_27;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RayCast2d::repeatInterval
	FsmInt_t874273141 * ___repeatInterval_28;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.RayCast2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_29;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RayCast2d::invertMask
	FsmBool_t163807967 * ___invertMask_30;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.RayCast2d::debugColor
	FsmColor_t1738900188 * ___debugColor_31;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RayCast2d::debug
	FsmBool_t163807967 * ___debug_32;
	// UnityEngine.Transform HutongGames.PlayMaker.Actions.RayCast2d::_transform
	Transform_t3600365921 * ____transform_33;
	// System.Int32 HutongGames.PlayMaker.Actions.RayCast2d::repeat
	int32_t ___repeat_34;

public:
	inline static int32_t get_offset_of_fromGameObject_14() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___fromGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_14() const { return ___fromGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_14() { return &___fromGameObject_14; }
	inline void set_fromGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___fromPosition_15)); }
	inline FsmVector2_t2965096677 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector2_t2965096677 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector2_t2965096677 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___direction_16)); }
	inline FsmVector2_t2965096677 * get_direction_16() const { return ___direction_16; }
	inline FsmVector2_t2965096677 ** get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(FsmVector2_t2965096677 * value)
	{
		___direction_16 = value;
		Il2CppCodeGenWriteBarrier((&___direction_16), value);
	}

	inline static int32_t get_offset_of_space_17() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___space_17)); }
	inline int32_t get_space_17() const { return ___space_17; }
	inline int32_t* get_address_of_space_17() { return &___space_17; }
	inline void set_space_17(int32_t value)
	{
		___space_17 = value;
	}

	inline static int32_t get_offset_of_distance_18() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___distance_18)); }
	inline FsmFloat_t2883254149 * get_distance_18() const { return ___distance_18; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_18() { return &___distance_18; }
	inline void set_distance_18(FsmFloat_t2883254149 * value)
	{
		___distance_18 = value;
		Il2CppCodeGenWriteBarrier((&___distance_18), value);
	}

	inline static int32_t get_offset_of_minDepth_19() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___minDepth_19)); }
	inline FsmInt_t874273141 * get_minDepth_19() const { return ___minDepth_19; }
	inline FsmInt_t874273141 ** get_address_of_minDepth_19() { return &___minDepth_19; }
	inline void set_minDepth_19(FsmInt_t874273141 * value)
	{
		___minDepth_19 = value;
		Il2CppCodeGenWriteBarrier((&___minDepth_19), value);
	}

	inline static int32_t get_offset_of_maxDepth_20() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___maxDepth_20)); }
	inline FsmInt_t874273141 * get_maxDepth_20() const { return ___maxDepth_20; }
	inline FsmInt_t874273141 ** get_address_of_maxDepth_20() { return &___maxDepth_20; }
	inline void set_maxDepth_20(FsmInt_t874273141 * value)
	{
		___maxDepth_20 = value;
		Il2CppCodeGenWriteBarrier((&___maxDepth_20), value);
	}

	inline static int32_t get_offset_of_hitEvent_21() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___hitEvent_21)); }
	inline FsmEvent_t3736299882 * get_hitEvent_21() const { return ___hitEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_21() { return &___hitEvent_21; }
	inline void set_hitEvent_21(FsmEvent_t3736299882 * value)
	{
		___hitEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_21), value);
	}

	inline static int32_t get_offset_of_storeDidHit_22() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___storeDidHit_22)); }
	inline FsmBool_t163807967 * get_storeDidHit_22() const { return ___storeDidHit_22; }
	inline FsmBool_t163807967 ** get_address_of_storeDidHit_22() { return &___storeDidHit_22; }
	inline void set_storeDidHit_22(FsmBool_t163807967 * value)
	{
		___storeDidHit_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidHit_22), value);
	}

	inline static int32_t get_offset_of_storeHitObject_23() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___storeHitObject_23)); }
	inline FsmGameObject_t3581898942 * get_storeHitObject_23() const { return ___storeHitObject_23; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeHitObject_23() { return &___storeHitObject_23; }
	inline void set_storeHitObject_23(FsmGameObject_t3581898942 * value)
	{
		___storeHitObject_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitObject_23), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_24() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___storeHitPoint_24)); }
	inline FsmVector2_t2965096677 * get_storeHitPoint_24() const { return ___storeHitPoint_24; }
	inline FsmVector2_t2965096677 ** get_address_of_storeHitPoint_24() { return &___storeHitPoint_24; }
	inline void set_storeHitPoint_24(FsmVector2_t2965096677 * value)
	{
		___storeHitPoint_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_24), value);
	}

	inline static int32_t get_offset_of_storeHitNormal_25() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___storeHitNormal_25)); }
	inline FsmVector2_t2965096677 * get_storeHitNormal_25() const { return ___storeHitNormal_25; }
	inline FsmVector2_t2965096677 ** get_address_of_storeHitNormal_25() { return &___storeHitNormal_25; }
	inline void set_storeHitNormal_25(FsmVector2_t2965096677 * value)
	{
		___storeHitNormal_25 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitNormal_25), value);
	}

	inline static int32_t get_offset_of_storeHitDistance_26() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___storeHitDistance_26)); }
	inline FsmFloat_t2883254149 * get_storeHitDistance_26() const { return ___storeHitDistance_26; }
	inline FsmFloat_t2883254149 ** get_address_of_storeHitDistance_26() { return &___storeHitDistance_26; }
	inline void set_storeHitDistance_26(FsmFloat_t2883254149 * value)
	{
		___storeHitDistance_26 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitDistance_26), value);
	}

	inline static int32_t get_offset_of_storeHitFraction_27() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___storeHitFraction_27)); }
	inline FsmFloat_t2883254149 * get_storeHitFraction_27() const { return ___storeHitFraction_27; }
	inline FsmFloat_t2883254149 ** get_address_of_storeHitFraction_27() { return &___storeHitFraction_27; }
	inline void set_storeHitFraction_27(FsmFloat_t2883254149 * value)
	{
		___storeHitFraction_27 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitFraction_27), value);
	}

	inline static int32_t get_offset_of_repeatInterval_28() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___repeatInterval_28)); }
	inline FsmInt_t874273141 * get_repeatInterval_28() const { return ___repeatInterval_28; }
	inline FsmInt_t874273141 ** get_address_of_repeatInterval_28() { return &___repeatInterval_28; }
	inline void set_repeatInterval_28(FsmInt_t874273141 * value)
	{
		___repeatInterval_28 = value;
		Il2CppCodeGenWriteBarrier((&___repeatInterval_28), value);
	}

	inline static int32_t get_offset_of_layerMask_29() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___layerMask_29)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_29() const { return ___layerMask_29; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_29() { return &___layerMask_29; }
	inline void set_layerMask_29(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_29 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_29), value);
	}

	inline static int32_t get_offset_of_invertMask_30() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___invertMask_30)); }
	inline FsmBool_t163807967 * get_invertMask_30() const { return ___invertMask_30; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_30() { return &___invertMask_30; }
	inline void set_invertMask_30(FsmBool_t163807967 * value)
	{
		___invertMask_30 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_30), value);
	}

	inline static int32_t get_offset_of_debugColor_31() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___debugColor_31)); }
	inline FsmColor_t1738900188 * get_debugColor_31() const { return ___debugColor_31; }
	inline FsmColor_t1738900188 ** get_address_of_debugColor_31() { return &___debugColor_31; }
	inline void set_debugColor_31(FsmColor_t1738900188 * value)
	{
		___debugColor_31 = value;
		Il2CppCodeGenWriteBarrier((&___debugColor_31), value);
	}

	inline static int32_t get_offset_of_debug_32() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___debug_32)); }
	inline FsmBool_t163807967 * get_debug_32() const { return ___debug_32; }
	inline FsmBool_t163807967 ** get_address_of_debug_32() { return &___debug_32; }
	inline void set_debug_32(FsmBool_t163807967 * value)
	{
		___debug_32 = value;
		Il2CppCodeGenWriteBarrier((&___debug_32), value);
	}

	inline static int32_t get_offset_of__transform_33() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ____transform_33)); }
	inline Transform_t3600365921 * get__transform_33() const { return ____transform_33; }
	inline Transform_t3600365921 ** get_address_of__transform_33() { return &____transform_33; }
	inline void set__transform_33(Transform_t3600365921 * value)
	{
		____transform_33 = value;
		Il2CppCodeGenWriteBarrier((&____transform_33), value);
	}

	inline static int32_t get_offset_of_repeat_34() { return static_cast<int32_t>(offsetof(RayCast2d_t1611024692, ___repeat_34)); }
	inline int32_t get_repeat_34() const { return ___repeat_34; }
	inline int32_t* get_address_of_repeat_34() { return &___repeat_34; }
	inline void set_repeat_34(int32_t value)
	{
		___repeat_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2D_T1611024692_H
#ifndef RAYCAST_T3332644267_H
#define RAYCAST_T3332644267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Raycast
struct  Raycast_t3332644267  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Raycast::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::fromPosition
	FsmVector3_t626444517 * ___fromPosition_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::direction
	FsmVector3_t626444517 * ___direction_16;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.Raycast::space
	int32_t ___space_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Raycast::distance
	FsmFloat_t2883254149 * ___distance_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Raycast::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Raycast::storeDidHit
	FsmBool_t163807967 * ___storeDidHit_20;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.Raycast::storeHitObject
	FsmGameObject_t3581898942 * ___storeHitObject_21;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::storeHitPoint
	FsmVector3_t626444517 * ___storeHitPoint_22;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Raycast::storeHitNormal
	FsmVector3_t626444517 * ___storeHitNormal_23;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Raycast::storeHitDistance
	FsmFloat_t2883254149 * ___storeHitDistance_24;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Raycast::repeatInterval
	FsmInt_t874273141 * ___repeatInterval_25;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.Raycast::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Raycast::invertMask
	FsmBool_t163807967 * ___invertMask_27;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.Raycast::debugColor
	FsmColor_t1738900188 * ___debugColor_28;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Raycast::debug
	FsmBool_t163807967 * ___debug_29;
	// System.Int32 HutongGames.PlayMaker.Actions.Raycast::repeat
	int32_t ___repeat_30;

public:
	inline static int32_t get_offset_of_fromGameObject_14() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___fromGameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_14() const { return ___fromGameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_14() { return &___fromGameObject_14; }
	inline void set_fromGameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_14), value);
	}

	inline static int32_t get_offset_of_fromPosition_15() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___fromPosition_15)); }
	inline FsmVector3_t626444517 * get_fromPosition_15() const { return ___fromPosition_15; }
	inline FsmVector3_t626444517 ** get_address_of_fromPosition_15() { return &___fromPosition_15; }
	inline void set_fromPosition_15(FsmVector3_t626444517 * value)
	{
		___fromPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_15), value);
	}

	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___direction_16)); }
	inline FsmVector3_t626444517 * get_direction_16() const { return ___direction_16; }
	inline FsmVector3_t626444517 ** get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(FsmVector3_t626444517 * value)
	{
		___direction_16 = value;
		Il2CppCodeGenWriteBarrier((&___direction_16), value);
	}

	inline static int32_t get_offset_of_space_17() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___space_17)); }
	inline int32_t get_space_17() const { return ___space_17; }
	inline int32_t* get_address_of_space_17() { return &___space_17; }
	inline void set_space_17(int32_t value)
	{
		___space_17 = value;
	}

	inline static int32_t get_offset_of_distance_18() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___distance_18)); }
	inline FsmFloat_t2883254149 * get_distance_18() const { return ___distance_18; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_18() { return &___distance_18; }
	inline void set_distance_18(FsmFloat_t2883254149 * value)
	{
		___distance_18 = value;
		Il2CppCodeGenWriteBarrier((&___distance_18), value);
	}

	inline static int32_t get_offset_of_hitEvent_19() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___hitEvent_19)); }
	inline FsmEvent_t3736299882 * get_hitEvent_19() const { return ___hitEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_19() { return &___hitEvent_19; }
	inline void set_hitEvent_19(FsmEvent_t3736299882 * value)
	{
		___hitEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_19), value);
	}

	inline static int32_t get_offset_of_storeDidHit_20() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___storeDidHit_20)); }
	inline FsmBool_t163807967 * get_storeDidHit_20() const { return ___storeDidHit_20; }
	inline FsmBool_t163807967 ** get_address_of_storeDidHit_20() { return &___storeDidHit_20; }
	inline void set_storeDidHit_20(FsmBool_t163807967 * value)
	{
		___storeDidHit_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidHit_20), value);
	}

	inline static int32_t get_offset_of_storeHitObject_21() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___storeHitObject_21)); }
	inline FsmGameObject_t3581898942 * get_storeHitObject_21() const { return ___storeHitObject_21; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeHitObject_21() { return &___storeHitObject_21; }
	inline void set_storeHitObject_21(FsmGameObject_t3581898942 * value)
	{
		___storeHitObject_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitObject_21), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_22() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___storeHitPoint_22)); }
	inline FsmVector3_t626444517 * get_storeHitPoint_22() const { return ___storeHitPoint_22; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitPoint_22() { return &___storeHitPoint_22; }
	inline void set_storeHitPoint_22(FsmVector3_t626444517 * value)
	{
		___storeHitPoint_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_22), value);
	}

	inline static int32_t get_offset_of_storeHitNormal_23() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___storeHitNormal_23)); }
	inline FsmVector3_t626444517 * get_storeHitNormal_23() const { return ___storeHitNormal_23; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitNormal_23() { return &___storeHitNormal_23; }
	inline void set_storeHitNormal_23(FsmVector3_t626444517 * value)
	{
		___storeHitNormal_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitNormal_23), value);
	}

	inline static int32_t get_offset_of_storeHitDistance_24() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___storeHitDistance_24)); }
	inline FsmFloat_t2883254149 * get_storeHitDistance_24() const { return ___storeHitDistance_24; }
	inline FsmFloat_t2883254149 ** get_address_of_storeHitDistance_24() { return &___storeHitDistance_24; }
	inline void set_storeHitDistance_24(FsmFloat_t2883254149 * value)
	{
		___storeHitDistance_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitDistance_24), value);
	}

	inline static int32_t get_offset_of_repeatInterval_25() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___repeatInterval_25)); }
	inline FsmInt_t874273141 * get_repeatInterval_25() const { return ___repeatInterval_25; }
	inline FsmInt_t874273141 ** get_address_of_repeatInterval_25() { return &___repeatInterval_25; }
	inline void set_repeatInterval_25(FsmInt_t874273141 * value)
	{
		___repeatInterval_25 = value;
		Il2CppCodeGenWriteBarrier((&___repeatInterval_25), value);
	}

	inline static int32_t get_offset_of_layerMask_26() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___layerMask_26)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_26() const { return ___layerMask_26; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_26() { return &___layerMask_26; }
	inline void set_layerMask_26(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_26 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_26), value);
	}

	inline static int32_t get_offset_of_invertMask_27() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___invertMask_27)); }
	inline FsmBool_t163807967 * get_invertMask_27() const { return ___invertMask_27; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_27() { return &___invertMask_27; }
	inline void set_invertMask_27(FsmBool_t163807967 * value)
	{
		___invertMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_27), value);
	}

	inline static int32_t get_offset_of_debugColor_28() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___debugColor_28)); }
	inline FsmColor_t1738900188 * get_debugColor_28() const { return ___debugColor_28; }
	inline FsmColor_t1738900188 ** get_address_of_debugColor_28() { return &___debugColor_28; }
	inline void set_debugColor_28(FsmColor_t1738900188 * value)
	{
		___debugColor_28 = value;
		Il2CppCodeGenWriteBarrier((&___debugColor_28), value);
	}

	inline static int32_t get_offset_of_debug_29() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___debug_29)); }
	inline FsmBool_t163807967 * get_debug_29() const { return ___debug_29; }
	inline FsmBool_t163807967 ** get_address_of_debug_29() { return &___debug_29; }
	inline void set_debug_29(FsmBool_t163807967 * value)
	{
		___debug_29 = value;
		Il2CppCodeGenWriteBarrier((&___debug_29), value);
	}

	inline static int32_t get_offset_of_repeat_30() { return static_cast<int32_t>(offsetof(Raycast_t3332644267, ___repeat_30)); }
	inline int32_t get_repeat_30() const { return ___repeat_30; }
	inline int32_t* get_address_of_repeat_30() { return &___repeat_30; }
	inline void set_repeat_30(int32_t value)
	{
		___repeat_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST_T3332644267_H
#ifndef RAYCASTALL_T710648924_H
#define RAYCASTALL_T710648924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RaycastAll
struct  RaycastAll_t710648924  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RaycastAll::fromGameObject
	FsmOwnerDefault_t3590610434 * ___fromGameObject_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RaycastAll::fromPosition
	FsmVector3_t626444517 * ___fromPosition_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RaycastAll::direction
	FsmVector3_t626444517 * ___direction_17;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.RaycastAll::space
	int32_t ___space_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RaycastAll::distance
	FsmFloat_t2883254149 * ___distance_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RaycastAll::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RaycastAll::storeDidHit
	FsmBool_t163807967 * ___storeDidHit_21;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.RaycastAll::storeHitObjects
	FsmArray_t1756862219 * ___storeHitObjects_22;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RaycastAll::storeHitPoint
	FsmVector3_t626444517 * ___storeHitPoint_23;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RaycastAll::storeHitNormal
	FsmVector3_t626444517 * ___storeHitNormal_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RaycastAll::storeHitDistance
	FsmFloat_t2883254149 * ___storeHitDistance_25;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RaycastAll::repeatInterval
	FsmInt_t874273141 * ___repeatInterval_26;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.RaycastAll::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_27;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RaycastAll::invertMask
	FsmBool_t163807967 * ___invertMask_28;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.RaycastAll::debugColor
	FsmColor_t1738900188 * ___debugColor_29;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RaycastAll::debug
	FsmBool_t163807967 * ___debug_30;
	// System.Int32 HutongGames.PlayMaker.Actions.RaycastAll::repeat
	int32_t ___repeat_31;

public:
	inline static int32_t get_offset_of_fromGameObject_15() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___fromGameObject_15)); }
	inline FsmOwnerDefault_t3590610434 * get_fromGameObject_15() const { return ___fromGameObject_15; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_fromGameObject_15() { return &___fromGameObject_15; }
	inline void set_fromGameObject_15(FsmOwnerDefault_t3590610434 * value)
	{
		___fromGameObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___fromGameObject_15), value);
	}

	inline static int32_t get_offset_of_fromPosition_16() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___fromPosition_16)); }
	inline FsmVector3_t626444517 * get_fromPosition_16() const { return ___fromPosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_fromPosition_16() { return &___fromPosition_16; }
	inline void set_fromPosition_16(FsmVector3_t626444517 * value)
	{
		___fromPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___fromPosition_16), value);
	}

	inline static int32_t get_offset_of_direction_17() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___direction_17)); }
	inline FsmVector3_t626444517 * get_direction_17() const { return ___direction_17; }
	inline FsmVector3_t626444517 ** get_address_of_direction_17() { return &___direction_17; }
	inline void set_direction_17(FsmVector3_t626444517 * value)
	{
		___direction_17 = value;
		Il2CppCodeGenWriteBarrier((&___direction_17), value);
	}

	inline static int32_t get_offset_of_space_18() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___space_18)); }
	inline int32_t get_space_18() const { return ___space_18; }
	inline int32_t* get_address_of_space_18() { return &___space_18; }
	inline void set_space_18(int32_t value)
	{
		___space_18 = value;
	}

	inline static int32_t get_offset_of_distance_19() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___distance_19)); }
	inline FsmFloat_t2883254149 * get_distance_19() const { return ___distance_19; }
	inline FsmFloat_t2883254149 ** get_address_of_distance_19() { return &___distance_19; }
	inline void set_distance_19(FsmFloat_t2883254149 * value)
	{
		___distance_19 = value;
		Il2CppCodeGenWriteBarrier((&___distance_19), value);
	}

	inline static int32_t get_offset_of_hitEvent_20() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___hitEvent_20)); }
	inline FsmEvent_t3736299882 * get_hitEvent_20() const { return ___hitEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_20() { return &___hitEvent_20; }
	inline void set_hitEvent_20(FsmEvent_t3736299882 * value)
	{
		___hitEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_20), value);
	}

	inline static int32_t get_offset_of_storeDidHit_21() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___storeDidHit_21)); }
	inline FsmBool_t163807967 * get_storeDidHit_21() const { return ___storeDidHit_21; }
	inline FsmBool_t163807967 ** get_address_of_storeDidHit_21() { return &___storeDidHit_21; }
	inline void set_storeDidHit_21(FsmBool_t163807967 * value)
	{
		___storeDidHit_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidHit_21), value);
	}

	inline static int32_t get_offset_of_storeHitObjects_22() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___storeHitObjects_22)); }
	inline FsmArray_t1756862219 * get_storeHitObjects_22() const { return ___storeHitObjects_22; }
	inline FsmArray_t1756862219 ** get_address_of_storeHitObjects_22() { return &___storeHitObjects_22; }
	inline void set_storeHitObjects_22(FsmArray_t1756862219 * value)
	{
		___storeHitObjects_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitObjects_22), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_23() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___storeHitPoint_23)); }
	inline FsmVector3_t626444517 * get_storeHitPoint_23() const { return ___storeHitPoint_23; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitPoint_23() { return &___storeHitPoint_23; }
	inline void set_storeHitPoint_23(FsmVector3_t626444517 * value)
	{
		___storeHitPoint_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_23), value);
	}

	inline static int32_t get_offset_of_storeHitNormal_24() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___storeHitNormal_24)); }
	inline FsmVector3_t626444517 * get_storeHitNormal_24() const { return ___storeHitNormal_24; }
	inline FsmVector3_t626444517 ** get_address_of_storeHitNormal_24() { return &___storeHitNormal_24; }
	inline void set_storeHitNormal_24(FsmVector3_t626444517 * value)
	{
		___storeHitNormal_24 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitNormal_24), value);
	}

	inline static int32_t get_offset_of_storeHitDistance_25() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___storeHitDistance_25)); }
	inline FsmFloat_t2883254149 * get_storeHitDistance_25() const { return ___storeHitDistance_25; }
	inline FsmFloat_t2883254149 ** get_address_of_storeHitDistance_25() { return &___storeHitDistance_25; }
	inline void set_storeHitDistance_25(FsmFloat_t2883254149 * value)
	{
		___storeHitDistance_25 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitDistance_25), value);
	}

	inline static int32_t get_offset_of_repeatInterval_26() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___repeatInterval_26)); }
	inline FsmInt_t874273141 * get_repeatInterval_26() const { return ___repeatInterval_26; }
	inline FsmInt_t874273141 ** get_address_of_repeatInterval_26() { return &___repeatInterval_26; }
	inline void set_repeatInterval_26(FsmInt_t874273141 * value)
	{
		___repeatInterval_26 = value;
		Il2CppCodeGenWriteBarrier((&___repeatInterval_26), value);
	}

	inline static int32_t get_offset_of_layerMask_27() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___layerMask_27)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_27() const { return ___layerMask_27; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_27() { return &___layerMask_27; }
	inline void set_layerMask_27(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_27 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_27), value);
	}

	inline static int32_t get_offset_of_invertMask_28() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___invertMask_28)); }
	inline FsmBool_t163807967 * get_invertMask_28() const { return ___invertMask_28; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_28() { return &___invertMask_28; }
	inline void set_invertMask_28(FsmBool_t163807967 * value)
	{
		___invertMask_28 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_28), value);
	}

	inline static int32_t get_offset_of_debugColor_29() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___debugColor_29)); }
	inline FsmColor_t1738900188 * get_debugColor_29() const { return ___debugColor_29; }
	inline FsmColor_t1738900188 ** get_address_of_debugColor_29() { return &___debugColor_29; }
	inline void set_debugColor_29(FsmColor_t1738900188 * value)
	{
		___debugColor_29 = value;
		Il2CppCodeGenWriteBarrier((&___debugColor_29), value);
	}

	inline static int32_t get_offset_of_debug_30() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___debug_30)); }
	inline FsmBool_t163807967 * get_debug_30() const { return ___debug_30; }
	inline FsmBool_t163807967 ** get_address_of_debug_30() { return &___debug_30; }
	inline void set_debug_30(FsmBool_t163807967 * value)
	{
		___debug_30 = value;
		Il2CppCodeGenWriteBarrier((&___debug_30), value);
	}

	inline static int32_t get_offset_of_repeat_31() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924, ___repeat_31)); }
	inline int32_t get_repeat_31() const { return ___repeat_31; }
	inline int32_t* get_address_of_repeat_31() { return &___repeat_31; }
	inline void set_repeat_31(int32_t value)
	{
		___repeat_31 = value;
	}
};

struct RaycastAll_t710648924_StaticFields
{
public:
	// UnityEngine.RaycastHit[] HutongGames.PlayMaker.Actions.RaycastAll::RaycastAllHitInfo
	RaycastHitU5BU5D_t1690781147* ___RaycastAllHitInfo_14;

public:
	inline static int32_t get_offset_of_RaycastAllHitInfo_14() { return static_cast<int32_t>(offsetof(RaycastAll_t710648924_StaticFields, ___RaycastAllHitInfo_14)); }
	inline RaycastHitU5BU5D_t1690781147* get_RaycastAllHitInfo_14() const { return ___RaycastAllHitInfo_14; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_RaycastAllHitInfo_14() { return &___RaycastAllHitInfo_14; }
	inline void set_RaycastAllHitInfo_14(RaycastHitU5BU5D_t1690781147* value)
	{
		___RaycastAllHitInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___RaycastAllHitInfo_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALL_T710648924_H
#ifndef SAMPLECURVE_T4047358761_H
#define SAMPLECURVE_T4047358761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SampleCurve
struct  SampleCurve_t4047358761  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.SampleCurve::curve
	FsmAnimationCurve_t3432711236 * ___curve_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SampleCurve::sampleAt
	FsmFloat_t2883254149 * ___sampleAt_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SampleCurve::storeValue
	FsmFloat_t2883254149 * ___storeValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SampleCurve::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_curve_14() { return static_cast<int32_t>(offsetof(SampleCurve_t4047358761, ___curve_14)); }
	inline FsmAnimationCurve_t3432711236 * get_curve_14() const { return ___curve_14; }
	inline FsmAnimationCurve_t3432711236 ** get_address_of_curve_14() { return &___curve_14; }
	inline void set_curve_14(FsmAnimationCurve_t3432711236 * value)
	{
		___curve_14 = value;
		Il2CppCodeGenWriteBarrier((&___curve_14), value);
	}

	inline static int32_t get_offset_of_sampleAt_15() { return static_cast<int32_t>(offsetof(SampleCurve_t4047358761, ___sampleAt_15)); }
	inline FsmFloat_t2883254149 * get_sampleAt_15() const { return ___sampleAt_15; }
	inline FsmFloat_t2883254149 ** get_address_of_sampleAt_15() { return &___sampleAt_15; }
	inline void set_sampleAt_15(FsmFloat_t2883254149 * value)
	{
		___sampleAt_15 = value;
		Il2CppCodeGenWriteBarrier((&___sampleAt_15), value);
	}

	inline static int32_t get_offset_of_storeValue_16() { return static_cast<int32_t>(offsetof(SampleCurve_t4047358761, ___storeValue_16)); }
	inline FsmFloat_t2883254149 * get_storeValue_16() const { return ___storeValue_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeValue_16() { return &___storeValue_16; }
	inline void set_storeValue_16(FsmFloat_t2883254149 * value)
	{
		___storeValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SampleCurve_t4047358761, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLECURVE_T4047358761_H
#ifndef SCREENPICK2D_T206632054_H
#define SCREENPICK2D_T206632054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenPick2d
struct  ScreenPick2d_t206632054  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick2d::screenVector
	FsmVector3_t626444517 * ___screenVector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick2d::screenX
	FsmFloat_t2883254149 * ___screenX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick2d::screenY
	FsmFloat_t2883254149 * ___screenY_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick2d::normalized
	FsmBool_t163807967 * ___normalized_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick2d::storeDidPickObject
	FsmBool_t163807967 * ___storeDidPickObject_18;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ScreenPick2d::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick2d::storePoint
	FsmVector3_t626444517 * ___storePoint_20;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.ScreenPick2d::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick2d::invertMask
	FsmBool_t163807967 * ___invertMask_22;
	// System.Boolean HutongGames.PlayMaker.Actions.ScreenPick2d::everyFrame
	bool ___everyFrame_23;

public:
	inline static int32_t get_offset_of_screenVector_14() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___screenVector_14)); }
	inline FsmVector3_t626444517 * get_screenVector_14() const { return ___screenVector_14; }
	inline FsmVector3_t626444517 ** get_address_of_screenVector_14() { return &___screenVector_14; }
	inline void set_screenVector_14(FsmVector3_t626444517 * value)
	{
		___screenVector_14 = value;
		Il2CppCodeGenWriteBarrier((&___screenVector_14), value);
	}

	inline static int32_t get_offset_of_screenX_15() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___screenX_15)); }
	inline FsmFloat_t2883254149 * get_screenX_15() const { return ___screenX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_screenX_15() { return &___screenX_15; }
	inline void set_screenX_15(FsmFloat_t2883254149 * value)
	{
		___screenX_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenX_15), value);
	}

	inline static int32_t get_offset_of_screenY_16() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___screenY_16)); }
	inline FsmFloat_t2883254149 * get_screenY_16() const { return ___screenY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_screenY_16() { return &___screenY_16; }
	inline void set_screenY_16(FsmFloat_t2883254149 * value)
	{
		___screenY_16 = value;
		Il2CppCodeGenWriteBarrier((&___screenY_16), value);
	}

	inline static int32_t get_offset_of_normalized_17() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___normalized_17)); }
	inline FsmBool_t163807967 * get_normalized_17() const { return ___normalized_17; }
	inline FsmBool_t163807967 ** get_address_of_normalized_17() { return &___normalized_17; }
	inline void set_normalized_17(FsmBool_t163807967 * value)
	{
		___normalized_17 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_17), value);
	}

	inline static int32_t get_offset_of_storeDidPickObject_18() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___storeDidPickObject_18)); }
	inline FsmBool_t163807967 * get_storeDidPickObject_18() const { return ___storeDidPickObject_18; }
	inline FsmBool_t163807967 ** get_address_of_storeDidPickObject_18() { return &___storeDidPickObject_18; }
	inline void set_storeDidPickObject_18(FsmBool_t163807967 * value)
	{
		___storeDidPickObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidPickObject_18), value);
	}

	inline static int32_t get_offset_of_storeGameObject_19() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___storeGameObject_19)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_19() const { return ___storeGameObject_19; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_19() { return &___storeGameObject_19; }
	inline void set_storeGameObject_19(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_19), value);
	}

	inline static int32_t get_offset_of_storePoint_20() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___storePoint_20)); }
	inline FsmVector3_t626444517 * get_storePoint_20() const { return ___storePoint_20; }
	inline FsmVector3_t626444517 ** get_address_of_storePoint_20() { return &___storePoint_20; }
	inline void set_storePoint_20(FsmVector3_t626444517 * value)
	{
		___storePoint_20 = value;
		Il2CppCodeGenWriteBarrier((&___storePoint_20), value);
	}

	inline static int32_t get_offset_of_layerMask_21() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___layerMask_21)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_21() const { return ___layerMask_21; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_21() { return &___layerMask_21; }
	inline void set_layerMask_21(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_21), value);
	}

	inline static int32_t get_offset_of_invertMask_22() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___invertMask_22)); }
	inline FsmBool_t163807967 * get_invertMask_22() const { return ___invertMask_22; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_22() { return &___invertMask_22; }
	inline void set_invertMask_22(FsmBool_t163807967 * value)
	{
		___invertMask_22 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(ScreenPick2d_t206632054, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENPICK2D_T206632054_H
#ifndef SETBOOLVALUE_T4097795596_H
#define SETBOOLVALUE_T4097795596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetBoolValue
struct  SetBoolValue_t4097795596  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetBoolValue::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetBoolValue::boolValue
	FsmBool_t163807967 * ___boolValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetBoolValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(SetBoolValue_t4097795596, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_boolValue_15() { return static_cast<int32_t>(offsetof(SetBoolValue_t4097795596, ___boolValue_15)); }
	inline FsmBool_t163807967 * get_boolValue_15() const { return ___boolValue_15; }
	inline FsmBool_t163807967 ** get_address_of_boolValue_15() { return &___boolValue_15; }
	inline void set_boolValue_15(FsmBool_t163807967 * value)
	{
		___boolValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___boolValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetBoolValue_t4097795596, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETBOOLVALUE_T4097795596_H
#ifndef SETCOLLIDER2DISTRIGGER_T819920189_H
#define SETCOLLIDER2DISTRIGGER_T819920189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger
struct  SetCollider2dIsTrigger_t819920189  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::isTrigger
	FsmBool_t163807967 * ___isTrigger_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::setAllColliders
	bool ___setAllColliders_16;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetCollider2dIsTrigger_t819920189, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_isTrigger_15() { return static_cast<int32_t>(offsetof(SetCollider2dIsTrigger_t819920189, ___isTrigger_15)); }
	inline FsmBool_t163807967 * get_isTrigger_15() const { return ___isTrigger_15; }
	inline FsmBool_t163807967 ** get_address_of_isTrigger_15() { return &___isTrigger_15; }
	inline void set_isTrigger_15(FsmBool_t163807967 * value)
	{
		___isTrigger_15 = value;
		Il2CppCodeGenWriteBarrier((&___isTrigger_15), value);
	}

	inline static int32_t get_offset_of_setAllColliders_16() { return static_cast<int32_t>(offsetof(SetCollider2dIsTrigger_t819920189, ___setAllColliders_16)); }
	inline bool get_setAllColliders_16() const { return ___setAllColliders_16; }
	inline bool* get_address_of_setAllColliders_16() { return &___setAllColliders_16; }
	inline void set_setAllColliders_16(bool value)
	{
		___setAllColliders_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETCOLLIDER2DISTRIGGER_T819920189_H
#ifndef SETFLOATVALUE_T3224737059_H
#define SETFLOATVALUE_T3224737059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFloatValue
struct  SetFloatValue_t3224737059  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFloatValue::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFloatValue::floatValue
	FsmFloat_t2883254149 * ___floatValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFloatValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(SetFloatValue_t3224737059, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_floatValue_15() { return static_cast<int32_t>(offsetof(SetFloatValue_t3224737059, ___floatValue_15)); }
	inline FsmFloat_t2883254149 * get_floatValue_15() const { return ___floatValue_15; }
	inline FsmFloat_t2883254149 ** get_address_of_floatValue_15() { return &___floatValue_15; }
	inline void set_floatValue_15(FsmFloat_t2883254149 * value)
	{
		___floatValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___floatValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetFloatValue_t3224737059, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFLOATVALUE_T3224737059_H
#ifndef SETGRAVITY_T85159497_H
#define SETGRAVITY_T85159497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGravity
struct  SetGravity_t85159497  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetGravity::vector
	FsmVector3_t626444517 * ___vector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity::x
	FsmFloat_t2883254149 * ___x_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity::y
	FsmFloat_t2883254149 * ___y_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity::z
	FsmFloat_t2883254149 * ___z_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGravity::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_vector_14() { return static_cast<int32_t>(offsetof(SetGravity_t85159497, ___vector_14)); }
	inline FsmVector3_t626444517 * get_vector_14() const { return ___vector_14; }
	inline FsmVector3_t626444517 ** get_address_of_vector_14() { return &___vector_14; }
	inline void set_vector_14(FsmVector3_t626444517 * value)
	{
		___vector_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector_14), value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(SetGravity_t85159497, ___x_15)); }
	inline FsmFloat_t2883254149 * get_x_15() const { return ___x_15; }
	inline FsmFloat_t2883254149 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmFloat_t2883254149 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier((&___x_15), value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(SetGravity_t85159497, ___y_16)); }
	inline FsmFloat_t2883254149 * get_y_16() const { return ___y_16; }
	inline FsmFloat_t2883254149 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmFloat_t2883254149 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier((&___y_16), value);
	}

	inline static int32_t get_offset_of_z_17() { return static_cast<int32_t>(offsetof(SetGravity_t85159497, ___z_17)); }
	inline FsmFloat_t2883254149 * get_z_17() const { return ___z_17; }
	inline FsmFloat_t2883254149 ** get_address_of_z_17() { return &___z_17; }
	inline void set_z_17(FsmFloat_t2883254149 * value)
	{
		___z_17 = value;
		Il2CppCodeGenWriteBarrier((&___z_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetGravity_t85159497, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGRAVITY_T85159497_H
#ifndef SETGRAVITY2D_T1484030169_H
#define SETGRAVITY2D_T1484030169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGravity2d
struct  SetGravity2d_t1484030169  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetGravity2d::vector
	FsmVector2_t2965096677 * ___vector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity2d::x
	FsmFloat_t2883254149 * ___x_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity2d::y
	FsmFloat_t2883254149 * ___y_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetGravity2d::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_vector_14() { return static_cast<int32_t>(offsetof(SetGravity2d_t1484030169, ___vector_14)); }
	inline FsmVector2_t2965096677 * get_vector_14() const { return ___vector_14; }
	inline FsmVector2_t2965096677 ** get_address_of_vector_14() { return &___vector_14; }
	inline void set_vector_14(FsmVector2_t2965096677 * value)
	{
		___vector_14 = value;
		Il2CppCodeGenWriteBarrier((&___vector_14), value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(SetGravity2d_t1484030169, ___x_15)); }
	inline FsmFloat_t2883254149 * get_x_15() const { return ___x_15; }
	inline FsmFloat_t2883254149 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmFloat_t2883254149 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier((&___x_15), value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(SetGravity2d_t1484030169, ___y_16)); }
	inline FsmFloat_t2883254149 * get_y_16() const { return ___y_16; }
	inline FsmFloat_t2883254149 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmFloat_t2883254149 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier((&___y_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetGravity2d_t1484030169, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGRAVITY2D_T1484030169_H
#ifndef SETHINGEJOINT2DPROPERTIES_T1483576128_H
#define SETHINGEJOINT2DPROPERTIES_T1483576128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties
struct  SetHingeJoint2dProperties_t1483576128  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::useLimits
	FsmBool_t163807967 * ___useLimits_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::min
	FsmFloat_t2883254149 * ___min_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::max
	FsmFloat_t2883254149 * ___max_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::useMotor
	FsmBool_t163807967 * ___useMotor_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::motorSpeed
	FsmFloat_t2883254149 * ___motorSpeed_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::maxMotorTorque
	FsmFloat_t2883254149 * ___maxMotorTorque_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.HingeJoint2D HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::_joint
	HingeJoint2D_t3442948838 * ____joint_22;
	// UnityEngine.JointMotor2D HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::_motor
	JointMotor2D_t142461323  ____motor_23;
	// UnityEngine.JointAngleLimits2D HutongGames.PlayMaker.Actions.SetHingeJoint2dProperties::_limits
	JointAngleLimits2D_t972279075  ____limits_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_useLimits_15() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___useLimits_15)); }
	inline FsmBool_t163807967 * get_useLimits_15() const { return ___useLimits_15; }
	inline FsmBool_t163807967 ** get_address_of_useLimits_15() { return &___useLimits_15; }
	inline void set_useLimits_15(FsmBool_t163807967 * value)
	{
		___useLimits_15 = value;
		Il2CppCodeGenWriteBarrier((&___useLimits_15), value);
	}

	inline static int32_t get_offset_of_min_16() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___min_16)); }
	inline FsmFloat_t2883254149 * get_min_16() const { return ___min_16; }
	inline FsmFloat_t2883254149 ** get_address_of_min_16() { return &___min_16; }
	inline void set_min_16(FsmFloat_t2883254149 * value)
	{
		___min_16 = value;
		Il2CppCodeGenWriteBarrier((&___min_16), value);
	}

	inline static int32_t get_offset_of_max_17() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___max_17)); }
	inline FsmFloat_t2883254149 * get_max_17() const { return ___max_17; }
	inline FsmFloat_t2883254149 ** get_address_of_max_17() { return &___max_17; }
	inline void set_max_17(FsmFloat_t2883254149 * value)
	{
		___max_17 = value;
		Il2CppCodeGenWriteBarrier((&___max_17), value);
	}

	inline static int32_t get_offset_of_useMotor_18() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___useMotor_18)); }
	inline FsmBool_t163807967 * get_useMotor_18() const { return ___useMotor_18; }
	inline FsmBool_t163807967 ** get_address_of_useMotor_18() { return &___useMotor_18; }
	inline void set_useMotor_18(FsmBool_t163807967 * value)
	{
		___useMotor_18 = value;
		Il2CppCodeGenWriteBarrier((&___useMotor_18), value);
	}

	inline static int32_t get_offset_of_motorSpeed_19() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___motorSpeed_19)); }
	inline FsmFloat_t2883254149 * get_motorSpeed_19() const { return ___motorSpeed_19; }
	inline FsmFloat_t2883254149 ** get_address_of_motorSpeed_19() { return &___motorSpeed_19; }
	inline void set_motorSpeed_19(FsmFloat_t2883254149 * value)
	{
		___motorSpeed_19 = value;
		Il2CppCodeGenWriteBarrier((&___motorSpeed_19), value);
	}

	inline static int32_t get_offset_of_maxMotorTorque_20() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___maxMotorTorque_20)); }
	inline FsmFloat_t2883254149 * get_maxMotorTorque_20() const { return ___maxMotorTorque_20; }
	inline FsmFloat_t2883254149 ** get_address_of_maxMotorTorque_20() { return &___maxMotorTorque_20; }
	inline void set_maxMotorTorque_20(FsmFloat_t2883254149 * value)
	{
		___maxMotorTorque_20 = value;
		Il2CppCodeGenWriteBarrier((&___maxMotorTorque_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of__joint_22() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ____joint_22)); }
	inline HingeJoint2D_t3442948838 * get__joint_22() const { return ____joint_22; }
	inline HingeJoint2D_t3442948838 ** get_address_of__joint_22() { return &____joint_22; }
	inline void set__joint_22(HingeJoint2D_t3442948838 * value)
	{
		____joint_22 = value;
		Il2CppCodeGenWriteBarrier((&____joint_22), value);
	}

	inline static int32_t get_offset_of__motor_23() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ____motor_23)); }
	inline JointMotor2D_t142461323  get__motor_23() const { return ____motor_23; }
	inline JointMotor2D_t142461323 * get_address_of__motor_23() { return &____motor_23; }
	inline void set__motor_23(JointMotor2D_t142461323  value)
	{
		____motor_23 = value;
	}

	inline static int32_t get_offset_of__limits_24() { return static_cast<int32_t>(offsetof(SetHingeJoint2dProperties_t1483576128, ____limits_24)); }
	inline JointAngleLimits2D_t972279075  get__limits_24() const { return ____limits_24; }
	inline JointAngleLimits2D_t972279075 * get_address_of__limits_24() { return &____limits_24; }
	inline void set__limits_24(JointAngleLimits2D_t972279075  value)
	{
		____limits_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETHINGEJOINT2DPROPERTIES_T1483576128_H
#ifndef SETINTFROMFLOAT_T7289849_H
#define SETINTFROMFLOAT_T7289849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetIntFromFloat
struct  SetIntFromFloat_t7289849  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetIntFromFloat::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetIntFromFloat::floatValue
	FsmFloat_t2883254149 * ___floatValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetIntFromFloat::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(SetIntFromFloat_t7289849, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_floatValue_15() { return static_cast<int32_t>(offsetof(SetIntFromFloat_t7289849, ___floatValue_15)); }
	inline FsmFloat_t2883254149 * get_floatValue_15() const { return ___floatValue_15; }
	inline FsmFloat_t2883254149 ** get_address_of_floatValue_15() { return &___floatValue_15; }
	inline void set_floatValue_15(FsmFloat_t2883254149 * value)
	{
		___floatValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___floatValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetIntFromFloat_t7289849, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETINTFROMFLOAT_T7289849_H
#ifndef SETINTVALUE_T120909476_H
#define SETINTVALUE_T120909476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetIntValue
struct  SetIntValue_t120909476  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetIntValue::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetIntValue::intValue
	FsmInt_t874273141 * ___intValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetIntValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(SetIntValue_t120909476, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_intValue_15() { return static_cast<int32_t>(offsetof(SetIntValue_t120909476, ___intValue_15)); }
	inline FsmInt_t874273141 * get_intValue_15() const { return ___intValue_15; }
	inline FsmInt_t874273141 ** get_address_of_intValue_15() { return &___intValue_15; }
	inline void set_intValue_15(FsmInt_t874273141 * value)
	{
		___intValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___intValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetIntValue_t120909476, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETINTVALUE_T120909476_H
#ifndef SETJOINTCONNECTEDBODY_T3705285269_H
#define SETJOINTCONNECTEDBODY_T3705285269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetJointConnectedBody
struct  SetJointConnectedBody_t3705285269  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetJointConnectedBody::joint
	FsmOwnerDefault_t3590610434 * ___joint_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetJointConnectedBody::rigidBody
	FsmGameObject_t3581898942 * ___rigidBody_15;

public:
	inline static int32_t get_offset_of_joint_14() { return static_cast<int32_t>(offsetof(SetJointConnectedBody_t3705285269, ___joint_14)); }
	inline FsmOwnerDefault_t3590610434 * get_joint_14() const { return ___joint_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_joint_14() { return &___joint_14; }
	inline void set_joint_14(FsmOwnerDefault_t3590610434 * value)
	{
		___joint_14 = value;
		Il2CppCodeGenWriteBarrier((&___joint_14), value);
	}

	inline static int32_t get_offset_of_rigidBody_15() { return static_cast<int32_t>(offsetof(SetJointConnectedBody_t3705285269, ___rigidBody_15)); }
	inline FsmGameObject_t3581898942 * get_rigidBody_15() const { return ___rigidBody_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_rigidBody_15() { return &___rigidBody_15; }
	inline void set_rigidBody_15(FsmGameObject_t3581898942 * value)
	{
		___rigidBody_15 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETJOINTCONNECTEDBODY_T3705285269_H
#ifndef SETWHEELJOINT2DPROPERTIES_T2618454698_H
#define SETWHEELJOINT2DPROPERTIES_T2618454698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties
struct  SetWheelJoint2dProperties_t2618454698  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::useMotor
	FsmBool_t163807967 * ___useMotor_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::motorSpeed
	FsmFloat_t2883254149 * ___motorSpeed_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::maxMotorTorque
	FsmFloat_t2883254149 * ___maxMotorTorque_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::angle
	FsmFloat_t2883254149 * ___angle_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::dampingRatio
	FsmFloat_t2883254149 * ___dampingRatio_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::frequency
	FsmFloat_t2883254149 * ___frequency_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.WheelJoint2D HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::_wj2d
	WheelJoint2D_t1756757149 * ____wj2d_22;
	// UnityEngine.JointMotor2D HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::_motor
	JointMotor2D_t142461323  ____motor_23;
	// UnityEngine.JointSuspension2D HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::_suspension
	JointSuspension2D_t3350541332  ____suspension_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_useMotor_15() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___useMotor_15)); }
	inline FsmBool_t163807967 * get_useMotor_15() const { return ___useMotor_15; }
	inline FsmBool_t163807967 ** get_address_of_useMotor_15() { return &___useMotor_15; }
	inline void set_useMotor_15(FsmBool_t163807967 * value)
	{
		___useMotor_15 = value;
		Il2CppCodeGenWriteBarrier((&___useMotor_15), value);
	}

	inline static int32_t get_offset_of_motorSpeed_16() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___motorSpeed_16)); }
	inline FsmFloat_t2883254149 * get_motorSpeed_16() const { return ___motorSpeed_16; }
	inline FsmFloat_t2883254149 ** get_address_of_motorSpeed_16() { return &___motorSpeed_16; }
	inline void set_motorSpeed_16(FsmFloat_t2883254149 * value)
	{
		___motorSpeed_16 = value;
		Il2CppCodeGenWriteBarrier((&___motorSpeed_16), value);
	}

	inline static int32_t get_offset_of_maxMotorTorque_17() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___maxMotorTorque_17)); }
	inline FsmFloat_t2883254149 * get_maxMotorTorque_17() const { return ___maxMotorTorque_17; }
	inline FsmFloat_t2883254149 ** get_address_of_maxMotorTorque_17() { return &___maxMotorTorque_17; }
	inline void set_maxMotorTorque_17(FsmFloat_t2883254149 * value)
	{
		___maxMotorTorque_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxMotorTorque_17), value);
	}

	inline static int32_t get_offset_of_angle_18() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___angle_18)); }
	inline FsmFloat_t2883254149 * get_angle_18() const { return ___angle_18; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_18() { return &___angle_18; }
	inline void set_angle_18(FsmFloat_t2883254149 * value)
	{
		___angle_18 = value;
		Il2CppCodeGenWriteBarrier((&___angle_18), value);
	}

	inline static int32_t get_offset_of_dampingRatio_19() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___dampingRatio_19)); }
	inline FsmFloat_t2883254149 * get_dampingRatio_19() const { return ___dampingRatio_19; }
	inline FsmFloat_t2883254149 ** get_address_of_dampingRatio_19() { return &___dampingRatio_19; }
	inline void set_dampingRatio_19(FsmFloat_t2883254149 * value)
	{
		___dampingRatio_19 = value;
		Il2CppCodeGenWriteBarrier((&___dampingRatio_19), value);
	}

	inline static int32_t get_offset_of_frequency_20() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___frequency_20)); }
	inline FsmFloat_t2883254149 * get_frequency_20() const { return ___frequency_20; }
	inline FsmFloat_t2883254149 ** get_address_of_frequency_20() { return &___frequency_20; }
	inline void set_frequency_20(FsmFloat_t2883254149 * value)
	{
		___frequency_20 = value;
		Il2CppCodeGenWriteBarrier((&___frequency_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of__wj2d_22() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ____wj2d_22)); }
	inline WheelJoint2D_t1756757149 * get__wj2d_22() const { return ____wj2d_22; }
	inline WheelJoint2D_t1756757149 ** get_address_of__wj2d_22() { return &____wj2d_22; }
	inline void set__wj2d_22(WheelJoint2D_t1756757149 * value)
	{
		____wj2d_22 = value;
		Il2CppCodeGenWriteBarrier((&____wj2d_22), value);
	}

	inline static int32_t get_offset_of__motor_23() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ____motor_23)); }
	inline JointMotor2D_t142461323  get__motor_23() const { return ____motor_23; }
	inline JointMotor2D_t142461323 * get_address_of__motor_23() { return &____motor_23; }
	inline void set__motor_23(JointMotor2D_t142461323  value)
	{
		____motor_23 = value;
	}

	inline static int32_t get_offset_of__suspension_24() { return static_cast<int32_t>(offsetof(SetWheelJoint2dProperties_t2618454698, ____suspension_24)); }
	inline JointSuspension2D_t3350541332  get__suspension_24() const { return ____suspension_24; }
	inline JointSuspension2D_t3350541332 * get_address_of__suspension_24() { return &____suspension_24; }
	inline void set__suspension_24(JointSuspension2D_t3350541332  value)
	{
		____suspension_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETWHEELJOINT2DPROPERTIES_T2618454698_H
#ifndef SMOOTHLOOKAT2D_T3829571255_H
#define SMOOTHLOOKAT2D_T3829571255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SmoothLookAt2d
struct  SmoothLookAt2d_t3829571255  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SmoothLookAt2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SmoothLookAt2d::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SmoothLookAt2d::targetPosition2d
	FsmVector2_t2965096677 * ___targetPosition2d_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SmoothLookAt2d::targetPosition
	FsmVector3_t626444517 * ___targetPosition_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAt2d::rotationOffset
	FsmFloat_t2883254149 * ___rotationOffset_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAt2d::speed
	FsmFloat_t2883254149 * ___speed_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SmoothLookAt2d::debug
	FsmBool_t163807967 * ___debug_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SmoothLookAt2d::finishTolerance
	FsmFloat_t2883254149 * ___finishTolerance_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SmoothLookAt2d::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_22;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SmoothLookAt2d::previousGo
	GameObject_t1113636619 * ___previousGo_23;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAt2d::lastRotation
	Quaternion_t2301928331  ___lastRotation_24;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.SmoothLookAt2d::desiredRotation
	Quaternion_t2301928331  ___desiredRotation_25;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_targetPosition2d_16() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___targetPosition2d_16)); }
	inline FsmVector2_t2965096677 * get_targetPosition2d_16() const { return ___targetPosition2d_16; }
	inline FsmVector2_t2965096677 ** get_address_of_targetPosition2d_16() { return &___targetPosition2d_16; }
	inline void set_targetPosition2d_16(FsmVector2_t2965096677 * value)
	{
		___targetPosition2d_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition2d_16), value);
	}

	inline static int32_t get_offset_of_targetPosition_17() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___targetPosition_17)); }
	inline FsmVector3_t626444517 * get_targetPosition_17() const { return ___targetPosition_17; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_17() { return &___targetPosition_17; }
	inline void set_targetPosition_17(FsmVector3_t626444517 * value)
	{
		___targetPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_17), value);
	}

	inline static int32_t get_offset_of_rotationOffset_18() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___rotationOffset_18)); }
	inline FsmFloat_t2883254149 * get_rotationOffset_18() const { return ___rotationOffset_18; }
	inline FsmFloat_t2883254149 ** get_address_of_rotationOffset_18() { return &___rotationOffset_18; }
	inline void set_rotationOffset_18(FsmFloat_t2883254149 * value)
	{
		___rotationOffset_18 = value;
		Il2CppCodeGenWriteBarrier((&___rotationOffset_18), value);
	}

	inline static int32_t get_offset_of_speed_19() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___speed_19)); }
	inline FsmFloat_t2883254149 * get_speed_19() const { return ___speed_19; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_19() { return &___speed_19; }
	inline void set_speed_19(FsmFloat_t2883254149 * value)
	{
		___speed_19 = value;
		Il2CppCodeGenWriteBarrier((&___speed_19), value);
	}

	inline static int32_t get_offset_of_debug_20() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___debug_20)); }
	inline FsmBool_t163807967 * get_debug_20() const { return ___debug_20; }
	inline FsmBool_t163807967 ** get_address_of_debug_20() { return &___debug_20; }
	inline void set_debug_20(FsmBool_t163807967 * value)
	{
		___debug_20 = value;
		Il2CppCodeGenWriteBarrier((&___debug_20), value);
	}

	inline static int32_t get_offset_of_finishTolerance_21() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___finishTolerance_21)); }
	inline FsmFloat_t2883254149 * get_finishTolerance_21() const { return ___finishTolerance_21; }
	inline FsmFloat_t2883254149 ** get_address_of_finishTolerance_21() { return &___finishTolerance_21; }
	inline void set_finishTolerance_21(FsmFloat_t2883254149 * value)
	{
		___finishTolerance_21 = value;
		Il2CppCodeGenWriteBarrier((&___finishTolerance_21), value);
	}

	inline static int32_t get_offset_of_finishEvent_22() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___finishEvent_22)); }
	inline FsmEvent_t3736299882 * get_finishEvent_22() const { return ___finishEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_22() { return &___finishEvent_22; }
	inline void set_finishEvent_22(FsmEvent_t3736299882 * value)
	{
		___finishEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_22), value);
	}

	inline static int32_t get_offset_of_previousGo_23() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___previousGo_23)); }
	inline GameObject_t1113636619 * get_previousGo_23() const { return ___previousGo_23; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_23() { return &___previousGo_23; }
	inline void set_previousGo_23(GameObject_t1113636619 * value)
	{
		___previousGo_23 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_23), value);
	}

	inline static int32_t get_offset_of_lastRotation_24() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___lastRotation_24)); }
	inline Quaternion_t2301928331  get_lastRotation_24() const { return ___lastRotation_24; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_24() { return &___lastRotation_24; }
	inline void set_lastRotation_24(Quaternion_t2301928331  value)
	{
		___lastRotation_24 = value;
	}

	inline static int32_t get_offset_of_desiredRotation_25() { return static_cast<int32_t>(offsetof(SmoothLookAt2d_t3829571255, ___desiredRotation_25)); }
	inline Quaternion_t2301928331  get_desiredRotation_25() const { return ___desiredRotation_25; }
	inline Quaternion_t2301928331 * get_address_of_desiredRotation_25() { return &___desiredRotation_25; }
	inline void set_desiredRotation_25(Quaternion_t2301928331  value)
	{
		___desiredRotation_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHLOOKAT2D_T3829571255_H
#ifndef TOUCHOBJECT2DEVENT_T1107737691_H
#define TOUCHOBJECT2DEVENT_T1107737691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TouchObject2dEvent
struct  TouchObject2dEvent_t1107737691  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TouchObject2dEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchObject2dEvent::fingerId
	FsmInt_t874273141 * ___fingerId_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObject2dEvent::touchBegan
	FsmEvent_t3736299882 * ___touchBegan_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObject2dEvent::touchMoved
	FsmEvent_t3736299882 * ___touchMoved_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObject2dEvent::touchStationary
	FsmEvent_t3736299882 * ___touchStationary_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObject2dEvent::touchEnded
	FsmEvent_t3736299882 * ___touchEnded_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TouchObject2dEvent::touchCanceled
	FsmEvent_t3736299882 * ___touchCanceled_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.TouchObject2dEvent::storeFingerId
	FsmInt_t874273141 * ___storeFingerId_21;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.TouchObject2dEvent::storeHitPoint
	FsmVector2_t2965096677 * ___storeHitPoint_22;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fingerId_15() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___fingerId_15)); }
	inline FsmInt_t874273141 * get_fingerId_15() const { return ___fingerId_15; }
	inline FsmInt_t874273141 ** get_address_of_fingerId_15() { return &___fingerId_15; }
	inline void set_fingerId_15(FsmInt_t874273141 * value)
	{
		___fingerId_15 = value;
		Il2CppCodeGenWriteBarrier((&___fingerId_15), value);
	}

	inline static int32_t get_offset_of_touchBegan_16() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___touchBegan_16)); }
	inline FsmEvent_t3736299882 * get_touchBegan_16() const { return ___touchBegan_16; }
	inline FsmEvent_t3736299882 ** get_address_of_touchBegan_16() { return &___touchBegan_16; }
	inline void set_touchBegan_16(FsmEvent_t3736299882 * value)
	{
		___touchBegan_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchBegan_16), value);
	}

	inline static int32_t get_offset_of_touchMoved_17() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___touchMoved_17)); }
	inline FsmEvent_t3736299882 * get_touchMoved_17() const { return ___touchMoved_17; }
	inline FsmEvent_t3736299882 ** get_address_of_touchMoved_17() { return &___touchMoved_17; }
	inline void set_touchMoved_17(FsmEvent_t3736299882 * value)
	{
		___touchMoved_17 = value;
		Il2CppCodeGenWriteBarrier((&___touchMoved_17), value);
	}

	inline static int32_t get_offset_of_touchStationary_18() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___touchStationary_18)); }
	inline FsmEvent_t3736299882 * get_touchStationary_18() const { return ___touchStationary_18; }
	inline FsmEvent_t3736299882 ** get_address_of_touchStationary_18() { return &___touchStationary_18; }
	inline void set_touchStationary_18(FsmEvent_t3736299882 * value)
	{
		___touchStationary_18 = value;
		Il2CppCodeGenWriteBarrier((&___touchStationary_18), value);
	}

	inline static int32_t get_offset_of_touchEnded_19() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___touchEnded_19)); }
	inline FsmEvent_t3736299882 * get_touchEnded_19() const { return ___touchEnded_19; }
	inline FsmEvent_t3736299882 ** get_address_of_touchEnded_19() { return &___touchEnded_19; }
	inline void set_touchEnded_19(FsmEvent_t3736299882 * value)
	{
		___touchEnded_19 = value;
		Il2CppCodeGenWriteBarrier((&___touchEnded_19), value);
	}

	inline static int32_t get_offset_of_touchCanceled_20() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___touchCanceled_20)); }
	inline FsmEvent_t3736299882 * get_touchCanceled_20() const { return ___touchCanceled_20; }
	inline FsmEvent_t3736299882 ** get_address_of_touchCanceled_20() { return &___touchCanceled_20; }
	inline void set_touchCanceled_20(FsmEvent_t3736299882 * value)
	{
		___touchCanceled_20 = value;
		Il2CppCodeGenWriteBarrier((&___touchCanceled_20), value);
	}

	inline static int32_t get_offset_of_storeFingerId_21() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___storeFingerId_21)); }
	inline FsmInt_t874273141 * get_storeFingerId_21() const { return ___storeFingerId_21; }
	inline FsmInt_t874273141 ** get_address_of_storeFingerId_21() { return &___storeFingerId_21; }
	inline void set_storeFingerId_21(FsmInt_t874273141 * value)
	{
		___storeFingerId_21 = value;
		Il2CppCodeGenWriteBarrier((&___storeFingerId_21), value);
	}

	inline static int32_t get_offset_of_storeHitPoint_22() { return static_cast<int32_t>(offsetof(TouchObject2dEvent_t1107737691, ___storeHitPoint_22)); }
	inline FsmVector2_t2965096677 * get_storeHitPoint_22() const { return ___storeHitPoint_22; }
	inline FsmVector2_t2965096677 ** get_address_of_storeHitPoint_22() { return &___storeHitPoint_22; }
	inline void set_storeHitPoint_22(FsmVector2_t2965096677 * value)
	{
		___storeHitPoint_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeHitPoint_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHOBJECT2DEVENT_T1107737691_H
#ifndef TRIGGER2DEVENT_T1735501411_H
#define TRIGGER2DEVENT_T1735501411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Trigger2dEvent
struct  Trigger2dEvent_t1735501411  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Trigger2dEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.Trigger2DType HutongGames.PlayMaker.Actions.Trigger2dEvent::trigger
	int32_t ___trigger_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.Trigger2dEvent::collideTag
	FsmString_t1785915204 * ___collideTag_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Trigger2dEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.Trigger2dEvent::storeCollider
	FsmGameObject_t3581898942 * ___storeCollider_18;
	// PlayMakerProxyBase HutongGames.PlayMaker.Actions.Trigger2dEvent::cachedProxy
	PlayMakerProxyBase_t90512809 * ___cachedProxy_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(Trigger2dEvent_t1735501411, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_trigger_15() { return static_cast<int32_t>(offsetof(Trigger2dEvent_t1735501411, ___trigger_15)); }
	inline int32_t get_trigger_15() const { return ___trigger_15; }
	inline int32_t* get_address_of_trigger_15() { return &___trigger_15; }
	inline void set_trigger_15(int32_t value)
	{
		___trigger_15 = value;
	}

	inline static int32_t get_offset_of_collideTag_16() { return static_cast<int32_t>(offsetof(Trigger2dEvent_t1735501411, ___collideTag_16)); }
	inline FsmString_t1785915204 * get_collideTag_16() const { return ___collideTag_16; }
	inline FsmString_t1785915204 ** get_address_of_collideTag_16() { return &___collideTag_16; }
	inline void set_collideTag_16(FsmString_t1785915204 * value)
	{
		___collideTag_16 = value;
		Il2CppCodeGenWriteBarrier((&___collideTag_16), value);
	}

	inline static int32_t get_offset_of_sendEvent_17() { return static_cast<int32_t>(offsetof(Trigger2dEvent_t1735501411, ___sendEvent_17)); }
	inline FsmEvent_t3736299882 * get_sendEvent_17() const { return ___sendEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_17() { return &___sendEvent_17; }
	inline void set_sendEvent_17(FsmEvent_t3736299882 * value)
	{
		___sendEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_17), value);
	}

	inline static int32_t get_offset_of_storeCollider_18() { return static_cast<int32_t>(offsetof(Trigger2dEvent_t1735501411, ___storeCollider_18)); }
	inline FsmGameObject_t3581898942 * get_storeCollider_18() const { return ___storeCollider_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeCollider_18() { return &___storeCollider_18; }
	inline void set_storeCollider_18(FsmGameObject_t3581898942 * value)
	{
		___storeCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeCollider_18), value);
	}

	inline static int32_t get_offset_of_cachedProxy_19() { return static_cast<int32_t>(offsetof(Trigger2dEvent_t1735501411, ___cachedProxy_19)); }
	inline PlayMakerProxyBase_t90512809 * get_cachedProxy_19() const { return ___cachedProxy_19; }
	inline PlayMakerProxyBase_t90512809 ** get_address_of_cachedProxy_19() { return &___cachedProxy_19; }
	inline void set_cachedProxy_19(PlayMakerProxyBase_t90512809 * value)
	{
		___cachedProxy_19 = value;
		Il2CppCodeGenWriteBarrier((&___cachedProxy_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER2DEVENT_T1735501411_H
#ifndef TRIGGEREVENT_T2948667628_H
#define TRIGGEREVENT_T2948667628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TriggerEvent
struct  TriggerEvent_t2948667628  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.TriggerEvent::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.TriggerType HutongGames.PlayMaker.Actions.TriggerEvent::trigger
	int32_t ___trigger_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.TriggerEvent::collideTag
	FsmString_t1785915204 * ___collideTag_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.TriggerEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.TriggerEvent::storeCollider
	FsmGameObject_t3581898942 * ___storeCollider_18;
	// PlayMakerProxyBase HutongGames.PlayMaker.Actions.TriggerEvent::cachedProxy
	PlayMakerProxyBase_t90512809 * ___cachedProxy_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(TriggerEvent_t2948667628, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_trigger_15() { return static_cast<int32_t>(offsetof(TriggerEvent_t2948667628, ___trigger_15)); }
	inline int32_t get_trigger_15() const { return ___trigger_15; }
	inline int32_t* get_address_of_trigger_15() { return &___trigger_15; }
	inline void set_trigger_15(int32_t value)
	{
		___trigger_15 = value;
	}

	inline static int32_t get_offset_of_collideTag_16() { return static_cast<int32_t>(offsetof(TriggerEvent_t2948667628, ___collideTag_16)); }
	inline FsmString_t1785915204 * get_collideTag_16() const { return ___collideTag_16; }
	inline FsmString_t1785915204 ** get_address_of_collideTag_16() { return &___collideTag_16; }
	inline void set_collideTag_16(FsmString_t1785915204 * value)
	{
		___collideTag_16 = value;
		Il2CppCodeGenWriteBarrier((&___collideTag_16), value);
	}

	inline static int32_t get_offset_of_sendEvent_17() { return static_cast<int32_t>(offsetof(TriggerEvent_t2948667628, ___sendEvent_17)); }
	inline FsmEvent_t3736299882 * get_sendEvent_17() const { return ___sendEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_17() { return &___sendEvent_17; }
	inline void set_sendEvent_17(FsmEvent_t3736299882 * value)
	{
		___sendEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_17), value);
	}

	inline static int32_t get_offset_of_storeCollider_18() { return static_cast<int32_t>(offsetof(TriggerEvent_t2948667628, ___storeCollider_18)); }
	inline FsmGameObject_t3581898942 * get_storeCollider_18() const { return ___storeCollider_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeCollider_18() { return &___storeCollider_18; }
	inline void set_storeCollider_18(FsmGameObject_t3581898942 * value)
	{
		___storeCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeCollider_18), value);
	}

	inline static int32_t get_offset_of_cachedProxy_19() { return static_cast<int32_t>(offsetof(TriggerEvent_t2948667628, ___cachedProxy_19)); }
	inline PlayMakerProxyBase_t90512809 * get_cachedProxy_19() const { return ___cachedProxy_19; }
	inline PlayMakerProxyBase_t90512809 ** get_address_of_cachedProxy_19() { return &___cachedProxy_19; }
	inline void set_cachedProxy_19(PlayMakerProxyBase_t90512809 * value)
	{
		___cachedProxy_19 = value;
		Il2CppCodeGenWriteBarrier((&___cachedProxy_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T2948667628_H
#ifndef WAKEALLRIGIDBODIES_T502384016_H
#define WAKEALLRIGIDBODIES_T502384016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies
struct  WakeAllRigidBodies_t502384016  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.WakeAllRigidBodies::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.Rigidbody[] HutongGames.PlayMaker.Actions.WakeAllRigidBodies::bodies
	RigidbodyU5BU5D_t3308997185* ___bodies_15;

public:
	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(WakeAllRigidBodies_t502384016, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_bodies_15() { return static_cast<int32_t>(offsetof(WakeAllRigidBodies_t502384016, ___bodies_15)); }
	inline RigidbodyU5BU5D_t3308997185* get_bodies_15() const { return ___bodies_15; }
	inline RigidbodyU5BU5D_t3308997185** get_address_of_bodies_15() { return &___bodies_15; }
	inline void set_bodies_15(RigidbodyU5BU5D_t3308997185* value)
	{
		___bodies_15 = value;
		Il2CppCodeGenWriteBarrier((&___bodies_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAKEALLRIGIDBODIES_T502384016_H
#ifndef WAKEALLRIGIDBODIES2D_T384709002_H
#define WAKEALLRIGIDBODIES2D_T384709002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d
struct  WakeAllRigidBodies2d_t384709002  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(WakeAllRigidBodies2d_t384709002, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAKEALLRIGIDBODIES2D_T384709002_H
#ifndef ADDEXPLOSIONFORCE_T3481544241_H
#define ADDEXPLOSIONFORCE_T3481544241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddExplosionForce
struct  AddExplosionForce_t3481544241  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddExplosionForce::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddExplosionForce::center
	FsmVector3_t626444517 * ___center_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddExplosionForce::force
	FsmFloat_t2883254149 * ___force_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddExplosionForce::radius
	FsmFloat_t2883254149 * ___radius_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddExplosionForce::upwardsModifier
	FsmFloat_t2883254149 * ___upwardsModifier_20;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.AddExplosionForce::forceMode
	int32_t ___forceMode_21;
	// System.Boolean HutongGames.PlayMaker.Actions.AddExplosionForce::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_center_17() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___center_17)); }
	inline FsmVector3_t626444517 * get_center_17() const { return ___center_17; }
	inline FsmVector3_t626444517 ** get_address_of_center_17() { return &___center_17; }
	inline void set_center_17(FsmVector3_t626444517 * value)
	{
		___center_17 = value;
		Il2CppCodeGenWriteBarrier((&___center_17), value);
	}

	inline static int32_t get_offset_of_force_18() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___force_18)); }
	inline FsmFloat_t2883254149 * get_force_18() const { return ___force_18; }
	inline FsmFloat_t2883254149 ** get_address_of_force_18() { return &___force_18; }
	inline void set_force_18(FsmFloat_t2883254149 * value)
	{
		___force_18 = value;
		Il2CppCodeGenWriteBarrier((&___force_18), value);
	}

	inline static int32_t get_offset_of_radius_19() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___radius_19)); }
	inline FsmFloat_t2883254149 * get_radius_19() const { return ___radius_19; }
	inline FsmFloat_t2883254149 ** get_address_of_radius_19() { return &___radius_19; }
	inline void set_radius_19(FsmFloat_t2883254149 * value)
	{
		___radius_19 = value;
		Il2CppCodeGenWriteBarrier((&___radius_19), value);
	}

	inline static int32_t get_offset_of_upwardsModifier_20() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___upwardsModifier_20)); }
	inline FsmFloat_t2883254149 * get_upwardsModifier_20() const { return ___upwardsModifier_20; }
	inline FsmFloat_t2883254149 ** get_address_of_upwardsModifier_20() { return &___upwardsModifier_20; }
	inline void set_upwardsModifier_20(FsmFloat_t2883254149 * value)
	{
		___upwardsModifier_20 = value;
		Il2CppCodeGenWriteBarrier((&___upwardsModifier_20), value);
	}

	inline static int32_t get_offset_of_forceMode_21() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___forceMode_21)); }
	inline int32_t get_forceMode_21() const { return ___forceMode_21; }
	inline int32_t* get_address_of_forceMode_21() { return &___forceMode_21; }
	inline void set_forceMode_21(int32_t value)
	{
		___forceMode_21 = value;
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(AddExplosionForce_t3481544241, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDEXPLOSIONFORCE_T3481544241_H
#ifndef ADDFORCE_T1469844092_H
#define ADDFORCE_T1469844092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddForce
struct  AddForce_t1469844092  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddForce::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddForce::atPosition
	FsmVector3_t626444517 * ___atPosition_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddForce::vector
	FsmVector3_t626444517 * ___vector_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce::x
	FsmFloat_t2883254149 * ___x_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce::y
	FsmFloat_t2883254149 * ___y_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce::z
	FsmFloat_t2883254149 * ___z_21;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.AddForce::space
	int32_t ___space_22;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.AddForce::forceMode
	int32_t ___forceMode_23;
	// System.Boolean HutongGames.PlayMaker.Actions.AddForce::everyFrame
	bool ___everyFrame_24;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_atPosition_17() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___atPosition_17)); }
	inline FsmVector3_t626444517 * get_atPosition_17() const { return ___atPosition_17; }
	inline FsmVector3_t626444517 ** get_address_of_atPosition_17() { return &___atPosition_17; }
	inline void set_atPosition_17(FsmVector3_t626444517 * value)
	{
		___atPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___atPosition_17), value);
	}

	inline static int32_t get_offset_of_vector_18() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___vector_18)); }
	inline FsmVector3_t626444517 * get_vector_18() const { return ___vector_18; }
	inline FsmVector3_t626444517 ** get_address_of_vector_18() { return &___vector_18; }
	inline void set_vector_18(FsmVector3_t626444517 * value)
	{
		___vector_18 = value;
		Il2CppCodeGenWriteBarrier((&___vector_18), value);
	}

	inline static int32_t get_offset_of_x_19() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___x_19)); }
	inline FsmFloat_t2883254149 * get_x_19() const { return ___x_19; }
	inline FsmFloat_t2883254149 ** get_address_of_x_19() { return &___x_19; }
	inline void set_x_19(FsmFloat_t2883254149 * value)
	{
		___x_19 = value;
		Il2CppCodeGenWriteBarrier((&___x_19), value);
	}

	inline static int32_t get_offset_of_y_20() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___y_20)); }
	inline FsmFloat_t2883254149 * get_y_20() const { return ___y_20; }
	inline FsmFloat_t2883254149 ** get_address_of_y_20() { return &___y_20; }
	inline void set_y_20(FsmFloat_t2883254149 * value)
	{
		___y_20 = value;
		Il2CppCodeGenWriteBarrier((&___y_20), value);
	}

	inline static int32_t get_offset_of_z_21() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___z_21)); }
	inline FsmFloat_t2883254149 * get_z_21() const { return ___z_21; }
	inline FsmFloat_t2883254149 ** get_address_of_z_21() { return &___z_21; }
	inline void set_z_21(FsmFloat_t2883254149 * value)
	{
		___z_21 = value;
		Il2CppCodeGenWriteBarrier((&___z_21), value);
	}

	inline static int32_t get_offset_of_space_22() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___space_22)); }
	inline int32_t get_space_22() const { return ___space_22; }
	inline int32_t* get_address_of_space_22() { return &___space_22; }
	inline void set_space_22(int32_t value)
	{
		___space_22 = value;
	}

	inline static int32_t get_offset_of_forceMode_23() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___forceMode_23)); }
	inline int32_t get_forceMode_23() const { return ___forceMode_23; }
	inline int32_t* get_address_of_forceMode_23() { return &___forceMode_23; }
	inline void set_forceMode_23(int32_t value)
	{
		___forceMode_23 = value;
	}

	inline static int32_t get_offset_of_everyFrame_24() { return static_cast<int32_t>(offsetof(AddForce_t1469844092, ___everyFrame_24)); }
	inline bool get_everyFrame_24() const { return ___everyFrame_24; }
	inline bool* get_address_of_everyFrame_24() { return &___everyFrame_24; }
	inline void set_everyFrame_24(bool value)
	{
		___everyFrame_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDFORCE_T1469844092_H
#ifndef ADDFORCE2D_T3511446623_H
#define ADDFORCE2D_T3511446623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddForce2d
struct  AddForce2d_t3511446623  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddForce2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.ForceMode2D HutongGames.PlayMaker.Actions.AddForce2d::forceMode
	int32_t ___forceMode_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.AddForce2d::atPosition
	FsmVector2_t2965096677 * ___atPosition_18;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.AddForce2d::vector
	FsmVector2_t2965096677 * ___vector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce2d::x
	FsmFloat_t2883254149 * ___x_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddForce2d::y
	FsmFloat_t2883254149 * ___y_21;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddForce2d::vector3
	FsmVector3_t626444517 * ___vector3_22;
	// System.Boolean HutongGames.PlayMaker.Actions.AddForce2d::everyFrame
	bool ___everyFrame_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_forceMode_17() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___forceMode_17)); }
	inline int32_t get_forceMode_17() const { return ___forceMode_17; }
	inline int32_t* get_address_of_forceMode_17() { return &___forceMode_17; }
	inline void set_forceMode_17(int32_t value)
	{
		___forceMode_17 = value;
	}

	inline static int32_t get_offset_of_atPosition_18() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___atPosition_18)); }
	inline FsmVector2_t2965096677 * get_atPosition_18() const { return ___atPosition_18; }
	inline FsmVector2_t2965096677 ** get_address_of_atPosition_18() { return &___atPosition_18; }
	inline void set_atPosition_18(FsmVector2_t2965096677 * value)
	{
		___atPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___atPosition_18), value);
	}

	inline static int32_t get_offset_of_vector_19() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___vector_19)); }
	inline FsmVector2_t2965096677 * get_vector_19() const { return ___vector_19; }
	inline FsmVector2_t2965096677 ** get_address_of_vector_19() { return &___vector_19; }
	inline void set_vector_19(FsmVector2_t2965096677 * value)
	{
		___vector_19 = value;
		Il2CppCodeGenWriteBarrier((&___vector_19), value);
	}

	inline static int32_t get_offset_of_x_20() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___x_20)); }
	inline FsmFloat_t2883254149 * get_x_20() const { return ___x_20; }
	inline FsmFloat_t2883254149 ** get_address_of_x_20() { return &___x_20; }
	inline void set_x_20(FsmFloat_t2883254149 * value)
	{
		___x_20 = value;
		Il2CppCodeGenWriteBarrier((&___x_20), value);
	}

	inline static int32_t get_offset_of_y_21() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___y_21)); }
	inline FsmFloat_t2883254149 * get_y_21() const { return ___y_21; }
	inline FsmFloat_t2883254149 ** get_address_of_y_21() { return &___y_21; }
	inline void set_y_21(FsmFloat_t2883254149 * value)
	{
		___y_21 = value;
		Il2CppCodeGenWriteBarrier((&___y_21), value);
	}

	inline static int32_t get_offset_of_vector3_22() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___vector3_22)); }
	inline FsmVector3_t626444517 * get_vector3_22() const { return ___vector3_22; }
	inline FsmVector3_t626444517 ** get_address_of_vector3_22() { return &___vector3_22; }
	inline void set_vector3_22(FsmVector3_t626444517 * value)
	{
		___vector3_22 = value;
		Il2CppCodeGenWriteBarrier((&___vector3_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(AddForce2d_t3511446623, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDFORCE2D_T3511446623_H
#ifndef ADDRELATIVEFORCE2D_T1292494680_H
#define ADDRELATIVEFORCE2D_T1292494680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddRelativeForce2d
struct  AddRelativeForce2d_t1292494680  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddRelativeForce2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.ForceMode2D HutongGames.PlayMaker.Actions.AddRelativeForce2d::forceMode
	int32_t ___forceMode_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.AddRelativeForce2d::vector
	FsmVector2_t2965096677 * ___vector_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddRelativeForce2d::x
	FsmFloat_t2883254149 * ___x_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddRelativeForce2d::y
	FsmFloat_t2883254149 * ___y_20;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddRelativeForce2d::vector3
	FsmVector3_t626444517 * ___vector3_21;
	// System.Boolean HutongGames.PlayMaker.Actions.AddRelativeForce2d::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_forceMode_17() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___forceMode_17)); }
	inline int32_t get_forceMode_17() const { return ___forceMode_17; }
	inline int32_t* get_address_of_forceMode_17() { return &___forceMode_17; }
	inline void set_forceMode_17(int32_t value)
	{
		___forceMode_17 = value;
	}

	inline static int32_t get_offset_of_vector_18() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___vector_18)); }
	inline FsmVector2_t2965096677 * get_vector_18() const { return ___vector_18; }
	inline FsmVector2_t2965096677 ** get_address_of_vector_18() { return &___vector_18; }
	inline void set_vector_18(FsmVector2_t2965096677 * value)
	{
		___vector_18 = value;
		Il2CppCodeGenWriteBarrier((&___vector_18), value);
	}

	inline static int32_t get_offset_of_x_19() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___x_19)); }
	inline FsmFloat_t2883254149 * get_x_19() const { return ___x_19; }
	inline FsmFloat_t2883254149 ** get_address_of_x_19() { return &___x_19; }
	inline void set_x_19(FsmFloat_t2883254149 * value)
	{
		___x_19 = value;
		Il2CppCodeGenWriteBarrier((&___x_19), value);
	}

	inline static int32_t get_offset_of_y_20() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___y_20)); }
	inline FsmFloat_t2883254149 * get_y_20() const { return ___y_20; }
	inline FsmFloat_t2883254149 ** get_address_of_y_20() { return &___y_20; }
	inline void set_y_20(FsmFloat_t2883254149 * value)
	{
		___y_20 = value;
		Il2CppCodeGenWriteBarrier((&___y_20), value);
	}

	inline static int32_t get_offset_of_vector3_21() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___vector3_21)); }
	inline FsmVector3_t626444517 * get_vector3_21() const { return ___vector3_21; }
	inline FsmVector3_t626444517 ** get_address_of_vector3_21() { return &___vector3_21; }
	inline void set_vector3_21(FsmVector3_t626444517 * value)
	{
		___vector3_21 = value;
		Il2CppCodeGenWriteBarrier((&___vector3_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(AddRelativeForce2d_t1292494680, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRELATIVEFORCE2D_T1292494680_H
#ifndef ADDTORQUE_T800833020_H
#define ADDTORQUE_T800833020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddTorque
struct  AddTorque_t800833020  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddTorque::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.AddTorque::vector
	FsmVector3_t626444517 * ___vector_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque::y
	FsmFloat_t2883254149 * ___y_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque::z
	FsmFloat_t2883254149 * ___z_20;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.AddTorque::space
	int32_t ___space_21;
	// UnityEngine.ForceMode HutongGames.PlayMaker.Actions.AddTorque::forceMode
	int32_t ___forceMode_22;
	// System.Boolean HutongGames.PlayMaker.Actions.AddTorque::everyFrame
	bool ___everyFrame_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_vector_17() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___vector_17)); }
	inline FsmVector3_t626444517 * get_vector_17() const { return ___vector_17; }
	inline FsmVector3_t626444517 ** get_address_of_vector_17() { return &___vector_17; }
	inline void set_vector_17(FsmVector3_t626444517 * value)
	{
		___vector_17 = value;
		Il2CppCodeGenWriteBarrier((&___vector_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_z_20() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___z_20)); }
	inline FsmFloat_t2883254149 * get_z_20() const { return ___z_20; }
	inline FsmFloat_t2883254149 ** get_address_of_z_20() { return &___z_20; }
	inline void set_z_20(FsmFloat_t2883254149 * value)
	{
		___z_20 = value;
		Il2CppCodeGenWriteBarrier((&___z_20), value);
	}

	inline static int32_t get_offset_of_space_21() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___space_21)); }
	inline int32_t get_space_21() const { return ___space_21; }
	inline int32_t* get_address_of_space_21() { return &___space_21; }
	inline void set_space_21(int32_t value)
	{
		___space_21 = value;
	}

	inline static int32_t get_offset_of_forceMode_22() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___forceMode_22)); }
	inline int32_t get_forceMode_22() const { return ___forceMode_22; }
	inline int32_t* get_address_of_forceMode_22() { return &___forceMode_22; }
	inline void set_forceMode_22(int32_t value)
	{
		___forceMode_22 = value;
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(AddTorque_t800833020, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTORQUE_T800833020_H
#ifndef ADDTORQUE2D_T3500577466_H
#define ADDTORQUE2D_T3500577466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddTorque2d
struct  AddTorque2d_t3500577466  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddTorque2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.ForceMode2D HutongGames.PlayMaker.Actions.AddTorque2d::forceMode
	int32_t ___forceMode_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AddTorque2d::torque
	FsmFloat_t2883254149 * ___torque_18;
	// System.Boolean HutongGames.PlayMaker.Actions.AddTorque2d::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(AddTorque2d_t3500577466, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_forceMode_17() { return static_cast<int32_t>(offsetof(AddTorque2d_t3500577466, ___forceMode_17)); }
	inline int32_t get_forceMode_17() const { return ___forceMode_17; }
	inline int32_t* get_address_of_forceMode_17() { return &___forceMode_17; }
	inline void set_forceMode_17(int32_t value)
	{
		___forceMode_17 = value;
	}

	inline static int32_t get_offset_of_torque_18() { return static_cast<int32_t>(offsetof(AddTorque2d_t3500577466, ___torque_18)); }
	inline FsmFloat_t2883254149 * get_torque_18() const { return ___torque_18; }
	inline FsmFloat_t2883254149 ** get_address_of_torque_18() { return &___torque_18; }
	inline void set_torque_18(FsmFloat_t2883254149 * value)
	{
		___torque_18 = value;
		Il2CppCodeGenWriteBarrier((&___torque_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(AddTorque2d_t3500577466, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTORQUE2D_T3500577466_H
#ifndef GETMASS_T3973594356_H
#define GETMASS_T3973594356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMass
struct  GetMass_t3973594356  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetMass::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetMass::storeResult
	FsmFloat_t2883254149 * ___storeResult_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetMass_t3973594356, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetMass_t3973594356, ___storeResult_17)); }
	inline FsmFloat_t2883254149 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmFloat_t2883254149 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMASS_T3973594356_H
#ifndef GETMASS2D_T2644716189_H
#define GETMASS2D_T2644716189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMass2d
struct  GetMass2d_t2644716189  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetMass2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetMass2d::storeResult
	FsmFloat_t2883254149 * ___storeResult_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetMass2d_t2644716189, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetMass2d_t2644716189, ___storeResult_17)); }
	inline FsmFloat_t2883254149 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmFloat_t2883254149 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMASS2D_T2644716189_H
#ifndef GETSPEED_T1149696821_H
#define GETSPEED_T1149696821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSpeed
struct  GetSpeed_t1149696821  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetSpeed::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetSpeed::storeResult
	FsmFloat_t2883254149 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSpeed::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetSpeed_t1149696821, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetSpeed_t1149696821, ___storeResult_17)); }
	inline FsmFloat_t2883254149 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmFloat_t2883254149 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetSpeed_t1149696821, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSPEED_T1149696821_H
#ifndef GETSPEED2D_T2660198156_H
#define GETSPEED2D_T2660198156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSpeed2d
struct  GetSpeed2d_t2660198156  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetSpeed2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetSpeed2d::storeResult
	FsmFloat_t2883254149 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSpeed2d::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetSpeed2d_t2660198156, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetSpeed2d_t2660198156, ___storeResult_17)); }
	inline FsmFloat_t2883254149 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmFloat_t2883254149 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetSpeed2d_t2660198156, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSPEED2D_T2660198156_H
#ifndef GETVELOCITY_T3685857823_H
#define GETVELOCITY_T3685857823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVelocity
struct  GetVelocity_t3685857823  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetVelocity::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetVelocity::vector
	FsmVector3_t626444517 * ___vector_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVelocity::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVelocity::y
	FsmFloat_t2883254149 * ___y_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVelocity::z
	FsmFloat_t2883254149 * ___z_20;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetVelocity::space
	int32_t ___space_21;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVelocity::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_vector_17() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___vector_17)); }
	inline FsmVector3_t626444517 * get_vector_17() const { return ___vector_17; }
	inline FsmVector3_t626444517 ** get_address_of_vector_17() { return &___vector_17; }
	inline void set_vector_17(FsmVector3_t626444517 * value)
	{
		___vector_17 = value;
		Il2CppCodeGenWriteBarrier((&___vector_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_z_20() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___z_20)); }
	inline FsmFloat_t2883254149 * get_z_20() const { return ___z_20; }
	inline FsmFloat_t2883254149 ** get_address_of_z_20() { return &___z_20; }
	inline void set_z_20(FsmFloat_t2883254149 * value)
	{
		___z_20 = value;
		Il2CppCodeGenWriteBarrier((&___z_20), value);
	}

	inline static int32_t get_offset_of_space_21() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___space_21)); }
	inline int32_t get_space_21() const { return ___space_21; }
	inline int32_t* get_address_of_space_21() { return &___space_21; }
	inline void set_space_21(int32_t value)
	{
		___space_21 = value;
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(GetVelocity_t3685857823, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVELOCITY_T3685857823_H
#ifndef GETVELOCITY2D_T1516202885_H
#define GETVELOCITY2D_T1516202885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetVelocity2d
struct  GetVelocity2d_t1516202885  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetVelocity2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetVelocity2d::vector
	FsmVector2_t2965096677 * ___vector_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVelocity2d::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetVelocity2d::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetVelocity2d::space
	int32_t ___space_20;
	// System.Boolean HutongGames.PlayMaker.Actions.GetVelocity2d::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetVelocity2d_t1516202885, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_vector_17() { return static_cast<int32_t>(offsetof(GetVelocity2d_t1516202885, ___vector_17)); }
	inline FsmVector2_t2965096677 * get_vector_17() const { return ___vector_17; }
	inline FsmVector2_t2965096677 ** get_address_of_vector_17() { return &___vector_17; }
	inline void set_vector_17(FsmVector2_t2965096677 * value)
	{
		___vector_17 = value;
		Il2CppCodeGenWriteBarrier((&___vector_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(GetVelocity2d_t1516202885, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(GetVelocity2d_t1516202885, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_space_20() { return static_cast<int32_t>(offsetof(GetVelocity2d_t1516202885, ___space_20)); }
	inline int32_t get_space_20() const { return ___space_20; }
	inline int32_t* get_address_of_space_20() { return &___space_20; }
	inline void set_space_20(int32_t value)
	{
		___space_20 = value;
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(GetVelocity2d_t1516202885, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVELOCITY2D_T1516202885_H
#ifndef ISFIXEDANGLE2D_T1048615485_H
#define ISFIXEDANGLE2D_T1048615485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsFixedAngle2d
struct  IsFixedAngle2d_t1048615485  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsFixedAngle2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsFixedAngle2d::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsFixedAngle2d::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsFixedAngle2d::store
	FsmBool_t163807967 * ___store_19;
	// System.Boolean HutongGames.PlayMaker.Actions.IsFixedAngle2d::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(IsFixedAngle2d_t1048615485, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(IsFixedAngle2d_t1048615485, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(IsFixedAngle2d_t1048615485, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_store_19() { return static_cast<int32_t>(offsetof(IsFixedAngle2d_t1048615485, ___store_19)); }
	inline FsmBool_t163807967 * get_store_19() const { return ___store_19; }
	inline FsmBool_t163807967 ** get_address_of_store_19() { return &___store_19; }
	inline void set_store_19(FsmBool_t163807967 * value)
	{
		___store_19 = value;
		Il2CppCodeGenWriteBarrier((&___store_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(IsFixedAngle2d_t1048615485, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISFIXEDANGLE2D_T1048615485_H
#ifndef ISKINEMATIC_T2321814586_H
#define ISKINEMATIC_T2321814586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsKinematic
struct  IsKinematic_t2321814586  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsKinematic::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsKinematic::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsKinematic::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsKinematic::store
	FsmBool_t163807967 * ___store_19;
	// System.Boolean HutongGames.PlayMaker.Actions.IsKinematic::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(IsKinematic_t2321814586, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(IsKinematic_t2321814586, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(IsKinematic_t2321814586, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_store_19() { return static_cast<int32_t>(offsetof(IsKinematic_t2321814586, ___store_19)); }
	inline FsmBool_t163807967 * get_store_19() const { return ___store_19; }
	inline FsmBool_t163807967 ** get_address_of_store_19() { return &___store_19; }
	inline void set_store_19(FsmBool_t163807967 * value)
	{
		___store_19 = value;
		Il2CppCodeGenWriteBarrier((&___store_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(IsKinematic_t2321814586, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISKINEMATIC_T2321814586_H
#ifndef ISKINEMATIC2D_T3018647848_H
#define ISKINEMATIC2D_T3018647848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsKinematic2d
struct  IsKinematic2d_t3018647848  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsKinematic2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsKinematic2d::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsKinematic2d::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsKinematic2d::store
	FsmBool_t163807967 * ___store_19;
	// System.Boolean HutongGames.PlayMaker.Actions.IsKinematic2d::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(IsKinematic2d_t3018647848, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(IsKinematic2d_t3018647848, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(IsKinematic2d_t3018647848, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_store_19() { return static_cast<int32_t>(offsetof(IsKinematic2d_t3018647848, ___store_19)); }
	inline FsmBool_t163807967 * get_store_19() const { return ___store_19; }
	inline FsmBool_t163807967 ** get_address_of_store_19() { return &___store_19; }
	inline void set_store_19(FsmBool_t163807967 * value)
	{
		___store_19 = value;
		Il2CppCodeGenWriteBarrier((&___store_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(IsKinematic2d_t3018647848, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISKINEMATIC2D_T3018647848_H
#ifndef ISSLEEPING_T2673733942_H
#define ISSLEEPING_T2673733942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsSleeping
struct  IsSleeping_t2673733942  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsSleeping::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsSleeping::store
	FsmBool_t163807967 * ___store_19;
	// System.Boolean HutongGames.PlayMaker.Actions.IsSleeping::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(IsSleeping_t2673733942, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(IsSleeping_t2673733942, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(IsSleeping_t2673733942, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_store_19() { return static_cast<int32_t>(offsetof(IsSleeping_t2673733942, ___store_19)); }
	inline FsmBool_t163807967 * get_store_19() const { return ___store_19; }
	inline FsmBool_t163807967 ** get_address_of_store_19() { return &___store_19; }
	inline void set_store_19(FsmBool_t163807967 * value)
	{
		___store_19 = value;
		Il2CppCodeGenWriteBarrier((&___store_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(IsSleeping_t2673733942, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISSLEEPING_T2673733942_H
#ifndef ISSLEEPING2D_T2208738341_H
#define ISSLEEPING2D_T2208738341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsSleeping2d
struct  IsSleeping2d_t2208738341  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.IsSleeping2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping2d::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsSleeping2d::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsSleeping2d::store
	FsmBool_t163807967 * ___store_19;
	// System.Boolean HutongGames.PlayMaker.Actions.IsSleeping2d::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(IsSleeping2d_t2208738341, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(IsSleeping2d_t2208738341, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(IsSleeping2d_t2208738341, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_store_19() { return static_cast<int32_t>(offsetof(IsSleeping2d_t2208738341, ___store_19)); }
	inline FsmBool_t163807967 * get_store_19() const { return ___store_19; }
	inline FsmBool_t163807967 ** get_address_of_store_19() { return &___store_19; }
	inline void set_store_19(FsmBool_t163807967 * value)
	{
		___store_19 = value;
		Il2CppCodeGenWriteBarrier((&___store_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(IsSleeping2d_t2208738341, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISSLEEPING2D_T2208738341_H
#ifndef SETDRAG_T3220774457_H
#define SETDRAG_T3220774457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetDrag
struct  SetDrag_t3220774457  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetDrag::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetDrag::drag
	FsmFloat_t2883254149 * ___drag_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetDrag::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetDrag_t3220774457, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_drag_17() { return static_cast<int32_t>(offsetof(SetDrag_t3220774457, ___drag_17)); }
	inline FsmFloat_t2883254149 * get_drag_17() const { return ___drag_17; }
	inline FsmFloat_t2883254149 ** get_address_of_drag_17() { return &___drag_17; }
	inline void set_drag_17(FsmFloat_t2883254149 * value)
	{
		___drag_17 = value;
		Il2CppCodeGenWriteBarrier((&___drag_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetDrag_t3220774457, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETDRAG_T3220774457_H
#ifndef SETGRAVITY2DSCALE_T3897070255_H
#define SETGRAVITY2DSCALE_T3897070255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetGravity2dScale
struct  SetGravity2dScale_t3897070255  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetGravity2dScale::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetGravity2dScale::gravityScale
	FsmFloat_t2883254149 * ___gravityScale_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetGravity2dScale_t3897070255, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_gravityScale_17() { return static_cast<int32_t>(offsetof(SetGravity2dScale_t3897070255, ___gravityScale_17)); }
	inline FsmFloat_t2883254149 * get_gravityScale_17() const { return ___gravityScale_17; }
	inline FsmFloat_t2883254149 ** get_address_of_gravityScale_17() { return &___gravityScale_17; }
	inline void set_gravityScale_17(FsmFloat_t2883254149 * value)
	{
		___gravityScale_17 = value;
		Il2CppCodeGenWriteBarrier((&___gravityScale_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETGRAVITY2DSCALE_T3897070255_H
#ifndef SETISFIXEDANGLE2D_T3710656154_H
#define SETISFIXEDANGLE2D_T3710656154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetIsFixedAngle2d
struct  SetIsFixedAngle2d_t3710656154  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::isFixedAngle
	FsmBool_t163807967 * ___isFixedAngle_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetIsFixedAngle2d::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetIsFixedAngle2d_t3710656154, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_isFixedAngle_17() { return static_cast<int32_t>(offsetof(SetIsFixedAngle2d_t3710656154, ___isFixedAngle_17)); }
	inline FsmBool_t163807967 * get_isFixedAngle_17() const { return ___isFixedAngle_17; }
	inline FsmBool_t163807967 ** get_address_of_isFixedAngle_17() { return &___isFixedAngle_17; }
	inline void set_isFixedAngle_17(FsmBool_t163807967 * value)
	{
		___isFixedAngle_17 = value;
		Il2CppCodeGenWriteBarrier((&___isFixedAngle_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetIsFixedAngle2d_t3710656154, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETISFIXEDANGLE2D_T3710656154_H
#ifndef SETISKINEMATIC_T3592521295_H
#define SETISKINEMATIC_T3592521295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetIsKinematic
struct  SetIsKinematic_t3592521295  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetIsKinematic::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetIsKinematic::isKinematic
	FsmBool_t163807967 * ___isKinematic_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetIsKinematic_t3592521295, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_isKinematic_17() { return static_cast<int32_t>(offsetof(SetIsKinematic_t3592521295, ___isKinematic_17)); }
	inline FsmBool_t163807967 * get_isKinematic_17() const { return ___isKinematic_17; }
	inline FsmBool_t163807967 ** get_address_of_isKinematic_17() { return &___isKinematic_17; }
	inline void set_isKinematic_17(FsmBool_t163807967 * value)
	{
		___isKinematic_17 = value;
		Il2CppCodeGenWriteBarrier((&___isKinematic_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETISKINEMATIC_T3592521295_H
#ifndef SETISKINEMATIC2D_T3305420780_H
#define SETISKINEMATIC2D_T3305420780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetIsKinematic2d
struct  SetIsKinematic2d_t3305420780  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetIsKinematic2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetIsKinematic2d::isKinematic
	FsmBool_t163807967 * ___isKinematic_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetIsKinematic2d_t3305420780, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_isKinematic_17() { return static_cast<int32_t>(offsetof(SetIsKinematic2d_t3305420780, ___isKinematic_17)); }
	inline FsmBool_t163807967 * get_isKinematic_17() const { return ___isKinematic_17; }
	inline FsmBool_t163807967 ** get_address_of_isKinematic_17() { return &___isKinematic_17; }
	inline void set_isKinematic_17(FsmBool_t163807967 * value)
	{
		___isKinematic_17 = value;
		Il2CppCodeGenWriteBarrier((&___isKinematic_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETISKINEMATIC2D_T3305420780_H
#ifndef SETMASS_T1532681360_H
#define SETMASS_T1532681360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMass
struct  SetMass_t1532681360  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMass::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetMass::mass
	FsmFloat_t2883254149 * ___mass_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetMass_t1532681360, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_mass_17() { return static_cast<int32_t>(offsetof(SetMass_t1532681360, ___mass_17)); }
	inline FsmFloat_t2883254149 * get_mass_17() const { return ___mass_17; }
	inline FsmFloat_t2883254149 ** get_address_of_mass_17() { return &___mass_17; }
	inline void set_mass_17(FsmFloat_t2883254149 * value)
	{
		___mass_17 = value;
		Il2CppCodeGenWriteBarrier((&___mass_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMASS_T1532681360_H
#ifndef SETMASS2D_T3302329633_H
#define SETMASS2D_T3302329633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMass2d
struct  SetMass2d_t3302329633  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMass2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetMass2d::mass
	FsmFloat_t2883254149 * ___mass_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetMass2d_t3302329633, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_mass_17() { return static_cast<int32_t>(offsetof(SetMass2d_t3302329633, ___mass_17)); }
	inline FsmFloat_t2883254149 * get_mass_17() const { return ___mass_17; }
	inline FsmFloat_t2883254149 ** get_address_of_mass_17() { return &___mass_17; }
	inline void set_mass_17(FsmFloat_t2883254149 * value)
	{
		___mass_17 = value;
		Il2CppCodeGenWriteBarrier((&___mass_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMASS2D_T3302329633_H
#ifndef SETVELOCITY_T1801814435_H
#define SETVELOCITY_T1801814435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVelocity
struct  SetVelocity_t1801814435  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetVelocity::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetVelocity::vector
	FsmVector3_t626444517 * ___vector_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity::y
	FsmFloat_t2883254149 * ___y_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity::z
	FsmFloat_t2883254149 * ___z_20;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.SetVelocity::space
	int32_t ___space_21;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVelocity::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_vector_17() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___vector_17)); }
	inline FsmVector3_t626444517 * get_vector_17() const { return ___vector_17; }
	inline FsmVector3_t626444517 ** get_address_of_vector_17() { return &___vector_17; }
	inline void set_vector_17(FsmVector3_t626444517 * value)
	{
		___vector_17 = value;
		Il2CppCodeGenWriteBarrier((&___vector_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_z_20() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___z_20)); }
	inline FsmFloat_t2883254149 * get_z_20() const { return ___z_20; }
	inline FsmFloat_t2883254149 ** get_address_of_z_20() { return &___z_20; }
	inline void set_z_20(FsmFloat_t2883254149 * value)
	{
		___z_20 = value;
		Il2CppCodeGenWriteBarrier((&___z_20), value);
	}

	inline static int32_t get_offset_of_space_21() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___space_21)); }
	inline int32_t get_space_21() const { return ___space_21; }
	inline int32_t* get_address_of_space_21() { return &___space_21; }
	inline void set_space_21(int32_t value)
	{
		___space_21 = value;
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(SetVelocity_t1801814435, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVELOCITY_T1801814435_H
#ifndef SETVELOCITY2D_T520801329_H
#define SETVELOCITY2D_T520801329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVelocity2d
struct  SetVelocity2d_t520801329  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetVelocity2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetVelocity2d::vector
	FsmVector2_t2965096677 * ___vector_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity2d::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetVelocity2d::y
	FsmFloat_t2883254149 * ___y_19;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVelocity2d::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetVelocity2d_t520801329, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_vector_17() { return static_cast<int32_t>(offsetof(SetVelocity2d_t520801329, ___vector_17)); }
	inline FsmVector2_t2965096677 * get_vector_17() const { return ___vector_17; }
	inline FsmVector2_t2965096677 ** get_address_of_vector_17() { return &___vector_17; }
	inline void set_vector_17(FsmVector2_t2965096677 * value)
	{
		___vector_17 = value;
		Il2CppCodeGenWriteBarrier((&___vector_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(SetVelocity2d_t520801329, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(SetVelocity2d_t520801329, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(SetVelocity2d_t520801329, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVELOCITY2D_T520801329_H
#ifndef SLEEP_T3978799563_H
#define SLEEP_T3978799563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Sleep
struct  Sleep_t3978799563  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Sleep::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(Sleep_t3978799563, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLEEP_T3978799563_H
#ifndef SLEEP2D_T2711291265_H
#define SLEEP2D_T2711291265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Sleep2d
struct  Sleep2d_t2711291265  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Sleep2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(Sleep2d_t2711291265, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLEEP2D_T2711291265_H
#ifndef USEGRAVITY_T2943585729_H
#define USEGRAVITY_T2943585729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UseGravity
struct  UseGravity_t2943585729  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UseGravity::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UseGravity::useGravity
	FsmBool_t163807967 * ___useGravity_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(UseGravity_t2943585729, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_useGravity_17() { return static_cast<int32_t>(offsetof(UseGravity_t2943585729, ___useGravity_17)); }
	inline FsmBool_t163807967 * get_useGravity_17() const { return ___useGravity_17; }
	inline FsmBool_t163807967 ** get_address_of_useGravity_17() { return &___useGravity_17; }
	inline void set_useGravity_17(FsmBool_t163807967 * value)
	{
		___useGravity_17 = value;
		Il2CppCodeGenWriteBarrier((&___useGravity_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEGRAVITY_T2943585729_H
#ifndef WAKEUP_T115702558_H
#define WAKEUP_T115702558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WakeUp
struct  WakeUp_t115702558  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.WakeUp::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(WakeUp_t115702558, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAKEUP_T115702558_H
#ifndef WAKEUP2D_T2180700450_H
#define WAKEUP2D_T2180700450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.WakeUp2d
struct  WakeUp2d_t2180700450  : public ComponentAction_1_t3816781823
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.WakeUp2d::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(WakeUp2d_t2180700450, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAKEUP2D_T2180700450_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (FloatAddMultiple_t200444465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3400[3] = 
{
	FloatAddMultiple_t200444465::get_offset_of_floatVariables_14(),
	FloatAddMultiple_t200444465::get_offset_of_addTo_15(),
	FloatAddMultiple_t200444465::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (FloatClamp_t3804449708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3401[4] = 
{
	FloatClamp_t3804449708::get_offset_of_floatVariable_14(),
	FloatClamp_t3804449708::get_offset_of_minValue_15(),
	FloatClamp_t3804449708::get_offset_of_maxValue_16(),
	FloatClamp_t3804449708::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (FloatDivide_t2012883063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3402[3] = 
{
	FloatDivide_t2012883063::get_offset_of_floatVariable_14(),
	FloatDivide_t2012883063::get_offset_of_divideBy_15(),
	FloatDivide_t2012883063::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (FloatInterpolate_t1692993408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3403[9] = 
{
	FloatInterpolate_t1692993408::get_offset_of_mode_14(),
	FloatInterpolate_t1692993408::get_offset_of_fromFloat_15(),
	FloatInterpolate_t1692993408::get_offset_of_toFloat_16(),
	FloatInterpolate_t1692993408::get_offset_of_time_17(),
	FloatInterpolate_t1692993408::get_offset_of_storeResult_18(),
	FloatInterpolate_t1692993408::get_offset_of_finishEvent_19(),
	FloatInterpolate_t1692993408::get_offset_of_realTime_20(),
	FloatInterpolate_t1692993408::get_offset_of_startTime_21(),
	FloatInterpolate_t1692993408::get_offset_of_currentTime_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (FloatMultiply_t1744521690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[3] = 
{
	FloatMultiply_t1744521690::get_offset_of_floatVariable_14(),
	FloatMultiply_t1744521690::get_offset_of_multiplyBy_15(),
	FloatMultiply_t1744521690::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (FloatOperator_t3450066926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3405[5] = 
{
	FloatOperator_t3450066926::get_offset_of_float1_14(),
	FloatOperator_t3450066926::get_offset_of_float2_15(),
	FloatOperator_t3450066926::get_offset_of_operation_16(),
	FloatOperator_t3450066926::get_offset_of_storeResult_17(),
	FloatOperator_t3450066926::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (Operation_t79835483)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3406[7] = 
{
	Operation_t79835483::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (FloatSubtract_t2940113104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[4] = 
{
	FloatSubtract_t2940113104::get_offset_of_floatVariable_14(),
	FloatSubtract_t2940113104::get_offset_of_subtract_15(),
	FloatSubtract_t2940113104::get_offset_of_everyFrame_16(),
	FloatSubtract_t2940113104::get_offset_of_perSecond_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (IntAdd_t2214403792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[3] = 
{
	IntAdd_t2214403792::get_offset_of_intVariable_14(),
	IntAdd_t2214403792::get_offset_of_add_15(),
	IntAdd_t2214403792::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (IntClamp_t3495764295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[4] = 
{
	IntClamp_t3495764295::get_offset_of_intVariable_14(),
	IntClamp_t3495764295::get_offset_of_minValue_15(),
	IntClamp_t3495764295::get_offset_of_maxValue_16(),
	IntClamp_t3495764295::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (IntOperator_t945431901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3410[5] = 
{
	IntOperator_t945431901::get_offset_of_integer1_14(),
	IntOperator_t945431901::get_offset_of_integer2_15(),
	IntOperator_t945431901::get_offset_of_operation_16(),
	IntOperator_t945431901::get_offset_of_storeResult_17(),
	IntOperator_t945431901::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (Operation_t3934176782)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3411[7] = 
{
	Operation_t3934176782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (RandomBool_t566293558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[1] = 
{
	RandomBool_t566293558::get_offset_of_storeResult_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (RandomFloat_t3836739527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[3] = 
{
	RandomFloat_t3836739527::get_offset_of_min_14(),
	RandomFloat_t3836739527::get_offset_of_max_15(),
	RandomFloat_t3836739527::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (RandomInt_t1104706112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[7] = 
{
	RandomInt_t1104706112::get_offset_of_min_14(),
	RandomInt_t1104706112::get_offset_of_max_15(),
	RandomInt_t1104706112::get_offset_of_storeResult_16(),
	RandomInt_t1104706112::get_offset_of_inclusiveMax_17(),
	RandomInt_t1104706112::get_offset_of_noRepeat_18(),
	RandomInt_t1104706112::get_offset_of_randomIndex_19(),
	RandomInt_t1104706112::get_offset_of_lastIndex_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (SampleCurve_t4047358761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[4] = 
{
	SampleCurve_t4047358761::get_offset_of_curve_14(),
	SampleCurve_t4047358761::get_offset_of_sampleAt_15(),
	SampleCurve_t4047358761::get_offset_of_storeValue_16(),
	SampleCurve_t4047358761::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (SetBoolValue_t4097795596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3416[3] = 
{
	SetBoolValue_t4097795596::get_offset_of_boolVariable_14(),
	SetBoolValue_t4097795596::get_offset_of_boolValue_15(),
	SetBoolValue_t4097795596::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (SetFloatValue_t3224737059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3417[3] = 
{
	SetFloatValue_t3224737059::get_offset_of_floatVariable_14(),
	SetFloatValue_t3224737059::get_offset_of_floatValue_15(),
	SetFloatValue_t3224737059::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (SetIntFromFloat_t7289849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3418[3] = 
{
	SetIntFromFloat_t7289849::get_offset_of_intVariable_14(),
	SetIntFromFloat_t7289849::get_offset_of_floatValue_15(),
	SetIntFromFloat_t7289849::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (SetIntValue_t120909476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[3] = 
{
	SetIntValue_t120909476::get_offset_of_intVariable_14(),
	SetIntValue_t120909476::get_offset_of_intValue_15(),
	SetIntValue_t120909476::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (GetVertexCount_t1911616008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3420[3] = 
{
	GetVertexCount_t1911616008::get_offset_of_gameObject_14(),
	GetVertexCount_t1911616008::get_offset_of_storeCount_15(),
	GetVertexCount_t1911616008::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (GetVertexPosition_t1214669549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3421[5] = 
{
	GetVertexPosition_t1214669549::get_offset_of_gameObject_14(),
	GetVertexPosition_t1214669549::get_offset_of_vertexIndex_15(),
	GetVertexPosition_t1214669549::get_offset_of_space_16(),
	GetVertexPosition_t1214669549::get_offset_of_storePosition_17(),
	GetVertexPosition_t1214669549::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (AddExplosionForce_t3481544241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3422[7] = 
{
	AddExplosionForce_t3481544241::get_offset_of_gameObject_16(),
	AddExplosionForce_t3481544241::get_offset_of_center_17(),
	AddExplosionForce_t3481544241::get_offset_of_force_18(),
	AddExplosionForce_t3481544241::get_offset_of_radius_19(),
	AddExplosionForce_t3481544241::get_offset_of_upwardsModifier_20(),
	AddExplosionForce_t3481544241::get_offset_of_forceMode_21(),
	AddExplosionForce_t3481544241::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (AddForce_t1469844092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[9] = 
{
	AddForce_t1469844092::get_offset_of_gameObject_16(),
	AddForce_t1469844092::get_offset_of_atPosition_17(),
	AddForce_t1469844092::get_offset_of_vector_18(),
	AddForce_t1469844092::get_offset_of_x_19(),
	AddForce_t1469844092::get_offset_of_y_20(),
	AddForce_t1469844092::get_offset_of_z_21(),
	AddForce_t1469844092::get_offset_of_space_22(),
	AddForce_t1469844092::get_offset_of_forceMode_23(),
	AddForce_t1469844092::get_offset_of_everyFrame_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (AddTorque_t800833020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3424[8] = 
{
	AddTorque_t800833020::get_offset_of_gameObject_16(),
	AddTorque_t800833020::get_offset_of_vector_17(),
	AddTorque_t800833020::get_offset_of_x_18(),
	AddTorque_t800833020::get_offset_of_y_19(),
	AddTorque_t800833020::get_offset_of_z_20(),
	AddTorque_t800833020::get_offset_of_space_21(),
	AddTorque_t800833020::get_offset_of_forceMode_22(),
	AddTorque_t800833020::get_offset_of_everyFrame_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (CollisionEvent_t3345175258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3425[7] = 
{
	CollisionEvent_t3345175258::get_offset_of_gameObject_14(),
	CollisionEvent_t3345175258::get_offset_of_collision_15(),
	CollisionEvent_t3345175258::get_offset_of_collideTag_16(),
	CollisionEvent_t3345175258::get_offset_of_sendEvent_17(),
	CollisionEvent_t3345175258::get_offset_of_storeCollider_18(),
	CollisionEvent_t3345175258::get_offset_of_storeForce_19(),
	CollisionEvent_t3345175258::get_offset_of_cachedProxy_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (Explosion_t2931029749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3426[9] = 
{
	Explosion_t2931029749::get_offset_of_center_14(),
	Explosion_t2931029749::get_offset_of_force_15(),
	Explosion_t2931029749::get_offset_of_radius_16(),
	Explosion_t2931029749::get_offset_of_upwardsModifier_17(),
	Explosion_t2931029749::get_offset_of_forceMode_18(),
	Explosion_t2931029749::get_offset_of_layer_19(),
	Explosion_t2931029749::get_offset_of_layerMask_20(),
	Explosion_t2931029749::get_offset_of_invertMask_21(),
	Explosion_t2931029749::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (GetCollisionInfo_t3436222585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[6] = 
{
	GetCollisionInfo_t3436222585::get_offset_of_gameObjectHit_14(),
	GetCollisionInfo_t3436222585::get_offset_of_relativeVelocity_15(),
	GetCollisionInfo_t3436222585::get_offset_of_relativeSpeed_16(),
	GetCollisionInfo_t3436222585::get_offset_of_contactPoint_17(),
	GetCollisionInfo_t3436222585::get_offset_of_contactNormal_18(),
	GetCollisionInfo_t3436222585::get_offset_of_physicsMaterialName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (GetJointBreakInfo_t181654311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3428[1] = 
{
	GetJointBreakInfo_t181654311::get_offset_of_breakForce_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (GetMass_t3973594356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[2] = 
{
	GetMass_t3973594356::get_offset_of_gameObject_16(),
	GetMass_t3973594356::get_offset_of_storeResult_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (GetParticleCollisionInfo_t297522531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[1] = 
{
	GetParticleCollisionInfo_t297522531::get_offset_of_gameObjectHit_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (GetRaycastAllInfo_t2362054518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[5] = 
{
	GetRaycastAllInfo_t2362054518::get_offset_of_storeHitObjects_14(),
	GetRaycastAllInfo_t2362054518::get_offset_of_points_15(),
	GetRaycastAllInfo_t2362054518::get_offset_of_normals_16(),
	GetRaycastAllInfo_t2362054518::get_offset_of_distances_17(),
	GetRaycastAllInfo_t2362054518::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (GetRaycastHitInfo_t1121319619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3432[5] = 
{
	GetRaycastHitInfo_t1121319619::get_offset_of_gameObjectHit_14(),
	GetRaycastHitInfo_t1121319619::get_offset_of_point_15(),
	GetRaycastHitInfo_t1121319619::get_offset_of_normal_16(),
	GetRaycastHitInfo_t1121319619::get_offset_of_distance_17(),
	GetRaycastHitInfo_t1121319619::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (GetSpeed_t1149696821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3433[3] = 
{
	GetSpeed_t1149696821::get_offset_of_gameObject_16(),
	GetSpeed_t1149696821::get_offset_of_storeResult_17(),
	GetSpeed_t1149696821::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (GetTriggerInfo_t3792290394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[2] = 
{
	GetTriggerInfo_t3792290394::get_offset_of_gameObjectHit_14(),
	GetTriggerInfo_t3792290394::get_offset_of_physicsMaterialName_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (GetVelocity_t3685857823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[7] = 
{
	GetVelocity_t3685857823::get_offset_of_gameObject_16(),
	GetVelocity_t3685857823::get_offset_of_vector_17(),
	GetVelocity_t3685857823::get_offset_of_x_18(),
	GetVelocity_t3685857823::get_offset_of_y_19(),
	GetVelocity_t3685857823::get_offset_of_z_20(),
	GetVelocity_t3685857823::get_offset_of_space_21(),
	GetVelocity_t3685857823::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (IsKinematic_t2321814586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3436[5] = 
{
	IsKinematic_t2321814586::get_offset_of_gameObject_16(),
	IsKinematic_t2321814586::get_offset_of_trueEvent_17(),
	IsKinematic_t2321814586::get_offset_of_falseEvent_18(),
	IsKinematic_t2321814586::get_offset_of_store_19(),
	IsKinematic_t2321814586::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (IsSleeping_t2673733942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[5] = 
{
	IsSleeping_t2673733942::get_offset_of_gameObject_16(),
	IsSleeping_t2673733942::get_offset_of_trueEvent_17(),
	IsSleeping_t2673733942::get_offset_of_falseEvent_18(),
	IsSleeping_t2673733942::get_offset_of_store_19(),
	IsSleeping_t2673733942::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (Raycast_t3332644267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[17] = 
{
	Raycast_t3332644267::get_offset_of_fromGameObject_14(),
	Raycast_t3332644267::get_offset_of_fromPosition_15(),
	Raycast_t3332644267::get_offset_of_direction_16(),
	Raycast_t3332644267::get_offset_of_space_17(),
	Raycast_t3332644267::get_offset_of_distance_18(),
	Raycast_t3332644267::get_offset_of_hitEvent_19(),
	Raycast_t3332644267::get_offset_of_storeDidHit_20(),
	Raycast_t3332644267::get_offset_of_storeHitObject_21(),
	Raycast_t3332644267::get_offset_of_storeHitPoint_22(),
	Raycast_t3332644267::get_offset_of_storeHitNormal_23(),
	Raycast_t3332644267::get_offset_of_storeHitDistance_24(),
	Raycast_t3332644267::get_offset_of_repeatInterval_25(),
	Raycast_t3332644267::get_offset_of_layerMask_26(),
	Raycast_t3332644267::get_offset_of_invertMask_27(),
	Raycast_t3332644267::get_offset_of_debugColor_28(),
	Raycast_t3332644267::get_offset_of_debug_29(),
	Raycast_t3332644267::get_offset_of_repeat_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (RaycastAll_t710648924), -1, sizeof(RaycastAll_t710648924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3439[18] = 
{
	RaycastAll_t710648924_StaticFields::get_offset_of_RaycastAllHitInfo_14(),
	RaycastAll_t710648924::get_offset_of_fromGameObject_15(),
	RaycastAll_t710648924::get_offset_of_fromPosition_16(),
	RaycastAll_t710648924::get_offset_of_direction_17(),
	RaycastAll_t710648924::get_offset_of_space_18(),
	RaycastAll_t710648924::get_offset_of_distance_19(),
	RaycastAll_t710648924::get_offset_of_hitEvent_20(),
	RaycastAll_t710648924::get_offset_of_storeDidHit_21(),
	RaycastAll_t710648924::get_offset_of_storeHitObjects_22(),
	RaycastAll_t710648924::get_offset_of_storeHitPoint_23(),
	RaycastAll_t710648924::get_offset_of_storeHitNormal_24(),
	RaycastAll_t710648924::get_offset_of_storeHitDistance_25(),
	RaycastAll_t710648924::get_offset_of_repeatInterval_26(),
	RaycastAll_t710648924::get_offset_of_layerMask_27(),
	RaycastAll_t710648924::get_offset_of_invertMask_28(),
	RaycastAll_t710648924::get_offset_of_debugColor_29(),
	RaycastAll_t710648924::get_offset_of_debug_30(),
	RaycastAll_t710648924::get_offset_of_repeat_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (SetDrag_t3220774457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3440[3] = 
{
	SetDrag_t3220774457::get_offset_of_gameObject_16(),
	SetDrag_t3220774457::get_offset_of_drag_17(),
	SetDrag_t3220774457::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (SetGravity_t85159497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3441[5] = 
{
	SetGravity_t85159497::get_offset_of_vector_14(),
	SetGravity_t85159497::get_offset_of_x_15(),
	SetGravity_t85159497::get_offset_of_y_16(),
	SetGravity_t85159497::get_offset_of_z_17(),
	SetGravity_t85159497::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (SetIsKinematic_t3592521295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[2] = 
{
	SetIsKinematic_t3592521295::get_offset_of_gameObject_16(),
	SetIsKinematic_t3592521295::get_offset_of_isKinematic_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (SetJointConnectedBody_t3705285269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3443[2] = 
{
	SetJointConnectedBody_t3705285269::get_offset_of_joint_14(),
	SetJointConnectedBody_t3705285269::get_offset_of_rigidBody_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (SetMass_t1532681360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[2] = 
{
	SetMass_t1532681360::get_offset_of_gameObject_16(),
	SetMass_t1532681360::get_offset_of_mass_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (SetVelocity_t1801814435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[7] = 
{
	SetVelocity_t1801814435::get_offset_of_gameObject_16(),
	SetVelocity_t1801814435::get_offset_of_vector_17(),
	SetVelocity_t1801814435::get_offset_of_x_18(),
	SetVelocity_t1801814435::get_offset_of_y_19(),
	SetVelocity_t1801814435::get_offset_of_z_20(),
	SetVelocity_t1801814435::get_offset_of_space_21(),
	SetVelocity_t1801814435::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (Sleep_t3978799563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[1] = 
{
	Sleep_t3978799563::get_offset_of_gameObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (TriggerEvent_t2948667628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3447[6] = 
{
	TriggerEvent_t2948667628::get_offset_of_gameObject_14(),
	TriggerEvent_t2948667628::get_offset_of_trigger_15(),
	TriggerEvent_t2948667628::get_offset_of_collideTag_16(),
	TriggerEvent_t2948667628::get_offset_of_sendEvent_17(),
	TriggerEvent_t2948667628::get_offset_of_storeCollider_18(),
	TriggerEvent_t2948667628::get_offset_of_cachedProxy_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (UseGravity_t2943585729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3448[2] = 
{
	UseGravity_t2943585729::get_offset_of_gameObject_16(),
	UseGravity_t2943585729::get_offset_of_useGravity_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (WakeAllRigidBodies_t502384016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[2] = 
{
	WakeAllRigidBodies_t502384016::get_offset_of_everyFrame_14(),
	WakeAllRigidBodies_t502384016::get_offset_of_bodies_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (WakeUp_t115702558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3450[1] = 
{
	WakeUp_t115702558::get_offset_of_gameObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (AddForce2d_t3511446623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[8] = 
{
	AddForce2d_t3511446623::get_offset_of_gameObject_16(),
	AddForce2d_t3511446623::get_offset_of_forceMode_17(),
	AddForce2d_t3511446623::get_offset_of_atPosition_18(),
	AddForce2d_t3511446623::get_offset_of_vector_19(),
	AddForce2d_t3511446623::get_offset_of_x_20(),
	AddForce2d_t3511446623::get_offset_of_y_21(),
	AddForce2d_t3511446623::get_offset_of_vector3_22(),
	AddForce2d_t3511446623::get_offset_of_everyFrame_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (AddRelativeForce2d_t1292494680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[7] = 
{
	AddRelativeForce2d_t1292494680::get_offset_of_gameObject_16(),
	AddRelativeForce2d_t1292494680::get_offset_of_forceMode_17(),
	AddRelativeForce2d_t1292494680::get_offset_of_vector_18(),
	AddRelativeForce2d_t1292494680::get_offset_of_x_19(),
	AddRelativeForce2d_t1292494680::get_offset_of_y_20(),
	AddRelativeForce2d_t1292494680::get_offset_of_vector3_21(),
	AddRelativeForce2d_t1292494680::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (AddTorque2d_t3500577466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[4] = 
{
	AddTorque2d_t3500577466::get_offset_of_gameObject_16(),
	AddTorque2d_t3500577466::get_offset_of_forceMode_17(),
	AddTorque2d_t3500577466::get_offset_of_torque_18(),
	AddTorque2d_t3500577466::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (Collision2dEvent_t2872455664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3454[7] = 
{
	Collision2dEvent_t2872455664::get_offset_of_gameObject_14(),
	Collision2dEvent_t2872455664::get_offset_of_collision_15(),
	Collision2dEvent_t2872455664::get_offset_of_collideTag_16(),
	Collision2dEvent_t2872455664::get_offset_of_sendEvent_17(),
	Collision2dEvent_t2872455664::get_offset_of_storeCollider_18(),
	Collision2dEvent_t2872455664::get_offset_of_storeForce_19(),
	Collision2dEvent_t2872455664::get_offset_of_cachedProxy_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (GetCollision2dInfo_t1725384043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[7] = 
{
	GetCollision2dInfo_t1725384043::get_offset_of_gameObjectHit_14(),
	GetCollision2dInfo_t1725384043::get_offset_of_relativeVelocity_15(),
	GetCollision2dInfo_t1725384043::get_offset_of_relativeSpeed_16(),
	GetCollision2dInfo_t1725384043::get_offset_of_contactPoint_17(),
	GetCollision2dInfo_t1725384043::get_offset_of_contactNormal_18(),
	GetCollision2dInfo_t1725384043::get_offset_of_shapeCount_19(),
	GetCollision2dInfo_t1725384043::get_offset_of_physics2dMaterialName_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (GetJointBreak2dInfo_t519263708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3456[4] = 
{
	GetJointBreak2dInfo_t519263708::get_offset_of_brokenJoint_14(),
	GetJointBreak2dInfo_t519263708::get_offset_of_reactionForce_15(),
	GetJointBreak2dInfo_t519263708::get_offset_of_reactionForceMagnitude_16(),
	GetJointBreak2dInfo_t519263708::get_offset_of_reactionTorque_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (GetMass2d_t2644716189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3457[2] = 
{
	GetMass2d_t2644716189::get_offset_of_gameObject_16(),
	GetMass2d_t2644716189::get_offset_of_storeResult_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (GetNextLineCast2d_t4255660837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3458[19] = 
{
	GetNextLineCast2d_t4255660837::get_offset_of_fromGameObject_14(),
	GetNextLineCast2d_t4255660837::get_offset_of_fromPosition_15(),
	GetNextLineCast2d_t4255660837::get_offset_of_toGameObject_16(),
	GetNextLineCast2d_t4255660837::get_offset_of_toPosition_17(),
	GetNextLineCast2d_t4255660837::get_offset_of_minDepth_18(),
	GetNextLineCast2d_t4255660837::get_offset_of_maxDepth_19(),
	GetNextLineCast2d_t4255660837::get_offset_of_resetFlag_20(),
	GetNextLineCast2d_t4255660837::get_offset_of_layerMask_21(),
	GetNextLineCast2d_t4255660837::get_offset_of_invertMask_22(),
	GetNextLineCast2d_t4255660837::get_offset_of_collidersCount_23(),
	GetNextLineCast2d_t4255660837::get_offset_of_storeNextCollider_24(),
	GetNextLineCast2d_t4255660837::get_offset_of_storeNextHitPoint_25(),
	GetNextLineCast2d_t4255660837::get_offset_of_storeNextHitNormal_26(),
	GetNextLineCast2d_t4255660837::get_offset_of_storeNextHitDistance_27(),
	GetNextLineCast2d_t4255660837::get_offset_of_loopEvent_28(),
	GetNextLineCast2d_t4255660837::get_offset_of_finishedEvent_29(),
	GetNextLineCast2d_t4255660837::get_offset_of_hits_30(),
	GetNextLineCast2d_t4255660837::get_offset_of_colliderCount_31(),
	GetNextLineCast2d_t4255660837::get_offset_of_nextColliderIndex_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (GetNextOverlapArea2d_t3488004213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3459[16] = 
{
	GetNextOverlapArea2d_t3488004213::get_offset_of_firstCornerGameObject_14(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_firstCornerPosition_15(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_secondCornerGameObject_16(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_secondCornerPosition_17(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_minDepth_18(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_maxDepth_19(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_resetFlag_20(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_layerMask_21(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_invertMask_22(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_collidersCount_23(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_storeNextCollider_24(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_loopEvent_25(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_finishedEvent_26(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_colliders_27(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_colliderCount_28(),
	GetNextOverlapArea2d_t3488004213::get_offset_of_nextColliderIndex_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (GetNextOverlapCircle2d_t2630547077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3460[15] = 
{
	GetNextOverlapCircle2d_t2630547077::get_offset_of_fromGameObject_14(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_fromPosition_15(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_radius_16(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_minDepth_17(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_maxDepth_18(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_resetFlag_19(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_layerMask_20(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_invertMask_21(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_collidersCount_22(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_storeNextCollider_23(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_loopEvent_24(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_finishedEvent_25(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_colliders_26(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_colliderCount_27(),
	GetNextOverlapCircle2d_t2630547077::get_offset_of_nextColliderIndex_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (GetNextOverlapPoint2d_t3527264863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[14] = 
{
	GetNextOverlapPoint2d_t3527264863::get_offset_of_gameObject_14(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_position_15(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_minDepth_16(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_maxDepth_17(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_resetFlag_18(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_layerMask_19(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_invertMask_20(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_collidersCount_21(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_storeNextCollider_22(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_loopEvent_23(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_finishedEvent_24(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_colliders_25(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_colliderCount_26(),
	GetNextOverlapPoint2d_t3527264863::get_offset_of_nextColliderIndex_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (GetNextRayCast2d_t2379810548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3462[21] = 
{
	GetNextRayCast2d_t2379810548::get_offset_of_fromGameObject_14(),
	GetNextRayCast2d_t2379810548::get_offset_of_fromPosition_15(),
	GetNextRayCast2d_t2379810548::get_offset_of_direction_16(),
	GetNextRayCast2d_t2379810548::get_offset_of_space_17(),
	GetNextRayCast2d_t2379810548::get_offset_of_distance_18(),
	GetNextRayCast2d_t2379810548::get_offset_of_minDepth_19(),
	GetNextRayCast2d_t2379810548::get_offset_of_maxDepth_20(),
	GetNextRayCast2d_t2379810548::get_offset_of_resetFlag_21(),
	GetNextRayCast2d_t2379810548::get_offset_of_layerMask_22(),
	GetNextRayCast2d_t2379810548::get_offset_of_invertMask_23(),
	GetNextRayCast2d_t2379810548::get_offset_of_collidersCount_24(),
	GetNextRayCast2d_t2379810548::get_offset_of_storeNextCollider_25(),
	GetNextRayCast2d_t2379810548::get_offset_of_storeNextHitPoint_26(),
	GetNextRayCast2d_t2379810548::get_offset_of_storeNextHitNormal_27(),
	GetNextRayCast2d_t2379810548::get_offset_of_storeNextHitDistance_28(),
	GetNextRayCast2d_t2379810548::get_offset_of_storeNextHitFraction_29(),
	GetNextRayCast2d_t2379810548::get_offset_of_loopEvent_30(),
	GetNextRayCast2d_t2379810548::get_offset_of_finishedEvent_31(),
	GetNextRayCast2d_t2379810548::get_offset_of_hits_32(),
	GetNextRayCast2d_t2379810548::get_offset_of_colliderCount_33(),
	GetNextRayCast2d_t2379810548::get_offset_of_nextColliderIndex_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (GetRayCastHit2dInfo_t543528974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3463[5] = 
{
	GetRayCastHit2dInfo_t543528974::get_offset_of_gameObjectHit_14(),
	GetRayCastHit2dInfo_t543528974::get_offset_of_point_15(),
	GetRayCastHit2dInfo_t543528974::get_offset_of_normal_16(),
	GetRayCastHit2dInfo_t543528974::get_offset_of_distance_17(),
	GetRayCastHit2dInfo_t543528974::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (GetSpeed2d_t2660198156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3464[3] = 
{
	GetSpeed2d_t2660198156::get_offset_of_gameObject_16(),
	GetSpeed2d_t2660198156::get_offset_of_storeResult_17(),
	GetSpeed2d_t2660198156::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (GetTrigger2dInfo_t2591660132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3465[3] = 
{
	GetTrigger2dInfo_t2591660132::get_offset_of_gameObjectHit_14(),
	GetTrigger2dInfo_t2591660132::get_offset_of_shapeCount_15(),
	GetTrigger2dInfo_t2591660132::get_offset_of_physics2dMaterialName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (GetVelocity2d_t1516202885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3466[6] = 
{
	GetVelocity2d_t1516202885::get_offset_of_gameObject_16(),
	GetVelocity2d_t1516202885::get_offset_of_vector_17(),
	GetVelocity2d_t1516202885::get_offset_of_x_18(),
	GetVelocity2d_t1516202885::get_offset_of_y_19(),
	GetVelocity2d_t1516202885::get_offset_of_space_20(),
	GetVelocity2d_t1516202885::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (IsFixedAngle2d_t1048615485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3467[5] = 
{
	IsFixedAngle2d_t1048615485::get_offset_of_gameObject_16(),
	IsFixedAngle2d_t1048615485::get_offset_of_trueEvent_17(),
	IsFixedAngle2d_t1048615485::get_offset_of_falseEvent_18(),
	IsFixedAngle2d_t1048615485::get_offset_of_store_19(),
	IsFixedAngle2d_t1048615485::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (IsKinematic2d_t3018647848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[5] = 
{
	IsKinematic2d_t3018647848::get_offset_of_gameObject_16(),
	IsKinematic2d_t3018647848::get_offset_of_trueEvent_17(),
	IsKinematic2d_t3018647848::get_offset_of_falseEvent_18(),
	IsKinematic2d_t3018647848::get_offset_of_store_19(),
	IsKinematic2d_t3018647848::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (IsSleeping2d_t2208738341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3469[5] = 
{
	IsSleeping2d_t2208738341::get_offset_of_gameObject_16(),
	IsSleeping2d_t2208738341::get_offset_of_trueEvent_17(),
	IsSleeping2d_t2208738341::get_offset_of_falseEvent_18(),
	IsSleeping2d_t2208738341::get_offset_of_store_19(),
	IsSleeping2d_t2208738341::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (LineCast2d_t30898162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3470[20] = 
{
	LineCast2d_t30898162::get_offset_of_fromGameObject_14(),
	LineCast2d_t30898162::get_offset_of_fromPosition_15(),
	LineCast2d_t30898162::get_offset_of_toGameObject_16(),
	LineCast2d_t30898162::get_offset_of_toPosition_17(),
	LineCast2d_t30898162::get_offset_of_minDepth_18(),
	LineCast2d_t30898162::get_offset_of_maxDepth_19(),
	LineCast2d_t30898162::get_offset_of_hitEvent_20(),
	LineCast2d_t30898162::get_offset_of_storeDidHit_21(),
	LineCast2d_t30898162::get_offset_of_storeHitObject_22(),
	LineCast2d_t30898162::get_offset_of_storeHitPoint_23(),
	LineCast2d_t30898162::get_offset_of_storeHitNormal_24(),
	LineCast2d_t30898162::get_offset_of_storeHitDistance_25(),
	LineCast2d_t30898162::get_offset_of_repeatInterval_26(),
	LineCast2d_t30898162::get_offset_of_layerMask_27(),
	LineCast2d_t30898162::get_offset_of_invertMask_28(),
	LineCast2d_t30898162::get_offset_of_debugColor_29(),
	LineCast2d_t30898162::get_offset_of_debug_30(),
	LineCast2d_t30898162::get_offset_of__fromTrans_31(),
	LineCast2d_t30898162::get_offset_of__toTrans_32(),
	LineCast2d_t30898162::get_offset_of_repeat_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (LookAt2d_t2942890213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3471[7] = 
{
	LookAt2d_t2942890213::get_offset_of_gameObject_14(),
	LookAt2d_t2942890213::get_offset_of_vector2Target_15(),
	LookAt2d_t2942890213::get_offset_of_vector3Target_16(),
	LookAt2d_t2942890213::get_offset_of_rotationOffset_17(),
	LookAt2d_t2942890213::get_offset_of_debug_18(),
	LookAt2d_t2942890213::get_offset_of_debugLineColor_19(),
	LookAt2d_t2942890213::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (LookAt2dGameObject_t1764891540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3472[8] = 
{
	LookAt2dGameObject_t1764891540::get_offset_of_gameObject_14(),
	LookAt2dGameObject_t1764891540::get_offset_of_targetObject_15(),
	LookAt2dGameObject_t1764891540::get_offset_of_rotationOffset_16(),
	LookAt2dGameObject_t1764891540::get_offset_of_debug_17(),
	LookAt2dGameObject_t1764891540::get_offset_of_debugLineColor_18(),
	LookAt2dGameObject_t1764891540::get_offset_of_everyFrame_19(),
	LookAt2dGameObject_t1764891540::get_offset_of_go_20(),
	LookAt2dGameObject_t1764891540::get_offset_of_goTarget_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (MousePick2d_t219204948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[6] = 
{
	MousePick2d_t219204948::get_offset_of_storeDidPickObject_14(),
	MousePick2d_t219204948::get_offset_of_storeGameObject_15(),
	MousePick2d_t219204948::get_offset_of_storePoint_16(),
	MousePick2d_t219204948::get_offset_of_layerMask_17(),
	MousePick2d_t219204948::get_offset_of_invertMask_18(),
	MousePick2d_t219204948::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (MousePick2dEvent_t4132667874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[8] = 
{
	MousePick2dEvent_t4132667874::get_offset_of_GameObject_14(),
	MousePick2dEvent_t4132667874::get_offset_of_mouseOver_15(),
	MousePick2dEvent_t4132667874::get_offset_of_mouseDown_16(),
	MousePick2dEvent_t4132667874::get_offset_of_mouseUp_17(),
	MousePick2dEvent_t4132667874::get_offset_of_mouseOff_18(),
	MousePick2dEvent_t4132667874::get_offset_of_layerMask_19(),
	MousePick2dEvent_t4132667874::get_offset_of_invertMask_20(),
	MousePick2dEvent_t4132667874::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (RayCast2d_t1611024692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3475[21] = 
{
	RayCast2d_t1611024692::get_offset_of_fromGameObject_14(),
	RayCast2d_t1611024692::get_offset_of_fromPosition_15(),
	RayCast2d_t1611024692::get_offset_of_direction_16(),
	RayCast2d_t1611024692::get_offset_of_space_17(),
	RayCast2d_t1611024692::get_offset_of_distance_18(),
	RayCast2d_t1611024692::get_offset_of_minDepth_19(),
	RayCast2d_t1611024692::get_offset_of_maxDepth_20(),
	RayCast2d_t1611024692::get_offset_of_hitEvent_21(),
	RayCast2d_t1611024692::get_offset_of_storeDidHit_22(),
	RayCast2d_t1611024692::get_offset_of_storeHitObject_23(),
	RayCast2d_t1611024692::get_offset_of_storeHitPoint_24(),
	RayCast2d_t1611024692::get_offset_of_storeHitNormal_25(),
	RayCast2d_t1611024692::get_offset_of_storeHitDistance_26(),
	RayCast2d_t1611024692::get_offset_of_storeHitFraction_27(),
	RayCast2d_t1611024692::get_offset_of_repeatInterval_28(),
	RayCast2d_t1611024692::get_offset_of_layerMask_29(),
	RayCast2d_t1611024692::get_offset_of_invertMask_30(),
	RayCast2d_t1611024692::get_offset_of_debugColor_31(),
	RayCast2d_t1611024692::get_offset_of_debug_32(),
	RayCast2d_t1611024692::get_offset_of__transform_33(),
	RayCast2d_t1611024692::get_offset_of_repeat_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (ScreenPick2d_t206632054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3476[10] = 
{
	ScreenPick2d_t206632054::get_offset_of_screenVector_14(),
	ScreenPick2d_t206632054::get_offset_of_screenX_15(),
	ScreenPick2d_t206632054::get_offset_of_screenY_16(),
	ScreenPick2d_t206632054::get_offset_of_normalized_17(),
	ScreenPick2d_t206632054::get_offset_of_storeDidPickObject_18(),
	ScreenPick2d_t206632054::get_offset_of_storeGameObject_19(),
	ScreenPick2d_t206632054::get_offset_of_storePoint_20(),
	ScreenPick2d_t206632054::get_offset_of_layerMask_21(),
	ScreenPick2d_t206632054::get_offset_of_invertMask_22(),
	ScreenPick2d_t206632054::get_offset_of_everyFrame_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (SetCollider2dIsTrigger_t819920189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3477[3] = 
{
	SetCollider2dIsTrigger_t819920189::get_offset_of_gameObject_14(),
	SetCollider2dIsTrigger_t819920189::get_offset_of_isTrigger_15(),
	SetCollider2dIsTrigger_t819920189::get_offset_of_setAllColliders_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (SetGravity2d_t1484030169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[4] = 
{
	SetGravity2d_t1484030169::get_offset_of_vector_14(),
	SetGravity2d_t1484030169::get_offset_of_x_15(),
	SetGravity2d_t1484030169::get_offset_of_y_16(),
	SetGravity2d_t1484030169::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (SetGravity2dScale_t3897070255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3479[2] = 
{
	SetGravity2dScale_t3897070255::get_offset_of_gameObject_16(),
	SetGravity2dScale_t3897070255::get_offset_of_gravityScale_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (SetHingeJoint2dProperties_t1483576128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[11] = 
{
	SetHingeJoint2dProperties_t1483576128::get_offset_of_gameObject_14(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_useLimits_15(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_min_16(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_max_17(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_useMotor_18(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_motorSpeed_19(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_maxMotorTorque_20(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of_everyFrame_21(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of__joint_22(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of__motor_23(),
	SetHingeJoint2dProperties_t1483576128::get_offset_of__limits_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (SetIsFixedAngle2d_t3710656154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[3] = 
{
	SetIsFixedAngle2d_t3710656154::get_offset_of_gameObject_16(),
	SetIsFixedAngle2d_t3710656154::get_offset_of_isFixedAngle_17(),
	SetIsFixedAngle2d_t3710656154::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (SetIsKinematic2d_t3305420780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3482[2] = 
{
	SetIsKinematic2d_t3305420780::get_offset_of_gameObject_16(),
	SetIsKinematic2d_t3305420780::get_offset_of_isKinematic_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (SetMass2d_t3302329633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[2] = 
{
	SetMass2d_t3302329633::get_offset_of_gameObject_16(),
	SetMass2d_t3302329633::get_offset_of_mass_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (SetVelocity2d_t520801329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[5] = 
{
	SetVelocity2d_t520801329::get_offset_of_gameObject_16(),
	SetVelocity2d_t520801329::get_offset_of_vector_17(),
	SetVelocity2d_t520801329::get_offset_of_x_18(),
	SetVelocity2d_t520801329::get_offset_of_y_19(),
	SetVelocity2d_t520801329::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (SetWheelJoint2dProperties_t2618454698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3485[11] = 
{
	SetWheelJoint2dProperties_t2618454698::get_offset_of_gameObject_14(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_useMotor_15(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_motorSpeed_16(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_maxMotorTorque_17(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_angle_18(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_dampingRatio_19(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_frequency_20(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of_everyFrame_21(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of__wj2d_22(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of__motor_23(),
	SetWheelJoint2dProperties_t2618454698::get_offset_of__suspension_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (Sleep2d_t2711291265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3486[1] = 
{
	Sleep2d_t2711291265::get_offset_of_gameObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (SmoothLookAt2d_t3829571255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3487[12] = 
{
	SmoothLookAt2d_t3829571255::get_offset_of_gameObject_14(),
	SmoothLookAt2d_t3829571255::get_offset_of_targetObject_15(),
	SmoothLookAt2d_t3829571255::get_offset_of_targetPosition2d_16(),
	SmoothLookAt2d_t3829571255::get_offset_of_targetPosition_17(),
	SmoothLookAt2d_t3829571255::get_offset_of_rotationOffset_18(),
	SmoothLookAt2d_t3829571255::get_offset_of_speed_19(),
	SmoothLookAt2d_t3829571255::get_offset_of_debug_20(),
	SmoothLookAt2d_t3829571255::get_offset_of_finishTolerance_21(),
	SmoothLookAt2d_t3829571255::get_offset_of_finishEvent_22(),
	SmoothLookAt2d_t3829571255::get_offset_of_previousGo_23(),
	SmoothLookAt2d_t3829571255::get_offset_of_lastRotation_24(),
	SmoothLookAt2d_t3829571255::get_offset_of_desiredRotation_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (TouchObject2dEvent_t1107737691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[9] = 
{
	TouchObject2dEvent_t1107737691::get_offset_of_gameObject_14(),
	TouchObject2dEvent_t1107737691::get_offset_of_fingerId_15(),
	TouchObject2dEvent_t1107737691::get_offset_of_touchBegan_16(),
	TouchObject2dEvent_t1107737691::get_offset_of_touchMoved_17(),
	TouchObject2dEvent_t1107737691::get_offset_of_touchStationary_18(),
	TouchObject2dEvent_t1107737691::get_offset_of_touchEnded_19(),
	TouchObject2dEvent_t1107737691::get_offset_of_touchCanceled_20(),
	TouchObject2dEvent_t1107737691::get_offset_of_storeFingerId_21(),
	TouchObject2dEvent_t1107737691::get_offset_of_storeHitPoint_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (Trigger2dEvent_t1735501411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3489[6] = 
{
	Trigger2dEvent_t1735501411::get_offset_of_gameObject_14(),
	Trigger2dEvent_t1735501411::get_offset_of_trigger_15(),
	Trigger2dEvent_t1735501411::get_offset_of_collideTag_16(),
	Trigger2dEvent_t1735501411::get_offset_of_sendEvent_17(),
	Trigger2dEvent_t1735501411::get_offset_of_storeCollider_18(),
	Trigger2dEvent_t1735501411::get_offset_of_cachedProxy_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (WakeAllRigidBodies2d_t384709002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[1] = 
{
	WakeAllRigidBodies2d_t384709002::get_offset_of_everyFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (WakeUp2d_t2180700450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3491[1] = 
{
	WakeUp2d_t2180700450::get_offset_of_gameObject_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (PlayerPrefsDeleteAll_t724049092), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (PlayerPrefsDeleteKey_t1869683915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3493[1] = 
{
	PlayerPrefsDeleteKey_t1869683915::get_offset_of_key_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (PlayerPrefsGetFloat_t2326825523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3494[2] = 
{
	PlayerPrefsGetFloat_t2326825523::get_offset_of_keys_14(),
	PlayerPrefsGetFloat_t2326825523::get_offset_of_variables_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (PlayerPrefsGetInt_t3361394777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3495[2] = 
{
	PlayerPrefsGetInt_t3361394777::get_offset_of_keys_14(),
	PlayerPrefsGetInt_t3361394777::get_offset_of_variables_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (PlayerPrefsGetString_t1146956221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[2] = 
{
	PlayerPrefsGetString_t1146956221::get_offset_of_keys_14(),
	PlayerPrefsGetString_t1146956221::get_offset_of_variables_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (PlayerPrefsHasKey_t4143475044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[4] = 
{
	PlayerPrefsHasKey_t4143475044::get_offset_of_key_14(),
	PlayerPrefsHasKey_t4143475044::get_offset_of_variable_15(),
	PlayerPrefsHasKey_t4143475044::get_offset_of_trueEvent_16(),
	PlayerPrefsHasKey_t4143475044::get_offset_of_falseEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (PlayerPrefsSetFloat_t1453099571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3498[2] = 
{
	PlayerPrefsSetFloat_t1453099571::get_offset_of_keys_14(),
	PlayerPrefsSetFloat_t1453099571::get_offset_of_values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (PlayerPrefsSetInt_t3334918233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3499[2] = 
{
	PlayerPrefsSetInt_t3334918233::get_offset_of_keys_14(),
	PlayerPrefsSetInt_t3334918233::get_offset_of_values_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
