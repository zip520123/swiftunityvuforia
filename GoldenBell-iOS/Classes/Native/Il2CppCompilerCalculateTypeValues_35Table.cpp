﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t2057874452;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t4259038327;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t3649179877;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.AsyncOperation>
struct Dictionary_2_t333745174;
// System.String
struct String_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef UPDATETYPE_T4176746407_H
#define UPDATETYPE_T4176746407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseUpdateAction/UpdateType
struct  UpdateType_t4176746407 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.BaseUpdateAction/UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t4176746407, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T4176746407_H
#ifndef SCENEALLREFERENCEOPTIONS_T3801934101_H
#define SCENEALLREFERENCEOPTIONS_T3801934101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneAllReferenceOptions
struct  SceneAllReferenceOptions_t3801934101 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneAllReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneAllReferenceOptions_t3801934101, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEALLREFERENCEOPTIONS_T3801934101_H
#ifndef SCENEBUILDREFERENCEOPTIONS_T2990954388_H
#define SCENEBUILDREFERENCEOPTIONS_T2990954388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneBuildReferenceOptions
struct  SceneBuildReferenceOptions_t2990954388 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneBuildReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneBuildReferenceOptions_t2990954388, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEBUILDREFERENCEOPTIONS_T2990954388_H
#ifndef SCENEREFERENCEOPTIONS_T3051583257_H
#define SCENEREFERENCEOPTIONS_T3051583257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneReferenceOptions
struct  SceneReferenceOptions_t3051583257 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneReferenceOptions_t3051583257, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEREFERENCEOPTIONS_T3051583257_H
#ifndef SCENESIMPLEREFERENCEOPTIONS_T925546987_H
#define SCENESIMPLEREFERENCEOPTIONS_T925546987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneSimpleReferenceOptions
struct  SceneSimpleReferenceOptions_t925546987 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneSimpleReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneSimpleReferenceOptions_t925546987, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENESIMPLEREFERENCEOPTIONS_T925546987_H
#ifndef EVERYFRAMEOPTIONS_T3183592540_H
#define EVERYFRAMEOPTIONS_T3183592540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionBaseAction/everyFrameOptions
struct  everyFrameOptions_t3183592540 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.QuaternionBaseAction/everyFrameOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(everyFrameOptions_t3183592540, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVERYFRAMEOPTIONS_T3183592540_H
#ifndef RECTTRANSFORMFLIPOPTIONS_T3615818486_H
#define RECTTRANSFORMFLIPOPTIONS_T3615818486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis/RectTransformFlipOptions
struct  RectTransformFlipOptions_t3615818486 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis/RectTransformFlipOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RectTransformFlipOptions_t3615818486, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMFLIPOPTIONS_T3615818486_H
#ifndef LOCALPOSITIONREFERENCE_T3695602212_H
#define LOCALPOSITIONREFERENCE_T3695602212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition/LocalPositionReference
struct  LocalPositionReference_t3695602212 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition/LocalPositionReference::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LocalPositionReference_t3695602212, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALPOSITIONREFERENCE_T3695602212_H
#ifndef ANCHORREFERENCE_T1326731945_H
#define ANCHORREFERENCE_T1326731945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition/AnchorReference
struct  AnchorReference_t1326731945 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition/AnchorReference::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorReference_t1326731945, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORREFERENCE_T1326731945_H
#ifndef SCENEREFERENCEOPTIONS_T2424404743_H
#define SCENEREFERENCEOPTIONS_T2424404743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetActiveScene/SceneReferenceOptions
struct  SceneReferenceOptions_t2424404743 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.SetActiveScene/SceneReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneReferenceOptions_t2424404743, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEREFERENCEOPTIONS_T2424404743_H
#ifndef SCENEREFERENCEOPTIONS_T3425810120_H
#define SCENEREFERENCEOPTIONS_T3425810120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UnloadScene/SceneReferenceOptions
struct  SceneReferenceOptions_t3425810120 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.UnloadScene/SceneReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneReferenceOptions_t3425810120, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEREFERENCEOPTIONS_T3425810120_H
#ifndef SCENEREFERENCEOPTIONS_T108674727_H
#define SCENEREFERENCEOPTIONS_T108674727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UnloadSceneAsynch/SceneReferenceOptions
struct  SceneReferenceOptions_t108674727 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.UnloadSceneAsynch/SceneReferenceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SceneReferenceOptions_t108674727, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEREFERENCEOPTIONS_T108674727_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef LOADSCENEMODE_T3251202195_H
#define LOADSCENEMODE_T3251202195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_t3251202195 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadSceneMode_t3251202195, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEMODE_T3251202195_H
#ifndef ALLOWSCENEACTIVATION_T2067239854_H
#define ALLOWSCENEACTIVATION_T2067239854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AllowSceneActivation
struct  AllowSceneActivation_t2067239854  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.AllowSceneActivation::aSynchOperationHashCode
	FsmInt_t874273141 * ___aSynchOperationHashCode_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AllowSceneActivation::allowSceneActivation
	FsmBool_t163807967 * ___allowSceneActivation_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.AllowSceneActivation::progress
	FsmFloat_t2883254149 * ___progress_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AllowSceneActivation::isDone
	FsmBool_t163807967 * ___isDone_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AllowSceneActivation::doneEvent
	FsmEvent_t3736299882 * ___doneEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AllowSceneActivation::failureEvent
	FsmEvent_t3736299882 * ___failureEvent_19;

public:
	inline static int32_t get_offset_of_aSynchOperationHashCode_14() { return static_cast<int32_t>(offsetof(AllowSceneActivation_t2067239854, ___aSynchOperationHashCode_14)); }
	inline FsmInt_t874273141 * get_aSynchOperationHashCode_14() const { return ___aSynchOperationHashCode_14; }
	inline FsmInt_t874273141 ** get_address_of_aSynchOperationHashCode_14() { return &___aSynchOperationHashCode_14; }
	inline void set_aSynchOperationHashCode_14(FsmInt_t874273141 * value)
	{
		___aSynchOperationHashCode_14 = value;
		Il2CppCodeGenWriteBarrier((&___aSynchOperationHashCode_14), value);
	}

	inline static int32_t get_offset_of_allowSceneActivation_15() { return static_cast<int32_t>(offsetof(AllowSceneActivation_t2067239854, ___allowSceneActivation_15)); }
	inline FsmBool_t163807967 * get_allowSceneActivation_15() const { return ___allowSceneActivation_15; }
	inline FsmBool_t163807967 ** get_address_of_allowSceneActivation_15() { return &___allowSceneActivation_15; }
	inline void set_allowSceneActivation_15(FsmBool_t163807967 * value)
	{
		___allowSceneActivation_15 = value;
		Il2CppCodeGenWriteBarrier((&___allowSceneActivation_15), value);
	}

	inline static int32_t get_offset_of_progress_16() { return static_cast<int32_t>(offsetof(AllowSceneActivation_t2067239854, ___progress_16)); }
	inline FsmFloat_t2883254149 * get_progress_16() const { return ___progress_16; }
	inline FsmFloat_t2883254149 ** get_address_of_progress_16() { return &___progress_16; }
	inline void set_progress_16(FsmFloat_t2883254149 * value)
	{
		___progress_16 = value;
		Il2CppCodeGenWriteBarrier((&___progress_16), value);
	}

	inline static int32_t get_offset_of_isDone_17() { return static_cast<int32_t>(offsetof(AllowSceneActivation_t2067239854, ___isDone_17)); }
	inline FsmBool_t163807967 * get_isDone_17() const { return ___isDone_17; }
	inline FsmBool_t163807967 ** get_address_of_isDone_17() { return &___isDone_17; }
	inline void set_isDone_17(FsmBool_t163807967 * value)
	{
		___isDone_17 = value;
		Il2CppCodeGenWriteBarrier((&___isDone_17), value);
	}

	inline static int32_t get_offset_of_doneEvent_18() { return static_cast<int32_t>(offsetof(AllowSceneActivation_t2067239854, ___doneEvent_18)); }
	inline FsmEvent_t3736299882 * get_doneEvent_18() const { return ___doneEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_doneEvent_18() { return &___doneEvent_18; }
	inline void set_doneEvent_18(FsmEvent_t3736299882 * value)
	{
		___doneEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___doneEvent_18), value);
	}

	inline static int32_t get_offset_of_failureEvent_19() { return static_cast<int32_t>(offsetof(AllowSceneActivation_t2067239854, ___failureEvent_19)); }
	inline FsmEvent_t3736299882 * get_failureEvent_19() const { return ___failureEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_failureEvent_19() { return &___failureEvent_19; }
	inline void set_failureEvent_19(FsmEvent_t3736299882 * value)
	{
		___failureEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___failureEvent_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOWSCENEACTIVATION_T2067239854_H
#ifndef BASEUPDATEACTION_T497653765_H
#define BASEUPDATEACTION_T497653765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseUpdateAction
struct  BaseUpdateAction_t497653765  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.BaseUpdateAction::everyFrame
	bool ___everyFrame_14;
	// HutongGames.PlayMaker.Actions.BaseUpdateAction/UpdateType HutongGames.PlayMaker.Actions.BaseUpdateAction::updateType
	int32_t ___updateType_15;

public:
	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(BaseUpdateAction_t497653765, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_updateType_15() { return static_cast<int32_t>(offsetof(BaseUpdateAction_t497653765, ___updateType_15)); }
	inline int32_t get_updateType_15() const { return ___updateType_15; }
	inline int32_t* get_address_of_updateType_15() { return &___updateType_15; }
	inline void set_updateType_15(int32_t value)
	{
		___updateType_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEUPDATEACTION_T497653765_H
#ifndef CREATESCENE_T1317922373_H
#define CREATESCENE_T1317922373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CreateScene
struct  CreateScene_t1317922373  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CreateScene::sceneName
	FsmString_t1785915204 * ___sceneName_14;

public:
	inline static int32_t get_offset_of_sceneName_14() { return static_cast<int32_t>(offsetof(CreateScene_t1317922373, ___sceneName_14)); }
	inline FsmString_t1785915204 * get_sceneName_14() const { return ___sceneName_14; }
	inline FsmString_t1785915204 ** get_address_of_sceneName_14() { return &___sceneName_14; }
	inline void set_sceneName_14(FsmString_t1785915204 * value)
	{
		___sceneName_14 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATESCENE_T1317922373_H
#ifndef ENABLEFOG_T1758211407_H
#define ENABLEFOG_T1758211407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableFog
struct  EnableFog_t1758211407  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableFog::enableFog
	FsmBool_t163807967 * ___enableFog_14;
	// System.Boolean HutongGames.PlayMaker.Actions.EnableFog::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_enableFog_14() { return static_cast<int32_t>(offsetof(EnableFog_t1758211407, ___enableFog_14)); }
	inline FsmBool_t163807967 * get_enableFog_14() const { return ___enableFog_14; }
	inline FsmBool_t163807967 ** get_address_of_enableFog_14() { return &___enableFog_14; }
	inline void set_enableFog_14(FsmBool_t163807967 * value)
	{
		___enableFog_14 = value;
		Il2CppCodeGenWriteBarrier((&___enableFog_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(EnableFog_t1758211407, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEFOG_T1758211407_H
#ifndef GETRECTFIELDS_T2150919948_H
#define GETRECTFIELDS_T2150919948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRectFields
struct  GetRectFields_t2150919948  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GetRectFields::rectVariable
	FsmRect_t3649179877 * ___rectVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeX
	FsmFloat_t2883254149 * ___storeX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeY
	FsmFloat_t2883254149 * ___storeY_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeWidth
	FsmFloat_t2883254149 * ___storeWidth_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRectFields::storeHeight
	FsmFloat_t2883254149 * ___storeHeight_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRectFields::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_rectVariable_14() { return static_cast<int32_t>(offsetof(GetRectFields_t2150919948, ___rectVariable_14)); }
	inline FsmRect_t3649179877 * get_rectVariable_14() const { return ___rectVariable_14; }
	inline FsmRect_t3649179877 ** get_address_of_rectVariable_14() { return &___rectVariable_14; }
	inline void set_rectVariable_14(FsmRect_t3649179877 * value)
	{
		___rectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariable_14), value);
	}

	inline static int32_t get_offset_of_storeX_15() { return static_cast<int32_t>(offsetof(GetRectFields_t2150919948, ___storeX_15)); }
	inline FsmFloat_t2883254149 * get_storeX_15() const { return ___storeX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeX_15() { return &___storeX_15; }
	inline void set_storeX_15(FsmFloat_t2883254149 * value)
	{
		___storeX_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeX_15), value);
	}

	inline static int32_t get_offset_of_storeY_16() { return static_cast<int32_t>(offsetof(GetRectFields_t2150919948, ___storeY_16)); }
	inline FsmFloat_t2883254149 * get_storeY_16() const { return ___storeY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_storeY_16() { return &___storeY_16; }
	inline void set_storeY_16(FsmFloat_t2883254149 * value)
	{
		___storeY_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeY_16), value);
	}

	inline static int32_t get_offset_of_storeWidth_17() { return static_cast<int32_t>(offsetof(GetRectFields_t2150919948, ___storeWidth_17)); }
	inline FsmFloat_t2883254149 * get_storeWidth_17() const { return ___storeWidth_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeWidth_17() { return &___storeWidth_17; }
	inline void set_storeWidth_17(FsmFloat_t2883254149 * value)
	{
		___storeWidth_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeWidth_17), value);
	}

	inline static int32_t get_offset_of_storeHeight_18() { return static_cast<int32_t>(offsetof(GetRectFields_t2150919948, ___storeHeight_18)); }
	inline FsmFloat_t2883254149 * get_storeHeight_18() const { return ___storeHeight_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeHeight_18() { return &___storeHeight_18; }
	inline void set_storeHeight_18(FsmFloat_t2883254149 * value)
	{
		___storeHeight_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeHeight_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GetRectFields_t2150919948, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRECTFIELDS_T2150919948_H
#ifndef GETSCENEACTIONBASE_T2551360596_H
#define GETSCENEACTIONBASE_T2551360596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActionBase
struct  GetSceneActionBase_t2551360596  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneAllReferenceOptions HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneReference
	int32_t ___sceneReference_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneAtIndex
	FsmInt_t874273141 * ___sceneAtIndex_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneByName
	FsmString_t1785915204 * ___sceneByName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneByPath
	FsmString_t1785915204 * ___sceneByPath_17;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneByGameObject
	FsmOwnerDefault_t3590610434 * ___sceneByGameObject_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneFound
	FsmBool_t163807967 * ___sceneFound_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneFoundEvent
	FsmEvent_t3736299882 * ___sceneFoundEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneActionBase::sceneNotFoundEvent
	FsmEvent_t3736299882 * ___sceneNotFoundEvent_21;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.GetSceneActionBase::_scene
	Scene_t2348375561  ____scene_22;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneActionBase::_sceneFound
	bool ____sceneFound_23;

public:
	inline static int32_t get_offset_of_sceneReference_14() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneReference_14)); }
	inline int32_t get_sceneReference_14() const { return ___sceneReference_14; }
	inline int32_t* get_address_of_sceneReference_14() { return &___sceneReference_14; }
	inline void set_sceneReference_14(int32_t value)
	{
		___sceneReference_14 = value;
	}

	inline static int32_t get_offset_of_sceneAtIndex_15() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneAtIndex_15)); }
	inline FsmInt_t874273141 * get_sceneAtIndex_15() const { return ___sceneAtIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtIndex_15() { return &___sceneAtIndex_15; }
	inline void set_sceneAtIndex_15(FsmInt_t874273141 * value)
	{
		___sceneAtIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtIndex_15), value);
	}

	inline static int32_t get_offset_of_sceneByName_16() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneByName_16)); }
	inline FsmString_t1785915204 * get_sceneByName_16() const { return ___sceneByName_16; }
	inline FsmString_t1785915204 ** get_address_of_sceneByName_16() { return &___sceneByName_16; }
	inline void set_sceneByName_16(FsmString_t1785915204 * value)
	{
		___sceneByName_16 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByName_16), value);
	}

	inline static int32_t get_offset_of_sceneByPath_17() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneByPath_17)); }
	inline FsmString_t1785915204 * get_sceneByPath_17() const { return ___sceneByPath_17; }
	inline FsmString_t1785915204 ** get_address_of_sceneByPath_17() { return &___sceneByPath_17; }
	inline void set_sceneByPath_17(FsmString_t1785915204 * value)
	{
		___sceneByPath_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByPath_17), value);
	}

	inline static int32_t get_offset_of_sceneByGameObject_18() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneByGameObject_18)); }
	inline FsmOwnerDefault_t3590610434 * get_sceneByGameObject_18() const { return ___sceneByGameObject_18; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_sceneByGameObject_18() { return &___sceneByGameObject_18; }
	inline void set_sceneByGameObject_18(FsmOwnerDefault_t3590610434 * value)
	{
		___sceneByGameObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByGameObject_18), value);
	}

	inline static int32_t get_offset_of_sceneFound_19() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneFound_19)); }
	inline FsmBool_t163807967 * get_sceneFound_19() const { return ___sceneFound_19; }
	inline FsmBool_t163807967 ** get_address_of_sceneFound_19() { return &___sceneFound_19; }
	inline void set_sceneFound_19(FsmBool_t163807967 * value)
	{
		___sceneFound_19 = value;
		Il2CppCodeGenWriteBarrier((&___sceneFound_19), value);
	}

	inline static int32_t get_offset_of_sceneFoundEvent_20() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneFoundEvent_20)); }
	inline FsmEvent_t3736299882 * get_sceneFoundEvent_20() const { return ___sceneFoundEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneFoundEvent_20() { return &___sceneFoundEvent_20; }
	inline void set_sceneFoundEvent_20(FsmEvent_t3736299882 * value)
	{
		___sceneFoundEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___sceneFoundEvent_20), value);
	}

	inline static int32_t get_offset_of_sceneNotFoundEvent_21() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ___sceneNotFoundEvent_21)); }
	inline FsmEvent_t3736299882 * get_sceneNotFoundEvent_21() const { return ___sceneNotFoundEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneNotFoundEvent_21() { return &___sceneNotFoundEvent_21; }
	inline void set_sceneNotFoundEvent_21(FsmEvent_t3736299882 * value)
	{
		___sceneNotFoundEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___sceneNotFoundEvent_21), value);
	}

	inline static int32_t get_offset_of__scene_22() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ____scene_22)); }
	inline Scene_t2348375561  get__scene_22() const { return ____scene_22; }
	inline Scene_t2348375561 * get_address_of__scene_22() { return &____scene_22; }
	inline void set__scene_22(Scene_t2348375561  value)
	{
		____scene_22 = value;
	}

	inline static int32_t get_offset_of__sceneFound_23() { return static_cast<int32_t>(offsetof(GetSceneActionBase_t2551360596, ____sceneFound_23)); }
	inline bool get__sceneFound_23() const { return ____sceneFound_23; }
	inline bool* get_address_of__sceneFound_23() { return &____sceneFound_23; }
	inline void set__sceneFound_23(bool value)
	{
		____sceneFound_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEACTIONBASE_T2551360596_H
#ifndef GETSCENEACTIVATECHANGEDEVENTDATA_T3243756110_H
#define GETSCENEACTIVATECHANGEDEVENTDATA_T3243756110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData
struct  GetSceneActivateChangedEventData_t3243756110  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newName
	FsmString_t1785915204 * ___newName_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newPath
	FsmString_t1785915204 * ___newPath_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newIsValid
	FsmBool_t163807967 * ___newIsValid_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newBuildIndex
	FsmInt_t874273141 * ___newBuildIndex_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newIsLoaded
	FsmBool_t163807967 * ___newIsLoaded_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newIsDirty
	FsmBool_t163807967 * ___newIsDirty_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newRootCount
	FsmInt_t874273141 * ___newRootCount_20;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newRootGameObjects
	FsmArray_t1756862219 * ___newRootGameObjects_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousName
	FsmString_t1785915204 * ___previousName_22;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousPath
	FsmString_t1785915204 * ___previousPath_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousIsValid
	FsmBool_t163807967 * ___previousIsValid_24;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousBuildIndex
	FsmInt_t874273141 * ___previousBuildIndex_25;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousIsLoaded
	FsmBool_t163807967 * ___previousIsLoaded_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousIsDirty
	FsmBool_t163807967 * ___previousIsDirty_27;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousRootCount
	FsmInt_t874273141 * ___previousRootCount_28;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousRootGameObjects
	FsmArray_t1756862219 * ___previousRootGameObjects_29;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::_scene
	Scene_t2348375561  ____scene_30;

public:
	inline static int32_t get_offset_of_newName_14() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newName_14)); }
	inline FsmString_t1785915204 * get_newName_14() const { return ___newName_14; }
	inline FsmString_t1785915204 ** get_address_of_newName_14() { return &___newName_14; }
	inline void set_newName_14(FsmString_t1785915204 * value)
	{
		___newName_14 = value;
		Il2CppCodeGenWriteBarrier((&___newName_14), value);
	}

	inline static int32_t get_offset_of_newPath_15() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newPath_15)); }
	inline FsmString_t1785915204 * get_newPath_15() const { return ___newPath_15; }
	inline FsmString_t1785915204 ** get_address_of_newPath_15() { return &___newPath_15; }
	inline void set_newPath_15(FsmString_t1785915204 * value)
	{
		___newPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___newPath_15), value);
	}

	inline static int32_t get_offset_of_newIsValid_16() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newIsValid_16)); }
	inline FsmBool_t163807967 * get_newIsValid_16() const { return ___newIsValid_16; }
	inline FsmBool_t163807967 ** get_address_of_newIsValid_16() { return &___newIsValid_16; }
	inline void set_newIsValid_16(FsmBool_t163807967 * value)
	{
		___newIsValid_16 = value;
		Il2CppCodeGenWriteBarrier((&___newIsValid_16), value);
	}

	inline static int32_t get_offset_of_newBuildIndex_17() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newBuildIndex_17)); }
	inline FsmInt_t874273141 * get_newBuildIndex_17() const { return ___newBuildIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_newBuildIndex_17() { return &___newBuildIndex_17; }
	inline void set_newBuildIndex_17(FsmInt_t874273141 * value)
	{
		___newBuildIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___newBuildIndex_17), value);
	}

	inline static int32_t get_offset_of_newIsLoaded_18() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newIsLoaded_18)); }
	inline FsmBool_t163807967 * get_newIsLoaded_18() const { return ___newIsLoaded_18; }
	inline FsmBool_t163807967 ** get_address_of_newIsLoaded_18() { return &___newIsLoaded_18; }
	inline void set_newIsLoaded_18(FsmBool_t163807967 * value)
	{
		___newIsLoaded_18 = value;
		Il2CppCodeGenWriteBarrier((&___newIsLoaded_18), value);
	}

	inline static int32_t get_offset_of_newIsDirty_19() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newIsDirty_19)); }
	inline FsmBool_t163807967 * get_newIsDirty_19() const { return ___newIsDirty_19; }
	inline FsmBool_t163807967 ** get_address_of_newIsDirty_19() { return &___newIsDirty_19; }
	inline void set_newIsDirty_19(FsmBool_t163807967 * value)
	{
		___newIsDirty_19 = value;
		Il2CppCodeGenWriteBarrier((&___newIsDirty_19), value);
	}

	inline static int32_t get_offset_of_newRootCount_20() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newRootCount_20)); }
	inline FsmInt_t874273141 * get_newRootCount_20() const { return ___newRootCount_20; }
	inline FsmInt_t874273141 ** get_address_of_newRootCount_20() { return &___newRootCount_20; }
	inline void set_newRootCount_20(FsmInt_t874273141 * value)
	{
		___newRootCount_20 = value;
		Il2CppCodeGenWriteBarrier((&___newRootCount_20), value);
	}

	inline static int32_t get_offset_of_newRootGameObjects_21() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___newRootGameObjects_21)); }
	inline FsmArray_t1756862219 * get_newRootGameObjects_21() const { return ___newRootGameObjects_21; }
	inline FsmArray_t1756862219 ** get_address_of_newRootGameObjects_21() { return &___newRootGameObjects_21; }
	inline void set_newRootGameObjects_21(FsmArray_t1756862219 * value)
	{
		___newRootGameObjects_21 = value;
		Il2CppCodeGenWriteBarrier((&___newRootGameObjects_21), value);
	}

	inline static int32_t get_offset_of_previousName_22() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousName_22)); }
	inline FsmString_t1785915204 * get_previousName_22() const { return ___previousName_22; }
	inline FsmString_t1785915204 ** get_address_of_previousName_22() { return &___previousName_22; }
	inline void set_previousName_22(FsmString_t1785915204 * value)
	{
		___previousName_22 = value;
		Il2CppCodeGenWriteBarrier((&___previousName_22), value);
	}

	inline static int32_t get_offset_of_previousPath_23() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousPath_23)); }
	inline FsmString_t1785915204 * get_previousPath_23() const { return ___previousPath_23; }
	inline FsmString_t1785915204 ** get_address_of_previousPath_23() { return &___previousPath_23; }
	inline void set_previousPath_23(FsmString_t1785915204 * value)
	{
		___previousPath_23 = value;
		Il2CppCodeGenWriteBarrier((&___previousPath_23), value);
	}

	inline static int32_t get_offset_of_previousIsValid_24() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousIsValid_24)); }
	inline FsmBool_t163807967 * get_previousIsValid_24() const { return ___previousIsValid_24; }
	inline FsmBool_t163807967 ** get_address_of_previousIsValid_24() { return &___previousIsValid_24; }
	inline void set_previousIsValid_24(FsmBool_t163807967 * value)
	{
		___previousIsValid_24 = value;
		Il2CppCodeGenWriteBarrier((&___previousIsValid_24), value);
	}

	inline static int32_t get_offset_of_previousBuildIndex_25() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousBuildIndex_25)); }
	inline FsmInt_t874273141 * get_previousBuildIndex_25() const { return ___previousBuildIndex_25; }
	inline FsmInt_t874273141 ** get_address_of_previousBuildIndex_25() { return &___previousBuildIndex_25; }
	inline void set_previousBuildIndex_25(FsmInt_t874273141 * value)
	{
		___previousBuildIndex_25 = value;
		Il2CppCodeGenWriteBarrier((&___previousBuildIndex_25), value);
	}

	inline static int32_t get_offset_of_previousIsLoaded_26() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousIsLoaded_26)); }
	inline FsmBool_t163807967 * get_previousIsLoaded_26() const { return ___previousIsLoaded_26; }
	inline FsmBool_t163807967 ** get_address_of_previousIsLoaded_26() { return &___previousIsLoaded_26; }
	inline void set_previousIsLoaded_26(FsmBool_t163807967 * value)
	{
		___previousIsLoaded_26 = value;
		Il2CppCodeGenWriteBarrier((&___previousIsLoaded_26), value);
	}

	inline static int32_t get_offset_of_previousIsDirty_27() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousIsDirty_27)); }
	inline FsmBool_t163807967 * get_previousIsDirty_27() const { return ___previousIsDirty_27; }
	inline FsmBool_t163807967 ** get_address_of_previousIsDirty_27() { return &___previousIsDirty_27; }
	inline void set_previousIsDirty_27(FsmBool_t163807967 * value)
	{
		___previousIsDirty_27 = value;
		Il2CppCodeGenWriteBarrier((&___previousIsDirty_27), value);
	}

	inline static int32_t get_offset_of_previousRootCount_28() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousRootCount_28)); }
	inline FsmInt_t874273141 * get_previousRootCount_28() const { return ___previousRootCount_28; }
	inline FsmInt_t874273141 ** get_address_of_previousRootCount_28() { return &___previousRootCount_28; }
	inline void set_previousRootCount_28(FsmInt_t874273141 * value)
	{
		___previousRootCount_28 = value;
		Il2CppCodeGenWriteBarrier((&___previousRootCount_28), value);
	}

	inline static int32_t get_offset_of_previousRootGameObjects_29() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ___previousRootGameObjects_29)); }
	inline FsmArray_t1756862219 * get_previousRootGameObjects_29() const { return ___previousRootGameObjects_29; }
	inline FsmArray_t1756862219 ** get_address_of_previousRootGameObjects_29() { return &___previousRootGameObjects_29; }
	inline void set_previousRootGameObjects_29(FsmArray_t1756862219 * value)
	{
		___previousRootGameObjects_29 = value;
		Il2CppCodeGenWriteBarrier((&___previousRootGameObjects_29), value);
	}

	inline static int32_t get_offset_of__scene_30() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t3243756110, ____scene_30)); }
	inline Scene_t2348375561  get__scene_30() const { return ____scene_30; }
	inline Scene_t2348375561 * get_address_of__scene_30() { return &____scene_30; }
	inline void set__scene_30(Scene_t2348375561  value)
	{
		____scene_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEACTIVATECHANGEDEVENTDATA_T3243756110_H
#ifndef GETSCENECOUNT_T4210588623_H
#define GETSCENECOUNT_T4210588623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneCount
struct  GetSceneCount_t4210588623  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneCount::sceneCount
	FsmInt_t874273141 * ___sceneCount_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneCount::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_sceneCount_14() { return static_cast<int32_t>(offsetof(GetSceneCount_t4210588623, ___sceneCount_14)); }
	inline FsmInt_t874273141 * get_sceneCount_14() const { return ___sceneCount_14; }
	inline FsmInt_t874273141 ** get_address_of_sceneCount_14() { return &___sceneCount_14; }
	inline void set_sceneCount_14(FsmInt_t874273141 * value)
	{
		___sceneCount_14 = value;
		Il2CppCodeGenWriteBarrier((&___sceneCount_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(GetSceneCount_t4210588623, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENECOUNT_T4210588623_H
#ifndef GETSCENECOUNTINBUILDSETTINGS_T1563283541_H
#define GETSCENECOUNTINBUILDSETTINGS_T1563283541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneCountInBuildSettings
struct  GetSceneCountInBuildSettings_t1563283541  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneCountInBuildSettings::sceneCountInBuildSettings
	FsmInt_t874273141 * ___sceneCountInBuildSettings_14;

public:
	inline static int32_t get_offset_of_sceneCountInBuildSettings_14() { return static_cast<int32_t>(offsetof(GetSceneCountInBuildSettings_t1563283541, ___sceneCountInBuildSettings_14)); }
	inline FsmInt_t874273141 * get_sceneCountInBuildSettings_14() const { return ___sceneCountInBuildSettings_14; }
	inline FsmInt_t874273141 ** get_address_of_sceneCountInBuildSettings_14() { return &___sceneCountInBuildSettings_14; }
	inline void set_sceneCountInBuildSettings_14(FsmInt_t874273141 * value)
	{
		___sceneCountInBuildSettings_14 = value;
		Il2CppCodeGenWriteBarrier((&___sceneCountInBuildSettings_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENECOUNTINBUILDSETTINGS_T1563283541_H
#ifndef GETSCENELOADEDEVENTDATA_T1535803245_H
#define GETSCENELOADEDEVENTDATA_T1535803245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneLoadedEventData
struct  GetSceneLoadedEventData_t1535803245  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::loadedMode
	FsmEnum_t2861764163 * ___loadedMode_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::name
	FsmString_t1785915204 * ___name_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::path
	FsmString_t1785915204 * ___path_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::isValid
	FsmBool_t163807967 * ___isValid_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::buildIndex
	FsmInt_t874273141 * ___buildIndex_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::isLoaded
	FsmBool_t163807967 * ___isLoaded_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::isDirty
	FsmBool_t163807967 * ___isDirty_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::rootCount
	FsmInt_t874273141 * ___rootCount_21;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::rootGameObjects
	FsmArray_t1756862219 * ___rootGameObjects_22;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.GetSceneLoadedEventData::_scene
	Scene_t2348375561  ____scene_23;

public:
	inline static int32_t get_offset_of_loadedMode_14() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___loadedMode_14)); }
	inline FsmEnum_t2861764163 * get_loadedMode_14() const { return ___loadedMode_14; }
	inline FsmEnum_t2861764163 ** get_address_of_loadedMode_14() { return &___loadedMode_14; }
	inline void set_loadedMode_14(FsmEnum_t2861764163 * value)
	{
		___loadedMode_14 = value;
		Il2CppCodeGenWriteBarrier((&___loadedMode_14), value);
	}

	inline static int32_t get_offset_of_name_15() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___name_15)); }
	inline FsmString_t1785915204 * get_name_15() const { return ___name_15; }
	inline FsmString_t1785915204 ** get_address_of_name_15() { return &___name_15; }
	inline void set_name_15(FsmString_t1785915204 * value)
	{
		___name_15 = value;
		Il2CppCodeGenWriteBarrier((&___name_15), value);
	}

	inline static int32_t get_offset_of_path_16() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___path_16)); }
	inline FsmString_t1785915204 * get_path_16() const { return ___path_16; }
	inline FsmString_t1785915204 ** get_address_of_path_16() { return &___path_16; }
	inline void set_path_16(FsmString_t1785915204 * value)
	{
		___path_16 = value;
		Il2CppCodeGenWriteBarrier((&___path_16), value);
	}

	inline static int32_t get_offset_of_isValid_17() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___isValid_17)); }
	inline FsmBool_t163807967 * get_isValid_17() const { return ___isValid_17; }
	inline FsmBool_t163807967 ** get_address_of_isValid_17() { return &___isValid_17; }
	inline void set_isValid_17(FsmBool_t163807967 * value)
	{
		___isValid_17 = value;
		Il2CppCodeGenWriteBarrier((&___isValid_17), value);
	}

	inline static int32_t get_offset_of_buildIndex_18() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___buildIndex_18)); }
	inline FsmInt_t874273141 * get_buildIndex_18() const { return ___buildIndex_18; }
	inline FsmInt_t874273141 ** get_address_of_buildIndex_18() { return &___buildIndex_18; }
	inline void set_buildIndex_18(FsmInt_t874273141 * value)
	{
		___buildIndex_18 = value;
		Il2CppCodeGenWriteBarrier((&___buildIndex_18), value);
	}

	inline static int32_t get_offset_of_isLoaded_19() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___isLoaded_19)); }
	inline FsmBool_t163807967 * get_isLoaded_19() const { return ___isLoaded_19; }
	inline FsmBool_t163807967 ** get_address_of_isLoaded_19() { return &___isLoaded_19; }
	inline void set_isLoaded_19(FsmBool_t163807967 * value)
	{
		___isLoaded_19 = value;
		Il2CppCodeGenWriteBarrier((&___isLoaded_19), value);
	}

	inline static int32_t get_offset_of_isDirty_20() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___isDirty_20)); }
	inline FsmBool_t163807967 * get_isDirty_20() const { return ___isDirty_20; }
	inline FsmBool_t163807967 ** get_address_of_isDirty_20() { return &___isDirty_20; }
	inline void set_isDirty_20(FsmBool_t163807967 * value)
	{
		___isDirty_20 = value;
		Il2CppCodeGenWriteBarrier((&___isDirty_20), value);
	}

	inline static int32_t get_offset_of_rootCount_21() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___rootCount_21)); }
	inline FsmInt_t874273141 * get_rootCount_21() const { return ___rootCount_21; }
	inline FsmInt_t874273141 ** get_address_of_rootCount_21() { return &___rootCount_21; }
	inline void set_rootCount_21(FsmInt_t874273141 * value)
	{
		___rootCount_21 = value;
		Il2CppCodeGenWriteBarrier((&___rootCount_21), value);
	}

	inline static int32_t get_offset_of_rootGameObjects_22() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ___rootGameObjects_22)); }
	inline FsmArray_t1756862219 * get_rootGameObjects_22() const { return ___rootGameObjects_22; }
	inline FsmArray_t1756862219 ** get_address_of_rootGameObjects_22() { return &___rootGameObjects_22; }
	inline void set_rootGameObjects_22(FsmArray_t1756862219 * value)
	{
		___rootGameObjects_22 = value;
		Il2CppCodeGenWriteBarrier((&___rootGameObjects_22), value);
	}

	inline static int32_t get_offset_of__scene_23() { return static_cast<int32_t>(offsetof(GetSceneLoadedEventData_t1535803245, ____scene_23)); }
	inline Scene_t2348375561  get__scene_23() const { return ____scene_23; }
	inline Scene_t2348375561 * get_address_of__scene_23() { return &____scene_23; }
	inline void set__scene_23(Scene_t2348375561  value)
	{
		____scene_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENELOADEDEVENTDATA_T1535803245_H
#ifndef GETSCENEUNLOADEDEVENTDATA_T4107210274_H
#define GETSCENEUNLOADEDEVENTDATA_T4107210274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData
struct  GetSceneUnloadedEventData_t4107210274  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::name
	FsmString_t1785915204 * ___name_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::path
	FsmString_t1785915204 * ___path_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::buildIndex
	FsmInt_t874273141 * ___buildIndex_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::isValid
	FsmBool_t163807967 * ___isValid_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::isLoaded
	FsmBool_t163807967 * ___isLoaded_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::isDirty
	FsmBool_t163807967 * ___isDirty_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::rootCount
	FsmInt_t874273141 * ___rootCount_20;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::rootGameObjects
	FsmArray_t1756862219 * ___rootGameObjects_21;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::everyFrame
	bool ___everyFrame_22;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.GetSceneUnloadedEventData::_scene
	Scene_t2348375561  ____scene_23;

public:
	inline static int32_t get_offset_of_name_14() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___name_14)); }
	inline FsmString_t1785915204 * get_name_14() const { return ___name_14; }
	inline FsmString_t1785915204 ** get_address_of_name_14() { return &___name_14; }
	inline void set_name_14(FsmString_t1785915204 * value)
	{
		___name_14 = value;
		Il2CppCodeGenWriteBarrier((&___name_14), value);
	}

	inline static int32_t get_offset_of_path_15() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___path_15)); }
	inline FsmString_t1785915204 * get_path_15() const { return ___path_15; }
	inline FsmString_t1785915204 ** get_address_of_path_15() { return &___path_15; }
	inline void set_path_15(FsmString_t1785915204 * value)
	{
		___path_15 = value;
		Il2CppCodeGenWriteBarrier((&___path_15), value);
	}

	inline static int32_t get_offset_of_buildIndex_16() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___buildIndex_16)); }
	inline FsmInt_t874273141 * get_buildIndex_16() const { return ___buildIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_buildIndex_16() { return &___buildIndex_16; }
	inline void set_buildIndex_16(FsmInt_t874273141 * value)
	{
		___buildIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___buildIndex_16), value);
	}

	inline static int32_t get_offset_of_isValid_17() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___isValid_17)); }
	inline FsmBool_t163807967 * get_isValid_17() const { return ___isValid_17; }
	inline FsmBool_t163807967 ** get_address_of_isValid_17() { return &___isValid_17; }
	inline void set_isValid_17(FsmBool_t163807967 * value)
	{
		___isValid_17 = value;
		Il2CppCodeGenWriteBarrier((&___isValid_17), value);
	}

	inline static int32_t get_offset_of_isLoaded_18() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___isLoaded_18)); }
	inline FsmBool_t163807967 * get_isLoaded_18() const { return ___isLoaded_18; }
	inline FsmBool_t163807967 ** get_address_of_isLoaded_18() { return &___isLoaded_18; }
	inline void set_isLoaded_18(FsmBool_t163807967 * value)
	{
		___isLoaded_18 = value;
		Il2CppCodeGenWriteBarrier((&___isLoaded_18), value);
	}

	inline static int32_t get_offset_of_isDirty_19() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___isDirty_19)); }
	inline FsmBool_t163807967 * get_isDirty_19() const { return ___isDirty_19; }
	inline FsmBool_t163807967 ** get_address_of_isDirty_19() { return &___isDirty_19; }
	inline void set_isDirty_19(FsmBool_t163807967 * value)
	{
		___isDirty_19 = value;
		Il2CppCodeGenWriteBarrier((&___isDirty_19), value);
	}

	inline static int32_t get_offset_of_rootCount_20() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___rootCount_20)); }
	inline FsmInt_t874273141 * get_rootCount_20() const { return ___rootCount_20; }
	inline FsmInt_t874273141 ** get_address_of_rootCount_20() { return &___rootCount_20; }
	inline void set_rootCount_20(FsmInt_t874273141 * value)
	{
		___rootCount_20 = value;
		Il2CppCodeGenWriteBarrier((&___rootCount_20), value);
	}

	inline static int32_t get_offset_of_rootGameObjects_21() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___rootGameObjects_21)); }
	inline FsmArray_t1756862219 * get_rootGameObjects_21() const { return ___rootGameObjects_21; }
	inline FsmArray_t1756862219 ** get_address_of_rootGameObjects_21() { return &___rootGameObjects_21; }
	inline void set_rootGameObjects_21(FsmArray_t1756862219 * value)
	{
		___rootGameObjects_21 = value;
		Il2CppCodeGenWriteBarrier((&___rootGameObjects_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}

	inline static int32_t get_offset_of__scene_23() { return static_cast<int32_t>(offsetof(GetSceneUnloadedEventData_t4107210274, ____scene_23)); }
	inline Scene_t2348375561  get__scene_23() const { return ____scene_23; }
	inline Scene_t2348375561 * get_address_of__scene_23() { return &____scene_23; }
	inline void set__scene_23(Scene_t2348375561  value)
	{
		____scene_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEUNLOADEDEVENTDATA_T4107210274_H
#ifndef LOADSCENE_T2656597926_H
#define LOADSCENE_T2656597926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LoadScene
struct  LoadScene_t2656597926  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneSimpleReferenceOptions HutongGames.PlayMaker.Actions.LoadScene::sceneReference
	int32_t ___sceneReference_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.LoadScene::sceneByName
	FsmString_t1785915204 * ___sceneByName_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LoadScene::sceneAtIndex
	FsmInt_t874273141 * ___sceneAtIndex_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.LoadScene::loadSceneMode
	FsmEnum_t2861764163 * ___loadSceneMode_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadScene::success
	FsmBool_t163807967 * ___success_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadScene::successEvent
	FsmEvent_t3736299882 * ___successEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadScene::failureEvent
	FsmEvent_t3736299882 * ___failureEvent_20;

public:
	inline static int32_t get_offset_of_sceneReference_14() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___sceneReference_14)); }
	inline int32_t get_sceneReference_14() const { return ___sceneReference_14; }
	inline int32_t* get_address_of_sceneReference_14() { return &___sceneReference_14; }
	inline void set_sceneReference_14(int32_t value)
	{
		___sceneReference_14 = value;
	}

	inline static int32_t get_offset_of_sceneByName_15() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___sceneByName_15)); }
	inline FsmString_t1785915204 * get_sceneByName_15() const { return ___sceneByName_15; }
	inline FsmString_t1785915204 ** get_address_of_sceneByName_15() { return &___sceneByName_15; }
	inline void set_sceneByName_15(FsmString_t1785915204 * value)
	{
		___sceneByName_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByName_15), value);
	}

	inline static int32_t get_offset_of_sceneAtIndex_16() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___sceneAtIndex_16)); }
	inline FsmInt_t874273141 * get_sceneAtIndex_16() const { return ___sceneAtIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtIndex_16() { return &___sceneAtIndex_16; }
	inline void set_sceneAtIndex_16(FsmInt_t874273141 * value)
	{
		___sceneAtIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtIndex_16), value);
	}

	inline static int32_t get_offset_of_loadSceneMode_17() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___loadSceneMode_17)); }
	inline FsmEnum_t2861764163 * get_loadSceneMode_17() const { return ___loadSceneMode_17; }
	inline FsmEnum_t2861764163 ** get_address_of_loadSceneMode_17() { return &___loadSceneMode_17; }
	inline void set_loadSceneMode_17(FsmEnum_t2861764163 * value)
	{
		___loadSceneMode_17 = value;
		Il2CppCodeGenWriteBarrier((&___loadSceneMode_17), value);
	}

	inline static int32_t get_offset_of_success_18() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___success_18)); }
	inline FsmBool_t163807967 * get_success_18() const { return ___success_18; }
	inline FsmBool_t163807967 ** get_address_of_success_18() { return &___success_18; }
	inline void set_success_18(FsmBool_t163807967 * value)
	{
		___success_18 = value;
		Il2CppCodeGenWriteBarrier((&___success_18), value);
	}

	inline static int32_t get_offset_of_successEvent_19() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___successEvent_19)); }
	inline FsmEvent_t3736299882 * get_successEvent_19() const { return ___successEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_successEvent_19() { return &___successEvent_19; }
	inline void set_successEvent_19(FsmEvent_t3736299882 * value)
	{
		___successEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___successEvent_19), value);
	}

	inline static int32_t get_offset_of_failureEvent_20() { return static_cast<int32_t>(offsetof(LoadScene_t2656597926, ___failureEvent_20)); }
	inline FsmEvent_t3736299882 * get_failureEvent_20() const { return ___failureEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_failureEvent_20() { return &___failureEvent_20; }
	inline void set_failureEvent_20(FsmEvent_t3736299882 * value)
	{
		___failureEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___failureEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENE_T2656597926_H
#ifndef LOADSCENEASYNCH_T4132231259_H
#define LOADSCENEASYNCH_T4132231259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LoadSceneAsynch
struct  LoadSceneAsynch_t4132231259  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneSimpleReferenceOptions HutongGames.PlayMaker.Actions.LoadSceneAsynch::sceneReference
	int32_t ___sceneReference_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.LoadSceneAsynch::sceneByName
	FsmString_t1785915204 * ___sceneByName_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LoadSceneAsynch::sceneAtIndex
	FsmInt_t874273141 * ___sceneAtIndex_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.LoadSceneAsynch::loadSceneMode
	FsmEnum_t2861764163 * ___loadSceneMode_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadSceneAsynch::allowSceneActivation
	FsmBool_t163807967 * ___allowSceneActivation_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LoadSceneAsynch::operationPriority
	FsmInt_t874273141 * ___operationPriority_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LoadSceneAsynch::aSyncOperationHashCode
	FsmInt_t874273141 * ___aSyncOperationHashCode_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.LoadSceneAsynch::progress
	FsmFloat_t2883254149 * ___progress_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadSceneAsynch::isDone
	FsmBool_t163807967 * ___isDone_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadSceneAsynch::pendingActivation
	FsmBool_t163807967 * ___pendingActivation_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadSceneAsynch::doneEvent
	FsmEvent_t3736299882 * ___doneEvent_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadSceneAsynch::pendingActivationEvent
	FsmEvent_t3736299882 * ___pendingActivationEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadSceneAsynch::sceneNotFoundEvent
	FsmEvent_t3736299882 * ___sceneNotFoundEvent_26;
	// UnityEngine.AsyncOperation HutongGames.PlayMaker.Actions.LoadSceneAsynch::_asyncOperation
	AsyncOperation_t1445031843 * ____asyncOperation_27;
	// System.Int32 HutongGames.PlayMaker.Actions.LoadSceneAsynch::_asynchOperationUid
	int32_t ____asynchOperationUid_28;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadSceneAsynch::pendingActivationCallBackDone
	bool ___pendingActivationCallBackDone_29;

public:
	inline static int32_t get_offset_of_sceneReference_14() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___sceneReference_14)); }
	inline int32_t get_sceneReference_14() const { return ___sceneReference_14; }
	inline int32_t* get_address_of_sceneReference_14() { return &___sceneReference_14; }
	inline void set_sceneReference_14(int32_t value)
	{
		___sceneReference_14 = value;
	}

	inline static int32_t get_offset_of_sceneByName_15() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___sceneByName_15)); }
	inline FsmString_t1785915204 * get_sceneByName_15() const { return ___sceneByName_15; }
	inline FsmString_t1785915204 ** get_address_of_sceneByName_15() { return &___sceneByName_15; }
	inline void set_sceneByName_15(FsmString_t1785915204 * value)
	{
		___sceneByName_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByName_15), value);
	}

	inline static int32_t get_offset_of_sceneAtIndex_16() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___sceneAtIndex_16)); }
	inline FsmInt_t874273141 * get_sceneAtIndex_16() const { return ___sceneAtIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtIndex_16() { return &___sceneAtIndex_16; }
	inline void set_sceneAtIndex_16(FsmInt_t874273141 * value)
	{
		___sceneAtIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtIndex_16), value);
	}

	inline static int32_t get_offset_of_loadSceneMode_17() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___loadSceneMode_17)); }
	inline FsmEnum_t2861764163 * get_loadSceneMode_17() const { return ___loadSceneMode_17; }
	inline FsmEnum_t2861764163 ** get_address_of_loadSceneMode_17() { return &___loadSceneMode_17; }
	inline void set_loadSceneMode_17(FsmEnum_t2861764163 * value)
	{
		___loadSceneMode_17 = value;
		Il2CppCodeGenWriteBarrier((&___loadSceneMode_17), value);
	}

	inline static int32_t get_offset_of_allowSceneActivation_18() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___allowSceneActivation_18)); }
	inline FsmBool_t163807967 * get_allowSceneActivation_18() const { return ___allowSceneActivation_18; }
	inline FsmBool_t163807967 ** get_address_of_allowSceneActivation_18() { return &___allowSceneActivation_18; }
	inline void set_allowSceneActivation_18(FsmBool_t163807967 * value)
	{
		___allowSceneActivation_18 = value;
		Il2CppCodeGenWriteBarrier((&___allowSceneActivation_18), value);
	}

	inline static int32_t get_offset_of_operationPriority_19() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___operationPriority_19)); }
	inline FsmInt_t874273141 * get_operationPriority_19() const { return ___operationPriority_19; }
	inline FsmInt_t874273141 ** get_address_of_operationPriority_19() { return &___operationPriority_19; }
	inline void set_operationPriority_19(FsmInt_t874273141 * value)
	{
		___operationPriority_19 = value;
		Il2CppCodeGenWriteBarrier((&___operationPriority_19), value);
	}

	inline static int32_t get_offset_of_aSyncOperationHashCode_20() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___aSyncOperationHashCode_20)); }
	inline FsmInt_t874273141 * get_aSyncOperationHashCode_20() const { return ___aSyncOperationHashCode_20; }
	inline FsmInt_t874273141 ** get_address_of_aSyncOperationHashCode_20() { return &___aSyncOperationHashCode_20; }
	inline void set_aSyncOperationHashCode_20(FsmInt_t874273141 * value)
	{
		___aSyncOperationHashCode_20 = value;
		Il2CppCodeGenWriteBarrier((&___aSyncOperationHashCode_20), value);
	}

	inline static int32_t get_offset_of_progress_21() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___progress_21)); }
	inline FsmFloat_t2883254149 * get_progress_21() const { return ___progress_21; }
	inline FsmFloat_t2883254149 ** get_address_of_progress_21() { return &___progress_21; }
	inline void set_progress_21(FsmFloat_t2883254149 * value)
	{
		___progress_21 = value;
		Il2CppCodeGenWriteBarrier((&___progress_21), value);
	}

	inline static int32_t get_offset_of_isDone_22() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___isDone_22)); }
	inline FsmBool_t163807967 * get_isDone_22() const { return ___isDone_22; }
	inline FsmBool_t163807967 ** get_address_of_isDone_22() { return &___isDone_22; }
	inline void set_isDone_22(FsmBool_t163807967 * value)
	{
		___isDone_22 = value;
		Il2CppCodeGenWriteBarrier((&___isDone_22), value);
	}

	inline static int32_t get_offset_of_pendingActivation_23() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___pendingActivation_23)); }
	inline FsmBool_t163807967 * get_pendingActivation_23() const { return ___pendingActivation_23; }
	inline FsmBool_t163807967 ** get_address_of_pendingActivation_23() { return &___pendingActivation_23; }
	inline void set_pendingActivation_23(FsmBool_t163807967 * value)
	{
		___pendingActivation_23 = value;
		Il2CppCodeGenWriteBarrier((&___pendingActivation_23), value);
	}

	inline static int32_t get_offset_of_doneEvent_24() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___doneEvent_24)); }
	inline FsmEvent_t3736299882 * get_doneEvent_24() const { return ___doneEvent_24; }
	inline FsmEvent_t3736299882 ** get_address_of_doneEvent_24() { return &___doneEvent_24; }
	inline void set_doneEvent_24(FsmEvent_t3736299882 * value)
	{
		___doneEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___doneEvent_24), value);
	}

	inline static int32_t get_offset_of_pendingActivationEvent_25() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___pendingActivationEvent_25)); }
	inline FsmEvent_t3736299882 * get_pendingActivationEvent_25() const { return ___pendingActivationEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_pendingActivationEvent_25() { return &___pendingActivationEvent_25; }
	inline void set_pendingActivationEvent_25(FsmEvent_t3736299882 * value)
	{
		___pendingActivationEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___pendingActivationEvent_25), value);
	}

	inline static int32_t get_offset_of_sceneNotFoundEvent_26() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___sceneNotFoundEvent_26)); }
	inline FsmEvent_t3736299882 * get_sceneNotFoundEvent_26() const { return ___sceneNotFoundEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneNotFoundEvent_26() { return &___sceneNotFoundEvent_26; }
	inline void set_sceneNotFoundEvent_26(FsmEvent_t3736299882 * value)
	{
		___sceneNotFoundEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___sceneNotFoundEvent_26), value);
	}

	inline static int32_t get_offset_of__asyncOperation_27() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ____asyncOperation_27)); }
	inline AsyncOperation_t1445031843 * get__asyncOperation_27() const { return ____asyncOperation_27; }
	inline AsyncOperation_t1445031843 ** get_address_of__asyncOperation_27() { return &____asyncOperation_27; }
	inline void set__asyncOperation_27(AsyncOperation_t1445031843 * value)
	{
		____asyncOperation_27 = value;
		Il2CppCodeGenWriteBarrier((&____asyncOperation_27), value);
	}

	inline static int32_t get_offset_of__asynchOperationUid_28() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ____asynchOperationUid_28)); }
	inline int32_t get__asynchOperationUid_28() const { return ____asynchOperationUid_28; }
	inline int32_t* get_address_of__asynchOperationUid_28() { return &____asynchOperationUid_28; }
	inline void set__asynchOperationUid_28(int32_t value)
	{
		____asynchOperationUid_28 = value;
	}

	inline static int32_t get_offset_of_pendingActivationCallBackDone_29() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259, ___pendingActivationCallBackDone_29)); }
	inline bool get_pendingActivationCallBackDone_29() const { return ___pendingActivationCallBackDone_29; }
	inline bool* get_address_of_pendingActivationCallBackDone_29() { return &___pendingActivationCallBackDone_29; }
	inline void set_pendingActivationCallBackDone_29(bool value)
	{
		___pendingActivationCallBackDone_29 = value;
	}
};

struct LoadSceneAsynch_t4132231259_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.AsyncOperation> HutongGames.PlayMaker.Actions.LoadSceneAsynch::aSyncOperationLUT
	Dictionary_2_t333745174 * ___aSyncOperationLUT_30;
	// System.Int32 HutongGames.PlayMaker.Actions.LoadSceneAsynch::aSynchUidCounter
	int32_t ___aSynchUidCounter_31;

public:
	inline static int32_t get_offset_of_aSyncOperationLUT_30() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259_StaticFields, ___aSyncOperationLUT_30)); }
	inline Dictionary_2_t333745174 * get_aSyncOperationLUT_30() const { return ___aSyncOperationLUT_30; }
	inline Dictionary_2_t333745174 ** get_address_of_aSyncOperationLUT_30() { return &___aSyncOperationLUT_30; }
	inline void set_aSyncOperationLUT_30(Dictionary_2_t333745174 * value)
	{
		___aSyncOperationLUT_30 = value;
		Il2CppCodeGenWriteBarrier((&___aSyncOperationLUT_30), value);
	}

	inline static int32_t get_offset_of_aSynchUidCounter_31() { return static_cast<int32_t>(offsetof(LoadSceneAsynch_t4132231259_StaticFields, ___aSynchUidCounter_31)); }
	inline int32_t get_aSynchUidCounter_31() const { return ___aSynchUidCounter_31; }
	inline int32_t* get_address_of_aSynchUidCounter_31() { return &___aSynchUidCounter_31; }
	inline void set_aSynchUidCounter_31(int32_t value)
	{
		___aSynchUidCounter_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENEASYNCH_T4132231259_H
#ifndef MERGESCENES_T2033590507_H
#define MERGESCENES_T2033590507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MergeScenes
struct  MergeScenes_t2033590507  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneAllReferenceOptions HutongGames.PlayMaker.Actions.MergeScenes::sourceReference
	int32_t ___sourceReference_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MergeScenes::sourceAtIndex
	FsmInt_t874273141 * ___sourceAtIndex_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MergeScenes::sourceByName
	FsmString_t1785915204 * ___sourceByName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MergeScenes::sourceByPath
	FsmString_t1785915204 * ___sourceByPath_17;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MergeScenes::sourceByGameObject
	FsmOwnerDefault_t3590610434 * ___sourceByGameObject_18;
	// HutongGames.PlayMaker.Actions.GetSceneActionBase/SceneAllReferenceOptions HutongGames.PlayMaker.Actions.MergeScenes::destinationReference
	int32_t ___destinationReference_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MergeScenes::destinationAtIndex
	FsmInt_t874273141 * ___destinationAtIndex_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MergeScenes::destinationByName
	FsmString_t1785915204 * ___destinationByName_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MergeScenes::destinationByPath
	FsmString_t1785915204 * ___destinationByPath_22;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MergeScenes::destinationByGameObject
	FsmOwnerDefault_t3590610434 * ___destinationByGameObject_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MergeScenes::success
	FsmBool_t163807967 * ___success_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MergeScenes::successEvent
	FsmEvent_t3736299882 * ___successEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MergeScenes::failureEvent
	FsmEvent_t3736299882 * ___failureEvent_26;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.MergeScenes::_sourceScene
	Scene_t2348375561  ____sourceScene_27;
	// System.Boolean HutongGames.PlayMaker.Actions.MergeScenes::_sourceFound
	bool ____sourceFound_28;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.MergeScenes::_destinationScene
	Scene_t2348375561  ____destinationScene_29;
	// System.Boolean HutongGames.PlayMaker.Actions.MergeScenes::_destinationFound
	bool ____destinationFound_30;

public:
	inline static int32_t get_offset_of_sourceReference_14() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___sourceReference_14)); }
	inline int32_t get_sourceReference_14() const { return ___sourceReference_14; }
	inline int32_t* get_address_of_sourceReference_14() { return &___sourceReference_14; }
	inline void set_sourceReference_14(int32_t value)
	{
		___sourceReference_14 = value;
	}

	inline static int32_t get_offset_of_sourceAtIndex_15() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___sourceAtIndex_15)); }
	inline FsmInt_t874273141 * get_sourceAtIndex_15() const { return ___sourceAtIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_sourceAtIndex_15() { return &___sourceAtIndex_15; }
	inline void set_sourceAtIndex_15(FsmInt_t874273141 * value)
	{
		___sourceAtIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___sourceAtIndex_15), value);
	}

	inline static int32_t get_offset_of_sourceByName_16() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___sourceByName_16)); }
	inline FsmString_t1785915204 * get_sourceByName_16() const { return ___sourceByName_16; }
	inline FsmString_t1785915204 ** get_address_of_sourceByName_16() { return &___sourceByName_16; }
	inline void set_sourceByName_16(FsmString_t1785915204 * value)
	{
		___sourceByName_16 = value;
		Il2CppCodeGenWriteBarrier((&___sourceByName_16), value);
	}

	inline static int32_t get_offset_of_sourceByPath_17() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___sourceByPath_17)); }
	inline FsmString_t1785915204 * get_sourceByPath_17() const { return ___sourceByPath_17; }
	inline FsmString_t1785915204 ** get_address_of_sourceByPath_17() { return &___sourceByPath_17; }
	inline void set_sourceByPath_17(FsmString_t1785915204 * value)
	{
		___sourceByPath_17 = value;
		Il2CppCodeGenWriteBarrier((&___sourceByPath_17), value);
	}

	inline static int32_t get_offset_of_sourceByGameObject_18() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___sourceByGameObject_18)); }
	inline FsmOwnerDefault_t3590610434 * get_sourceByGameObject_18() const { return ___sourceByGameObject_18; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_sourceByGameObject_18() { return &___sourceByGameObject_18; }
	inline void set_sourceByGameObject_18(FsmOwnerDefault_t3590610434 * value)
	{
		___sourceByGameObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___sourceByGameObject_18), value);
	}

	inline static int32_t get_offset_of_destinationReference_19() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___destinationReference_19)); }
	inline int32_t get_destinationReference_19() const { return ___destinationReference_19; }
	inline int32_t* get_address_of_destinationReference_19() { return &___destinationReference_19; }
	inline void set_destinationReference_19(int32_t value)
	{
		___destinationReference_19 = value;
	}

	inline static int32_t get_offset_of_destinationAtIndex_20() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___destinationAtIndex_20)); }
	inline FsmInt_t874273141 * get_destinationAtIndex_20() const { return ___destinationAtIndex_20; }
	inline FsmInt_t874273141 ** get_address_of_destinationAtIndex_20() { return &___destinationAtIndex_20; }
	inline void set_destinationAtIndex_20(FsmInt_t874273141 * value)
	{
		___destinationAtIndex_20 = value;
		Il2CppCodeGenWriteBarrier((&___destinationAtIndex_20), value);
	}

	inline static int32_t get_offset_of_destinationByName_21() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___destinationByName_21)); }
	inline FsmString_t1785915204 * get_destinationByName_21() const { return ___destinationByName_21; }
	inline FsmString_t1785915204 ** get_address_of_destinationByName_21() { return &___destinationByName_21; }
	inline void set_destinationByName_21(FsmString_t1785915204 * value)
	{
		___destinationByName_21 = value;
		Il2CppCodeGenWriteBarrier((&___destinationByName_21), value);
	}

	inline static int32_t get_offset_of_destinationByPath_22() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___destinationByPath_22)); }
	inline FsmString_t1785915204 * get_destinationByPath_22() const { return ___destinationByPath_22; }
	inline FsmString_t1785915204 ** get_address_of_destinationByPath_22() { return &___destinationByPath_22; }
	inline void set_destinationByPath_22(FsmString_t1785915204 * value)
	{
		___destinationByPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___destinationByPath_22), value);
	}

	inline static int32_t get_offset_of_destinationByGameObject_23() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___destinationByGameObject_23)); }
	inline FsmOwnerDefault_t3590610434 * get_destinationByGameObject_23() const { return ___destinationByGameObject_23; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_destinationByGameObject_23() { return &___destinationByGameObject_23; }
	inline void set_destinationByGameObject_23(FsmOwnerDefault_t3590610434 * value)
	{
		___destinationByGameObject_23 = value;
		Il2CppCodeGenWriteBarrier((&___destinationByGameObject_23), value);
	}

	inline static int32_t get_offset_of_success_24() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___success_24)); }
	inline FsmBool_t163807967 * get_success_24() const { return ___success_24; }
	inline FsmBool_t163807967 ** get_address_of_success_24() { return &___success_24; }
	inline void set_success_24(FsmBool_t163807967 * value)
	{
		___success_24 = value;
		Il2CppCodeGenWriteBarrier((&___success_24), value);
	}

	inline static int32_t get_offset_of_successEvent_25() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___successEvent_25)); }
	inline FsmEvent_t3736299882 * get_successEvent_25() const { return ___successEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_successEvent_25() { return &___successEvent_25; }
	inline void set_successEvent_25(FsmEvent_t3736299882 * value)
	{
		___successEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___successEvent_25), value);
	}

	inline static int32_t get_offset_of_failureEvent_26() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ___failureEvent_26)); }
	inline FsmEvent_t3736299882 * get_failureEvent_26() const { return ___failureEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_failureEvent_26() { return &___failureEvent_26; }
	inline void set_failureEvent_26(FsmEvent_t3736299882 * value)
	{
		___failureEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___failureEvent_26), value);
	}

	inline static int32_t get_offset_of__sourceScene_27() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ____sourceScene_27)); }
	inline Scene_t2348375561  get__sourceScene_27() const { return ____sourceScene_27; }
	inline Scene_t2348375561 * get_address_of__sourceScene_27() { return &____sourceScene_27; }
	inline void set__sourceScene_27(Scene_t2348375561  value)
	{
		____sourceScene_27 = value;
	}

	inline static int32_t get_offset_of__sourceFound_28() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ____sourceFound_28)); }
	inline bool get__sourceFound_28() const { return ____sourceFound_28; }
	inline bool* get_address_of__sourceFound_28() { return &____sourceFound_28; }
	inline void set__sourceFound_28(bool value)
	{
		____sourceFound_28 = value;
	}

	inline static int32_t get_offset_of__destinationScene_29() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ____destinationScene_29)); }
	inline Scene_t2348375561  get__destinationScene_29() const { return ____destinationScene_29; }
	inline Scene_t2348375561 * get_address_of__destinationScene_29() { return &____destinationScene_29; }
	inline void set__destinationScene_29(Scene_t2348375561  value)
	{
		____destinationScene_29 = value;
	}

	inline static int32_t get_offset_of__destinationFound_30() { return static_cast<int32_t>(offsetof(MergeScenes_t2033590507, ____destinationFound_30)); }
	inline bool get__destinationFound_30() const { return ____destinationFound_30; }
	inline bool* get_address_of__destinationFound_30() { return &____destinationFound_30; }
	inline void set__destinationFound_30(bool value)
	{
		____destinationFound_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGESCENES_T2033590507_H
#ifndef PLAYERPREFSSETSTRING_T337717693_H
#define PLAYERPREFSSETSTRING_T337717693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PlayerPrefsSetString
struct  PlayerPrefsSetString_t337717693  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsSetString::keys
	FsmStringU5BU5D_t252501805* ___keys_14;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.PlayerPrefsSetString::values
	FsmStringU5BU5D_t252501805* ___values_15;

public:
	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(PlayerPrefsSetString_t337717693, ___keys_14)); }
	inline FsmStringU5BU5D_t252501805* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t252501805* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier((&___keys_14), value);
	}

	inline static int32_t get_offset_of_values_15() { return static_cast<int32_t>(offsetof(PlayerPrefsSetString_t337717693, ___values_15)); }
	inline FsmStringU5BU5D_t252501805* get_values_15() const { return ___values_15; }
	inline FsmStringU5BU5D_t252501805** get_address_of_values_15() { return &___values_15; }
	inline void set_values_15(FsmStringU5BU5D_t252501805* value)
	{
		___values_15 = value;
		Il2CppCodeGenWriteBarrier((&___values_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERPREFSSETSTRING_T337717693_H
#ifndef QUATERNIONBASEACTION_T3672550121_H
#define QUATERNIONBASEACTION_T3672550121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionBaseAction
struct  QuaternionBaseAction_t3672550121  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.QuaternionBaseAction::everyFrame
	bool ___everyFrame_14;
	// HutongGames.PlayMaker.Actions.QuaternionBaseAction/everyFrameOptions HutongGames.PlayMaker.Actions.QuaternionBaseAction::everyFrameOption
	int32_t ___everyFrameOption_15;

public:
	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(QuaternionBaseAction_t3672550121, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of_everyFrameOption_15() { return static_cast<int32_t>(offsetof(QuaternionBaseAction_t3672550121, ___everyFrameOption_15)); }
	inline int32_t get_everyFrameOption_15() const { return ___everyFrameOption_15; }
	inline int32_t* get_address_of_everyFrameOption_15() { return &___everyFrameOption_15; }
	inline void set_everyFrameOption_15(int32_t value)
	{
		___everyFrameOption_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONBASEACTION_T3672550121_H
#ifndef REBUILDTEXTURES_T3968693992_H
#define REBUILDTEXTURES_T3968693992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RebuildTextures
struct  RebuildTextures_t3968693992  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.RebuildTextures::substanceMaterial
	FsmMaterial_t2057874452 * ___substanceMaterial_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RebuildTextures::immediately
	FsmBool_t163807967 * ___immediately_15;
	// System.Boolean HutongGames.PlayMaker.Actions.RebuildTextures::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_substanceMaterial_14() { return static_cast<int32_t>(offsetof(RebuildTextures_t3968693992, ___substanceMaterial_14)); }
	inline FsmMaterial_t2057874452 * get_substanceMaterial_14() const { return ___substanceMaterial_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_substanceMaterial_14() { return &___substanceMaterial_14; }
	inline void set_substanceMaterial_14(FsmMaterial_t2057874452 * value)
	{
		___substanceMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___substanceMaterial_14), value);
	}

	inline static int32_t get_offset_of_immediately_15() { return static_cast<int32_t>(offsetof(RebuildTextures_t3968693992, ___immediately_15)); }
	inline FsmBool_t163807967 * get_immediately_15() const { return ___immediately_15; }
	inline FsmBool_t163807967 ** get_address_of_immediately_15() { return &___immediately_15; }
	inline void set_immediately_15(FsmBool_t163807967 * value)
	{
		___immediately_15 = value;
		Il2CppCodeGenWriteBarrier((&___immediately_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(RebuildTextures_t3968693992, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REBUILDTEXTURES_T3968693992_H
#ifndef RECTCONTAINS_T3948379732_H
#define RECTCONTAINS_T3948379732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectContains
struct  RectContains_t3948379732  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectContains::rectangle
	FsmRect_t3649179877 * ___rectangle_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectContains::point
	FsmVector3_t626444517 * ___point_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectContains::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectContains::y
	FsmFloat_t2883254149 * ___y_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectContains::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectContains::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectContains::storeResult
	FsmBool_t163807967 * ___storeResult_20;
	// System.Boolean HutongGames.PlayMaker.Actions.RectContains::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_rectangle_14() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___rectangle_14)); }
	inline FsmRect_t3649179877 * get_rectangle_14() const { return ___rectangle_14; }
	inline FsmRect_t3649179877 ** get_address_of_rectangle_14() { return &___rectangle_14; }
	inline void set_rectangle_14(FsmRect_t3649179877 * value)
	{
		___rectangle_14 = value;
		Il2CppCodeGenWriteBarrier((&___rectangle_14), value);
	}

	inline static int32_t get_offset_of_point_15() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___point_15)); }
	inline FsmVector3_t626444517 * get_point_15() const { return ___point_15; }
	inline FsmVector3_t626444517 ** get_address_of_point_15() { return &___point_15; }
	inline void set_point_15(FsmVector3_t626444517 * value)
	{
		___point_15 = value;
		Il2CppCodeGenWriteBarrier((&___point_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_trueEvent_18() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___trueEvent_18)); }
	inline FsmEvent_t3736299882 * get_trueEvent_18() const { return ___trueEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_18() { return &___trueEvent_18; }
	inline void set_trueEvent_18(FsmEvent_t3736299882 * value)
	{
		___trueEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_18), value);
	}

	inline static int32_t get_offset_of_falseEvent_19() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___falseEvent_19)); }
	inline FsmEvent_t3736299882 * get_falseEvent_19() const { return ___falseEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_19() { return &___falseEvent_19; }
	inline void set_falseEvent_19(FsmEvent_t3736299882 * value)
	{
		___falseEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_19), value);
	}

	inline static int32_t get_offset_of_storeResult_20() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___storeResult_20)); }
	inline FsmBool_t163807967 * get_storeResult_20() const { return ___storeResult_20; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_20() { return &___storeResult_20; }
	inline void set_storeResult_20(FsmBool_t163807967 * value)
	{
		___storeResult_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(RectContains_t3948379732, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTCONTAINS_T3948379732_H
#ifndef RECTOVERLAPS_T1548172352_H
#define RECTOVERLAPS_T1548172352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectOverlaps
struct  RectOverlaps_t1548172352  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectOverlaps::rect1
	FsmRect_t3649179877 * ___rect1_14;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectOverlaps::rect2
	FsmRect_t3649179877 * ___rect2_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectOverlaps::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectOverlaps::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectOverlaps::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.RectOverlaps::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_rect1_14() { return static_cast<int32_t>(offsetof(RectOverlaps_t1548172352, ___rect1_14)); }
	inline FsmRect_t3649179877 * get_rect1_14() const { return ___rect1_14; }
	inline FsmRect_t3649179877 ** get_address_of_rect1_14() { return &___rect1_14; }
	inline void set_rect1_14(FsmRect_t3649179877 * value)
	{
		___rect1_14 = value;
		Il2CppCodeGenWriteBarrier((&___rect1_14), value);
	}

	inline static int32_t get_offset_of_rect2_15() { return static_cast<int32_t>(offsetof(RectOverlaps_t1548172352, ___rect2_15)); }
	inline FsmRect_t3649179877 * get_rect2_15() const { return ___rect2_15; }
	inline FsmRect_t3649179877 ** get_address_of_rect2_15() { return &___rect2_15; }
	inline void set_rect2_15(FsmRect_t3649179877 * value)
	{
		___rect2_15 = value;
		Il2CppCodeGenWriteBarrier((&___rect2_15), value);
	}

	inline static int32_t get_offset_of_trueEvent_16() { return static_cast<int32_t>(offsetof(RectOverlaps_t1548172352, ___trueEvent_16)); }
	inline FsmEvent_t3736299882 * get_trueEvent_16() const { return ___trueEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_16() { return &___trueEvent_16; }
	inline void set_trueEvent_16(FsmEvent_t3736299882 * value)
	{
		___trueEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_16), value);
	}

	inline static int32_t get_offset_of_falseEvent_17() { return static_cast<int32_t>(offsetof(RectOverlaps_t1548172352, ___falseEvent_17)); }
	inline FsmEvent_t3736299882 * get_falseEvent_17() const { return ___falseEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_17() { return &___falseEvent_17; }
	inline void set_falseEvent_17(FsmEvent_t3736299882 * value)
	{
		___falseEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(RectOverlaps_t1548172352, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(RectOverlaps_t1548172352, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTOVERLAPS_T1548172352_H
#ifndef RECTTRANSFORMCONTAINSSCREENPOINT_T106483262_H
#define RECTTRANSFORMCONTAINSSCREENPOINT_T106483262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint
struct  RectTransformContainsScreenPoint_t106483262  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::screenPointVector2
	FsmVector2_t2965096677 * ___screenPointVector2_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::orScreenPointVector3
	FsmVector3_t626444517 * ___orScreenPointVector3_16;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::normalizedScreenPoint
	bool ___normalizedScreenPoint_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::camera
	FsmGameObject_t3581898942 * ___camera_18;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::everyFrame
	bool ___everyFrame_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::isContained
	FsmBool_t163807967 * ___isContained_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::isContainedEvent
	FsmEvent_t3736299882 * ___isContainedEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::isNotContainedEvent
	FsmEvent_t3736299882 * ___isNotContainedEvent_22;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::_rt
	RectTransform_t3704657025 * ____rt_23;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::_camera
	Camera_t4157153871 * ____camera_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_screenPointVector2_15() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___screenPointVector2_15)); }
	inline FsmVector2_t2965096677 * get_screenPointVector2_15() const { return ___screenPointVector2_15; }
	inline FsmVector2_t2965096677 ** get_address_of_screenPointVector2_15() { return &___screenPointVector2_15; }
	inline void set_screenPointVector2_15(FsmVector2_t2965096677 * value)
	{
		___screenPointVector2_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenPointVector2_15), value);
	}

	inline static int32_t get_offset_of_orScreenPointVector3_16() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___orScreenPointVector3_16)); }
	inline FsmVector3_t626444517 * get_orScreenPointVector3_16() const { return ___orScreenPointVector3_16; }
	inline FsmVector3_t626444517 ** get_address_of_orScreenPointVector3_16() { return &___orScreenPointVector3_16; }
	inline void set_orScreenPointVector3_16(FsmVector3_t626444517 * value)
	{
		___orScreenPointVector3_16 = value;
		Il2CppCodeGenWriteBarrier((&___orScreenPointVector3_16), value);
	}

	inline static int32_t get_offset_of_normalizedScreenPoint_17() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___normalizedScreenPoint_17)); }
	inline bool get_normalizedScreenPoint_17() const { return ___normalizedScreenPoint_17; }
	inline bool* get_address_of_normalizedScreenPoint_17() { return &___normalizedScreenPoint_17; }
	inline void set_normalizedScreenPoint_17(bool value)
	{
		___normalizedScreenPoint_17 = value;
	}

	inline static int32_t get_offset_of_camera_18() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___camera_18)); }
	inline FsmGameObject_t3581898942 * get_camera_18() const { return ___camera_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_camera_18() { return &___camera_18; }
	inline void set_camera_18(FsmGameObject_t3581898942 * value)
	{
		___camera_18 = value;
		Il2CppCodeGenWriteBarrier((&___camera_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_isContained_20() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___isContained_20)); }
	inline FsmBool_t163807967 * get_isContained_20() const { return ___isContained_20; }
	inline FsmBool_t163807967 ** get_address_of_isContained_20() { return &___isContained_20; }
	inline void set_isContained_20(FsmBool_t163807967 * value)
	{
		___isContained_20 = value;
		Il2CppCodeGenWriteBarrier((&___isContained_20), value);
	}

	inline static int32_t get_offset_of_isContainedEvent_21() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___isContainedEvent_21)); }
	inline FsmEvent_t3736299882 * get_isContainedEvent_21() const { return ___isContainedEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_isContainedEvent_21() { return &___isContainedEvent_21; }
	inline void set_isContainedEvent_21(FsmEvent_t3736299882 * value)
	{
		___isContainedEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___isContainedEvent_21), value);
	}

	inline static int32_t get_offset_of_isNotContainedEvent_22() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ___isNotContainedEvent_22)); }
	inline FsmEvent_t3736299882 * get_isNotContainedEvent_22() const { return ___isNotContainedEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotContainedEvent_22() { return &___isNotContainedEvent_22; }
	inline void set_isNotContainedEvent_22(FsmEvent_t3736299882 * value)
	{
		___isNotContainedEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___isNotContainedEvent_22), value);
	}

	inline static int32_t get_offset_of__rt_23() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ____rt_23)); }
	inline RectTransform_t3704657025 * get__rt_23() const { return ____rt_23; }
	inline RectTransform_t3704657025 ** get_address_of__rt_23() { return &____rt_23; }
	inline void set__rt_23(RectTransform_t3704657025 * value)
	{
		____rt_23 = value;
		Il2CppCodeGenWriteBarrier((&____rt_23), value);
	}

	inline static int32_t get_offset_of__camera_24() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t106483262, ____camera_24)); }
	inline Camera_t4157153871 * get__camera_24() const { return ____camera_24; }
	inline Camera_t4157153871 ** get_address_of__camera_24() { return &____camera_24; }
	inline void set__camera_24(Camera_t4157153871 * value)
	{
		____camera_24 = value;
		Il2CppCodeGenWriteBarrier((&____camera_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMCONTAINSSCREENPOINT_T106483262_H
#ifndef RECTTRANSFORMFLIPLAYOUTAXIS_T2865099254_H
#define RECTTRANSFORMFLIPLAYOUTAXIS_T2865099254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis
struct  RectTransformFlipLayoutAxis_t2865099254  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis/RectTransformFlipOptions HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::axis
	int32_t ___axis_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::keepPositioning
	FsmBool_t163807967 * ___keepPositioning_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::recursive
	FsmBool_t163807967 * ___recursive_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2865099254, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_axis_15() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2865099254, ___axis_15)); }
	inline int32_t get_axis_15() const { return ___axis_15; }
	inline int32_t* get_address_of_axis_15() { return &___axis_15; }
	inline void set_axis_15(int32_t value)
	{
		___axis_15 = value;
	}

	inline static int32_t get_offset_of_keepPositioning_16() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2865099254, ___keepPositioning_16)); }
	inline FsmBool_t163807967 * get_keepPositioning_16() const { return ___keepPositioning_16; }
	inline FsmBool_t163807967 ** get_address_of_keepPositioning_16() { return &___keepPositioning_16; }
	inline void set_keepPositioning_16(FsmBool_t163807967 * value)
	{
		___keepPositioning_16 = value;
		Il2CppCodeGenWriteBarrier((&___keepPositioning_16), value);
	}

	inline static int32_t get_offset_of_recursive_17() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2865099254, ___recursive_17)); }
	inline FsmBool_t163807967 * get_recursive_17() const { return ___recursive_17; }
	inline FsmBool_t163807967 ** get_address_of_recursive_17() { return &___recursive_17; }
	inline void set_recursive_17(FsmBool_t163807967 * value)
	{
		___recursive_17 = value;
		Il2CppCodeGenWriteBarrier((&___recursive_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMFLIPLAYOUTAXIS_T2865099254_H
#ifndef RECTTRANSFORMSCREENPOINTTOLOCALPOINTINRECTANGLE_T1705222147_H
#define RECTTRANSFORMSCREENPOINTTOLOCALPOINTINRECTANGLE_T1705222147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle
struct  RectTransformScreenPointToLocalPointInRectangle_t1705222147  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::screenPointVector2
	FsmVector2_t2965096677 * ___screenPointVector2_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::orScreenPointVector3
	FsmVector3_t626444517 * ___orScreenPointVector3_16;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::normalizedScreenPoint
	bool ___normalizedScreenPoint_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::camera
	FsmGameObject_t3581898942 * ___camera_18;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::everyFrame
	bool ___everyFrame_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::localPosition
	FsmVector3_t626444517 * ___localPosition_20;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::localPosition2d
	FsmVector2_t2965096677 * ___localPosition2d_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::isHit
	FsmBool_t163807967 * ___isHit_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::noHitEvent
	FsmEvent_t3736299882 * ___noHitEvent_24;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::_rt
	RectTransform_t3704657025 * ____rt_25;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::_camera
	Camera_t4157153871 * ____camera_26;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_screenPointVector2_15() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___screenPointVector2_15)); }
	inline FsmVector2_t2965096677 * get_screenPointVector2_15() const { return ___screenPointVector2_15; }
	inline FsmVector2_t2965096677 ** get_address_of_screenPointVector2_15() { return &___screenPointVector2_15; }
	inline void set_screenPointVector2_15(FsmVector2_t2965096677 * value)
	{
		___screenPointVector2_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenPointVector2_15), value);
	}

	inline static int32_t get_offset_of_orScreenPointVector3_16() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___orScreenPointVector3_16)); }
	inline FsmVector3_t626444517 * get_orScreenPointVector3_16() const { return ___orScreenPointVector3_16; }
	inline FsmVector3_t626444517 ** get_address_of_orScreenPointVector3_16() { return &___orScreenPointVector3_16; }
	inline void set_orScreenPointVector3_16(FsmVector3_t626444517 * value)
	{
		___orScreenPointVector3_16 = value;
		Il2CppCodeGenWriteBarrier((&___orScreenPointVector3_16), value);
	}

	inline static int32_t get_offset_of_normalizedScreenPoint_17() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___normalizedScreenPoint_17)); }
	inline bool get_normalizedScreenPoint_17() const { return ___normalizedScreenPoint_17; }
	inline bool* get_address_of_normalizedScreenPoint_17() { return &___normalizedScreenPoint_17; }
	inline void set_normalizedScreenPoint_17(bool value)
	{
		___normalizedScreenPoint_17 = value;
	}

	inline static int32_t get_offset_of_camera_18() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___camera_18)); }
	inline FsmGameObject_t3581898942 * get_camera_18() const { return ___camera_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_camera_18() { return &___camera_18; }
	inline void set_camera_18(FsmGameObject_t3581898942 * value)
	{
		___camera_18 = value;
		Il2CppCodeGenWriteBarrier((&___camera_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_localPosition_20() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___localPosition_20)); }
	inline FsmVector3_t626444517 * get_localPosition_20() const { return ___localPosition_20; }
	inline FsmVector3_t626444517 ** get_address_of_localPosition_20() { return &___localPosition_20; }
	inline void set_localPosition_20(FsmVector3_t626444517 * value)
	{
		___localPosition_20 = value;
		Il2CppCodeGenWriteBarrier((&___localPosition_20), value);
	}

	inline static int32_t get_offset_of_localPosition2d_21() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___localPosition2d_21)); }
	inline FsmVector2_t2965096677 * get_localPosition2d_21() const { return ___localPosition2d_21; }
	inline FsmVector2_t2965096677 ** get_address_of_localPosition2d_21() { return &___localPosition2d_21; }
	inline void set_localPosition2d_21(FsmVector2_t2965096677 * value)
	{
		___localPosition2d_21 = value;
		Il2CppCodeGenWriteBarrier((&___localPosition2d_21), value);
	}

	inline static int32_t get_offset_of_isHit_22() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___isHit_22)); }
	inline FsmBool_t163807967 * get_isHit_22() const { return ___isHit_22; }
	inline FsmBool_t163807967 ** get_address_of_isHit_22() { return &___isHit_22; }
	inline void set_isHit_22(FsmBool_t163807967 * value)
	{
		___isHit_22 = value;
		Il2CppCodeGenWriteBarrier((&___isHit_22), value);
	}

	inline static int32_t get_offset_of_hitEvent_23() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___hitEvent_23)); }
	inline FsmEvent_t3736299882 * get_hitEvent_23() const { return ___hitEvent_23; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_23() { return &___hitEvent_23; }
	inline void set_hitEvent_23(FsmEvent_t3736299882 * value)
	{
		___hitEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_23), value);
	}

	inline static int32_t get_offset_of_noHitEvent_24() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ___noHitEvent_24)); }
	inline FsmEvent_t3736299882 * get_noHitEvent_24() const { return ___noHitEvent_24; }
	inline FsmEvent_t3736299882 ** get_address_of_noHitEvent_24() { return &___noHitEvent_24; }
	inline void set_noHitEvent_24(FsmEvent_t3736299882 * value)
	{
		___noHitEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___noHitEvent_24), value);
	}

	inline static int32_t get_offset_of__rt_25() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ____rt_25)); }
	inline RectTransform_t3704657025 * get__rt_25() const { return ____rt_25; }
	inline RectTransform_t3704657025 ** get_address_of__rt_25() { return &____rt_25; }
	inline void set__rt_25(RectTransform_t3704657025 * value)
	{
		____rt_25 = value;
		Il2CppCodeGenWriteBarrier((&____rt_25), value);
	}

	inline static int32_t get_offset_of__camera_26() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t1705222147, ____camera_26)); }
	inline Camera_t4157153871 * get__camera_26() const { return ____camera_26; }
	inline Camera_t4157153871 ** get_address_of__camera_26() { return &____camera_26; }
	inline void set__camera_26(Camera_t4157153871 * value)
	{
		____camera_26 = value;
		Il2CppCodeGenWriteBarrier((&____camera_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSCREENPOINTTOLOCALPOINTINRECTANGLE_T1705222147_H
#ifndef RECTTRANSFORMSCREENPOINTTOWORLDPOINTINRECTANGLE_T163018785_H
#define RECTTRANSFORMSCREENPOINTTOWORLDPOINTINRECTANGLE_T163018785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle
struct  RectTransformScreenPointToWorldPointInRectangle_t163018785  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::screenPointVector2
	FsmVector2_t2965096677 * ___screenPointVector2_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::orScreenPointVector3
	FsmVector3_t626444517 * ___orScreenPointVector3_16;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::normalizedScreenPoint
	bool ___normalizedScreenPoint_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::camera
	FsmGameObject_t3581898942 * ___camera_18;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::everyFrame
	bool ___everyFrame_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::worldPosition
	FsmVector3_t626444517 * ___worldPosition_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::isHit
	FsmBool_t163807967 * ___isHit_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::hitEvent
	FsmEvent_t3736299882 * ___hitEvent_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::noHitEvent
	FsmEvent_t3736299882 * ___noHitEvent_23;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::_rt
	RectTransform_t3704657025 * ____rt_24;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::_camera
	Camera_t4157153871 * ____camera_25;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_screenPointVector2_15() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___screenPointVector2_15)); }
	inline FsmVector2_t2965096677 * get_screenPointVector2_15() const { return ___screenPointVector2_15; }
	inline FsmVector2_t2965096677 ** get_address_of_screenPointVector2_15() { return &___screenPointVector2_15; }
	inline void set_screenPointVector2_15(FsmVector2_t2965096677 * value)
	{
		___screenPointVector2_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenPointVector2_15), value);
	}

	inline static int32_t get_offset_of_orScreenPointVector3_16() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___orScreenPointVector3_16)); }
	inline FsmVector3_t626444517 * get_orScreenPointVector3_16() const { return ___orScreenPointVector3_16; }
	inline FsmVector3_t626444517 ** get_address_of_orScreenPointVector3_16() { return &___orScreenPointVector3_16; }
	inline void set_orScreenPointVector3_16(FsmVector3_t626444517 * value)
	{
		___orScreenPointVector3_16 = value;
		Il2CppCodeGenWriteBarrier((&___orScreenPointVector3_16), value);
	}

	inline static int32_t get_offset_of_normalizedScreenPoint_17() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___normalizedScreenPoint_17)); }
	inline bool get_normalizedScreenPoint_17() const { return ___normalizedScreenPoint_17; }
	inline bool* get_address_of_normalizedScreenPoint_17() { return &___normalizedScreenPoint_17; }
	inline void set_normalizedScreenPoint_17(bool value)
	{
		___normalizedScreenPoint_17 = value;
	}

	inline static int32_t get_offset_of_camera_18() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___camera_18)); }
	inline FsmGameObject_t3581898942 * get_camera_18() const { return ___camera_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_camera_18() { return &___camera_18; }
	inline void set_camera_18(FsmGameObject_t3581898942 * value)
	{
		___camera_18 = value;
		Il2CppCodeGenWriteBarrier((&___camera_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of_worldPosition_20() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___worldPosition_20)); }
	inline FsmVector3_t626444517 * get_worldPosition_20() const { return ___worldPosition_20; }
	inline FsmVector3_t626444517 ** get_address_of_worldPosition_20() { return &___worldPosition_20; }
	inline void set_worldPosition_20(FsmVector3_t626444517 * value)
	{
		___worldPosition_20 = value;
		Il2CppCodeGenWriteBarrier((&___worldPosition_20), value);
	}

	inline static int32_t get_offset_of_isHit_21() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___isHit_21)); }
	inline FsmBool_t163807967 * get_isHit_21() const { return ___isHit_21; }
	inline FsmBool_t163807967 ** get_address_of_isHit_21() { return &___isHit_21; }
	inline void set_isHit_21(FsmBool_t163807967 * value)
	{
		___isHit_21 = value;
		Il2CppCodeGenWriteBarrier((&___isHit_21), value);
	}

	inline static int32_t get_offset_of_hitEvent_22() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___hitEvent_22)); }
	inline FsmEvent_t3736299882 * get_hitEvent_22() const { return ___hitEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_hitEvent_22() { return &___hitEvent_22; }
	inline void set_hitEvent_22(FsmEvent_t3736299882 * value)
	{
		___hitEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___hitEvent_22), value);
	}

	inline static int32_t get_offset_of_noHitEvent_23() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ___noHitEvent_23)); }
	inline FsmEvent_t3736299882 * get_noHitEvent_23() const { return ___noHitEvent_23; }
	inline FsmEvent_t3736299882 ** get_address_of_noHitEvent_23() { return &___noHitEvent_23; }
	inline void set_noHitEvent_23(FsmEvent_t3736299882 * value)
	{
		___noHitEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___noHitEvent_23), value);
	}

	inline static int32_t get_offset_of__rt_24() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ____rt_24)); }
	inline RectTransform_t3704657025 * get__rt_24() const { return ____rt_24; }
	inline RectTransform_t3704657025 ** get_address_of__rt_24() { return &____rt_24; }
	inline void set__rt_24(RectTransform_t3704657025 * value)
	{
		____rt_24 = value;
		Il2CppCodeGenWriteBarrier((&____rt_24), value);
	}

	inline static int32_t get_offset_of__camera_25() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t163018785, ____camera_25)); }
	inline Camera_t4157153871 * get__camera_25() const { return ____camera_25; }
	inline Camera_t4157153871 ** get_address_of__camera_25() { return &____camera_25; }
	inline void set__camera_25(Camera_t4157153871 * value)
	{
		____camera_25 = value;
		Il2CppCodeGenWriteBarrier((&____camera_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSCREENPOINTTOWORLDPOINTINRECTANGLE_T163018785_H
#ifndef SENDACTIVESCENECHANGEDEVENT_T2439287554_H
#define SENDACTIVESCENECHANGEDEVENT_T2439287554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendActiveSceneChangedEvent
struct  SendActiveSceneChangedEvent_t2439287554  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SendActiveSceneChangedEvent::activeSceneChanged
	FsmEvent_t3736299882 * ___activeSceneChanged_14;

public:
	inline static int32_t get_offset_of_activeSceneChanged_14() { return static_cast<int32_t>(offsetof(SendActiveSceneChangedEvent_t2439287554, ___activeSceneChanged_14)); }
	inline FsmEvent_t3736299882 * get_activeSceneChanged_14() const { return ___activeSceneChanged_14; }
	inline FsmEvent_t3736299882 ** get_address_of_activeSceneChanged_14() { return &___activeSceneChanged_14; }
	inline void set_activeSceneChanged_14(FsmEvent_t3736299882 * value)
	{
		___activeSceneChanged_14 = value;
		Il2CppCodeGenWriteBarrier((&___activeSceneChanged_14), value);
	}
};

struct SendActiveSceneChangedEvent_t2439287554_StaticFields
{
public:
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.SendActiveSceneChangedEvent::lastPreviousActiveScene
	Scene_t2348375561  ___lastPreviousActiveScene_15;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.SendActiveSceneChangedEvent::lastNewActiveScene
	Scene_t2348375561  ___lastNewActiveScene_16;

public:
	inline static int32_t get_offset_of_lastPreviousActiveScene_15() { return static_cast<int32_t>(offsetof(SendActiveSceneChangedEvent_t2439287554_StaticFields, ___lastPreviousActiveScene_15)); }
	inline Scene_t2348375561  get_lastPreviousActiveScene_15() const { return ___lastPreviousActiveScene_15; }
	inline Scene_t2348375561 * get_address_of_lastPreviousActiveScene_15() { return &___lastPreviousActiveScene_15; }
	inline void set_lastPreviousActiveScene_15(Scene_t2348375561  value)
	{
		___lastPreviousActiveScene_15 = value;
	}

	inline static int32_t get_offset_of_lastNewActiveScene_16() { return static_cast<int32_t>(offsetof(SendActiveSceneChangedEvent_t2439287554_StaticFields, ___lastNewActiveScene_16)); }
	inline Scene_t2348375561  get_lastNewActiveScene_16() const { return ___lastNewActiveScene_16; }
	inline Scene_t2348375561 * get_address_of_lastNewActiveScene_16() { return &___lastNewActiveScene_16; }
	inline void set_lastNewActiveScene_16(Scene_t2348375561  value)
	{
		___lastNewActiveScene_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDACTIVESCENECHANGEDEVENT_T2439287554_H
#ifndef SENDSCENELOADEDEVENT_T903181904_H
#define SENDSCENELOADEDEVENT_T903181904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendSceneLoadedEvent
struct  SendSceneLoadedEvent_t903181904  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SendSceneLoadedEvent::sceneLoaded
	FsmEvent_t3736299882 * ___sceneLoaded_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SendSceneLoadedEvent::sceneLoadedSafe
	FsmEvent_t3736299882 * ___sceneLoadedSafe_15;
	// System.Int32 HutongGames.PlayMaker.Actions.SendSceneLoadedEvent::_loaded
	int32_t ____loaded_18;

public:
	inline static int32_t get_offset_of_sceneLoaded_14() { return static_cast<int32_t>(offsetof(SendSceneLoadedEvent_t903181904, ___sceneLoaded_14)); }
	inline FsmEvent_t3736299882 * get_sceneLoaded_14() const { return ___sceneLoaded_14; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneLoaded_14() { return &___sceneLoaded_14; }
	inline void set_sceneLoaded_14(FsmEvent_t3736299882 * value)
	{
		___sceneLoaded_14 = value;
		Il2CppCodeGenWriteBarrier((&___sceneLoaded_14), value);
	}

	inline static int32_t get_offset_of_sceneLoadedSafe_15() { return static_cast<int32_t>(offsetof(SendSceneLoadedEvent_t903181904, ___sceneLoadedSafe_15)); }
	inline FsmEvent_t3736299882 * get_sceneLoadedSafe_15() const { return ___sceneLoadedSafe_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneLoadedSafe_15() { return &___sceneLoadedSafe_15; }
	inline void set_sceneLoadedSafe_15(FsmEvent_t3736299882 * value)
	{
		___sceneLoadedSafe_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneLoadedSafe_15), value);
	}

	inline static int32_t get_offset_of__loaded_18() { return static_cast<int32_t>(offsetof(SendSceneLoadedEvent_t903181904, ____loaded_18)); }
	inline int32_t get__loaded_18() const { return ____loaded_18; }
	inline int32_t* get_address_of__loaded_18() { return &____loaded_18; }
	inline void set__loaded_18(int32_t value)
	{
		____loaded_18 = value;
	}
};

struct SendSceneLoadedEvent_t903181904_StaticFields
{
public:
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.SendSceneLoadedEvent::lastLoadedScene
	Scene_t2348375561  ___lastLoadedScene_16;
	// UnityEngine.SceneManagement.LoadSceneMode HutongGames.PlayMaker.Actions.SendSceneLoadedEvent::lastLoadedMode
	int32_t ___lastLoadedMode_17;

public:
	inline static int32_t get_offset_of_lastLoadedScene_16() { return static_cast<int32_t>(offsetof(SendSceneLoadedEvent_t903181904_StaticFields, ___lastLoadedScene_16)); }
	inline Scene_t2348375561  get_lastLoadedScene_16() const { return ___lastLoadedScene_16; }
	inline Scene_t2348375561 * get_address_of_lastLoadedScene_16() { return &___lastLoadedScene_16; }
	inline void set_lastLoadedScene_16(Scene_t2348375561  value)
	{
		___lastLoadedScene_16 = value;
	}

	inline static int32_t get_offset_of_lastLoadedMode_17() { return static_cast<int32_t>(offsetof(SendSceneLoadedEvent_t903181904_StaticFields, ___lastLoadedMode_17)); }
	inline int32_t get_lastLoadedMode_17() const { return ___lastLoadedMode_17; }
	inline int32_t* get_address_of_lastLoadedMode_17() { return &___lastLoadedMode_17; }
	inline void set_lastLoadedMode_17(int32_t value)
	{
		___lastLoadedMode_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDSCENELOADEDEVENT_T903181904_H
#ifndef SENDSCENEUNLOADEDEVENT_T352636565_H
#define SENDSCENEUNLOADEDEVENT_T352636565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendSceneUnloadedEvent
struct  SendSceneUnloadedEvent_t352636565  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SendSceneUnloadedEvent::sceneUnloaded
	FsmEvent_t3736299882 * ___sceneUnloaded_14;

public:
	inline static int32_t get_offset_of_sceneUnloaded_14() { return static_cast<int32_t>(offsetof(SendSceneUnloadedEvent_t352636565, ___sceneUnloaded_14)); }
	inline FsmEvent_t3736299882 * get_sceneUnloaded_14() const { return ___sceneUnloaded_14; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneUnloaded_14() { return &___sceneUnloaded_14; }
	inline void set_sceneUnloaded_14(FsmEvent_t3736299882 * value)
	{
		___sceneUnloaded_14 = value;
		Il2CppCodeGenWriteBarrier((&___sceneUnloaded_14), value);
	}
};

struct SendSceneUnloadedEvent_t352636565_StaticFields
{
public:
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.SendSceneUnloadedEvent::lastUnLoadedScene
	Scene_t2348375561  ___lastUnLoadedScene_15;

public:
	inline static int32_t get_offset_of_lastUnLoadedScene_15() { return static_cast<int32_t>(offsetof(SendSceneUnloadedEvent_t352636565_StaticFields, ___lastUnLoadedScene_15)); }
	inline Scene_t2348375561  get_lastUnLoadedScene_15() const { return ___lastUnLoadedScene_15; }
	inline Scene_t2348375561 * get_address_of_lastUnLoadedScene_15() { return &___lastUnLoadedScene_15; }
	inline void set_lastUnLoadedScene_15(Scene_t2348375561  value)
	{
		___lastUnLoadedScene_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDSCENEUNLOADEDEVENT_T352636565_H
#ifndef SETACTIVESCENE_T3066865810_H
#define SETACTIVESCENE_T3066865810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetActiveScene
struct  SetActiveScene_t3066865810  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.SetActiveScene/SceneReferenceOptions HutongGames.PlayMaker.Actions.SetActiveScene::sceneReference
	int32_t ___sceneReference_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetActiveScene::sceneByName
	FsmString_t1785915204 * ___sceneByName_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetActiveScene::sceneAtBuildIndex
	FsmInt_t874273141 * ___sceneAtBuildIndex_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetActiveScene::sceneAtIndex
	FsmInt_t874273141 * ___sceneAtIndex_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetActiveScene::sceneByPath
	FsmString_t1785915204 * ___sceneByPath_18;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetActiveScene::sceneByGameObject
	FsmOwnerDefault_t3590610434 * ___sceneByGameObject_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetActiveScene::success
	FsmBool_t163807967 * ___success_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SetActiveScene::successEvent
	FsmEvent_t3736299882 * ___successEvent_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetActiveScene::sceneFound
	FsmBool_t163807967 * ___sceneFound_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SetActiveScene::sceneNotActivatedEvent
	FsmEvent_t3736299882 * ___sceneNotActivatedEvent_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SetActiveScene::sceneNotFoundEvent
	FsmEvent_t3736299882 * ___sceneNotFoundEvent_24;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.SetActiveScene::_scene
	Scene_t2348375561  ____scene_25;
	// System.Boolean HutongGames.PlayMaker.Actions.SetActiveScene::_sceneFound
	bool ____sceneFound_26;
	// System.Boolean HutongGames.PlayMaker.Actions.SetActiveScene::_success
	bool ____success_27;

public:
	inline static int32_t get_offset_of_sceneReference_14() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneReference_14)); }
	inline int32_t get_sceneReference_14() const { return ___sceneReference_14; }
	inline int32_t* get_address_of_sceneReference_14() { return &___sceneReference_14; }
	inline void set_sceneReference_14(int32_t value)
	{
		___sceneReference_14 = value;
	}

	inline static int32_t get_offset_of_sceneByName_15() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneByName_15)); }
	inline FsmString_t1785915204 * get_sceneByName_15() const { return ___sceneByName_15; }
	inline FsmString_t1785915204 ** get_address_of_sceneByName_15() { return &___sceneByName_15; }
	inline void set_sceneByName_15(FsmString_t1785915204 * value)
	{
		___sceneByName_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByName_15), value);
	}

	inline static int32_t get_offset_of_sceneAtBuildIndex_16() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneAtBuildIndex_16)); }
	inline FsmInt_t874273141 * get_sceneAtBuildIndex_16() const { return ___sceneAtBuildIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtBuildIndex_16() { return &___sceneAtBuildIndex_16; }
	inline void set_sceneAtBuildIndex_16(FsmInt_t874273141 * value)
	{
		___sceneAtBuildIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtBuildIndex_16), value);
	}

	inline static int32_t get_offset_of_sceneAtIndex_17() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneAtIndex_17)); }
	inline FsmInt_t874273141 * get_sceneAtIndex_17() const { return ___sceneAtIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtIndex_17() { return &___sceneAtIndex_17; }
	inline void set_sceneAtIndex_17(FsmInt_t874273141 * value)
	{
		___sceneAtIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtIndex_17), value);
	}

	inline static int32_t get_offset_of_sceneByPath_18() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneByPath_18)); }
	inline FsmString_t1785915204 * get_sceneByPath_18() const { return ___sceneByPath_18; }
	inline FsmString_t1785915204 ** get_address_of_sceneByPath_18() { return &___sceneByPath_18; }
	inline void set_sceneByPath_18(FsmString_t1785915204 * value)
	{
		___sceneByPath_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByPath_18), value);
	}

	inline static int32_t get_offset_of_sceneByGameObject_19() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneByGameObject_19)); }
	inline FsmOwnerDefault_t3590610434 * get_sceneByGameObject_19() const { return ___sceneByGameObject_19; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_sceneByGameObject_19() { return &___sceneByGameObject_19; }
	inline void set_sceneByGameObject_19(FsmOwnerDefault_t3590610434 * value)
	{
		___sceneByGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByGameObject_19), value);
	}

	inline static int32_t get_offset_of_success_20() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___success_20)); }
	inline FsmBool_t163807967 * get_success_20() const { return ___success_20; }
	inline FsmBool_t163807967 ** get_address_of_success_20() { return &___success_20; }
	inline void set_success_20(FsmBool_t163807967 * value)
	{
		___success_20 = value;
		Il2CppCodeGenWriteBarrier((&___success_20), value);
	}

	inline static int32_t get_offset_of_successEvent_21() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___successEvent_21)); }
	inline FsmEvent_t3736299882 * get_successEvent_21() const { return ___successEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_successEvent_21() { return &___successEvent_21; }
	inline void set_successEvent_21(FsmEvent_t3736299882 * value)
	{
		___successEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___successEvent_21), value);
	}

	inline static int32_t get_offset_of_sceneFound_22() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneFound_22)); }
	inline FsmBool_t163807967 * get_sceneFound_22() const { return ___sceneFound_22; }
	inline FsmBool_t163807967 ** get_address_of_sceneFound_22() { return &___sceneFound_22; }
	inline void set_sceneFound_22(FsmBool_t163807967 * value)
	{
		___sceneFound_22 = value;
		Il2CppCodeGenWriteBarrier((&___sceneFound_22), value);
	}

	inline static int32_t get_offset_of_sceneNotActivatedEvent_23() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneNotActivatedEvent_23)); }
	inline FsmEvent_t3736299882 * get_sceneNotActivatedEvent_23() const { return ___sceneNotActivatedEvent_23; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneNotActivatedEvent_23() { return &___sceneNotActivatedEvent_23; }
	inline void set_sceneNotActivatedEvent_23(FsmEvent_t3736299882 * value)
	{
		___sceneNotActivatedEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___sceneNotActivatedEvent_23), value);
	}

	inline static int32_t get_offset_of_sceneNotFoundEvent_24() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ___sceneNotFoundEvent_24)); }
	inline FsmEvent_t3736299882 * get_sceneNotFoundEvent_24() const { return ___sceneNotFoundEvent_24; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneNotFoundEvent_24() { return &___sceneNotFoundEvent_24; }
	inline void set_sceneNotFoundEvent_24(FsmEvent_t3736299882 * value)
	{
		___sceneNotFoundEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___sceneNotFoundEvent_24), value);
	}

	inline static int32_t get_offset_of__scene_25() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ____scene_25)); }
	inline Scene_t2348375561  get__scene_25() const { return ____scene_25; }
	inline Scene_t2348375561 * get_address_of__scene_25() { return &____scene_25; }
	inline void set__scene_25(Scene_t2348375561  value)
	{
		____scene_25 = value;
	}

	inline static int32_t get_offset_of__sceneFound_26() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ____sceneFound_26)); }
	inline bool get__sceneFound_26() const { return ____sceneFound_26; }
	inline bool* get_address_of__sceneFound_26() { return &____sceneFound_26; }
	inline void set__sceneFound_26(bool value)
	{
		____sceneFound_26 = value;
	}

	inline static int32_t get_offset_of__success_27() { return static_cast<int32_t>(offsetof(SetActiveScene_t3066865810, ____success_27)); }
	inline bool get__success_27() const { return ____success_27; }
	inline bool* get_address_of__success_27() { return &____success_27; }
	inline void set__success_27(bool value)
	{
		____success_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETACTIVESCENE_T3066865810_H
#ifndef SETAMBIENTLIGHT_T2149961347_H
#define SETAMBIENTLIGHT_T2149961347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetAmbientLight
struct  SetAmbientLight_t2149961347  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetAmbientLight::ambientColor
	FsmColor_t1738900188 * ___ambientColor_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetAmbientLight::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_ambientColor_14() { return static_cast<int32_t>(offsetof(SetAmbientLight_t2149961347, ___ambientColor_14)); }
	inline FsmColor_t1738900188 * get_ambientColor_14() const { return ___ambientColor_14; }
	inline FsmColor_t1738900188 ** get_address_of_ambientColor_14() { return &___ambientColor_14; }
	inline void set_ambientColor_14(FsmColor_t1738900188 * value)
	{
		___ambientColor_14 = value;
		Il2CppCodeGenWriteBarrier((&___ambientColor_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetAmbientLight_t2149961347, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETAMBIENTLIGHT_T2149961347_H
#ifndef SETFLARESTRENGTH_T1365963185_H
#define SETFLARESTRENGTH_T1365963185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFlareStrength
struct  SetFlareStrength_t1365963185  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFlareStrength::flareStrength
	FsmFloat_t2883254149 * ___flareStrength_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFlareStrength::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_flareStrength_14() { return static_cast<int32_t>(offsetof(SetFlareStrength_t1365963185, ___flareStrength_14)); }
	inline FsmFloat_t2883254149 * get_flareStrength_14() const { return ___flareStrength_14; }
	inline FsmFloat_t2883254149 ** get_address_of_flareStrength_14() { return &___flareStrength_14; }
	inline void set_flareStrength_14(FsmFloat_t2883254149 * value)
	{
		___flareStrength_14 = value;
		Il2CppCodeGenWriteBarrier((&___flareStrength_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetFlareStrength_t1365963185, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFLARESTRENGTH_T1365963185_H
#ifndef SETFOGCOLOR_T3043078047_H
#define SETFOGCOLOR_T3043078047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFogColor
struct  SetFogColor_t3043078047  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetFogColor::fogColor
	FsmColor_t1738900188 * ___fogColor_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFogColor::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_fogColor_14() { return static_cast<int32_t>(offsetof(SetFogColor_t3043078047, ___fogColor_14)); }
	inline FsmColor_t1738900188 * get_fogColor_14() const { return ___fogColor_14; }
	inline FsmColor_t1738900188 ** get_address_of_fogColor_14() { return &___fogColor_14; }
	inline void set_fogColor_14(FsmColor_t1738900188 * value)
	{
		___fogColor_14 = value;
		Il2CppCodeGenWriteBarrier((&___fogColor_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetFogColor_t3043078047, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFOGCOLOR_T3043078047_H
#ifndef SETFOGDENSITY_T1161296091_H
#define SETFOGDENSITY_T1161296091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFogDensity
struct  SetFogDensity_t1161296091  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFogDensity::fogDensity
	FsmFloat_t2883254149 * ___fogDensity_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFogDensity::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_fogDensity_14() { return static_cast<int32_t>(offsetof(SetFogDensity_t1161296091, ___fogDensity_14)); }
	inline FsmFloat_t2883254149 * get_fogDensity_14() const { return ___fogDensity_14; }
	inline FsmFloat_t2883254149 ** get_address_of_fogDensity_14() { return &___fogDensity_14; }
	inline void set_fogDensity_14(FsmFloat_t2883254149 * value)
	{
		___fogDensity_14 = value;
		Il2CppCodeGenWriteBarrier((&___fogDensity_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetFogDensity_t1161296091, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFOGDENSITY_T1161296091_H
#ifndef SETHALOSTRENGTH_T3595622030_H
#define SETHALOSTRENGTH_T3595622030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetHaloStrength
struct  SetHaloStrength_t3595622030  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetHaloStrength::haloStrength
	FsmFloat_t2883254149 * ___haloStrength_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetHaloStrength::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_haloStrength_14() { return static_cast<int32_t>(offsetof(SetHaloStrength_t3595622030, ___haloStrength_14)); }
	inline FsmFloat_t2883254149 * get_haloStrength_14() const { return ___haloStrength_14; }
	inline FsmFloat_t2883254149 ** get_address_of_haloStrength_14() { return &___haloStrength_14; }
	inline void set_haloStrength_14(FsmFloat_t2883254149 * value)
	{
		___haloStrength_14 = value;
		Il2CppCodeGenWriteBarrier((&___haloStrength_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetHaloStrength_t3595622030, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETHALOSTRENGTH_T3595622030_H
#ifndef SETPROCEDURALBOOLEAN_T384194045_H
#define SETPROCEDURALBOOLEAN_T384194045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralBoolean
struct  SetProceduralBoolean_t384194045  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralBoolean::substanceMaterial
	FsmMaterial_t2057874452 * ___substanceMaterial_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralBoolean::boolProperty
	FsmString_t1785915204 * ___boolProperty_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetProceduralBoolean::boolValue
	FsmBool_t163807967 * ___boolValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralBoolean::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_substanceMaterial_14() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t384194045, ___substanceMaterial_14)); }
	inline FsmMaterial_t2057874452 * get_substanceMaterial_14() const { return ___substanceMaterial_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_substanceMaterial_14() { return &___substanceMaterial_14; }
	inline void set_substanceMaterial_14(FsmMaterial_t2057874452 * value)
	{
		___substanceMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___substanceMaterial_14), value);
	}

	inline static int32_t get_offset_of_boolProperty_15() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t384194045, ___boolProperty_15)); }
	inline FsmString_t1785915204 * get_boolProperty_15() const { return ___boolProperty_15; }
	inline FsmString_t1785915204 ** get_address_of_boolProperty_15() { return &___boolProperty_15; }
	inline void set_boolProperty_15(FsmString_t1785915204 * value)
	{
		___boolProperty_15 = value;
		Il2CppCodeGenWriteBarrier((&___boolProperty_15), value);
	}

	inline static int32_t get_offset_of_boolValue_16() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t384194045, ___boolValue_16)); }
	inline FsmBool_t163807967 * get_boolValue_16() const { return ___boolValue_16; }
	inline FsmBool_t163807967 ** get_address_of_boolValue_16() { return &___boolValue_16; }
	inline void set_boolValue_16(FsmBool_t163807967 * value)
	{
		___boolValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___boolValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetProceduralBoolean_t384194045, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROCEDURALBOOLEAN_T384194045_H
#ifndef SETPROCEDURALCOLOR_T3770124595_H
#define SETPROCEDURALCOLOR_T3770124595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralColor
struct  SetProceduralColor_t3770124595  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralColor::substanceMaterial
	FsmMaterial_t2057874452 * ___substanceMaterial_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralColor::colorProperty
	FsmString_t1785915204 * ___colorProperty_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetProceduralColor::colorValue
	FsmColor_t1738900188 * ___colorValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralColor::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_substanceMaterial_14() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3770124595, ___substanceMaterial_14)); }
	inline FsmMaterial_t2057874452 * get_substanceMaterial_14() const { return ___substanceMaterial_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_substanceMaterial_14() { return &___substanceMaterial_14; }
	inline void set_substanceMaterial_14(FsmMaterial_t2057874452 * value)
	{
		___substanceMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___substanceMaterial_14), value);
	}

	inline static int32_t get_offset_of_colorProperty_15() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3770124595, ___colorProperty_15)); }
	inline FsmString_t1785915204 * get_colorProperty_15() const { return ___colorProperty_15; }
	inline FsmString_t1785915204 ** get_address_of_colorProperty_15() { return &___colorProperty_15; }
	inline void set_colorProperty_15(FsmString_t1785915204 * value)
	{
		___colorProperty_15 = value;
		Il2CppCodeGenWriteBarrier((&___colorProperty_15), value);
	}

	inline static int32_t get_offset_of_colorValue_16() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3770124595, ___colorValue_16)); }
	inline FsmColor_t1738900188 * get_colorValue_16() const { return ___colorValue_16; }
	inline FsmColor_t1738900188 ** get_address_of_colorValue_16() { return &___colorValue_16; }
	inline void set_colorValue_16(FsmColor_t1738900188 * value)
	{
		___colorValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___colorValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetProceduralColor_t3770124595, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROCEDURALCOLOR_T3770124595_H
#ifndef SETPROCEDURALFLOAT_T1084947234_H
#define SETPROCEDURALFLOAT_T1084947234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralFloat
struct  SetProceduralFloat_t1084947234  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralFloat::substanceMaterial
	FsmMaterial_t2057874452 * ___substanceMaterial_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralFloat::floatProperty
	FsmString_t1785915204 * ___floatProperty_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetProceduralFloat::floatValue
	FsmFloat_t2883254149 * ___floatValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralFloat::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_substanceMaterial_14() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t1084947234, ___substanceMaterial_14)); }
	inline FsmMaterial_t2057874452 * get_substanceMaterial_14() const { return ___substanceMaterial_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_substanceMaterial_14() { return &___substanceMaterial_14; }
	inline void set_substanceMaterial_14(FsmMaterial_t2057874452 * value)
	{
		___substanceMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___substanceMaterial_14), value);
	}

	inline static int32_t get_offset_of_floatProperty_15() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t1084947234, ___floatProperty_15)); }
	inline FsmString_t1785915204 * get_floatProperty_15() const { return ___floatProperty_15; }
	inline FsmString_t1785915204 ** get_address_of_floatProperty_15() { return &___floatProperty_15; }
	inline void set_floatProperty_15(FsmString_t1785915204 * value)
	{
		___floatProperty_15 = value;
		Il2CppCodeGenWriteBarrier((&___floatProperty_15), value);
	}

	inline static int32_t get_offset_of_floatValue_16() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t1084947234, ___floatValue_16)); }
	inline FsmFloat_t2883254149 * get_floatValue_16() const { return ___floatValue_16; }
	inline FsmFloat_t2883254149 ** get_address_of_floatValue_16() { return &___floatValue_16; }
	inline void set_floatValue_16(FsmFloat_t2883254149 * value)
	{
		___floatValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___floatValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetProceduralFloat_t1084947234, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROCEDURALFLOAT_T1084947234_H
#ifndef SETPROCEDURALVECTOR2_T2655254814_H
#define SETPROCEDURALVECTOR2_T2655254814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralVector2
struct  SetProceduralVector2_t2655254814  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralVector2::substanceMaterial
	FsmMaterial_t2057874452 * ___substanceMaterial_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralVector2::vector2Property
	FsmString_t1785915204 * ___vector2Property_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetProceduralVector2::vector2Value
	FsmVector2_t2965096677 * ___vector2Value_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralVector2::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_substanceMaterial_14() { return static_cast<int32_t>(offsetof(SetProceduralVector2_t2655254814, ___substanceMaterial_14)); }
	inline FsmMaterial_t2057874452 * get_substanceMaterial_14() const { return ___substanceMaterial_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_substanceMaterial_14() { return &___substanceMaterial_14; }
	inline void set_substanceMaterial_14(FsmMaterial_t2057874452 * value)
	{
		___substanceMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___substanceMaterial_14), value);
	}

	inline static int32_t get_offset_of_vector2Property_15() { return static_cast<int32_t>(offsetof(SetProceduralVector2_t2655254814, ___vector2Property_15)); }
	inline FsmString_t1785915204 * get_vector2Property_15() const { return ___vector2Property_15; }
	inline FsmString_t1785915204 ** get_address_of_vector2Property_15() { return &___vector2Property_15; }
	inline void set_vector2Property_15(FsmString_t1785915204 * value)
	{
		___vector2Property_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Property_15), value);
	}

	inline static int32_t get_offset_of_vector2Value_16() { return static_cast<int32_t>(offsetof(SetProceduralVector2_t2655254814, ___vector2Value_16)); }
	inline FsmVector2_t2965096677 * get_vector2Value_16() const { return ___vector2Value_16; }
	inline FsmVector2_t2965096677 ** get_address_of_vector2Value_16() { return &___vector2Value_16; }
	inline void set_vector2Value_16(FsmVector2_t2965096677 * value)
	{
		___vector2Value_16 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Value_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetProceduralVector2_t2655254814, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROCEDURALVECTOR2_T2655254814_H
#ifndef SETPROCEDURALVECTOR3_T2655189278_H
#define SETPROCEDURALVECTOR3_T2655189278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetProceduralVector3
struct  SetProceduralVector3_t2655189278  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetProceduralVector3::substanceMaterial
	FsmMaterial_t2057874452 * ___substanceMaterial_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetProceduralVector3::vector3Property
	FsmString_t1785915204 * ___vector3Property_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetProceduralVector3::vector3Value
	FsmVector3_t626444517 * ___vector3Value_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SetProceduralVector3::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_substanceMaterial_14() { return static_cast<int32_t>(offsetof(SetProceduralVector3_t2655189278, ___substanceMaterial_14)); }
	inline FsmMaterial_t2057874452 * get_substanceMaterial_14() const { return ___substanceMaterial_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_substanceMaterial_14() { return &___substanceMaterial_14; }
	inline void set_substanceMaterial_14(FsmMaterial_t2057874452 * value)
	{
		___substanceMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___substanceMaterial_14), value);
	}

	inline static int32_t get_offset_of_vector3Property_15() { return static_cast<int32_t>(offsetof(SetProceduralVector3_t2655189278, ___vector3Property_15)); }
	inline FsmString_t1785915204 * get_vector3Property_15() const { return ___vector3Property_15; }
	inline FsmString_t1785915204 ** get_address_of_vector3Property_15() { return &___vector3Property_15; }
	inline void set_vector3Property_15(FsmString_t1785915204 * value)
	{
		___vector3Property_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Property_15), value);
	}

	inline static int32_t get_offset_of_vector3Value_16() { return static_cast<int32_t>(offsetof(SetProceduralVector3_t2655189278, ___vector3Value_16)); }
	inline FsmVector3_t626444517 * get_vector3Value_16() const { return ___vector3Value_16; }
	inline FsmVector3_t626444517 ** get_address_of_vector3Value_16() { return &___vector3Value_16; }
	inline void set_vector3Value_16(FsmVector3_t626444517 * value)
	{
		___vector3Value_16 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Value_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SetProceduralVector3_t2655189278, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROCEDURALVECTOR3_T2655189278_H
#ifndef SETRECTFIELDS_T3909148976_H
#define SETRECTFIELDS_T3909148976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRectFields
struct  SetRectFields_t3909148976  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetRectFields::rectVariable
	FsmRect_t3649179877 * ___rectVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::x
	FsmFloat_t2883254149 * ___x_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::y
	FsmFloat_t2883254149 * ___y_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::width
	FsmFloat_t2883254149 * ___width_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetRectFields::height
	FsmFloat_t2883254149 * ___height_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRectFields::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_rectVariable_14() { return static_cast<int32_t>(offsetof(SetRectFields_t3909148976, ___rectVariable_14)); }
	inline FsmRect_t3649179877 * get_rectVariable_14() const { return ___rectVariable_14; }
	inline FsmRect_t3649179877 ** get_address_of_rectVariable_14() { return &___rectVariable_14; }
	inline void set_rectVariable_14(FsmRect_t3649179877 * value)
	{
		___rectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariable_14), value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(SetRectFields_t3909148976, ___x_15)); }
	inline FsmFloat_t2883254149 * get_x_15() const { return ___x_15; }
	inline FsmFloat_t2883254149 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmFloat_t2883254149 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier((&___x_15), value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(SetRectFields_t3909148976, ___y_16)); }
	inline FsmFloat_t2883254149 * get_y_16() const { return ___y_16; }
	inline FsmFloat_t2883254149 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmFloat_t2883254149 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier((&___y_16), value);
	}

	inline static int32_t get_offset_of_width_17() { return static_cast<int32_t>(offsetof(SetRectFields_t3909148976, ___width_17)); }
	inline FsmFloat_t2883254149 * get_width_17() const { return ___width_17; }
	inline FsmFloat_t2883254149 ** get_address_of_width_17() { return &___width_17; }
	inline void set_width_17(FsmFloat_t2883254149 * value)
	{
		___width_17 = value;
		Il2CppCodeGenWriteBarrier((&___width_17), value);
	}

	inline static int32_t get_offset_of_height_18() { return static_cast<int32_t>(offsetof(SetRectFields_t3909148976, ___height_18)); }
	inline FsmFloat_t2883254149 * get_height_18() const { return ___height_18; }
	inline FsmFloat_t2883254149 ** get_address_of_height_18() { return &___height_18; }
	inline void set_height_18(FsmFloat_t2883254149 * value)
	{
		___height_18 = value;
		Il2CppCodeGenWriteBarrier((&___height_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(SetRectFields_t3909148976, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETRECTFIELDS_T3909148976_H
#ifndef SETRECTVALUE_T2541018530_H
#define SETRECTVALUE_T2541018530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRectValue
struct  SetRectValue_t2541018530  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetRectValue::rectVariable
	FsmRect_t3649179877 * ___rectVariable_14;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetRectValue::rectValue
	FsmRect_t3649179877 * ___rectValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetRectValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_rectVariable_14() { return static_cast<int32_t>(offsetof(SetRectValue_t2541018530, ___rectVariable_14)); }
	inline FsmRect_t3649179877 * get_rectVariable_14() const { return ___rectVariable_14; }
	inline FsmRect_t3649179877 ** get_address_of_rectVariable_14() { return &___rectVariable_14; }
	inline void set_rectVariable_14(FsmRect_t3649179877 * value)
	{
		___rectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariable_14), value);
	}

	inline static int32_t get_offset_of_rectValue_15() { return static_cast<int32_t>(offsetof(SetRectValue_t2541018530, ___rectValue_15)); }
	inline FsmRect_t3649179877 * get_rectValue_15() const { return ___rectValue_15; }
	inline FsmRect_t3649179877 ** get_address_of_rectValue_15() { return &___rectValue_15; }
	inline void set_rectValue_15(FsmRect_t3649179877 * value)
	{
		___rectValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___rectValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetRectValue_t2541018530, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETRECTVALUE_T2541018530_H
#ifndef SETSKYBOX_T109181542_H
#define SETSKYBOX_T109181542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetSkybox
struct  SetSkybox_t109181542  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetSkybox::skybox
	FsmMaterial_t2057874452 * ___skybox_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetSkybox::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_skybox_14() { return static_cast<int32_t>(offsetof(SetSkybox_t109181542, ___skybox_14)); }
	inline FsmMaterial_t2057874452 * get_skybox_14() const { return ___skybox_14; }
	inline FsmMaterial_t2057874452 ** get_address_of_skybox_14() { return &___skybox_14; }
	inline void set_skybox_14(FsmMaterial_t2057874452 * value)
	{
		___skybox_14 = value;
		Il2CppCodeGenWriteBarrier((&___skybox_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetSkybox_t109181542, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETSKYBOX_T109181542_H
#ifndef UNLOADSCENE_T2244765154_H
#define UNLOADSCENE_T2244765154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UnloadScene
struct  UnloadScene_t2244765154  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.UnloadScene/SceneReferenceOptions HutongGames.PlayMaker.Actions.UnloadScene::sceneReference
	int32_t ___sceneReference_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UnloadScene::sceneByName
	FsmString_t1785915204 * ___sceneByName_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UnloadScene::sceneAtBuildIndex
	FsmInt_t874273141 * ___sceneAtBuildIndex_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UnloadScene::sceneAtIndex
	FsmInt_t874273141 * ___sceneAtIndex_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UnloadScene::sceneByPath
	FsmString_t1785915204 * ___sceneByPath_18;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UnloadScene::sceneByGameObject
	FsmOwnerDefault_t3590610434 * ___sceneByGameObject_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UnloadScene::unloaded
	FsmBool_t163807967 * ___unloaded_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnloadScene::unloadedEvent
	FsmEvent_t3736299882 * ___unloadedEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnloadScene::failureEvent
	FsmEvent_t3736299882 * ___failureEvent_22;

public:
	inline static int32_t get_offset_of_sceneReference_14() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___sceneReference_14)); }
	inline int32_t get_sceneReference_14() const { return ___sceneReference_14; }
	inline int32_t* get_address_of_sceneReference_14() { return &___sceneReference_14; }
	inline void set_sceneReference_14(int32_t value)
	{
		___sceneReference_14 = value;
	}

	inline static int32_t get_offset_of_sceneByName_15() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___sceneByName_15)); }
	inline FsmString_t1785915204 * get_sceneByName_15() const { return ___sceneByName_15; }
	inline FsmString_t1785915204 ** get_address_of_sceneByName_15() { return &___sceneByName_15; }
	inline void set_sceneByName_15(FsmString_t1785915204 * value)
	{
		___sceneByName_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByName_15), value);
	}

	inline static int32_t get_offset_of_sceneAtBuildIndex_16() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___sceneAtBuildIndex_16)); }
	inline FsmInt_t874273141 * get_sceneAtBuildIndex_16() const { return ___sceneAtBuildIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtBuildIndex_16() { return &___sceneAtBuildIndex_16; }
	inline void set_sceneAtBuildIndex_16(FsmInt_t874273141 * value)
	{
		___sceneAtBuildIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtBuildIndex_16), value);
	}

	inline static int32_t get_offset_of_sceneAtIndex_17() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___sceneAtIndex_17)); }
	inline FsmInt_t874273141 * get_sceneAtIndex_17() const { return ___sceneAtIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtIndex_17() { return &___sceneAtIndex_17; }
	inline void set_sceneAtIndex_17(FsmInt_t874273141 * value)
	{
		___sceneAtIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtIndex_17), value);
	}

	inline static int32_t get_offset_of_sceneByPath_18() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___sceneByPath_18)); }
	inline FsmString_t1785915204 * get_sceneByPath_18() const { return ___sceneByPath_18; }
	inline FsmString_t1785915204 ** get_address_of_sceneByPath_18() { return &___sceneByPath_18; }
	inline void set_sceneByPath_18(FsmString_t1785915204 * value)
	{
		___sceneByPath_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByPath_18), value);
	}

	inline static int32_t get_offset_of_sceneByGameObject_19() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___sceneByGameObject_19)); }
	inline FsmOwnerDefault_t3590610434 * get_sceneByGameObject_19() const { return ___sceneByGameObject_19; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_sceneByGameObject_19() { return &___sceneByGameObject_19; }
	inline void set_sceneByGameObject_19(FsmOwnerDefault_t3590610434 * value)
	{
		___sceneByGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByGameObject_19), value);
	}

	inline static int32_t get_offset_of_unloaded_20() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___unloaded_20)); }
	inline FsmBool_t163807967 * get_unloaded_20() const { return ___unloaded_20; }
	inline FsmBool_t163807967 ** get_address_of_unloaded_20() { return &___unloaded_20; }
	inline void set_unloaded_20(FsmBool_t163807967 * value)
	{
		___unloaded_20 = value;
		Il2CppCodeGenWriteBarrier((&___unloaded_20), value);
	}

	inline static int32_t get_offset_of_unloadedEvent_21() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___unloadedEvent_21)); }
	inline FsmEvent_t3736299882 * get_unloadedEvent_21() const { return ___unloadedEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_unloadedEvent_21() { return &___unloadedEvent_21; }
	inline void set_unloadedEvent_21(FsmEvent_t3736299882 * value)
	{
		___unloadedEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___unloadedEvent_21), value);
	}

	inline static int32_t get_offset_of_failureEvent_22() { return static_cast<int32_t>(offsetof(UnloadScene_t2244765154, ___failureEvent_22)); }
	inline FsmEvent_t3736299882 * get_failureEvent_22() const { return ___failureEvent_22; }
	inline FsmEvent_t3736299882 ** get_address_of_failureEvent_22() { return &___failureEvent_22; }
	inline void set_failureEvent_22(FsmEvent_t3736299882 * value)
	{
		___failureEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___failureEvent_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOADSCENE_T2244765154_H
#ifndef UNLOADSCENEASYNCH_T792856094_H
#define UNLOADSCENEASYNCH_T792856094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UnloadSceneAsynch
struct  UnloadSceneAsynch_t792856094  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.UnloadSceneAsynch/SceneReferenceOptions HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneReference
	int32_t ___sceneReference_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneByName
	FsmString_t1785915204 * ___sceneByName_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneAtBuildIndex
	FsmInt_t874273141 * ___sceneAtBuildIndex_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneAtIndex
	FsmInt_t874273141 * ___sceneAtIndex_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneByPath
	FsmString_t1785915204 * ___sceneByPath_18;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneByGameObject
	FsmOwnerDefault_t3590610434 * ___sceneByGameObject_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.UnloadSceneAsynch::operationPriority
	FsmInt_t874273141 * ___operationPriority_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.UnloadSceneAsynch::progress
	FsmFloat_t2883254149 * ___progress_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.UnloadSceneAsynch::isDone
	FsmBool_t163807967 * ___isDone_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnloadSceneAsynch::doneEvent
	FsmEvent_t3736299882 * ___doneEvent_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.UnloadSceneAsynch::sceneNotFoundEvent
	FsmEvent_t3736299882 * ___sceneNotFoundEvent_24;
	// UnityEngine.AsyncOperation HutongGames.PlayMaker.Actions.UnloadSceneAsynch::_asyncOperation
	AsyncOperation_t1445031843 * ____asyncOperation_25;

public:
	inline static int32_t get_offset_of_sceneReference_14() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneReference_14)); }
	inline int32_t get_sceneReference_14() const { return ___sceneReference_14; }
	inline int32_t* get_address_of_sceneReference_14() { return &___sceneReference_14; }
	inline void set_sceneReference_14(int32_t value)
	{
		___sceneReference_14 = value;
	}

	inline static int32_t get_offset_of_sceneByName_15() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneByName_15)); }
	inline FsmString_t1785915204 * get_sceneByName_15() const { return ___sceneByName_15; }
	inline FsmString_t1785915204 ** get_address_of_sceneByName_15() { return &___sceneByName_15; }
	inline void set_sceneByName_15(FsmString_t1785915204 * value)
	{
		___sceneByName_15 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByName_15), value);
	}

	inline static int32_t get_offset_of_sceneAtBuildIndex_16() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneAtBuildIndex_16)); }
	inline FsmInt_t874273141 * get_sceneAtBuildIndex_16() const { return ___sceneAtBuildIndex_16; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtBuildIndex_16() { return &___sceneAtBuildIndex_16; }
	inline void set_sceneAtBuildIndex_16(FsmInt_t874273141 * value)
	{
		___sceneAtBuildIndex_16 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtBuildIndex_16), value);
	}

	inline static int32_t get_offset_of_sceneAtIndex_17() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneAtIndex_17)); }
	inline FsmInt_t874273141 * get_sceneAtIndex_17() const { return ___sceneAtIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_sceneAtIndex_17() { return &___sceneAtIndex_17; }
	inline void set_sceneAtIndex_17(FsmInt_t874273141 * value)
	{
		___sceneAtIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___sceneAtIndex_17), value);
	}

	inline static int32_t get_offset_of_sceneByPath_18() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneByPath_18)); }
	inline FsmString_t1785915204 * get_sceneByPath_18() const { return ___sceneByPath_18; }
	inline FsmString_t1785915204 ** get_address_of_sceneByPath_18() { return &___sceneByPath_18; }
	inline void set_sceneByPath_18(FsmString_t1785915204 * value)
	{
		___sceneByPath_18 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByPath_18), value);
	}

	inline static int32_t get_offset_of_sceneByGameObject_19() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneByGameObject_19)); }
	inline FsmOwnerDefault_t3590610434 * get_sceneByGameObject_19() const { return ___sceneByGameObject_19; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_sceneByGameObject_19() { return &___sceneByGameObject_19; }
	inline void set_sceneByGameObject_19(FsmOwnerDefault_t3590610434 * value)
	{
		___sceneByGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___sceneByGameObject_19), value);
	}

	inline static int32_t get_offset_of_operationPriority_20() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___operationPriority_20)); }
	inline FsmInt_t874273141 * get_operationPriority_20() const { return ___operationPriority_20; }
	inline FsmInt_t874273141 ** get_address_of_operationPriority_20() { return &___operationPriority_20; }
	inline void set_operationPriority_20(FsmInt_t874273141 * value)
	{
		___operationPriority_20 = value;
		Il2CppCodeGenWriteBarrier((&___operationPriority_20), value);
	}

	inline static int32_t get_offset_of_progress_21() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___progress_21)); }
	inline FsmFloat_t2883254149 * get_progress_21() const { return ___progress_21; }
	inline FsmFloat_t2883254149 ** get_address_of_progress_21() { return &___progress_21; }
	inline void set_progress_21(FsmFloat_t2883254149 * value)
	{
		___progress_21 = value;
		Il2CppCodeGenWriteBarrier((&___progress_21), value);
	}

	inline static int32_t get_offset_of_isDone_22() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___isDone_22)); }
	inline FsmBool_t163807967 * get_isDone_22() const { return ___isDone_22; }
	inline FsmBool_t163807967 ** get_address_of_isDone_22() { return &___isDone_22; }
	inline void set_isDone_22(FsmBool_t163807967 * value)
	{
		___isDone_22 = value;
		Il2CppCodeGenWriteBarrier((&___isDone_22), value);
	}

	inline static int32_t get_offset_of_doneEvent_23() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___doneEvent_23)); }
	inline FsmEvent_t3736299882 * get_doneEvent_23() const { return ___doneEvent_23; }
	inline FsmEvent_t3736299882 ** get_address_of_doneEvent_23() { return &___doneEvent_23; }
	inline void set_doneEvent_23(FsmEvent_t3736299882 * value)
	{
		___doneEvent_23 = value;
		Il2CppCodeGenWriteBarrier((&___doneEvent_23), value);
	}

	inline static int32_t get_offset_of_sceneNotFoundEvent_24() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ___sceneNotFoundEvent_24)); }
	inline FsmEvent_t3736299882 * get_sceneNotFoundEvent_24() const { return ___sceneNotFoundEvent_24; }
	inline FsmEvent_t3736299882 ** get_address_of_sceneNotFoundEvent_24() { return &___sceneNotFoundEvent_24; }
	inline void set_sceneNotFoundEvent_24(FsmEvent_t3736299882 * value)
	{
		___sceneNotFoundEvent_24 = value;
		Il2CppCodeGenWriteBarrier((&___sceneNotFoundEvent_24), value);
	}

	inline static int32_t get_offset_of__asyncOperation_25() { return static_cast<int32_t>(offsetof(UnloadSceneAsynch_t792856094, ____asyncOperation_25)); }
	inline AsyncOperation_t1445031843 * get__asyncOperation_25() const { return ____asyncOperation_25; }
	inline AsyncOperation_t1445031843 ** get_address_of__asyncOperation_25() { return &____asyncOperation_25; }
	inline void set__asyncOperation_25(AsyncOperation_t1445031843 * value)
	{
		____asyncOperation_25 = value;
		Il2CppCodeGenWriteBarrier((&____asyncOperation_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNLOADSCENEASYNCH_T792856094_H
#ifndef GETQUATERNIONEULERANGLES_T823427448_H
#define GETQUATERNIONEULERANGLES_T823427448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles
struct  GetQuaternionEulerAngles_t823427448  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::quaternion
	FsmQuaternion_t4259038327 * ___quaternion_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::eulerAngles
	FsmVector3_t626444517 * ___eulerAngles_17;

public:
	inline static int32_t get_offset_of_quaternion_16() { return static_cast<int32_t>(offsetof(GetQuaternionEulerAngles_t823427448, ___quaternion_16)); }
	inline FsmQuaternion_t4259038327 * get_quaternion_16() const { return ___quaternion_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternion_16() { return &___quaternion_16; }
	inline void set_quaternion_16(FsmQuaternion_t4259038327 * value)
	{
		___quaternion_16 = value;
		Il2CppCodeGenWriteBarrier((&___quaternion_16), value);
	}

	inline static int32_t get_offset_of_eulerAngles_17() { return static_cast<int32_t>(offsetof(GetQuaternionEulerAngles_t823427448, ___eulerAngles_17)); }
	inline FsmVector3_t626444517 * get_eulerAngles_17() const { return ___eulerAngles_17; }
	inline FsmVector3_t626444517 ** get_address_of_eulerAngles_17() { return &___eulerAngles_17; }
	inline void set_eulerAngles_17(FsmVector3_t626444517 * value)
	{
		___eulerAngles_17 = value;
		Il2CppCodeGenWriteBarrier((&___eulerAngles_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETQUATERNIONEULERANGLES_T823427448_H
#ifndef GETQUATERNIONFROMROTATION_T1468389705_H
#define GETQUATERNIONFROMROTATION_T1468389705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetQuaternionFromRotation
struct  GetQuaternionFromRotation_t1468389705  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::fromDirection
	FsmVector3_t626444517 * ___fromDirection_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::toDirection
	FsmVector3_t626444517 * ___toDirection_17;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::result
	FsmQuaternion_t4259038327 * ___result_18;

public:
	inline static int32_t get_offset_of_fromDirection_16() { return static_cast<int32_t>(offsetof(GetQuaternionFromRotation_t1468389705, ___fromDirection_16)); }
	inline FsmVector3_t626444517 * get_fromDirection_16() const { return ___fromDirection_16; }
	inline FsmVector3_t626444517 ** get_address_of_fromDirection_16() { return &___fromDirection_16; }
	inline void set_fromDirection_16(FsmVector3_t626444517 * value)
	{
		___fromDirection_16 = value;
		Il2CppCodeGenWriteBarrier((&___fromDirection_16), value);
	}

	inline static int32_t get_offset_of_toDirection_17() { return static_cast<int32_t>(offsetof(GetQuaternionFromRotation_t1468389705, ___toDirection_17)); }
	inline FsmVector3_t626444517 * get_toDirection_17() const { return ___toDirection_17; }
	inline FsmVector3_t626444517 ** get_address_of_toDirection_17() { return &___toDirection_17; }
	inline void set_toDirection_17(FsmVector3_t626444517 * value)
	{
		___toDirection_17 = value;
		Il2CppCodeGenWriteBarrier((&___toDirection_17), value);
	}

	inline static int32_t get_offset_of_result_18() { return static_cast<int32_t>(offsetof(GetQuaternionFromRotation_t1468389705, ___result_18)); }
	inline FsmQuaternion_t4259038327 * get_result_18() const { return ___result_18; }
	inline FsmQuaternion_t4259038327 ** get_address_of_result_18() { return &___result_18; }
	inline void set_result_18(FsmQuaternion_t4259038327 * value)
	{
		___result_18 = value;
		Il2CppCodeGenWriteBarrier((&___result_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETQUATERNIONFROMROTATION_T1468389705_H
#ifndef GETQUATERNIONMULTIPLIEDBYQUATERNION_T2297620533_H
#define GETQUATERNIONMULTIPLIEDBYQUATERNION_T2297620533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion
struct  GetQuaternionMultipliedByQuaternion_t2297620533  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::quaternionA
	FsmQuaternion_t4259038327 * ___quaternionA_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::quaternionB
	FsmQuaternion_t4259038327 * ___quaternionB_17;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::result
	FsmQuaternion_t4259038327 * ___result_18;

public:
	inline static int32_t get_offset_of_quaternionA_16() { return static_cast<int32_t>(offsetof(GetQuaternionMultipliedByQuaternion_t2297620533, ___quaternionA_16)); }
	inline FsmQuaternion_t4259038327 * get_quaternionA_16() const { return ___quaternionA_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternionA_16() { return &___quaternionA_16; }
	inline void set_quaternionA_16(FsmQuaternion_t4259038327 * value)
	{
		___quaternionA_16 = value;
		Il2CppCodeGenWriteBarrier((&___quaternionA_16), value);
	}

	inline static int32_t get_offset_of_quaternionB_17() { return static_cast<int32_t>(offsetof(GetQuaternionMultipliedByQuaternion_t2297620533, ___quaternionB_17)); }
	inline FsmQuaternion_t4259038327 * get_quaternionB_17() const { return ___quaternionB_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternionB_17() { return &___quaternionB_17; }
	inline void set_quaternionB_17(FsmQuaternion_t4259038327 * value)
	{
		___quaternionB_17 = value;
		Il2CppCodeGenWriteBarrier((&___quaternionB_17), value);
	}

	inline static int32_t get_offset_of_result_18() { return static_cast<int32_t>(offsetof(GetQuaternionMultipliedByQuaternion_t2297620533, ___result_18)); }
	inline FsmQuaternion_t4259038327 * get_result_18() const { return ___result_18; }
	inline FsmQuaternion_t4259038327 ** get_address_of_result_18() { return &___result_18; }
	inline void set_result_18(FsmQuaternion_t4259038327 * value)
	{
		___result_18 = value;
		Il2CppCodeGenWriteBarrier((&___result_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETQUATERNIONMULTIPLIEDBYQUATERNION_T2297620533_H
#ifndef GETQUATERNIONMULTIPLIEDBYVECTOR_T2299213397_H
#define GETQUATERNIONMULTIPLIEDBYVECTOR_T2299213397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector
struct  GetQuaternionMultipliedByVector_t2299213397  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::quaternion
	FsmQuaternion_t4259038327 * ___quaternion_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::vector3
	FsmVector3_t626444517 * ___vector3_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::result
	FsmVector3_t626444517 * ___result_18;

public:
	inline static int32_t get_offset_of_quaternion_16() { return static_cast<int32_t>(offsetof(GetQuaternionMultipliedByVector_t2299213397, ___quaternion_16)); }
	inline FsmQuaternion_t4259038327 * get_quaternion_16() const { return ___quaternion_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternion_16() { return &___quaternion_16; }
	inline void set_quaternion_16(FsmQuaternion_t4259038327 * value)
	{
		___quaternion_16 = value;
		Il2CppCodeGenWriteBarrier((&___quaternion_16), value);
	}

	inline static int32_t get_offset_of_vector3_17() { return static_cast<int32_t>(offsetof(GetQuaternionMultipliedByVector_t2299213397, ___vector3_17)); }
	inline FsmVector3_t626444517 * get_vector3_17() const { return ___vector3_17; }
	inline FsmVector3_t626444517 ** get_address_of_vector3_17() { return &___vector3_17; }
	inline void set_vector3_17(FsmVector3_t626444517 * value)
	{
		___vector3_17 = value;
		Il2CppCodeGenWriteBarrier((&___vector3_17), value);
	}

	inline static int32_t get_offset_of_result_18() { return static_cast<int32_t>(offsetof(GetQuaternionMultipliedByVector_t2299213397, ___result_18)); }
	inline FsmVector3_t626444517 * get_result_18() const { return ___result_18; }
	inline FsmVector3_t626444517 ** get_address_of_result_18() { return &___result_18; }
	inline void set_result_18(FsmVector3_t626444517 * value)
	{
		___result_18 = value;
		Il2CppCodeGenWriteBarrier((&___result_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETQUATERNIONMULTIPLIEDBYVECTOR_T2299213397_H
#ifndef GETSCENEBUILDINDEX_T2039529201_H
#define GETSCENEBUILDINDEX_T2039529201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneBuildIndex
struct  GetSceneBuildIndex_t2039529201  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneBuildIndex::buildIndex
	FsmInt_t874273141 * ___buildIndex_24;

public:
	inline static int32_t get_offset_of_buildIndex_24() { return static_cast<int32_t>(offsetof(GetSceneBuildIndex_t2039529201, ___buildIndex_24)); }
	inline FsmInt_t874273141 * get_buildIndex_24() const { return ___buildIndex_24; }
	inline FsmInt_t874273141 ** get_address_of_buildIndex_24() { return &___buildIndex_24; }
	inline void set_buildIndex_24(FsmInt_t874273141 * value)
	{
		___buildIndex_24 = value;
		Il2CppCodeGenWriteBarrier((&___buildIndex_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEBUILDINDEX_T2039529201_H
#ifndef GETSCENEISDIRTY_T3294465093_H
#define GETSCENEISDIRTY_T3294465093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneIsDirty
struct  GetSceneIsDirty_t3294465093  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneIsDirty::isDirty
	FsmBool_t163807967 * ___isDirty_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneIsDirty::isDirtyEvent
	FsmEvent_t3736299882 * ___isDirtyEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneIsDirty::isNotDirtyEvent
	FsmEvent_t3736299882 * ___isNotDirtyEvent_26;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneIsDirty::everyFrame
	bool ___everyFrame_27;

public:
	inline static int32_t get_offset_of_isDirty_24() { return static_cast<int32_t>(offsetof(GetSceneIsDirty_t3294465093, ___isDirty_24)); }
	inline FsmBool_t163807967 * get_isDirty_24() const { return ___isDirty_24; }
	inline FsmBool_t163807967 ** get_address_of_isDirty_24() { return &___isDirty_24; }
	inline void set_isDirty_24(FsmBool_t163807967 * value)
	{
		___isDirty_24 = value;
		Il2CppCodeGenWriteBarrier((&___isDirty_24), value);
	}

	inline static int32_t get_offset_of_isDirtyEvent_25() { return static_cast<int32_t>(offsetof(GetSceneIsDirty_t3294465093, ___isDirtyEvent_25)); }
	inline FsmEvent_t3736299882 * get_isDirtyEvent_25() const { return ___isDirtyEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_isDirtyEvent_25() { return &___isDirtyEvent_25; }
	inline void set_isDirtyEvent_25(FsmEvent_t3736299882 * value)
	{
		___isDirtyEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___isDirtyEvent_25), value);
	}

	inline static int32_t get_offset_of_isNotDirtyEvent_26() { return static_cast<int32_t>(offsetof(GetSceneIsDirty_t3294465093, ___isNotDirtyEvent_26)); }
	inline FsmEvent_t3736299882 * get_isNotDirtyEvent_26() const { return ___isNotDirtyEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotDirtyEvent_26() { return &___isNotDirtyEvent_26; }
	inline void set_isNotDirtyEvent_26(FsmEvent_t3736299882 * value)
	{
		___isNotDirtyEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___isNotDirtyEvent_26), value);
	}

	inline static int32_t get_offset_of_everyFrame_27() { return static_cast<int32_t>(offsetof(GetSceneIsDirty_t3294465093, ___everyFrame_27)); }
	inline bool get_everyFrame_27() const { return ___everyFrame_27; }
	inline bool* get_address_of_everyFrame_27() { return &___everyFrame_27; }
	inline void set_everyFrame_27(bool value)
	{
		___everyFrame_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEISDIRTY_T3294465093_H
#ifndef GETSCENEISLOADED_T1206798716_H
#define GETSCENEISLOADED_T1206798716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneIsLoaded
struct  GetSceneIsLoaded_t1206798716  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneIsLoaded::isLoaded
	FsmBool_t163807967 * ___isLoaded_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneIsLoaded::isLoadedEvent
	FsmEvent_t3736299882 * ___isLoadedEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneIsLoaded::isNotLoadedEvent
	FsmEvent_t3736299882 * ___isNotLoadedEvent_26;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneIsLoaded::everyFrame
	bool ___everyFrame_27;

public:
	inline static int32_t get_offset_of_isLoaded_24() { return static_cast<int32_t>(offsetof(GetSceneIsLoaded_t1206798716, ___isLoaded_24)); }
	inline FsmBool_t163807967 * get_isLoaded_24() const { return ___isLoaded_24; }
	inline FsmBool_t163807967 ** get_address_of_isLoaded_24() { return &___isLoaded_24; }
	inline void set_isLoaded_24(FsmBool_t163807967 * value)
	{
		___isLoaded_24 = value;
		Il2CppCodeGenWriteBarrier((&___isLoaded_24), value);
	}

	inline static int32_t get_offset_of_isLoadedEvent_25() { return static_cast<int32_t>(offsetof(GetSceneIsLoaded_t1206798716, ___isLoadedEvent_25)); }
	inline FsmEvent_t3736299882 * get_isLoadedEvent_25() const { return ___isLoadedEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_isLoadedEvent_25() { return &___isLoadedEvent_25; }
	inline void set_isLoadedEvent_25(FsmEvent_t3736299882 * value)
	{
		___isLoadedEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___isLoadedEvent_25), value);
	}

	inline static int32_t get_offset_of_isNotLoadedEvent_26() { return static_cast<int32_t>(offsetof(GetSceneIsLoaded_t1206798716, ___isNotLoadedEvent_26)); }
	inline FsmEvent_t3736299882 * get_isNotLoadedEvent_26() const { return ___isNotLoadedEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotLoadedEvent_26() { return &___isNotLoadedEvent_26; }
	inline void set_isNotLoadedEvent_26(FsmEvent_t3736299882 * value)
	{
		___isNotLoadedEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___isNotLoadedEvent_26), value);
	}

	inline static int32_t get_offset_of_everyFrame_27() { return static_cast<int32_t>(offsetof(GetSceneIsLoaded_t1206798716, ___everyFrame_27)); }
	inline bool get_everyFrame_27() const { return ___everyFrame_27; }
	inline bool* get_address_of_everyFrame_27() { return &___everyFrame_27; }
	inline void set_everyFrame_27(bool value)
	{
		___everyFrame_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEISLOADED_T1206798716_H
#ifndef GETSCENEISVALID_T3529258936_H
#define GETSCENEISVALID_T3529258936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneIsValid
struct  GetSceneIsValid_t3529258936  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneIsValid::isValid
	FsmBool_t163807967 * ___isValid_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneIsValid::isValidEvent
	FsmEvent_t3736299882 * ___isValidEvent_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetSceneIsValid::isNotValidEvent
	FsmEvent_t3736299882 * ___isNotValidEvent_26;

public:
	inline static int32_t get_offset_of_isValid_24() { return static_cast<int32_t>(offsetof(GetSceneIsValid_t3529258936, ___isValid_24)); }
	inline FsmBool_t163807967 * get_isValid_24() const { return ___isValid_24; }
	inline FsmBool_t163807967 ** get_address_of_isValid_24() { return &___isValid_24; }
	inline void set_isValid_24(FsmBool_t163807967 * value)
	{
		___isValid_24 = value;
		Il2CppCodeGenWriteBarrier((&___isValid_24), value);
	}

	inline static int32_t get_offset_of_isValidEvent_25() { return static_cast<int32_t>(offsetof(GetSceneIsValid_t3529258936, ___isValidEvent_25)); }
	inline FsmEvent_t3736299882 * get_isValidEvent_25() const { return ___isValidEvent_25; }
	inline FsmEvent_t3736299882 ** get_address_of_isValidEvent_25() { return &___isValidEvent_25; }
	inline void set_isValidEvent_25(FsmEvent_t3736299882 * value)
	{
		___isValidEvent_25 = value;
		Il2CppCodeGenWriteBarrier((&___isValidEvent_25), value);
	}

	inline static int32_t get_offset_of_isNotValidEvent_26() { return static_cast<int32_t>(offsetof(GetSceneIsValid_t3529258936, ___isNotValidEvent_26)); }
	inline FsmEvent_t3736299882 * get_isNotValidEvent_26() const { return ___isNotValidEvent_26; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotValidEvent_26() { return &___isNotValidEvent_26; }
	inline void set_isNotValidEvent_26(FsmEvent_t3736299882 * value)
	{
		___isNotValidEvent_26 = value;
		Il2CppCodeGenWriteBarrier((&___isNotValidEvent_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEISVALID_T3529258936_H
#ifndef GETSCENENAME_T473291379_H
#define GETSCENENAME_T473291379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneName
struct  GetSceneName_t473291379  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneName::name
	FsmString_t1785915204 * ___name_24;

public:
	inline static int32_t get_offset_of_name_24() { return static_cast<int32_t>(offsetof(GetSceneName_t473291379, ___name_24)); }
	inline FsmString_t1785915204 * get_name_24() const { return ___name_24; }
	inline FsmString_t1785915204 ** get_address_of_name_24() { return &___name_24; }
	inline void set_name_24(FsmString_t1785915204 * value)
	{
		___name_24 = value;
		Il2CppCodeGenWriteBarrier((&___name_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENENAME_T473291379_H
#ifndef GETSCENEPATH_T735216820_H
#define GETSCENEPATH_T735216820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetScenePath
struct  GetScenePath_t735216820  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetScenePath::path
	FsmString_t1785915204 * ___path_24;

public:
	inline static int32_t get_offset_of_path_24() { return static_cast<int32_t>(offsetof(GetScenePath_t735216820, ___path_24)); }
	inline FsmString_t1785915204 * get_path_24() const { return ___path_24; }
	inline FsmString_t1785915204 ** get_address_of_path_24() { return &___path_24; }
	inline void set_path_24(FsmString_t1785915204 * value)
	{
		___path_24 = value;
		Il2CppCodeGenWriteBarrier((&___path_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEPATH_T735216820_H
#ifndef GETSCENEPROPERTIES_T396337852_H
#define GETSCENEPROPERTIES_T396337852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneProperties
struct  GetSceneProperties_t396337852  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneProperties::name
	FsmString_t1785915204 * ___name_24;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneProperties::path
	FsmString_t1785915204 * ___path_25;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneProperties::buildIndex
	FsmInt_t874273141 * ___buildIndex_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneProperties::isValid
	FsmBool_t163807967 * ___isValid_27;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneProperties::isLoaded
	FsmBool_t163807967 * ___isLoaded_28;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneProperties::isDirty
	FsmBool_t163807967 * ___isDirty_29;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneProperties::rootCount
	FsmInt_t874273141 * ___rootCount_30;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneProperties::rootGameObjects
	FsmArray_t1756862219 * ___rootGameObjects_31;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneProperties::everyFrame
	bool ___everyFrame_32;

public:
	inline static int32_t get_offset_of_name_24() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___name_24)); }
	inline FsmString_t1785915204 * get_name_24() const { return ___name_24; }
	inline FsmString_t1785915204 ** get_address_of_name_24() { return &___name_24; }
	inline void set_name_24(FsmString_t1785915204 * value)
	{
		___name_24 = value;
		Il2CppCodeGenWriteBarrier((&___name_24), value);
	}

	inline static int32_t get_offset_of_path_25() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___path_25)); }
	inline FsmString_t1785915204 * get_path_25() const { return ___path_25; }
	inline FsmString_t1785915204 ** get_address_of_path_25() { return &___path_25; }
	inline void set_path_25(FsmString_t1785915204 * value)
	{
		___path_25 = value;
		Il2CppCodeGenWriteBarrier((&___path_25), value);
	}

	inline static int32_t get_offset_of_buildIndex_26() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___buildIndex_26)); }
	inline FsmInt_t874273141 * get_buildIndex_26() const { return ___buildIndex_26; }
	inline FsmInt_t874273141 ** get_address_of_buildIndex_26() { return &___buildIndex_26; }
	inline void set_buildIndex_26(FsmInt_t874273141 * value)
	{
		___buildIndex_26 = value;
		Il2CppCodeGenWriteBarrier((&___buildIndex_26), value);
	}

	inline static int32_t get_offset_of_isValid_27() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___isValid_27)); }
	inline FsmBool_t163807967 * get_isValid_27() const { return ___isValid_27; }
	inline FsmBool_t163807967 ** get_address_of_isValid_27() { return &___isValid_27; }
	inline void set_isValid_27(FsmBool_t163807967 * value)
	{
		___isValid_27 = value;
		Il2CppCodeGenWriteBarrier((&___isValid_27), value);
	}

	inline static int32_t get_offset_of_isLoaded_28() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___isLoaded_28)); }
	inline FsmBool_t163807967 * get_isLoaded_28() const { return ___isLoaded_28; }
	inline FsmBool_t163807967 ** get_address_of_isLoaded_28() { return &___isLoaded_28; }
	inline void set_isLoaded_28(FsmBool_t163807967 * value)
	{
		___isLoaded_28 = value;
		Il2CppCodeGenWriteBarrier((&___isLoaded_28), value);
	}

	inline static int32_t get_offset_of_isDirty_29() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___isDirty_29)); }
	inline FsmBool_t163807967 * get_isDirty_29() const { return ___isDirty_29; }
	inline FsmBool_t163807967 ** get_address_of_isDirty_29() { return &___isDirty_29; }
	inline void set_isDirty_29(FsmBool_t163807967 * value)
	{
		___isDirty_29 = value;
		Il2CppCodeGenWriteBarrier((&___isDirty_29), value);
	}

	inline static int32_t get_offset_of_rootCount_30() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___rootCount_30)); }
	inline FsmInt_t874273141 * get_rootCount_30() const { return ___rootCount_30; }
	inline FsmInt_t874273141 ** get_address_of_rootCount_30() { return &___rootCount_30; }
	inline void set_rootCount_30(FsmInt_t874273141 * value)
	{
		___rootCount_30 = value;
		Il2CppCodeGenWriteBarrier((&___rootCount_30), value);
	}

	inline static int32_t get_offset_of_rootGameObjects_31() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___rootGameObjects_31)); }
	inline FsmArray_t1756862219 * get_rootGameObjects_31() const { return ___rootGameObjects_31; }
	inline FsmArray_t1756862219 ** get_address_of_rootGameObjects_31() { return &___rootGameObjects_31; }
	inline void set_rootGameObjects_31(FsmArray_t1756862219 * value)
	{
		___rootGameObjects_31 = value;
		Il2CppCodeGenWriteBarrier((&___rootGameObjects_31), value);
	}

	inline static int32_t get_offset_of_everyFrame_32() { return static_cast<int32_t>(offsetof(GetSceneProperties_t396337852, ___everyFrame_32)); }
	inline bool get_everyFrame_32() const { return ___everyFrame_32; }
	inline bool* get_address_of_everyFrame_32() { return &___everyFrame_32; }
	inline void set_everyFrame_32(bool value)
	{
		___everyFrame_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEPROPERTIES_T396337852_H
#ifndef GETSCENEROOTCOUNT_T2311826246_H
#define GETSCENEROOTCOUNT_T2311826246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneRootCount
struct  GetSceneRootCount_t2311826246  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneRootCount::rootCount
	FsmInt_t874273141 * ___rootCount_24;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneRootCount::everyFrame
	bool ___everyFrame_25;

public:
	inline static int32_t get_offset_of_rootCount_24() { return static_cast<int32_t>(offsetof(GetSceneRootCount_t2311826246, ___rootCount_24)); }
	inline FsmInt_t874273141 * get_rootCount_24() const { return ___rootCount_24; }
	inline FsmInt_t874273141 ** get_address_of_rootCount_24() { return &___rootCount_24; }
	inline void set_rootCount_24(FsmInt_t874273141 * value)
	{
		___rootCount_24 = value;
		Il2CppCodeGenWriteBarrier((&___rootCount_24), value);
	}

	inline static int32_t get_offset_of_everyFrame_25() { return static_cast<int32_t>(offsetof(GetSceneRootCount_t2311826246, ___everyFrame_25)); }
	inline bool get_everyFrame_25() const { return ___everyFrame_25; }
	inline bool* get_address_of_everyFrame_25() { return &___everyFrame_25; }
	inline void set_everyFrame_25(bool value)
	{
		___everyFrame_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEROOTCOUNT_T2311826246_H
#ifndef GETSCENEROOTGAMEOBJECTS_T1901668039_H
#define GETSCENEROOTGAMEOBJECTS_T1901668039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneRootGameObjects
struct  GetSceneRootGameObjects_t1901668039  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneRootGameObjects::rootGameObjects
	FsmArray_t1756862219 * ___rootGameObjects_24;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSceneRootGameObjects::everyFrame
	bool ___everyFrame_25;

public:
	inline static int32_t get_offset_of_rootGameObjects_24() { return static_cast<int32_t>(offsetof(GetSceneRootGameObjects_t1901668039, ___rootGameObjects_24)); }
	inline FsmArray_t1756862219 * get_rootGameObjects_24() const { return ___rootGameObjects_24; }
	inline FsmArray_t1756862219 ** get_address_of_rootGameObjects_24() { return &___rootGameObjects_24; }
	inline void set_rootGameObjects_24(FsmArray_t1756862219 * value)
	{
		___rootGameObjects_24 = value;
		Il2CppCodeGenWriteBarrier((&___rootGameObjects_24), value);
	}

	inline static int32_t get_offset_of_everyFrame_25() { return static_cast<int32_t>(offsetof(GetSceneRootGameObjects_t1901668039, ___everyFrame_25)); }
	inline bool get_everyFrame_25() const { return ___everyFrame_25; }
	inline bool* get_address_of_everyFrame_25() { return &___everyFrame_25; }
	inline void set_everyFrame_25(bool value)
	{
		___everyFrame_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCENEROOTGAMEOBJECTS_T1901668039_H
#ifndef MOVEGAMEOBJECTTOSCENE_T1066798052_H
#define MOVEGAMEOBJECTTOSCENE_T1066798052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MoveGameObjectToScene
struct  MoveGameObjectToScene_t1066798052  : public GetSceneActionBase_t2551360596
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MoveGameObjectToScene::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_24;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MoveGameObjectToScene::findRootIfNecessary
	FsmBool_t163807967 * ___findRootIfNecessary_25;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MoveGameObjectToScene::success
	FsmBool_t163807967 * ___success_26;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MoveGameObjectToScene::successEvent
	FsmEvent_t3736299882 * ___successEvent_27;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MoveGameObjectToScene::failureEvent
	FsmEvent_t3736299882 * ___failureEvent_28;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.MoveGameObjectToScene::_go
	GameObject_t1113636619 * ____go_29;

public:
	inline static int32_t get_offset_of_gameObject_24() { return static_cast<int32_t>(offsetof(MoveGameObjectToScene_t1066798052, ___gameObject_24)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_24() const { return ___gameObject_24; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_24() { return &___gameObject_24; }
	inline void set_gameObject_24(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_24 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_24), value);
	}

	inline static int32_t get_offset_of_findRootIfNecessary_25() { return static_cast<int32_t>(offsetof(MoveGameObjectToScene_t1066798052, ___findRootIfNecessary_25)); }
	inline FsmBool_t163807967 * get_findRootIfNecessary_25() const { return ___findRootIfNecessary_25; }
	inline FsmBool_t163807967 ** get_address_of_findRootIfNecessary_25() { return &___findRootIfNecessary_25; }
	inline void set_findRootIfNecessary_25(FsmBool_t163807967 * value)
	{
		___findRootIfNecessary_25 = value;
		Il2CppCodeGenWriteBarrier((&___findRootIfNecessary_25), value);
	}

	inline static int32_t get_offset_of_success_26() { return static_cast<int32_t>(offsetof(MoveGameObjectToScene_t1066798052, ___success_26)); }
	inline FsmBool_t163807967 * get_success_26() const { return ___success_26; }
	inline FsmBool_t163807967 ** get_address_of_success_26() { return &___success_26; }
	inline void set_success_26(FsmBool_t163807967 * value)
	{
		___success_26 = value;
		Il2CppCodeGenWriteBarrier((&___success_26), value);
	}

	inline static int32_t get_offset_of_successEvent_27() { return static_cast<int32_t>(offsetof(MoveGameObjectToScene_t1066798052, ___successEvent_27)); }
	inline FsmEvent_t3736299882 * get_successEvent_27() const { return ___successEvent_27; }
	inline FsmEvent_t3736299882 ** get_address_of_successEvent_27() { return &___successEvent_27; }
	inline void set_successEvent_27(FsmEvent_t3736299882 * value)
	{
		___successEvent_27 = value;
		Il2CppCodeGenWriteBarrier((&___successEvent_27), value);
	}

	inline static int32_t get_offset_of_failureEvent_28() { return static_cast<int32_t>(offsetof(MoveGameObjectToScene_t1066798052, ___failureEvent_28)); }
	inline FsmEvent_t3736299882 * get_failureEvent_28() const { return ___failureEvent_28; }
	inline FsmEvent_t3736299882 ** get_address_of_failureEvent_28() { return &___failureEvent_28; }
	inline void set_failureEvent_28(FsmEvent_t3736299882 * value)
	{
		___failureEvent_28 = value;
		Il2CppCodeGenWriteBarrier((&___failureEvent_28), value);
	}

	inline static int32_t get_offset_of__go_29() { return static_cast<int32_t>(offsetof(MoveGameObjectToScene_t1066798052, ____go_29)); }
	inline GameObject_t1113636619 * get__go_29() const { return ____go_29; }
	inline GameObject_t1113636619 ** get_address_of__go_29() { return &____go_29; }
	inline void set__go_29(GameObject_t1113636619 * value)
	{
		____go_29 = value;
		Il2CppCodeGenWriteBarrier((&____go_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEGAMEOBJECTTOSCENE_T1066798052_H
#ifndef QUATERNIONANGLEAXIS_T2080243336_H
#define QUATERNIONANGLEAXIS_T2080243336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionAngleAxis
struct  QuaternionAngleAxis_t2080243336  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionAngleAxis::angle
	FsmFloat_t2883254149 * ___angle_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.QuaternionAngleAxis::axis
	FsmVector3_t626444517 * ___axis_17;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionAngleAxis::result
	FsmQuaternion_t4259038327 * ___result_18;

public:
	inline static int32_t get_offset_of_angle_16() { return static_cast<int32_t>(offsetof(QuaternionAngleAxis_t2080243336, ___angle_16)); }
	inline FsmFloat_t2883254149 * get_angle_16() const { return ___angle_16; }
	inline FsmFloat_t2883254149 ** get_address_of_angle_16() { return &___angle_16; }
	inline void set_angle_16(FsmFloat_t2883254149 * value)
	{
		___angle_16 = value;
		Il2CppCodeGenWriteBarrier((&___angle_16), value);
	}

	inline static int32_t get_offset_of_axis_17() { return static_cast<int32_t>(offsetof(QuaternionAngleAxis_t2080243336, ___axis_17)); }
	inline FsmVector3_t626444517 * get_axis_17() const { return ___axis_17; }
	inline FsmVector3_t626444517 ** get_address_of_axis_17() { return &___axis_17; }
	inline void set_axis_17(FsmVector3_t626444517 * value)
	{
		___axis_17 = value;
		Il2CppCodeGenWriteBarrier((&___axis_17), value);
	}

	inline static int32_t get_offset_of_result_18() { return static_cast<int32_t>(offsetof(QuaternionAngleAxis_t2080243336, ___result_18)); }
	inline FsmQuaternion_t4259038327 * get_result_18() const { return ___result_18; }
	inline FsmQuaternion_t4259038327 ** get_address_of_result_18() { return &___result_18; }
	inline void set_result_18(FsmQuaternion_t4259038327 * value)
	{
		___result_18 = value;
		Il2CppCodeGenWriteBarrier((&___result_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONANGLEAXIS_T2080243336_H
#ifndef QUATERNIONCOMPARE_T1279087300_H
#define QUATERNIONCOMPARE_T1279087300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionCompare
struct  QuaternionCompare_t1279087300  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionCompare::Quaternion1
	FsmQuaternion_t4259038327 * ___Quaternion1_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionCompare::Quaternion2
	FsmQuaternion_t4259038327 * ___Quaternion2_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.QuaternionCompare::equal
	FsmBool_t163807967 * ___equal_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.QuaternionCompare::equalEvent
	FsmEvent_t3736299882 * ___equalEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.QuaternionCompare::notEqualEvent
	FsmEvent_t3736299882 * ___notEqualEvent_20;

public:
	inline static int32_t get_offset_of_Quaternion1_16() { return static_cast<int32_t>(offsetof(QuaternionCompare_t1279087300, ___Quaternion1_16)); }
	inline FsmQuaternion_t4259038327 * get_Quaternion1_16() const { return ___Quaternion1_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_Quaternion1_16() { return &___Quaternion1_16; }
	inline void set_Quaternion1_16(FsmQuaternion_t4259038327 * value)
	{
		___Quaternion1_16 = value;
		Il2CppCodeGenWriteBarrier((&___Quaternion1_16), value);
	}

	inline static int32_t get_offset_of_Quaternion2_17() { return static_cast<int32_t>(offsetof(QuaternionCompare_t1279087300, ___Quaternion2_17)); }
	inline FsmQuaternion_t4259038327 * get_Quaternion2_17() const { return ___Quaternion2_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_Quaternion2_17() { return &___Quaternion2_17; }
	inline void set_Quaternion2_17(FsmQuaternion_t4259038327 * value)
	{
		___Quaternion2_17 = value;
		Il2CppCodeGenWriteBarrier((&___Quaternion2_17), value);
	}

	inline static int32_t get_offset_of_equal_18() { return static_cast<int32_t>(offsetof(QuaternionCompare_t1279087300, ___equal_18)); }
	inline FsmBool_t163807967 * get_equal_18() const { return ___equal_18; }
	inline FsmBool_t163807967 ** get_address_of_equal_18() { return &___equal_18; }
	inline void set_equal_18(FsmBool_t163807967 * value)
	{
		___equal_18 = value;
		Il2CppCodeGenWriteBarrier((&___equal_18), value);
	}

	inline static int32_t get_offset_of_equalEvent_19() { return static_cast<int32_t>(offsetof(QuaternionCompare_t1279087300, ___equalEvent_19)); }
	inline FsmEvent_t3736299882 * get_equalEvent_19() const { return ___equalEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_equalEvent_19() { return &___equalEvent_19; }
	inline void set_equalEvent_19(FsmEvent_t3736299882 * value)
	{
		___equalEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___equalEvent_19), value);
	}

	inline static int32_t get_offset_of_notEqualEvent_20() { return static_cast<int32_t>(offsetof(QuaternionCompare_t1279087300, ___notEqualEvent_20)); }
	inline FsmEvent_t3736299882 * get_notEqualEvent_20() const { return ___notEqualEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_notEqualEvent_20() { return &___notEqualEvent_20; }
	inline void set_notEqualEvent_20(FsmEvent_t3736299882 * value)
	{
		___notEqualEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___notEqualEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONCOMPARE_T1279087300_H
#ifndef QUATERNIONEULER_T1634553366_H
#define QUATERNIONEULER_T1634553366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionEuler
struct  QuaternionEuler_t1634553366  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.QuaternionEuler::eulerAngles
	FsmVector3_t626444517 * ___eulerAngles_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionEuler::result
	FsmQuaternion_t4259038327 * ___result_17;

public:
	inline static int32_t get_offset_of_eulerAngles_16() { return static_cast<int32_t>(offsetof(QuaternionEuler_t1634553366, ___eulerAngles_16)); }
	inline FsmVector3_t626444517 * get_eulerAngles_16() const { return ___eulerAngles_16; }
	inline FsmVector3_t626444517 ** get_address_of_eulerAngles_16() { return &___eulerAngles_16; }
	inline void set_eulerAngles_16(FsmVector3_t626444517 * value)
	{
		___eulerAngles_16 = value;
		Il2CppCodeGenWriteBarrier((&___eulerAngles_16), value);
	}

	inline static int32_t get_offset_of_result_17() { return static_cast<int32_t>(offsetof(QuaternionEuler_t1634553366, ___result_17)); }
	inline FsmQuaternion_t4259038327 * get_result_17() const { return ___result_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_result_17() { return &___result_17; }
	inline void set_result_17(FsmQuaternion_t4259038327 * value)
	{
		___result_17 = value;
		Il2CppCodeGenWriteBarrier((&___result_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONEULER_T1634553366_H
#ifndef QUATERNIONINVERSE_T1832578058_H
#define QUATERNIONINVERSE_T1832578058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionInverse
struct  QuaternionInverse_t1832578058  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionInverse::rotation
	FsmQuaternion_t4259038327 * ___rotation_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionInverse::result
	FsmQuaternion_t4259038327 * ___result_17;

public:
	inline static int32_t get_offset_of_rotation_16() { return static_cast<int32_t>(offsetof(QuaternionInverse_t1832578058, ___rotation_16)); }
	inline FsmQuaternion_t4259038327 * get_rotation_16() const { return ___rotation_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_rotation_16() { return &___rotation_16; }
	inline void set_rotation_16(FsmQuaternion_t4259038327 * value)
	{
		___rotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_16), value);
	}

	inline static int32_t get_offset_of_result_17() { return static_cast<int32_t>(offsetof(QuaternionInverse_t1832578058, ___result_17)); }
	inline FsmQuaternion_t4259038327 * get_result_17() const { return ___result_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_result_17() { return &___result_17; }
	inline void set_result_17(FsmQuaternion_t4259038327 * value)
	{
		___result_17 = value;
		Il2CppCodeGenWriteBarrier((&___result_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONINVERSE_T1832578058_H
#ifndef QUATERNIONLERP_T3856070935_H
#define QUATERNIONLERP_T3856070935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionLerp
struct  QuaternionLerp_t3856070935  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionLerp::fromQuaternion
	FsmQuaternion_t4259038327 * ___fromQuaternion_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionLerp::toQuaternion
	FsmQuaternion_t4259038327 * ___toQuaternion_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionLerp::amount
	FsmFloat_t2883254149 * ___amount_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionLerp::storeResult
	FsmQuaternion_t4259038327 * ___storeResult_19;

public:
	inline static int32_t get_offset_of_fromQuaternion_16() { return static_cast<int32_t>(offsetof(QuaternionLerp_t3856070935, ___fromQuaternion_16)); }
	inline FsmQuaternion_t4259038327 * get_fromQuaternion_16() const { return ___fromQuaternion_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_fromQuaternion_16() { return &___fromQuaternion_16; }
	inline void set_fromQuaternion_16(FsmQuaternion_t4259038327 * value)
	{
		___fromQuaternion_16 = value;
		Il2CppCodeGenWriteBarrier((&___fromQuaternion_16), value);
	}

	inline static int32_t get_offset_of_toQuaternion_17() { return static_cast<int32_t>(offsetof(QuaternionLerp_t3856070935, ___toQuaternion_17)); }
	inline FsmQuaternion_t4259038327 * get_toQuaternion_17() const { return ___toQuaternion_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_toQuaternion_17() { return &___toQuaternion_17; }
	inline void set_toQuaternion_17(FsmQuaternion_t4259038327 * value)
	{
		___toQuaternion_17 = value;
		Il2CppCodeGenWriteBarrier((&___toQuaternion_17), value);
	}

	inline static int32_t get_offset_of_amount_18() { return static_cast<int32_t>(offsetof(QuaternionLerp_t3856070935, ___amount_18)); }
	inline FsmFloat_t2883254149 * get_amount_18() const { return ___amount_18; }
	inline FsmFloat_t2883254149 ** get_address_of_amount_18() { return &___amount_18; }
	inline void set_amount_18(FsmFloat_t2883254149 * value)
	{
		___amount_18 = value;
		Il2CppCodeGenWriteBarrier((&___amount_18), value);
	}

	inline static int32_t get_offset_of_storeResult_19() { return static_cast<int32_t>(offsetof(QuaternionLerp_t3856070935, ___storeResult_19)); }
	inline FsmQuaternion_t4259038327 * get_storeResult_19() const { return ___storeResult_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_storeResult_19() { return &___storeResult_19; }
	inline void set_storeResult_19(FsmQuaternion_t4259038327 * value)
	{
		___storeResult_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONLERP_T3856070935_H
#ifndef QUATERNIONLOOKROTATION_T2008970098_H
#define QUATERNIONLOOKROTATION_T2008970098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionLookRotation
struct  QuaternionLookRotation_t2008970098  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.QuaternionLookRotation::direction
	FsmVector3_t626444517 * ___direction_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.QuaternionLookRotation::upVector
	FsmVector3_t626444517 * ___upVector_17;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionLookRotation::result
	FsmQuaternion_t4259038327 * ___result_18;

public:
	inline static int32_t get_offset_of_direction_16() { return static_cast<int32_t>(offsetof(QuaternionLookRotation_t2008970098, ___direction_16)); }
	inline FsmVector3_t626444517 * get_direction_16() const { return ___direction_16; }
	inline FsmVector3_t626444517 ** get_address_of_direction_16() { return &___direction_16; }
	inline void set_direction_16(FsmVector3_t626444517 * value)
	{
		___direction_16 = value;
		Il2CppCodeGenWriteBarrier((&___direction_16), value);
	}

	inline static int32_t get_offset_of_upVector_17() { return static_cast<int32_t>(offsetof(QuaternionLookRotation_t2008970098, ___upVector_17)); }
	inline FsmVector3_t626444517 * get_upVector_17() const { return ___upVector_17; }
	inline FsmVector3_t626444517 ** get_address_of_upVector_17() { return &___upVector_17; }
	inline void set_upVector_17(FsmVector3_t626444517 * value)
	{
		___upVector_17 = value;
		Il2CppCodeGenWriteBarrier((&___upVector_17), value);
	}

	inline static int32_t get_offset_of_result_18() { return static_cast<int32_t>(offsetof(QuaternionLookRotation_t2008970098, ___result_18)); }
	inline FsmQuaternion_t4259038327 * get_result_18() const { return ___result_18; }
	inline FsmQuaternion_t4259038327 ** get_address_of_result_18() { return &___result_18; }
	inline void set_result_18(FsmQuaternion_t4259038327 * value)
	{
		___result_18 = value;
		Il2CppCodeGenWriteBarrier((&___result_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONLOOKROTATION_T2008970098_H
#ifndef QUATERNIONLOWPASSFILTER_T4253160803_H
#define QUATERNIONLOWPASSFILTER_T4253160803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionLowPassFilter
struct  QuaternionLowPassFilter_t4253160803  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::quaternionVariable
	FsmQuaternion_t4259038327 * ___quaternionVariable_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::filteringFactor
	FsmFloat_t2883254149 * ___filteringFactor_17;
	// UnityEngine.Quaternion HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::filteredQuaternion
	Quaternion_t2301928331  ___filteredQuaternion_18;

public:
	inline static int32_t get_offset_of_quaternionVariable_16() { return static_cast<int32_t>(offsetof(QuaternionLowPassFilter_t4253160803, ___quaternionVariable_16)); }
	inline FsmQuaternion_t4259038327 * get_quaternionVariable_16() const { return ___quaternionVariable_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternionVariable_16() { return &___quaternionVariable_16; }
	inline void set_quaternionVariable_16(FsmQuaternion_t4259038327 * value)
	{
		___quaternionVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___quaternionVariable_16), value);
	}

	inline static int32_t get_offset_of_filteringFactor_17() { return static_cast<int32_t>(offsetof(QuaternionLowPassFilter_t4253160803, ___filteringFactor_17)); }
	inline FsmFloat_t2883254149 * get_filteringFactor_17() const { return ___filteringFactor_17; }
	inline FsmFloat_t2883254149 ** get_address_of_filteringFactor_17() { return &___filteringFactor_17; }
	inline void set_filteringFactor_17(FsmFloat_t2883254149 * value)
	{
		___filteringFactor_17 = value;
		Il2CppCodeGenWriteBarrier((&___filteringFactor_17), value);
	}

	inline static int32_t get_offset_of_filteredQuaternion_18() { return static_cast<int32_t>(offsetof(QuaternionLowPassFilter_t4253160803, ___filteredQuaternion_18)); }
	inline Quaternion_t2301928331  get_filteredQuaternion_18() const { return ___filteredQuaternion_18; }
	inline Quaternion_t2301928331 * get_address_of_filteredQuaternion_18() { return &___filteredQuaternion_18; }
	inline void set_filteredQuaternion_18(Quaternion_t2301928331  value)
	{
		___filteredQuaternion_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONLOWPASSFILTER_T4253160803_H
#ifndef QUATERNIONROTATETOWARDS_T2237857460_H
#define QUATERNIONROTATETOWARDS_T2237857460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionRotateTowards
struct  QuaternionRotateTowards_t2237857460  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionRotateTowards::fromQuaternion
	FsmQuaternion_t4259038327 * ___fromQuaternion_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionRotateTowards::toQuaternion
	FsmQuaternion_t4259038327 * ___toQuaternion_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionRotateTowards::maxDegreesDelta
	FsmFloat_t2883254149 * ___maxDegreesDelta_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionRotateTowards::storeResult
	FsmQuaternion_t4259038327 * ___storeResult_19;

public:
	inline static int32_t get_offset_of_fromQuaternion_16() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t2237857460, ___fromQuaternion_16)); }
	inline FsmQuaternion_t4259038327 * get_fromQuaternion_16() const { return ___fromQuaternion_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_fromQuaternion_16() { return &___fromQuaternion_16; }
	inline void set_fromQuaternion_16(FsmQuaternion_t4259038327 * value)
	{
		___fromQuaternion_16 = value;
		Il2CppCodeGenWriteBarrier((&___fromQuaternion_16), value);
	}

	inline static int32_t get_offset_of_toQuaternion_17() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t2237857460, ___toQuaternion_17)); }
	inline FsmQuaternion_t4259038327 * get_toQuaternion_17() const { return ___toQuaternion_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_toQuaternion_17() { return &___toQuaternion_17; }
	inline void set_toQuaternion_17(FsmQuaternion_t4259038327 * value)
	{
		___toQuaternion_17 = value;
		Il2CppCodeGenWriteBarrier((&___toQuaternion_17), value);
	}

	inline static int32_t get_offset_of_maxDegreesDelta_18() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t2237857460, ___maxDegreesDelta_18)); }
	inline FsmFloat_t2883254149 * get_maxDegreesDelta_18() const { return ___maxDegreesDelta_18; }
	inline FsmFloat_t2883254149 ** get_address_of_maxDegreesDelta_18() { return &___maxDegreesDelta_18; }
	inline void set_maxDegreesDelta_18(FsmFloat_t2883254149 * value)
	{
		___maxDegreesDelta_18 = value;
		Il2CppCodeGenWriteBarrier((&___maxDegreesDelta_18), value);
	}

	inline static int32_t get_offset_of_storeResult_19() { return static_cast<int32_t>(offsetof(QuaternionRotateTowards_t2237857460, ___storeResult_19)); }
	inline FsmQuaternion_t4259038327 * get_storeResult_19() const { return ___storeResult_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_storeResult_19() { return &___storeResult_19; }
	inline void set_storeResult_19(FsmQuaternion_t4259038327 * value)
	{
		___storeResult_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONROTATETOWARDS_T2237857460_H
#ifndef QUATERNIONSLERP_T4066974401_H
#define QUATERNIONSLERP_T4066974401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.QuaternionSlerp
struct  QuaternionSlerp_t4066974401  : public QuaternionBaseAction_t3672550121
{
public:
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionSlerp::fromQuaternion
	FsmQuaternion_t4259038327 * ___fromQuaternion_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionSlerp::toQuaternion
	FsmQuaternion_t4259038327 * ___toQuaternion_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.QuaternionSlerp::amount
	FsmFloat_t2883254149 * ___amount_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.QuaternionSlerp::storeResult
	FsmQuaternion_t4259038327 * ___storeResult_19;

public:
	inline static int32_t get_offset_of_fromQuaternion_16() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t4066974401, ___fromQuaternion_16)); }
	inline FsmQuaternion_t4259038327 * get_fromQuaternion_16() const { return ___fromQuaternion_16; }
	inline FsmQuaternion_t4259038327 ** get_address_of_fromQuaternion_16() { return &___fromQuaternion_16; }
	inline void set_fromQuaternion_16(FsmQuaternion_t4259038327 * value)
	{
		___fromQuaternion_16 = value;
		Il2CppCodeGenWriteBarrier((&___fromQuaternion_16), value);
	}

	inline static int32_t get_offset_of_toQuaternion_17() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t4066974401, ___toQuaternion_17)); }
	inline FsmQuaternion_t4259038327 * get_toQuaternion_17() const { return ___toQuaternion_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_toQuaternion_17() { return &___toQuaternion_17; }
	inline void set_toQuaternion_17(FsmQuaternion_t4259038327 * value)
	{
		___toQuaternion_17 = value;
		Il2CppCodeGenWriteBarrier((&___toQuaternion_17), value);
	}

	inline static int32_t get_offset_of_amount_18() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t4066974401, ___amount_18)); }
	inline FsmFloat_t2883254149 * get_amount_18() const { return ___amount_18; }
	inline FsmFloat_t2883254149 ** get_address_of_amount_18() { return &___amount_18; }
	inline void set_amount_18(FsmFloat_t2883254149 * value)
	{
		___amount_18 = value;
		Il2CppCodeGenWriteBarrier((&___amount_18), value);
	}

	inline static int32_t get_offset_of_storeResult_19() { return static_cast<int32_t>(offsetof(QuaternionSlerp_t4066974401, ___storeResult_19)); }
	inline FsmQuaternion_t4259038327 * get_storeResult_19() const { return ___storeResult_19; }
	inline FsmQuaternion_t4259038327 ** get_address_of_storeResult_19() { return &___storeResult_19; }
	inline void set_storeResult_19(FsmQuaternion_t4259038327 * value)
	{
		___storeResult_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONSLERP_T4066974401_H
#ifndef RECTTRANSFORMGETANCHORMAX_T3935051232_H
#define RECTTRANSFORMGETANCHORMAX_T3935051232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax
struct  RectTransformGetAnchorMax_t3935051232  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::anchorMax
	FsmVector2_t2965096677 * ___anchorMax_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMax_t3935051232, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorMax_17() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMax_t3935051232, ___anchorMax_17)); }
	inline FsmVector2_t2965096677 * get_anchorMax_17() const { return ___anchorMax_17; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMax_17() { return &___anchorMax_17; }
	inline void set_anchorMax_17(FsmVector2_t2965096677 * value)
	{
		___anchorMax_17 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMax_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMax_t3935051232, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMax_t3935051232, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMax_t3935051232, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETANCHORMAX_T3935051232_H
#ifndef RECTTRANSFORMGETANCHORMIN_T4028635278_H
#define RECTTRANSFORMGETANCHORMIN_T4028635278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin
struct  RectTransformGetAnchorMin_t4028635278  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::anchorMin
	FsmVector2_t2965096677 * ___anchorMin_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMin_t4028635278, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorMin_17() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMin_t4028635278, ___anchorMin_17)); }
	inline FsmVector2_t2965096677 * get_anchorMin_17() const { return ___anchorMin_17; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMin_17() { return &___anchorMin_17; }
	inline void set_anchorMin_17(FsmVector2_t2965096677 * value)
	{
		___anchorMin_17 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMin_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMin_t4028635278, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMin_t4028635278, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMin_t4028635278, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETANCHORMIN_T4028635278_H
#ifndef RECTTRANSFORMGETANCHORMINANDMAX_T344340047_H
#define RECTTRANSFORMGETANCHORMINANDMAX_T344340047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax
struct  RectTransformGetAnchorMinAndMax_t344340047  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::anchorMax
	FsmVector2_t2965096677 * ___anchorMax_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::anchorMin
	FsmVector2_t2965096677 * ___anchorMin_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::xMax
	FsmFloat_t2883254149 * ___xMax_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::yMax
	FsmFloat_t2883254149 * ___yMax_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::xMin
	FsmFloat_t2883254149 * ___xMin_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::yMin
	FsmFloat_t2883254149 * ___yMin_22;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::_rt
	RectTransform_t3704657025 * ____rt_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorMax_17() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___anchorMax_17)); }
	inline FsmVector2_t2965096677 * get_anchorMax_17() const { return ___anchorMax_17; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMax_17() { return &___anchorMax_17; }
	inline void set_anchorMax_17(FsmVector2_t2965096677 * value)
	{
		___anchorMax_17 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMax_17), value);
	}

	inline static int32_t get_offset_of_anchorMin_18() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___anchorMin_18)); }
	inline FsmVector2_t2965096677 * get_anchorMin_18() const { return ___anchorMin_18; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMin_18() { return &___anchorMin_18; }
	inline void set_anchorMin_18(FsmVector2_t2965096677 * value)
	{
		___anchorMin_18 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMin_18), value);
	}

	inline static int32_t get_offset_of_xMax_19() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___xMax_19)); }
	inline FsmFloat_t2883254149 * get_xMax_19() const { return ___xMax_19; }
	inline FsmFloat_t2883254149 ** get_address_of_xMax_19() { return &___xMax_19; }
	inline void set_xMax_19(FsmFloat_t2883254149 * value)
	{
		___xMax_19 = value;
		Il2CppCodeGenWriteBarrier((&___xMax_19), value);
	}

	inline static int32_t get_offset_of_yMax_20() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___yMax_20)); }
	inline FsmFloat_t2883254149 * get_yMax_20() const { return ___yMax_20; }
	inline FsmFloat_t2883254149 ** get_address_of_yMax_20() { return &___yMax_20; }
	inline void set_yMax_20(FsmFloat_t2883254149 * value)
	{
		___yMax_20 = value;
		Il2CppCodeGenWriteBarrier((&___yMax_20), value);
	}

	inline static int32_t get_offset_of_xMin_21() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___xMin_21)); }
	inline FsmFloat_t2883254149 * get_xMin_21() const { return ___xMin_21; }
	inline FsmFloat_t2883254149 ** get_address_of_xMin_21() { return &___xMin_21; }
	inline void set_xMin_21(FsmFloat_t2883254149 * value)
	{
		___xMin_21 = value;
		Il2CppCodeGenWriteBarrier((&___xMin_21), value);
	}

	inline static int32_t get_offset_of_yMin_22() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ___yMin_22)); }
	inline FsmFloat_t2883254149 * get_yMin_22() const { return ___yMin_22; }
	inline FsmFloat_t2883254149 ** get_address_of_yMin_22() { return &___yMin_22; }
	inline void set_yMin_22(FsmFloat_t2883254149 * value)
	{
		___yMin_22 = value;
		Il2CppCodeGenWriteBarrier((&___yMin_22), value);
	}

	inline static int32_t get_offset_of__rt_23() { return static_cast<int32_t>(offsetof(RectTransformGetAnchorMinAndMax_t344340047, ____rt_23)); }
	inline RectTransform_t3704657025 * get__rt_23() const { return ____rt_23; }
	inline RectTransform_t3704657025 ** get_address_of__rt_23() { return &____rt_23; }
	inline void set__rt_23(RectTransform_t3704657025 * value)
	{
		____rt_23 = value;
		Il2CppCodeGenWriteBarrier((&____rt_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETANCHORMINANDMAX_T344340047_H
#ifndef RECTTRANSFORMGETANCHOREDPOSITION_T2956266264_H
#define RECTTRANSFORMGETANCHOREDPOSITION_T2956266264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition
struct  RectTransformGetAnchoredPosition_t2956266264  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::position
	FsmVector2_t2965096677 * ___position_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetAnchoredPosition_t2956266264, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(RectTransformGetAnchoredPosition_t2956266264, ___position_17)); }
	inline FsmVector2_t2965096677 * get_position_17() const { return ___position_17; }
	inline FsmVector2_t2965096677 ** get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(FsmVector2_t2965096677 * value)
	{
		___position_17 = value;
		Il2CppCodeGenWriteBarrier((&___position_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetAnchoredPosition_t2956266264, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetAnchoredPosition_t2956266264, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetAnchoredPosition_t2956266264, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETANCHOREDPOSITION_T2956266264_H
#ifndef RECTTRANSFORMGETLOCALPOSITION_T2637760766_H
#define RECTTRANSFORMGETLOCALPOSITION_T2637760766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition
struct  RectTransformGetLocalPosition_t2637760766  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition/LocalPositionReference HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::reference
	int32_t ___reference_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::position
	FsmVector3_t626444517 * ___position_18;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::position2d
	FsmVector2_t2965096677 * ___position2d_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::x
	FsmFloat_t2883254149 * ___x_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::y
	FsmFloat_t2883254149 * ___y_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::z
	FsmFloat_t2883254149 * ___z_22;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::_rt
	RectTransform_t3704657025 * ____rt_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_reference_17() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___reference_17)); }
	inline int32_t get_reference_17() const { return ___reference_17; }
	inline int32_t* get_address_of_reference_17() { return &___reference_17; }
	inline void set_reference_17(int32_t value)
	{
		___reference_17 = value;
	}

	inline static int32_t get_offset_of_position_18() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___position_18)); }
	inline FsmVector3_t626444517 * get_position_18() const { return ___position_18; }
	inline FsmVector3_t626444517 ** get_address_of_position_18() { return &___position_18; }
	inline void set_position_18(FsmVector3_t626444517 * value)
	{
		___position_18 = value;
		Il2CppCodeGenWriteBarrier((&___position_18), value);
	}

	inline static int32_t get_offset_of_position2d_19() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___position2d_19)); }
	inline FsmVector2_t2965096677 * get_position2d_19() const { return ___position2d_19; }
	inline FsmVector2_t2965096677 ** get_address_of_position2d_19() { return &___position2d_19; }
	inline void set_position2d_19(FsmVector2_t2965096677 * value)
	{
		___position2d_19 = value;
		Il2CppCodeGenWriteBarrier((&___position2d_19), value);
	}

	inline static int32_t get_offset_of_x_20() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___x_20)); }
	inline FsmFloat_t2883254149 * get_x_20() const { return ___x_20; }
	inline FsmFloat_t2883254149 ** get_address_of_x_20() { return &___x_20; }
	inline void set_x_20(FsmFloat_t2883254149 * value)
	{
		___x_20 = value;
		Il2CppCodeGenWriteBarrier((&___x_20), value);
	}

	inline static int32_t get_offset_of_y_21() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___y_21)); }
	inline FsmFloat_t2883254149 * get_y_21() const { return ___y_21; }
	inline FsmFloat_t2883254149 ** get_address_of_y_21() { return &___y_21; }
	inline void set_y_21(FsmFloat_t2883254149 * value)
	{
		___y_21 = value;
		Il2CppCodeGenWriteBarrier((&___y_21), value);
	}

	inline static int32_t get_offset_of_z_22() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ___z_22)); }
	inline FsmFloat_t2883254149 * get_z_22() const { return ___z_22; }
	inline FsmFloat_t2883254149 ** get_address_of_z_22() { return &___z_22; }
	inline void set_z_22(FsmFloat_t2883254149 * value)
	{
		___z_22 = value;
		Il2CppCodeGenWriteBarrier((&___z_22), value);
	}

	inline static int32_t get_offset_of__rt_23() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t2637760766, ____rt_23)); }
	inline RectTransform_t3704657025 * get__rt_23() const { return ____rt_23; }
	inline RectTransform_t3704657025 ** get_address_of__rt_23() { return &____rt_23; }
	inline void set__rt_23(RectTransform_t3704657025 * value)
	{
		____rt_23 = value;
		Il2CppCodeGenWriteBarrier((&____rt_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETLOCALPOSITION_T2637760766_H
#ifndef RECTTRANSFORMGETLOCALROTATION_T3876875206_H
#define RECTTRANSFORMGETLOCALROTATION_T3876875206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation
struct  RectTransformGetLocalRotation_t3876875206  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::rotation
	FsmVector3_t626444517 * ___rotation_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::y
	FsmFloat_t2883254149 * ___y_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::z
	FsmFloat_t2883254149 * ___z_20;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::_rt
	RectTransform_t3704657025 * ____rt_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetLocalRotation_t3876875206, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_rotation_17() { return static_cast<int32_t>(offsetof(RectTransformGetLocalRotation_t3876875206, ___rotation_17)); }
	inline FsmVector3_t626444517 * get_rotation_17() const { return ___rotation_17; }
	inline FsmVector3_t626444517 ** get_address_of_rotation_17() { return &___rotation_17; }
	inline void set_rotation_17(FsmVector3_t626444517 * value)
	{
		___rotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetLocalRotation_t3876875206, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetLocalRotation_t3876875206, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_z_20() { return static_cast<int32_t>(offsetof(RectTransformGetLocalRotation_t3876875206, ___z_20)); }
	inline FsmFloat_t2883254149 * get_z_20() const { return ___z_20; }
	inline FsmFloat_t2883254149 ** get_address_of_z_20() { return &___z_20; }
	inline void set_z_20(FsmFloat_t2883254149 * value)
	{
		___z_20 = value;
		Il2CppCodeGenWriteBarrier((&___z_20), value);
	}

	inline static int32_t get_offset_of__rt_21() { return static_cast<int32_t>(offsetof(RectTransformGetLocalRotation_t3876875206, ____rt_21)); }
	inline RectTransform_t3704657025 * get__rt_21() const { return ____rt_21; }
	inline RectTransform_t3704657025 ** get_address_of__rt_21() { return &____rt_21; }
	inline void set__rt_21(RectTransform_t3704657025 * value)
	{
		____rt_21 = value;
		Il2CppCodeGenWriteBarrier((&____rt_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETLOCALROTATION_T3876875206_H
#ifndef RECTTRANSFORMGETOFFSETMAX_T72590789_H
#define RECTTRANSFORMGETOFFSETMAX_T72590789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax
struct  RectTransformGetOffsetMax_t72590789  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::offsetMax
	FsmVector2_t2965096677 * ___offsetMax_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMax_t72590789, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_offsetMax_17() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMax_t72590789, ___offsetMax_17)); }
	inline FsmVector2_t2965096677 * get_offsetMax_17() const { return ___offsetMax_17; }
	inline FsmVector2_t2965096677 ** get_address_of_offsetMax_17() { return &___offsetMax_17; }
	inline void set_offsetMax_17(FsmVector2_t2965096677 * value)
	{
		___offsetMax_17 = value;
		Il2CppCodeGenWriteBarrier((&___offsetMax_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMax_t72590789, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMax_t72590789, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMax_t72590789, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETOFFSETMAX_T72590789_H
#ifndef RECTTRANSFORMGETOFFSETMIN_T1592144851_H
#define RECTTRANSFORMGETOFFSETMIN_T1592144851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin
struct  RectTransformGetOffsetMin_t1592144851  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::offsetMin
	FsmVector2_t2965096677 * ___offsetMin_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMin_t1592144851, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_offsetMin_17() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMin_t1592144851, ___offsetMin_17)); }
	inline FsmVector2_t2965096677 * get_offsetMin_17() const { return ___offsetMin_17; }
	inline FsmVector2_t2965096677 ** get_address_of_offsetMin_17() { return &___offsetMin_17; }
	inline void set_offsetMin_17(FsmVector2_t2965096677 * value)
	{
		___offsetMin_17 = value;
		Il2CppCodeGenWriteBarrier((&___offsetMin_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMin_t1592144851, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMin_t1592144851, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetOffsetMin_t1592144851, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETOFFSETMIN_T1592144851_H
#ifndef RECTTRANSFORMGETPIVOT_T922167116_H
#define RECTTRANSFORMGETPIVOT_T922167116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetPivot
struct  RectTransformGetPivot_t922167116  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetPivot::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetPivot::pivot
	FsmVector2_t2965096677 * ___pivot_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetPivot::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetPivot::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetPivot::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetPivot_t922167116, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_pivot_17() { return static_cast<int32_t>(offsetof(RectTransformGetPivot_t922167116, ___pivot_17)); }
	inline FsmVector2_t2965096677 * get_pivot_17() const { return ___pivot_17; }
	inline FsmVector2_t2965096677 ** get_address_of_pivot_17() { return &___pivot_17; }
	inline void set_pivot_17(FsmVector2_t2965096677 * value)
	{
		___pivot_17 = value;
		Il2CppCodeGenWriteBarrier((&___pivot_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetPivot_t922167116, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetPivot_t922167116, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetPivot_t922167116, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETPIVOT_T922167116_H
#ifndef RECTTRANSFORMGETRECT_T1452626980_H
#define RECTTRANSFORMGETRECT_T1452626980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetRect
struct  RectTransformGetRect_t1452626980  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetRect::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectTransformGetRect::rect
	FsmRect_t3649179877 * ___rect_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::y
	FsmFloat_t2883254149 * ___y_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::width
	FsmFloat_t2883254149 * ___width_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::height
	FsmFloat_t2883254149 * ___height_21;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetRect::_rt
	RectTransform_t3704657025 * ____rt_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_rect_17() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ___rect_17)); }
	inline FsmRect_t3649179877 * get_rect_17() const { return ___rect_17; }
	inline FsmRect_t3649179877 ** get_address_of_rect_17() { return &___rect_17; }
	inline void set_rect_17(FsmRect_t3649179877 * value)
	{
		___rect_17 = value;
		Il2CppCodeGenWriteBarrier((&___rect_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_width_20() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ___width_20)); }
	inline FsmFloat_t2883254149 * get_width_20() const { return ___width_20; }
	inline FsmFloat_t2883254149 ** get_address_of_width_20() { return &___width_20; }
	inline void set_width_20(FsmFloat_t2883254149 * value)
	{
		___width_20 = value;
		Il2CppCodeGenWriteBarrier((&___width_20), value);
	}

	inline static int32_t get_offset_of_height_21() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ___height_21)); }
	inline FsmFloat_t2883254149 * get_height_21() const { return ___height_21; }
	inline FsmFloat_t2883254149 ** get_address_of_height_21() { return &___height_21; }
	inline void set_height_21(FsmFloat_t2883254149 * value)
	{
		___height_21 = value;
		Il2CppCodeGenWriteBarrier((&___height_21), value);
	}

	inline static int32_t get_offset_of__rt_22() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t1452626980, ____rt_22)); }
	inline RectTransform_t3704657025 * get__rt_22() const { return ____rt_22; }
	inline RectTransform_t3704657025 ** get_address_of__rt_22() { return &____rt_22; }
	inline void set__rt_22(RectTransform_t3704657025 * value)
	{
		____rt_22 = value;
		Il2CppCodeGenWriteBarrier((&____rt_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETRECT_T1452626980_H
#ifndef RECTTRANSFORMGETSIZEDELTA_T334801076_H
#define RECTTRANSFORMGETSIZEDELTA_T334801076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta
struct  RectTransformGetSizeDelta_t334801076  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::sizeDelta
	FsmVector2_t2965096677 * ___sizeDelta_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::width
	FsmFloat_t2883254149 * ___width_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::height
	FsmFloat_t2883254149 * ___height_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t334801076, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_sizeDelta_17() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t334801076, ___sizeDelta_17)); }
	inline FsmVector2_t2965096677 * get_sizeDelta_17() const { return ___sizeDelta_17; }
	inline FsmVector2_t2965096677 ** get_address_of_sizeDelta_17() { return &___sizeDelta_17; }
	inline void set_sizeDelta_17(FsmVector2_t2965096677 * value)
	{
		___sizeDelta_17 = value;
		Il2CppCodeGenWriteBarrier((&___sizeDelta_17), value);
	}

	inline static int32_t get_offset_of_width_18() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t334801076, ___width_18)); }
	inline FsmFloat_t2883254149 * get_width_18() const { return ___width_18; }
	inline FsmFloat_t2883254149 ** get_address_of_width_18() { return &___width_18; }
	inline void set_width_18(FsmFloat_t2883254149 * value)
	{
		___width_18 = value;
		Il2CppCodeGenWriteBarrier((&___width_18), value);
	}

	inline static int32_t get_offset_of_height_19() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t334801076, ___height_19)); }
	inline FsmFloat_t2883254149 * get_height_19() const { return ___height_19; }
	inline FsmFloat_t2883254149 ** get_address_of_height_19() { return &___height_19; }
	inline void set_height_19(FsmFloat_t2883254149 * value)
	{
		___height_19 = value;
		Il2CppCodeGenWriteBarrier((&___height_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t334801076, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMGETSIZEDELTA_T334801076_H
#ifndef RECTTRANSFORMPIXELADJUSTPOINT_T2444478249_H
#define RECTTRANSFORMPIXELADJUSTPOINT_T2444478249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint
struct  RectTransformPixelAdjustPoint_t2444478249  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::canvas
	FsmGameObject_t3581898942 * ___canvas_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::screenPoint
	FsmVector2_t2965096677 * ___screenPoint_18;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::pixelPoint
	FsmVector2_t2965096677 * ___pixelPoint_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::_rt
	RectTransform_t3704657025 * ____rt_20;
	// UnityEngine.Canvas HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::_canvas
	Canvas_t3310196443 * ____canvas_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t2444478249, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_canvas_17() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t2444478249, ___canvas_17)); }
	inline FsmGameObject_t3581898942 * get_canvas_17() const { return ___canvas_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_canvas_17() { return &___canvas_17; }
	inline void set_canvas_17(FsmGameObject_t3581898942 * value)
	{
		___canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_17), value);
	}

	inline static int32_t get_offset_of_screenPoint_18() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t2444478249, ___screenPoint_18)); }
	inline FsmVector2_t2965096677 * get_screenPoint_18() const { return ___screenPoint_18; }
	inline FsmVector2_t2965096677 ** get_address_of_screenPoint_18() { return &___screenPoint_18; }
	inline void set_screenPoint_18(FsmVector2_t2965096677 * value)
	{
		___screenPoint_18 = value;
		Il2CppCodeGenWriteBarrier((&___screenPoint_18), value);
	}

	inline static int32_t get_offset_of_pixelPoint_19() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t2444478249, ___pixelPoint_19)); }
	inline FsmVector2_t2965096677 * get_pixelPoint_19() const { return ___pixelPoint_19; }
	inline FsmVector2_t2965096677 ** get_address_of_pixelPoint_19() { return &___pixelPoint_19; }
	inline void set_pixelPoint_19(FsmVector2_t2965096677 * value)
	{
		___pixelPoint_19 = value;
		Il2CppCodeGenWriteBarrier((&___pixelPoint_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t2444478249, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}

	inline static int32_t get_offset_of__canvas_21() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t2444478249, ____canvas_21)); }
	inline Canvas_t3310196443 * get__canvas_21() const { return ____canvas_21; }
	inline Canvas_t3310196443 ** get_address_of__canvas_21() { return &____canvas_21; }
	inline void set__canvas_21(Canvas_t3310196443 * value)
	{
		____canvas_21 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMPIXELADJUSTPOINT_T2444478249_H
#ifndef RECTTRANSFORMPIXELADJUSTRECT_T2193207965_H
#define RECTTRANSFORMPIXELADJUSTRECT_T2193207965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect
struct  RectTransformPixelAdjustRect_t2193207965  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::canvas
	FsmGameObject_t3581898942 * ___canvas_17;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::pixelRect
	FsmRect_t3649179877 * ___pixelRect_18;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::_rt
	RectTransform_t3704657025 * ____rt_19;
	// UnityEngine.Canvas HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::_canvas
	Canvas_t3310196443 * ____canvas_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t2193207965, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_canvas_17() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t2193207965, ___canvas_17)); }
	inline FsmGameObject_t3581898942 * get_canvas_17() const { return ___canvas_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_canvas_17() { return &___canvas_17; }
	inline void set_canvas_17(FsmGameObject_t3581898942 * value)
	{
		___canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_17), value);
	}

	inline static int32_t get_offset_of_pixelRect_18() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t2193207965, ___pixelRect_18)); }
	inline FsmRect_t3649179877 * get_pixelRect_18() const { return ___pixelRect_18; }
	inline FsmRect_t3649179877 ** get_address_of_pixelRect_18() { return &___pixelRect_18; }
	inline void set_pixelRect_18(FsmRect_t3649179877 * value)
	{
		___pixelRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___pixelRect_18), value);
	}

	inline static int32_t get_offset_of__rt_19() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t2193207965, ____rt_19)); }
	inline RectTransform_t3704657025 * get__rt_19() const { return ____rt_19; }
	inline RectTransform_t3704657025 ** get_address_of__rt_19() { return &____rt_19; }
	inline void set__rt_19(RectTransform_t3704657025 * value)
	{
		____rt_19 = value;
		Il2CppCodeGenWriteBarrier((&____rt_19), value);
	}

	inline static int32_t get_offset_of__canvas_20() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t2193207965, ____canvas_20)); }
	inline Canvas_t3310196443 * get__canvas_20() const { return ____canvas_20; }
	inline Canvas_t3310196443 ** get_address_of__canvas_20() { return &____canvas_20; }
	inline void set__canvas_20(Canvas_t3310196443 * value)
	{
		____canvas_20 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMPIXELADJUSTRECT_T2193207965_H
#ifndef RECTTRANSFORMSETANCHORMAX_T3042733759_H
#define RECTTRANSFORMSETANCHORMAX_T3042733759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax
struct  RectTransformSetAnchorMax_t3042733759  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::anchorMax
	FsmVector2_t2965096677 * ___anchorMax_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMax_t3042733759, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorMax_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMax_t3042733759, ___anchorMax_17)); }
	inline FsmVector2_t2965096677 * get_anchorMax_17() const { return ___anchorMax_17; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMax_17() { return &___anchorMax_17; }
	inline void set_anchorMax_17(FsmVector2_t2965096677 * value)
	{
		___anchorMax_17 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMax_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMax_t3042733759, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMax_t3042733759, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMax_t3042733759, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETANCHORMAX_T3042733759_H
#ifndef RECTTRANSFORMSETANCHORMIN_T2591870777_H
#define RECTTRANSFORMSETANCHORMIN_T2591870777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin
struct  RectTransformSetAnchorMin_t2591870777  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::anchorMin
	FsmVector2_t2965096677 * ___anchorMin_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMin_t2591870777, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorMin_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMin_t2591870777, ___anchorMin_17)); }
	inline FsmVector2_t2965096677 * get_anchorMin_17() const { return ___anchorMin_17; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMin_17() { return &___anchorMin_17; }
	inline void set_anchorMin_17(FsmVector2_t2965096677 * value)
	{
		___anchorMin_17 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMin_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMin_t2591870777, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMin_t2591870777, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMin_t2591870777, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETANCHORMIN_T2591870777_H
#ifndef RECTTRANSFORMSETANCHORMINANDMAX_T1703422762_H
#define RECTTRANSFORMSETANCHORMINANDMAX_T1703422762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax
struct  RectTransformSetAnchorMinAndMax_t1703422762  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::anchorMax
	FsmVector2_t2965096677 * ___anchorMax_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::anchorMin
	FsmVector2_t2965096677 * ___anchorMin_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::xMax
	FsmFloat_t2883254149 * ___xMax_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::yMax
	FsmFloat_t2883254149 * ___yMax_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::xMin
	FsmFloat_t2883254149 * ___xMin_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::yMin
	FsmFloat_t2883254149 * ___yMin_22;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::_rt
	RectTransform_t3704657025 * ____rt_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorMax_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___anchorMax_17)); }
	inline FsmVector2_t2965096677 * get_anchorMax_17() const { return ___anchorMax_17; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMax_17() { return &___anchorMax_17; }
	inline void set_anchorMax_17(FsmVector2_t2965096677 * value)
	{
		___anchorMax_17 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMax_17), value);
	}

	inline static int32_t get_offset_of_anchorMin_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___anchorMin_18)); }
	inline FsmVector2_t2965096677 * get_anchorMin_18() const { return ___anchorMin_18; }
	inline FsmVector2_t2965096677 ** get_address_of_anchorMin_18() { return &___anchorMin_18; }
	inline void set_anchorMin_18(FsmVector2_t2965096677 * value)
	{
		___anchorMin_18 = value;
		Il2CppCodeGenWriteBarrier((&___anchorMin_18), value);
	}

	inline static int32_t get_offset_of_xMax_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___xMax_19)); }
	inline FsmFloat_t2883254149 * get_xMax_19() const { return ___xMax_19; }
	inline FsmFloat_t2883254149 ** get_address_of_xMax_19() { return &___xMax_19; }
	inline void set_xMax_19(FsmFloat_t2883254149 * value)
	{
		___xMax_19 = value;
		Il2CppCodeGenWriteBarrier((&___xMax_19), value);
	}

	inline static int32_t get_offset_of_yMax_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___yMax_20)); }
	inline FsmFloat_t2883254149 * get_yMax_20() const { return ___yMax_20; }
	inline FsmFloat_t2883254149 ** get_address_of_yMax_20() { return &___yMax_20; }
	inline void set_yMax_20(FsmFloat_t2883254149 * value)
	{
		___yMax_20 = value;
		Il2CppCodeGenWriteBarrier((&___yMax_20), value);
	}

	inline static int32_t get_offset_of_xMin_21() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___xMin_21)); }
	inline FsmFloat_t2883254149 * get_xMin_21() const { return ___xMin_21; }
	inline FsmFloat_t2883254149 ** get_address_of_xMin_21() { return &___xMin_21; }
	inline void set_xMin_21(FsmFloat_t2883254149 * value)
	{
		___xMin_21 = value;
		Il2CppCodeGenWriteBarrier((&___xMin_21), value);
	}

	inline static int32_t get_offset_of_yMin_22() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ___yMin_22)); }
	inline FsmFloat_t2883254149 * get_yMin_22() const { return ___yMin_22; }
	inline FsmFloat_t2883254149 ** get_address_of_yMin_22() { return &___yMin_22; }
	inline void set_yMin_22(FsmFloat_t2883254149 * value)
	{
		___yMin_22 = value;
		Il2CppCodeGenWriteBarrier((&___yMin_22), value);
	}

	inline static int32_t get_offset_of__rt_23() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t1703422762, ____rt_23)); }
	inline RectTransform_t3704657025 * get__rt_23() const { return ____rt_23; }
	inline RectTransform_t3704657025 ** get_address_of__rt_23() { return &____rt_23; }
	inline void set__rt_23(RectTransform_t3704657025 * value)
	{
		____rt_23 = value;
		Il2CppCodeGenWriteBarrier((&____rt_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETANCHORMINANDMAX_T1703422762_H
#ifndef RECTTRANSFORMSETANCHORRECTPOSITION_T4095992421_H
#define RECTTRANSFORMSETANCHORRECTPOSITION_T4095992421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition
struct  RectTransformSetAnchorRectPosition_t4095992421  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition/AnchorReference HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::anchorReference
	int32_t ___anchorReference_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::normalized
	FsmBool_t163807967 * ___normalized_18;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::anchor
	FsmVector2_t2965096677 * ___anchor_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::x
	FsmFloat_t2883254149 * ___x_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::y
	FsmFloat_t2883254149 * ___y_21;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::_rt
	RectTransform_t3704657025 * ____rt_22;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::_anchorRect
	Rect_t2360479859  ____anchorRect_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_anchorReference_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ___anchorReference_17)); }
	inline int32_t get_anchorReference_17() const { return ___anchorReference_17; }
	inline int32_t* get_address_of_anchorReference_17() { return &___anchorReference_17; }
	inline void set_anchorReference_17(int32_t value)
	{
		___anchorReference_17 = value;
	}

	inline static int32_t get_offset_of_normalized_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ___normalized_18)); }
	inline FsmBool_t163807967 * get_normalized_18() const { return ___normalized_18; }
	inline FsmBool_t163807967 ** get_address_of_normalized_18() { return &___normalized_18; }
	inline void set_normalized_18(FsmBool_t163807967 * value)
	{
		___normalized_18 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_18), value);
	}

	inline static int32_t get_offset_of_anchor_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ___anchor_19)); }
	inline FsmVector2_t2965096677 * get_anchor_19() const { return ___anchor_19; }
	inline FsmVector2_t2965096677 ** get_address_of_anchor_19() { return &___anchor_19; }
	inline void set_anchor_19(FsmVector2_t2965096677 * value)
	{
		___anchor_19 = value;
		Il2CppCodeGenWriteBarrier((&___anchor_19), value);
	}

	inline static int32_t get_offset_of_x_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ___x_20)); }
	inline FsmFloat_t2883254149 * get_x_20() const { return ___x_20; }
	inline FsmFloat_t2883254149 ** get_address_of_x_20() { return &___x_20; }
	inline void set_x_20(FsmFloat_t2883254149 * value)
	{
		___x_20 = value;
		Il2CppCodeGenWriteBarrier((&___x_20), value);
	}

	inline static int32_t get_offset_of_y_21() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ___y_21)); }
	inline FsmFloat_t2883254149 * get_y_21() const { return ___y_21; }
	inline FsmFloat_t2883254149 ** get_address_of_y_21() { return &___y_21; }
	inline void set_y_21(FsmFloat_t2883254149 * value)
	{
		___y_21 = value;
		Il2CppCodeGenWriteBarrier((&___y_21), value);
	}

	inline static int32_t get_offset_of__rt_22() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ____rt_22)); }
	inline RectTransform_t3704657025 * get__rt_22() const { return ____rt_22; }
	inline RectTransform_t3704657025 ** get_address_of__rt_22() { return &____rt_22; }
	inline void set__rt_22(RectTransform_t3704657025 * value)
	{
		____rt_22 = value;
		Il2CppCodeGenWriteBarrier((&____rt_22), value);
	}

	inline static int32_t get_offset_of__anchorRect_23() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4095992421, ____anchorRect_23)); }
	inline Rect_t2360479859  get__anchorRect_23() const { return ____anchorRect_23; }
	inline Rect_t2360479859 * get_address_of__anchorRect_23() { return &____anchorRect_23; }
	inline void set__anchorRect_23(Rect_t2360479859  value)
	{
		____anchorRect_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETANCHORRECTPOSITION_T4095992421_H
#ifndef RECTTRANSFORMSETANCHOREDPOSITION_T524600729_H
#define RECTTRANSFORMSETANCHOREDPOSITION_T524600729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition
struct  RectTransformSetAnchoredPosition_t524600729  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::position
	FsmVector2_t2965096677 * ___position_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchoredPosition_t524600729, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_position_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchoredPosition_t524600729, ___position_17)); }
	inline FsmVector2_t2965096677 * get_position_17() const { return ___position_17; }
	inline FsmVector2_t2965096677 ** get_address_of_position_17() { return &___position_17; }
	inline void set_position_17(FsmVector2_t2965096677 * value)
	{
		___position_17 = value;
		Il2CppCodeGenWriteBarrier((&___position_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchoredPosition_t524600729, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchoredPosition_t524600729, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchoredPosition_t524600729, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETANCHOREDPOSITION_T524600729_H
#ifndef RECTTRANSFORMSETLOCALPOSITION_T2000355408_H
#define RECTTRANSFORMSETLOCALPOSITION_T2000355408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition
struct  RectTransformSetLocalPosition_t2000355408  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::position2d
	FsmVector2_t2965096677 * ___position2d_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::position
	FsmVector3_t626444517 * ___position_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::x
	FsmFloat_t2883254149 * ___x_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::y
	FsmFloat_t2883254149 * ___y_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::z
	FsmFloat_t2883254149 * ___z_21;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::_rt
	RectTransform_t3704657025 * ____rt_22;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_position2d_17() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ___position2d_17)); }
	inline FsmVector2_t2965096677 * get_position2d_17() const { return ___position2d_17; }
	inline FsmVector2_t2965096677 ** get_address_of_position2d_17() { return &___position2d_17; }
	inline void set_position2d_17(FsmVector2_t2965096677 * value)
	{
		___position2d_17 = value;
		Il2CppCodeGenWriteBarrier((&___position2d_17), value);
	}

	inline static int32_t get_offset_of_position_18() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ___position_18)); }
	inline FsmVector3_t626444517 * get_position_18() const { return ___position_18; }
	inline FsmVector3_t626444517 ** get_address_of_position_18() { return &___position_18; }
	inline void set_position_18(FsmVector3_t626444517 * value)
	{
		___position_18 = value;
		Il2CppCodeGenWriteBarrier((&___position_18), value);
	}

	inline static int32_t get_offset_of_x_19() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ___x_19)); }
	inline FsmFloat_t2883254149 * get_x_19() const { return ___x_19; }
	inline FsmFloat_t2883254149 ** get_address_of_x_19() { return &___x_19; }
	inline void set_x_19(FsmFloat_t2883254149 * value)
	{
		___x_19 = value;
		Il2CppCodeGenWriteBarrier((&___x_19), value);
	}

	inline static int32_t get_offset_of_y_20() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ___y_20)); }
	inline FsmFloat_t2883254149 * get_y_20() const { return ___y_20; }
	inline FsmFloat_t2883254149 ** get_address_of_y_20() { return &___y_20; }
	inline void set_y_20(FsmFloat_t2883254149 * value)
	{
		___y_20 = value;
		Il2CppCodeGenWriteBarrier((&___y_20), value);
	}

	inline static int32_t get_offset_of_z_21() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ___z_21)); }
	inline FsmFloat_t2883254149 * get_z_21() const { return ___z_21; }
	inline FsmFloat_t2883254149 ** get_address_of_z_21() { return &___z_21; }
	inline void set_z_21(FsmFloat_t2883254149 * value)
	{
		___z_21 = value;
		Il2CppCodeGenWriteBarrier((&___z_21), value);
	}

	inline static int32_t get_offset_of__rt_22() { return static_cast<int32_t>(offsetof(RectTransformSetLocalPosition_t2000355408, ____rt_22)); }
	inline RectTransform_t3704657025 * get__rt_22() const { return ____rt_22; }
	inline RectTransform_t3704657025 ** get_address_of__rt_22() { return &____rt_22; }
	inline void set__rt_22(RectTransform_t3704657025 * value)
	{
		____rt_22 = value;
		Il2CppCodeGenWriteBarrier((&____rt_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETLOCALPOSITION_T2000355408_H
#ifndef RECTTRANSFORMSETLOCALROTATION_T1266785672_H
#define RECTTRANSFORMSETLOCALROTATION_T1266785672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation
struct  RectTransformSetLocalRotation_t1266785672  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::rotation
	FsmVector3_t626444517 * ___rotation_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::y
	FsmFloat_t2883254149 * ___y_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::z
	FsmFloat_t2883254149 * ___z_20;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::_rt
	RectTransform_t3704657025 * ____rt_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetLocalRotation_t1266785672, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_rotation_17() { return static_cast<int32_t>(offsetof(RectTransformSetLocalRotation_t1266785672, ___rotation_17)); }
	inline FsmVector3_t626444517 * get_rotation_17() const { return ___rotation_17; }
	inline FsmVector3_t626444517 ** get_address_of_rotation_17() { return &___rotation_17; }
	inline void set_rotation_17(FsmVector3_t626444517 * value)
	{
		___rotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetLocalRotation_t1266785672, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetLocalRotation_t1266785672, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of_z_20() { return static_cast<int32_t>(offsetof(RectTransformSetLocalRotation_t1266785672, ___z_20)); }
	inline FsmFloat_t2883254149 * get_z_20() const { return ___z_20; }
	inline FsmFloat_t2883254149 ** get_address_of_z_20() { return &___z_20; }
	inline void set_z_20(FsmFloat_t2883254149 * value)
	{
		___z_20 = value;
		Il2CppCodeGenWriteBarrier((&___z_20), value);
	}

	inline static int32_t get_offset_of__rt_21() { return static_cast<int32_t>(offsetof(RectTransformSetLocalRotation_t1266785672, ____rt_21)); }
	inline RectTransform_t3704657025 * get__rt_21() const { return ____rt_21; }
	inline RectTransform_t3704657025 ** get_address_of__rt_21() { return &____rt_21; }
	inline void set__rt_21(RectTransform_t3704657025 * value)
	{
		____rt_21 = value;
		Il2CppCodeGenWriteBarrier((&____rt_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETLOCALROTATION_T1266785672_H
#ifndef RECTTRANSFORMSETOFFSETMAX_T1075876260_H
#define RECTTRANSFORMSETOFFSETMAX_T1075876260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax
struct  RectTransformSetOffsetMax_t1075876260  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::offsetMax
	FsmVector2_t2965096677 * ___offsetMax_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t1075876260, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_offsetMax_17() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t1075876260, ___offsetMax_17)); }
	inline FsmVector2_t2965096677 * get_offsetMax_17() const { return ___offsetMax_17; }
	inline FsmVector2_t2965096677 ** get_address_of_offsetMax_17() { return &___offsetMax_17; }
	inline void set_offsetMax_17(FsmVector2_t2965096677 * value)
	{
		___offsetMax_17 = value;
		Il2CppCodeGenWriteBarrier((&___offsetMax_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t1075876260, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t1075876260, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t1075876260, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETOFFSETMAX_T1075876260_H
#ifndef RECTTRANSFORMSETOFFSETMIN_T3496107710_H
#define RECTTRANSFORMSETOFFSETMIN_T3496107710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin
struct  RectTransformSetOffsetMin_t3496107710  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::offsetMin
	FsmVector2_t2965096677 * ___offsetMin_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMin_t3496107710, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_offsetMin_17() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMin_t3496107710, ___offsetMin_17)); }
	inline FsmVector2_t2965096677 * get_offsetMin_17() const { return ___offsetMin_17; }
	inline FsmVector2_t2965096677 ** get_address_of_offsetMin_17() { return &___offsetMin_17; }
	inline void set_offsetMin_17(FsmVector2_t2965096677 * value)
	{
		___offsetMin_17 = value;
		Il2CppCodeGenWriteBarrier((&___offsetMin_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMin_t3496107710, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMin_t3496107710, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMin_t3496107710, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETOFFSETMIN_T3496107710_H
#ifndef RECTTRANSFORMSETPIVOT_T2834804063_H
#define RECTTRANSFORMSETPIVOT_T2834804063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetPivot
struct  RectTransformSetPivot_t2834804063  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetPivot::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetPivot::pivot
	FsmVector2_t2965096677 * ___pivot_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetPivot::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetPivot::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetPivot::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetPivot_t2834804063, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_pivot_17() { return static_cast<int32_t>(offsetof(RectTransformSetPivot_t2834804063, ___pivot_17)); }
	inline FsmVector2_t2965096677 * get_pivot_17() const { return ___pivot_17; }
	inline FsmVector2_t2965096677 ** get_address_of_pivot_17() { return &___pivot_17; }
	inline void set_pivot_17(FsmVector2_t2965096677 * value)
	{
		___pivot_17 = value;
		Il2CppCodeGenWriteBarrier((&___pivot_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetPivot_t2834804063, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetPivot_t2834804063, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetPivot_t2834804063, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETPIVOT_T2834804063_H
#ifndef RECTTRANSFORMSETSIZEDELTA_T816513331_H
#define RECTTRANSFORMSETSIZEDELTA_T816513331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta
struct  RectTransformSetSizeDelta_t816513331  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::sizeDelta
	FsmVector2_t2965096677 * ___sizeDelta_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::x
	FsmFloat_t2883254149 * ___x_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::y
	FsmFloat_t2883254149 * ___y_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::_rt
	RectTransform_t3704657025 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformSetSizeDelta_t816513331, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_sizeDelta_17() { return static_cast<int32_t>(offsetof(RectTransformSetSizeDelta_t816513331, ___sizeDelta_17)); }
	inline FsmVector2_t2965096677 * get_sizeDelta_17() const { return ___sizeDelta_17; }
	inline FsmVector2_t2965096677 ** get_address_of_sizeDelta_17() { return &___sizeDelta_17; }
	inline void set_sizeDelta_17(FsmVector2_t2965096677 * value)
	{
		___sizeDelta_17 = value;
		Il2CppCodeGenWriteBarrier((&___sizeDelta_17), value);
	}

	inline static int32_t get_offset_of_x_18() { return static_cast<int32_t>(offsetof(RectTransformSetSizeDelta_t816513331, ___x_18)); }
	inline FsmFloat_t2883254149 * get_x_18() const { return ___x_18; }
	inline FsmFloat_t2883254149 ** get_address_of_x_18() { return &___x_18; }
	inline void set_x_18(FsmFloat_t2883254149 * value)
	{
		___x_18 = value;
		Il2CppCodeGenWriteBarrier((&___x_18), value);
	}

	inline static int32_t get_offset_of_y_19() { return static_cast<int32_t>(offsetof(RectTransformSetSizeDelta_t816513331, ___y_19)); }
	inline FsmFloat_t2883254149 * get_y_19() const { return ___y_19; }
	inline FsmFloat_t2883254149 ** get_address_of_y_19() { return &___y_19; }
	inline void set_y_19(FsmFloat_t2883254149 * value)
	{
		___y_19 = value;
		Il2CppCodeGenWriteBarrier((&___y_19), value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetSizeDelta_t816513331, ____rt_20)); }
	inline RectTransform_t3704657025 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3704657025 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3704657025 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier((&____rt_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMSETSIZEDELTA_T816513331_H
#ifndef RECTTRANSFORMWORLDTOSCREENPOINT_T1753462501_H
#define RECTTRANSFORMWORLDTOSCREENPOINT_T1753462501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint
struct  RectTransformWorldToScreenPoint_t1753462501  : public BaseUpdateAction_t497653765
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::camera
	FsmOwnerDefault_t3590610434 * ___camera_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::screenPoint
	FsmVector3_t626444517 * ___screenPoint_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::screenX
	FsmFloat_t2883254149 * ___screenX_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::screenY
	FsmFloat_t2883254149 * ___screenY_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::normalize
	FsmBool_t163807967 * ___normalize_21;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::_rt
	RectTransform_t3704657025 * ____rt_22;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::_cam
	Camera_t4157153871 * ____cam_23;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_camera_17() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ___camera_17)); }
	inline FsmOwnerDefault_t3590610434 * get_camera_17() const { return ___camera_17; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_camera_17() { return &___camera_17; }
	inline void set_camera_17(FsmOwnerDefault_t3590610434 * value)
	{
		___camera_17 = value;
		Il2CppCodeGenWriteBarrier((&___camera_17), value);
	}

	inline static int32_t get_offset_of_screenPoint_18() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ___screenPoint_18)); }
	inline FsmVector3_t626444517 * get_screenPoint_18() const { return ___screenPoint_18; }
	inline FsmVector3_t626444517 ** get_address_of_screenPoint_18() { return &___screenPoint_18; }
	inline void set_screenPoint_18(FsmVector3_t626444517 * value)
	{
		___screenPoint_18 = value;
		Il2CppCodeGenWriteBarrier((&___screenPoint_18), value);
	}

	inline static int32_t get_offset_of_screenX_19() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ___screenX_19)); }
	inline FsmFloat_t2883254149 * get_screenX_19() const { return ___screenX_19; }
	inline FsmFloat_t2883254149 ** get_address_of_screenX_19() { return &___screenX_19; }
	inline void set_screenX_19(FsmFloat_t2883254149 * value)
	{
		___screenX_19 = value;
		Il2CppCodeGenWriteBarrier((&___screenX_19), value);
	}

	inline static int32_t get_offset_of_screenY_20() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ___screenY_20)); }
	inline FsmFloat_t2883254149 * get_screenY_20() const { return ___screenY_20; }
	inline FsmFloat_t2883254149 ** get_address_of_screenY_20() { return &___screenY_20; }
	inline void set_screenY_20(FsmFloat_t2883254149 * value)
	{
		___screenY_20 = value;
		Il2CppCodeGenWriteBarrier((&___screenY_20), value);
	}

	inline static int32_t get_offset_of_normalize_21() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ___normalize_21)); }
	inline FsmBool_t163807967 * get_normalize_21() const { return ___normalize_21; }
	inline FsmBool_t163807967 ** get_address_of_normalize_21() { return &___normalize_21; }
	inline void set_normalize_21(FsmBool_t163807967 * value)
	{
		___normalize_21 = value;
		Il2CppCodeGenWriteBarrier((&___normalize_21), value);
	}

	inline static int32_t get_offset_of__rt_22() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ____rt_22)); }
	inline RectTransform_t3704657025 * get__rt_22() const { return ____rt_22; }
	inline RectTransform_t3704657025 ** get_address_of__rt_22() { return &____rt_22; }
	inline void set__rt_22(RectTransform_t3704657025 * value)
	{
		____rt_22 = value;
		Il2CppCodeGenWriteBarrier((&____rt_22), value);
	}

	inline static int32_t get_offset_of__cam_23() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1753462501, ____cam_23)); }
	inline Camera_t4157153871 * get__cam_23() const { return ____cam_23; }
	inline Camera_t4157153871 ** get_address_of__cam_23() { return &____cam_23; }
	inline void set__cam_23(Camera_t4157153871 * value)
	{
		____cam_23 = value;
		Il2CppCodeGenWriteBarrier((&____cam_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORMWORLDTOSCREENPOINT_T1753462501_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (PlayerPrefsSetString_t337717693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[2] = 
{
	PlayerPrefsSetString_t337717693::get_offset_of_keys_14(),
	PlayerPrefsSetString_t337717693::get_offset_of_values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (RebuildTextures_t3968693992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[3] = 
{
	RebuildTextures_t3968693992::get_offset_of_substanceMaterial_14(),
	RebuildTextures_t3968693992::get_offset_of_immediately_15(),
	RebuildTextures_t3968693992::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (SetProceduralBoolean_t384194045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[4] = 
{
	SetProceduralBoolean_t384194045::get_offset_of_substanceMaterial_14(),
	SetProceduralBoolean_t384194045::get_offset_of_boolProperty_15(),
	SetProceduralBoolean_t384194045::get_offset_of_boolValue_16(),
	SetProceduralBoolean_t384194045::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (SetProceduralColor_t3770124595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3503[4] = 
{
	SetProceduralColor_t3770124595::get_offset_of_substanceMaterial_14(),
	SetProceduralColor_t3770124595::get_offset_of_colorProperty_15(),
	SetProceduralColor_t3770124595::get_offset_of_colorValue_16(),
	SetProceduralColor_t3770124595::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (SetProceduralFloat_t1084947234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[4] = 
{
	SetProceduralFloat_t1084947234::get_offset_of_substanceMaterial_14(),
	SetProceduralFloat_t1084947234::get_offset_of_floatProperty_15(),
	SetProceduralFloat_t1084947234::get_offset_of_floatValue_16(),
	SetProceduralFloat_t1084947234::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (SetProceduralVector2_t2655254814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[4] = 
{
	SetProceduralVector2_t2655254814::get_offset_of_substanceMaterial_14(),
	SetProceduralVector2_t2655254814::get_offset_of_vector2Property_15(),
	SetProceduralVector2_t2655254814::get_offset_of_vector2Value_16(),
	SetProceduralVector2_t2655254814::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (SetProceduralVector3_t2655189278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3506[4] = 
{
	SetProceduralVector3_t2655189278::get_offset_of_substanceMaterial_14(),
	SetProceduralVector3_t2655189278::get_offset_of_vector3Property_15(),
	SetProceduralVector3_t2655189278::get_offset_of_vector3Value_16(),
	SetProceduralVector3_t2655189278::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (GetQuaternionEulerAngles_t823427448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3507[2] = 
{
	GetQuaternionEulerAngles_t823427448::get_offset_of_quaternion_16(),
	GetQuaternionEulerAngles_t823427448::get_offset_of_eulerAngles_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (GetQuaternionFromRotation_t1468389705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3508[3] = 
{
	GetQuaternionFromRotation_t1468389705::get_offset_of_fromDirection_16(),
	GetQuaternionFromRotation_t1468389705::get_offset_of_toDirection_17(),
	GetQuaternionFromRotation_t1468389705::get_offset_of_result_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (GetQuaternionMultipliedByQuaternion_t2297620533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3509[3] = 
{
	GetQuaternionMultipliedByQuaternion_t2297620533::get_offset_of_quaternionA_16(),
	GetQuaternionMultipliedByQuaternion_t2297620533::get_offset_of_quaternionB_17(),
	GetQuaternionMultipliedByQuaternion_t2297620533::get_offset_of_result_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (GetQuaternionMultipliedByVector_t2299213397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3510[3] = 
{
	GetQuaternionMultipliedByVector_t2299213397::get_offset_of_quaternion_16(),
	GetQuaternionMultipliedByVector_t2299213397::get_offset_of_vector3_17(),
	GetQuaternionMultipliedByVector_t2299213397::get_offset_of_result_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (QuaternionAngleAxis_t2080243336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3511[3] = 
{
	QuaternionAngleAxis_t2080243336::get_offset_of_angle_16(),
	QuaternionAngleAxis_t2080243336::get_offset_of_axis_17(),
	QuaternionAngleAxis_t2080243336::get_offset_of_result_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (QuaternionBaseAction_t3672550121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3512[2] = 
{
	QuaternionBaseAction_t3672550121::get_offset_of_everyFrame_14(),
	QuaternionBaseAction_t3672550121::get_offset_of_everyFrameOption_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (everyFrameOptions_t3183592540)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3513[4] = 
{
	everyFrameOptions_t3183592540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (QuaternionCompare_t1279087300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3514[5] = 
{
	QuaternionCompare_t1279087300::get_offset_of_Quaternion1_16(),
	QuaternionCompare_t1279087300::get_offset_of_Quaternion2_17(),
	QuaternionCompare_t1279087300::get_offset_of_equal_18(),
	QuaternionCompare_t1279087300::get_offset_of_equalEvent_19(),
	QuaternionCompare_t1279087300::get_offset_of_notEqualEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (QuaternionEuler_t1634553366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3515[2] = 
{
	QuaternionEuler_t1634553366::get_offset_of_eulerAngles_16(),
	QuaternionEuler_t1634553366::get_offset_of_result_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (QuaternionInverse_t1832578058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3516[2] = 
{
	QuaternionInverse_t1832578058::get_offset_of_rotation_16(),
	QuaternionInverse_t1832578058::get_offset_of_result_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (QuaternionLerp_t3856070935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3517[4] = 
{
	QuaternionLerp_t3856070935::get_offset_of_fromQuaternion_16(),
	QuaternionLerp_t3856070935::get_offset_of_toQuaternion_17(),
	QuaternionLerp_t3856070935::get_offset_of_amount_18(),
	QuaternionLerp_t3856070935::get_offset_of_storeResult_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (QuaternionLookRotation_t2008970098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3518[3] = 
{
	QuaternionLookRotation_t2008970098::get_offset_of_direction_16(),
	QuaternionLookRotation_t2008970098::get_offset_of_upVector_17(),
	QuaternionLookRotation_t2008970098::get_offset_of_result_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (QuaternionLowPassFilter_t4253160803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3519[3] = 
{
	QuaternionLowPassFilter_t4253160803::get_offset_of_quaternionVariable_16(),
	QuaternionLowPassFilter_t4253160803::get_offset_of_filteringFactor_17(),
	QuaternionLowPassFilter_t4253160803::get_offset_of_filteredQuaternion_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (QuaternionRotateTowards_t2237857460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3520[4] = 
{
	QuaternionRotateTowards_t2237857460::get_offset_of_fromQuaternion_16(),
	QuaternionRotateTowards_t2237857460::get_offset_of_toQuaternion_17(),
	QuaternionRotateTowards_t2237857460::get_offset_of_maxDegreesDelta_18(),
	QuaternionRotateTowards_t2237857460::get_offset_of_storeResult_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (QuaternionSlerp_t4066974401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3521[4] = 
{
	QuaternionSlerp_t4066974401::get_offset_of_fromQuaternion_16(),
	QuaternionSlerp_t4066974401::get_offset_of_toQuaternion_17(),
	QuaternionSlerp_t4066974401::get_offset_of_amount_18(),
	QuaternionSlerp_t4066974401::get_offset_of_storeResult_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (GetRectFields_t2150919948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3522[6] = 
{
	GetRectFields_t2150919948::get_offset_of_rectVariable_14(),
	GetRectFields_t2150919948::get_offset_of_storeX_15(),
	GetRectFields_t2150919948::get_offset_of_storeY_16(),
	GetRectFields_t2150919948::get_offset_of_storeWidth_17(),
	GetRectFields_t2150919948::get_offset_of_storeHeight_18(),
	GetRectFields_t2150919948::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (RectContains_t3948379732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[8] = 
{
	RectContains_t3948379732::get_offset_of_rectangle_14(),
	RectContains_t3948379732::get_offset_of_point_15(),
	RectContains_t3948379732::get_offset_of_x_16(),
	RectContains_t3948379732::get_offset_of_y_17(),
	RectContains_t3948379732::get_offset_of_trueEvent_18(),
	RectContains_t3948379732::get_offset_of_falseEvent_19(),
	RectContains_t3948379732::get_offset_of_storeResult_20(),
	RectContains_t3948379732::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (RectOverlaps_t1548172352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[6] = 
{
	RectOverlaps_t1548172352::get_offset_of_rect1_14(),
	RectOverlaps_t1548172352::get_offset_of_rect2_15(),
	RectOverlaps_t1548172352::get_offset_of_trueEvent_16(),
	RectOverlaps_t1548172352::get_offset_of_falseEvent_17(),
	RectOverlaps_t1548172352::get_offset_of_storeResult_18(),
	RectOverlaps_t1548172352::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (SetRectFields_t3909148976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[6] = 
{
	SetRectFields_t3909148976::get_offset_of_rectVariable_14(),
	SetRectFields_t3909148976::get_offset_of_x_15(),
	SetRectFields_t3909148976::get_offset_of_y_16(),
	SetRectFields_t3909148976::get_offset_of_width_17(),
	SetRectFields_t3909148976::get_offset_of_height_18(),
	SetRectFields_t3909148976::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (SetRectValue_t2541018530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3526[3] = 
{
	SetRectValue_t2541018530::get_offset_of_rectVariable_14(),
	SetRectValue_t2541018530::get_offset_of_rectValue_15(),
	SetRectValue_t2541018530::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (RectTransformContainsScreenPoint_t106483262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3527[11] = 
{
	RectTransformContainsScreenPoint_t106483262::get_offset_of_gameObject_14(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_screenPointVector2_15(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_orScreenPointVector3_16(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_normalizedScreenPoint_17(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_camera_18(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_everyFrame_19(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_isContained_20(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_isContainedEvent_21(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of_isNotContainedEvent_22(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of__rt_23(),
	RectTransformContainsScreenPoint_t106483262::get_offset_of__camera_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (RectTransformFlipLayoutAxis_t2865099254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3528[4] = 
{
	RectTransformFlipLayoutAxis_t2865099254::get_offset_of_gameObject_14(),
	RectTransformFlipLayoutAxis_t2865099254::get_offset_of_axis_15(),
	RectTransformFlipLayoutAxis_t2865099254::get_offset_of_keepPositioning_16(),
	RectTransformFlipLayoutAxis_t2865099254::get_offset_of_recursive_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (RectTransformFlipOptions_t3615818486)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3529[4] = 
{
	RectTransformFlipOptions_t3615818486::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (RectTransformGetAnchoredPosition_t2956266264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3530[5] = 
{
	RectTransformGetAnchoredPosition_t2956266264::get_offset_of_gameObject_16(),
	RectTransformGetAnchoredPosition_t2956266264::get_offset_of_position_17(),
	RectTransformGetAnchoredPosition_t2956266264::get_offset_of_x_18(),
	RectTransformGetAnchoredPosition_t2956266264::get_offset_of_y_19(),
	RectTransformGetAnchoredPosition_t2956266264::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (RectTransformGetAnchorMax_t3935051232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3531[5] = 
{
	RectTransformGetAnchorMax_t3935051232::get_offset_of_gameObject_16(),
	RectTransformGetAnchorMax_t3935051232::get_offset_of_anchorMax_17(),
	RectTransformGetAnchorMax_t3935051232::get_offset_of_x_18(),
	RectTransformGetAnchorMax_t3935051232::get_offset_of_y_19(),
	RectTransformGetAnchorMax_t3935051232::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (RectTransformGetAnchorMin_t4028635278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3532[5] = 
{
	RectTransformGetAnchorMin_t4028635278::get_offset_of_gameObject_16(),
	RectTransformGetAnchorMin_t4028635278::get_offset_of_anchorMin_17(),
	RectTransformGetAnchorMin_t4028635278::get_offset_of_x_18(),
	RectTransformGetAnchorMin_t4028635278::get_offset_of_y_19(),
	RectTransformGetAnchorMin_t4028635278::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (RectTransformGetAnchorMinAndMax_t344340047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3533[8] = 
{
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_gameObject_16(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_anchorMax_17(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_anchorMin_18(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_xMax_19(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_yMax_20(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_xMin_21(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of_yMin_22(),
	RectTransformGetAnchorMinAndMax_t344340047::get_offset_of__rt_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (RectTransformGetLocalPosition_t2637760766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3534[8] = 
{
	RectTransformGetLocalPosition_t2637760766::get_offset_of_gameObject_16(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of_reference_17(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of_position_18(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of_position2d_19(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of_x_20(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of_y_21(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of_z_22(),
	RectTransformGetLocalPosition_t2637760766::get_offset_of__rt_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (LocalPositionReference_t3695602212)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3535[3] = 
{
	LocalPositionReference_t3695602212::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (RectTransformGetLocalRotation_t3876875206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3536[6] = 
{
	RectTransformGetLocalRotation_t3876875206::get_offset_of_gameObject_16(),
	RectTransformGetLocalRotation_t3876875206::get_offset_of_rotation_17(),
	RectTransformGetLocalRotation_t3876875206::get_offset_of_x_18(),
	RectTransformGetLocalRotation_t3876875206::get_offset_of_y_19(),
	RectTransformGetLocalRotation_t3876875206::get_offset_of_z_20(),
	RectTransformGetLocalRotation_t3876875206::get_offset_of__rt_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (RectTransformGetOffsetMax_t72590789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3537[5] = 
{
	RectTransformGetOffsetMax_t72590789::get_offset_of_gameObject_16(),
	RectTransformGetOffsetMax_t72590789::get_offset_of_offsetMax_17(),
	RectTransformGetOffsetMax_t72590789::get_offset_of_x_18(),
	RectTransformGetOffsetMax_t72590789::get_offset_of_y_19(),
	RectTransformGetOffsetMax_t72590789::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (RectTransformGetOffsetMin_t1592144851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3538[5] = 
{
	RectTransformGetOffsetMin_t1592144851::get_offset_of_gameObject_16(),
	RectTransformGetOffsetMin_t1592144851::get_offset_of_offsetMin_17(),
	RectTransformGetOffsetMin_t1592144851::get_offset_of_x_18(),
	RectTransformGetOffsetMin_t1592144851::get_offset_of_y_19(),
	RectTransformGetOffsetMin_t1592144851::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (RectTransformGetPivot_t922167116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3539[5] = 
{
	RectTransformGetPivot_t922167116::get_offset_of_gameObject_16(),
	RectTransformGetPivot_t922167116::get_offset_of_pivot_17(),
	RectTransformGetPivot_t922167116::get_offset_of_x_18(),
	RectTransformGetPivot_t922167116::get_offset_of_y_19(),
	RectTransformGetPivot_t922167116::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (RectTransformGetRect_t1452626980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3540[7] = 
{
	RectTransformGetRect_t1452626980::get_offset_of_gameObject_16(),
	RectTransformGetRect_t1452626980::get_offset_of_rect_17(),
	RectTransformGetRect_t1452626980::get_offset_of_x_18(),
	RectTransformGetRect_t1452626980::get_offset_of_y_19(),
	RectTransformGetRect_t1452626980::get_offset_of_width_20(),
	RectTransformGetRect_t1452626980::get_offset_of_height_21(),
	RectTransformGetRect_t1452626980::get_offset_of__rt_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (RectTransformGetSizeDelta_t334801076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3541[5] = 
{
	RectTransformGetSizeDelta_t334801076::get_offset_of_gameObject_16(),
	RectTransformGetSizeDelta_t334801076::get_offset_of_sizeDelta_17(),
	RectTransformGetSizeDelta_t334801076::get_offset_of_width_18(),
	RectTransformGetSizeDelta_t334801076::get_offset_of_height_19(),
	RectTransformGetSizeDelta_t334801076::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (RectTransformPixelAdjustPoint_t2444478249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[6] = 
{
	RectTransformPixelAdjustPoint_t2444478249::get_offset_of_gameObject_16(),
	RectTransformPixelAdjustPoint_t2444478249::get_offset_of_canvas_17(),
	RectTransformPixelAdjustPoint_t2444478249::get_offset_of_screenPoint_18(),
	RectTransformPixelAdjustPoint_t2444478249::get_offset_of_pixelPoint_19(),
	RectTransformPixelAdjustPoint_t2444478249::get_offset_of__rt_20(),
	RectTransformPixelAdjustPoint_t2444478249::get_offset_of__canvas_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (RectTransformPixelAdjustRect_t2193207965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[5] = 
{
	RectTransformPixelAdjustRect_t2193207965::get_offset_of_gameObject_16(),
	RectTransformPixelAdjustRect_t2193207965::get_offset_of_canvas_17(),
	RectTransformPixelAdjustRect_t2193207965::get_offset_of_pixelRect_18(),
	RectTransformPixelAdjustRect_t2193207965::get_offset_of__rt_19(),
	RectTransformPixelAdjustRect_t2193207965::get_offset_of__canvas_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (RectTransformScreenPointToLocalPointInRectangle_t1705222147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3544[13] = 
{
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_gameObject_14(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_screenPointVector2_15(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_orScreenPointVector3_16(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_normalizedScreenPoint_17(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_camera_18(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_everyFrame_19(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_localPosition_20(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_localPosition2d_21(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_isHit_22(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_hitEvent_23(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of_noHitEvent_24(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of__rt_25(),
	RectTransformScreenPointToLocalPointInRectangle_t1705222147::get_offset_of__camera_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (RectTransformScreenPointToWorldPointInRectangle_t163018785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3545[12] = 
{
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_gameObject_14(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_screenPointVector2_15(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_orScreenPointVector3_16(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_normalizedScreenPoint_17(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_camera_18(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_everyFrame_19(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_worldPosition_20(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_isHit_21(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_hitEvent_22(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of_noHitEvent_23(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of__rt_24(),
	RectTransformScreenPointToWorldPointInRectangle_t163018785::get_offset_of__camera_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (RectTransformSetAnchoredPosition_t524600729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3546[5] = 
{
	RectTransformSetAnchoredPosition_t524600729::get_offset_of_gameObject_16(),
	RectTransformSetAnchoredPosition_t524600729::get_offset_of_position_17(),
	RectTransformSetAnchoredPosition_t524600729::get_offset_of_x_18(),
	RectTransformSetAnchoredPosition_t524600729::get_offset_of_y_19(),
	RectTransformSetAnchoredPosition_t524600729::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (RectTransformSetAnchorMax_t3042733759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3547[5] = 
{
	RectTransformSetAnchorMax_t3042733759::get_offset_of_gameObject_16(),
	RectTransformSetAnchorMax_t3042733759::get_offset_of_anchorMax_17(),
	RectTransformSetAnchorMax_t3042733759::get_offset_of_x_18(),
	RectTransformSetAnchorMax_t3042733759::get_offset_of_y_19(),
	RectTransformSetAnchorMax_t3042733759::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (RectTransformSetAnchorMin_t2591870777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[5] = 
{
	RectTransformSetAnchorMin_t2591870777::get_offset_of_gameObject_16(),
	RectTransformSetAnchorMin_t2591870777::get_offset_of_anchorMin_17(),
	RectTransformSetAnchorMin_t2591870777::get_offset_of_x_18(),
	RectTransformSetAnchorMin_t2591870777::get_offset_of_y_19(),
	RectTransformSetAnchorMin_t2591870777::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (RectTransformSetAnchorMinAndMax_t1703422762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[8] = 
{
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_gameObject_16(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_anchorMax_17(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_anchorMin_18(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_xMax_19(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_yMax_20(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_xMin_21(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of_yMin_22(),
	RectTransformSetAnchorMinAndMax_t1703422762::get_offset_of__rt_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (RectTransformSetAnchorRectPosition_t4095992421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3550[8] = 
{
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of_gameObject_16(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of_anchorReference_17(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of_normalized_18(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of_anchor_19(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of_x_20(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of_y_21(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of__rt_22(),
	RectTransformSetAnchorRectPosition_t4095992421::get_offset_of__anchorRect_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (AnchorReference_t1326731945)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3551[10] = 
{
	AnchorReference_t1326731945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (RectTransformSetLocalPosition_t2000355408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3552[7] = 
{
	RectTransformSetLocalPosition_t2000355408::get_offset_of_gameObject_16(),
	RectTransformSetLocalPosition_t2000355408::get_offset_of_position2d_17(),
	RectTransformSetLocalPosition_t2000355408::get_offset_of_position_18(),
	RectTransformSetLocalPosition_t2000355408::get_offset_of_x_19(),
	RectTransformSetLocalPosition_t2000355408::get_offset_of_y_20(),
	RectTransformSetLocalPosition_t2000355408::get_offset_of_z_21(),
	RectTransformSetLocalPosition_t2000355408::get_offset_of__rt_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (RectTransformSetLocalRotation_t1266785672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[6] = 
{
	RectTransformSetLocalRotation_t1266785672::get_offset_of_gameObject_16(),
	RectTransformSetLocalRotation_t1266785672::get_offset_of_rotation_17(),
	RectTransformSetLocalRotation_t1266785672::get_offset_of_x_18(),
	RectTransformSetLocalRotation_t1266785672::get_offset_of_y_19(),
	RectTransformSetLocalRotation_t1266785672::get_offset_of_z_20(),
	RectTransformSetLocalRotation_t1266785672::get_offset_of__rt_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (RectTransformSetOffsetMax_t1075876260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3554[5] = 
{
	RectTransformSetOffsetMax_t1075876260::get_offset_of_gameObject_16(),
	RectTransformSetOffsetMax_t1075876260::get_offset_of_offsetMax_17(),
	RectTransformSetOffsetMax_t1075876260::get_offset_of_x_18(),
	RectTransformSetOffsetMax_t1075876260::get_offset_of_y_19(),
	RectTransformSetOffsetMax_t1075876260::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (RectTransformSetOffsetMin_t3496107710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3555[5] = 
{
	RectTransformSetOffsetMin_t3496107710::get_offset_of_gameObject_16(),
	RectTransformSetOffsetMin_t3496107710::get_offset_of_offsetMin_17(),
	RectTransformSetOffsetMin_t3496107710::get_offset_of_x_18(),
	RectTransformSetOffsetMin_t3496107710::get_offset_of_y_19(),
	RectTransformSetOffsetMin_t3496107710::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (RectTransformSetPivot_t2834804063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3556[5] = 
{
	RectTransformSetPivot_t2834804063::get_offset_of_gameObject_16(),
	RectTransformSetPivot_t2834804063::get_offset_of_pivot_17(),
	RectTransformSetPivot_t2834804063::get_offset_of_x_18(),
	RectTransformSetPivot_t2834804063::get_offset_of_y_19(),
	RectTransformSetPivot_t2834804063::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (RectTransformSetSizeDelta_t816513331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[5] = 
{
	RectTransformSetSizeDelta_t816513331::get_offset_of_gameObject_16(),
	RectTransformSetSizeDelta_t816513331::get_offset_of_sizeDelta_17(),
	RectTransformSetSizeDelta_t816513331::get_offset_of_x_18(),
	RectTransformSetSizeDelta_t816513331::get_offset_of_y_19(),
	RectTransformSetSizeDelta_t816513331::get_offset_of__rt_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (RectTransformWorldToScreenPoint_t1753462501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[8] = 
{
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of_gameObject_16(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of_camera_17(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of_screenPoint_18(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of_screenX_19(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of_screenY_20(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of_normalize_21(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of__rt_22(),
	RectTransformWorldToScreenPoint_t1753462501::get_offset_of__cam_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (EnableFog_t1758211407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3559[2] = 
{
	EnableFog_t1758211407::get_offset_of_enableFog_14(),
	EnableFog_t1758211407::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (SetAmbientLight_t2149961347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3560[2] = 
{
	SetAmbientLight_t2149961347::get_offset_of_ambientColor_14(),
	SetAmbientLight_t2149961347::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (SetFlareStrength_t1365963185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[2] = 
{
	SetFlareStrength_t1365963185::get_offset_of_flareStrength_14(),
	SetFlareStrength_t1365963185::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (SetFogColor_t3043078047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3562[2] = 
{
	SetFogColor_t3043078047::get_offset_of_fogColor_14(),
	SetFogColor_t3043078047::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (SetFogDensity_t1161296091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3563[2] = 
{
	SetFogDensity_t1161296091::get_offset_of_fogDensity_14(),
	SetFogDensity_t1161296091::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (SetHaloStrength_t3595622030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3564[2] = 
{
	SetHaloStrength_t3595622030::get_offset_of_haloStrength_14(),
	SetHaloStrength_t3595622030::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (SetSkybox_t109181542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3565[2] = 
{
	SetSkybox_t109181542::get_offset_of_skybox_14(),
	SetSkybox_t109181542::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (AllowSceneActivation_t2067239854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3566[6] = 
{
	AllowSceneActivation_t2067239854::get_offset_of_aSynchOperationHashCode_14(),
	AllowSceneActivation_t2067239854::get_offset_of_allowSceneActivation_15(),
	AllowSceneActivation_t2067239854::get_offset_of_progress_16(),
	AllowSceneActivation_t2067239854::get_offset_of_isDone_17(),
	AllowSceneActivation_t2067239854::get_offset_of_doneEvent_18(),
	AllowSceneActivation_t2067239854::get_offset_of_failureEvent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (CreateScene_t1317922373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3567[1] = 
{
	CreateScene_t1317922373::get_offset_of_sceneName_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (GetSceneActivateChangedEventData_t3243756110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3568[17] = 
{
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newName_14(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newPath_15(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newIsValid_16(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newBuildIndex_17(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newIsLoaded_18(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newIsDirty_19(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newRootCount_20(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_newRootGameObjects_21(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousName_22(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousPath_23(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousIsValid_24(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousBuildIndex_25(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousIsLoaded_26(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousIsDirty_27(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousRootCount_28(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of_previousRootGameObjects_29(),
	GetSceneActivateChangedEventData_t3243756110::get_offset_of__scene_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (GetSceneBuildIndex_t2039529201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3569[1] = 
{
	GetSceneBuildIndex_t2039529201::get_offset_of_buildIndex_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (GetSceneCount_t4210588623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3570[2] = 
{
	GetSceneCount_t4210588623::get_offset_of_sceneCount_14(),
	GetSceneCount_t4210588623::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (GetSceneCountInBuildSettings_t1563283541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3571[1] = 
{
	GetSceneCountInBuildSettings_t1563283541::get_offset_of_sceneCountInBuildSettings_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (GetSceneIsDirty_t3294465093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3572[4] = 
{
	GetSceneIsDirty_t3294465093::get_offset_of_isDirty_24(),
	GetSceneIsDirty_t3294465093::get_offset_of_isDirtyEvent_25(),
	GetSceneIsDirty_t3294465093::get_offset_of_isNotDirtyEvent_26(),
	GetSceneIsDirty_t3294465093::get_offset_of_everyFrame_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (GetSceneIsLoaded_t1206798716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3573[4] = 
{
	GetSceneIsLoaded_t1206798716::get_offset_of_isLoaded_24(),
	GetSceneIsLoaded_t1206798716::get_offset_of_isLoadedEvent_25(),
	GetSceneIsLoaded_t1206798716::get_offset_of_isNotLoadedEvent_26(),
	GetSceneIsLoaded_t1206798716::get_offset_of_everyFrame_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (GetSceneIsValid_t3529258936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3574[3] = 
{
	GetSceneIsValid_t3529258936::get_offset_of_isValid_24(),
	GetSceneIsValid_t3529258936::get_offset_of_isValidEvent_25(),
	GetSceneIsValid_t3529258936::get_offset_of_isNotValidEvent_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (GetSceneLoadedEventData_t1535803245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[10] = 
{
	GetSceneLoadedEventData_t1535803245::get_offset_of_loadedMode_14(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_name_15(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_path_16(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_isValid_17(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_buildIndex_18(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_isLoaded_19(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_isDirty_20(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_rootCount_21(),
	GetSceneLoadedEventData_t1535803245::get_offset_of_rootGameObjects_22(),
	GetSceneLoadedEventData_t1535803245::get_offset_of__scene_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (GetSceneName_t473291379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3576[1] = 
{
	GetSceneName_t473291379::get_offset_of_name_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (GetScenePath_t735216820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[1] = 
{
	GetScenePath_t735216820::get_offset_of_path_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (GetSceneProperties_t396337852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3578[9] = 
{
	GetSceneProperties_t396337852::get_offset_of_name_24(),
	GetSceneProperties_t396337852::get_offset_of_path_25(),
	GetSceneProperties_t396337852::get_offset_of_buildIndex_26(),
	GetSceneProperties_t396337852::get_offset_of_isValid_27(),
	GetSceneProperties_t396337852::get_offset_of_isLoaded_28(),
	GetSceneProperties_t396337852::get_offset_of_isDirty_29(),
	GetSceneProperties_t396337852::get_offset_of_rootCount_30(),
	GetSceneProperties_t396337852::get_offset_of_rootGameObjects_31(),
	GetSceneProperties_t396337852::get_offset_of_everyFrame_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (GetSceneRootCount_t2311826246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[2] = 
{
	GetSceneRootCount_t2311826246::get_offset_of_rootCount_24(),
	GetSceneRootCount_t2311826246::get_offset_of_everyFrame_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (GetSceneRootGameObjects_t1901668039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[2] = 
{
	GetSceneRootGameObjects_t1901668039::get_offset_of_rootGameObjects_24(),
	GetSceneRootGameObjects_t1901668039::get_offset_of_everyFrame_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (GetSceneUnloadedEventData_t4107210274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3581[10] = 
{
	GetSceneUnloadedEventData_t4107210274::get_offset_of_name_14(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_path_15(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_buildIndex_16(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_isValid_17(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_isLoaded_18(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_isDirty_19(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_rootCount_20(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_rootGameObjects_21(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of_everyFrame_22(),
	GetSceneUnloadedEventData_t4107210274::get_offset_of__scene_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (GetSceneActionBase_t2551360596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3582[10] = 
{
	GetSceneActionBase_t2551360596::get_offset_of_sceneReference_14(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneAtIndex_15(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneByName_16(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneByPath_17(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneByGameObject_18(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneFound_19(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneFoundEvent_20(),
	GetSceneActionBase_t2551360596::get_offset_of_sceneNotFoundEvent_21(),
	GetSceneActionBase_t2551360596::get_offset_of__scene_22(),
	GetSceneActionBase_t2551360596::get_offset_of__sceneFound_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (SceneReferenceOptions_t3051583257)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3583[4] = 
{
	SceneReferenceOptions_t3051583257::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (SceneSimpleReferenceOptions_t925546987)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3584[3] = 
{
	SceneSimpleReferenceOptions_t925546987::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (SceneBuildReferenceOptions_t2990954388)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3585[3] = 
{
	SceneBuildReferenceOptions_t2990954388::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (SceneAllReferenceOptions_t3801934101)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3586[6] = 
{
	SceneAllReferenceOptions_t3801934101::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (LoadScene_t2656597926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3587[7] = 
{
	LoadScene_t2656597926::get_offset_of_sceneReference_14(),
	LoadScene_t2656597926::get_offset_of_sceneByName_15(),
	LoadScene_t2656597926::get_offset_of_sceneAtIndex_16(),
	LoadScene_t2656597926::get_offset_of_loadSceneMode_17(),
	LoadScene_t2656597926::get_offset_of_success_18(),
	LoadScene_t2656597926::get_offset_of_successEvent_19(),
	LoadScene_t2656597926::get_offset_of_failureEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (LoadSceneAsynch_t4132231259), -1, sizeof(LoadSceneAsynch_t4132231259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3588[18] = 
{
	LoadSceneAsynch_t4132231259::get_offset_of_sceneReference_14(),
	LoadSceneAsynch_t4132231259::get_offset_of_sceneByName_15(),
	LoadSceneAsynch_t4132231259::get_offset_of_sceneAtIndex_16(),
	LoadSceneAsynch_t4132231259::get_offset_of_loadSceneMode_17(),
	LoadSceneAsynch_t4132231259::get_offset_of_allowSceneActivation_18(),
	LoadSceneAsynch_t4132231259::get_offset_of_operationPriority_19(),
	LoadSceneAsynch_t4132231259::get_offset_of_aSyncOperationHashCode_20(),
	LoadSceneAsynch_t4132231259::get_offset_of_progress_21(),
	LoadSceneAsynch_t4132231259::get_offset_of_isDone_22(),
	LoadSceneAsynch_t4132231259::get_offset_of_pendingActivation_23(),
	LoadSceneAsynch_t4132231259::get_offset_of_doneEvent_24(),
	LoadSceneAsynch_t4132231259::get_offset_of_pendingActivationEvent_25(),
	LoadSceneAsynch_t4132231259::get_offset_of_sceneNotFoundEvent_26(),
	LoadSceneAsynch_t4132231259::get_offset_of__asyncOperation_27(),
	LoadSceneAsynch_t4132231259::get_offset_of__asynchOperationUid_28(),
	LoadSceneAsynch_t4132231259::get_offset_of_pendingActivationCallBackDone_29(),
	LoadSceneAsynch_t4132231259_StaticFields::get_offset_of_aSyncOperationLUT_30(),
	LoadSceneAsynch_t4132231259_StaticFields::get_offset_of_aSynchUidCounter_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (MergeScenes_t2033590507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3589[17] = 
{
	MergeScenes_t2033590507::get_offset_of_sourceReference_14(),
	MergeScenes_t2033590507::get_offset_of_sourceAtIndex_15(),
	MergeScenes_t2033590507::get_offset_of_sourceByName_16(),
	MergeScenes_t2033590507::get_offset_of_sourceByPath_17(),
	MergeScenes_t2033590507::get_offset_of_sourceByGameObject_18(),
	MergeScenes_t2033590507::get_offset_of_destinationReference_19(),
	MergeScenes_t2033590507::get_offset_of_destinationAtIndex_20(),
	MergeScenes_t2033590507::get_offset_of_destinationByName_21(),
	MergeScenes_t2033590507::get_offset_of_destinationByPath_22(),
	MergeScenes_t2033590507::get_offset_of_destinationByGameObject_23(),
	MergeScenes_t2033590507::get_offset_of_success_24(),
	MergeScenes_t2033590507::get_offset_of_successEvent_25(),
	MergeScenes_t2033590507::get_offset_of_failureEvent_26(),
	MergeScenes_t2033590507::get_offset_of__sourceScene_27(),
	MergeScenes_t2033590507::get_offset_of__sourceFound_28(),
	MergeScenes_t2033590507::get_offset_of__destinationScene_29(),
	MergeScenes_t2033590507::get_offset_of__destinationFound_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (MoveGameObjectToScene_t1066798052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3590[6] = 
{
	MoveGameObjectToScene_t1066798052::get_offset_of_gameObject_24(),
	MoveGameObjectToScene_t1066798052::get_offset_of_findRootIfNecessary_25(),
	MoveGameObjectToScene_t1066798052::get_offset_of_success_26(),
	MoveGameObjectToScene_t1066798052::get_offset_of_successEvent_27(),
	MoveGameObjectToScene_t1066798052::get_offset_of_failureEvent_28(),
	MoveGameObjectToScene_t1066798052::get_offset_of__go_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (SendActiveSceneChangedEvent_t2439287554), -1, sizeof(SendActiveSceneChangedEvent_t2439287554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3591[3] = 
{
	SendActiveSceneChangedEvent_t2439287554::get_offset_of_activeSceneChanged_14(),
	SendActiveSceneChangedEvent_t2439287554_StaticFields::get_offset_of_lastPreviousActiveScene_15(),
	SendActiveSceneChangedEvent_t2439287554_StaticFields::get_offset_of_lastNewActiveScene_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (SendSceneLoadedEvent_t903181904), -1, sizeof(SendSceneLoadedEvent_t903181904_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3592[5] = 
{
	SendSceneLoadedEvent_t903181904::get_offset_of_sceneLoaded_14(),
	SendSceneLoadedEvent_t903181904::get_offset_of_sceneLoadedSafe_15(),
	SendSceneLoadedEvent_t903181904_StaticFields::get_offset_of_lastLoadedScene_16(),
	SendSceneLoadedEvent_t903181904_StaticFields::get_offset_of_lastLoadedMode_17(),
	SendSceneLoadedEvent_t903181904::get_offset_of__loaded_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (SendSceneUnloadedEvent_t352636565), -1, sizeof(SendSceneUnloadedEvent_t352636565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3593[2] = 
{
	SendSceneUnloadedEvent_t352636565::get_offset_of_sceneUnloaded_14(),
	SendSceneUnloadedEvent_t352636565_StaticFields::get_offset_of_lastUnLoadedScene_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (SetActiveScene_t3066865810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3594[14] = 
{
	SetActiveScene_t3066865810::get_offset_of_sceneReference_14(),
	SetActiveScene_t3066865810::get_offset_of_sceneByName_15(),
	SetActiveScene_t3066865810::get_offset_of_sceneAtBuildIndex_16(),
	SetActiveScene_t3066865810::get_offset_of_sceneAtIndex_17(),
	SetActiveScene_t3066865810::get_offset_of_sceneByPath_18(),
	SetActiveScene_t3066865810::get_offset_of_sceneByGameObject_19(),
	SetActiveScene_t3066865810::get_offset_of_success_20(),
	SetActiveScene_t3066865810::get_offset_of_successEvent_21(),
	SetActiveScene_t3066865810::get_offset_of_sceneFound_22(),
	SetActiveScene_t3066865810::get_offset_of_sceneNotActivatedEvent_23(),
	SetActiveScene_t3066865810::get_offset_of_sceneNotFoundEvent_24(),
	SetActiveScene_t3066865810::get_offset_of__scene_25(),
	SetActiveScene_t3066865810::get_offset_of__sceneFound_26(),
	SetActiveScene_t3066865810::get_offset_of__success_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (SceneReferenceOptions_t2424404743)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3595[6] = 
{
	SceneReferenceOptions_t2424404743::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (UnloadScene_t2244765154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3596[9] = 
{
	UnloadScene_t2244765154::get_offset_of_sceneReference_14(),
	UnloadScene_t2244765154::get_offset_of_sceneByName_15(),
	UnloadScene_t2244765154::get_offset_of_sceneAtBuildIndex_16(),
	UnloadScene_t2244765154::get_offset_of_sceneAtIndex_17(),
	UnloadScene_t2244765154::get_offset_of_sceneByPath_18(),
	UnloadScene_t2244765154::get_offset_of_sceneByGameObject_19(),
	UnloadScene_t2244765154::get_offset_of_unloaded_20(),
	UnloadScene_t2244765154::get_offset_of_unloadedEvent_21(),
	UnloadScene_t2244765154::get_offset_of_failureEvent_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (SceneReferenceOptions_t3425810120)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3597[7] = 
{
	SceneReferenceOptions_t3425810120::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (UnloadSceneAsynch_t792856094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[12] = 
{
	UnloadSceneAsynch_t792856094::get_offset_of_sceneReference_14(),
	UnloadSceneAsynch_t792856094::get_offset_of_sceneByName_15(),
	UnloadSceneAsynch_t792856094::get_offset_of_sceneAtBuildIndex_16(),
	UnloadSceneAsynch_t792856094::get_offset_of_sceneAtIndex_17(),
	UnloadSceneAsynch_t792856094::get_offset_of_sceneByPath_18(),
	UnloadSceneAsynch_t792856094::get_offset_of_sceneByGameObject_19(),
	UnloadSceneAsynch_t792856094::get_offset_of_operationPriority_20(),
	UnloadSceneAsynch_t792856094::get_offset_of_progress_21(),
	UnloadSceneAsynch_t792856094::get_offset_of_isDone_22(),
	UnloadSceneAsynch_t792856094::get_offset_of_doneEvent_23(),
	UnloadSceneAsynch_t792856094::get_offset_of_sceneNotFoundEvent_24(),
	UnloadSceneAsynch_t792856094::get_offset_of__asyncOperation_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (SceneReferenceOptions_t108674727)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3599[7] = 
{
	SceneReferenceOptions_t108674727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
