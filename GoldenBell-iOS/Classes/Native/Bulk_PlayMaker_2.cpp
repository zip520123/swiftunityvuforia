﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// FsmTemplate
struct FsmTemplate_t1663266674;
// HutongGames.PlayMaker.ActionData
struct ActionData_t1922786972;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t2002857066;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t1155011270;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t1111370293;
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t1010932050;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1692568573;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t919699796;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2511618479;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t719957643;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2904461592;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t3265252880;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t3870809373;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t635502296;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t495607758;
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t3969342952;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t3801304101;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1918078376;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t3458670015;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t2313143784;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1340699069;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t3577734832;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t3349589544;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t1097319912;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t402703848;
// HutongGames.PlayMaker.Fsm[]
struct FsmU5BU5D_t3243132433;
// HutongGames.PlayMaker.RequiredFieldAttribute
struct RequiredFieldAttribute_t3920128169;
// HutongGames.PlayMaker.SettingsMenuItemAttribute
struct SettingsMenuItemAttribute_t2376176270;
// HutongGames.PlayMaker.SettingsMenuItemStateAttribute
struct SettingsMenuItemStateAttribute_t3930598301;
// HutongGames.PlayMaker.TitleAttribute
struct TitleAttribute_t196394849;
// HutongGames.PlayMaker.TooltipAttribute
struct TooltipAttribute_t2411168496;
// HutongGames.PlayMaker.UIHintAttribute
struct UIHintAttribute_t2334957252;
// HutongGames.PlayMaker.VariableTypeAttribute
struct VariableTypeAttribute_t3556842571;
// HutongGames.PlayMaker.VariableTypeFilter
struct VariableTypeFilter_t3022214907;
// PlayMakerAnimatorIK
struct PlayMakerAnimatorIK_t3219858232;
// PlayMakerAnimatorMove
struct PlayMakerAnimatorMove_t426800548;
// PlayMakerApplicationEvents
struct PlayMakerApplicationEvents_t286866203;
// PlayMakerCollisionEnter
struct PlayMakerCollisionEnter_t3733719168;
// PlayMakerCollisionEnter2D
struct PlayMakerCollisionEnter2D_t3463350103;
// PlayMakerCollisionExit
struct PlayMakerCollisionExit_t3975926555;
// PlayMakerCollisionExit2D
struct PlayMakerCollisionExit2D_t2644894131;
// PlayMakerCollisionStay
struct PlayMakerCollisionStay_t2320041465;
// PlayMakerCollisionStay2D
struct PlayMakerCollisionStay2D_t1641352069;
// PlayMakerControllerColliderHit
struct PlayMakerControllerColliderHit_t3864538305;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// PlayMakerFSM/<DoCoroutine>d__43
struct U3CDoCoroutineU3Ed__43_t4100907003;
// PlayMakerFSM/AddEventHandlerDelegate
struct AddEventHandlerDelegate_t772649125;
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t560886798;
// PlayMakerFixedUpdate
struct PlayMakerFixedUpdate_t1242491047;
// PlayMakerGUI
struct PlayMakerGUI_t1571180761;
// PlayMakerGUI/<>c
struct U3CU3Ec_t2300308631;
// PlayMakerGlobals
struct PlayMakerGlobals_t1863708399;
// PlayMakerGlobals/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_t1424664817;
// PlayMakerJointBreak
struct PlayMakerJointBreak_t32499873;
// PlayMakerJointBreak2D
struct PlayMakerJointBreak2D_t628918649;
// PlayMakerLateUpdate
struct PlayMakerLateUpdate_t211890678;
// PlayMakerMouseEvents
struct PlayMakerMouseEvents_t4026808530;
// PlayMakerOnGUI
struct PlayMakerOnGUI_t3357975613;
// PlayMakerParticleCollision
struct PlayMakerParticleCollision_t716698883;
// PlayMakerPrefs
struct PlayMakerPrefs_t2834574540;
// PlayMakerProxyBase
struct PlayMakerProxyBase_t90512809;
// PlayMakerProxyBase/Collision2DEvent
struct Collision2DEvent_t1483235594;
// PlayMakerProxyBase/CollisionEvent
struct CollisionEvent_t2644514664;
// PlayMakerProxyBase/ControllerCollisionEvent
struct ControllerCollisionEvent_t2755027817;
// PlayMakerProxyBase/ParticleCollisionEvent
struct ParticleCollisionEvent_t3984792766;
// PlayMakerProxyBase/Trigger2DEvent
struct Trigger2DEvent_t2411364400;
// PlayMakerProxyBase/TriggerEvent
struct TriggerEvent_t1933226311;
// PlayMakerTriggerEnter
struct PlayMakerTriggerEnter_t2165260292;
// PlayMakerTriggerEnter2D
struct PlayMakerTriggerEnter2D_t1611673000;
// PlayMakerTriggerExit
struct PlayMakerTriggerExit_t2127564928;
// PlayMakerTriggerExit2D
struct PlayMakerTriggerExit2D_t2899497211;
// PlayMakerTriggerStay
struct PlayMakerTriggerStay_t1432841146;
// PlayMakerTriggerStay2D
struct PlayMakerTriggerStay2D_t2180132409;
// System.Action`1<HutongGames.PlayMaker.FsmState>
struct Action_1_t1898533;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Attribute
struct Attribute_t861562559;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>
struct Dictionary_2_t4026181381;
// System.Collections.Generic.Dictionary`2<System.String,HutongGames.PlayMaker.FsmEvent>
struct Dictionary_2_t3521556181;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3742157810;
// System.Collections.Generic.IEnumerable`1<PlayMakerFSM>
struct IEnumerable_1_t592863120;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent>
struct List_1_t1164733674;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct List_1_t1304255270;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>
struct List_1_t913407328;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction>
struct List_1_t978411547;
// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t3085084973;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Comparison`1<PlayMakerFSM>
struct Comparison_1_t1387941410;
// System.Comparison`1<System.Object>
struct Comparison_1_t2855037343;
// System.Delegate
struct Delegate_t1188392813;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.Predicate`1<System.String>
struct Predicate_1_t2672744813;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.RegularExpressions.Capture
struct Capture_t2232016050;
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t1760593541;
// System.Text.RegularExpressions.Group
struct Group_t2468205786;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_t69770484;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1880820351;
// System.Text.RegularExpressions.IMachine
struct IMachine_t2106687985;
// System.Text.RegularExpressions.Match
struct Match_t3408321083;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.Collision2D
struct Collision2D_t2842956331;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t96683501;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GUIContent
struct GUIContent_t3050628031;
// UnityEngine.GUISettings
struct GUISettings_t1774757634;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1143955295;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.GUITexture
struct GUITexture_t951903601;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Joint2D
struct Joint2D_t4180440564;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;

extern RuntimeClass* AddEventHandlerDelegate_t772649125_il2cpp_TypeInfo_var;
extern RuntimeClass* Collision2DEvent_t1483235594_il2cpp_TypeInfo_var;
extern RuntimeClass* CollisionEvent_t2644514664_il2cpp_TypeInfo_var;
extern RuntimeClass* ColorU5BU5D_t941916413_il2cpp_TypeInfo_var;
extern RuntimeClass* Comparison_1_t1387941410_il2cpp_TypeInfo_var;
extern RuntimeClass* ControllerCollisionEvent_t2755027817_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* FsmEvent_t3736299882_il2cpp_TypeInfo_var;
extern RuntimeClass* FsmExecutionStack_t2219150547_il2cpp_TypeInfo_var;
extern RuntimeClass* FsmLog_t3265252880_il2cpp_TypeInfo_var;
extern RuntimeClass* FsmVariables_t3349589544_il2cpp_TypeInfo_var;
extern RuntimeClass* Fsm_t4127147824_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIContent_t3050628031_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t3956901511_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3085084973_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* ParticleCollisionEvent_t3984792766_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var;
extern RuntimeClass* Predicate_1_t2672744813_il2cpp_TypeInfo_var;
extern RuntimeClass* RectOffset_t1369453676_il2cpp_TypeInfo_var;
extern RuntimeClass* ReflectionUtils_t2510826940_il2cpp_TypeInfo_var;
extern RuntimeClass* Regex_t3657309853_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* Trigger2DEvent_t2411364400_il2cpp_TypeInfo_var;
extern RuntimeClass* TriggerEvent_t1933226311_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CDoCoroutineU3Ed__43_t4100907003_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec__DisplayClass36_0_t1424664817_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec_t2300308631_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1580592898;
extern String_t* _stringLiteral1619056972;
extern String_t* _stringLiteral1829750146;
extern String_t* _stringLiteral1948333211;
extern String_t* _stringLiteral2289476910;
extern String_t* _stringLiteral259155937;
extern String_t* _stringLiteral3265678564;
extern String_t* _stringLiteral3451434880;
extern String_t* _stringLiteral3452614528;
extern String_t* _stringLiteral3452614620;
extern String_t* _stringLiteral3506861936;
extern String_t* _stringLiteral3552476482;
extern String_t* _stringLiteral4001789887;
extern String_t* _stringLiteral423668695;
extern String_t* _stringLiteral4876274;
extern String_t* _stringLiteral566356367;
extern String_t* _stringLiteral586731623;
extern String_t* _stringLiteral60608109;
extern String_t* _stringLiteral62725275;
extern String_t* _stringLiteral757602046;
extern String_t* _stringLiteral811194538;
extern String_t* _stringLiteral82471179;
extern String_t* _stringLiteral858884005;
extern String_t* _stringLiteral900290204;
extern const RuntimeMethod* Comparison_1__ctor_m899976308_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisGUIText_t402233326_m1400901891_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisGUITexture_t951903601_m2110957654_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisPlayMakerOnGUI_t3357975613_m1251119906_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1625029548_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4053319692_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m827365246_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisPlayMakerGUI_t1571180761_m3641687522_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisPlayMakerOnGUI_t3357975613_m2071952433_RuntimeMethod_var;
extern const RuntimeMethod* List_1_AddRange_m1618812805_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1270336517_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m246811976_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m2497552032_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m3607818559_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m417274155_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m1452861271_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m379049447_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Sort_m450531192_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m3961749393_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1624985937_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m216985474_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2406428141_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2358759014_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2787267307_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3488709207_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m587783994_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t3219858232_m1067537140_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t426800548_m490261559_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t286866203_m4204577506_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t3463350103_m1263814587_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t3733719168_m1361982853_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2644894131_m1687377651_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3975926555_m2854978248_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1641352069_m3660820280_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2320041465_m1461421557_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t3864538305_m317364569_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t1242491047_m4120879459_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t32499873_m24598600_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerLateUpdate_t211890678_m3673586923_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t4026808530_m2525016204_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t716698883_m13872882_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t1611673000_m1561107553_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2165260292_m1613341182_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t2899497211_m78230100_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2127564928_m2522439781_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t2180132409_m3915672404_RuntimeMethod_var;
extern const RuntimeMethod* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t1432841146_m3573483980_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m201911024_RuntimeMethod_var;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisPlayMakerGlobals_t1863708399_m1046660433_RuntimeMethod_var;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisPlayMakerPrefs_t2834574540_m1074476695_RuntimeMethod_var;
extern const RuntimeMethod* U3CDoCoroutineU3Ed__43_System_Collections_IEnumerator_Reset_m2796445315_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec_U3CDrawStateLabelsU3Eb__65_0_m66306034_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass36_0_U3CRemoveGlobalEventU3Eb__0_m2080862436_RuntimeMethod_var;
extern const RuntimeType* AddEventHandlerDelegate_t772649125_0_0_0_var;
extern const RuntimeType* PlayMakerGUI_t1571180761_0_0_0_var;
extern const RuntimeType* PlayMakerGlobals_t1863708399_0_0_0_var;
extern const uint32_t ColorUtils_Approximately_m401312687_MetadataUsageId;
extern const uint32_t PlayMakerAnimatorIK_OnAnimatorIK_m3481914875_MetadataUsageId;
extern const uint32_t PlayMakerAnimatorMove_OnAnimatorMove_m4158779815_MetadataUsageId;
extern const uint32_t PlayMakerApplicationEvents_OnApplicationFocus_m3055338962_MetadataUsageId;
extern const uint32_t PlayMakerApplicationEvents_OnApplicationPause_m789568602_MetadataUsageId;
extern const uint32_t PlayMakerCollisionEnter2D_OnCollisionEnter2D_m1570857150_MetadataUsageId;
extern const uint32_t PlayMakerCollisionEnter_OnCollisionEnter_m3832574366_MetadataUsageId;
extern const uint32_t PlayMakerCollisionExit2D_OnCollisionExit2D_m2715138103_MetadataUsageId;
extern const uint32_t PlayMakerCollisionExit_OnCollisionExit_m4074297343_MetadataUsageId;
extern const uint32_t PlayMakerCollisionStay2D_OnCollisionStay2D_m3386751790_MetadataUsageId;
extern const uint32_t PlayMakerCollisionStay_OnCollisionStay_m405027212_MetadataUsageId;
extern const uint32_t PlayMakerControllerColliderHit_OnControllerColliderHit_m48225694_MetadataUsageId;
extern const uint32_t PlayMakerFSM_AddEventHandlerComponents_m2049794372_MetadataUsageId;
extern const uint32_t PlayMakerFSM_Awake_m3471102124_MetadataUsageId;
extern const uint32_t PlayMakerFSM_BroadcastEvent_m1842675870_MetadataUsageId;
extern const uint32_t PlayMakerFSM_BroadcastEvent_m1927056291_MetadataUsageId;
extern const uint32_t PlayMakerFSM_DoCoroutine_m1084357490_MetadataUsageId;
extern const uint32_t PlayMakerFSM_FindFsmOnGameObject_m119364970_MetadataUsageId;
extern const uint32_t PlayMakerFSM_InitFsm_m3561656545_MetadataUsageId;
extern const uint32_t PlayMakerFSM_InitTemplate_m895909370_MetadataUsageId;
extern const uint32_t PlayMakerFSM_Init_m1246910327_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnAfterDeserialize_m3653043181_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnApplicationQuit_m3772709317_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnBecameInvisible_m3452402576_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnBecameVisible_m3709742732_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnDestroy_m4064784760_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnDisable_m2124961948_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnEnable_m246380954_MetadataUsageId;
extern const uint32_t PlayMakerFSM_Preprocess_m2246609862_MetadataUsageId;
extern const uint32_t PlayMakerFSM_Reset_m2803450298_MetadataUsageId;
extern const uint32_t PlayMakerFSM_SendRemoteFsmEventWithData_m42325452_MetadataUsageId;
extern const uint32_t PlayMakerFSM_SetFsmTemplate_m1859692523_MetadataUsageId;
extern const uint32_t PlayMakerFSM__cctor_m3719180614_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_ActiveStateName_m2221793783_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_AddEventHandlers_m2329939143_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_DrawGizmos_m1178323582_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_FsmList_m2691337861_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_GuiText_m1330723702_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_GuiTexture_m659311724_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_UsesTemplate_m701935324_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_VersionLabel_m3699917828_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_VersionNotes_m2298671410_MetadataUsageId;
extern const uint32_t PlayMakerFSM_set_DrawGizmos_m1771254959_MetadataUsageId;
extern const uint32_t PlayMakerFixedUpdate_FixedUpdate_m3896899329_MetadataUsageId;
extern const uint32_t PlayMakerGUI_Awake_m2966660766_MetadataUsageId;
extern const uint32_t PlayMakerGUI_DoEditGUI_m1724976334_MetadataUsageId;
extern const uint32_t PlayMakerGUI_DrawStateLabel_m1670404612_MetadataUsageId;
extern const uint32_t PlayMakerGUI_DrawStateLabels_m1713504128_MetadataUsageId;
extern const uint32_t PlayMakerGUI_GenerateStateLabel_m3421774616_MetadataUsageId;
extern const uint32_t PlayMakerGUI_InitInstance_m1546024309_MetadataUsageId;
extern const uint32_t PlayMakerGUI_InitLabelStyle_m44230268_MetadataUsageId;
extern const uint32_t PlayMakerGUI_OnApplicationQuit_m1022255247_MetadataUsageId;
extern const uint32_t PlayMakerGUI_OnDisable_m2705404189_MetadataUsageId;
extern const uint32_t PlayMakerGUI_OnGUI_m372789782_MetadataUsageId;
extern const uint32_t PlayMakerGUI__cctor_m3996719456_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_EnableStateLabelsInBuild_m1678793621_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_EnableStateLabels_m1411432592_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_Enabled_m1033451335_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIBackgroundColor_m3544528621_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIColor_m3847206507_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIContentColor_m2658339020_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIMatrix_m3062280553_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUISkin_m3406552685_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_HideCursor_m1592595061_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_Instance_m1759058258_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_LockCursor_m3827892467_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_MouseCursor_m3451464712_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_EnableStateLabelsInBuild_m2333001203_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_EnableStateLabels_m3044022820_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIBackgroundColor_m3467957232_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIColor_m4066615607_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIContentColor_m1416786292_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIMatrix_m1274160697_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUISkin_m1528967737_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_HideCursor_m1860311390_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_LockCursor_m2624587674_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_MouseCursor_m747393345_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_AddEvent_m2652243670_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_AddGlobalEvent_m3539697052_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_Initialize_m1152949201_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_OnDestroy_m609605670_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_RemoveGlobalEvent_m2206567853_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_ResetInstance_m750794251_MetadataUsageId;
extern const uint32_t PlayMakerGlobals__ctor_m723307631_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_Initialized_m4216351915_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_Instance_m782995651_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsBuilding_m790260703_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsEditor_m4098855694_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsPlayingInEditor_m1115484028_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsPlaying_m3613842894_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_Initialized_m3402930854_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsBuilding_m861315209_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsEditor_m3248841557_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsPlayingInEditor_m3532164442_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsPlaying_m82396395_MetadataUsageId;
extern const uint32_t PlayMakerJointBreak2D_OnJointBreak2D_m3011672567_MetadataUsageId;
extern const uint32_t PlayMakerJointBreak_OnJointBreak_m792863235_MetadataUsageId;
extern const uint32_t PlayMakerLateUpdate_LateUpdate_m333097745_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseDown_m436448441_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseDrag_m3560491499_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseEnter_m3395333956_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseExit_m1940162432_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseOver_m2287848480_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseUpAsButton_m3452186487_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseUp_m2433996139_MetadataUsageId;
extern const uint32_t PlayMakerOnGUI_DoEditGUI_m1319786893_MetadataUsageId;
extern const uint32_t PlayMakerOnGUI_OnGUI_m2523943160_MetadataUsageId;
extern const uint32_t PlayMakerOnGUI_Start_m2796628171_MetadataUsageId;
extern const uint32_t PlayMakerParticleCollision_OnParticleCollision_m1117432973_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_ResetDefaultColors_m1044259044_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_SaveChanges_m571565725_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_UpdateMinimapColors_m3596199612_MetadataUsageId;
extern const uint32_t PlayMakerPrefs__cctor_m3014713217_MetadataUsageId;
extern const uint32_t PlayMakerPrefs__ctor_m3564739458_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_ColorNames_m556735336_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_Colors_m3509984984_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_Instance_m4280752655_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_LogPerformanceWarnings_m2853607083_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_MinimapColors_m3269389195_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_ShowEventHandlerComponents_m947219729_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_TweenFromColor_m2505016700_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_TweenToColor_m2388405964_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_ColorNames_m1678819117_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_Colors_m891229754_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_LogPerformanceWarnings_m3158938153_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_ShowEventHandlerComponents_m2653779661_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_TweenFromColor_m1247798575_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_TweenToColor_m2761377687_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_AddTarget_m3229991323_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase__ctor_m2965400855_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_add_Collision2DEventCallback_m1748445263_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_add_CollisionEventCallback_m555388545_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_add_ControllerCollisionEventCallback_m399454418_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_add_ParticleCollisionEventCallback_m371344463_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_add_Trigger2DEventCallback_m3684239397_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_add_TriggerEventCallback_m3172485254_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_get_playMakerFSMs_m3489375989_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_remove_Collision2DEventCallback_m727498677_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_remove_CollisionEventCallback_m3921268190_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_remove_ControllerCollisionEventCallback_m2917022917_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_remove_ParticleCollisionEventCallback_m2118262247_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_remove_Trigger2DEventCallback_m3473419264_MetadataUsageId;
extern const uint32_t PlayMakerProxyBase_remove_TriggerEventCallback_m1155547507_MetadataUsageId;
extern const uint32_t PlayMakerTriggerEnter2D_OnTriggerEnter2D_m1049597878_MetadataUsageId;
extern const uint32_t PlayMakerTriggerEnter_OnTriggerEnter_m1486868521_MetadataUsageId;
extern const uint32_t PlayMakerTriggerExit2D_OnTriggerExit2D_m3700787535_MetadataUsageId;
extern const uint32_t PlayMakerTriggerExit_OnTriggerExit_m602018866_MetadataUsageId;
extern const uint32_t PlayMakerTriggerStay2D_OnTriggerStay2D_m4238132012_MetadataUsageId;
extern const uint32_t PlayMakerTriggerStay_OnTriggerStay_m3827530242_MetadataUsageId;
extern const uint32_t StringUtils_IncrementStringCounter_m2169459698_MetadataUsageId;
extern const uint32_t U3CDoCoroutineU3Ed__43_MoveNext_m2448439397_MetadataUsageId;
extern const uint32_t U3CDoCoroutineU3Ed__43_System_Collections_IEnumerator_Reset_m2796445315_MetadataUsageId;
extern const uint32_t U3CU3Ec_U3CDrawStateLabelsU3Eb__65_0_m66306034_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass36_0_U3CRemoveGlobalEventU3Eb__0_m2080862436_MetadataUsageId;
extern const uint32_t U3CU3Ec__cctor_m1135028354_MetadataUsageId;
struct ContactPoint2D_t3390240644 ;
struct ContactPoint_t3758755253 ;
struct GUIStyleState_t1397964415_marshaled_com;
struct GUIStyleState_t1397964415_marshaled_pinvoke;
struct GUIStyle_t3956901511_marshaled_com;
struct GUIStyle_t3956901511_marshaled_pinvoke;
struct RectOffset_t1369453676_marshaled_com;

struct FsmEventU5BU5D_t2511618479;
struct FsmStateActionU5BU5D_t1918078376;
struct FsmStateU5BU5D_t3458670015;
struct FsmTransitionU5BU5D_t3577734832;
struct PlayMakerFSMU5BU5D_t560886798;
struct StringU5BU5D_t1281789340;
struct ColorU5BU5D_t941916413;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FSMEVENT_T3736299882_H
#define FSMEVENT_T3736299882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEvent
struct  FsmEvent_t3736299882  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmEvent::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmEvent::isSystemEvent
	bool ___isSystemEvent_3;
	// System.Boolean HutongGames.PlayMaker.FsmEvent::isGlobal
	bool ___isGlobal_4;
	// System.String HutongGames.PlayMaker.FsmEvent::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_isSystemEvent_3() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___isSystemEvent_3)); }
	inline bool get_isSystemEvent_3() const { return ___isSystemEvent_3; }
	inline bool* get_address_of_isSystemEvent_3() { return &___isSystemEvent_3; }
	inline void set_isSystemEvent_3(bool value)
	{
		___isSystemEvent_3 = value;
	}

	inline static int32_t get_offset_of_isGlobal_4() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___isGlobal_4)); }
	inline bool get_isGlobal_4() const { return ___isGlobal_4; }
	inline bool* get_address_of_isGlobal_4() { return &___isGlobal_4; }
	inline void set_isGlobal_4(bool value)
	{
		___isGlobal_4 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___U3CPathU3Ek__BackingField_5)); }
	inline String_t* get_U3CPathU3Ek__BackingField_5() const { return ___U3CPathU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_5() { return &___U3CPathU3Ek__BackingField_5; }
	inline void set_U3CPathU3Ek__BackingField_5(String_t* value)
	{
		___U3CPathU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_5), value);
	}
};

struct FsmEvent_t3736299882_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.FsmEvent::_eventLookup
	Dictionary_2_t3521556181 * ____eventLookup_0;
	// System.Object HutongGames.PlayMaker.FsmEvent::syncObj
	RuntimeObject * ___syncObj_1;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<BecameInvisible>k__BackingField
	FsmEvent_t3736299882 * ___U3CBecameInvisibleU3Ek__BackingField_6;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<BecameVisible>k__BackingField
	FsmEvent_t3736299882 * ___U3CBecameVisibleU3Ek__BackingField_7;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionEnterU3Ek__BackingField_8;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionExitU3Ek__BackingField_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionStay>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionStayU3Ek__BackingField_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionEnter2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionEnter2DU3Ek__BackingField_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionExit2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionExit2DU3Ek__BackingField_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionStay2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionStay2DU3Ek__BackingField_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ControllerColliderHit>k__BackingField
	FsmEvent_t3736299882 * ___U3CControllerColliderHitU3Ek__BackingField_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<Finished>k__BackingField
	FsmEvent_t3736299882 * ___U3CFinishedU3Ek__BackingField_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<LevelLoaded>k__BackingField
	FsmEvent_t3736299882 * ___U3CLevelLoadedU3Ek__BackingField_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseDown>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseDownU3Ek__BackingField_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseDragU3Ek__BackingField_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseEnterU3Ek__BackingField_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseExitU3Ek__BackingField_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseOver>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseOverU3Ek__BackingField_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseUp>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseUpU3Ek__BackingField_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseUpAsButton>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseUpAsButtonU3Ek__BackingField_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerEnterU3Ek__BackingField_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerExitU3Ek__BackingField_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerStay>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerStayU3Ek__BackingField_26;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerEnter2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerEnter2DU3Ek__BackingField_27;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerExit2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerExit2DU3Ek__BackingField_28;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerStay2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerStay2DU3Ek__BackingField_29;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ApplicationFocus>k__BackingField
	FsmEvent_t3736299882 * ___U3CApplicationFocusU3Ek__BackingField_30;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ApplicationPause>k__BackingField
	FsmEvent_t3736299882 * ___U3CApplicationPauseU3Ek__BackingField_31;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ApplicationQuit>k__BackingField
	FsmEvent_t3736299882 * ___U3CApplicationQuitU3Ek__BackingField_32;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ParticleCollision>k__BackingField
	FsmEvent_t3736299882 * ___U3CParticleCollisionU3Ek__BackingField_33;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<JointBreak>k__BackingField
	FsmEvent_t3736299882 * ___U3CJointBreakU3Ek__BackingField_34;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<JointBreak2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CJointBreak2DU3Ek__BackingField_35;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<Disable>k__BackingField
	FsmEvent_t3736299882 * ___U3CDisableU3Ek__BackingField_36;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<PlayerConnected>k__BackingField
	FsmEvent_t3736299882 * ___U3CPlayerConnectedU3Ek__BackingField_37;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ServerInitialized>k__BackingField
	FsmEvent_t3736299882 * ___U3CServerInitializedU3Ek__BackingField_38;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ConnectedToServer>k__BackingField
	FsmEvent_t3736299882 * ___U3CConnectedToServerU3Ek__BackingField_39;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<PlayerDisconnected>k__BackingField
	FsmEvent_t3736299882 * ___U3CPlayerDisconnectedU3Ek__BackingField_40;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<DisconnectedFromServer>k__BackingField
	FsmEvent_t3736299882 * ___U3CDisconnectedFromServerU3Ek__BackingField_41;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<FailedToConnect>k__BackingField
	FsmEvent_t3736299882 * ___U3CFailedToConnectU3Ek__BackingField_42;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<FailedToConnectToMasterServer>k__BackingField
	FsmEvent_t3736299882 * ___U3CFailedToConnectToMasterServerU3Ek__BackingField_43;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MasterServerEvent>k__BackingField
	FsmEvent_t3736299882 * ___U3CMasterServerEventU3Ek__BackingField_44;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<NetworkInstantiate>k__BackingField
	FsmEvent_t3736299882 * ___U3CNetworkInstantiateU3Ek__BackingField_45;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiBeginDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiBeginDragU3Ek__BackingField_46;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiDragU3Ek__BackingField_47;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiEndDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiEndDragU3Ek__BackingField_48;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiClick>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiClickU3Ek__BackingField_49;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiDrop>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiDropU3Ek__BackingField_50;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerClick>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerClickU3Ek__BackingField_51;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerDown>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerDownU3Ek__BackingField_52;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerEnterU3Ek__BackingField_53;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerExitU3Ek__BackingField_54;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerUp>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerUpU3Ek__BackingField_55;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiBoolValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiBoolValueChangedU3Ek__BackingField_56;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiFloatValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiFloatValueChangedU3Ek__BackingField_57;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiIntValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiIntValueChangedU3Ek__BackingField_58;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiVector2ValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiVector2ValueChangedU3Ek__BackingField_59;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiEndEdit>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiEndEditU3Ek__BackingField_60;

public:
	inline static int32_t get_offset_of__eventLookup_0() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ____eventLookup_0)); }
	inline Dictionary_2_t3521556181 * get__eventLookup_0() const { return ____eventLookup_0; }
	inline Dictionary_2_t3521556181 ** get_address_of__eventLookup_0() { return &____eventLookup_0; }
	inline void set__eventLookup_0(Dictionary_2_t3521556181 * value)
	{
		____eventLookup_0 = value;
		Il2CppCodeGenWriteBarrier((&____eventLookup_0), value);
	}

	inline static int32_t get_offset_of_syncObj_1() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___syncObj_1)); }
	inline RuntimeObject * get_syncObj_1() const { return ___syncObj_1; }
	inline RuntimeObject ** get_address_of_syncObj_1() { return &___syncObj_1; }
	inline void set_syncObj_1(RuntimeObject * value)
	{
		___syncObj_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncObj_1), value);
	}

	inline static int32_t get_offset_of_U3CBecameInvisibleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CBecameInvisibleU3Ek__BackingField_6)); }
	inline FsmEvent_t3736299882 * get_U3CBecameInvisibleU3Ek__BackingField_6() const { return ___U3CBecameInvisibleU3Ek__BackingField_6; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CBecameInvisibleU3Ek__BackingField_6() { return &___U3CBecameInvisibleU3Ek__BackingField_6; }
	inline void set_U3CBecameInvisibleU3Ek__BackingField_6(FsmEvent_t3736299882 * value)
	{
		___U3CBecameInvisibleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBecameInvisibleU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CBecameVisibleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CBecameVisibleU3Ek__BackingField_7)); }
	inline FsmEvent_t3736299882 * get_U3CBecameVisibleU3Ek__BackingField_7() const { return ___U3CBecameVisibleU3Ek__BackingField_7; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CBecameVisibleU3Ek__BackingField_7() { return &___U3CBecameVisibleU3Ek__BackingField_7; }
	inline void set_U3CBecameVisibleU3Ek__BackingField_7(FsmEvent_t3736299882 * value)
	{
		___U3CBecameVisibleU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBecameVisibleU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CCollisionEnterU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionEnterU3Ek__BackingField_8)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionEnterU3Ek__BackingField_8() const { return ___U3CCollisionEnterU3Ek__BackingField_8; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionEnterU3Ek__BackingField_8() { return &___U3CCollisionEnterU3Ek__BackingField_8; }
	inline void set_U3CCollisionEnterU3Ek__BackingField_8(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionEnterU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionEnterU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CCollisionExitU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionExitU3Ek__BackingField_9)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionExitU3Ek__BackingField_9() const { return ___U3CCollisionExitU3Ek__BackingField_9; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionExitU3Ek__BackingField_9() { return &___U3CCollisionExitU3Ek__BackingField_9; }
	inline void set_U3CCollisionExitU3Ek__BackingField_9(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionExitU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionExitU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCollisionStayU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionStayU3Ek__BackingField_10)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionStayU3Ek__BackingField_10() const { return ___U3CCollisionStayU3Ek__BackingField_10; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionStayU3Ek__BackingField_10() { return &___U3CCollisionStayU3Ek__BackingField_10; }
	inline void set_U3CCollisionStayU3Ek__BackingField_10(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionStayU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionStayU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CCollisionEnter2DU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionEnter2DU3Ek__BackingField_11)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionEnter2DU3Ek__BackingField_11() const { return ___U3CCollisionEnter2DU3Ek__BackingField_11; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionEnter2DU3Ek__BackingField_11() { return &___U3CCollisionEnter2DU3Ek__BackingField_11; }
	inline void set_U3CCollisionEnter2DU3Ek__BackingField_11(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionEnter2DU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionEnter2DU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollisionExit2DU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionExit2DU3Ek__BackingField_12)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionExit2DU3Ek__BackingField_12() const { return ___U3CCollisionExit2DU3Ek__BackingField_12; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionExit2DU3Ek__BackingField_12() { return &___U3CCollisionExit2DU3Ek__BackingField_12; }
	inline void set_U3CCollisionExit2DU3Ek__BackingField_12(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionExit2DU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionExit2DU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollisionStay2DU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionStay2DU3Ek__BackingField_13)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionStay2DU3Ek__BackingField_13() const { return ___U3CCollisionStay2DU3Ek__BackingField_13; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionStay2DU3Ek__BackingField_13() { return &___U3CCollisionStay2DU3Ek__BackingField_13; }
	inline void set_U3CCollisionStay2DU3Ek__BackingField_13(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionStay2DU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionStay2DU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CControllerColliderHitU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CControllerColliderHitU3Ek__BackingField_14)); }
	inline FsmEvent_t3736299882 * get_U3CControllerColliderHitU3Ek__BackingField_14() const { return ___U3CControllerColliderHitU3Ek__BackingField_14; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CControllerColliderHitU3Ek__BackingField_14() { return &___U3CControllerColliderHitU3Ek__BackingField_14; }
	inline void set_U3CControllerColliderHitU3Ek__BackingField_14(FsmEvent_t3736299882 * value)
	{
		___U3CControllerColliderHitU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerColliderHitU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CFinishedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CFinishedU3Ek__BackingField_15)); }
	inline FsmEvent_t3736299882 * get_U3CFinishedU3Ek__BackingField_15() const { return ___U3CFinishedU3Ek__BackingField_15; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CFinishedU3Ek__BackingField_15() { return &___U3CFinishedU3Ek__BackingField_15; }
	inline void set_U3CFinishedU3Ek__BackingField_15(FsmEvent_t3736299882 * value)
	{
		___U3CFinishedU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinishedU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CLevelLoadedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CLevelLoadedU3Ek__BackingField_16)); }
	inline FsmEvent_t3736299882 * get_U3CLevelLoadedU3Ek__BackingField_16() const { return ___U3CLevelLoadedU3Ek__BackingField_16; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CLevelLoadedU3Ek__BackingField_16() { return &___U3CLevelLoadedU3Ek__BackingField_16; }
	inline void set_U3CLevelLoadedU3Ek__BackingField_16(FsmEvent_t3736299882 * value)
	{
		___U3CLevelLoadedU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLevelLoadedU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CMouseDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseDownU3Ek__BackingField_17)); }
	inline FsmEvent_t3736299882 * get_U3CMouseDownU3Ek__BackingField_17() const { return ___U3CMouseDownU3Ek__BackingField_17; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseDownU3Ek__BackingField_17() { return &___U3CMouseDownU3Ek__BackingField_17; }
	inline void set_U3CMouseDownU3Ek__BackingField_17(FsmEvent_t3736299882 * value)
	{
		___U3CMouseDownU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseDownU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CMouseDragU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseDragU3Ek__BackingField_18)); }
	inline FsmEvent_t3736299882 * get_U3CMouseDragU3Ek__BackingField_18() const { return ___U3CMouseDragU3Ek__BackingField_18; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseDragU3Ek__BackingField_18() { return &___U3CMouseDragU3Ek__BackingField_18; }
	inline void set_U3CMouseDragU3Ek__BackingField_18(FsmEvent_t3736299882 * value)
	{
		___U3CMouseDragU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseDragU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CMouseEnterU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseEnterU3Ek__BackingField_19)); }
	inline FsmEvent_t3736299882 * get_U3CMouseEnterU3Ek__BackingField_19() const { return ___U3CMouseEnterU3Ek__BackingField_19; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseEnterU3Ek__BackingField_19() { return &___U3CMouseEnterU3Ek__BackingField_19; }
	inline void set_U3CMouseEnterU3Ek__BackingField_19(FsmEvent_t3736299882 * value)
	{
		___U3CMouseEnterU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseEnterU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CMouseExitU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseExitU3Ek__BackingField_20)); }
	inline FsmEvent_t3736299882 * get_U3CMouseExitU3Ek__BackingField_20() const { return ___U3CMouseExitU3Ek__BackingField_20; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseExitU3Ek__BackingField_20() { return &___U3CMouseExitU3Ek__BackingField_20; }
	inline void set_U3CMouseExitU3Ek__BackingField_20(FsmEvent_t3736299882 * value)
	{
		___U3CMouseExitU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseExitU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CMouseOverU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseOverU3Ek__BackingField_21)); }
	inline FsmEvent_t3736299882 * get_U3CMouseOverU3Ek__BackingField_21() const { return ___U3CMouseOverU3Ek__BackingField_21; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseOverU3Ek__BackingField_21() { return &___U3CMouseOverU3Ek__BackingField_21; }
	inline void set_U3CMouseOverU3Ek__BackingField_21(FsmEvent_t3736299882 * value)
	{
		___U3CMouseOverU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseOverU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CMouseUpU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseUpU3Ek__BackingField_22)); }
	inline FsmEvent_t3736299882 * get_U3CMouseUpU3Ek__BackingField_22() const { return ___U3CMouseUpU3Ek__BackingField_22; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseUpU3Ek__BackingField_22() { return &___U3CMouseUpU3Ek__BackingField_22; }
	inline void set_U3CMouseUpU3Ek__BackingField_22(FsmEvent_t3736299882 * value)
	{
		___U3CMouseUpU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseUpU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CMouseUpAsButtonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseUpAsButtonU3Ek__BackingField_23)); }
	inline FsmEvent_t3736299882 * get_U3CMouseUpAsButtonU3Ek__BackingField_23() const { return ___U3CMouseUpAsButtonU3Ek__BackingField_23; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseUpAsButtonU3Ek__BackingField_23() { return &___U3CMouseUpAsButtonU3Ek__BackingField_23; }
	inline void set_U3CMouseUpAsButtonU3Ek__BackingField_23(FsmEvent_t3736299882 * value)
	{
		___U3CMouseUpAsButtonU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseUpAsButtonU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CTriggerEnterU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerEnterU3Ek__BackingField_24)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerEnterU3Ek__BackingField_24() const { return ___U3CTriggerEnterU3Ek__BackingField_24; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerEnterU3Ek__BackingField_24() { return &___U3CTriggerEnterU3Ek__BackingField_24; }
	inline void set_U3CTriggerEnterU3Ek__BackingField_24(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerEnterU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerEnterU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CTriggerExitU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerExitU3Ek__BackingField_25)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerExitU3Ek__BackingField_25() const { return ___U3CTriggerExitU3Ek__BackingField_25; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerExitU3Ek__BackingField_25() { return &___U3CTriggerExitU3Ek__BackingField_25; }
	inline void set_U3CTriggerExitU3Ek__BackingField_25(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerExitU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerExitU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CTriggerStayU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerStayU3Ek__BackingField_26)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerStayU3Ek__BackingField_26() const { return ___U3CTriggerStayU3Ek__BackingField_26; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerStayU3Ek__BackingField_26() { return &___U3CTriggerStayU3Ek__BackingField_26; }
	inline void set_U3CTriggerStayU3Ek__BackingField_26(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerStayU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerStayU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CTriggerEnter2DU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerEnter2DU3Ek__BackingField_27)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerEnter2DU3Ek__BackingField_27() const { return ___U3CTriggerEnter2DU3Ek__BackingField_27; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerEnter2DU3Ek__BackingField_27() { return &___U3CTriggerEnter2DU3Ek__BackingField_27; }
	inline void set_U3CTriggerEnter2DU3Ek__BackingField_27(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerEnter2DU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerEnter2DU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CTriggerExit2DU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerExit2DU3Ek__BackingField_28)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerExit2DU3Ek__BackingField_28() const { return ___U3CTriggerExit2DU3Ek__BackingField_28; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerExit2DU3Ek__BackingField_28() { return &___U3CTriggerExit2DU3Ek__BackingField_28; }
	inline void set_U3CTriggerExit2DU3Ek__BackingField_28(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerExit2DU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerExit2DU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CTriggerStay2DU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerStay2DU3Ek__BackingField_29)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerStay2DU3Ek__BackingField_29() const { return ___U3CTriggerStay2DU3Ek__BackingField_29; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerStay2DU3Ek__BackingField_29() { return &___U3CTriggerStay2DU3Ek__BackingField_29; }
	inline void set_U3CTriggerStay2DU3Ek__BackingField_29(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerStay2DU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerStay2DU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CApplicationFocusU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CApplicationFocusU3Ek__BackingField_30)); }
	inline FsmEvent_t3736299882 * get_U3CApplicationFocusU3Ek__BackingField_30() const { return ___U3CApplicationFocusU3Ek__BackingField_30; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CApplicationFocusU3Ek__BackingField_30() { return &___U3CApplicationFocusU3Ek__BackingField_30; }
	inline void set_U3CApplicationFocusU3Ek__BackingField_30(FsmEvent_t3736299882 * value)
	{
		___U3CApplicationFocusU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationFocusU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CApplicationPauseU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CApplicationPauseU3Ek__BackingField_31)); }
	inline FsmEvent_t3736299882 * get_U3CApplicationPauseU3Ek__BackingField_31() const { return ___U3CApplicationPauseU3Ek__BackingField_31; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CApplicationPauseU3Ek__BackingField_31() { return &___U3CApplicationPauseU3Ek__BackingField_31; }
	inline void set_U3CApplicationPauseU3Ek__BackingField_31(FsmEvent_t3736299882 * value)
	{
		___U3CApplicationPauseU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationPauseU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CApplicationQuitU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CApplicationQuitU3Ek__BackingField_32)); }
	inline FsmEvent_t3736299882 * get_U3CApplicationQuitU3Ek__BackingField_32() const { return ___U3CApplicationQuitU3Ek__BackingField_32; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CApplicationQuitU3Ek__BackingField_32() { return &___U3CApplicationQuitU3Ek__BackingField_32; }
	inline void set_U3CApplicationQuitU3Ek__BackingField_32(FsmEvent_t3736299882 * value)
	{
		___U3CApplicationQuitU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationQuitU3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_U3CParticleCollisionU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CParticleCollisionU3Ek__BackingField_33)); }
	inline FsmEvent_t3736299882 * get_U3CParticleCollisionU3Ek__BackingField_33() const { return ___U3CParticleCollisionU3Ek__BackingField_33; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CParticleCollisionU3Ek__BackingField_33() { return &___U3CParticleCollisionU3Ek__BackingField_33; }
	inline void set_U3CParticleCollisionU3Ek__BackingField_33(FsmEvent_t3736299882 * value)
	{
		___U3CParticleCollisionU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParticleCollisionU3Ek__BackingField_33), value);
	}

	inline static int32_t get_offset_of_U3CJointBreakU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CJointBreakU3Ek__BackingField_34)); }
	inline FsmEvent_t3736299882 * get_U3CJointBreakU3Ek__BackingField_34() const { return ___U3CJointBreakU3Ek__BackingField_34; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CJointBreakU3Ek__BackingField_34() { return &___U3CJointBreakU3Ek__BackingField_34; }
	inline void set_U3CJointBreakU3Ek__BackingField_34(FsmEvent_t3736299882 * value)
	{
		___U3CJointBreakU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJointBreakU3Ek__BackingField_34), value);
	}

	inline static int32_t get_offset_of_U3CJointBreak2DU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CJointBreak2DU3Ek__BackingField_35)); }
	inline FsmEvent_t3736299882 * get_U3CJointBreak2DU3Ek__BackingField_35() const { return ___U3CJointBreak2DU3Ek__BackingField_35; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CJointBreak2DU3Ek__BackingField_35() { return &___U3CJointBreak2DU3Ek__BackingField_35; }
	inline void set_U3CJointBreak2DU3Ek__BackingField_35(FsmEvent_t3736299882 * value)
	{
		___U3CJointBreak2DU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJointBreak2DU3Ek__BackingField_35), value);
	}

	inline static int32_t get_offset_of_U3CDisableU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CDisableU3Ek__BackingField_36)); }
	inline FsmEvent_t3736299882 * get_U3CDisableU3Ek__BackingField_36() const { return ___U3CDisableU3Ek__BackingField_36; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CDisableU3Ek__BackingField_36() { return &___U3CDisableU3Ek__BackingField_36; }
	inline void set_U3CDisableU3Ek__BackingField_36(FsmEvent_t3736299882 * value)
	{
		___U3CDisableU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisableU3Ek__BackingField_36), value);
	}

	inline static int32_t get_offset_of_U3CPlayerConnectedU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CPlayerConnectedU3Ek__BackingField_37)); }
	inline FsmEvent_t3736299882 * get_U3CPlayerConnectedU3Ek__BackingField_37() const { return ___U3CPlayerConnectedU3Ek__BackingField_37; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CPlayerConnectedU3Ek__BackingField_37() { return &___U3CPlayerConnectedU3Ek__BackingField_37; }
	inline void set_U3CPlayerConnectedU3Ek__BackingField_37(FsmEvent_t3736299882 * value)
	{
		___U3CPlayerConnectedU3Ek__BackingField_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerConnectedU3Ek__BackingField_37), value);
	}

	inline static int32_t get_offset_of_U3CServerInitializedU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CServerInitializedU3Ek__BackingField_38)); }
	inline FsmEvent_t3736299882 * get_U3CServerInitializedU3Ek__BackingField_38() const { return ___U3CServerInitializedU3Ek__BackingField_38; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CServerInitializedU3Ek__BackingField_38() { return &___U3CServerInitializedU3Ek__BackingField_38; }
	inline void set_U3CServerInitializedU3Ek__BackingField_38(FsmEvent_t3736299882 * value)
	{
		___U3CServerInitializedU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerInitializedU3Ek__BackingField_38), value);
	}

	inline static int32_t get_offset_of_U3CConnectedToServerU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CConnectedToServerU3Ek__BackingField_39)); }
	inline FsmEvent_t3736299882 * get_U3CConnectedToServerU3Ek__BackingField_39() const { return ___U3CConnectedToServerU3Ek__BackingField_39; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CConnectedToServerU3Ek__BackingField_39() { return &___U3CConnectedToServerU3Ek__BackingField_39; }
	inline void set_U3CConnectedToServerU3Ek__BackingField_39(FsmEvent_t3736299882 * value)
	{
		___U3CConnectedToServerU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectedToServerU3Ek__BackingField_39), value);
	}

	inline static int32_t get_offset_of_U3CPlayerDisconnectedU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CPlayerDisconnectedU3Ek__BackingField_40)); }
	inline FsmEvent_t3736299882 * get_U3CPlayerDisconnectedU3Ek__BackingField_40() const { return ___U3CPlayerDisconnectedU3Ek__BackingField_40; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CPlayerDisconnectedU3Ek__BackingField_40() { return &___U3CPlayerDisconnectedU3Ek__BackingField_40; }
	inline void set_U3CPlayerDisconnectedU3Ek__BackingField_40(FsmEvent_t3736299882 * value)
	{
		___U3CPlayerDisconnectedU3Ek__BackingField_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerDisconnectedU3Ek__BackingField_40), value);
	}

	inline static int32_t get_offset_of_U3CDisconnectedFromServerU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CDisconnectedFromServerU3Ek__BackingField_41)); }
	inline FsmEvent_t3736299882 * get_U3CDisconnectedFromServerU3Ek__BackingField_41() const { return ___U3CDisconnectedFromServerU3Ek__BackingField_41; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CDisconnectedFromServerU3Ek__BackingField_41() { return &___U3CDisconnectedFromServerU3Ek__BackingField_41; }
	inline void set_U3CDisconnectedFromServerU3Ek__BackingField_41(FsmEvent_t3736299882 * value)
	{
		___U3CDisconnectedFromServerU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisconnectedFromServerU3Ek__BackingField_41), value);
	}

	inline static int32_t get_offset_of_U3CFailedToConnectU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CFailedToConnectU3Ek__BackingField_42)); }
	inline FsmEvent_t3736299882 * get_U3CFailedToConnectU3Ek__BackingField_42() const { return ___U3CFailedToConnectU3Ek__BackingField_42; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CFailedToConnectU3Ek__BackingField_42() { return &___U3CFailedToConnectU3Ek__BackingField_42; }
	inline void set_U3CFailedToConnectU3Ek__BackingField_42(FsmEvent_t3736299882 * value)
	{
		___U3CFailedToConnectU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFailedToConnectU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CFailedToConnectToMasterServerU3Ek__BackingField_43)); }
	inline FsmEvent_t3736299882 * get_U3CFailedToConnectToMasterServerU3Ek__BackingField_43() const { return ___U3CFailedToConnectToMasterServerU3Ek__BackingField_43; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_43() { return &___U3CFailedToConnectToMasterServerU3Ek__BackingField_43; }
	inline void set_U3CFailedToConnectToMasterServerU3Ek__BackingField_43(FsmEvent_t3736299882 * value)
	{
		___U3CFailedToConnectToMasterServerU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFailedToConnectToMasterServerU3Ek__BackingField_43), value);
	}

	inline static int32_t get_offset_of_U3CMasterServerEventU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMasterServerEventU3Ek__BackingField_44)); }
	inline FsmEvent_t3736299882 * get_U3CMasterServerEventU3Ek__BackingField_44() const { return ___U3CMasterServerEventU3Ek__BackingField_44; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMasterServerEventU3Ek__BackingField_44() { return &___U3CMasterServerEventU3Ek__BackingField_44; }
	inline void set_U3CMasterServerEventU3Ek__BackingField_44(FsmEvent_t3736299882 * value)
	{
		___U3CMasterServerEventU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasterServerEventU3Ek__BackingField_44), value);
	}

	inline static int32_t get_offset_of_U3CNetworkInstantiateU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CNetworkInstantiateU3Ek__BackingField_45)); }
	inline FsmEvent_t3736299882 * get_U3CNetworkInstantiateU3Ek__BackingField_45() const { return ___U3CNetworkInstantiateU3Ek__BackingField_45; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CNetworkInstantiateU3Ek__BackingField_45() { return &___U3CNetworkInstantiateU3Ek__BackingField_45; }
	inline void set_U3CNetworkInstantiateU3Ek__BackingField_45(FsmEvent_t3736299882 * value)
	{
		___U3CNetworkInstantiateU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNetworkInstantiateU3Ek__BackingField_45), value);
	}

	inline static int32_t get_offset_of_U3CUiBeginDragU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiBeginDragU3Ek__BackingField_46)); }
	inline FsmEvent_t3736299882 * get_U3CUiBeginDragU3Ek__BackingField_46() const { return ___U3CUiBeginDragU3Ek__BackingField_46; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiBeginDragU3Ek__BackingField_46() { return &___U3CUiBeginDragU3Ek__BackingField_46; }
	inline void set_U3CUiBeginDragU3Ek__BackingField_46(FsmEvent_t3736299882 * value)
	{
		___U3CUiBeginDragU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiBeginDragU3Ek__BackingField_46), value);
	}

	inline static int32_t get_offset_of_U3CUiDragU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiDragU3Ek__BackingField_47)); }
	inline FsmEvent_t3736299882 * get_U3CUiDragU3Ek__BackingField_47() const { return ___U3CUiDragU3Ek__BackingField_47; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiDragU3Ek__BackingField_47() { return &___U3CUiDragU3Ek__BackingField_47; }
	inline void set_U3CUiDragU3Ek__BackingField_47(FsmEvent_t3736299882 * value)
	{
		___U3CUiDragU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiDragU3Ek__BackingField_47), value);
	}

	inline static int32_t get_offset_of_U3CUiEndDragU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiEndDragU3Ek__BackingField_48)); }
	inline FsmEvent_t3736299882 * get_U3CUiEndDragU3Ek__BackingField_48() const { return ___U3CUiEndDragU3Ek__BackingField_48; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiEndDragU3Ek__BackingField_48() { return &___U3CUiEndDragU3Ek__BackingField_48; }
	inline void set_U3CUiEndDragU3Ek__BackingField_48(FsmEvent_t3736299882 * value)
	{
		___U3CUiEndDragU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiEndDragU3Ek__BackingField_48), value);
	}

	inline static int32_t get_offset_of_U3CUiClickU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiClickU3Ek__BackingField_49)); }
	inline FsmEvent_t3736299882 * get_U3CUiClickU3Ek__BackingField_49() const { return ___U3CUiClickU3Ek__BackingField_49; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiClickU3Ek__BackingField_49() { return &___U3CUiClickU3Ek__BackingField_49; }
	inline void set_U3CUiClickU3Ek__BackingField_49(FsmEvent_t3736299882 * value)
	{
		___U3CUiClickU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiClickU3Ek__BackingField_49), value);
	}

	inline static int32_t get_offset_of_U3CUiDropU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiDropU3Ek__BackingField_50)); }
	inline FsmEvent_t3736299882 * get_U3CUiDropU3Ek__BackingField_50() const { return ___U3CUiDropU3Ek__BackingField_50; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiDropU3Ek__BackingField_50() { return &___U3CUiDropU3Ek__BackingField_50; }
	inline void set_U3CUiDropU3Ek__BackingField_50(FsmEvent_t3736299882 * value)
	{
		___U3CUiDropU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiDropU3Ek__BackingField_50), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerClickU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerClickU3Ek__BackingField_51)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerClickU3Ek__BackingField_51() const { return ___U3CUiPointerClickU3Ek__BackingField_51; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerClickU3Ek__BackingField_51() { return &___U3CUiPointerClickU3Ek__BackingField_51; }
	inline void set_U3CUiPointerClickU3Ek__BackingField_51(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerClickU3Ek__BackingField_51 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerClickU3Ek__BackingField_51), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerDownU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerDownU3Ek__BackingField_52)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerDownU3Ek__BackingField_52() const { return ___U3CUiPointerDownU3Ek__BackingField_52; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerDownU3Ek__BackingField_52() { return &___U3CUiPointerDownU3Ek__BackingField_52; }
	inline void set_U3CUiPointerDownU3Ek__BackingField_52(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerDownU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerDownU3Ek__BackingField_52), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerEnterU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerEnterU3Ek__BackingField_53)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerEnterU3Ek__BackingField_53() const { return ___U3CUiPointerEnterU3Ek__BackingField_53; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerEnterU3Ek__BackingField_53() { return &___U3CUiPointerEnterU3Ek__BackingField_53; }
	inline void set_U3CUiPointerEnterU3Ek__BackingField_53(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerEnterU3Ek__BackingField_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerEnterU3Ek__BackingField_53), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerExitU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerExitU3Ek__BackingField_54)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerExitU3Ek__BackingField_54() const { return ___U3CUiPointerExitU3Ek__BackingField_54; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerExitU3Ek__BackingField_54() { return &___U3CUiPointerExitU3Ek__BackingField_54; }
	inline void set_U3CUiPointerExitU3Ek__BackingField_54(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerExitU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerExitU3Ek__BackingField_54), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerUpU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerUpU3Ek__BackingField_55)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerUpU3Ek__BackingField_55() const { return ___U3CUiPointerUpU3Ek__BackingField_55; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerUpU3Ek__BackingField_55() { return &___U3CUiPointerUpU3Ek__BackingField_55; }
	inline void set_U3CUiPointerUpU3Ek__BackingField_55(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerUpU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerUpU3Ek__BackingField_55), value);
	}

	inline static int32_t get_offset_of_U3CUiBoolValueChangedU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiBoolValueChangedU3Ek__BackingField_56)); }
	inline FsmEvent_t3736299882 * get_U3CUiBoolValueChangedU3Ek__BackingField_56() const { return ___U3CUiBoolValueChangedU3Ek__BackingField_56; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiBoolValueChangedU3Ek__BackingField_56() { return &___U3CUiBoolValueChangedU3Ek__BackingField_56; }
	inline void set_U3CUiBoolValueChangedU3Ek__BackingField_56(FsmEvent_t3736299882 * value)
	{
		___U3CUiBoolValueChangedU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiBoolValueChangedU3Ek__BackingField_56), value);
	}

	inline static int32_t get_offset_of_U3CUiFloatValueChangedU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiFloatValueChangedU3Ek__BackingField_57)); }
	inline FsmEvent_t3736299882 * get_U3CUiFloatValueChangedU3Ek__BackingField_57() const { return ___U3CUiFloatValueChangedU3Ek__BackingField_57; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiFloatValueChangedU3Ek__BackingField_57() { return &___U3CUiFloatValueChangedU3Ek__BackingField_57; }
	inline void set_U3CUiFloatValueChangedU3Ek__BackingField_57(FsmEvent_t3736299882 * value)
	{
		___U3CUiFloatValueChangedU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiFloatValueChangedU3Ek__BackingField_57), value);
	}

	inline static int32_t get_offset_of_U3CUiIntValueChangedU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiIntValueChangedU3Ek__BackingField_58)); }
	inline FsmEvent_t3736299882 * get_U3CUiIntValueChangedU3Ek__BackingField_58() const { return ___U3CUiIntValueChangedU3Ek__BackingField_58; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiIntValueChangedU3Ek__BackingField_58() { return &___U3CUiIntValueChangedU3Ek__BackingField_58; }
	inline void set_U3CUiIntValueChangedU3Ek__BackingField_58(FsmEvent_t3736299882 * value)
	{
		___U3CUiIntValueChangedU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiIntValueChangedU3Ek__BackingField_58), value);
	}

	inline static int32_t get_offset_of_U3CUiVector2ValueChangedU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiVector2ValueChangedU3Ek__BackingField_59)); }
	inline FsmEvent_t3736299882 * get_U3CUiVector2ValueChangedU3Ek__BackingField_59() const { return ___U3CUiVector2ValueChangedU3Ek__BackingField_59; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiVector2ValueChangedU3Ek__BackingField_59() { return &___U3CUiVector2ValueChangedU3Ek__BackingField_59; }
	inline void set_U3CUiVector2ValueChangedU3Ek__BackingField_59(FsmEvent_t3736299882 * value)
	{
		___U3CUiVector2ValueChangedU3Ek__BackingField_59 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiVector2ValueChangedU3Ek__BackingField_59), value);
	}

	inline static int32_t get_offset_of_U3CUiEndEditU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiEndEditU3Ek__BackingField_60)); }
	inline FsmEvent_t3736299882 * get_U3CUiEndEditU3Ek__BackingField_60() const { return ___U3CUiEndEditU3Ek__BackingField_60; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiEndEditU3Ek__BackingField_60() { return &___U3CUiEndEditU3Ek__BackingField_60; }
	inline void set_U3CUiEndEditU3Ek__BackingField_60(FsmEvent_t3736299882 * value)
	{
		___U3CUiEndEditU3Ek__BackingField_60 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiEndEditU3Ek__BackingField_60), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEVENT_T3736299882_H
#ifndef FSMVARIABLES_T3349589544_H
#define FSMVARIABLES_T3349589544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVariables
struct  FsmVariables_t3349589544  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::floatVariables
	FsmFloatU5BU5D_t3637897416* ___floatVariables_1;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::intVariables
	FsmIntU5BU5D_t2904461592* ___intVariables_2;
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::boolVariables
	FsmBoolU5BU5D_t1155011270* ___boolVariables_3;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::stringVariables
	FsmStringU5BU5D_t252501805* ___stringVariables_4;
	// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::vector2Variables
	FsmVector2U5BU5D_t1097319912* ___vector2Variables_5;
	// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::vector3Variables
	FsmVector3U5BU5D_t402703848* ___vector3Variables_6;
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::colorVariables
	FsmColorU5BU5D_t1111370293* ___colorVariables_7;
	// HutongGames.PlayMaker.FsmRect[] HutongGames.PlayMaker.FsmVariables::rectVariables
	FsmRectU5BU5D_t3969342952* ___rectVariables_8;
	// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::quaternionVariables
	FsmQuaternionU5BU5D_t495607758* ___quaternionVariables_9;
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.FsmVariables::gameObjectVariables
	FsmGameObjectU5BU5D_t719957643* ___gameObjectVariables_10;
	// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.FsmVariables::objectVariables
	FsmObjectU5BU5D_t635502296* ___objectVariables_11;
	// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.FsmVariables::materialVariables
	FsmMaterialU5BU5D_t3870809373* ___materialVariables_12;
	// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.FsmVariables::textureVariables
	FsmTextureU5BU5D_t2313143784* ___textureVariables_13;
	// HutongGames.PlayMaker.FsmArray[] HutongGames.PlayMaker.FsmVariables::arrayVariables
	FsmArrayU5BU5D_t2002857066* ___arrayVariables_14;
	// HutongGames.PlayMaker.FsmEnum[] HutongGames.PlayMaker.FsmVariables::enumVariables
	FsmEnumU5BU5D_t1010932050* ___enumVariables_15;
	// System.String[] HutongGames.PlayMaker.FsmVariables::categories
	StringU5BU5D_t1281789340* ___categories_16;
	// System.Int32[] HutongGames.PlayMaker.FsmVariables::variableCategoryIDs
	Int32U5BU5D_t385246372* ___variableCategoryIDs_17;

public:
	inline static int32_t get_offset_of_floatVariables_1() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___floatVariables_1)); }
	inline FsmFloatU5BU5D_t3637897416* get_floatVariables_1() const { return ___floatVariables_1; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_floatVariables_1() { return &___floatVariables_1; }
	inline void set_floatVariables_1(FsmFloatU5BU5D_t3637897416* value)
	{
		___floatVariables_1 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariables_1), value);
	}

	inline static int32_t get_offset_of_intVariables_2() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___intVariables_2)); }
	inline FsmIntU5BU5D_t2904461592* get_intVariables_2() const { return ___intVariables_2; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_intVariables_2() { return &___intVariables_2; }
	inline void set_intVariables_2(FsmIntU5BU5D_t2904461592* value)
	{
		___intVariables_2 = value;
		Il2CppCodeGenWriteBarrier((&___intVariables_2), value);
	}

	inline static int32_t get_offset_of_boolVariables_3() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___boolVariables_3)); }
	inline FsmBoolU5BU5D_t1155011270* get_boolVariables_3() const { return ___boolVariables_3; }
	inline FsmBoolU5BU5D_t1155011270** get_address_of_boolVariables_3() { return &___boolVariables_3; }
	inline void set_boolVariables_3(FsmBoolU5BU5D_t1155011270* value)
	{
		___boolVariables_3 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariables_3), value);
	}

	inline static int32_t get_offset_of_stringVariables_4() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___stringVariables_4)); }
	inline FsmStringU5BU5D_t252501805* get_stringVariables_4() const { return ___stringVariables_4; }
	inline FsmStringU5BU5D_t252501805** get_address_of_stringVariables_4() { return &___stringVariables_4; }
	inline void set_stringVariables_4(FsmStringU5BU5D_t252501805* value)
	{
		___stringVariables_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariables_4), value);
	}

	inline static int32_t get_offset_of_vector2Variables_5() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___vector2Variables_5)); }
	inline FsmVector2U5BU5D_t1097319912* get_vector2Variables_5() const { return ___vector2Variables_5; }
	inline FsmVector2U5BU5D_t1097319912** get_address_of_vector2Variables_5() { return &___vector2Variables_5; }
	inline void set_vector2Variables_5(FsmVector2U5BU5D_t1097319912* value)
	{
		___vector2Variables_5 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variables_5), value);
	}

	inline static int32_t get_offset_of_vector3Variables_6() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___vector3Variables_6)); }
	inline FsmVector3U5BU5D_t402703848* get_vector3Variables_6() const { return ___vector3Variables_6; }
	inline FsmVector3U5BU5D_t402703848** get_address_of_vector3Variables_6() { return &___vector3Variables_6; }
	inline void set_vector3Variables_6(FsmVector3U5BU5D_t402703848* value)
	{
		___vector3Variables_6 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variables_6), value);
	}

	inline static int32_t get_offset_of_colorVariables_7() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___colorVariables_7)); }
	inline FsmColorU5BU5D_t1111370293* get_colorVariables_7() const { return ___colorVariables_7; }
	inline FsmColorU5BU5D_t1111370293** get_address_of_colorVariables_7() { return &___colorVariables_7; }
	inline void set_colorVariables_7(FsmColorU5BU5D_t1111370293* value)
	{
		___colorVariables_7 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariables_7), value);
	}

	inline static int32_t get_offset_of_rectVariables_8() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___rectVariables_8)); }
	inline FsmRectU5BU5D_t3969342952* get_rectVariables_8() const { return ___rectVariables_8; }
	inline FsmRectU5BU5D_t3969342952** get_address_of_rectVariables_8() { return &___rectVariables_8; }
	inline void set_rectVariables_8(FsmRectU5BU5D_t3969342952* value)
	{
		___rectVariables_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariables_8), value);
	}

	inline static int32_t get_offset_of_quaternionVariables_9() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___quaternionVariables_9)); }
	inline FsmQuaternionU5BU5D_t495607758* get_quaternionVariables_9() const { return ___quaternionVariables_9; }
	inline FsmQuaternionU5BU5D_t495607758** get_address_of_quaternionVariables_9() { return &___quaternionVariables_9; }
	inline void set_quaternionVariables_9(FsmQuaternionU5BU5D_t495607758* value)
	{
		___quaternionVariables_9 = value;
		Il2CppCodeGenWriteBarrier((&___quaternionVariables_9), value);
	}

	inline static int32_t get_offset_of_gameObjectVariables_10() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___gameObjectVariables_10)); }
	inline FsmGameObjectU5BU5D_t719957643* get_gameObjectVariables_10() const { return ___gameObjectVariables_10; }
	inline FsmGameObjectU5BU5D_t719957643** get_address_of_gameObjectVariables_10() { return &___gameObjectVariables_10; }
	inline void set_gameObjectVariables_10(FsmGameObjectU5BU5D_t719957643* value)
	{
		___gameObjectVariables_10 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectVariables_10), value);
	}

	inline static int32_t get_offset_of_objectVariables_11() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___objectVariables_11)); }
	inline FsmObjectU5BU5D_t635502296* get_objectVariables_11() const { return ___objectVariables_11; }
	inline FsmObjectU5BU5D_t635502296** get_address_of_objectVariables_11() { return &___objectVariables_11; }
	inline void set_objectVariables_11(FsmObjectU5BU5D_t635502296* value)
	{
		___objectVariables_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectVariables_11), value);
	}

	inline static int32_t get_offset_of_materialVariables_12() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___materialVariables_12)); }
	inline FsmMaterialU5BU5D_t3870809373* get_materialVariables_12() const { return ___materialVariables_12; }
	inline FsmMaterialU5BU5D_t3870809373** get_address_of_materialVariables_12() { return &___materialVariables_12; }
	inline void set_materialVariables_12(FsmMaterialU5BU5D_t3870809373* value)
	{
		___materialVariables_12 = value;
		Il2CppCodeGenWriteBarrier((&___materialVariables_12), value);
	}

	inline static int32_t get_offset_of_textureVariables_13() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___textureVariables_13)); }
	inline FsmTextureU5BU5D_t2313143784* get_textureVariables_13() const { return ___textureVariables_13; }
	inline FsmTextureU5BU5D_t2313143784** get_address_of_textureVariables_13() { return &___textureVariables_13; }
	inline void set_textureVariables_13(FsmTextureU5BU5D_t2313143784* value)
	{
		___textureVariables_13 = value;
		Il2CppCodeGenWriteBarrier((&___textureVariables_13), value);
	}

	inline static int32_t get_offset_of_arrayVariables_14() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___arrayVariables_14)); }
	inline FsmArrayU5BU5D_t2002857066* get_arrayVariables_14() const { return ___arrayVariables_14; }
	inline FsmArrayU5BU5D_t2002857066** get_address_of_arrayVariables_14() { return &___arrayVariables_14; }
	inline void set_arrayVariables_14(FsmArrayU5BU5D_t2002857066* value)
	{
		___arrayVariables_14 = value;
		Il2CppCodeGenWriteBarrier((&___arrayVariables_14), value);
	}

	inline static int32_t get_offset_of_enumVariables_15() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___enumVariables_15)); }
	inline FsmEnumU5BU5D_t1010932050* get_enumVariables_15() const { return ___enumVariables_15; }
	inline FsmEnumU5BU5D_t1010932050** get_address_of_enumVariables_15() { return &___enumVariables_15; }
	inline void set_enumVariables_15(FsmEnumU5BU5D_t1010932050* value)
	{
		___enumVariables_15 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariables_15), value);
	}

	inline static int32_t get_offset_of_categories_16() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___categories_16)); }
	inline StringU5BU5D_t1281789340* get_categories_16() const { return ___categories_16; }
	inline StringU5BU5D_t1281789340** get_address_of_categories_16() { return &___categories_16; }
	inline void set_categories_16(StringU5BU5D_t1281789340* value)
	{
		___categories_16 = value;
		Il2CppCodeGenWriteBarrier((&___categories_16), value);
	}

	inline static int32_t get_offset_of_variableCategoryIDs_17() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___variableCategoryIDs_17)); }
	inline Int32U5BU5D_t385246372* get_variableCategoryIDs_17() const { return ___variableCategoryIDs_17; }
	inline Int32U5BU5D_t385246372** get_address_of_variableCategoryIDs_17() { return &___variableCategoryIDs_17; }
	inline void set_variableCategoryIDs_17(Int32U5BU5D_t385246372* value)
	{
		___variableCategoryIDs_17 = value;
		Il2CppCodeGenWriteBarrier((&___variableCategoryIDs_17), value);
	}
};

struct FsmVariables_t3349589544_StaticFields
{
public:
	// System.Boolean HutongGames.PlayMaker.FsmVariables::<GlobalVariablesSynced>k__BackingField
	bool ___U3CGlobalVariablesSyncedU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544_StaticFields, ___U3CGlobalVariablesSyncedU3Ek__BackingField_0)); }
	inline bool get_U3CGlobalVariablesSyncedU3Ek__BackingField_0() const { return ___U3CGlobalVariablesSyncedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CGlobalVariablesSyncedU3Ek__BackingField_0() { return &___U3CGlobalVariablesSyncedU3Ek__BackingField_0; }
	inline void set_U3CGlobalVariablesSyncedU3Ek__BackingField_0(bool value)
	{
		___U3CGlobalVariablesSyncedU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMVARIABLES_T3349589544_H
#ifndef COLORUTILS_T43876202_H
#define COLORUTILS_T43876202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.ColorUtils
struct  ColorUtils_t43876202  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTILS_T43876202_H
#ifndef STRINGUTILS_T862297538_H
#define STRINGUTILS_T862297538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.StringUtils
struct  StringUtils_t862297538  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T862297538_H
#ifndef U3CDOCOROUTINEU3ED__43_T4100907003_H
#define U3CDOCOROUTINEU3ED__43_T4100907003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerFSM/<DoCoroutine>d__43
struct  U3CDoCoroutineU3Ed__43_t4100907003  : public RuntimeObject
{
public:
	// System.Int32 PlayMakerFSM/<DoCoroutine>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PlayMakerFSM/<DoCoroutine>d__43::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PlayMakerFSM PlayMakerFSM/<DoCoroutine>d__43::<>4__this
	PlayMakerFSM_t1613010231 * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator PlayMakerFSM/<DoCoroutine>d__43::routine
	RuntimeObject* ___routine_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__43_t4100907003, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__43_t4100907003, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__43_t4100907003, ___U3CU3E4__this_2)); }
	inline PlayMakerFSM_t1613010231 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayMakerFSM_t1613010231 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_routine_3() { return static_cast<int32_t>(offsetof(U3CDoCoroutineU3Ed__43_t4100907003, ___routine_3)); }
	inline RuntimeObject* get_routine_3() const { return ___routine_3; }
	inline RuntimeObject** get_address_of_routine_3() { return &___routine_3; }
	inline void set_routine_3(RuntimeObject* value)
	{
		___routine_3 = value;
		Il2CppCodeGenWriteBarrier((&___routine_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOCOROUTINEU3ED__43_T4100907003_H
#ifndef U3CU3EC_T2300308631_H
#define U3CU3EC_T2300308631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGUI/<>c
struct  U3CU3Ec_t2300308631  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2300308631_StaticFields
{
public:
	// PlayMakerGUI/<>c PlayMakerGUI/<>c::<>9
	U3CU3Ec_t2300308631 * ___U3CU3E9_0;
	// System.Comparison`1<PlayMakerFSM> PlayMakerGUI/<>c::<>9__65_0
	Comparison_1_t1387941410 * ___U3CU3E9__65_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2300308631_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2300308631 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2300308631 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2300308631 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__65_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2300308631_StaticFields, ___U3CU3E9__65_0_1)); }
	inline Comparison_1_t1387941410 * get_U3CU3E9__65_0_1() const { return ___U3CU3E9__65_0_1; }
	inline Comparison_1_t1387941410 ** get_address_of_U3CU3E9__65_0_1() { return &___U3CU3E9__65_0_1; }
	inline void set_U3CU3E9__65_0_1(Comparison_1_t1387941410 * value)
	{
		___U3CU3E9__65_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__65_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2300308631_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T1424664817_H
#define U3CU3EC__DISPLAYCLASS36_0_T1424664817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGlobals/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t1424664817  : public RuntimeObject
{
public:
	// System.String PlayMakerGlobals/<>c__DisplayClass36_0::eventName
	String_t* ___eventName_0;

public:
	inline static int32_t get_offset_of_eventName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1424664817, ___eventName_0)); }
	inline String_t* get_eventName_0() const { return ___eventName_0; }
	inline String_t** get_address_of_eventName_0() { return &___eventName_0; }
	inline void set_eventName_0(String_t* value)
	{
		___eventName_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T1424664817_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef LIST_1_T1304255270_H
#define LIST_1_T1304255270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct  List_1_t1304255270  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FsmU5BU5D_t3243132433* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1304255270, ____items_1)); }
	inline FsmU5BU5D_t3243132433* get__items_1() const { return ____items_1; }
	inline FsmU5BU5D_t3243132433** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FsmU5BU5D_t3243132433* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1304255270, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1304255270, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1304255270_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	FsmU5BU5D_t3243132433* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1304255270_StaticFields, ___EmptyArray_4)); }
	inline FsmU5BU5D_t3243132433* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline FsmU5BU5D_t3243132433** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(FsmU5BU5D_t3243132433* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1304255270_H
#ifndef LIST_1_T3085084973_H
#define LIST_1_T3085084973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<PlayMakerFSM>
struct  List_1_t3085084973  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PlayMakerFSMU5BU5D_t560886798* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3085084973, ____items_1)); }
	inline PlayMakerFSMU5BU5D_t560886798* get__items_1() const { return ____items_1; }
	inline PlayMakerFSMU5BU5D_t560886798** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PlayMakerFSMU5BU5D_t560886798* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3085084973, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3085084973, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3085084973_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PlayMakerFSMU5BU5D_t560886798* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3085084973_StaticFields, ___EmptyArray_4)); }
	inline PlayMakerFSMU5BU5D_t560886798* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PlayMakerFSMU5BU5D_t560886798** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PlayMakerFSMU5BU5D_t560886798* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3085084973_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef CAPTURE_T2232016050_H
#define CAPTURE_T2232016050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t2232016050  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.Capture::index
	int32_t ___index_0;
	// System.Int32 System.Text.RegularExpressions.Capture::length
	int32_t ___length_1;
	// System.String System.Text.RegularExpressions.Capture::text
	String_t* ___text_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Capture_t2232016050, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T2232016050_H
#ifndef GROUPCOLLECTION_T69770484_H
#define GROUPCOLLECTION_T69770484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_t69770484  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::list
	GroupU5BU5D_t1880820351* ___list_0;
	// System.Int32 System.Text.RegularExpressions.GroupCollection::gap
	int32_t ___gap_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ___list_0)); }
	inline GroupU5BU5D_t1880820351* get_list_0() const { return ___list_0; }
	inline GroupU5BU5D_t1880820351** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(GroupU5BU5D_t1880820351* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_gap_1() { return static_cast<int32_t>(offsetof(GroupCollection_t69770484, ___gap_1)); }
	inline int32_t get_gap_1() const { return ___gap_1; }
	inline int32_t* get_address_of_gap_1() { return &___gap_1; }
	inline void set_gap_1(int32_t value)
	{
		___gap_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_T69770484_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GUICONTENT_T3050628031_H
#define GUICONTENT_T3050628031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIContent
struct  GUIContent_t3050628031  : public RuntimeObject
{
public:
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t3661962703 * ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Image_1)); }
	inline Texture_t3661962703 * get_m_Image_1() const { return ___m_Image_1; }
	inline Texture_t3661962703 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Texture_t3661962703 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}
};

struct GUIContent_t3050628031_StaticFields
{
public:
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t3050628031 * ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t3050628031 * ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t3050628031 * ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t3050628031 * ___none_6;

public:
	inline static int32_t get_offset_of_s_Text_3() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_Text_3)); }
	inline GUIContent_t3050628031 * get_s_Text_3() const { return ___s_Text_3; }
	inline GUIContent_t3050628031 ** get_address_of_s_Text_3() { return &___s_Text_3; }
	inline void set_s_Text_3(GUIContent_t3050628031 * value)
	{
		___s_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Text_3), value);
	}

	inline static int32_t get_offset_of_s_Image_4() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_Image_4)); }
	inline GUIContent_t3050628031 * get_s_Image_4() const { return ___s_Image_4; }
	inline GUIContent_t3050628031 ** get_address_of_s_Image_4() { return &___s_Image_4; }
	inline void set_s_Image_4(GUIContent_t3050628031 * value)
	{
		___s_Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Image_4), value);
	}

	inline static int32_t get_offset_of_s_TextImage_5() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_TextImage_5)); }
	inline GUIContent_t3050628031 * get_s_TextImage_5() const { return ___s_TextImage_5; }
	inline GUIContent_t3050628031 ** get_address_of_s_TextImage_5() { return &___s_TextImage_5; }
	inline void set_s_TextImage_5(GUIContent_t3050628031 * value)
	{
		___s_TextImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextImage_5), value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___none_6)); }
	inline GUIContent_t3050628031 * get_none_6() const { return ___none_6; }
	inline GUIContent_t3050628031 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(GUIContent_t3050628031 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier((&___none_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t3050628031_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t3661962703 * ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t3050628031_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t3661962703 * ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};
#endif // GUICONTENT_T3050628031_H
#ifndef REQUIREDFIELDATTRIBUTE_T3920128169_H
#define REQUIREDFIELDATTRIBUTE_T3920128169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.RequiredFieldAttribute
struct  RequiredFieldAttribute_t3920128169  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDFIELDATTRIBUTE_T3920128169_H
#ifndef SETTINGSMENUITEMATTRIBUTE_T2376176270_H
#define SETTINGSMENUITEMATTRIBUTE_T2376176270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.SettingsMenuItemAttribute
struct  SettingsMenuItemAttribute_t2376176270  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.SettingsMenuItemAttribute::<MenuItem>k__BackingField
	String_t* ___U3CMenuItemU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMenuItemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SettingsMenuItemAttribute_t2376176270, ___U3CMenuItemU3Ek__BackingField_0)); }
	inline String_t* get_U3CMenuItemU3Ek__BackingField_0() const { return ___U3CMenuItemU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMenuItemU3Ek__BackingField_0() { return &___U3CMenuItemU3Ek__BackingField_0; }
	inline void set_U3CMenuItemU3Ek__BackingField_0(String_t* value)
	{
		___U3CMenuItemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuItemU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMENUITEMATTRIBUTE_T2376176270_H
#ifndef SETTINGSMENUITEMSTATEATTRIBUTE_T3930598301_H
#define SETTINGSMENUITEMSTATEATTRIBUTE_T3930598301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.SettingsMenuItemStateAttribute
struct  SettingsMenuItemStateAttribute_t3930598301  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.SettingsMenuItemStateAttribute::<MenuItem>k__BackingField
	String_t* ___U3CMenuItemU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMenuItemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SettingsMenuItemStateAttribute_t3930598301, ___U3CMenuItemU3Ek__BackingField_0)); }
	inline String_t* get_U3CMenuItemU3Ek__BackingField_0() const { return ___U3CMenuItemU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMenuItemU3Ek__BackingField_0() { return &___U3CMenuItemU3Ek__BackingField_0; }
	inline void set_U3CMenuItemU3Ek__BackingField_0(String_t* value)
	{
		___U3CMenuItemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuItemU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMENUITEMSTATEATTRIBUTE_T3930598301_H
#ifndef TITLEATTRIBUTE_T196394849_H
#define TITLEATTRIBUTE_T196394849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TitleAttribute
struct  TitleAttribute_t196394849  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.TitleAttribute::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(TitleAttribute_t196394849, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEATTRIBUTE_T196394849_H
#ifndef TOOLTIPATTRIBUTE_T2411168496_H
#define TOOLTIPATTRIBUTE_T2411168496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TooltipAttribute
struct  TooltipAttribute_t2411168496  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.TooltipAttribute::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t2411168496, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPATTRIBUTE_T2411168496_H
#ifndef VARIABLETYPEFILTER_T3022214907_H
#define VARIABLETYPEFILTER_T3022214907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableTypeFilter
struct  VariableTypeFilter_t3022214907  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPEFILTER_T3022214907_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUMERATOR_T679361554_H
#define ENUMERATOR_T679361554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>
struct  Enumerator_t679361554 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3085084973 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PlayMakerFSM_t1613010231 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t679361554, ___l_0)); }
	inline List_1_t3085084973 * get_l_0() const { return ___l_0; }
	inline List_1_t3085084973 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3085084973 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t679361554, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t679361554, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t679361554, ___current_3)); }
	inline PlayMakerFSM_t1613010231 * get_current_3() const { return ___current_3; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PlayMakerFSM_t1613010231 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T679361554_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef GROUP_T2468205786_H
#define GROUP_T2468205786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t2468205786  : public Capture_t2232016050
{
public:
	// System.Boolean System.Text.RegularExpressions.Group::success
	bool ___success_4;
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::captures
	CaptureCollection_t1760593541 * ___captures_5;

public:
	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Group_t2468205786, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_captures_5() { return static_cast<int32_t>(offsetof(Group_t2468205786, ___captures_5)); }
	inline CaptureCollection_t1760593541 * get_captures_5() const { return ___captures_5; }
	inline CaptureCollection_t1760593541 ** get_address_of_captures_5() { return &___captures_5; }
	inline void set_captures_5(CaptureCollection_t1760593541 * value)
	{
		___captures_5 = value;
		Il2CppCodeGenWriteBarrier((&___captures_5), value);
	}
};

struct Group_t2468205786_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::Fail
	Group_t2468205786 * ___Fail_3;

public:
	inline static int32_t get_offset_of_Fail_3() { return static_cast<int32_t>(offsetof(Group_t2468205786_StaticFields, ___Fail_3)); }
	inline Group_t2468205786 * get_Fail_3() const { return ___Fail_3; }
	inline Group_t2468205786 ** get_address_of_Fail_3() { return &___Fail_3; }
	inline void set_Fail_3(Group_t2468205786 * value)
	{
		___Fail_3 = value;
		Il2CppCodeGenWriteBarrier((&___Fail_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T2468205786_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef EDITORFLAGS_T2240759143_H
#define EDITORFLAGS_T2240759143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Fsm/EditorFlags
struct  EditorFlags_t2240759143 
{
public:
	// System.Int32 HutongGames.PlayMaker.Fsm/EditorFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EditorFlags_t2240759143, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFLAGS_T2240759143_H
#ifndef FSMEVENTDATA_T1692568573_H
#define FSMEVENTDATA_T1692568573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEventData
struct  FsmEventData_t1692568573  : public RuntimeObject
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmEventData::SentByGameObject
	GameObject_t1113636619 * ___SentByGameObject_0;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmEventData::SentByFsm
	Fsm_t4127147824 * ___SentByFsm_1;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmEventData::SentByState
	FsmState_t4124398234 * ___SentByState_2;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmEventData::SentByAction
	FsmStateAction_t3801304101 * ___SentByAction_3;
	// System.Boolean HutongGames.PlayMaker.FsmEventData::BoolData
	bool ___BoolData_4;
	// System.Int32 HutongGames.PlayMaker.FsmEventData::IntData
	int32_t ___IntData_5;
	// System.Single HutongGames.PlayMaker.FsmEventData::FloatData
	float ___FloatData_6;
	// UnityEngine.Vector2 HutongGames.PlayMaker.FsmEventData::Vector2Data
	Vector2_t2156229523  ___Vector2Data_7;
	// UnityEngine.Vector3 HutongGames.PlayMaker.FsmEventData::Vector3Data
	Vector3_t3722313464  ___Vector3Data_8;
	// System.String HutongGames.PlayMaker.FsmEventData::StringData
	String_t* ___StringData_9;
	// UnityEngine.Quaternion HutongGames.PlayMaker.FsmEventData::QuaternionData
	Quaternion_t2301928331  ___QuaternionData_10;
	// UnityEngine.Rect HutongGames.PlayMaker.FsmEventData::RectData
	Rect_t2360479859  ___RectData_11;
	// UnityEngine.Color HutongGames.PlayMaker.FsmEventData::ColorData
	Color_t2555686324  ___ColorData_12;
	// UnityEngine.Object HutongGames.PlayMaker.FsmEventData::ObjectData
	Object_t631007953 * ___ObjectData_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmEventData::GameObjectData
	GameObject_t1113636619 * ___GameObjectData_14;
	// UnityEngine.Material HutongGames.PlayMaker.FsmEventData::MaterialData
	Material_t340375123 * ___MaterialData_15;
	// UnityEngine.Texture HutongGames.PlayMaker.FsmEventData::TextureData
	Texture_t3661962703 * ___TextureData_16;

public:
	inline static int32_t get_offset_of_SentByGameObject_0() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByGameObject_0)); }
	inline GameObject_t1113636619 * get_SentByGameObject_0() const { return ___SentByGameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_SentByGameObject_0() { return &___SentByGameObject_0; }
	inline void set_SentByGameObject_0(GameObject_t1113636619 * value)
	{
		___SentByGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___SentByGameObject_0), value);
	}

	inline static int32_t get_offset_of_SentByFsm_1() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByFsm_1)); }
	inline Fsm_t4127147824 * get_SentByFsm_1() const { return ___SentByFsm_1; }
	inline Fsm_t4127147824 ** get_address_of_SentByFsm_1() { return &___SentByFsm_1; }
	inline void set_SentByFsm_1(Fsm_t4127147824 * value)
	{
		___SentByFsm_1 = value;
		Il2CppCodeGenWriteBarrier((&___SentByFsm_1), value);
	}

	inline static int32_t get_offset_of_SentByState_2() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByState_2)); }
	inline FsmState_t4124398234 * get_SentByState_2() const { return ___SentByState_2; }
	inline FsmState_t4124398234 ** get_address_of_SentByState_2() { return &___SentByState_2; }
	inline void set_SentByState_2(FsmState_t4124398234 * value)
	{
		___SentByState_2 = value;
		Il2CppCodeGenWriteBarrier((&___SentByState_2), value);
	}

	inline static int32_t get_offset_of_SentByAction_3() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByAction_3)); }
	inline FsmStateAction_t3801304101 * get_SentByAction_3() const { return ___SentByAction_3; }
	inline FsmStateAction_t3801304101 ** get_address_of_SentByAction_3() { return &___SentByAction_3; }
	inline void set_SentByAction_3(FsmStateAction_t3801304101 * value)
	{
		___SentByAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___SentByAction_3), value);
	}

	inline static int32_t get_offset_of_BoolData_4() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___BoolData_4)); }
	inline bool get_BoolData_4() const { return ___BoolData_4; }
	inline bool* get_address_of_BoolData_4() { return &___BoolData_4; }
	inline void set_BoolData_4(bool value)
	{
		___BoolData_4 = value;
	}

	inline static int32_t get_offset_of_IntData_5() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___IntData_5)); }
	inline int32_t get_IntData_5() const { return ___IntData_5; }
	inline int32_t* get_address_of_IntData_5() { return &___IntData_5; }
	inline void set_IntData_5(int32_t value)
	{
		___IntData_5 = value;
	}

	inline static int32_t get_offset_of_FloatData_6() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___FloatData_6)); }
	inline float get_FloatData_6() const { return ___FloatData_6; }
	inline float* get_address_of_FloatData_6() { return &___FloatData_6; }
	inline void set_FloatData_6(float value)
	{
		___FloatData_6 = value;
	}

	inline static int32_t get_offset_of_Vector2Data_7() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___Vector2Data_7)); }
	inline Vector2_t2156229523  get_Vector2Data_7() const { return ___Vector2Data_7; }
	inline Vector2_t2156229523 * get_address_of_Vector2Data_7() { return &___Vector2Data_7; }
	inline void set_Vector2Data_7(Vector2_t2156229523  value)
	{
		___Vector2Data_7 = value;
	}

	inline static int32_t get_offset_of_Vector3Data_8() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___Vector3Data_8)); }
	inline Vector3_t3722313464  get_Vector3Data_8() const { return ___Vector3Data_8; }
	inline Vector3_t3722313464 * get_address_of_Vector3Data_8() { return &___Vector3Data_8; }
	inline void set_Vector3Data_8(Vector3_t3722313464  value)
	{
		___Vector3Data_8 = value;
	}

	inline static int32_t get_offset_of_StringData_9() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___StringData_9)); }
	inline String_t* get_StringData_9() const { return ___StringData_9; }
	inline String_t** get_address_of_StringData_9() { return &___StringData_9; }
	inline void set_StringData_9(String_t* value)
	{
		___StringData_9 = value;
		Il2CppCodeGenWriteBarrier((&___StringData_9), value);
	}

	inline static int32_t get_offset_of_QuaternionData_10() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___QuaternionData_10)); }
	inline Quaternion_t2301928331  get_QuaternionData_10() const { return ___QuaternionData_10; }
	inline Quaternion_t2301928331 * get_address_of_QuaternionData_10() { return &___QuaternionData_10; }
	inline void set_QuaternionData_10(Quaternion_t2301928331  value)
	{
		___QuaternionData_10 = value;
	}

	inline static int32_t get_offset_of_RectData_11() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___RectData_11)); }
	inline Rect_t2360479859  get_RectData_11() const { return ___RectData_11; }
	inline Rect_t2360479859 * get_address_of_RectData_11() { return &___RectData_11; }
	inline void set_RectData_11(Rect_t2360479859  value)
	{
		___RectData_11 = value;
	}

	inline static int32_t get_offset_of_ColorData_12() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___ColorData_12)); }
	inline Color_t2555686324  get_ColorData_12() const { return ___ColorData_12; }
	inline Color_t2555686324 * get_address_of_ColorData_12() { return &___ColorData_12; }
	inline void set_ColorData_12(Color_t2555686324  value)
	{
		___ColorData_12 = value;
	}

	inline static int32_t get_offset_of_ObjectData_13() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___ObjectData_13)); }
	inline Object_t631007953 * get_ObjectData_13() const { return ___ObjectData_13; }
	inline Object_t631007953 ** get_address_of_ObjectData_13() { return &___ObjectData_13; }
	inline void set_ObjectData_13(Object_t631007953 * value)
	{
		___ObjectData_13 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectData_13), value);
	}

	inline static int32_t get_offset_of_GameObjectData_14() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___GameObjectData_14)); }
	inline GameObject_t1113636619 * get_GameObjectData_14() const { return ___GameObjectData_14; }
	inline GameObject_t1113636619 ** get_address_of_GameObjectData_14() { return &___GameObjectData_14; }
	inline void set_GameObjectData_14(GameObject_t1113636619 * value)
	{
		___GameObjectData_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectData_14), value);
	}

	inline static int32_t get_offset_of_MaterialData_15() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___MaterialData_15)); }
	inline Material_t340375123 * get_MaterialData_15() const { return ___MaterialData_15; }
	inline Material_t340375123 ** get_address_of_MaterialData_15() { return &___MaterialData_15; }
	inline void set_MaterialData_15(Material_t340375123 * value)
	{
		___MaterialData_15 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialData_15), value);
	}

	inline static int32_t get_offset_of_TextureData_16() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___TextureData_16)); }
	inline Texture_t3661962703 * get_TextureData_16() const { return ___TextureData_16; }
	inline Texture_t3661962703 ** get_address_of_TextureData_16() { return &___TextureData_16; }
	inline void set_TextureData_16(Texture_t3661962703 * value)
	{
		___TextureData_16 = value;
		Il2CppCodeGenWriteBarrier((&___TextureData_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEVENTDATA_T1692568573_H
#ifndef FSMSTATE_T4124398234_H
#define FSMSTATE_T4124398234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmState
struct  FsmState_t4124398234  : public RuntimeObject
{
public:
	// System.Single HutongGames.PlayMaker.FsmState::<StateTime>k__BackingField
	float ___U3CStateTimeU3Ek__BackingField_0;
	// System.Single HutongGames.PlayMaker.FsmState::<RealStartTime>k__BackingField
	float ___U3CRealStartTimeU3Ek__BackingField_1;
	// System.Int32 HutongGames.PlayMaker.FsmState::<loopCount>k__BackingField
	int32_t ___U3CloopCountU3Ek__BackingField_2;
	// System.Int32 HutongGames.PlayMaker.FsmState::<maxLoopCount>k__BackingField
	int32_t ___U3CmaxLoopCountU3Ek__BackingField_3;
	// System.Boolean HutongGames.PlayMaker.FsmState::active
	bool ___active_4;
	// System.Boolean HutongGames.PlayMaker.FsmState::finished
	bool ___finished_5;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmState::activeAction
	FsmStateAction_t3801304101 * ___activeAction_6;
	// System.Int32 HutongGames.PlayMaker.FsmState::activeActionIndex
	int32_t ___activeActionIndex_7;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmState::fsm
	Fsm_t4127147824 * ___fsm_8;
	// System.String HutongGames.PlayMaker.FsmState::name
	String_t* ___name_9;
	// System.String HutongGames.PlayMaker.FsmState::description
	String_t* ___description_10;
	// System.Byte HutongGames.PlayMaker.FsmState::colorIndex
	uint8_t ___colorIndex_11;
	// UnityEngine.Rect HutongGames.PlayMaker.FsmState::position
	Rect_t2360479859  ___position_12;
	// System.Boolean HutongGames.PlayMaker.FsmState::isBreakpoint
	bool ___isBreakpoint_13;
	// System.Boolean HutongGames.PlayMaker.FsmState::isSequence
	bool ___isSequence_14;
	// System.Boolean HutongGames.PlayMaker.FsmState::hideUnused
	bool ___hideUnused_15;
	// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.FsmState::transitions
	FsmTransitionU5BU5D_t3577734832* ___transitions_16;
	// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.FsmState::actions
	FsmStateActionU5BU5D_t1918078376* ___actions_17;
	// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.FsmState::actionData
	ActionData_t1922786972 * ___actionData_18;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::activeActions
	List_1_t978411547 * ___activeActions_19;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::_finishedActions
	List_1_t978411547 * ____finishedActions_20;

public:
	inline static int32_t get_offset_of_U3CStateTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CStateTimeU3Ek__BackingField_0)); }
	inline float get_U3CStateTimeU3Ek__BackingField_0() const { return ___U3CStateTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CStateTimeU3Ek__BackingField_0() { return &___U3CStateTimeU3Ek__BackingField_0; }
	inline void set_U3CStateTimeU3Ek__BackingField_0(float value)
	{
		___U3CStateTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRealStartTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CRealStartTimeU3Ek__BackingField_1)); }
	inline float get_U3CRealStartTimeU3Ek__BackingField_1() const { return ___U3CRealStartTimeU3Ek__BackingField_1; }
	inline float* get_address_of_U3CRealStartTimeU3Ek__BackingField_1() { return &___U3CRealStartTimeU3Ek__BackingField_1; }
	inline void set_U3CRealStartTimeU3Ek__BackingField_1(float value)
	{
		___U3CRealStartTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CloopCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CloopCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CloopCountU3Ek__BackingField_2() const { return ___U3CloopCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CloopCountU3Ek__BackingField_2() { return &___U3CloopCountU3Ek__BackingField_2; }
	inline void set_U3CloopCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CloopCountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmaxLoopCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CmaxLoopCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CmaxLoopCountU3Ek__BackingField_3() const { return ___U3CmaxLoopCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmaxLoopCountU3Ek__BackingField_3() { return &___U3CmaxLoopCountU3Ek__BackingField_3; }
	inline void set_U3CmaxLoopCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CmaxLoopCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_finished_5() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___finished_5)); }
	inline bool get_finished_5() const { return ___finished_5; }
	inline bool* get_address_of_finished_5() { return &___finished_5; }
	inline void set_finished_5(bool value)
	{
		___finished_5 = value;
	}

	inline static int32_t get_offset_of_activeAction_6() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___activeAction_6)); }
	inline FsmStateAction_t3801304101 * get_activeAction_6() const { return ___activeAction_6; }
	inline FsmStateAction_t3801304101 ** get_address_of_activeAction_6() { return &___activeAction_6; }
	inline void set_activeAction_6(FsmStateAction_t3801304101 * value)
	{
		___activeAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___activeAction_6), value);
	}

	inline static int32_t get_offset_of_activeActionIndex_7() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___activeActionIndex_7)); }
	inline int32_t get_activeActionIndex_7() const { return ___activeActionIndex_7; }
	inline int32_t* get_address_of_activeActionIndex_7() { return &___activeActionIndex_7; }
	inline void set_activeActionIndex_7(int32_t value)
	{
		___activeActionIndex_7 = value;
	}

	inline static int32_t get_offset_of_fsm_8() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___fsm_8)); }
	inline Fsm_t4127147824 * get_fsm_8() const { return ___fsm_8; }
	inline Fsm_t4127147824 ** get_address_of_fsm_8() { return &___fsm_8; }
	inline void set_fsm_8(Fsm_t4127147824 * value)
	{
		___fsm_8 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_8), value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_colorIndex_11() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___colorIndex_11)); }
	inline uint8_t get_colorIndex_11() const { return ___colorIndex_11; }
	inline uint8_t* get_address_of_colorIndex_11() { return &___colorIndex_11; }
	inline void set_colorIndex_11(uint8_t value)
	{
		___colorIndex_11 = value;
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___position_12)); }
	inline Rect_t2360479859  get_position_12() const { return ___position_12; }
	inline Rect_t2360479859 * get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(Rect_t2360479859  value)
	{
		___position_12 = value;
	}

	inline static int32_t get_offset_of_isBreakpoint_13() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___isBreakpoint_13)); }
	inline bool get_isBreakpoint_13() const { return ___isBreakpoint_13; }
	inline bool* get_address_of_isBreakpoint_13() { return &___isBreakpoint_13; }
	inline void set_isBreakpoint_13(bool value)
	{
		___isBreakpoint_13 = value;
	}

	inline static int32_t get_offset_of_isSequence_14() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___isSequence_14)); }
	inline bool get_isSequence_14() const { return ___isSequence_14; }
	inline bool* get_address_of_isSequence_14() { return &___isSequence_14; }
	inline void set_isSequence_14(bool value)
	{
		___isSequence_14 = value;
	}

	inline static int32_t get_offset_of_hideUnused_15() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___hideUnused_15)); }
	inline bool get_hideUnused_15() const { return ___hideUnused_15; }
	inline bool* get_address_of_hideUnused_15() { return &___hideUnused_15; }
	inline void set_hideUnused_15(bool value)
	{
		___hideUnused_15 = value;
	}

	inline static int32_t get_offset_of_transitions_16() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___transitions_16)); }
	inline FsmTransitionU5BU5D_t3577734832* get_transitions_16() const { return ___transitions_16; }
	inline FsmTransitionU5BU5D_t3577734832** get_address_of_transitions_16() { return &___transitions_16; }
	inline void set_transitions_16(FsmTransitionU5BU5D_t3577734832* value)
	{
		___transitions_16 = value;
		Il2CppCodeGenWriteBarrier((&___transitions_16), value);
	}

	inline static int32_t get_offset_of_actions_17() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___actions_17)); }
	inline FsmStateActionU5BU5D_t1918078376* get_actions_17() const { return ___actions_17; }
	inline FsmStateActionU5BU5D_t1918078376** get_address_of_actions_17() { return &___actions_17; }
	inline void set_actions_17(FsmStateActionU5BU5D_t1918078376* value)
	{
		___actions_17 = value;
		Il2CppCodeGenWriteBarrier((&___actions_17), value);
	}

	inline static int32_t get_offset_of_actionData_18() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___actionData_18)); }
	inline ActionData_t1922786972 * get_actionData_18() const { return ___actionData_18; }
	inline ActionData_t1922786972 ** get_address_of_actionData_18() { return &___actionData_18; }
	inline void set_actionData_18(ActionData_t1922786972 * value)
	{
		___actionData_18 = value;
		Il2CppCodeGenWriteBarrier((&___actionData_18), value);
	}

	inline static int32_t get_offset_of_activeActions_19() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___activeActions_19)); }
	inline List_1_t978411547 * get_activeActions_19() const { return ___activeActions_19; }
	inline List_1_t978411547 ** get_address_of_activeActions_19() { return &___activeActions_19; }
	inline void set_activeActions_19(List_1_t978411547 * value)
	{
		___activeActions_19 = value;
		Il2CppCodeGenWriteBarrier((&___activeActions_19), value);
	}

	inline static int32_t get_offset_of__finishedActions_20() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ____finishedActions_20)); }
	inline List_1_t978411547 * get__finishedActions_20() const { return ____finishedActions_20; }
	inline List_1_t978411547 ** get_address_of__finishedActions_20() { return &____finishedActions_20; }
	inline void set__finishedActions_20(List_1_t978411547 * value)
	{
		____finishedActions_20 = value;
		Il2CppCodeGenWriteBarrier((&____finishedActions_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATE_T4124398234_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef CUSTOMLINKCONSTRAINT_T2821529372_H
#define CUSTOMLINKCONSTRAINT_T2821529372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint
struct  CustomLinkConstraint_t2821529372 
{
public:
	// System.Byte HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomLinkConstraint_t2821529372, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLINKCONSTRAINT_T2821529372_H
#ifndef CUSTOMLINKSTYLE_T99432972_H
#define CUSTOMLINKSTYLE_T99432972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTransition/CustomLinkStyle
struct  CustomLinkStyle_t99432972 
{
public:
	// System.Byte HutongGames.PlayMaker.FsmTransition/CustomLinkStyle::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomLinkStyle_t99432972, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLINKSTYLE_T99432972_H
#ifndef TRIGGER2DTYPE_T3885175869_H
#define TRIGGER2DTYPE_T3885175869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Trigger2DType
struct  Trigger2DType_t3885175869 
{
public:
	// System.Int32 HutongGames.PlayMaker.Trigger2DType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger2DType_t3885175869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER2DTYPE_T3885175869_H
#ifndef TRIGGERTYPE_T1545777084_H
#define TRIGGERTYPE_T1545777084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TriggerType
struct  TriggerType_t1545777084 
{
public:
	// System.Int32 HutongGames.PlayMaker.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t1545777084, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T1545777084_H
#ifndef UIHINT_T1096202973_H
#define UIHINT_T1096202973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UIHint
struct  UIHint_t1096202973 
{
public:
	// System.Int32 HutongGames.PlayMaker.UIHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UIHint_t1096202973, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIHINT_T1096202973_H
#ifndef UIEVENTS_T57509442_H
#define UIEVENTS_T57509442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UiEvents
struct  UiEvents_t57509442 
{
public:
	// System.Int32 HutongGames.PlayMaker.UiEvents::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UiEvents_t57509442, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTS_T57509442_H
#ifndef VARIABLETYPE_T1723760892_H
#define VARIABLETYPE_T1723760892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableType
struct  VariableType_t1723760892 
{
public:
	// System.Int32 HutongGames.PlayMaker.VariableType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VariableType_t1723760892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPE_T1723760892_H
#ifndef VARIABLETYPENICIFIED_T4087325594_H
#define VARIABLETYPENICIFIED_T4087325594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableTypeNicified
struct  VariableTypeNicified_t4087325594 
{
public:
	// System.Int32 HutongGames.PlayMaker.VariableTypeNicified::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VariableTypeNicified_t4087325594, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPENICIFIED_T4087325594_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef MATCH_T3408321083_H
#define MATCH_T3408321083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t3408321083  : public Group_t2468205786
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::regex
	Regex_t3657309853 * ___regex_6;
	// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.Match::machine
	RuntimeObject* ___machine_7;
	// System.Int32 System.Text.RegularExpressions.Match::text_length
	int32_t ___text_length_8;
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::groups
	GroupCollection_t69770484 * ___groups_9;

public:
	inline static int32_t get_offset_of_regex_6() { return static_cast<int32_t>(offsetof(Match_t3408321083, ___regex_6)); }
	inline Regex_t3657309853 * get_regex_6() const { return ___regex_6; }
	inline Regex_t3657309853 ** get_address_of_regex_6() { return &___regex_6; }
	inline void set_regex_6(Regex_t3657309853 * value)
	{
		___regex_6 = value;
		Il2CppCodeGenWriteBarrier((&___regex_6), value);
	}

	inline static int32_t get_offset_of_machine_7() { return static_cast<int32_t>(offsetof(Match_t3408321083, ___machine_7)); }
	inline RuntimeObject* get_machine_7() const { return ___machine_7; }
	inline RuntimeObject** get_address_of_machine_7() { return &___machine_7; }
	inline void set_machine_7(RuntimeObject* value)
	{
		___machine_7 = value;
		Il2CppCodeGenWriteBarrier((&___machine_7), value);
	}

	inline static int32_t get_offset_of_text_length_8() { return static_cast<int32_t>(offsetof(Match_t3408321083, ___text_length_8)); }
	inline int32_t get_text_length_8() const { return ___text_length_8; }
	inline int32_t* get_address_of_text_length_8() { return &___text_length_8; }
	inline void set_text_length_8(int32_t value)
	{
		___text_length_8 = value;
	}

	inline static int32_t get_offset_of_groups_9() { return static_cast<int32_t>(offsetof(Match_t3408321083, ___groups_9)); }
	inline GroupCollection_t69770484 * get_groups_9() const { return ___groups_9; }
	inline GroupCollection_t69770484 ** get_address_of_groups_9() { return &___groups_9; }
	inline void set_groups_9(GroupCollection_t69770484 * value)
	{
		___groups_9 = value;
		Il2CppCodeGenWriteBarrier((&___groups_9), value);
	}
};

struct Match_t3408321083_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::empty
	Match_t3408321083 * ___empty_10;

public:
	inline static int32_t get_offset_of_empty_10() { return static_cast<int32_t>(offsetof(Match_t3408321083_StaticFields, ___empty_10)); }
	inline Match_t3408321083 * get_empty_10() const { return ___empty_10; }
	inline Match_t3408321083 ** get_address_of_empty_10() { return &___empty_10; }
	inline void set_empty_10(Match_t3408321083 * value)
	{
		___empty_10 = value;
		Il2CppCodeGenWriteBarrier((&___empty_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T3408321083_H
#ifndef COLLISION_T4262080450_H
#define COLLISION_T4262080450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision
struct  Collision_t4262080450  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityEngine.Collision::m_Impulse
	Vector3_t3722313464  ___m_Impulse_0;
	// UnityEngine.Vector3 UnityEngine.Collision::m_RelativeVelocity
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	// UnityEngine.Rigidbody UnityEngine.Collision::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	// UnityEngine.Collider UnityEngine.Collision::m_Collider
	Collider_t1773347010 * ___m_Collider_3;
	// UnityEngine.ContactPoint[] UnityEngine.Collision::m_Contacts
	ContactPointU5BU5D_t872956888* ___m_Contacts_4;

public:
	inline static int32_t get_offset_of_m_Impulse_0() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Impulse_0)); }
	inline Vector3_t3722313464  get_m_Impulse_0() const { return ___m_Impulse_0; }
	inline Vector3_t3722313464 * get_address_of_m_Impulse_0() { return &___m_Impulse_0; }
	inline void set_m_Impulse_0(Vector3_t3722313464  value)
	{
		___m_Impulse_0 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_1() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_RelativeVelocity_1)); }
	inline Vector3_t3722313464  get_m_RelativeVelocity_1() const { return ___m_RelativeVelocity_1; }
	inline Vector3_t3722313464 * get_address_of_m_RelativeVelocity_1() { return &___m_RelativeVelocity_1; }
	inline void set_m_RelativeVelocity_1(Vector3_t3722313464  value)
	{
		___m_RelativeVelocity_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Rigidbody_2)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_2), value);
	}

	inline static int32_t get_offset_of_m_Collider_3() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Collider_3)); }
	inline Collider_t1773347010 * get_m_Collider_3() const { return ___m_Collider_3; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_3() { return &___m_Collider_3; }
	inline void set_m_Collider_3(Collider_t1773347010 * value)
	{
		___m_Collider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_3), value);
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision_t4262080450, ___m_Contacts_4)); }
	inline ContactPointU5BU5D_t872956888* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPointU5BU5D_t872956888** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPointU5BU5D_t872956888* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
// Native definition for COM marshalling of UnityEngine.Collision
struct Collision_t4262080450_marshaled_com
{
	Vector3_t3722313464  ___m_Impulse_0;
	Vector3_t3722313464  ___m_RelativeVelocity_1;
	Rigidbody_t3916780224 * ___m_Rigidbody_2;
	Collider_t1773347010 * ___m_Collider_3;
	ContactPoint_t3758755253 * ___m_Contacts_4;
};
#endif // COLLISION_T4262080450_H
#ifndef CONTACTPOINT2D_T3390240644_H
#define CONTACTPOINT2D_T3390240644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContactPoint2D
struct  ContactPoint2D_t3390240644 
{
public:
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_t2156229523  ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_t2156229523  ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_t2156229523  ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Point_0)); }
	inline Vector2_t2156229523  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector2_t2156229523 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector2_t2156229523  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Normal_1)); }
	inline Vector2_t2156229523  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector2_t2156229523  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_2() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_RelativeVelocity_2)); }
	inline Vector2_t2156229523  get_m_RelativeVelocity_2() const { return ___m_RelativeVelocity_2; }
	inline Vector2_t2156229523 * get_address_of_m_RelativeVelocity_2() { return &___m_RelativeVelocity_2; }
	inline void set_m_RelativeVelocity_2(Vector2_t2156229523  value)
	{
		___m_RelativeVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_Separation_3() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Separation_3)); }
	inline float get_m_Separation_3() const { return ___m_Separation_3; }
	inline float* get_address_of_m_Separation_3() { return &___m_Separation_3; }
	inline void set_m_Separation_3(float value)
	{
		___m_Separation_3 = value;
	}

	inline static int32_t get_offset_of_m_NormalImpulse_4() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_NormalImpulse_4)); }
	inline float get_m_NormalImpulse_4() const { return ___m_NormalImpulse_4; }
	inline float* get_address_of_m_NormalImpulse_4() { return &___m_NormalImpulse_4; }
	inline void set_m_NormalImpulse_4(float value)
	{
		___m_NormalImpulse_4 = value;
	}

	inline static int32_t get_offset_of_m_TangentImpulse_5() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_TangentImpulse_5)); }
	inline float get_m_TangentImpulse_5() const { return ___m_TangentImpulse_5; }
	inline float* get_address_of_m_TangentImpulse_5() { return &___m_TangentImpulse_5; }
	inline void set_m_TangentImpulse_5(float value)
	{
		___m_TangentImpulse_5 = value;
	}

	inline static int32_t get_offset_of_m_Collider_6() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Collider_6)); }
	inline int32_t get_m_Collider_6() const { return ___m_Collider_6; }
	inline int32_t* get_address_of_m_Collider_6() { return &___m_Collider_6; }
	inline void set_m_Collider_6(int32_t value)
	{
		___m_Collider_6 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_7() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_OtherCollider_7)); }
	inline int32_t get_m_OtherCollider_7() const { return ___m_OtherCollider_7; }
	inline int32_t* get_address_of_m_OtherCollider_7() { return &___m_OtherCollider_7; }
	inline void set_m_OtherCollider_7(int32_t value)
	{
		___m_OtherCollider_7 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_8() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Rigidbody_8)); }
	inline int32_t get_m_Rigidbody_8() const { return ___m_Rigidbody_8; }
	inline int32_t* get_address_of_m_Rigidbody_8() { return &___m_Rigidbody_8; }
	inline void set_m_Rigidbody_8(int32_t value)
	{
		___m_Rigidbody_8 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_9() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_OtherRigidbody_9)); }
	inline int32_t get_m_OtherRigidbody_9() const { return ___m_OtherRigidbody_9; }
	inline int32_t* get_address_of_m_OtherRigidbody_9() { return &___m_OtherRigidbody_9; }
	inline void set_m_OtherRigidbody_9(int32_t value)
	{
		___m_OtherRigidbody_9 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_10() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Enabled_10)); }
	inline int32_t get_m_Enabled_10() const { return ___m_Enabled_10; }
	inline int32_t* get_address_of_m_Enabled_10() { return &___m_Enabled_10; }
	inline void set_m_Enabled_10(int32_t value)
	{
		___m_Enabled_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTPOINT2D_T3390240644_H
#ifndef CONTROLLERCOLLIDERHIT_T240592346_H
#define CONTROLLERCOLLIDERHIT_T240592346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ControllerColliderHit
struct  ControllerColliderHit_t240592346  : public RuntimeObject
{
public:
	// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::m_Controller
	CharacterController_t1138636865 * ___m_Controller_0;
	// UnityEngine.Collider UnityEngine.ControllerColliderHit::m_Collider
	Collider_t1773347010 * ___m_Collider_1;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Point
	Vector3_t3722313464  ___m_Point_2;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_Normal
	Vector3_t3722313464  ___m_Normal_3;
	// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::m_MoveDirection
	Vector3_t3722313464  ___m_MoveDirection_4;
	// System.Single UnityEngine.ControllerColliderHit::m_MoveLength
	float ___m_MoveLength_5;
	// System.Int32 UnityEngine.ControllerColliderHit::m_Push
	int32_t ___m_Push_6;

public:
	inline static int32_t get_offset_of_m_Controller_0() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Controller_0)); }
	inline CharacterController_t1138636865 * get_m_Controller_0() const { return ___m_Controller_0; }
	inline CharacterController_t1138636865 ** get_address_of_m_Controller_0() { return &___m_Controller_0; }
	inline void set_m_Controller_0(CharacterController_t1138636865 * value)
	{
		___m_Controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Controller_0), value);
	}

	inline static int32_t get_offset_of_m_Collider_1() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Collider_1)); }
	inline Collider_t1773347010 * get_m_Collider_1() const { return ___m_Collider_1; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_1() { return &___m_Collider_1; }
	inline void set_m_Collider_1(Collider_t1773347010 * value)
	{
		___m_Collider_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_1), value);
	}

	inline static int32_t get_offset_of_m_Point_2() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Point_2)); }
	inline Vector3_t3722313464  get_m_Point_2() const { return ___m_Point_2; }
	inline Vector3_t3722313464 * get_address_of_m_Point_2() { return &___m_Point_2; }
	inline void set_m_Point_2(Vector3_t3722313464  value)
	{
		___m_Point_2 = value;
	}

	inline static int32_t get_offset_of_m_Normal_3() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Normal_3)); }
	inline Vector3_t3722313464  get_m_Normal_3() const { return ___m_Normal_3; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_3() { return &___m_Normal_3; }
	inline void set_m_Normal_3(Vector3_t3722313464  value)
	{
		___m_Normal_3 = value;
	}

	inline static int32_t get_offset_of_m_MoveDirection_4() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveDirection_4)); }
	inline Vector3_t3722313464  get_m_MoveDirection_4() const { return ___m_MoveDirection_4; }
	inline Vector3_t3722313464 * get_address_of_m_MoveDirection_4() { return &___m_MoveDirection_4; }
	inline void set_m_MoveDirection_4(Vector3_t3722313464  value)
	{
		___m_MoveDirection_4 = value;
	}

	inline static int32_t get_offset_of_m_MoveLength_5() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_MoveLength_5)); }
	inline float get_m_MoveLength_5() const { return ___m_MoveLength_5; }
	inline float* get_address_of_m_MoveLength_5() { return &___m_MoveLength_5; }
	inline void set_m_MoveLength_5(float value)
	{
		___m_MoveLength_5 = value;
	}

	inline static int32_t get_offset_of_m_Push_6() { return static_cast<int32_t>(offsetof(ControllerColliderHit_t240592346, ___m_Push_6)); }
	inline int32_t get_m_Push_6() const { return ___m_Push_6; }
	inline int32_t* get_address_of_m_Push_6() { return &___m_Push_6; }
	inline void set_m_Push_6(int32_t value)
	{
		___m_Push_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_pinvoke
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
// Native definition for COM marshalling of UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346_marshaled_com
{
	CharacterController_t1138636865 * ___m_Controller_0;
	Collider_t1773347010 * ___m_Collider_1;
	Vector3_t3722313464  ___m_Point_2;
	Vector3_t3722313464  ___m_Normal_3;
	Vector3_t3722313464  ___m_MoveDirection_4;
	float ___m_MoveLength_5;
	int32_t ___m_Push_6;
};
#endif // CONTROLLERCOLLIDERHIT_T240592346_H
#ifndef CURSORLOCKMODE_T2840764040_H
#define CURSORLOCKMODE_T2840764040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CursorLockMode
struct  CursorLockMode_t2840764040 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CursorLockMode_t2840764040, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORLOCKMODE_T2840764040_H
#ifndef EVENT_T2956885303_H
#define EVENT_T2956885303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Event
struct  Event_t2956885303  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Event::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Event_t2956885303, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

struct Event_t2956885303_StaticFields
{
public:
	// UnityEngine.Event UnityEngine.Event::s_Current
	Event_t2956885303 * ___s_Current_1;
	// UnityEngine.Event UnityEngine.Event::s_MasterEvent
	Event_t2956885303 * ___s_MasterEvent_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnityEngine.Event::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_3;

public:
	inline static int32_t get_offset_of_s_Current_1() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___s_Current_1)); }
	inline Event_t2956885303 * get_s_Current_1() const { return ___s_Current_1; }
	inline Event_t2956885303 ** get_address_of_s_Current_1() { return &___s_Current_1; }
	inline void set_s_Current_1(Event_t2956885303 * value)
	{
		___s_Current_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Current_1), value);
	}

	inline static int32_t get_offset_of_s_MasterEvent_2() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___s_MasterEvent_2)); }
	inline Event_t2956885303 * get_s_MasterEvent_2() const { return ___s_MasterEvent_2; }
	inline Event_t2956885303 ** get_address_of_s_MasterEvent_2() { return &___s_MasterEvent_2; }
	inline void set_s_MasterEvent_2(Event_t2956885303 * value)
	{
		___s_MasterEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_MasterEvent_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_3() { return static_cast<int32_t>(offsetof(Event_t2956885303_StaticFields, ___U3CU3Ef__switchU24map0_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_3() const { return ___U3CU3Ef__switchU24map0_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_3() { return &___U3CU3Ef__switchU24map0_3; }
	inline void set_U3CU3Ef__switchU24map0_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Event
struct Event_t2956885303_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Event
struct Event_t2956885303_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // EVENT_T2956885303_H
#ifndef EVENTTYPE_T3528516131_H
#define EVENTTYPE_T3528516131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventType
struct  EventType_t3528516131 
{
public:
	// System.Int32 UnityEngine.EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t3528516131, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T3528516131_H
#ifndef GUISTYLESTATE_T1397964415_H
#define GUISTYLESTATE_T1397964415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyleState
struct  GUIStyleState_t1397964415  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t3956901511 * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_SourceStyle_1)); }
	inline GUIStyle_t3956901511 * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline GUIStyle_t3956901511 ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(GUIStyle_t3956901511 * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t3956901511_marshaled_pinvoke* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t3956901511_marshaled_com* ___m_SourceStyle_1;
};
#endif // GUISTYLESTATE_T1397964415_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef RECTOFFSET_T1369453676_H
#define RECTOFFSET_T1369453676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1369453676  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1369453676_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef FSM_T4127147824_H
#define FSM_T4127147824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Fsm
struct  Fsm_t4127147824  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo HutongGames.PlayMaker.Fsm::updateHelperSetDirty
	MethodInfo_t * ___updateHelperSetDirty_2;
	// System.Int32 HutongGames.PlayMaker.Fsm::dataVersion
	int32_t ___dataVersion_7;
	// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Fsm::owner
	MonoBehaviour_t3962482529 * ___owner_8;
	// FsmTemplate HutongGames.PlayMaker.Fsm::usedInTemplate
	FsmTemplate_t1663266674 * ___usedInTemplate_9;
	// System.String HutongGames.PlayMaker.Fsm::name
	String_t* ___name_10;
	// System.String HutongGames.PlayMaker.Fsm::startState
	String_t* ___startState_11;
	// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::states
	FsmStateU5BU5D_t3458670015* ___states_12;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::events
	FsmEventU5BU5D_t2511618479* ___events_13;
	// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::globalTransitions
	FsmTransitionU5BU5D_t3577734832* ___globalTransitions_14;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::variables
	FsmVariables_t3349589544 * ___variables_15;
	// System.String HutongGames.PlayMaker.Fsm::description
	String_t* ___description_16;
	// System.String HutongGames.PlayMaker.Fsm::docUrl
	String_t* ___docUrl_17;
	// System.Boolean HutongGames.PlayMaker.Fsm::showStateLabel
	bool ___showStateLabel_18;
	// System.Int32 HutongGames.PlayMaker.Fsm::maxLoopCount
	int32_t ___maxLoopCount_19;
	// System.String HutongGames.PlayMaker.Fsm::watermark
	String_t* ___watermark_20;
	// System.String HutongGames.PlayMaker.Fsm::password
	String_t* ___password_21;
	// System.Boolean HutongGames.PlayMaker.Fsm::locked
	bool ___locked_22;
	// System.Boolean HutongGames.PlayMaker.Fsm::manualUpdate
	bool ___manualUpdate_23;
	// System.Boolean HutongGames.PlayMaker.Fsm::keepDelayedEventsOnStateExit
	bool ___keepDelayedEventsOnStateExit_24;
	// System.Boolean HutongGames.PlayMaker.Fsm::preprocessed
	bool ___preprocessed_25;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::host
	Fsm_t4127147824 * ___host_26;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::rootFsm
	Fsm_t4127147824 * ___rootFsm_27;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::subFsmList
	List_1_t1304255270 * ___subFsmList_28;
	// System.Boolean HutongGames.PlayMaker.Fsm::setDirty
	bool ___setDirty_29;
	// System.Boolean HutongGames.PlayMaker.Fsm::activeStateEntered
	bool ___activeStateEntered_30;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.Fsm::ExposedEvents
	List_1_t913407328 * ___ExposedEvents_31;
	// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.Fsm::myLog
	FsmLog_t3265252880 * ___myLog_32;
	// System.Boolean HutongGames.PlayMaker.Fsm::RestartOnEnable
	bool ___RestartOnEnable_33;
	// System.Boolean HutongGames.PlayMaker.Fsm::<Started>k__BackingField
	bool ___U3CStartedU3Ek__BackingField_34;
	// System.Boolean HutongGames.PlayMaker.Fsm::EnableDebugFlow
	bool ___EnableDebugFlow_35;
	// System.Boolean HutongGames.PlayMaker.Fsm::EnableBreakpoints
	bool ___EnableBreakpoints_36;
	// System.Boolean HutongGames.PlayMaker.Fsm::StepFrame
	bool ___StepFrame_37;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::delayedEvents
	List_1_t1164733674 * ___delayedEvents_38;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::updateEvents
	List_1_t1164733674 * ___updateEvents_39;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::removeEvents
	List_1_t1164733674 * ___removeEvents_40;
	// HutongGames.PlayMaker.Fsm/EditorFlags HutongGames.PlayMaker.Fsm::editorFlags
	int32_t ___editorFlags_41;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::<EventTarget>k__BackingField
	FsmEventTarget_t919699796 * ___U3CEventTargetU3Ek__BackingField_42;
	// System.Boolean HutongGames.PlayMaker.Fsm::initialized
	bool ___initialized_43;
	// System.Boolean HutongGames.PlayMaker.Fsm::<Finished>k__BackingField
	bool ___U3CFinishedU3Ek__BackingField_44;
	// System.String HutongGames.PlayMaker.Fsm::activeStateName
	String_t* ___activeStateName_45;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::activeState
	FsmState_t4124398234 * ___activeState_46;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::switchToState
	FsmState_t4124398234 * ___switchToState_47;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::previousActiveState
	FsmState_t4124398234 * ___previousActiveState_48;
	// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.Fsm::<LastTransition>k__BackingField
	FsmTransition_t1340699069 * ___U3CLastTransitionU3Ek__BackingField_49;
	// System.Action`1<HutongGames.PlayMaker.FsmState> HutongGames.PlayMaker.Fsm::StateChanged
	Action_1_t1898533 * ___StateChanged_50;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsModifiedPrefabInstance>k__BackingField
	bool ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::editState
	FsmState_t4124398234 * ___editState_53;
	// System.Boolean HutongGames.PlayMaker.Fsm::<SwitchedState>k__BackingField
	bool ___U3CSwitchedStateU3Ek__BackingField_64;
	// System.Boolean HutongGames.PlayMaker.Fsm::mouseEvents
	bool ___mouseEvents_65;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleLevelLoaded
	bool ___handleLevelLoaded_66;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerEnter2D
	bool ___handleTriggerEnter2D_67;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerExit2D
	bool ___handleTriggerExit2D_68;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerStay2D
	bool ___handleTriggerStay2D_69;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionEnter2D
	bool ___handleCollisionEnter2D_70;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionExit2D
	bool ___handleCollisionExit2D_71;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionStay2D
	bool ___handleCollisionStay2D_72;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerEnter
	bool ___handleTriggerEnter_73;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerExit
	bool ___handleTriggerExit_74;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerStay
	bool ___handleTriggerStay_75;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionEnter
	bool ___handleCollisionEnter_76;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionExit
	bool ___handleCollisionExit_77;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionStay
	bool ___handleCollisionStay_78;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleParticleCollision
	bool ___handleParticleCollision_79;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleControllerColliderHit
	bool ___handleControllerColliderHit_80;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleJointBreak
	bool ___handleJointBreak_81;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleJointBreak2D
	bool ___handleJointBreak2D_82;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleOnGUI
	bool ___handleOnGUI_83;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleFixedUpdate
	bool ___handleFixedUpdate_84;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleLateUpdate
	bool ___handleLateUpdate_85;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleApplicationEvents
	bool ___handleApplicationEvents_86;
	// HutongGames.PlayMaker.UiEvents HutongGames.PlayMaker.Fsm::handleUiEvents
	int32_t ___handleUiEvents_87;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleLegacyNetworking
	bool ___handleLegacyNetworking_88;
	// UnityEngine.Collision HutongGames.PlayMaker.Fsm::<CollisionInfo>k__BackingField
	Collision_t4262080450 * ___U3CCollisionInfoU3Ek__BackingField_89;
	// UnityEngine.Collider HutongGames.PlayMaker.Fsm::<TriggerCollider>k__BackingField
	Collider_t1773347010 * ___U3CTriggerColliderU3Ek__BackingField_90;
	// UnityEngine.Collision2D HutongGames.PlayMaker.Fsm::<Collision2DInfo>k__BackingField
	Collision2D_t2842956331 * ___U3CCollision2DInfoU3Ek__BackingField_91;
	// UnityEngine.Collider2D HutongGames.PlayMaker.Fsm::<TriggerCollider2D>k__BackingField
	Collider2D_t2806799626 * ___U3CTriggerCollider2DU3Ek__BackingField_92;
	// System.Single HutongGames.PlayMaker.Fsm::<JointBreakForce>k__BackingField
	float ___U3CJointBreakForceU3Ek__BackingField_93;
	// UnityEngine.Joint2D HutongGames.PlayMaker.Fsm::<BrokenJoint2D>k__BackingField
	Joint2D_t4180440564 * ___U3CBrokenJoint2DU3Ek__BackingField_94;
	// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::<ParticleCollisionGO>k__BackingField
	GameObject_t1113636619 * ___U3CParticleCollisionGOU3Ek__BackingField_95;
	// System.String HutongGames.PlayMaker.Fsm::<TriggerName>k__BackingField
	String_t* ___U3CTriggerNameU3Ek__BackingField_96;
	// System.String HutongGames.PlayMaker.Fsm::<CollisionName>k__BackingField
	String_t* ___U3CCollisionNameU3Ek__BackingField_97;
	// System.String HutongGames.PlayMaker.Fsm::<Trigger2dName>k__BackingField
	String_t* ___U3CTrigger2dNameU3Ek__BackingField_98;
	// System.String HutongGames.PlayMaker.Fsm::<Collision2dName>k__BackingField
	String_t* ___U3CCollision2dNameU3Ek__BackingField_99;
	// UnityEngine.ControllerColliderHit HutongGames.PlayMaker.Fsm::<ControllerCollider>k__BackingField
	ControllerColliderHit_t240592346 * ___U3CControllerColliderU3Ek__BackingField_100;
	// UnityEngine.RaycastHit HutongGames.PlayMaker.Fsm::<RaycastHitInfo>k__BackingField
	RaycastHit_t1056001966  ___U3CRaycastHitInfoU3Ek__BackingField_101;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleAnimatorMove
	bool ___handleAnimatorMove_103;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleAnimatorIK
	bool ___handleAnimatorIK_104;

public:
	inline static int32_t get_offset_of_updateHelperSetDirty_2() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___updateHelperSetDirty_2)); }
	inline MethodInfo_t * get_updateHelperSetDirty_2() const { return ___updateHelperSetDirty_2; }
	inline MethodInfo_t ** get_address_of_updateHelperSetDirty_2() { return &___updateHelperSetDirty_2; }
	inline void set_updateHelperSetDirty_2(MethodInfo_t * value)
	{
		___updateHelperSetDirty_2 = value;
		Il2CppCodeGenWriteBarrier((&___updateHelperSetDirty_2), value);
	}

	inline static int32_t get_offset_of_dataVersion_7() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___dataVersion_7)); }
	inline int32_t get_dataVersion_7() const { return ___dataVersion_7; }
	inline int32_t* get_address_of_dataVersion_7() { return &___dataVersion_7; }
	inline void set_dataVersion_7(int32_t value)
	{
		___dataVersion_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___owner_8)); }
	inline MonoBehaviour_t3962482529 * get_owner_8() const { return ___owner_8; }
	inline MonoBehaviour_t3962482529 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(MonoBehaviour_t3962482529 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_usedInTemplate_9() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___usedInTemplate_9)); }
	inline FsmTemplate_t1663266674 * get_usedInTemplate_9() const { return ___usedInTemplate_9; }
	inline FsmTemplate_t1663266674 ** get_address_of_usedInTemplate_9() { return &___usedInTemplate_9; }
	inline void set_usedInTemplate_9(FsmTemplate_t1663266674 * value)
	{
		___usedInTemplate_9 = value;
		Il2CppCodeGenWriteBarrier((&___usedInTemplate_9), value);
	}

	inline static int32_t get_offset_of_name_10() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___name_10)); }
	inline String_t* get_name_10() const { return ___name_10; }
	inline String_t** get_address_of_name_10() { return &___name_10; }
	inline void set_name_10(String_t* value)
	{
		___name_10 = value;
		Il2CppCodeGenWriteBarrier((&___name_10), value);
	}

	inline static int32_t get_offset_of_startState_11() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___startState_11)); }
	inline String_t* get_startState_11() const { return ___startState_11; }
	inline String_t** get_address_of_startState_11() { return &___startState_11; }
	inline void set_startState_11(String_t* value)
	{
		___startState_11 = value;
		Il2CppCodeGenWriteBarrier((&___startState_11), value);
	}

	inline static int32_t get_offset_of_states_12() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___states_12)); }
	inline FsmStateU5BU5D_t3458670015* get_states_12() const { return ___states_12; }
	inline FsmStateU5BU5D_t3458670015** get_address_of_states_12() { return &___states_12; }
	inline void set_states_12(FsmStateU5BU5D_t3458670015* value)
	{
		___states_12 = value;
		Il2CppCodeGenWriteBarrier((&___states_12), value);
	}

	inline static int32_t get_offset_of_events_13() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___events_13)); }
	inline FsmEventU5BU5D_t2511618479* get_events_13() const { return ___events_13; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_events_13() { return &___events_13; }
	inline void set_events_13(FsmEventU5BU5D_t2511618479* value)
	{
		___events_13 = value;
		Il2CppCodeGenWriteBarrier((&___events_13), value);
	}

	inline static int32_t get_offset_of_globalTransitions_14() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___globalTransitions_14)); }
	inline FsmTransitionU5BU5D_t3577734832* get_globalTransitions_14() const { return ___globalTransitions_14; }
	inline FsmTransitionU5BU5D_t3577734832** get_address_of_globalTransitions_14() { return &___globalTransitions_14; }
	inline void set_globalTransitions_14(FsmTransitionU5BU5D_t3577734832* value)
	{
		___globalTransitions_14 = value;
		Il2CppCodeGenWriteBarrier((&___globalTransitions_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___variables_15)); }
	inline FsmVariables_t3349589544 * get_variables_15() const { return ___variables_15; }
	inline FsmVariables_t3349589544 ** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmVariables_t3349589544 * value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}

	inline static int32_t get_offset_of_description_16() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___description_16)); }
	inline String_t* get_description_16() const { return ___description_16; }
	inline String_t** get_address_of_description_16() { return &___description_16; }
	inline void set_description_16(String_t* value)
	{
		___description_16 = value;
		Il2CppCodeGenWriteBarrier((&___description_16), value);
	}

	inline static int32_t get_offset_of_docUrl_17() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___docUrl_17)); }
	inline String_t* get_docUrl_17() const { return ___docUrl_17; }
	inline String_t** get_address_of_docUrl_17() { return &___docUrl_17; }
	inline void set_docUrl_17(String_t* value)
	{
		___docUrl_17 = value;
		Il2CppCodeGenWriteBarrier((&___docUrl_17), value);
	}

	inline static int32_t get_offset_of_showStateLabel_18() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___showStateLabel_18)); }
	inline bool get_showStateLabel_18() const { return ___showStateLabel_18; }
	inline bool* get_address_of_showStateLabel_18() { return &___showStateLabel_18; }
	inline void set_showStateLabel_18(bool value)
	{
		___showStateLabel_18 = value;
	}

	inline static int32_t get_offset_of_maxLoopCount_19() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___maxLoopCount_19)); }
	inline int32_t get_maxLoopCount_19() const { return ___maxLoopCount_19; }
	inline int32_t* get_address_of_maxLoopCount_19() { return &___maxLoopCount_19; }
	inline void set_maxLoopCount_19(int32_t value)
	{
		___maxLoopCount_19 = value;
	}

	inline static int32_t get_offset_of_watermark_20() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___watermark_20)); }
	inline String_t* get_watermark_20() const { return ___watermark_20; }
	inline String_t** get_address_of_watermark_20() { return &___watermark_20; }
	inline void set_watermark_20(String_t* value)
	{
		___watermark_20 = value;
		Il2CppCodeGenWriteBarrier((&___watermark_20), value);
	}

	inline static int32_t get_offset_of_password_21() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___password_21)); }
	inline String_t* get_password_21() const { return ___password_21; }
	inline String_t** get_address_of_password_21() { return &___password_21; }
	inline void set_password_21(String_t* value)
	{
		___password_21 = value;
		Il2CppCodeGenWriteBarrier((&___password_21), value);
	}

	inline static int32_t get_offset_of_locked_22() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___locked_22)); }
	inline bool get_locked_22() const { return ___locked_22; }
	inline bool* get_address_of_locked_22() { return &___locked_22; }
	inline void set_locked_22(bool value)
	{
		___locked_22 = value;
	}

	inline static int32_t get_offset_of_manualUpdate_23() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___manualUpdate_23)); }
	inline bool get_manualUpdate_23() const { return ___manualUpdate_23; }
	inline bool* get_address_of_manualUpdate_23() { return &___manualUpdate_23; }
	inline void set_manualUpdate_23(bool value)
	{
		___manualUpdate_23 = value;
	}

	inline static int32_t get_offset_of_keepDelayedEventsOnStateExit_24() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___keepDelayedEventsOnStateExit_24)); }
	inline bool get_keepDelayedEventsOnStateExit_24() const { return ___keepDelayedEventsOnStateExit_24; }
	inline bool* get_address_of_keepDelayedEventsOnStateExit_24() { return &___keepDelayedEventsOnStateExit_24; }
	inline void set_keepDelayedEventsOnStateExit_24(bool value)
	{
		___keepDelayedEventsOnStateExit_24 = value;
	}

	inline static int32_t get_offset_of_preprocessed_25() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___preprocessed_25)); }
	inline bool get_preprocessed_25() const { return ___preprocessed_25; }
	inline bool* get_address_of_preprocessed_25() { return &___preprocessed_25; }
	inline void set_preprocessed_25(bool value)
	{
		___preprocessed_25 = value;
	}

	inline static int32_t get_offset_of_host_26() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___host_26)); }
	inline Fsm_t4127147824 * get_host_26() const { return ___host_26; }
	inline Fsm_t4127147824 ** get_address_of_host_26() { return &___host_26; }
	inline void set_host_26(Fsm_t4127147824 * value)
	{
		___host_26 = value;
		Il2CppCodeGenWriteBarrier((&___host_26), value);
	}

	inline static int32_t get_offset_of_rootFsm_27() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___rootFsm_27)); }
	inline Fsm_t4127147824 * get_rootFsm_27() const { return ___rootFsm_27; }
	inline Fsm_t4127147824 ** get_address_of_rootFsm_27() { return &___rootFsm_27; }
	inline void set_rootFsm_27(Fsm_t4127147824 * value)
	{
		___rootFsm_27 = value;
		Il2CppCodeGenWriteBarrier((&___rootFsm_27), value);
	}

	inline static int32_t get_offset_of_subFsmList_28() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___subFsmList_28)); }
	inline List_1_t1304255270 * get_subFsmList_28() const { return ___subFsmList_28; }
	inline List_1_t1304255270 ** get_address_of_subFsmList_28() { return &___subFsmList_28; }
	inline void set_subFsmList_28(List_1_t1304255270 * value)
	{
		___subFsmList_28 = value;
		Il2CppCodeGenWriteBarrier((&___subFsmList_28), value);
	}

	inline static int32_t get_offset_of_setDirty_29() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___setDirty_29)); }
	inline bool get_setDirty_29() const { return ___setDirty_29; }
	inline bool* get_address_of_setDirty_29() { return &___setDirty_29; }
	inline void set_setDirty_29(bool value)
	{
		___setDirty_29 = value;
	}

	inline static int32_t get_offset_of_activeStateEntered_30() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___activeStateEntered_30)); }
	inline bool get_activeStateEntered_30() const { return ___activeStateEntered_30; }
	inline bool* get_address_of_activeStateEntered_30() { return &___activeStateEntered_30; }
	inline void set_activeStateEntered_30(bool value)
	{
		___activeStateEntered_30 = value;
	}

	inline static int32_t get_offset_of_ExposedEvents_31() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___ExposedEvents_31)); }
	inline List_1_t913407328 * get_ExposedEvents_31() const { return ___ExposedEvents_31; }
	inline List_1_t913407328 ** get_address_of_ExposedEvents_31() { return &___ExposedEvents_31; }
	inline void set_ExposedEvents_31(List_1_t913407328 * value)
	{
		___ExposedEvents_31 = value;
		Il2CppCodeGenWriteBarrier((&___ExposedEvents_31), value);
	}

	inline static int32_t get_offset_of_myLog_32() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___myLog_32)); }
	inline FsmLog_t3265252880 * get_myLog_32() const { return ___myLog_32; }
	inline FsmLog_t3265252880 ** get_address_of_myLog_32() { return &___myLog_32; }
	inline void set_myLog_32(FsmLog_t3265252880 * value)
	{
		___myLog_32 = value;
		Il2CppCodeGenWriteBarrier((&___myLog_32), value);
	}

	inline static int32_t get_offset_of_RestartOnEnable_33() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___RestartOnEnable_33)); }
	inline bool get_RestartOnEnable_33() const { return ___RestartOnEnable_33; }
	inline bool* get_address_of_RestartOnEnable_33() { return &___RestartOnEnable_33; }
	inline void set_RestartOnEnable_33(bool value)
	{
		___RestartOnEnable_33 = value;
	}

	inline static int32_t get_offset_of_U3CStartedU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CStartedU3Ek__BackingField_34)); }
	inline bool get_U3CStartedU3Ek__BackingField_34() const { return ___U3CStartedU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CStartedU3Ek__BackingField_34() { return &___U3CStartedU3Ek__BackingField_34; }
	inline void set_U3CStartedU3Ek__BackingField_34(bool value)
	{
		___U3CStartedU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_EnableDebugFlow_35() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___EnableDebugFlow_35)); }
	inline bool get_EnableDebugFlow_35() const { return ___EnableDebugFlow_35; }
	inline bool* get_address_of_EnableDebugFlow_35() { return &___EnableDebugFlow_35; }
	inline void set_EnableDebugFlow_35(bool value)
	{
		___EnableDebugFlow_35 = value;
	}

	inline static int32_t get_offset_of_EnableBreakpoints_36() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___EnableBreakpoints_36)); }
	inline bool get_EnableBreakpoints_36() const { return ___EnableBreakpoints_36; }
	inline bool* get_address_of_EnableBreakpoints_36() { return &___EnableBreakpoints_36; }
	inline void set_EnableBreakpoints_36(bool value)
	{
		___EnableBreakpoints_36 = value;
	}

	inline static int32_t get_offset_of_StepFrame_37() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___StepFrame_37)); }
	inline bool get_StepFrame_37() const { return ___StepFrame_37; }
	inline bool* get_address_of_StepFrame_37() { return &___StepFrame_37; }
	inline void set_StepFrame_37(bool value)
	{
		___StepFrame_37 = value;
	}

	inline static int32_t get_offset_of_delayedEvents_38() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___delayedEvents_38)); }
	inline List_1_t1164733674 * get_delayedEvents_38() const { return ___delayedEvents_38; }
	inline List_1_t1164733674 ** get_address_of_delayedEvents_38() { return &___delayedEvents_38; }
	inline void set_delayedEvents_38(List_1_t1164733674 * value)
	{
		___delayedEvents_38 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvents_38), value);
	}

	inline static int32_t get_offset_of_updateEvents_39() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___updateEvents_39)); }
	inline List_1_t1164733674 * get_updateEvents_39() const { return ___updateEvents_39; }
	inline List_1_t1164733674 ** get_address_of_updateEvents_39() { return &___updateEvents_39; }
	inline void set_updateEvents_39(List_1_t1164733674 * value)
	{
		___updateEvents_39 = value;
		Il2CppCodeGenWriteBarrier((&___updateEvents_39), value);
	}

	inline static int32_t get_offset_of_removeEvents_40() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___removeEvents_40)); }
	inline List_1_t1164733674 * get_removeEvents_40() const { return ___removeEvents_40; }
	inline List_1_t1164733674 ** get_address_of_removeEvents_40() { return &___removeEvents_40; }
	inline void set_removeEvents_40(List_1_t1164733674 * value)
	{
		___removeEvents_40 = value;
		Il2CppCodeGenWriteBarrier((&___removeEvents_40), value);
	}

	inline static int32_t get_offset_of_editorFlags_41() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___editorFlags_41)); }
	inline int32_t get_editorFlags_41() const { return ___editorFlags_41; }
	inline int32_t* get_address_of_editorFlags_41() { return &___editorFlags_41; }
	inline void set_editorFlags_41(int32_t value)
	{
		___editorFlags_41 = value;
	}

	inline static int32_t get_offset_of_U3CEventTargetU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CEventTargetU3Ek__BackingField_42)); }
	inline FsmEventTarget_t919699796 * get_U3CEventTargetU3Ek__BackingField_42() const { return ___U3CEventTargetU3Ek__BackingField_42; }
	inline FsmEventTarget_t919699796 ** get_address_of_U3CEventTargetU3Ek__BackingField_42() { return &___U3CEventTargetU3Ek__BackingField_42; }
	inline void set_U3CEventTargetU3Ek__BackingField_42(FsmEventTarget_t919699796 * value)
	{
		___U3CEventTargetU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventTargetU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_initialized_43() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___initialized_43)); }
	inline bool get_initialized_43() const { return ___initialized_43; }
	inline bool* get_address_of_initialized_43() { return &___initialized_43; }
	inline void set_initialized_43(bool value)
	{
		___initialized_43 = value;
	}

	inline static int32_t get_offset_of_U3CFinishedU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CFinishedU3Ek__BackingField_44)); }
	inline bool get_U3CFinishedU3Ek__BackingField_44() const { return ___U3CFinishedU3Ek__BackingField_44; }
	inline bool* get_address_of_U3CFinishedU3Ek__BackingField_44() { return &___U3CFinishedU3Ek__BackingField_44; }
	inline void set_U3CFinishedU3Ek__BackingField_44(bool value)
	{
		___U3CFinishedU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_activeStateName_45() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___activeStateName_45)); }
	inline String_t* get_activeStateName_45() const { return ___activeStateName_45; }
	inline String_t** get_address_of_activeStateName_45() { return &___activeStateName_45; }
	inline void set_activeStateName_45(String_t* value)
	{
		___activeStateName_45 = value;
		Il2CppCodeGenWriteBarrier((&___activeStateName_45), value);
	}

	inline static int32_t get_offset_of_activeState_46() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___activeState_46)); }
	inline FsmState_t4124398234 * get_activeState_46() const { return ___activeState_46; }
	inline FsmState_t4124398234 ** get_address_of_activeState_46() { return &___activeState_46; }
	inline void set_activeState_46(FsmState_t4124398234 * value)
	{
		___activeState_46 = value;
		Il2CppCodeGenWriteBarrier((&___activeState_46), value);
	}

	inline static int32_t get_offset_of_switchToState_47() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___switchToState_47)); }
	inline FsmState_t4124398234 * get_switchToState_47() const { return ___switchToState_47; }
	inline FsmState_t4124398234 ** get_address_of_switchToState_47() { return &___switchToState_47; }
	inline void set_switchToState_47(FsmState_t4124398234 * value)
	{
		___switchToState_47 = value;
		Il2CppCodeGenWriteBarrier((&___switchToState_47), value);
	}

	inline static int32_t get_offset_of_previousActiveState_48() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___previousActiveState_48)); }
	inline FsmState_t4124398234 * get_previousActiveState_48() const { return ___previousActiveState_48; }
	inline FsmState_t4124398234 ** get_address_of_previousActiveState_48() { return &___previousActiveState_48; }
	inline void set_previousActiveState_48(FsmState_t4124398234 * value)
	{
		___previousActiveState_48 = value;
		Il2CppCodeGenWriteBarrier((&___previousActiveState_48), value);
	}

	inline static int32_t get_offset_of_U3CLastTransitionU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CLastTransitionU3Ek__BackingField_49)); }
	inline FsmTransition_t1340699069 * get_U3CLastTransitionU3Ek__BackingField_49() const { return ___U3CLastTransitionU3Ek__BackingField_49; }
	inline FsmTransition_t1340699069 ** get_address_of_U3CLastTransitionU3Ek__BackingField_49() { return &___U3CLastTransitionU3Ek__BackingField_49; }
	inline void set_U3CLastTransitionU3Ek__BackingField_49(FsmTransition_t1340699069 * value)
	{
		___U3CLastTransitionU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastTransitionU3Ek__BackingField_49), value);
	}

	inline static int32_t get_offset_of_StateChanged_50() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___StateChanged_50)); }
	inline Action_1_t1898533 * get_StateChanged_50() const { return ___StateChanged_50; }
	inline Action_1_t1898533 ** get_address_of_StateChanged_50() { return &___StateChanged_50; }
	inline void set_StateChanged_50(Action_1_t1898533 * value)
	{
		___StateChanged_50 = value;
		Il2CppCodeGenWriteBarrier((&___StateChanged_50), value);
	}

	inline static int32_t get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51)); }
	inline bool get_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51() const { return ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51; }
	inline bool* get_address_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51() { return &___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51; }
	inline void set_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51(bool value)
	{
		___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_editState_53() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___editState_53)); }
	inline FsmState_t4124398234 * get_editState_53() const { return ___editState_53; }
	inline FsmState_t4124398234 ** get_address_of_editState_53() { return &___editState_53; }
	inline void set_editState_53(FsmState_t4124398234 * value)
	{
		___editState_53 = value;
		Il2CppCodeGenWriteBarrier((&___editState_53), value);
	}

	inline static int32_t get_offset_of_U3CSwitchedStateU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CSwitchedStateU3Ek__BackingField_64)); }
	inline bool get_U3CSwitchedStateU3Ek__BackingField_64() const { return ___U3CSwitchedStateU3Ek__BackingField_64; }
	inline bool* get_address_of_U3CSwitchedStateU3Ek__BackingField_64() { return &___U3CSwitchedStateU3Ek__BackingField_64; }
	inline void set_U3CSwitchedStateU3Ek__BackingField_64(bool value)
	{
		___U3CSwitchedStateU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_mouseEvents_65() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___mouseEvents_65)); }
	inline bool get_mouseEvents_65() const { return ___mouseEvents_65; }
	inline bool* get_address_of_mouseEvents_65() { return &___mouseEvents_65; }
	inline void set_mouseEvents_65(bool value)
	{
		___mouseEvents_65 = value;
	}

	inline static int32_t get_offset_of_handleLevelLoaded_66() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleLevelLoaded_66)); }
	inline bool get_handleLevelLoaded_66() const { return ___handleLevelLoaded_66; }
	inline bool* get_address_of_handleLevelLoaded_66() { return &___handleLevelLoaded_66; }
	inline void set_handleLevelLoaded_66(bool value)
	{
		___handleLevelLoaded_66 = value;
	}

	inline static int32_t get_offset_of_handleTriggerEnter2D_67() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerEnter2D_67)); }
	inline bool get_handleTriggerEnter2D_67() const { return ___handleTriggerEnter2D_67; }
	inline bool* get_address_of_handleTriggerEnter2D_67() { return &___handleTriggerEnter2D_67; }
	inline void set_handleTriggerEnter2D_67(bool value)
	{
		___handleTriggerEnter2D_67 = value;
	}

	inline static int32_t get_offset_of_handleTriggerExit2D_68() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerExit2D_68)); }
	inline bool get_handleTriggerExit2D_68() const { return ___handleTriggerExit2D_68; }
	inline bool* get_address_of_handleTriggerExit2D_68() { return &___handleTriggerExit2D_68; }
	inline void set_handleTriggerExit2D_68(bool value)
	{
		___handleTriggerExit2D_68 = value;
	}

	inline static int32_t get_offset_of_handleTriggerStay2D_69() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerStay2D_69)); }
	inline bool get_handleTriggerStay2D_69() const { return ___handleTriggerStay2D_69; }
	inline bool* get_address_of_handleTriggerStay2D_69() { return &___handleTriggerStay2D_69; }
	inline void set_handleTriggerStay2D_69(bool value)
	{
		___handleTriggerStay2D_69 = value;
	}

	inline static int32_t get_offset_of_handleCollisionEnter2D_70() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionEnter2D_70)); }
	inline bool get_handleCollisionEnter2D_70() const { return ___handleCollisionEnter2D_70; }
	inline bool* get_address_of_handleCollisionEnter2D_70() { return &___handleCollisionEnter2D_70; }
	inline void set_handleCollisionEnter2D_70(bool value)
	{
		___handleCollisionEnter2D_70 = value;
	}

	inline static int32_t get_offset_of_handleCollisionExit2D_71() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionExit2D_71)); }
	inline bool get_handleCollisionExit2D_71() const { return ___handleCollisionExit2D_71; }
	inline bool* get_address_of_handleCollisionExit2D_71() { return &___handleCollisionExit2D_71; }
	inline void set_handleCollisionExit2D_71(bool value)
	{
		___handleCollisionExit2D_71 = value;
	}

	inline static int32_t get_offset_of_handleCollisionStay2D_72() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionStay2D_72)); }
	inline bool get_handleCollisionStay2D_72() const { return ___handleCollisionStay2D_72; }
	inline bool* get_address_of_handleCollisionStay2D_72() { return &___handleCollisionStay2D_72; }
	inline void set_handleCollisionStay2D_72(bool value)
	{
		___handleCollisionStay2D_72 = value;
	}

	inline static int32_t get_offset_of_handleTriggerEnter_73() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerEnter_73)); }
	inline bool get_handleTriggerEnter_73() const { return ___handleTriggerEnter_73; }
	inline bool* get_address_of_handleTriggerEnter_73() { return &___handleTriggerEnter_73; }
	inline void set_handleTriggerEnter_73(bool value)
	{
		___handleTriggerEnter_73 = value;
	}

	inline static int32_t get_offset_of_handleTriggerExit_74() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerExit_74)); }
	inline bool get_handleTriggerExit_74() const { return ___handleTriggerExit_74; }
	inline bool* get_address_of_handleTriggerExit_74() { return &___handleTriggerExit_74; }
	inline void set_handleTriggerExit_74(bool value)
	{
		___handleTriggerExit_74 = value;
	}

	inline static int32_t get_offset_of_handleTriggerStay_75() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerStay_75)); }
	inline bool get_handleTriggerStay_75() const { return ___handleTriggerStay_75; }
	inline bool* get_address_of_handleTriggerStay_75() { return &___handleTriggerStay_75; }
	inline void set_handleTriggerStay_75(bool value)
	{
		___handleTriggerStay_75 = value;
	}

	inline static int32_t get_offset_of_handleCollisionEnter_76() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionEnter_76)); }
	inline bool get_handleCollisionEnter_76() const { return ___handleCollisionEnter_76; }
	inline bool* get_address_of_handleCollisionEnter_76() { return &___handleCollisionEnter_76; }
	inline void set_handleCollisionEnter_76(bool value)
	{
		___handleCollisionEnter_76 = value;
	}

	inline static int32_t get_offset_of_handleCollisionExit_77() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionExit_77)); }
	inline bool get_handleCollisionExit_77() const { return ___handleCollisionExit_77; }
	inline bool* get_address_of_handleCollisionExit_77() { return &___handleCollisionExit_77; }
	inline void set_handleCollisionExit_77(bool value)
	{
		___handleCollisionExit_77 = value;
	}

	inline static int32_t get_offset_of_handleCollisionStay_78() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionStay_78)); }
	inline bool get_handleCollisionStay_78() const { return ___handleCollisionStay_78; }
	inline bool* get_address_of_handleCollisionStay_78() { return &___handleCollisionStay_78; }
	inline void set_handleCollisionStay_78(bool value)
	{
		___handleCollisionStay_78 = value;
	}

	inline static int32_t get_offset_of_handleParticleCollision_79() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleParticleCollision_79)); }
	inline bool get_handleParticleCollision_79() const { return ___handleParticleCollision_79; }
	inline bool* get_address_of_handleParticleCollision_79() { return &___handleParticleCollision_79; }
	inline void set_handleParticleCollision_79(bool value)
	{
		___handleParticleCollision_79 = value;
	}

	inline static int32_t get_offset_of_handleControllerColliderHit_80() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleControllerColliderHit_80)); }
	inline bool get_handleControllerColliderHit_80() const { return ___handleControllerColliderHit_80; }
	inline bool* get_address_of_handleControllerColliderHit_80() { return &___handleControllerColliderHit_80; }
	inline void set_handleControllerColliderHit_80(bool value)
	{
		___handleControllerColliderHit_80 = value;
	}

	inline static int32_t get_offset_of_handleJointBreak_81() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleJointBreak_81)); }
	inline bool get_handleJointBreak_81() const { return ___handleJointBreak_81; }
	inline bool* get_address_of_handleJointBreak_81() { return &___handleJointBreak_81; }
	inline void set_handleJointBreak_81(bool value)
	{
		___handleJointBreak_81 = value;
	}

	inline static int32_t get_offset_of_handleJointBreak2D_82() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleJointBreak2D_82)); }
	inline bool get_handleJointBreak2D_82() const { return ___handleJointBreak2D_82; }
	inline bool* get_address_of_handleJointBreak2D_82() { return &___handleJointBreak2D_82; }
	inline void set_handleJointBreak2D_82(bool value)
	{
		___handleJointBreak2D_82 = value;
	}

	inline static int32_t get_offset_of_handleOnGUI_83() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleOnGUI_83)); }
	inline bool get_handleOnGUI_83() const { return ___handleOnGUI_83; }
	inline bool* get_address_of_handleOnGUI_83() { return &___handleOnGUI_83; }
	inline void set_handleOnGUI_83(bool value)
	{
		___handleOnGUI_83 = value;
	}

	inline static int32_t get_offset_of_handleFixedUpdate_84() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleFixedUpdate_84)); }
	inline bool get_handleFixedUpdate_84() const { return ___handleFixedUpdate_84; }
	inline bool* get_address_of_handleFixedUpdate_84() { return &___handleFixedUpdate_84; }
	inline void set_handleFixedUpdate_84(bool value)
	{
		___handleFixedUpdate_84 = value;
	}

	inline static int32_t get_offset_of_handleLateUpdate_85() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleLateUpdate_85)); }
	inline bool get_handleLateUpdate_85() const { return ___handleLateUpdate_85; }
	inline bool* get_address_of_handleLateUpdate_85() { return &___handleLateUpdate_85; }
	inline void set_handleLateUpdate_85(bool value)
	{
		___handleLateUpdate_85 = value;
	}

	inline static int32_t get_offset_of_handleApplicationEvents_86() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleApplicationEvents_86)); }
	inline bool get_handleApplicationEvents_86() const { return ___handleApplicationEvents_86; }
	inline bool* get_address_of_handleApplicationEvents_86() { return &___handleApplicationEvents_86; }
	inline void set_handleApplicationEvents_86(bool value)
	{
		___handleApplicationEvents_86 = value;
	}

	inline static int32_t get_offset_of_handleUiEvents_87() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleUiEvents_87)); }
	inline int32_t get_handleUiEvents_87() const { return ___handleUiEvents_87; }
	inline int32_t* get_address_of_handleUiEvents_87() { return &___handleUiEvents_87; }
	inline void set_handleUiEvents_87(int32_t value)
	{
		___handleUiEvents_87 = value;
	}

	inline static int32_t get_offset_of_handleLegacyNetworking_88() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleLegacyNetworking_88)); }
	inline bool get_handleLegacyNetworking_88() const { return ___handleLegacyNetworking_88; }
	inline bool* get_address_of_handleLegacyNetworking_88() { return &___handleLegacyNetworking_88; }
	inline void set_handleLegacyNetworking_88(bool value)
	{
		___handleLegacyNetworking_88 = value;
	}

	inline static int32_t get_offset_of_U3CCollisionInfoU3Ek__BackingField_89() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollisionInfoU3Ek__BackingField_89)); }
	inline Collision_t4262080450 * get_U3CCollisionInfoU3Ek__BackingField_89() const { return ___U3CCollisionInfoU3Ek__BackingField_89; }
	inline Collision_t4262080450 ** get_address_of_U3CCollisionInfoU3Ek__BackingField_89() { return &___U3CCollisionInfoU3Ek__BackingField_89; }
	inline void set_U3CCollisionInfoU3Ek__BackingField_89(Collision_t4262080450 * value)
	{
		___U3CCollisionInfoU3Ek__BackingField_89 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionInfoU3Ek__BackingField_89), value);
	}

	inline static int32_t get_offset_of_U3CTriggerColliderU3Ek__BackingField_90() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTriggerColliderU3Ek__BackingField_90)); }
	inline Collider_t1773347010 * get_U3CTriggerColliderU3Ek__BackingField_90() const { return ___U3CTriggerColliderU3Ek__BackingField_90; }
	inline Collider_t1773347010 ** get_address_of_U3CTriggerColliderU3Ek__BackingField_90() { return &___U3CTriggerColliderU3Ek__BackingField_90; }
	inline void set_U3CTriggerColliderU3Ek__BackingField_90(Collider_t1773347010 * value)
	{
		___U3CTriggerColliderU3Ek__BackingField_90 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerColliderU3Ek__BackingField_90), value);
	}

	inline static int32_t get_offset_of_U3CCollision2DInfoU3Ek__BackingField_91() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollision2DInfoU3Ek__BackingField_91)); }
	inline Collision2D_t2842956331 * get_U3CCollision2DInfoU3Ek__BackingField_91() const { return ___U3CCollision2DInfoU3Ek__BackingField_91; }
	inline Collision2D_t2842956331 ** get_address_of_U3CCollision2DInfoU3Ek__BackingField_91() { return &___U3CCollision2DInfoU3Ek__BackingField_91; }
	inline void set_U3CCollision2DInfoU3Ek__BackingField_91(Collision2D_t2842956331 * value)
	{
		___U3CCollision2DInfoU3Ek__BackingField_91 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollision2DInfoU3Ek__BackingField_91), value);
	}

	inline static int32_t get_offset_of_U3CTriggerCollider2DU3Ek__BackingField_92() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTriggerCollider2DU3Ek__BackingField_92)); }
	inline Collider2D_t2806799626 * get_U3CTriggerCollider2DU3Ek__BackingField_92() const { return ___U3CTriggerCollider2DU3Ek__BackingField_92; }
	inline Collider2D_t2806799626 ** get_address_of_U3CTriggerCollider2DU3Ek__BackingField_92() { return &___U3CTriggerCollider2DU3Ek__BackingField_92; }
	inline void set_U3CTriggerCollider2DU3Ek__BackingField_92(Collider2D_t2806799626 * value)
	{
		___U3CTriggerCollider2DU3Ek__BackingField_92 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerCollider2DU3Ek__BackingField_92), value);
	}

	inline static int32_t get_offset_of_U3CJointBreakForceU3Ek__BackingField_93() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CJointBreakForceU3Ek__BackingField_93)); }
	inline float get_U3CJointBreakForceU3Ek__BackingField_93() const { return ___U3CJointBreakForceU3Ek__BackingField_93; }
	inline float* get_address_of_U3CJointBreakForceU3Ek__BackingField_93() { return &___U3CJointBreakForceU3Ek__BackingField_93; }
	inline void set_U3CJointBreakForceU3Ek__BackingField_93(float value)
	{
		___U3CJointBreakForceU3Ek__BackingField_93 = value;
	}

	inline static int32_t get_offset_of_U3CBrokenJoint2DU3Ek__BackingField_94() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CBrokenJoint2DU3Ek__BackingField_94)); }
	inline Joint2D_t4180440564 * get_U3CBrokenJoint2DU3Ek__BackingField_94() const { return ___U3CBrokenJoint2DU3Ek__BackingField_94; }
	inline Joint2D_t4180440564 ** get_address_of_U3CBrokenJoint2DU3Ek__BackingField_94() { return &___U3CBrokenJoint2DU3Ek__BackingField_94; }
	inline void set_U3CBrokenJoint2DU3Ek__BackingField_94(Joint2D_t4180440564 * value)
	{
		___U3CBrokenJoint2DU3Ek__BackingField_94 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBrokenJoint2DU3Ek__BackingField_94), value);
	}

	inline static int32_t get_offset_of_U3CParticleCollisionGOU3Ek__BackingField_95() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CParticleCollisionGOU3Ek__BackingField_95)); }
	inline GameObject_t1113636619 * get_U3CParticleCollisionGOU3Ek__BackingField_95() const { return ___U3CParticleCollisionGOU3Ek__BackingField_95; }
	inline GameObject_t1113636619 ** get_address_of_U3CParticleCollisionGOU3Ek__BackingField_95() { return &___U3CParticleCollisionGOU3Ek__BackingField_95; }
	inline void set_U3CParticleCollisionGOU3Ek__BackingField_95(GameObject_t1113636619 * value)
	{
		___U3CParticleCollisionGOU3Ek__BackingField_95 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParticleCollisionGOU3Ek__BackingField_95), value);
	}

	inline static int32_t get_offset_of_U3CTriggerNameU3Ek__BackingField_96() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTriggerNameU3Ek__BackingField_96)); }
	inline String_t* get_U3CTriggerNameU3Ek__BackingField_96() const { return ___U3CTriggerNameU3Ek__BackingField_96; }
	inline String_t** get_address_of_U3CTriggerNameU3Ek__BackingField_96() { return &___U3CTriggerNameU3Ek__BackingField_96; }
	inline void set_U3CTriggerNameU3Ek__BackingField_96(String_t* value)
	{
		___U3CTriggerNameU3Ek__BackingField_96 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerNameU3Ek__BackingField_96), value);
	}

	inline static int32_t get_offset_of_U3CCollisionNameU3Ek__BackingField_97() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollisionNameU3Ek__BackingField_97)); }
	inline String_t* get_U3CCollisionNameU3Ek__BackingField_97() const { return ___U3CCollisionNameU3Ek__BackingField_97; }
	inline String_t** get_address_of_U3CCollisionNameU3Ek__BackingField_97() { return &___U3CCollisionNameU3Ek__BackingField_97; }
	inline void set_U3CCollisionNameU3Ek__BackingField_97(String_t* value)
	{
		___U3CCollisionNameU3Ek__BackingField_97 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionNameU3Ek__BackingField_97), value);
	}

	inline static int32_t get_offset_of_U3CTrigger2dNameU3Ek__BackingField_98() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTrigger2dNameU3Ek__BackingField_98)); }
	inline String_t* get_U3CTrigger2dNameU3Ek__BackingField_98() const { return ___U3CTrigger2dNameU3Ek__BackingField_98; }
	inline String_t** get_address_of_U3CTrigger2dNameU3Ek__BackingField_98() { return &___U3CTrigger2dNameU3Ek__BackingField_98; }
	inline void set_U3CTrigger2dNameU3Ek__BackingField_98(String_t* value)
	{
		___U3CTrigger2dNameU3Ek__BackingField_98 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrigger2dNameU3Ek__BackingField_98), value);
	}

	inline static int32_t get_offset_of_U3CCollision2dNameU3Ek__BackingField_99() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollision2dNameU3Ek__BackingField_99)); }
	inline String_t* get_U3CCollision2dNameU3Ek__BackingField_99() const { return ___U3CCollision2dNameU3Ek__BackingField_99; }
	inline String_t** get_address_of_U3CCollision2dNameU3Ek__BackingField_99() { return &___U3CCollision2dNameU3Ek__BackingField_99; }
	inline void set_U3CCollision2dNameU3Ek__BackingField_99(String_t* value)
	{
		___U3CCollision2dNameU3Ek__BackingField_99 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollision2dNameU3Ek__BackingField_99), value);
	}

	inline static int32_t get_offset_of_U3CControllerColliderU3Ek__BackingField_100() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CControllerColliderU3Ek__BackingField_100)); }
	inline ControllerColliderHit_t240592346 * get_U3CControllerColliderU3Ek__BackingField_100() const { return ___U3CControllerColliderU3Ek__BackingField_100; }
	inline ControllerColliderHit_t240592346 ** get_address_of_U3CControllerColliderU3Ek__BackingField_100() { return &___U3CControllerColliderU3Ek__BackingField_100; }
	inline void set_U3CControllerColliderU3Ek__BackingField_100(ControllerColliderHit_t240592346 * value)
	{
		___U3CControllerColliderU3Ek__BackingField_100 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerColliderU3Ek__BackingField_100), value);
	}

	inline static int32_t get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_101() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CRaycastHitInfoU3Ek__BackingField_101)); }
	inline RaycastHit_t1056001966  get_U3CRaycastHitInfoU3Ek__BackingField_101() const { return ___U3CRaycastHitInfoU3Ek__BackingField_101; }
	inline RaycastHit_t1056001966 * get_address_of_U3CRaycastHitInfoU3Ek__BackingField_101() { return &___U3CRaycastHitInfoU3Ek__BackingField_101; }
	inline void set_U3CRaycastHitInfoU3Ek__BackingField_101(RaycastHit_t1056001966  value)
	{
		___U3CRaycastHitInfoU3Ek__BackingField_101 = value;
	}

	inline static int32_t get_offset_of_handleAnimatorMove_103() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleAnimatorMove_103)); }
	inline bool get_handleAnimatorMove_103() const { return ___handleAnimatorMove_103; }
	inline bool* get_address_of_handleAnimatorMove_103() { return &___handleAnimatorMove_103; }
	inline void set_handleAnimatorMove_103(bool value)
	{
		___handleAnimatorMove_103 = value;
	}

	inline static int32_t get_offset_of_handleAnimatorIK_104() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleAnimatorIK_104)); }
	inline bool get_handleAnimatorIK_104() const { return ___handleAnimatorIK_104; }
	inline bool* get_address_of_handleAnimatorIK_104() { return &___handleAnimatorIK_104; }
	inline void set_handleAnimatorIK_104(bool value)
	{
		___handleAnimatorIK_104 = value;
	}
};

struct Fsm_t4127147824_StaticFields
{
public:
	// HutongGames.PlayMaker.FsmEventData HutongGames.PlayMaker.Fsm::EventData
	FsmEventData_t1692568573 * ___EventData_4;
	// UnityEngine.Color HutongGames.PlayMaker.Fsm::debugLookAtColor
	Color_t2555686324  ___debugLookAtColor_5;
	// UnityEngine.Color HutongGames.PlayMaker.Fsm::debugRaycastColor
	Color_t2555686324  ___debugRaycastColor_6;
	// UnityEngine.Color[] HutongGames.PlayMaker.Fsm::StateColors
	ColorU5BU5D_t941916413* ___StateColors_52;
	// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::<LastClickedObject>k__BackingField
	GameObject_t1113636619 * ___U3CLastClickedObjectU3Ek__BackingField_54;
	// System.Boolean HutongGames.PlayMaker.Fsm::<BreakpointsEnabled>k__BackingField
	bool ___U3CBreakpointsEnabledU3Ek__BackingField_55;
	// System.Boolean HutongGames.PlayMaker.Fsm::<HitBreakpoint>k__BackingField
	bool ___U3CHitBreakpointU3Ek__BackingField_56;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::<BreakAtFsm>k__BackingField
	Fsm_t4127147824 * ___U3CBreakAtFsmU3Ek__BackingField_57;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::<BreakAtState>k__BackingField
	FsmState_t4124398234 * ___U3CBreakAtStateU3Ek__BackingField_58;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsBreak>k__BackingField
	bool ___U3CIsBreakU3Ek__BackingField_59;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsErrorBreak>k__BackingField
	bool ___U3CIsErrorBreakU3Ek__BackingField_60;
	// System.String HutongGames.PlayMaker.Fsm::<LastError>k__BackingField
	String_t* ___U3CLastErrorU3Ek__BackingField_61;
	// System.Boolean HutongGames.PlayMaker.Fsm::<StepToStateChange>k__BackingField
	bool ___U3CStepToStateChangeU3Ek__BackingField_62;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::<StepFsm>k__BackingField
	Fsm_t4127147824 * ___U3CStepFsmU3Ek__BackingField_63;
	// System.Collections.Generic.Dictionary`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D> HutongGames.PlayMaker.Fsm::lastRaycastHit2DInfoLUT
	Dictionary_2_t4026181381 * ___lastRaycastHit2DInfoLUT_102;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::targetSelf
	FsmEventTarget_t919699796 * ___targetSelf_105;

public:
	inline static int32_t get_offset_of_EventData_4() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___EventData_4)); }
	inline FsmEventData_t1692568573 * get_EventData_4() const { return ___EventData_4; }
	inline FsmEventData_t1692568573 ** get_address_of_EventData_4() { return &___EventData_4; }
	inline void set_EventData_4(FsmEventData_t1692568573 * value)
	{
		___EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___EventData_4), value);
	}

	inline static int32_t get_offset_of_debugLookAtColor_5() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___debugLookAtColor_5)); }
	inline Color_t2555686324  get_debugLookAtColor_5() const { return ___debugLookAtColor_5; }
	inline Color_t2555686324 * get_address_of_debugLookAtColor_5() { return &___debugLookAtColor_5; }
	inline void set_debugLookAtColor_5(Color_t2555686324  value)
	{
		___debugLookAtColor_5 = value;
	}

	inline static int32_t get_offset_of_debugRaycastColor_6() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___debugRaycastColor_6)); }
	inline Color_t2555686324  get_debugRaycastColor_6() const { return ___debugRaycastColor_6; }
	inline Color_t2555686324 * get_address_of_debugRaycastColor_6() { return &___debugRaycastColor_6; }
	inline void set_debugRaycastColor_6(Color_t2555686324  value)
	{
		___debugRaycastColor_6 = value;
	}

	inline static int32_t get_offset_of_StateColors_52() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___StateColors_52)); }
	inline ColorU5BU5D_t941916413* get_StateColors_52() const { return ___StateColors_52; }
	inline ColorU5BU5D_t941916413** get_address_of_StateColors_52() { return &___StateColors_52; }
	inline void set_StateColors_52(ColorU5BU5D_t941916413* value)
	{
		___StateColors_52 = value;
		Il2CppCodeGenWriteBarrier((&___StateColors_52), value);
	}

	inline static int32_t get_offset_of_U3CLastClickedObjectU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CLastClickedObjectU3Ek__BackingField_54)); }
	inline GameObject_t1113636619 * get_U3CLastClickedObjectU3Ek__BackingField_54() const { return ___U3CLastClickedObjectU3Ek__BackingField_54; }
	inline GameObject_t1113636619 ** get_address_of_U3CLastClickedObjectU3Ek__BackingField_54() { return &___U3CLastClickedObjectU3Ek__BackingField_54; }
	inline void set_U3CLastClickedObjectU3Ek__BackingField_54(GameObject_t1113636619 * value)
	{
		___U3CLastClickedObjectU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastClickedObjectU3Ek__BackingField_54), value);
	}

	inline static int32_t get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CBreakpointsEnabledU3Ek__BackingField_55)); }
	inline bool get_U3CBreakpointsEnabledU3Ek__BackingField_55() const { return ___U3CBreakpointsEnabledU3Ek__BackingField_55; }
	inline bool* get_address_of_U3CBreakpointsEnabledU3Ek__BackingField_55() { return &___U3CBreakpointsEnabledU3Ek__BackingField_55; }
	inline void set_U3CBreakpointsEnabledU3Ek__BackingField_55(bool value)
	{
		___U3CBreakpointsEnabledU3Ek__BackingField_55 = value;
	}

	inline static int32_t get_offset_of_U3CHitBreakpointU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CHitBreakpointU3Ek__BackingField_56)); }
	inline bool get_U3CHitBreakpointU3Ek__BackingField_56() const { return ___U3CHitBreakpointU3Ek__BackingField_56; }
	inline bool* get_address_of_U3CHitBreakpointU3Ek__BackingField_56() { return &___U3CHitBreakpointU3Ek__BackingField_56; }
	inline void set_U3CHitBreakpointU3Ek__BackingField_56(bool value)
	{
		___U3CHitBreakpointU3Ek__BackingField_56 = value;
	}

	inline static int32_t get_offset_of_U3CBreakAtFsmU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CBreakAtFsmU3Ek__BackingField_57)); }
	inline Fsm_t4127147824 * get_U3CBreakAtFsmU3Ek__BackingField_57() const { return ___U3CBreakAtFsmU3Ek__BackingField_57; }
	inline Fsm_t4127147824 ** get_address_of_U3CBreakAtFsmU3Ek__BackingField_57() { return &___U3CBreakAtFsmU3Ek__BackingField_57; }
	inline void set_U3CBreakAtFsmU3Ek__BackingField_57(Fsm_t4127147824 * value)
	{
		___U3CBreakAtFsmU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBreakAtFsmU3Ek__BackingField_57), value);
	}

	inline static int32_t get_offset_of_U3CBreakAtStateU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CBreakAtStateU3Ek__BackingField_58)); }
	inline FsmState_t4124398234 * get_U3CBreakAtStateU3Ek__BackingField_58() const { return ___U3CBreakAtStateU3Ek__BackingField_58; }
	inline FsmState_t4124398234 ** get_address_of_U3CBreakAtStateU3Ek__BackingField_58() { return &___U3CBreakAtStateU3Ek__BackingField_58; }
	inline void set_U3CBreakAtStateU3Ek__BackingField_58(FsmState_t4124398234 * value)
	{
		___U3CBreakAtStateU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBreakAtStateU3Ek__BackingField_58), value);
	}

	inline static int32_t get_offset_of_U3CIsBreakU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CIsBreakU3Ek__BackingField_59)); }
	inline bool get_U3CIsBreakU3Ek__BackingField_59() const { return ___U3CIsBreakU3Ek__BackingField_59; }
	inline bool* get_address_of_U3CIsBreakU3Ek__BackingField_59() { return &___U3CIsBreakU3Ek__BackingField_59; }
	inline void set_U3CIsBreakU3Ek__BackingField_59(bool value)
	{
		___U3CIsBreakU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of_U3CIsErrorBreakU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CIsErrorBreakU3Ek__BackingField_60)); }
	inline bool get_U3CIsErrorBreakU3Ek__BackingField_60() const { return ___U3CIsErrorBreakU3Ek__BackingField_60; }
	inline bool* get_address_of_U3CIsErrorBreakU3Ek__BackingField_60() { return &___U3CIsErrorBreakU3Ek__BackingField_60; }
	inline void set_U3CIsErrorBreakU3Ek__BackingField_60(bool value)
	{
		___U3CIsErrorBreakU3Ek__BackingField_60 = value;
	}

	inline static int32_t get_offset_of_U3CLastErrorU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CLastErrorU3Ek__BackingField_61)); }
	inline String_t* get_U3CLastErrorU3Ek__BackingField_61() const { return ___U3CLastErrorU3Ek__BackingField_61; }
	inline String_t** get_address_of_U3CLastErrorU3Ek__BackingField_61() { return &___U3CLastErrorU3Ek__BackingField_61; }
	inline void set_U3CLastErrorU3Ek__BackingField_61(String_t* value)
	{
		___U3CLastErrorU3Ek__BackingField_61 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastErrorU3Ek__BackingField_61), value);
	}

	inline static int32_t get_offset_of_U3CStepToStateChangeU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CStepToStateChangeU3Ek__BackingField_62)); }
	inline bool get_U3CStepToStateChangeU3Ek__BackingField_62() const { return ___U3CStepToStateChangeU3Ek__BackingField_62; }
	inline bool* get_address_of_U3CStepToStateChangeU3Ek__BackingField_62() { return &___U3CStepToStateChangeU3Ek__BackingField_62; }
	inline void set_U3CStepToStateChangeU3Ek__BackingField_62(bool value)
	{
		___U3CStepToStateChangeU3Ek__BackingField_62 = value;
	}

	inline static int32_t get_offset_of_U3CStepFsmU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CStepFsmU3Ek__BackingField_63)); }
	inline Fsm_t4127147824 * get_U3CStepFsmU3Ek__BackingField_63() const { return ___U3CStepFsmU3Ek__BackingField_63; }
	inline Fsm_t4127147824 ** get_address_of_U3CStepFsmU3Ek__BackingField_63() { return &___U3CStepFsmU3Ek__BackingField_63; }
	inline void set_U3CStepFsmU3Ek__BackingField_63(Fsm_t4127147824 * value)
	{
		___U3CStepFsmU3Ek__BackingField_63 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStepFsmU3Ek__BackingField_63), value);
	}

	inline static int32_t get_offset_of_lastRaycastHit2DInfoLUT_102() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___lastRaycastHit2DInfoLUT_102)); }
	inline Dictionary_2_t4026181381 * get_lastRaycastHit2DInfoLUT_102() const { return ___lastRaycastHit2DInfoLUT_102; }
	inline Dictionary_2_t4026181381 ** get_address_of_lastRaycastHit2DInfoLUT_102() { return &___lastRaycastHit2DInfoLUT_102; }
	inline void set_lastRaycastHit2DInfoLUT_102(Dictionary_2_t4026181381 * value)
	{
		___lastRaycastHit2DInfoLUT_102 = value;
		Il2CppCodeGenWriteBarrier((&___lastRaycastHit2DInfoLUT_102), value);
	}

	inline static int32_t get_offset_of_targetSelf_105() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___targetSelf_105)); }
	inline FsmEventTarget_t919699796 * get_targetSelf_105() const { return ___targetSelf_105; }
	inline FsmEventTarget_t919699796 ** get_address_of_targetSelf_105() { return &___targetSelf_105; }
	inline void set_targetSelf_105(FsmEventTarget_t919699796 * value)
	{
		___targetSelf_105 = value;
		Il2CppCodeGenWriteBarrier((&___targetSelf_105), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSM_T4127147824_H
#ifndef FSMTRANSITION_T1340699069_H
#define FSMTRANSITION_T1340699069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTransition
struct  FsmTransition_t1340699069  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmTransition::fsmEvent
	FsmEvent_t3736299882 * ___fsmEvent_0;
	// System.String HutongGames.PlayMaker.FsmTransition::toState
	String_t* ___toState_1;
	// HutongGames.PlayMaker.FsmTransition/CustomLinkStyle HutongGames.PlayMaker.FsmTransition::linkStyle
	uint8_t ___linkStyle_2;
	// HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint HutongGames.PlayMaker.FsmTransition::linkConstraint
	uint8_t ___linkConstraint_3;
	// System.Byte HutongGames.PlayMaker.FsmTransition::colorIndex
	uint8_t ___colorIndex_4;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmTransition::toFsmState
	FsmState_t4124398234 * ___toFsmState_5;

public:
	inline static int32_t get_offset_of_fsmEvent_0() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___fsmEvent_0)); }
	inline FsmEvent_t3736299882 * get_fsmEvent_0() const { return ___fsmEvent_0; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmEvent_0() { return &___fsmEvent_0; }
	inline void set_fsmEvent_0(FsmEvent_t3736299882 * value)
	{
		___fsmEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsmEvent_0), value);
	}

	inline static int32_t get_offset_of_toState_1() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___toState_1)); }
	inline String_t* get_toState_1() const { return ___toState_1; }
	inline String_t** get_address_of_toState_1() { return &___toState_1; }
	inline void set_toState_1(String_t* value)
	{
		___toState_1 = value;
		Il2CppCodeGenWriteBarrier((&___toState_1), value);
	}

	inline static int32_t get_offset_of_linkStyle_2() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___linkStyle_2)); }
	inline uint8_t get_linkStyle_2() const { return ___linkStyle_2; }
	inline uint8_t* get_address_of_linkStyle_2() { return &___linkStyle_2; }
	inline void set_linkStyle_2(uint8_t value)
	{
		___linkStyle_2 = value;
	}

	inline static int32_t get_offset_of_linkConstraint_3() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___linkConstraint_3)); }
	inline uint8_t get_linkConstraint_3() const { return ___linkConstraint_3; }
	inline uint8_t* get_address_of_linkConstraint_3() { return &___linkConstraint_3; }
	inline void set_linkConstraint_3(uint8_t value)
	{
		___linkConstraint_3 = value;
	}

	inline static int32_t get_offset_of_colorIndex_4() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___colorIndex_4)); }
	inline uint8_t get_colorIndex_4() const { return ___colorIndex_4; }
	inline uint8_t* get_address_of_colorIndex_4() { return &___colorIndex_4; }
	inline void set_colorIndex_4(uint8_t value)
	{
		___colorIndex_4 = value;
	}

	inline static int32_t get_offset_of_toFsmState_5() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___toFsmState_5)); }
	inline FsmState_t4124398234 * get_toFsmState_5() const { return ___toFsmState_5; }
	inline FsmState_t4124398234 ** get_address_of_toFsmState_5() { return &___toFsmState_5; }
	inline void set_toFsmState_5(FsmState_t4124398234 * value)
	{
		___toFsmState_5 = value;
		Il2CppCodeGenWriteBarrier((&___toFsmState_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMTRANSITION_T1340699069_H
#ifndef UIHINTATTRIBUTE_T2334957252_H
#define UIHINTATTRIBUTE_T2334957252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UIHintAttribute
struct  UIHintAttribute_t2334957252  : public Attribute_t861562559
{
public:
	// HutongGames.PlayMaker.UIHint HutongGames.PlayMaker.UIHintAttribute::hint
	int32_t ___hint_0;

public:
	inline static int32_t get_offset_of_hint_0() { return static_cast<int32_t>(offsetof(UIHintAttribute_t2334957252, ___hint_0)); }
	inline int32_t get_hint_0() const { return ___hint_0; }
	inline int32_t* get_address_of_hint_0() { return &___hint_0; }
	inline void set_hint_0(int32_t value)
	{
		___hint_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIHINTATTRIBUTE_T2334957252_H
#ifndef VARIABLETYPEATTRIBUTE_T3556842571_H
#define VARIABLETYPEATTRIBUTE_T3556842571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableTypeAttribute
struct  VariableTypeAttribute_t3556842571  : public Attribute_t861562559
{
public:
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.VariableTypeAttribute::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(VariableTypeAttribute_t3556842571, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPEATTRIBUTE_T3556842571_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef CACHEDCONTACTPOINTS2D_T2523437281_H
#define CACHEDCONTACTPOINTS2D_T2523437281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CachedContactPoints2D
struct  CachedContactPoints2D_t2523437281 
{
public:
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact0
	ContactPoint2D_t3390240644  ___m_Contact0_0;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact1
	ContactPoint2D_t3390240644  ___m_Contact1_1;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact2
	ContactPoint2D_t3390240644  ___m_Contact2_2;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact3
	ContactPoint2D_t3390240644  ___m_Contact3_3;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact4
	ContactPoint2D_t3390240644  ___m_Contact4_4;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact5
	ContactPoint2D_t3390240644  ___m_Contact5_5;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact6
	ContactPoint2D_t3390240644  ___m_Contact6_6;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact7
	ContactPoint2D_t3390240644  ___m_Contact7_7;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact8
	ContactPoint2D_t3390240644  ___m_Contact8_8;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact9
	ContactPoint2D_t3390240644  ___m_Contact9_9;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact10
	ContactPoint2D_t3390240644  ___m_Contact10_10;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact11
	ContactPoint2D_t3390240644  ___m_Contact11_11;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact12
	ContactPoint2D_t3390240644  ___m_Contact12_12;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact13
	ContactPoint2D_t3390240644  ___m_Contact13_13;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact14
	ContactPoint2D_t3390240644  ___m_Contact14_14;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact15
	ContactPoint2D_t3390240644  ___m_Contact15_15;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact16
	ContactPoint2D_t3390240644  ___m_Contact16_16;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact17
	ContactPoint2D_t3390240644  ___m_Contact17_17;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact18
	ContactPoint2D_t3390240644  ___m_Contact18_18;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact19
	ContactPoint2D_t3390240644  ___m_Contact19_19;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact20
	ContactPoint2D_t3390240644  ___m_Contact20_20;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact21
	ContactPoint2D_t3390240644  ___m_Contact21_21;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact22
	ContactPoint2D_t3390240644  ___m_Contact22_22;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact23
	ContactPoint2D_t3390240644  ___m_Contact23_23;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact24
	ContactPoint2D_t3390240644  ___m_Contact24_24;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact25
	ContactPoint2D_t3390240644  ___m_Contact25_25;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact26
	ContactPoint2D_t3390240644  ___m_Contact26_26;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact27
	ContactPoint2D_t3390240644  ___m_Contact27_27;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact28
	ContactPoint2D_t3390240644  ___m_Contact28_28;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact29
	ContactPoint2D_t3390240644  ___m_Contact29_29;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact30
	ContactPoint2D_t3390240644  ___m_Contact30_30;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact31
	ContactPoint2D_t3390240644  ___m_Contact31_31;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact32
	ContactPoint2D_t3390240644  ___m_Contact32_32;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact33
	ContactPoint2D_t3390240644  ___m_Contact33_33;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact34
	ContactPoint2D_t3390240644  ___m_Contact34_34;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact35
	ContactPoint2D_t3390240644  ___m_Contact35_35;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact36
	ContactPoint2D_t3390240644  ___m_Contact36_36;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact37
	ContactPoint2D_t3390240644  ___m_Contact37_37;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact38
	ContactPoint2D_t3390240644  ___m_Contact38_38;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact39
	ContactPoint2D_t3390240644  ___m_Contact39_39;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact40
	ContactPoint2D_t3390240644  ___m_Contact40_40;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact41
	ContactPoint2D_t3390240644  ___m_Contact41_41;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact42
	ContactPoint2D_t3390240644  ___m_Contact42_42;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact43
	ContactPoint2D_t3390240644  ___m_Contact43_43;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact44
	ContactPoint2D_t3390240644  ___m_Contact44_44;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact45
	ContactPoint2D_t3390240644  ___m_Contact45_45;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact46
	ContactPoint2D_t3390240644  ___m_Contact46_46;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact47
	ContactPoint2D_t3390240644  ___m_Contact47_47;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact48
	ContactPoint2D_t3390240644  ___m_Contact48_48;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact49
	ContactPoint2D_t3390240644  ___m_Contact49_49;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact50
	ContactPoint2D_t3390240644  ___m_Contact50_50;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact51
	ContactPoint2D_t3390240644  ___m_Contact51_51;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact52
	ContactPoint2D_t3390240644  ___m_Contact52_52;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact53
	ContactPoint2D_t3390240644  ___m_Contact53_53;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact54
	ContactPoint2D_t3390240644  ___m_Contact54_54;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact55
	ContactPoint2D_t3390240644  ___m_Contact55_55;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact56
	ContactPoint2D_t3390240644  ___m_Contact56_56;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact57
	ContactPoint2D_t3390240644  ___m_Contact57_57;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact58
	ContactPoint2D_t3390240644  ___m_Contact58_58;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact59
	ContactPoint2D_t3390240644  ___m_Contact59_59;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact60
	ContactPoint2D_t3390240644  ___m_Contact60_60;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact61
	ContactPoint2D_t3390240644  ___m_Contact61_61;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact62
	ContactPoint2D_t3390240644  ___m_Contact62_62;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact63
	ContactPoint2D_t3390240644  ___m_Contact63_63;

public:
	inline static int32_t get_offset_of_m_Contact0_0() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact0_0)); }
	inline ContactPoint2D_t3390240644  get_m_Contact0_0() const { return ___m_Contact0_0; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact0_0() { return &___m_Contact0_0; }
	inline void set_m_Contact0_0(ContactPoint2D_t3390240644  value)
	{
		___m_Contact0_0 = value;
	}

	inline static int32_t get_offset_of_m_Contact1_1() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact1_1)); }
	inline ContactPoint2D_t3390240644  get_m_Contact1_1() const { return ___m_Contact1_1; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact1_1() { return &___m_Contact1_1; }
	inline void set_m_Contact1_1(ContactPoint2D_t3390240644  value)
	{
		___m_Contact1_1 = value;
	}

	inline static int32_t get_offset_of_m_Contact2_2() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact2_2)); }
	inline ContactPoint2D_t3390240644  get_m_Contact2_2() const { return ___m_Contact2_2; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact2_2() { return &___m_Contact2_2; }
	inline void set_m_Contact2_2(ContactPoint2D_t3390240644  value)
	{
		___m_Contact2_2 = value;
	}

	inline static int32_t get_offset_of_m_Contact3_3() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact3_3)); }
	inline ContactPoint2D_t3390240644  get_m_Contact3_3() const { return ___m_Contact3_3; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact3_3() { return &___m_Contact3_3; }
	inline void set_m_Contact3_3(ContactPoint2D_t3390240644  value)
	{
		___m_Contact3_3 = value;
	}

	inline static int32_t get_offset_of_m_Contact4_4() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact4_4)); }
	inline ContactPoint2D_t3390240644  get_m_Contact4_4() const { return ___m_Contact4_4; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact4_4() { return &___m_Contact4_4; }
	inline void set_m_Contact4_4(ContactPoint2D_t3390240644  value)
	{
		___m_Contact4_4 = value;
	}

	inline static int32_t get_offset_of_m_Contact5_5() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact5_5)); }
	inline ContactPoint2D_t3390240644  get_m_Contact5_5() const { return ___m_Contact5_5; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact5_5() { return &___m_Contact5_5; }
	inline void set_m_Contact5_5(ContactPoint2D_t3390240644  value)
	{
		___m_Contact5_5 = value;
	}

	inline static int32_t get_offset_of_m_Contact6_6() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact6_6)); }
	inline ContactPoint2D_t3390240644  get_m_Contact6_6() const { return ___m_Contact6_6; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact6_6() { return &___m_Contact6_6; }
	inline void set_m_Contact6_6(ContactPoint2D_t3390240644  value)
	{
		___m_Contact6_6 = value;
	}

	inline static int32_t get_offset_of_m_Contact7_7() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact7_7)); }
	inline ContactPoint2D_t3390240644  get_m_Contact7_7() const { return ___m_Contact7_7; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact7_7() { return &___m_Contact7_7; }
	inline void set_m_Contact7_7(ContactPoint2D_t3390240644  value)
	{
		___m_Contact7_7 = value;
	}

	inline static int32_t get_offset_of_m_Contact8_8() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact8_8)); }
	inline ContactPoint2D_t3390240644  get_m_Contact8_8() const { return ___m_Contact8_8; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact8_8() { return &___m_Contact8_8; }
	inline void set_m_Contact8_8(ContactPoint2D_t3390240644  value)
	{
		___m_Contact8_8 = value;
	}

	inline static int32_t get_offset_of_m_Contact9_9() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact9_9)); }
	inline ContactPoint2D_t3390240644  get_m_Contact9_9() const { return ___m_Contact9_9; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact9_9() { return &___m_Contact9_9; }
	inline void set_m_Contact9_9(ContactPoint2D_t3390240644  value)
	{
		___m_Contact9_9 = value;
	}

	inline static int32_t get_offset_of_m_Contact10_10() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact10_10)); }
	inline ContactPoint2D_t3390240644  get_m_Contact10_10() const { return ___m_Contact10_10; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact10_10() { return &___m_Contact10_10; }
	inline void set_m_Contact10_10(ContactPoint2D_t3390240644  value)
	{
		___m_Contact10_10 = value;
	}

	inline static int32_t get_offset_of_m_Contact11_11() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact11_11)); }
	inline ContactPoint2D_t3390240644  get_m_Contact11_11() const { return ___m_Contact11_11; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact11_11() { return &___m_Contact11_11; }
	inline void set_m_Contact11_11(ContactPoint2D_t3390240644  value)
	{
		___m_Contact11_11 = value;
	}

	inline static int32_t get_offset_of_m_Contact12_12() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact12_12)); }
	inline ContactPoint2D_t3390240644  get_m_Contact12_12() const { return ___m_Contact12_12; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact12_12() { return &___m_Contact12_12; }
	inline void set_m_Contact12_12(ContactPoint2D_t3390240644  value)
	{
		___m_Contact12_12 = value;
	}

	inline static int32_t get_offset_of_m_Contact13_13() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact13_13)); }
	inline ContactPoint2D_t3390240644  get_m_Contact13_13() const { return ___m_Contact13_13; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact13_13() { return &___m_Contact13_13; }
	inline void set_m_Contact13_13(ContactPoint2D_t3390240644  value)
	{
		___m_Contact13_13 = value;
	}

	inline static int32_t get_offset_of_m_Contact14_14() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact14_14)); }
	inline ContactPoint2D_t3390240644  get_m_Contact14_14() const { return ___m_Contact14_14; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact14_14() { return &___m_Contact14_14; }
	inline void set_m_Contact14_14(ContactPoint2D_t3390240644  value)
	{
		___m_Contact14_14 = value;
	}

	inline static int32_t get_offset_of_m_Contact15_15() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact15_15)); }
	inline ContactPoint2D_t3390240644  get_m_Contact15_15() const { return ___m_Contact15_15; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact15_15() { return &___m_Contact15_15; }
	inline void set_m_Contact15_15(ContactPoint2D_t3390240644  value)
	{
		___m_Contact15_15 = value;
	}

	inline static int32_t get_offset_of_m_Contact16_16() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact16_16)); }
	inline ContactPoint2D_t3390240644  get_m_Contact16_16() const { return ___m_Contact16_16; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact16_16() { return &___m_Contact16_16; }
	inline void set_m_Contact16_16(ContactPoint2D_t3390240644  value)
	{
		___m_Contact16_16 = value;
	}

	inline static int32_t get_offset_of_m_Contact17_17() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact17_17)); }
	inline ContactPoint2D_t3390240644  get_m_Contact17_17() const { return ___m_Contact17_17; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact17_17() { return &___m_Contact17_17; }
	inline void set_m_Contact17_17(ContactPoint2D_t3390240644  value)
	{
		___m_Contact17_17 = value;
	}

	inline static int32_t get_offset_of_m_Contact18_18() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact18_18)); }
	inline ContactPoint2D_t3390240644  get_m_Contact18_18() const { return ___m_Contact18_18; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact18_18() { return &___m_Contact18_18; }
	inline void set_m_Contact18_18(ContactPoint2D_t3390240644  value)
	{
		___m_Contact18_18 = value;
	}

	inline static int32_t get_offset_of_m_Contact19_19() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact19_19)); }
	inline ContactPoint2D_t3390240644  get_m_Contact19_19() const { return ___m_Contact19_19; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact19_19() { return &___m_Contact19_19; }
	inline void set_m_Contact19_19(ContactPoint2D_t3390240644  value)
	{
		___m_Contact19_19 = value;
	}

	inline static int32_t get_offset_of_m_Contact20_20() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact20_20)); }
	inline ContactPoint2D_t3390240644  get_m_Contact20_20() const { return ___m_Contact20_20; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact20_20() { return &___m_Contact20_20; }
	inline void set_m_Contact20_20(ContactPoint2D_t3390240644  value)
	{
		___m_Contact20_20 = value;
	}

	inline static int32_t get_offset_of_m_Contact21_21() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact21_21)); }
	inline ContactPoint2D_t3390240644  get_m_Contact21_21() const { return ___m_Contact21_21; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact21_21() { return &___m_Contact21_21; }
	inline void set_m_Contact21_21(ContactPoint2D_t3390240644  value)
	{
		___m_Contact21_21 = value;
	}

	inline static int32_t get_offset_of_m_Contact22_22() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact22_22)); }
	inline ContactPoint2D_t3390240644  get_m_Contact22_22() const { return ___m_Contact22_22; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact22_22() { return &___m_Contact22_22; }
	inline void set_m_Contact22_22(ContactPoint2D_t3390240644  value)
	{
		___m_Contact22_22 = value;
	}

	inline static int32_t get_offset_of_m_Contact23_23() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact23_23)); }
	inline ContactPoint2D_t3390240644  get_m_Contact23_23() const { return ___m_Contact23_23; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact23_23() { return &___m_Contact23_23; }
	inline void set_m_Contact23_23(ContactPoint2D_t3390240644  value)
	{
		___m_Contact23_23 = value;
	}

	inline static int32_t get_offset_of_m_Contact24_24() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact24_24)); }
	inline ContactPoint2D_t3390240644  get_m_Contact24_24() const { return ___m_Contact24_24; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact24_24() { return &___m_Contact24_24; }
	inline void set_m_Contact24_24(ContactPoint2D_t3390240644  value)
	{
		___m_Contact24_24 = value;
	}

	inline static int32_t get_offset_of_m_Contact25_25() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact25_25)); }
	inline ContactPoint2D_t3390240644  get_m_Contact25_25() const { return ___m_Contact25_25; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact25_25() { return &___m_Contact25_25; }
	inline void set_m_Contact25_25(ContactPoint2D_t3390240644  value)
	{
		___m_Contact25_25 = value;
	}

	inline static int32_t get_offset_of_m_Contact26_26() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact26_26)); }
	inline ContactPoint2D_t3390240644  get_m_Contact26_26() const { return ___m_Contact26_26; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact26_26() { return &___m_Contact26_26; }
	inline void set_m_Contact26_26(ContactPoint2D_t3390240644  value)
	{
		___m_Contact26_26 = value;
	}

	inline static int32_t get_offset_of_m_Contact27_27() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact27_27)); }
	inline ContactPoint2D_t3390240644  get_m_Contact27_27() const { return ___m_Contact27_27; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact27_27() { return &___m_Contact27_27; }
	inline void set_m_Contact27_27(ContactPoint2D_t3390240644  value)
	{
		___m_Contact27_27 = value;
	}

	inline static int32_t get_offset_of_m_Contact28_28() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact28_28)); }
	inline ContactPoint2D_t3390240644  get_m_Contact28_28() const { return ___m_Contact28_28; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact28_28() { return &___m_Contact28_28; }
	inline void set_m_Contact28_28(ContactPoint2D_t3390240644  value)
	{
		___m_Contact28_28 = value;
	}

	inline static int32_t get_offset_of_m_Contact29_29() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact29_29)); }
	inline ContactPoint2D_t3390240644  get_m_Contact29_29() const { return ___m_Contact29_29; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact29_29() { return &___m_Contact29_29; }
	inline void set_m_Contact29_29(ContactPoint2D_t3390240644  value)
	{
		___m_Contact29_29 = value;
	}

	inline static int32_t get_offset_of_m_Contact30_30() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact30_30)); }
	inline ContactPoint2D_t3390240644  get_m_Contact30_30() const { return ___m_Contact30_30; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact30_30() { return &___m_Contact30_30; }
	inline void set_m_Contact30_30(ContactPoint2D_t3390240644  value)
	{
		___m_Contact30_30 = value;
	}

	inline static int32_t get_offset_of_m_Contact31_31() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact31_31)); }
	inline ContactPoint2D_t3390240644  get_m_Contact31_31() const { return ___m_Contact31_31; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact31_31() { return &___m_Contact31_31; }
	inline void set_m_Contact31_31(ContactPoint2D_t3390240644  value)
	{
		___m_Contact31_31 = value;
	}

	inline static int32_t get_offset_of_m_Contact32_32() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact32_32)); }
	inline ContactPoint2D_t3390240644  get_m_Contact32_32() const { return ___m_Contact32_32; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact32_32() { return &___m_Contact32_32; }
	inline void set_m_Contact32_32(ContactPoint2D_t3390240644  value)
	{
		___m_Contact32_32 = value;
	}

	inline static int32_t get_offset_of_m_Contact33_33() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact33_33)); }
	inline ContactPoint2D_t3390240644  get_m_Contact33_33() const { return ___m_Contact33_33; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact33_33() { return &___m_Contact33_33; }
	inline void set_m_Contact33_33(ContactPoint2D_t3390240644  value)
	{
		___m_Contact33_33 = value;
	}

	inline static int32_t get_offset_of_m_Contact34_34() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact34_34)); }
	inline ContactPoint2D_t3390240644  get_m_Contact34_34() const { return ___m_Contact34_34; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact34_34() { return &___m_Contact34_34; }
	inline void set_m_Contact34_34(ContactPoint2D_t3390240644  value)
	{
		___m_Contact34_34 = value;
	}

	inline static int32_t get_offset_of_m_Contact35_35() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact35_35)); }
	inline ContactPoint2D_t3390240644  get_m_Contact35_35() const { return ___m_Contact35_35; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact35_35() { return &___m_Contact35_35; }
	inline void set_m_Contact35_35(ContactPoint2D_t3390240644  value)
	{
		___m_Contact35_35 = value;
	}

	inline static int32_t get_offset_of_m_Contact36_36() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact36_36)); }
	inline ContactPoint2D_t3390240644  get_m_Contact36_36() const { return ___m_Contact36_36; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact36_36() { return &___m_Contact36_36; }
	inline void set_m_Contact36_36(ContactPoint2D_t3390240644  value)
	{
		___m_Contact36_36 = value;
	}

	inline static int32_t get_offset_of_m_Contact37_37() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact37_37)); }
	inline ContactPoint2D_t3390240644  get_m_Contact37_37() const { return ___m_Contact37_37; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact37_37() { return &___m_Contact37_37; }
	inline void set_m_Contact37_37(ContactPoint2D_t3390240644  value)
	{
		___m_Contact37_37 = value;
	}

	inline static int32_t get_offset_of_m_Contact38_38() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact38_38)); }
	inline ContactPoint2D_t3390240644  get_m_Contact38_38() const { return ___m_Contact38_38; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact38_38() { return &___m_Contact38_38; }
	inline void set_m_Contact38_38(ContactPoint2D_t3390240644  value)
	{
		___m_Contact38_38 = value;
	}

	inline static int32_t get_offset_of_m_Contact39_39() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact39_39)); }
	inline ContactPoint2D_t3390240644  get_m_Contact39_39() const { return ___m_Contact39_39; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact39_39() { return &___m_Contact39_39; }
	inline void set_m_Contact39_39(ContactPoint2D_t3390240644  value)
	{
		___m_Contact39_39 = value;
	}

	inline static int32_t get_offset_of_m_Contact40_40() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact40_40)); }
	inline ContactPoint2D_t3390240644  get_m_Contact40_40() const { return ___m_Contact40_40; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact40_40() { return &___m_Contact40_40; }
	inline void set_m_Contact40_40(ContactPoint2D_t3390240644  value)
	{
		___m_Contact40_40 = value;
	}

	inline static int32_t get_offset_of_m_Contact41_41() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact41_41)); }
	inline ContactPoint2D_t3390240644  get_m_Contact41_41() const { return ___m_Contact41_41; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact41_41() { return &___m_Contact41_41; }
	inline void set_m_Contact41_41(ContactPoint2D_t3390240644  value)
	{
		___m_Contact41_41 = value;
	}

	inline static int32_t get_offset_of_m_Contact42_42() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact42_42)); }
	inline ContactPoint2D_t3390240644  get_m_Contact42_42() const { return ___m_Contact42_42; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact42_42() { return &___m_Contact42_42; }
	inline void set_m_Contact42_42(ContactPoint2D_t3390240644  value)
	{
		___m_Contact42_42 = value;
	}

	inline static int32_t get_offset_of_m_Contact43_43() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact43_43)); }
	inline ContactPoint2D_t3390240644  get_m_Contact43_43() const { return ___m_Contact43_43; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact43_43() { return &___m_Contact43_43; }
	inline void set_m_Contact43_43(ContactPoint2D_t3390240644  value)
	{
		___m_Contact43_43 = value;
	}

	inline static int32_t get_offset_of_m_Contact44_44() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact44_44)); }
	inline ContactPoint2D_t3390240644  get_m_Contact44_44() const { return ___m_Contact44_44; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact44_44() { return &___m_Contact44_44; }
	inline void set_m_Contact44_44(ContactPoint2D_t3390240644  value)
	{
		___m_Contact44_44 = value;
	}

	inline static int32_t get_offset_of_m_Contact45_45() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact45_45)); }
	inline ContactPoint2D_t3390240644  get_m_Contact45_45() const { return ___m_Contact45_45; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact45_45() { return &___m_Contact45_45; }
	inline void set_m_Contact45_45(ContactPoint2D_t3390240644  value)
	{
		___m_Contact45_45 = value;
	}

	inline static int32_t get_offset_of_m_Contact46_46() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact46_46)); }
	inline ContactPoint2D_t3390240644  get_m_Contact46_46() const { return ___m_Contact46_46; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact46_46() { return &___m_Contact46_46; }
	inline void set_m_Contact46_46(ContactPoint2D_t3390240644  value)
	{
		___m_Contact46_46 = value;
	}

	inline static int32_t get_offset_of_m_Contact47_47() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact47_47)); }
	inline ContactPoint2D_t3390240644  get_m_Contact47_47() const { return ___m_Contact47_47; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact47_47() { return &___m_Contact47_47; }
	inline void set_m_Contact47_47(ContactPoint2D_t3390240644  value)
	{
		___m_Contact47_47 = value;
	}

	inline static int32_t get_offset_of_m_Contact48_48() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact48_48)); }
	inline ContactPoint2D_t3390240644  get_m_Contact48_48() const { return ___m_Contact48_48; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact48_48() { return &___m_Contact48_48; }
	inline void set_m_Contact48_48(ContactPoint2D_t3390240644  value)
	{
		___m_Contact48_48 = value;
	}

	inline static int32_t get_offset_of_m_Contact49_49() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact49_49)); }
	inline ContactPoint2D_t3390240644  get_m_Contact49_49() const { return ___m_Contact49_49; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact49_49() { return &___m_Contact49_49; }
	inline void set_m_Contact49_49(ContactPoint2D_t3390240644  value)
	{
		___m_Contact49_49 = value;
	}

	inline static int32_t get_offset_of_m_Contact50_50() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact50_50)); }
	inline ContactPoint2D_t3390240644  get_m_Contact50_50() const { return ___m_Contact50_50; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact50_50() { return &___m_Contact50_50; }
	inline void set_m_Contact50_50(ContactPoint2D_t3390240644  value)
	{
		___m_Contact50_50 = value;
	}

	inline static int32_t get_offset_of_m_Contact51_51() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact51_51)); }
	inline ContactPoint2D_t3390240644  get_m_Contact51_51() const { return ___m_Contact51_51; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact51_51() { return &___m_Contact51_51; }
	inline void set_m_Contact51_51(ContactPoint2D_t3390240644  value)
	{
		___m_Contact51_51 = value;
	}

	inline static int32_t get_offset_of_m_Contact52_52() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact52_52)); }
	inline ContactPoint2D_t3390240644  get_m_Contact52_52() const { return ___m_Contact52_52; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact52_52() { return &___m_Contact52_52; }
	inline void set_m_Contact52_52(ContactPoint2D_t3390240644  value)
	{
		___m_Contact52_52 = value;
	}

	inline static int32_t get_offset_of_m_Contact53_53() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact53_53)); }
	inline ContactPoint2D_t3390240644  get_m_Contact53_53() const { return ___m_Contact53_53; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact53_53() { return &___m_Contact53_53; }
	inline void set_m_Contact53_53(ContactPoint2D_t3390240644  value)
	{
		___m_Contact53_53 = value;
	}

	inline static int32_t get_offset_of_m_Contact54_54() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact54_54)); }
	inline ContactPoint2D_t3390240644  get_m_Contact54_54() const { return ___m_Contact54_54; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact54_54() { return &___m_Contact54_54; }
	inline void set_m_Contact54_54(ContactPoint2D_t3390240644  value)
	{
		___m_Contact54_54 = value;
	}

	inline static int32_t get_offset_of_m_Contact55_55() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact55_55)); }
	inline ContactPoint2D_t3390240644  get_m_Contact55_55() const { return ___m_Contact55_55; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact55_55() { return &___m_Contact55_55; }
	inline void set_m_Contact55_55(ContactPoint2D_t3390240644  value)
	{
		___m_Contact55_55 = value;
	}

	inline static int32_t get_offset_of_m_Contact56_56() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact56_56)); }
	inline ContactPoint2D_t3390240644  get_m_Contact56_56() const { return ___m_Contact56_56; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact56_56() { return &___m_Contact56_56; }
	inline void set_m_Contact56_56(ContactPoint2D_t3390240644  value)
	{
		___m_Contact56_56 = value;
	}

	inline static int32_t get_offset_of_m_Contact57_57() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact57_57)); }
	inline ContactPoint2D_t3390240644  get_m_Contact57_57() const { return ___m_Contact57_57; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact57_57() { return &___m_Contact57_57; }
	inline void set_m_Contact57_57(ContactPoint2D_t3390240644  value)
	{
		___m_Contact57_57 = value;
	}

	inline static int32_t get_offset_of_m_Contact58_58() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact58_58)); }
	inline ContactPoint2D_t3390240644  get_m_Contact58_58() const { return ___m_Contact58_58; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact58_58() { return &___m_Contact58_58; }
	inline void set_m_Contact58_58(ContactPoint2D_t3390240644  value)
	{
		___m_Contact58_58 = value;
	}

	inline static int32_t get_offset_of_m_Contact59_59() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact59_59)); }
	inline ContactPoint2D_t3390240644  get_m_Contact59_59() const { return ___m_Contact59_59; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact59_59() { return &___m_Contact59_59; }
	inline void set_m_Contact59_59(ContactPoint2D_t3390240644  value)
	{
		___m_Contact59_59 = value;
	}

	inline static int32_t get_offset_of_m_Contact60_60() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact60_60)); }
	inline ContactPoint2D_t3390240644  get_m_Contact60_60() const { return ___m_Contact60_60; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact60_60() { return &___m_Contact60_60; }
	inline void set_m_Contact60_60(ContactPoint2D_t3390240644  value)
	{
		___m_Contact60_60 = value;
	}

	inline static int32_t get_offset_of_m_Contact61_61() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact61_61)); }
	inline ContactPoint2D_t3390240644  get_m_Contact61_61() const { return ___m_Contact61_61; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact61_61() { return &___m_Contact61_61; }
	inline void set_m_Contact61_61(ContactPoint2D_t3390240644  value)
	{
		___m_Contact61_61 = value;
	}

	inline static int32_t get_offset_of_m_Contact62_62() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact62_62)); }
	inline ContactPoint2D_t3390240644  get_m_Contact62_62() const { return ___m_Contact62_62; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact62_62() { return &___m_Contact62_62; }
	inline void set_m_Contact62_62(ContactPoint2D_t3390240644  value)
	{
		___m_Contact62_62 = value;
	}

	inline static int32_t get_offset_of_m_Contact63_63() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact63_63)); }
	inline ContactPoint2D_t3390240644  get_m_Contact63_63() const { return ___m_Contact63_63; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact63_63() { return &___m_Contact63_63; }
	inline void set_m_Contact63_63(ContactPoint2D_t3390240644  value)
	{
		___m_Contact63_63 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDCONTACTPOINTS2D_T2523437281_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GUISTYLE_T3956901511_H
#define GUISTYLE_T3956901511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t3956901511  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t1397964415 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t1397964415 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t1397964415 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t1397964415 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t1397964415 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t1397964415 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t1397964415 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t1397964415 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1369453676 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1369453676 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1369453676 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1369453676 * ___m_Overflow_12;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Normal_1)); }
	inline GUIStyleState_t1397964415 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t1397964415 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Hover_2)); }
	inline GUIStyleState_t1397964415 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t1397964415 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Active_3)); }
	inline GUIStyleState_t1397964415 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t1397964415 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Focused_4)); }
	inline GUIStyleState_t1397964415 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t1397964415 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnNormal_5)); }
	inline GUIStyleState_t1397964415 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t1397964415 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnHover_6)); }
	inline GUIStyleState_t1397964415 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t1397964415 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnActive_7)); }
	inline GUIStyleState_t1397964415 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t1397964415 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnFocused_8)); }
	inline GUIStyleState_t1397964415 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t1397964415 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Border_9)); }
	inline RectOffset_t1369453676 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1369453676 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1369453676 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Padding_10)); }
	inline RectOffset_t1369453676 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1369453676 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Margin_11)); }
	inline RectOffset_t1369453676 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1369453676 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1369453676 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Overflow_12)); }
	inline RectOffset_t1369453676 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1369453676 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1369453676 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}
};

struct GUIStyle_t3956901511_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_13;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t3956901511 * ___s_None_14;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_13() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___showKeyboardFocus_13)); }
	inline bool get_showKeyboardFocus_13() const { return ___showKeyboardFocus_13; }
	inline bool* get_address_of_showKeyboardFocus_13() { return &___showKeyboardFocus_13; }
	inline void set_showKeyboardFocus_13(bool value)
	{
		___showKeyboardFocus_13 = value;
	}

	inline static int32_t get_offset_of_s_None_14() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___s_None_14)); }
	inline GUIStyle_t3956901511 * get_s_None_14() const { return ___s_None_14; }
	inline GUIStyle_t3956901511 ** get_address_of_s_None_14() { return &___s_None_14; }
	inline void set_s_None_14(GUIStyle_t3956901511 * value)
	{
		___s_None_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Overflow_12;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_com* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_com* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_com* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_com* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_com* ___m_Border_9;
	RectOffset_t1369453676_marshaled_com* ___m_Padding_10;
	RectOffset_t1369453676_marshaled_com* ___m_Margin_11;
	RectOffset_t1369453676_marshaled_com* ___m_Overflow_12;
};
#endif // GUISTYLE_T3956901511_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef FSMTEMPLATE_T1663266674_H
#define FSMTEMPLATE_T1663266674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FsmTemplate
struct  FsmTemplate_t1663266674  : public ScriptableObject_t2528358522
{
public:
	// System.String FsmTemplate::category
	String_t* ___category_4;
	// HutongGames.PlayMaker.Fsm FsmTemplate::fsm
	Fsm_t4127147824 * ___fsm_5;

public:
	inline static int32_t get_offset_of_category_4() { return static_cast<int32_t>(offsetof(FsmTemplate_t1663266674, ___category_4)); }
	inline String_t* get_category_4() const { return ___category_4; }
	inline String_t** get_address_of_category_4() { return &___category_4; }
	inline void set_category_4(String_t* value)
	{
		___category_4 = value;
		Il2CppCodeGenWriteBarrier((&___category_4), value);
	}

	inline static int32_t get_offset_of_fsm_5() { return static_cast<int32_t>(offsetof(FsmTemplate_t1663266674, ___fsm_5)); }
	inline Fsm_t4127147824 * get_fsm_5() const { return ___fsm_5; }
	inline Fsm_t4127147824 ** get_address_of_fsm_5() { return &___fsm_5; }
	inline void set_fsm_5(Fsm_t4127147824 * value)
	{
		___fsm_5 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMTEMPLATE_T1663266674_H
#ifndef ADDEVENTHANDLERDELEGATE_T772649125_H
#define ADDEVENTHANDLERDELEGATE_T772649125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerFSM/AddEventHandlerDelegate
struct  AddEventHandlerDelegate_t772649125  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDEVENTHANDLERDELEGATE_T772649125_H
#ifndef PLAYMAKERGLOBALS_T1863708399_H
#define PLAYMAKERGLOBALS_T1863708399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGlobals
struct  PlayMakerGlobals_t1863708399  : public ScriptableObject_t2528358522
{
public:
	// HutongGames.PlayMaker.FsmVariables PlayMakerGlobals::variables
	FsmVariables_t3349589544 * ___variables_10;
	// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::events
	List_1_t3319525431 * ___events_11;

public:
	inline static int32_t get_offset_of_variables_10() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399, ___variables_10)); }
	inline FsmVariables_t3349589544 * get_variables_10() const { return ___variables_10; }
	inline FsmVariables_t3349589544 ** get_address_of_variables_10() { return &___variables_10; }
	inline void set_variables_10(FsmVariables_t3349589544 * value)
	{
		___variables_10 = value;
		Il2CppCodeGenWriteBarrier((&___variables_10), value);
	}

	inline static int32_t get_offset_of_events_11() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399, ___events_11)); }
	inline List_1_t3319525431 * get_events_11() const { return ___events_11; }
	inline List_1_t3319525431 ** get_address_of_events_11() { return &___events_11; }
	inline void set_events_11(List_1_t3319525431 * value)
	{
		___events_11 = value;
		Il2CppCodeGenWriteBarrier((&___events_11), value);
	}
};

struct PlayMakerGlobals_t1863708399_StaticFields
{
public:
	// System.Boolean PlayMakerGlobals::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_4;
	// System.Boolean PlayMakerGlobals::<IsPlayingInEditor>k__BackingField
	bool ___U3CIsPlayingInEditorU3Ek__BackingField_5;
	// System.Boolean PlayMakerGlobals::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_6;
	// System.Boolean PlayMakerGlobals::<IsEditor>k__BackingField
	bool ___U3CIsEditorU3Ek__BackingField_7;
	// System.Boolean PlayMakerGlobals::<IsBuilding>k__BackingField
	bool ___U3CIsBuildingU3Ek__BackingField_8;
	// PlayMakerGlobals PlayMakerGlobals::instance
	PlayMakerGlobals_t1863708399 * ___instance_9;

public:
	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399_StaticFields, ___U3CInitializedU3Ek__BackingField_4)); }
	inline bool get_U3CInitializedU3Ek__BackingField_4() const { return ___U3CInitializedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_4() { return &___U3CInitializedU3Ek__BackingField_4; }
	inline void set_U3CInitializedU3Ek__BackingField_4(bool value)
	{
		___U3CInitializedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingInEditorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399_StaticFields, ___U3CIsPlayingInEditorU3Ek__BackingField_5)); }
	inline bool get_U3CIsPlayingInEditorU3Ek__BackingField_5() const { return ___U3CIsPlayingInEditorU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsPlayingInEditorU3Ek__BackingField_5() { return &___U3CIsPlayingInEditorU3Ek__BackingField_5; }
	inline void set_U3CIsPlayingInEditorU3Ek__BackingField_5(bool value)
	{
		___U3CIsPlayingInEditorU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399_StaticFields, ___U3CIsPlayingU3Ek__BackingField_6)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_6() const { return ___U3CIsPlayingU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_6() { return &___U3CIsPlayingU3Ek__BackingField_6; }
	inline void set_U3CIsPlayingU3Ek__BackingField_6(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsEditorU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399_StaticFields, ___U3CIsEditorU3Ek__BackingField_7)); }
	inline bool get_U3CIsEditorU3Ek__BackingField_7() const { return ___U3CIsEditorU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsEditorU3Ek__BackingField_7() { return &___U3CIsEditorU3Ek__BackingField_7; }
	inline void set_U3CIsEditorU3Ek__BackingField_7(bool value)
	{
		___U3CIsEditorU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsBuildingU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399_StaticFields, ___U3CIsBuildingU3Ek__BackingField_8)); }
	inline bool get_U3CIsBuildingU3Ek__BackingField_8() const { return ___U3CIsBuildingU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsBuildingU3Ek__BackingField_8() { return &___U3CIsBuildingU3Ek__BackingField_8; }
	inline void set_U3CIsBuildingU3Ek__BackingField_8(bool value)
	{
		___U3CIsBuildingU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(PlayMakerGlobals_t1863708399_StaticFields, ___instance_9)); }
	inline PlayMakerGlobals_t1863708399 * get_instance_9() const { return ___instance_9; }
	inline PlayMakerGlobals_t1863708399 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(PlayMakerGlobals_t1863708399 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERGLOBALS_T1863708399_H
#ifndef PLAYMAKERPREFS_T2834574540_H
#define PLAYMAKERPREFS_T2834574540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerPrefs
struct  PlayMakerPrefs_t2834574540  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean PlayMakerPrefs::logPerformanceWarnings
	bool ___logPerformanceWarnings_7;
	// System.Boolean PlayMakerPrefs::showEventHandlerComponents
	bool ___showEventHandlerComponents_8;
	// UnityEngine.Color[] PlayMakerPrefs::colors
	ColorU5BU5D_t941916413* ___colors_9;
	// System.String[] PlayMakerPrefs::colorNames
	StringU5BU5D_t1281789340* ___colorNames_10;
	// UnityEngine.Color PlayMakerPrefs::tweenFromColor
	Color_t2555686324  ___tweenFromColor_11;
	// UnityEngine.Color PlayMakerPrefs::tweenToColor
	Color_t2555686324  ___tweenToColor_12;

public:
	inline static int32_t get_offset_of_logPerformanceWarnings_7() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540, ___logPerformanceWarnings_7)); }
	inline bool get_logPerformanceWarnings_7() const { return ___logPerformanceWarnings_7; }
	inline bool* get_address_of_logPerformanceWarnings_7() { return &___logPerformanceWarnings_7; }
	inline void set_logPerformanceWarnings_7(bool value)
	{
		___logPerformanceWarnings_7 = value;
	}

	inline static int32_t get_offset_of_showEventHandlerComponents_8() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540, ___showEventHandlerComponents_8)); }
	inline bool get_showEventHandlerComponents_8() const { return ___showEventHandlerComponents_8; }
	inline bool* get_address_of_showEventHandlerComponents_8() { return &___showEventHandlerComponents_8; }
	inline void set_showEventHandlerComponents_8(bool value)
	{
		___showEventHandlerComponents_8 = value;
	}

	inline static int32_t get_offset_of_colors_9() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540, ___colors_9)); }
	inline ColorU5BU5D_t941916413* get_colors_9() const { return ___colors_9; }
	inline ColorU5BU5D_t941916413** get_address_of_colors_9() { return &___colors_9; }
	inline void set_colors_9(ColorU5BU5D_t941916413* value)
	{
		___colors_9 = value;
		Il2CppCodeGenWriteBarrier((&___colors_9), value);
	}

	inline static int32_t get_offset_of_colorNames_10() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540, ___colorNames_10)); }
	inline StringU5BU5D_t1281789340* get_colorNames_10() const { return ___colorNames_10; }
	inline StringU5BU5D_t1281789340** get_address_of_colorNames_10() { return &___colorNames_10; }
	inline void set_colorNames_10(StringU5BU5D_t1281789340* value)
	{
		___colorNames_10 = value;
		Il2CppCodeGenWriteBarrier((&___colorNames_10), value);
	}

	inline static int32_t get_offset_of_tweenFromColor_11() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540, ___tweenFromColor_11)); }
	inline Color_t2555686324  get_tweenFromColor_11() const { return ___tweenFromColor_11; }
	inline Color_t2555686324 * get_address_of_tweenFromColor_11() { return &___tweenFromColor_11; }
	inline void set_tweenFromColor_11(Color_t2555686324  value)
	{
		___tweenFromColor_11 = value;
	}

	inline static int32_t get_offset_of_tweenToColor_12() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540, ___tweenToColor_12)); }
	inline Color_t2555686324  get_tweenToColor_12() const { return ___tweenToColor_12; }
	inline Color_t2555686324 * get_address_of_tweenToColor_12() { return &___tweenToColor_12; }
	inline void set_tweenToColor_12(Color_t2555686324  value)
	{
		___tweenToColor_12 = value;
	}
};

struct PlayMakerPrefs_t2834574540_StaticFields
{
public:
	// PlayMakerPrefs PlayMakerPrefs::instance
	PlayMakerPrefs_t2834574540 * ___instance_4;
	// UnityEngine.Color[] PlayMakerPrefs::defaultColors
	ColorU5BU5D_t941916413* ___defaultColors_5;
	// System.String[] PlayMakerPrefs::defaultColorNames
	StringU5BU5D_t1281789340* ___defaultColorNames_6;
	// UnityEngine.Color[] PlayMakerPrefs::minimapColors
	ColorU5BU5D_t941916413* ___minimapColors_13;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540_StaticFields, ___instance_4)); }
	inline PlayMakerPrefs_t2834574540 * get_instance_4() const { return ___instance_4; }
	inline PlayMakerPrefs_t2834574540 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayMakerPrefs_t2834574540 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_defaultColors_5() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540_StaticFields, ___defaultColors_5)); }
	inline ColorU5BU5D_t941916413* get_defaultColors_5() const { return ___defaultColors_5; }
	inline ColorU5BU5D_t941916413** get_address_of_defaultColors_5() { return &___defaultColors_5; }
	inline void set_defaultColors_5(ColorU5BU5D_t941916413* value)
	{
		___defaultColors_5 = value;
		Il2CppCodeGenWriteBarrier((&___defaultColors_5), value);
	}

	inline static int32_t get_offset_of_defaultColorNames_6() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540_StaticFields, ___defaultColorNames_6)); }
	inline StringU5BU5D_t1281789340* get_defaultColorNames_6() const { return ___defaultColorNames_6; }
	inline StringU5BU5D_t1281789340** get_address_of_defaultColorNames_6() { return &___defaultColorNames_6; }
	inline void set_defaultColorNames_6(StringU5BU5D_t1281789340* value)
	{
		___defaultColorNames_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultColorNames_6), value);
	}

	inline static int32_t get_offset_of_minimapColors_13() { return static_cast<int32_t>(offsetof(PlayMakerPrefs_t2834574540_StaticFields, ___minimapColors_13)); }
	inline ColorU5BU5D_t941916413* get_minimapColors_13() const { return ___minimapColors_13; }
	inline ColorU5BU5D_t941916413** get_address_of_minimapColors_13() { return &___minimapColors_13; }
	inline void set_minimapColors_13(ColorU5BU5D_t941916413* value)
	{
		___minimapColors_13 = value;
		Il2CppCodeGenWriteBarrier((&___minimapColors_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERPREFS_T2834574540_H
#ifndef COLLISION2DEVENT_T1483235594_H
#define COLLISION2DEVENT_T1483235594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/Collision2DEvent
struct  Collision2DEvent_t1483235594  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISION2DEVENT_T1483235594_H
#ifndef COLLISIONEVENT_T2644514664_H
#define COLLISIONEVENT_T2644514664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/CollisionEvent
struct  CollisionEvent_t2644514664  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONEVENT_T2644514664_H
#ifndef CONTROLLERCOLLISIONEVENT_T2755027817_H
#define CONTROLLERCOLLISIONEVENT_T2755027817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/ControllerCollisionEvent
struct  ControllerCollisionEvent_t2755027817  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERCOLLISIONEVENT_T2755027817_H
#ifndef PARTICLECOLLISIONEVENT_T3984792766_H
#define PARTICLECOLLISIONEVENT_T3984792766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/ParticleCollisionEvent
struct  ParticleCollisionEvent_t3984792766  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISIONEVENT_T3984792766_H
#ifndef TRIGGER2DEVENT_T2411364400_H
#define TRIGGER2DEVENT_T2411364400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/Trigger2DEvent
struct  Trigger2DEvent_t2411364400  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER2DEVENT_T2411364400_H
#ifndef TRIGGEREVENT_T1933226311_H
#define TRIGGEREVENT_T1933226311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/TriggerEvent
struct  TriggerEvent_t1933226311  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T1933226311_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef COMPARISON_1_T1387941410_H
#define COMPARISON_1_T1387941410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Comparison`1<PlayMakerFSM>
struct  Comparison_1_t1387941410  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISON_1_T1387941410_H
#ifndef PREDICATE_1_T2672744813_H
#define PREDICATE_1_T2672744813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<System.String>
struct  Predicate_1_t2672744813  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T2672744813_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef COLLISION2D_T2842956331_H
#define COLLISION2D_T2842956331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision2D
struct  Collision2D_t2842956331  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t2156229523  ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.CachedContactPoints2D UnityEngine.Collision2D::m_CachedContactPoints
	CachedContactPoints2D_t2523437281  ___m_CachedContactPoints_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContactArray
	ContactPoint2DU5BU5D_t96683501* ___m_LegacyContactArray_8;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_4() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_RelativeVelocity_4)); }
	inline Vector2_t2156229523  get_m_RelativeVelocity_4() const { return ___m_RelativeVelocity_4; }
	inline Vector2_t2156229523 * get_address_of_m_RelativeVelocity_4() { return &___m_RelativeVelocity_4; }
	inline void set_m_RelativeVelocity_4(Vector2_t2156229523  value)
	{
		___m_RelativeVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_5() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Enabled_5)); }
	inline int32_t get_m_Enabled_5() const { return ___m_Enabled_5; }
	inline int32_t* get_address_of_m_Enabled_5() { return &___m_Enabled_5; }
	inline void set_m_Enabled_5(int32_t value)
	{
		___m_Enabled_5 = value;
	}

	inline static int32_t get_offset_of_m_ContactCount_6() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_ContactCount_6)); }
	inline int32_t get_m_ContactCount_6() const { return ___m_ContactCount_6; }
	inline int32_t* get_address_of_m_ContactCount_6() { return &___m_ContactCount_6; }
	inline void set_m_ContactCount_6(int32_t value)
	{
		___m_ContactCount_6 = value;
	}

	inline static int32_t get_offset_of_m_CachedContactPoints_7() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_CachedContactPoints_7)); }
	inline CachedContactPoints2D_t2523437281  get_m_CachedContactPoints_7() const { return ___m_CachedContactPoints_7; }
	inline CachedContactPoints2D_t2523437281 * get_address_of_m_CachedContactPoints_7() { return &___m_CachedContactPoints_7; }
	inline void set_m_CachedContactPoints_7(CachedContactPoints2D_t2523437281  value)
	{
		___m_CachedContactPoints_7 = value;
	}

	inline static int32_t get_offset_of_m_LegacyContactArray_8() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_LegacyContactArray_8)); }
	inline ContactPoint2DU5BU5D_t96683501* get_m_LegacyContactArray_8() const { return ___m_LegacyContactArray_8; }
	inline ContactPoint2DU5BU5D_t96683501** get_address_of_m_LegacyContactArray_8() { return &___m_LegacyContactArray_8; }
	inline void set_m_LegacyContactArray_8(ContactPoint2DU5BU5D_t96683501* value)
	{
		___m_LegacyContactArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyContactArray_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t2842956331_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t2156229523  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	CachedContactPoints2D_t2523437281  ___m_CachedContactPoints_7;
	ContactPoint2D_t3390240644 * ___m_LegacyContactArray_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t2842956331_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t2156229523  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	CachedContactPoints2D_t2523437281  ___m_CachedContactPoints_7;
	ContactPoint2D_t3390240644 * ___m_LegacyContactArray_8;
};
#endif // COLLISION2D_T2842956331_H
#ifndef GUISKIN_T1244372282_H
#define GUISKIN_T1244372282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t1244372282  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1956802104 * ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t3956901511 * ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t3956901511 * ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t3956901511 * ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t3956901511 * ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t3956901511 * ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t3956901511 * ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t3956901511 * ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t3956901511 * ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t3956901511 * ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t3956901511 * ___m_verticalSlider_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t3956901511 * ___m_verticalSliderThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t3956901511 * ___m_horizontalScrollbar_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t3956901511 * ___m_horizontalScrollbarThumb_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarLeftButton_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarRightButton_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t3956901511 * ___m_verticalScrollbar_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t3956901511 * ___m_verticalScrollbarThumb_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarUpButton_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarDownButton_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t3956901511 * ___m_ScrollView_24;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2383250302* ___m_CustomStyles_25;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1774757634 * ___m_Settings_26;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t3742157810 * ___m_Styles_28;

public:
	inline static int32_t get_offset_of_m_Font_4() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Font_4)); }
	inline Font_t1956802104 * get_m_Font_4() const { return ___m_Font_4; }
	inline Font_t1956802104 ** get_address_of_m_Font_4() { return &___m_Font_4; }
	inline void set_m_Font_4(Font_t1956802104 * value)
	{
		___m_Font_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_4), value);
	}

	inline static int32_t get_offset_of_m_box_5() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_box_5)); }
	inline GUIStyle_t3956901511 * get_m_box_5() const { return ___m_box_5; }
	inline GUIStyle_t3956901511 ** get_address_of_m_box_5() { return &___m_box_5; }
	inline void set_m_box_5(GUIStyle_t3956901511 * value)
	{
		___m_box_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_5), value);
	}

	inline static int32_t get_offset_of_m_button_6() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_button_6)); }
	inline GUIStyle_t3956901511 * get_m_button_6() const { return ___m_button_6; }
	inline GUIStyle_t3956901511 ** get_address_of_m_button_6() { return &___m_button_6; }
	inline void set_m_button_6(GUIStyle_t3956901511 * value)
	{
		___m_button_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_6), value);
	}

	inline static int32_t get_offset_of_m_toggle_7() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_toggle_7)); }
	inline GUIStyle_t3956901511 * get_m_toggle_7() const { return ___m_toggle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_toggle_7() { return &___m_toggle_7; }
	inline void set_m_toggle_7(GUIStyle_t3956901511 * value)
	{
		___m_toggle_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_7), value);
	}

	inline static int32_t get_offset_of_m_label_8() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_label_8)); }
	inline GUIStyle_t3956901511 * get_m_label_8() const { return ___m_label_8; }
	inline GUIStyle_t3956901511 ** get_address_of_m_label_8() { return &___m_label_8; }
	inline void set_m_label_8(GUIStyle_t3956901511 * value)
	{
		___m_label_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_8), value);
	}

	inline static int32_t get_offset_of_m_textField_9() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textField_9)); }
	inline GUIStyle_t3956901511 * get_m_textField_9() const { return ___m_textField_9; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textField_9() { return &___m_textField_9; }
	inline void set_m_textField_9(GUIStyle_t3956901511 * value)
	{
		___m_textField_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_9), value);
	}

	inline static int32_t get_offset_of_m_textArea_10() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textArea_10)); }
	inline GUIStyle_t3956901511 * get_m_textArea_10() const { return ___m_textArea_10; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textArea_10() { return &___m_textArea_10; }
	inline void set_m_textArea_10(GUIStyle_t3956901511 * value)
	{
		___m_textArea_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_10), value);
	}

	inline static int32_t get_offset_of_m_window_11() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_window_11)); }
	inline GUIStyle_t3956901511 * get_m_window_11() const { return ___m_window_11; }
	inline GUIStyle_t3956901511 ** get_address_of_m_window_11() { return &___m_window_11; }
	inline void set_m_window_11(GUIStyle_t3956901511 * value)
	{
		___m_window_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_11), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSlider_12)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSlider_12() const { return ___m_horizontalSlider_12; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSlider_12() { return &___m_horizontalSlider_12; }
	inline void set_m_horizontalSlider_12(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSliderThumb_13)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSliderThumb_13() const { return ___m_horizontalSliderThumb_13; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSliderThumb_13() { return &___m_horizontalSliderThumb_13; }
	inline void set_m_horizontalSliderThumb_13(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_14() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSlider_14)); }
	inline GUIStyle_t3956901511 * get_m_verticalSlider_14() const { return ___m_verticalSlider_14; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSlider_14() { return &___m_verticalSlider_14; }
	inline void set_m_verticalSlider_14(GUIStyle_t3956901511 * value)
	{
		___m_verticalSlider_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_14), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSliderThumb_15)); }
	inline GUIStyle_t3956901511 * get_m_verticalSliderThumb_15() const { return ___m_verticalSliderThumb_15; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSliderThumb_15() { return &___m_verticalSliderThumb_15; }
	inline void set_m_verticalSliderThumb_15(GUIStyle_t3956901511 * value)
	{
		___m_verticalSliderThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_16() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbar_16)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbar_16() const { return ___m_horizontalScrollbar_16; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbar_16() { return &___m_horizontalScrollbar_16; }
	inline void set_m_horizontalScrollbar_16(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbar_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_17() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarThumb_17)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarThumb_17() const { return ___m_horizontalScrollbarThumb_17; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarThumb_17() { return &___m_horizontalScrollbarThumb_17; }
	inline void set_m_horizontalScrollbarThumb_17(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarThumb_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_17), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_18() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarLeftButton_18)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarLeftButton_18() const { return ___m_horizontalScrollbarLeftButton_18; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarLeftButton_18() { return &___m_horizontalScrollbarLeftButton_18; }
	inline void set_m_horizontalScrollbarLeftButton_18(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarLeftButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_18), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_19() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarRightButton_19)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarRightButton_19() const { return ___m_horizontalScrollbarRightButton_19; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarRightButton_19() { return &___m_horizontalScrollbarRightButton_19; }
	inline void set_m_horizontalScrollbarRightButton_19(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarRightButton_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_20() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbar_20)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbar_20() const { return ___m_verticalScrollbar_20; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbar_20() { return &___m_verticalScrollbar_20; }
	inline void set_m_verticalScrollbar_20(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_21() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarThumb_21)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarThumb_21() const { return ___m_verticalScrollbarThumb_21; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarThumb_21() { return &___m_verticalScrollbarThumb_21; }
	inline void set_m_verticalScrollbarThumb_21(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarThumb_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_21), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_22() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarUpButton_22)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarUpButton_22() const { return ___m_verticalScrollbarUpButton_22; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarUpButton_22() { return &___m_verticalScrollbarUpButton_22; }
	inline void set_m_verticalScrollbarUpButton_22(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarUpButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_22), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_23() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarDownButton_23)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarDownButton_23() const { return ___m_verticalScrollbarDownButton_23; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarDownButton_23() { return &___m_verticalScrollbarDownButton_23; }
	inline void set_m_verticalScrollbarDownButton_23(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarDownButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_23), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_24() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_ScrollView_24)); }
	inline GUIStyle_t3956901511 * get_m_ScrollView_24() const { return ___m_ScrollView_24; }
	inline GUIStyle_t3956901511 ** get_address_of_m_ScrollView_24() { return &___m_ScrollView_24; }
	inline void set_m_ScrollView_24(GUIStyle_t3956901511 * value)
	{
		___m_ScrollView_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_24), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_25() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_CustomStyles_25)); }
	inline GUIStyleU5BU5D_t2383250302* get_m_CustomStyles_25() const { return ___m_CustomStyles_25; }
	inline GUIStyleU5BU5D_t2383250302** get_address_of_m_CustomStyles_25() { return &___m_CustomStyles_25; }
	inline void set_m_CustomStyles_25(GUIStyleU5BU5D_t2383250302* value)
	{
		___m_CustomStyles_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_25), value);
	}

	inline static int32_t get_offset_of_m_Settings_26() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Settings_26)); }
	inline GUISettings_t1774757634 * get_m_Settings_26() const { return ___m_Settings_26; }
	inline GUISettings_t1774757634 ** get_address_of_m_Settings_26() { return &___m_Settings_26; }
	inline void set_m_Settings_26(GUISettings_t1774757634 * value)
	{
		___m_Settings_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_26), value);
	}

	inline static int32_t get_offset_of_m_Styles_28() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Styles_28)); }
	inline Dictionary_2_t3742157810 * get_m_Styles_28() const { return ___m_Styles_28; }
	inline Dictionary_2_t3742157810 ** get_address_of_m_Styles_28() { return &___m_Styles_28; }
	inline void set_m_Styles_28(Dictionary_2_t3742157810 * value)
	{
		___m_Styles_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_28), value);
	}
};

struct GUISkin_t1244372282_StaticFields
{
public:
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t3956901511 * ___ms_Error_27;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t1143955295 * ___m_SkinChanged_29;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t1244372282 * ___current_30;

public:
	inline static int32_t get_offset_of_ms_Error_27() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___ms_Error_27)); }
	inline GUIStyle_t3956901511 * get_ms_Error_27() const { return ___ms_Error_27; }
	inline GUIStyle_t3956901511 ** get_address_of_ms_Error_27() { return &___ms_Error_27; }
	inline void set_ms_Error_27(GUIStyle_t3956901511 * value)
	{
		___ms_Error_27 = value;
		Il2CppCodeGenWriteBarrier((&___ms_Error_27), value);
	}

	inline static int32_t get_offset_of_m_SkinChanged_29() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___m_SkinChanged_29)); }
	inline SkinChangedDelegate_t1143955295 * get_m_SkinChanged_29() const { return ___m_SkinChanged_29; }
	inline SkinChangedDelegate_t1143955295 ** get_address_of_m_SkinChanged_29() { return &___m_SkinChanged_29; }
	inline void set_m_SkinChanged_29(SkinChangedDelegate_t1143955295 * value)
	{
		___m_SkinChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_29), value);
	}

	inline static int32_t get_offset_of_current_30() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___current_30)); }
	inline GUISkin_t1244372282 * get_current_30() const { return ___current_30; }
	inline GUISkin_t1244372282 ** get_address_of_current_30() { return &___current_30; }
	inline void set_current_30(GUISkin_t1244372282 * value)
	{
		___current_30 = value;
		Il2CppCodeGenWriteBarrier((&___current_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T1244372282_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t190067161 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t190067161 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t190067161 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t190067161 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t190067161 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t190067161 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef GUIELEMENT_T3567083079_H
#define GUIELEMENT_T3567083079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t3567083079  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T3567083079_H
#ifndef JOINT2D_T4180440564_H
#define JOINT2D_T4180440564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Joint2D
struct  Joint2D_t4180440564  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINT2D_T4180440564_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PLAYMAKERFSM_T1613010231_H
#define PLAYMAKERFSM_T1613010231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerFSM
struct  PlayMakerFSM_t1613010231  : public MonoBehaviour_t3962482529
{
public:
	// HutongGames.PlayMaker.Fsm PlayMakerFSM::fsm
	Fsm_t4127147824 * ___fsm_8;
	// FsmTemplate PlayMakerFSM::fsmTemplate
	FsmTemplate_t1663266674 * ___fsmTemplate_9;
	// System.Boolean PlayMakerFSM::eventHandlerComponentsAdded
	bool ___eventHandlerComponentsAdded_10;
	// PlayMakerFSM/AddEventHandlerDelegate PlayMakerFSM::addEventHandlers
	AddEventHandlerDelegate_t772649125 * ___addEventHandlers_12;
	// UnityEngine.GUITexture PlayMakerFSM::_guiTexture
	GUITexture_t951903601 * ____guiTexture_13;
	// UnityEngine.GUIText PlayMakerFSM::_guiText
	GUIText_t402233326 * ____guiText_14;

public:
	inline static int32_t get_offset_of_fsm_8() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231, ___fsm_8)); }
	inline Fsm_t4127147824 * get_fsm_8() const { return ___fsm_8; }
	inline Fsm_t4127147824 ** get_address_of_fsm_8() { return &___fsm_8; }
	inline void set_fsm_8(Fsm_t4127147824 * value)
	{
		___fsm_8 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_8), value);
	}

	inline static int32_t get_offset_of_fsmTemplate_9() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231, ___fsmTemplate_9)); }
	inline FsmTemplate_t1663266674 * get_fsmTemplate_9() const { return ___fsmTemplate_9; }
	inline FsmTemplate_t1663266674 ** get_address_of_fsmTemplate_9() { return &___fsmTemplate_9; }
	inline void set_fsmTemplate_9(FsmTemplate_t1663266674 * value)
	{
		___fsmTemplate_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmTemplate_9), value);
	}

	inline static int32_t get_offset_of_eventHandlerComponentsAdded_10() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231, ___eventHandlerComponentsAdded_10)); }
	inline bool get_eventHandlerComponentsAdded_10() const { return ___eventHandlerComponentsAdded_10; }
	inline bool* get_address_of_eventHandlerComponentsAdded_10() { return &___eventHandlerComponentsAdded_10; }
	inline void set_eventHandlerComponentsAdded_10(bool value)
	{
		___eventHandlerComponentsAdded_10 = value;
	}

	inline static int32_t get_offset_of_addEventHandlers_12() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231, ___addEventHandlers_12)); }
	inline AddEventHandlerDelegate_t772649125 * get_addEventHandlers_12() const { return ___addEventHandlers_12; }
	inline AddEventHandlerDelegate_t772649125 ** get_address_of_addEventHandlers_12() { return &___addEventHandlers_12; }
	inline void set_addEventHandlers_12(AddEventHandlerDelegate_t772649125 * value)
	{
		___addEventHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((&___addEventHandlers_12), value);
	}

	inline static int32_t get_offset_of__guiTexture_13() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231, ____guiTexture_13)); }
	inline GUITexture_t951903601 * get__guiTexture_13() const { return ____guiTexture_13; }
	inline GUITexture_t951903601 ** get_address_of__guiTexture_13() { return &____guiTexture_13; }
	inline void set__guiTexture_13(GUITexture_t951903601 * value)
	{
		____guiTexture_13 = value;
		Il2CppCodeGenWriteBarrier((&____guiTexture_13), value);
	}

	inline static int32_t get_offset_of__guiText_14() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231, ____guiText_14)); }
	inline GUIText_t402233326 * get__guiText_14() const { return ____guiText_14; }
	inline GUIText_t402233326 ** get_address_of__guiText_14() { return &____guiText_14; }
	inline void set__guiText_14(GUIText_t402233326 * value)
	{
		____guiText_14 = value;
		Il2CppCodeGenWriteBarrier((&____guiText_14), value);
	}
};

struct PlayMakerFSM_t1613010231_StaticFields
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::fsmList
	List_1_t3085084973 * ___fsmList_4;
	// System.Boolean PlayMakerFSM::MaximizeFileCompatibility
	bool ___MaximizeFileCompatibility_5;
	// System.Boolean PlayMakerFSM::ApplicationIsQuitting
	bool ___ApplicationIsQuitting_6;
	// System.Boolean PlayMakerFSM::NotMainThread
	bool ___NotMainThread_7;
	// System.Boolean PlayMakerFSM::<DrawGizmos>k__BackingField
	bool ___U3CDrawGizmosU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_fsmList_4() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231_StaticFields, ___fsmList_4)); }
	inline List_1_t3085084973 * get_fsmList_4() const { return ___fsmList_4; }
	inline List_1_t3085084973 ** get_address_of_fsmList_4() { return &___fsmList_4; }
	inline void set_fsmList_4(List_1_t3085084973 * value)
	{
		___fsmList_4 = value;
		Il2CppCodeGenWriteBarrier((&___fsmList_4), value);
	}

	inline static int32_t get_offset_of_MaximizeFileCompatibility_5() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231_StaticFields, ___MaximizeFileCompatibility_5)); }
	inline bool get_MaximizeFileCompatibility_5() const { return ___MaximizeFileCompatibility_5; }
	inline bool* get_address_of_MaximizeFileCompatibility_5() { return &___MaximizeFileCompatibility_5; }
	inline void set_MaximizeFileCompatibility_5(bool value)
	{
		___MaximizeFileCompatibility_5 = value;
	}

	inline static int32_t get_offset_of_ApplicationIsQuitting_6() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231_StaticFields, ___ApplicationIsQuitting_6)); }
	inline bool get_ApplicationIsQuitting_6() const { return ___ApplicationIsQuitting_6; }
	inline bool* get_address_of_ApplicationIsQuitting_6() { return &___ApplicationIsQuitting_6; }
	inline void set_ApplicationIsQuitting_6(bool value)
	{
		___ApplicationIsQuitting_6 = value;
	}

	inline static int32_t get_offset_of_NotMainThread_7() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231_StaticFields, ___NotMainThread_7)); }
	inline bool get_NotMainThread_7() const { return ___NotMainThread_7; }
	inline bool* get_address_of_NotMainThread_7() { return &___NotMainThread_7; }
	inline void set_NotMainThread_7(bool value)
	{
		___NotMainThread_7 = value;
	}

	inline static int32_t get_offset_of_U3CDrawGizmosU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PlayMakerFSM_t1613010231_StaticFields, ___U3CDrawGizmosU3Ek__BackingField_11)); }
	inline bool get_U3CDrawGizmosU3Ek__BackingField_11() const { return ___U3CDrawGizmosU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CDrawGizmosU3Ek__BackingField_11() { return &___U3CDrawGizmosU3Ek__BackingField_11; }
	inline void set_U3CDrawGizmosU3Ek__BackingField_11(bool value)
	{
		___U3CDrawGizmosU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERFSM_T1613010231_H
#ifndef PLAYMAKERGUI_T1571180761_H
#define PLAYMAKERGUI_T1571180761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGUI
struct  PlayMakerGUI_t1571180761  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PlayMakerGUI::previewOnGUI
	bool ___previewOnGUI_7;
	// System.Boolean PlayMakerGUI::enableGUILayout
	bool ___enableGUILayout_8;
	// System.Boolean PlayMakerGUI::drawStateLabels
	bool ___drawStateLabels_9;
	// System.Boolean PlayMakerGUI::enableStateLabelsInBuilds
	bool ___enableStateLabelsInBuilds_10;
	// System.Boolean PlayMakerGUI::GUITextureStateLabels
	bool ___GUITextureStateLabels_11;
	// System.Boolean PlayMakerGUI::GUITextStateLabels
	bool ___GUITextStateLabels_12;
	// System.Boolean PlayMakerGUI::filterLabelsWithDistance
	bool ___filterLabelsWithDistance_13;
	// System.Single PlayMakerGUI::maxLabelDistance
	float ___maxLabelDistance_14;
	// System.Boolean PlayMakerGUI::controlMouseCursor
	bool ___controlMouseCursor_15;
	// System.Single PlayMakerGUI::labelScale
	float ___labelScale_16;
	// System.Single PlayMakerGUI::initLabelScale
	float ___initLabelScale_32;

public:
	inline static int32_t get_offset_of_previewOnGUI_7() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___previewOnGUI_7)); }
	inline bool get_previewOnGUI_7() const { return ___previewOnGUI_7; }
	inline bool* get_address_of_previewOnGUI_7() { return &___previewOnGUI_7; }
	inline void set_previewOnGUI_7(bool value)
	{
		___previewOnGUI_7 = value;
	}

	inline static int32_t get_offset_of_enableGUILayout_8() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___enableGUILayout_8)); }
	inline bool get_enableGUILayout_8() const { return ___enableGUILayout_8; }
	inline bool* get_address_of_enableGUILayout_8() { return &___enableGUILayout_8; }
	inline void set_enableGUILayout_8(bool value)
	{
		___enableGUILayout_8 = value;
	}

	inline static int32_t get_offset_of_drawStateLabels_9() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___drawStateLabels_9)); }
	inline bool get_drawStateLabels_9() const { return ___drawStateLabels_9; }
	inline bool* get_address_of_drawStateLabels_9() { return &___drawStateLabels_9; }
	inline void set_drawStateLabels_9(bool value)
	{
		___drawStateLabels_9 = value;
	}

	inline static int32_t get_offset_of_enableStateLabelsInBuilds_10() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___enableStateLabelsInBuilds_10)); }
	inline bool get_enableStateLabelsInBuilds_10() const { return ___enableStateLabelsInBuilds_10; }
	inline bool* get_address_of_enableStateLabelsInBuilds_10() { return &___enableStateLabelsInBuilds_10; }
	inline void set_enableStateLabelsInBuilds_10(bool value)
	{
		___enableStateLabelsInBuilds_10 = value;
	}

	inline static int32_t get_offset_of_GUITextureStateLabels_11() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___GUITextureStateLabels_11)); }
	inline bool get_GUITextureStateLabels_11() const { return ___GUITextureStateLabels_11; }
	inline bool* get_address_of_GUITextureStateLabels_11() { return &___GUITextureStateLabels_11; }
	inline void set_GUITextureStateLabels_11(bool value)
	{
		___GUITextureStateLabels_11 = value;
	}

	inline static int32_t get_offset_of_GUITextStateLabels_12() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___GUITextStateLabels_12)); }
	inline bool get_GUITextStateLabels_12() const { return ___GUITextStateLabels_12; }
	inline bool* get_address_of_GUITextStateLabels_12() { return &___GUITextStateLabels_12; }
	inline void set_GUITextStateLabels_12(bool value)
	{
		___GUITextStateLabels_12 = value;
	}

	inline static int32_t get_offset_of_filterLabelsWithDistance_13() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___filterLabelsWithDistance_13)); }
	inline bool get_filterLabelsWithDistance_13() const { return ___filterLabelsWithDistance_13; }
	inline bool* get_address_of_filterLabelsWithDistance_13() { return &___filterLabelsWithDistance_13; }
	inline void set_filterLabelsWithDistance_13(bool value)
	{
		___filterLabelsWithDistance_13 = value;
	}

	inline static int32_t get_offset_of_maxLabelDistance_14() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___maxLabelDistance_14)); }
	inline float get_maxLabelDistance_14() const { return ___maxLabelDistance_14; }
	inline float* get_address_of_maxLabelDistance_14() { return &___maxLabelDistance_14; }
	inline void set_maxLabelDistance_14(float value)
	{
		___maxLabelDistance_14 = value;
	}

	inline static int32_t get_offset_of_controlMouseCursor_15() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___controlMouseCursor_15)); }
	inline bool get_controlMouseCursor_15() const { return ___controlMouseCursor_15; }
	inline bool* get_address_of_controlMouseCursor_15() { return &___controlMouseCursor_15; }
	inline void set_controlMouseCursor_15(bool value)
	{
		___controlMouseCursor_15 = value;
	}

	inline static int32_t get_offset_of_labelScale_16() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___labelScale_16)); }
	inline float get_labelScale_16() const { return ___labelScale_16; }
	inline float* get_address_of_labelScale_16() { return &___labelScale_16; }
	inline void set_labelScale_16(float value)
	{
		___labelScale_16 = value;
	}

	inline static int32_t get_offset_of_initLabelScale_32() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761, ___initLabelScale_32)); }
	inline float get_initLabelScale_32() const { return ___initLabelScale_32; }
	inline float* get_address_of_initLabelScale_32() { return &___initLabelScale_32; }
	inline void set_initLabelScale_32(float value)
	{
		___initLabelScale_32 = value;
	}
};

struct PlayMakerGUI_t1571180761_StaticFields
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::fsmList
	List_1_t3085084973 * ___fsmList_4;
	// HutongGames.PlayMaker.Fsm PlayMakerGUI::SelectedFSM
	Fsm_t4127147824 * ___SelectedFSM_5;
	// UnityEngine.GUIContent PlayMakerGUI::labelContent
	GUIContent_t3050628031 * ___labelContent_6;
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::SortedFsmList
	List_1_t3085084973 * ___SortedFsmList_17;
	// UnityEngine.GameObject PlayMakerGUI::labelGameObject
	GameObject_t1113636619 * ___labelGameObject_18;
	// System.Single PlayMakerGUI::fsmLabelIndex
	float ___fsmLabelIndex_19;
	// PlayMakerGUI PlayMakerGUI::instance
	PlayMakerGUI_t1571180761 * ___instance_20;
	// UnityEngine.GUISkin PlayMakerGUI::guiSkin
	GUISkin_t1244372282 * ___guiSkin_21;
	// UnityEngine.Color PlayMakerGUI::guiColor
	Color_t2555686324  ___guiColor_22;
	// UnityEngine.Color PlayMakerGUI::guiBackgroundColor
	Color_t2555686324  ___guiBackgroundColor_23;
	// UnityEngine.Color PlayMakerGUI::guiContentColor
	Color_t2555686324  ___guiContentColor_24;
	// UnityEngine.Matrix4x4 PlayMakerGUI::guiMatrix
	Matrix4x4_t1817901843  ___guiMatrix_25;
	// UnityEngine.Texture PlayMakerGUI::<MouseCursor>k__BackingField
	Texture_t3661962703 * ___U3CMouseCursorU3Ek__BackingField_26;
	// System.Boolean PlayMakerGUI::<LockCursor>k__BackingField
	bool ___U3CLockCursorU3Ek__BackingField_27;
	// System.Boolean PlayMakerGUI::<HideCursor>k__BackingField
	bool ___U3CHideCursorU3Ek__BackingField_28;
	// UnityEngine.GUIStyle PlayMakerGUI::stateLabelStyle
	GUIStyle_t3956901511 * ___stateLabelStyle_30;
	// UnityEngine.Texture2D PlayMakerGUI::stateLabelBackground
	Texture2D_t3840446185 * ___stateLabelBackground_31;

public:
	inline static int32_t get_offset_of_fsmList_4() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___fsmList_4)); }
	inline List_1_t3085084973 * get_fsmList_4() const { return ___fsmList_4; }
	inline List_1_t3085084973 ** get_address_of_fsmList_4() { return &___fsmList_4; }
	inline void set_fsmList_4(List_1_t3085084973 * value)
	{
		___fsmList_4 = value;
		Il2CppCodeGenWriteBarrier((&___fsmList_4), value);
	}

	inline static int32_t get_offset_of_SelectedFSM_5() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___SelectedFSM_5)); }
	inline Fsm_t4127147824 * get_SelectedFSM_5() const { return ___SelectedFSM_5; }
	inline Fsm_t4127147824 ** get_address_of_SelectedFSM_5() { return &___SelectedFSM_5; }
	inline void set_SelectedFSM_5(Fsm_t4127147824 * value)
	{
		___SelectedFSM_5 = value;
		Il2CppCodeGenWriteBarrier((&___SelectedFSM_5), value);
	}

	inline static int32_t get_offset_of_labelContent_6() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___labelContent_6)); }
	inline GUIContent_t3050628031 * get_labelContent_6() const { return ___labelContent_6; }
	inline GUIContent_t3050628031 ** get_address_of_labelContent_6() { return &___labelContent_6; }
	inline void set_labelContent_6(GUIContent_t3050628031 * value)
	{
		___labelContent_6 = value;
		Il2CppCodeGenWriteBarrier((&___labelContent_6), value);
	}

	inline static int32_t get_offset_of_SortedFsmList_17() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___SortedFsmList_17)); }
	inline List_1_t3085084973 * get_SortedFsmList_17() const { return ___SortedFsmList_17; }
	inline List_1_t3085084973 ** get_address_of_SortedFsmList_17() { return &___SortedFsmList_17; }
	inline void set_SortedFsmList_17(List_1_t3085084973 * value)
	{
		___SortedFsmList_17 = value;
		Il2CppCodeGenWriteBarrier((&___SortedFsmList_17), value);
	}

	inline static int32_t get_offset_of_labelGameObject_18() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___labelGameObject_18)); }
	inline GameObject_t1113636619 * get_labelGameObject_18() const { return ___labelGameObject_18; }
	inline GameObject_t1113636619 ** get_address_of_labelGameObject_18() { return &___labelGameObject_18; }
	inline void set_labelGameObject_18(GameObject_t1113636619 * value)
	{
		___labelGameObject_18 = value;
		Il2CppCodeGenWriteBarrier((&___labelGameObject_18), value);
	}

	inline static int32_t get_offset_of_fsmLabelIndex_19() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___fsmLabelIndex_19)); }
	inline float get_fsmLabelIndex_19() const { return ___fsmLabelIndex_19; }
	inline float* get_address_of_fsmLabelIndex_19() { return &___fsmLabelIndex_19; }
	inline void set_fsmLabelIndex_19(float value)
	{
		___fsmLabelIndex_19 = value;
	}

	inline static int32_t get_offset_of_instance_20() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___instance_20)); }
	inline PlayMakerGUI_t1571180761 * get_instance_20() const { return ___instance_20; }
	inline PlayMakerGUI_t1571180761 ** get_address_of_instance_20() { return &___instance_20; }
	inline void set_instance_20(PlayMakerGUI_t1571180761 * value)
	{
		___instance_20 = value;
		Il2CppCodeGenWriteBarrier((&___instance_20), value);
	}

	inline static int32_t get_offset_of_guiSkin_21() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___guiSkin_21)); }
	inline GUISkin_t1244372282 * get_guiSkin_21() const { return ___guiSkin_21; }
	inline GUISkin_t1244372282 ** get_address_of_guiSkin_21() { return &___guiSkin_21; }
	inline void set_guiSkin_21(GUISkin_t1244372282 * value)
	{
		___guiSkin_21 = value;
		Il2CppCodeGenWriteBarrier((&___guiSkin_21), value);
	}

	inline static int32_t get_offset_of_guiColor_22() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___guiColor_22)); }
	inline Color_t2555686324  get_guiColor_22() const { return ___guiColor_22; }
	inline Color_t2555686324 * get_address_of_guiColor_22() { return &___guiColor_22; }
	inline void set_guiColor_22(Color_t2555686324  value)
	{
		___guiColor_22 = value;
	}

	inline static int32_t get_offset_of_guiBackgroundColor_23() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___guiBackgroundColor_23)); }
	inline Color_t2555686324  get_guiBackgroundColor_23() const { return ___guiBackgroundColor_23; }
	inline Color_t2555686324 * get_address_of_guiBackgroundColor_23() { return &___guiBackgroundColor_23; }
	inline void set_guiBackgroundColor_23(Color_t2555686324  value)
	{
		___guiBackgroundColor_23 = value;
	}

	inline static int32_t get_offset_of_guiContentColor_24() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___guiContentColor_24)); }
	inline Color_t2555686324  get_guiContentColor_24() const { return ___guiContentColor_24; }
	inline Color_t2555686324 * get_address_of_guiContentColor_24() { return &___guiContentColor_24; }
	inline void set_guiContentColor_24(Color_t2555686324  value)
	{
		___guiContentColor_24 = value;
	}

	inline static int32_t get_offset_of_guiMatrix_25() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___guiMatrix_25)); }
	inline Matrix4x4_t1817901843  get_guiMatrix_25() const { return ___guiMatrix_25; }
	inline Matrix4x4_t1817901843 * get_address_of_guiMatrix_25() { return &___guiMatrix_25; }
	inline void set_guiMatrix_25(Matrix4x4_t1817901843  value)
	{
		___guiMatrix_25 = value;
	}

	inline static int32_t get_offset_of_U3CMouseCursorU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___U3CMouseCursorU3Ek__BackingField_26)); }
	inline Texture_t3661962703 * get_U3CMouseCursorU3Ek__BackingField_26() const { return ___U3CMouseCursorU3Ek__BackingField_26; }
	inline Texture_t3661962703 ** get_address_of_U3CMouseCursorU3Ek__BackingField_26() { return &___U3CMouseCursorU3Ek__BackingField_26; }
	inline void set_U3CMouseCursorU3Ek__BackingField_26(Texture_t3661962703 * value)
	{
		___U3CMouseCursorU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseCursorU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CLockCursorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___U3CLockCursorU3Ek__BackingField_27)); }
	inline bool get_U3CLockCursorU3Ek__BackingField_27() const { return ___U3CLockCursorU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CLockCursorU3Ek__BackingField_27() { return &___U3CLockCursorU3Ek__BackingField_27; }
	inline void set_U3CLockCursorU3Ek__BackingField_27(bool value)
	{
		___U3CLockCursorU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CHideCursorU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___U3CHideCursorU3Ek__BackingField_28)); }
	inline bool get_U3CHideCursorU3Ek__BackingField_28() const { return ___U3CHideCursorU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CHideCursorU3Ek__BackingField_28() { return &___U3CHideCursorU3Ek__BackingField_28; }
	inline void set_U3CHideCursorU3Ek__BackingField_28(bool value)
	{
		___U3CHideCursorU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_stateLabelStyle_30() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___stateLabelStyle_30)); }
	inline GUIStyle_t3956901511 * get_stateLabelStyle_30() const { return ___stateLabelStyle_30; }
	inline GUIStyle_t3956901511 ** get_address_of_stateLabelStyle_30() { return &___stateLabelStyle_30; }
	inline void set_stateLabelStyle_30(GUIStyle_t3956901511 * value)
	{
		___stateLabelStyle_30 = value;
		Il2CppCodeGenWriteBarrier((&___stateLabelStyle_30), value);
	}

	inline static int32_t get_offset_of_stateLabelBackground_31() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t1571180761_StaticFields, ___stateLabelBackground_31)); }
	inline Texture2D_t3840446185 * get_stateLabelBackground_31() const { return ___stateLabelBackground_31; }
	inline Texture2D_t3840446185 ** get_address_of_stateLabelBackground_31() { return &___stateLabelBackground_31; }
	inline void set_stateLabelBackground_31(Texture2D_t3840446185 * value)
	{
		___stateLabelBackground_31 = value;
		Il2CppCodeGenWriteBarrier((&___stateLabelBackground_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERGUI_T1571180761_H
#ifndef PLAYMAKERONGUI_T3357975613_H
#define PLAYMAKERONGUI_T3357975613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerOnGUI
struct  PlayMakerOnGUI_t3357975613  : public MonoBehaviour_t3962482529
{
public:
	// PlayMakerFSM PlayMakerOnGUI::playMakerFSM
	PlayMakerFSM_t1613010231 * ___playMakerFSM_4;
	// System.Boolean PlayMakerOnGUI::previewInEditMode
	bool ___previewInEditMode_5;

public:
	inline static int32_t get_offset_of_playMakerFSM_4() { return static_cast<int32_t>(offsetof(PlayMakerOnGUI_t3357975613, ___playMakerFSM_4)); }
	inline PlayMakerFSM_t1613010231 * get_playMakerFSM_4() const { return ___playMakerFSM_4; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_playMakerFSM_4() { return &___playMakerFSM_4; }
	inline void set_playMakerFSM_4(PlayMakerFSM_t1613010231 * value)
	{
		___playMakerFSM_4 = value;
		Il2CppCodeGenWriteBarrier((&___playMakerFSM_4), value);
	}

	inline static int32_t get_offset_of_previewInEditMode_5() { return static_cast<int32_t>(offsetof(PlayMakerOnGUI_t3357975613, ___previewInEditMode_5)); }
	inline bool get_previewInEditMode_5() const { return ___previewInEditMode_5; }
	inline bool* get_address_of_previewInEditMode_5() { return &___previewInEditMode_5; }
	inline void set_previewInEditMode_5(bool value)
	{
		___previewInEditMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERONGUI_T3357975613_H
#ifndef PLAYMAKERPROXYBASE_T90512809_H
#define PLAYMAKERPROXYBASE_T90512809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase
struct  PlayMakerProxyBase_t90512809  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerProxyBase::TargetFSMs
	List_1_t3085084973 * ___TargetFSMs_4;
	// PlayMakerProxyBase/TriggerEvent PlayMakerProxyBase::TriggerEventCallback
	TriggerEvent_t1933226311 * ___TriggerEventCallback_5;
	// PlayMakerProxyBase/CollisionEvent PlayMakerProxyBase::CollisionEventCallback
	CollisionEvent_t2644514664 * ___CollisionEventCallback_6;
	// PlayMakerProxyBase/ParticleCollisionEvent PlayMakerProxyBase::ParticleCollisionEventCallback
	ParticleCollisionEvent_t3984792766 * ___ParticleCollisionEventCallback_7;
	// PlayMakerProxyBase/ControllerCollisionEvent PlayMakerProxyBase::ControllerCollisionEventCallback
	ControllerCollisionEvent_t2755027817 * ___ControllerCollisionEventCallback_8;
	// PlayMakerProxyBase/Trigger2DEvent PlayMakerProxyBase::Trigger2DEventCallback
	Trigger2DEvent_t2411364400 * ___Trigger2DEventCallback_9;
	// PlayMakerProxyBase/Collision2DEvent PlayMakerProxyBase::Collision2DEventCallback
	Collision2DEvent_t1483235594 * ___Collision2DEventCallback_10;

public:
	inline static int32_t get_offset_of_TargetFSMs_4() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___TargetFSMs_4)); }
	inline List_1_t3085084973 * get_TargetFSMs_4() const { return ___TargetFSMs_4; }
	inline List_1_t3085084973 ** get_address_of_TargetFSMs_4() { return &___TargetFSMs_4; }
	inline void set_TargetFSMs_4(List_1_t3085084973 * value)
	{
		___TargetFSMs_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetFSMs_4), value);
	}

	inline static int32_t get_offset_of_TriggerEventCallback_5() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___TriggerEventCallback_5)); }
	inline TriggerEvent_t1933226311 * get_TriggerEventCallback_5() const { return ___TriggerEventCallback_5; }
	inline TriggerEvent_t1933226311 ** get_address_of_TriggerEventCallback_5() { return &___TriggerEventCallback_5; }
	inline void set_TriggerEventCallback_5(TriggerEvent_t1933226311 * value)
	{
		___TriggerEventCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerEventCallback_5), value);
	}

	inline static int32_t get_offset_of_CollisionEventCallback_6() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___CollisionEventCallback_6)); }
	inline CollisionEvent_t2644514664 * get_CollisionEventCallback_6() const { return ___CollisionEventCallback_6; }
	inline CollisionEvent_t2644514664 ** get_address_of_CollisionEventCallback_6() { return &___CollisionEventCallback_6; }
	inline void set_CollisionEventCallback_6(CollisionEvent_t2644514664 * value)
	{
		___CollisionEventCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___CollisionEventCallback_6), value);
	}

	inline static int32_t get_offset_of_ParticleCollisionEventCallback_7() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___ParticleCollisionEventCallback_7)); }
	inline ParticleCollisionEvent_t3984792766 * get_ParticleCollisionEventCallback_7() const { return ___ParticleCollisionEventCallback_7; }
	inline ParticleCollisionEvent_t3984792766 ** get_address_of_ParticleCollisionEventCallback_7() { return &___ParticleCollisionEventCallback_7; }
	inline void set_ParticleCollisionEventCallback_7(ParticleCollisionEvent_t3984792766 * value)
	{
		___ParticleCollisionEventCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleCollisionEventCallback_7), value);
	}

	inline static int32_t get_offset_of_ControllerCollisionEventCallback_8() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___ControllerCollisionEventCallback_8)); }
	inline ControllerCollisionEvent_t2755027817 * get_ControllerCollisionEventCallback_8() const { return ___ControllerCollisionEventCallback_8; }
	inline ControllerCollisionEvent_t2755027817 ** get_address_of_ControllerCollisionEventCallback_8() { return &___ControllerCollisionEventCallback_8; }
	inline void set_ControllerCollisionEventCallback_8(ControllerCollisionEvent_t2755027817 * value)
	{
		___ControllerCollisionEventCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerCollisionEventCallback_8), value);
	}

	inline static int32_t get_offset_of_Trigger2DEventCallback_9() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___Trigger2DEventCallback_9)); }
	inline Trigger2DEvent_t2411364400 * get_Trigger2DEventCallback_9() const { return ___Trigger2DEventCallback_9; }
	inline Trigger2DEvent_t2411364400 ** get_address_of_Trigger2DEventCallback_9() { return &___Trigger2DEventCallback_9; }
	inline void set_Trigger2DEventCallback_9(Trigger2DEvent_t2411364400 * value)
	{
		___Trigger2DEventCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger2DEventCallback_9), value);
	}

	inline static int32_t get_offset_of_Collision2DEventCallback_10() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___Collision2DEventCallback_10)); }
	inline Collision2DEvent_t1483235594 * get_Collision2DEventCallback_10() const { return ___Collision2DEventCallback_10; }
	inline Collision2DEvent_t1483235594 ** get_address_of_Collision2DEventCallback_10() { return &___Collision2DEventCallback_10; }
	inline void set_Collision2DEventCallback_10(Collision2DEvent_t1483235594 * value)
	{
		___Collision2DEventCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___Collision2DEventCallback_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERPROXYBASE_T90512809_H
#ifndef GUITEXT_T402233326_H
#define GUITEXT_T402233326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIText
struct  GUIText_t402233326  : public GUIElement_t3567083079
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXT_T402233326_H
#ifndef GUITEXTURE_T951903601_H
#define GUITEXTURE_T951903601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUITexture
struct  GUITexture_t951903601  : public GUIElement_t3567083079
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXTURE_T951903601_H
#ifndef PLAYMAKERANIMATORIK_T3219858232_H
#define PLAYMAKERANIMATORIK_T3219858232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerAnimatorIK
struct  PlayMakerAnimatorIK_t3219858232  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERANIMATORIK_T3219858232_H
#ifndef PLAYMAKERANIMATORMOVE_T426800548_H
#define PLAYMAKERANIMATORMOVE_T426800548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerAnimatorMove
struct  PlayMakerAnimatorMove_t426800548  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERANIMATORMOVE_T426800548_H
#ifndef PLAYMAKERAPPLICATIONEVENTS_T286866203_H
#define PLAYMAKERAPPLICATIONEVENTS_T286866203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerApplicationEvents
struct  PlayMakerApplicationEvents_t286866203  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERAPPLICATIONEVENTS_T286866203_H
#ifndef PLAYMAKERCOLLISIONENTER_T3733719168_H
#define PLAYMAKERCOLLISIONENTER_T3733719168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollisionEnter
struct  PlayMakerCollisionEnter_t3733719168  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCOLLISIONENTER_T3733719168_H
#ifndef PLAYMAKERCOLLISIONENTER2D_T3463350103_H
#define PLAYMAKERCOLLISIONENTER2D_T3463350103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollisionEnter2D
struct  PlayMakerCollisionEnter2D_t3463350103  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCOLLISIONENTER2D_T3463350103_H
#ifndef PLAYMAKERCOLLISIONEXIT_T3975926555_H
#define PLAYMAKERCOLLISIONEXIT_T3975926555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollisionExit
struct  PlayMakerCollisionExit_t3975926555  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCOLLISIONEXIT_T3975926555_H
#ifndef PLAYMAKERCOLLISIONEXIT2D_T2644894131_H
#define PLAYMAKERCOLLISIONEXIT2D_T2644894131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollisionExit2D
struct  PlayMakerCollisionExit2D_t2644894131  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCOLLISIONEXIT2D_T2644894131_H
#ifndef PLAYMAKERCOLLISIONSTAY_T2320041465_H
#define PLAYMAKERCOLLISIONSTAY_T2320041465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollisionStay
struct  PlayMakerCollisionStay_t2320041465  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCOLLISIONSTAY_T2320041465_H
#ifndef PLAYMAKERCOLLISIONSTAY2D_T1641352069_H
#define PLAYMAKERCOLLISIONSTAY2D_T1641352069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollisionStay2D
struct  PlayMakerCollisionStay2D_t1641352069  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCOLLISIONSTAY2D_T1641352069_H
#ifndef PLAYMAKERCONTROLLERCOLLIDERHIT_T3864538305_H
#define PLAYMAKERCONTROLLERCOLLIDERHIT_T3864538305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerControllerColliderHit
struct  PlayMakerControllerColliderHit_t3864538305  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERCONTROLLERCOLLIDERHIT_T3864538305_H
#ifndef PLAYMAKERFIXEDUPDATE_T1242491047_H
#define PLAYMAKERFIXEDUPDATE_T1242491047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerFixedUpdate
struct  PlayMakerFixedUpdate_t1242491047  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERFIXEDUPDATE_T1242491047_H
#ifndef PLAYMAKERJOINTBREAK_T32499873_H
#define PLAYMAKERJOINTBREAK_T32499873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerJointBreak
struct  PlayMakerJointBreak_t32499873  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERJOINTBREAK_T32499873_H
#ifndef PLAYMAKERJOINTBREAK2D_T628918649_H
#define PLAYMAKERJOINTBREAK2D_T628918649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerJointBreak2D
struct  PlayMakerJointBreak2D_t628918649  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERJOINTBREAK2D_T628918649_H
#ifndef PLAYMAKERLATEUPDATE_T211890678_H
#define PLAYMAKERLATEUPDATE_T211890678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerLateUpdate
struct  PlayMakerLateUpdate_t211890678  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERLATEUPDATE_T211890678_H
#ifndef PLAYMAKERMOUSEEVENTS_T4026808530_H
#define PLAYMAKERMOUSEEVENTS_T4026808530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerMouseEvents
struct  PlayMakerMouseEvents_t4026808530  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERMOUSEEVENTS_T4026808530_H
#ifndef PLAYMAKERPARTICLECOLLISION_T716698883_H
#define PLAYMAKERPARTICLECOLLISION_T716698883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerParticleCollision
struct  PlayMakerParticleCollision_t716698883  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERPARTICLECOLLISION_T716698883_H
#ifndef PLAYMAKERTRIGGERENTER_T2165260292_H
#define PLAYMAKERTRIGGERENTER_T2165260292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerEnter
struct  PlayMakerTriggerEnter_t2165260292  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERENTER_T2165260292_H
#ifndef PLAYMAKERTRIGGERENTER2D_T1611673000_H
#define PLAYMAKERTRIGGERENTER2D_T1611673000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerEnter2D
struct  PlayMakerTriggerEnter2D_t1611673000  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERENTER2D_T1611673000_H
#ifndef PLAYMAKERTRIGGEREXIT_T2127564928_H
#define PLAYMAKERTRIGGEREXIT_T2127564928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerExit
struct  PlayMakerTriggerExit_t2127564928  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGEREXIT_T2127564928_H
#ifndef PLAYMAKERTRIGGEREXIT2D_T2899497211_H
#define PLAYMAKERTRIGGEREXIT2D_T2899497211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerExit2D
struct  PlayMakerTriggerExit2D_t2899497211  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGEREXIT2D_T2899497211_H
#ifndef PLAYMAKERTRIGGERSTAY_T1432841146_H
#define PLAYMAKERTRIGGERSTAY_T1432841146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerStay
struct  PlayMakerTriggerStay_t1432841146  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERSTAY_T1432841146_H
#ifndef PLAYMAKERTRIGGERSTAY2D_T2180132409_H
#define PLAYMAKERTRIGGERSTAY2D_T2180132409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerStay2D
struct  PlayMakerTriggerStay2D_t2180132409  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERSTAY2D_T2180132409_H
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t3458670015  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FsmState_t4124398234 * m_Items[1];

public:
	inline FsmState_t4124398234 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmState_t4124398234 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmState_t4124398234 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmState_t4124398234 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmState_t4124398234 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmState_t4124398234 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2511618479  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FsmEvent_t3736299882 * m_Items[1];

public:
	inline FsmEvent_t3736299882 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmEvent_t3736299882 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmEvent_t3736299882 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmEvent_t3736299882 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmEvent_t3736299882 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmEvent_t3736299882 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t3577734832  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FsmTransition_t1340699069 * m_Items[1];

public:
	inline FsmTransition_t1340699069 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmTransition_t1340699069 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmTransition_t1340699069 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmTransition_t1340699069 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmTransition_t1340699069 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmTransition_t1340699069 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1918078376  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FsmStateAction_t3801304101 * m_Items[1];

public:
	inline FsmStateAction_t3801304101 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmStateAction_t3801304101 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmStateAction_t3801304101 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmStateAction_t3801304101 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmStateAction_t3801304101 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmStateAction_t3801304101 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t560886798  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) PlayMakerFSM_t1613010231 * m_Items[1];

public:
	inline PlayMakerFSM_t1613010231 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayMakerFSM_t1613010231 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayMakerFSM_t1613010231 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PlayMakerFSM_t1613010231 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayMakerFSM_t1613010231 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayMakerFSM_t1613010231 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m1902841207_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m1328752868_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Comparison_1__ctor_m793514796_gshared (Comparison_1_t2855037343 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1_Sort_m2076177611_gshared (List_1_t257213610 * __this, Comparison_1_t2855037343 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1_AddRange_m3709462088_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m1552711675_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Contains_m2654125393_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Predicate_1__ctor_m327447107_gshared (Predicate_1_t3905400288 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m4292035398_gshared (List_1_t257213610 * __this, Predicate_1_t3905400288 * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* List_1_ToArray_m4168020446_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);

// System.Void System.Attribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.SettingsMenuItemAttribute::set_MenuItem(System.String)
extern "C" IL2CPP_METHOD_ATTR void SettingsMenuItemAttribute_set_MenuItem_m3801438979 (SettingsMenuItemAttribute_t2376176270 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.SettingsMenuItemStateAttribute::set_MenuItem(System.String)
extern "C" IL2CPP_METHOD_ATTR void SettingsMenuItemStateAttribute_set_MenuItem_m3482663431 (SettingsMenuItemStateAttribute_t3930598301 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR bool Mathf_Approximately_m245805902 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Regex::Match(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR Match_t3408321083 * Regex_Match_m2057380353 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean System.Text.RegularExpressions.Group::get_Success()
extern "C" IL2CPP_METHOD_ATTR bool Group_get_Success_m1492300455 (Group_t2468205786 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Text.RegularExpressions.Group System.Text.RegularExpressions.GroupCollection::get_Item(System.String)
extern "C" IL2CPP_METHOD_ATTR Group_t2468205786 * GroupCollection_get_Item_m3293401907 (GroupCollection_t69770484 * __this, String_t* p0, const RuntimeMethod* method);
// System.Int32 System.Text.RegularExpressions.Capture::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t Capture_get_Length_m4245536461 (Capture_t2232016050 * __this, const RuntimeMethod* method);
// System.String System.Text.RegularExpressions.Capture::get_Value()
extern "C" IL2CPP_METHOD_ATTR String_t* Capture_get_Value_m538076933 (Capture_t2232016050 * __this, const RuntimeMethod* method);
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
extern "C" IL2CPP_METHOD_ATTR bool Int32_TryParse_m2404707562 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t* p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.String System.Int32::ToString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m372259452 (int32_t* __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<PlayMakerFSM>::get_Item(System.Int32)
inline PlayMakerFSM_t1613010231 * List_1_get_Item_m587783994 (List_1_t3085084973 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  PlayMakerFSM_t1613010231 * (*) (List_1_t3085084973 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method);
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C" IL2CPP_METHOD_ATTR Fsm_t4127147824 * PlayMakerFSM_get_Fsm_m2313055607 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Boolean PlayMakerFSM::get_Active()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerFSM_get_Active_m232505091 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorIK()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleAnimatorIK_m1347802419 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorIK(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnAnimatorIK_m3666493791 (Fsm_t4127147824 * __this, int32_t ___layerIndex0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<PlayMakerFSM>::get_Count()
inline int32_t List_1_get_Count_m2358759014 (List_1_t3085084973 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3085084973 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Void PlayMakerProxyBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase__ctor_m2965400855 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorMove()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleAnimatorMove_m659754095 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorMove()
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnAnimatorMove_m3967454478 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleApplicationEvents()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleApplicationEvents_m2776477914 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationFocus()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_ApplicationFocus_m4003945488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEvent)
extern "C" IL2CPP_METHOD_ATTR void Fsm_Event_m1030970055 (Fsm_t4127147824 * __this, FsmEvent_t3736299882 * ___fsmEvent0, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationPause()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_ApplicationPause_m1164961934 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleCollisionEnter_m3593559450 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnCollisionEnter_m1310005679 (Fsm_t4127147824 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::DoCollisionEventCallback(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoCollisionEventCallback_m2866947341 (PlayMakerProxyBase_t90512809 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleCollisionEnter2D_m3301833285 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnCollisionEnter2D_m45124035 (Fsm_t4127147824 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::DoCollision2DEventCallback(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoCollision2DEventCallback_m2085672570 (PlayMakerProxyBase_t90512809 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleCollisionExit_m2619444042 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnCollisionExit_m3418540421 (Fsm_t4127147824 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleCollisionExit2D_m2085212887 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnCollisionExit2D_m2859712456 (Fsm_t4127147824 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleCollisionStay_m137011183 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnCollisionStay_m215705888 (Fsm_t4127147824 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleCollisionStay2D_m3813250687 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnCollisionStay2D_m4291282023 (Fsm_t4127147824 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleControllerColliderHit()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleControllerColliderHit_m2106051336 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnControllerColliderHit_m2433446129 (Fsm_t4127147824 * __this, ControllerColliderHit_t240592346 * ___collider0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::DoControllerCollisionEventCallback(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoControllerCollisionEventCallback_m1869161337 (PlayMakerProxyBase_t90512809 * __this, ControllerColliderHit_t240592346 * ___hitCollider0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PlayMakerFSM>::GetEnumerator()
inline Enumerator_t679361554  List_1_GetEnumerator_m417274155 (List_1_t3085084973 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t679361554  (*) (List_1_t3085084973 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::get_Current()
inline PlayMakerFSM_t1613010231 * Enumerator_get_Current_m827365246 (Enumerator_t679361554 * __this, const RuntimeMethod* method)
{
	return ((  PlayMakerFSM_t1613010231 * (*) (Enumerator_t679361554 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.String PlayMakerFSM::get_FsmName()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerFSM_get_FsmName_m1388102387 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::MoveNext()
inline bool Enumerator_MoveNext_m4053319692 (Enumerator_t679361554 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t679361554 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::Dispose()
inline void Enumerator_Dispose_m1625029548 (Enumerator_t679361554 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t679361554 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void HutongGames.PlayMaker.Fsm::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Fsm__ctor_m818822609 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Reset(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void Fsm_Reset_m2312237609 (Fsm_t4127147824 * __this, MonoBehaviour_t3962482529 * ___component0, const RuntimeMethod* method);
// System.Void PlayMakerGlobals::Initialize()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_Initialize_m1152949201 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsEditor_m4098855694 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.FsmLog::set_LoggingEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FsmLog_set_LoggingEnabled_m3437905370 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.Void PlayMakerFSM::Init()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Init_m1246910327 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void PlayMakerFSM::InitTemplate()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_InitTemplate_m895909370 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::InitFsm()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_InitFsm_m3561656545 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Preprocess(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void Fsm_Preprocess_m13838580 (Fsm_t4127147824 * __this, MonoBehaviour_t3962482529 * ___component0, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponents()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_AddEventHandlerComponents_m2049794372 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m100394690 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_Preprocessed(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_Preprocessed_m534188963 (Fsm_t4127147824 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Init(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void Fsm_Init_m732989421 (Fsm_t4127147824 * __this, MonoBehaviour_t3962482529 * ___component0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_Preprocessed()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_Preprocessed_m1827486853 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.String HutongGames.PlayMaker.Fsm::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* Fsm_get_Name_m607141101 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_ShowStateLabel()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_ShowStateLabel_m4206798333 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::get_Variables()
extern "C" IL2CPP_METHOD_ATTR FsmVariables_t3349589544 * Fsm_get_Variables_m3361360136 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVariables)
extern "C" IL2CPP_METHOD_ATTR void Fsm__ctor_m2193636708 (Fsm_t4127147824 * __this, Fsm_t4127147824 * ___source0, FsmVariables_t3349589544 * ___overrideVariables1, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_Name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_Name_m3770397025 (Fsm_t4127147824 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_UsedInTemplate(FsmTemplate)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_UsedInTemplate_m3974031304 (Fsm_t4127147824 * __this, FsmTemplate_t1663266674 * ___value0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_ShowStateLabel(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_ShowStateLabel_m1728801951 (Fsm_t4127147824 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void PlayMakerFSM::Reset()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Reset_m2803450298 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method);
// System.Boolean PlayMakerPrefs::get_LogPerformanceWarnings()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerPrefs_get_LogPerformanceWarnings_m2853607083 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String HutongGames.PlayMaker.FsmUtility::GetFullFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C" IL2CPP_METHOD_ATTR String_t* FsmUtility_GetFullFsmLabel_m2050711782 (RuntimeObject * __this /* static, unused */, Fsm_t4127147824 * ___fsm0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_MouseEvents()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_MouseEvents_m1397553736 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerMouseEvents>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t4026808530_m2525016204 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionEnter>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t3733719168_m1361982853 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionExit>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3975926555_m2854978248 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionStay>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2320041465_m1461421557 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleTriggerEnter_m2907444739 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerEnter>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2165260292_m1613341182 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleTriggerExit_m260601769 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerExit>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2127564928_m2522439781 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleTriggerStay_m1648714122 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerStay>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t1432841146_m3573483980 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionEnter2D>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t3463350103_m1263814587 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionExit2D>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2644894131_m1687377651 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionStay2D>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1641352069_m3660820280 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleTriggerEnter2D_m2359995862 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerEnter2D>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t1611673000_m1561107553 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleTriggerExit2D_m122397604 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerExit2D>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t2899497211_m78230100 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleTriggerStay2D_m2215176015 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerStay2D>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t2180132409_m3915672404 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleParticleCollision()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleParticleCollision_m2694133425 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerParticleCollision>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t716698883_m13872882 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerControllerColliderHit>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t3864538305_m317364569 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleJointBreak_m145287290 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerJointBreak>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t32499873_m24598600 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak2D()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleJointBreak2D_m621821210 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleFixedUpdate()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleFixedUpdate_m603463774 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerFixedUpdate>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t1242491047_m4120879459 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleLateUpdate()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleLateUpdate_m1605664630 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerLateUpdate>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerLateUpdate_t211890678_m3673586923 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleOnGUI()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleOnGUI_m3287859215 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<PlayMakerOnGUI>()
inline PlayMakerOnGUI_t3357975613 * Component_GetComponent_TisPlayMakerOnGUI_t3357975613_m1251119906 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  PlayMakerOnGUI_t3357975613 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<PlayMakerOnGUI>()
inline PlayMakerOnGUI_t3357975613 * GameObject_AddComponent_TisPlayMakerOnGUI_t3357975613_m2071952433 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  PlayMakerOnGUI_t3357975613 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m1902841207_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerApplicationEvents>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t286866203_m4204577506 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerAnimatorMove>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t426800548_m490261559 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerAnimatorIK>()
inline void PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t3219858232_m1067537140 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	((  void (*) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*))PlayMakerFSM_AddEventHandlerComponent_TisRuntimeObject_m1712350287_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleLegacyNetworking()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_HandleLegacyNetworking_m1815348609 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.UiEvents HutongGames.PlayMaker.Fsm::get_HandleUiEvents()
extern "C" IL2CPP_METHOD_ATTR int32_t Fsm_get_HandleUiEvents_m3826291192 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// PlayMakerFSM/AddEventHandlerDelegate PlayMakerFSM::get_AddEventHandlers()
extern "C" IL2CPP_METHOD_ATTR AddEventHandlerDelegate_t772649125 * PlayMakerFSM_get_AddEventHandlers_m2329939143 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM/AddEventHandlerDelegate::Invoke(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR void AddEventHandlerDelegate_Invoke_m2843946657 (AddEventHandlerDelegate_t772649125 * __this, PlayMakerFSM_t1613010231 * ___fsm0, const RuntimeMethod* method);
// System.Type HutongGames.PlayMaker.ReflectionUtils::GetGlobalType(System.String)
extern "C" IL2CPP_METHOD_ATTR Type_t * ReflectionUtils_GetGlobalType_m1537385989 (RuntimeObject * __this /* static, unused */, String_t* ___typeName0, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Type::GetMethod(System.String)
extern "C" IL2CPP_METHOD_ATTR MethodInfo_t * Type_GetMethod_m2019726356 (Type_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Object,System.Reflection.MethodInfo)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_CreateDelegate_m995503480 (RuntimeObject * __this /* static, unused */, Type_t * p0, RuntimeObject * p1, MethodInfo_t * p2, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Clear(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void Fsm_Clear_m1271565575 (Fsm_t4127147824 * __this, MonoBehaviour_t3962482529 * ___component0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor(HutongGames.PlayMaker.FsmVariables)
extern "C" IL2CPP_METHOD_ATTR void FsmVariables__ctor_m3197115087 (FsmVariables_t3349589544 * __this, FsmVariables_t3349589544 * ___source0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_Variables_m4052446290 (Fsm_t4127147824 * __this, FsmVariables_t3349589544 * ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
inline GUITexture_t951903601 * Component_GetComponent_TisGUITexture_t951903601_m2110957654 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  GUITexture_t951903601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
inline GUIText_t402233326 * Component_GetComponent_TisGUIText_t402233326_m1400901891 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  GUIText_t402233326 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Boolean HutongGames.PlayMaker.Fsm::get_Started()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_Started_m3545277412 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Start()
extern "C" IL2CPP_METHOD_ATTR void Fsm_Start_m1434361673 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::Add(!0)
inline void List_1_Add_m1270336517 (List_1_t3085084973 * __this, PlayMakerFSM_t1613010231 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3085084973 *, PlayMakerFSM_t1613010231 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void HutongGames.PlayMaker.Fsm::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnEnable_m4038921800 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_Finished()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_Finished_m2412670668 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_ManualUpdate()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_ManualUpdate_m3466403006 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Update()
extern "C" IL2CPP_METHOD_ATTR void Fsm_Update_m2356052934 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerFSM/<DoCoroutine>d__43::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CDoCoroutineU3Ed__43__ctor_m3247423016 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_Disable()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_Disable_m1758544398 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<PlayMakerFSM>::Remove(!0)
inline bool List_1_Remove_m379049447 (List_1_t3085084973 * __this, PlayMakerFSM_t1613010231 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3085084973 *, PlayMakerFSM_t1613010231 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method);
}
// System.Void HutongGames.PlayMaker.Fsm::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnDisable_m2295088278 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnDestroy_m3254168318 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationQuit()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_ApplicationQuit_m922826774 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnDrawGizmos()
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnDrawGizmos_m1527377766 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::SetState(System.String)
extern "C" IL2CPP_METHOD_ATTR void Fsm_SetState_m401305471 (Fsm_t4127147824 * __this, String_t* ___stateName0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::Event(System.String)
extern "C" IL2CPP_METHOD_ATTR void Fsm_Event_m2215699026 (Fsm_t4127147824 * __this, String_t* ___fsmEventName0, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::GetFsmEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_GetFsmEvent_m2862863017 (RuntimeObject * __this /* static, unused */, String_t* ___eventName0, const RuntimeMethod* method);
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_BroadcastEvent_m1927056291 (RuntimeObject * __this /* static, unused */, FsmEvent_t3736299882 * ___fsmEvent0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern "C" IL2CPP_METHOD_ATTR List_1_t3085084973 * PlayMakerFSM_get_FsmList_m2691337861 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m2406428141 (List_1_t3085084973 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3085084973 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1328752868_gshared)(__this, p0, method);
}
// System.Void HutongGames.PlayMaker.Fsm::ProcessEvent(HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventData)
extern "C" IL2CPP_METHOD_ATTR void Fsm_ProcessEvent_m3558743891 (Fsm_t4127147824 * __this, FsmEvent_t3736299882 * ___fsmEvent0, FsmEventData_t1692568573 * ___eventData1, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameVisible()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_BecameVisible_m3680977252 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameInvisible()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_BecameInvisible_m3526322890 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_Owner(UnityEngine.MonoBehaviour)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_Owner_m1154650843 (Fsm_t4127147824 * __this, MonoBehaviour_t3962482529 * ___value0, const RuntimeMethod* method);
// System.String HutongGames.PlayMaker.Fsm::get_Description()
extern "C" IL2CPP_METHOD_ATTR String_t* Fsm_get_Description_m439968824 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_Description(System.String)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_Description_m249187881 (Fsm_t4127147824 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.Fsm::get_Active()
extern "C" IL2CPP_METHOD_ATTR bool Fsm_get_Active_m3726403215 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_ActiveState()
extern "C" IL2CPP_METHOD_ATTR FsmState_t4124398234 * Fsm_get_ActiveState_m523704067 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.String HutongGames.PlayMaker.FsmState::get_Name()
extern "C" IL2CPP_METHOD_ATTR String_t* FsmState_get_Name_m3784269523 (FsmState_t4124398234 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::get_States()
extern "C" IL2CPP_METHOD_ATTR FsmStateU5BU5D_t3458670015* Fsm_get_States_m1430473027 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::get_Events()
extern "C" IL2CPP_METHOD_ATTR FsmEventU5BU5D_t2511618479* Fsm_get_Events_m3037061137 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::get_GlobalTransitions()
extern "C" IL2CPP_METHOD_ATTR FsmTransitionU5BU5D_t3577734832* Fsm_get_GlobalTransitions_m215306635 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean PlayMakerGlobals::get_Initialized()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_Initialized_m4216351915 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::InitData()
extern "C" IL2CPP_METHOD_ATTR void Fsm_InitData_m844382496 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::.ctor()
inline void List_1__ctor_m1624985937 (List_1_t3085084973 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3085084973 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PushFsm(HutongGames.PlayMaker.Fsm)
extern "C" IL2CPP_METHOD_ATTR void FsmExecutionStack_PushFsm_m4001877049 (RuntimeObject * __this /* static, unused */, Fsm_t4127147824 * ___executingFsm0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PopFsm()
extern "C" IL2CPP_METHOD_ATTR void FsmExecutionStack_PopFsm_m598675584 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void Fsm_FixedUpdate_m2018825486 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void PlayMakerGUI::InitInstance()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_InitInstance_m1546024309 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C" IL2CPP_METHOD_ATTR bool Application_get_isEditor_m857789090 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * Object_FindObjectOfType_m67275058 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<PlayMakerGUI>()
inline PlayMakerGUI_t1571180761 * GameObject_AddComponent_TisPlayMakerGUI_t1571180761_m3641687522 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  PlayMakerGUI_t1571180761 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m1902841207_gshared)(__this, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m373113269 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_SetPixel_m2984741184 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, Color_t2555686324  p2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GUIStyle__ctor_m4038363858 (GUIStyle_t3956901511 * __this, const RuntimeMethod* method);
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern "C" IL2CPP_METHOD_ATTR GUIStyleState_t1397964415 * GUIStyle_get_normal_m729441812 (GUIStyle_t3956901511 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyleState::set_background(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void GUIStyleState_set_background_m369476077 (GUIStyleState_t1397964415 * __this, Texture2D_t3840446185 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void GUIStyleState_set_textColor_m1105876047 (GUIStyleState_t1397964415 * __this, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_fontSize_m1566850023 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_alignment_m3944619660 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void RectOffset__ctor_m732140021 (RectOffset_t1369453676 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_padding(UnityEngine.RectOffset)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_padding_m3302456044 (GUIStyle_t3956901511 * __this, RectOffset_t1369453676 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::Clear()
inline void List_1_Clear_m246811976 (List_1_t3085084973 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3085084973 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method);
}
// System.Void System.Comparison`1<PlayMakerFSM>::.ctor(System.Object,System.IntPtr)
inline void Comparison_1__ctor_m899976308 (Comparison_1_t1387941410 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Comparison_1_t1387941410 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Comparison_1__ctor_m793514796_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::Sort(System.Comparison`1<!0>)
inline void List_1_Sort_m450531192 (List_1_t3085084973 * __this, Comparison_1_t1387941410 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3085084973 *, Comparison_1_t1387941410 *, const RuntimeMethod*))List_1_Sort_m2076177611_gshared)(__this, p0, method);
}
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_DrawStateLabel_m1670404612 (PlayMakerGUI_t1571180761 * __this, PlayMakerFSM_t1613010231 * ___fsm0, const RuntimeMethod* method);
// System.Void PlayMakerGUI::InitLabelStyle()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_InitLabelStyle_m44230268 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerGUI_GenerateStateLabel_m3421774616 (RuntimeObject * __this /* static, unused */, PlayMakerFSM_t1613010231 * ___fsm0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" IL2CPP_METHOD_ATTR void GUIContent_set_text_m607297463 (GUIContent_t3050628031 * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  GUIStyle_CalcSize_m1046812636 (GUIStyle_t3956901511 * __this, GUIContent_t3050628031 * p0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C" IL2CPP_METHOD_ATTR GUITexture_t951903601 * PlayMakerFSM_get_GuiTexture_m659311724 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.GUITexture::get_pixelInset()
extern "C" IL2CPP_METHOD_ATTR Rect_t2360479859  GUITexture_get_pixelInset_m2981475662 (GUITexture_t951903601 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_x()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_x_m3839990490 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_y()
extern "C" IL2CPP_METHOD_ATTR float Rect_get_y_m1501338330 (Rect_t2360479859 * __this, const RuntimeMethod* method);
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C" IL2CPP_METHOD_ATTR GUIText_t402233326 * PlayMakerFSM_get_GuiText_m1330723702 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Vector3_Distance_m886789632 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_InverseTransformPoint_m1343916000 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Camera_WorldToScreenPoint_m3726311023 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.GUI::get_backgroundColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  GUI_get_backgroundColor_m492704139 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.GUI::get_color()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  GUI_get_color_m4285352717 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 HutongGames.PlayMaker.FsmState::get_ColorIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t FsmState_get_ColorIndex_m2169552874 (FsmState_t4124398234 * __this, const RuntimeMethod* method);
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t941916413* PlayMakerPrefs_get_Colors_m3509984984 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_backgroundColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_backgroundColor_m2936584335 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_contentColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_contentColor_m1403854338 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" IL2CPP_METHOD_ATTR void GUI_Label_m2420537077 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, GUIStyle_t3956901511 * p2, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_color_m1028198571 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_set_useGUILayout_m3492031340 (MonoBehaviour_t3962482529 * __this, bool p0, const RuntimeMethod* method);
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern "C" IL2CPP_METHOD_ATTR GUISkin_t1244372282 * PlayMakerGUI_get_GUISkin_m3406552685 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_skin_m3073574632 (RuntimeObject * __this /* static, unused */, GUISkin_t1244372282 * p0, const RuntimeMethod* method);
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerGUI_get_GUIColor_m3847206507 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerGUI_get_GUIBackgroundColor_m3544528621 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerGUI_get_GUIContentColor_m2658339020 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PlayMakerGUI::DoEditGUI()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_DoEditGUI_m1724976334 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1_AddRange_m1618812805 (List_1_t3085084973 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3085084973 *, RuntimeObject*, const RuntimeMethod*))List_1_AddRange_m3709462088_gshared)(__this, p0, method);
}
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_CallOnGUI_m3709965242 (PlayMakerGUI_t1571180761 * __this, Fsm_t4127147824 * ___fsm0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_SubFsmList()
extern "C" IL2CPP_METHOD_ATTR List_1_t1304255270 * Fsm_get_SubFsmList_m2810392772 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>::get_Item(System.Int32)
inline Fsm_t4127147824 * List_1_get_Item_m3488709207 (List_1_t1304255270 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Fsm_t4127147824 * (*) (List_1_t1304255270 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>::get_Count()
inline int32_t List_1_get_Count_m2787267307 (List_1_t1304255270 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t1304255270 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern "C" IL2CPP_METHOD_ATTR Event_t2956885303 * Event_get_current_m2393892120 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" IL2CPP_METHOD_ATTR int32_t Event_get_type_m1370041809 (Event_t2956885303 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.GUI::get_matrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  GUI_get_matrix_m2941988505 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Matrix4x4_get_identity_m1406790249 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_matrix(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_matrix_m3965800372 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, const RuntimeMethod* method);
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern "C" IL2CPP_METHOD_ATTR Texture_t3661962703 * PlayMakerGUI_get_MouseCursor_m3451464712 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void GUI_DrawTexture_m3124770796 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, Texture_t3661962703 * p1, const RuntimeMethod* method);
// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_EnableStateLabels_m1411432592 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PlayMakerGUI::DrawStateLabels()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_DrawStateLabels_m1713504128 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method);
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_GUIMatrix_m1274160697 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___value0, const RuntimeMethod* method);
// System.Boolean PlayMakerGUI::get_LockCursor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_LockCursor_m3827892467 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C" IL2CPP_METHOD_ATTR void Cursor_set_lockState_m831310062 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Boolean PlayMakerGUI::get_HideCursor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_HideCursor_m1592595061 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Cursor_set_visible_m2693238713 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.FsmState::get_Actions()
extern "C" IL2CPP_METHOD_ATTR FsmStateActionU5BU5D_t1918078376* FsmState_get_Actions_m2096890200 (FsmState_t4124398234 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Active()
extern "C" IL2CPP_METHOD_ATTR bool FsmStateAction_get_Active_m3504448199 (FsmStateAction_t3801304101 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_EditState()
extern "C" IL2CPP_METHOD_ATTR FsmState_t4124398234 * Fsm_get_EditState_m1641309013 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool FsmState_get_IsInitialized_m496409664 (FsmState_t4124398234 * __this, const RuntimeMethod* method);
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Enabled()
extern "C" IL2CPP_METHOD_ATTR bool FsmStateAction_get_Enabled_m3495869990 (FsmStateAction_t3801304101 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIContent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GUIContent__ctor_m3360759894 (GUIContent_t3050628031 * __this, const RuntimeMethod* method);
// System.Void PlayMakerGUI/<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m2492410959 (U3CU3Ec_t2300308631 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method);
// System.Int32 System.String::CompareOrdinal(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t String_CompareOrdinal_m786132908 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsPlayingInEditor_m3532164442 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsBuilding_m790260703 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsPlaying_m82396395 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsEditor_m3248841557 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_InitApplicationFlags_m1738268013 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * Resources_Load_m3480190876 (RuntimeObject * __this /* static, unused */, String_t* p0, Type_t * p1, const RuntimeMethod* method);
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsPlayingInEditor_m1115484028 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<PlayMakerGlobals>()
inline PlayMakerGlobals_t1863708399 * ScriptableObject_CreateInstance_TisPlayMakerGlobals_t1863708399_m1046660433 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  PlayMakerGlobals_t1863708399 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m1552711675_gshared)(__this /* static, unused */, method);
}
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_Variables_m1286583897 (PlayMakerGlobals_t1863708399 * __this, FsmVariables_t3349589544 * ___value0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * PlayMakerGlobals_get_Events_m1555858756 (PlayMakerGlobals_t1863708399 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m216985474 (List_1_t3319525431 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1328752868_gshared)(__this, p0, method);
}
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_Events_m1176162321 (PlayMakerGlobals_t1863708399 * __this, List_1_t3319525431 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_Initialized_m3402930854 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0)
inline bool List_1_Contains_m3607818559 (List_1_t3319525431 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m1685793073 (List_1_t3319525431 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::FindEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_FindEvent_m3186976377 (RuntimeObject * __this /* static, unused */, String_t* ___eventName0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.FsmEvent::set_IsGlobal(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FsmEvent_set_IsGlobal_m3065222085 (FsmEvent_t3736299882 * __this, bool ___value0, const RuntimeMethod* method);
// PlayMakerGlobals PlayMakerGlobals::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayMakerGlobals_t1863708399 * PlayMakerGlobals_get_Instance_m782995651 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PlayMakerGlobals/<>c__DisplayClass36_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0__ctor_m1135129098 (U3CU3Ec__DisplayClass36_0_t1424664817 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m201911024 (Predicate_1_t2672744813 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t2672744813 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m327447107_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m1452861271 (List_1_t3319525431 * __this, Predicate_1_t2672744813 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3319525431 *, Predicate_1_t2672744813 *, const RuntimeMethod*))List_1_RemoveAll_m4292035398_gshared)(__this, p0, method);
}
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FsmVariables__ctor_m565553959 (FsmVariables_t3349589544 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m706204246 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnJointBreak_m136230705 (Fsm_t4127147824 * __this, float ___breakForce0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak2D(UnityEngine.Joint2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnJointBreak2D_m1228224085 (Fsm_t4127147824 * __this, Joint2D_t4180440564 * ___brokenJoint0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::LateUpdate()
extern "C" IL2CPP_METHOD_ATTR void Fsm_LateUpdate_m354612717 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseEnter()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseEnter_m1471820455 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDown()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseDown_m712643734 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUp()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseUp_m1067072383 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_LastClickedObject(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_LastClickedObject_m1315180585 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___value0, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUpAsButton()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseUpAsButton_m126250691 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseExit()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseExit_m878588781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDrag()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseDrag_m2080773280 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseOver()
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * FsmEvent_get_MouseOver_m1174616455 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::set_HandleOnGUI(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Fsm_set_HandleOnGUI_m3034596896 (Fsm_t4127147824 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void PlayMakerOnGUI::DoEditGUI()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerOnGUI_DoEditGUI_m1319786893 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnGUI_m715957636 (Fsm_t4127147824 * __this, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnParticleCollision(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnParticleCollision_m3691854700 (Fsm_t4127147824 * __this, GameObject_t1113636619 * ___other0, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * Resources_Load_m3880010804 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<PlayMakerPrefs>()
inline PlayMakerPrefs_t2834574540 * ScriptableObject_CreateInstance_TisPlayMakerPrefs_t2834574540_m1074476695 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  PlayMakerPrefs_t2834574540 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m1552711675_gshared)(__this /* static, unused */, method);
}
// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayMakerPrefs_t2834574540 * PlayMakerPrefs_get_Instance_m4280752655 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_UpdateMinimapColors_m3596199612 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  Color_get_grey_m3440705476 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<PlayMakerFSM>::ToArray()
inline PlayMakerFSMU5BU5D_t560886798* List_1_ToArray_m3961749393 (List_1_t3085084973 * __this, const RuntimeMethod* method)
{
	return ((  PlayMakerFSMU5BU5D_t560886798* (*) (List_1_t3085084973 *, const RuntimeMethod*))List_1_ToArray_m4168020446_gshared)(__this, method);
}
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<PlayMakerFSM>::Contains(!0)
inline bool List_1_Contains_m2497552032 (List_1_t3085084973 * __this, PlayMakerFSM_t1613010231 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t3085084973 *, PlayMakerFSM_t1613010231 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method);
}
// System.Void PlayMakerProxyBase::add_TriggerEventCallback(PlayMakerProxyBase/TriggerEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_TriggerEventCallback_m3172485254 (PlayMakerProxyBase_t90512809 * __this, TriggerEvent_t1933226311 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::remove_TriggerEventCallback(PlayMakerProxyBase/TriggerEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_TriggerEventCallback_m1155547507 (PlayMakerProxyBase_t90512809 * __this, TriggerEvent_t1933226311 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase/TriggerEvent::Invoke(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void TriggerEvent_Invoke_m3191717391 (TriggerEvent_t1933226311 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::add_Trigger2DEventCallback(PlayMakerProxyBase/Trigger2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_Trigger2DEventCallback_m3684239397 (PlayMakerProxyBase_t90512809 * __this, Trigger2DEvent_t2411364400 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::remove_Trigger2DEventCallback(PlayMakerProxyBase/Trigger2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_Trigger2DEventCallback_m3473419264 (PlayMakerProxyBase_t90512809 * __this, Trigger2DEvent_t2411364400 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase/Trigger2DEvent::Invoke(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Trigger2DEvent_Invoke_m850316776 (Trigger2DEvent_t2411364400 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::add_CollisionEventCallback(PlayMakerProxyBase/CollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_CollisionEventCallback_m555388545 (PlayMakerProxyBase_t90512809 * __this, CollisionEvent_t2644514664 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::remove_CollisionEventCallback(PlayMakerProxyBase/CollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_CollisionEventCallback_m3921268190 (PlayMakerProxyBase_t90512809 * __this, CollisionEvent_t2644514664 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase/CollisionEvent::Invoke(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void CollisionEvent_Invoke_m4155047233 (CollisionEvent_t2644514664 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::add_Collision2DEventCallback(PlayMakerProxyBase/Collision2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_Collision2DEventCallback_m1748445263 (PlayMakerProxyBase_t90512809 * __this, Collision2DEvent_t1483235594 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::remove_Collision2DEventCallback(PlayMakerProxyBase/Collision2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_Collision2DEventCallback_m727498677 (PlayMakerProxyBase_t90512809 * __this, Collision2DEvent_t1483235594 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase/Collision2DEvent::Invoke(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Collision2DEvent_Invoke_m349239800 (Collision2DEvent_t1483235594 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::add_ParticleCollisionEventCallback(PlayMakerProxyBase/ParticleCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_ParticleCollisionEventCallback_m371344463 (PlayMakerProxyBase_t90512809 * __this, ParticleCollisionEvent_t3984792766 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::remove_ParticleCollisionEventCallback(PlayMakerProxyBase/ParticleCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_ParticleCollisionEventCallback_m2118262247 (PlayMakerProxyBase_t90512809 * __this, ParticleCollisionEvent_t3984792766 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase/ParticleCollisionEvent::Invoke(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void ParticleCollisionEvent_Invoke_m304638276 (ParticleCollisionEvent_t3984792766 * __this, GameObject_t1113636619 * ___gameObject0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::add_ControllerCollisionEventCallback(PlayMakerProxyBase/ControllerCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_ControllerCollisionEventCallback_m399454418 (PlayMakerProxyBase_t90512809 * __this, ControllerCollisionEvent_t2755027817 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::remove_ControllerCollisionEventCallback(PlayMakerProxyBase/ControllerCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_ControllerCollisionEventCallback_m2917022917 (PlayMakerProxyBase_t90512809 * __this, ControllerCollisionEvent_t2755027817 * ___value0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase/ControllerCollisionEvent::Invoke(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void ControllerCollisionEvent_Invoke_m4119380781 (ControllerCollisionEvent_t2755027817 * __this, ControllerColliderHit_t240592346 * ___hitCollider0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnTriggerEnter_m979455358 (Fsm_t4127147824 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::DoTriggerEventCallback(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoTriggerEventCallback_m3149201495 (PlayMakerProxyBase_t90512809 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnTriggerEnter2D_m672290962 (Fsm_t4127147824 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method);
// System.Void PlayMakerProxyBase::DoTrigger2DEventCallback(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoTrigger2DEventCallback_m3967730350 (PlayMakerProxyBase_t90512809 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnTriggerExit_m2908555696 (Fsm_t4127147824 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnTriggerExit2D_m923717752 (Fsm_t4127147824 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnTriggerStay_m2187419734 (Fsm_t4127147824 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method);
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Fsm_OnTriggerStay2D_m3259809030 (Fsm_t4127147824 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.RequiredFieldAttribute::.ctor()
extern "C" IL2CPP_METHOD_ATTR void RequiredFieldAttribute__ctor_m1182790897 (RequiredFieldAttribute_t3920128169 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String HutongGames.PlayMaker.SettingsMenuItemAttribute::get_MenuItem()
extern "C" IL2CPP_METHOD_ATTR String_t* SettingsMenuItemAttribute_get_MenuItem_m426247547 (SettingsMenuItemAttribute_t2376176270 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CMenuItemU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.SettingsMenuItemAttribute::set_MenuItem(System.String)
extern "C" IL2CPP_METHOD_ATTR void SettingsMenuItemAttribute_set_MenuItem_m3801438979 (SettingsMenuItemAttribute_t2376176270 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMenuItemU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.SettingsMenuItemAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void SettingsMenuItemAttribute__ctor_m2726361318 (SettingsMenuItemAttribute_t2376176270 * __this, String_t* ___menuItem0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuItem0;
		SettingsMenuItemAttribute_set_MenuItem_m3801438979(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String HutongGames.PlayMaker.SettingsMenuItemStateAttribute::get_MenuItem()
extern "C" IL2CPP_METHOD_ATTR String_t* SettingsMenuItemStateAttribute_get_MenuItem_m1738568794 (SettingsMenuItemStateAttribute_t3930598301 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CMenuItemU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.SettingsMenuItemStateAttribute::set_MenuItem(System.String)
extern "C" IL2CPP_METHOD_ATTR void SettingsMenuItemStateAttribute_set_MenuItem_m3482663431 (SettingsMenuItemStateAttribute_t3930598301 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMenuItemU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void HutongGames.PlayMaker.SettingsMenuItemStateAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void SettingsMenuItemStateAttribute__ctor_m1524778686 (SettingsMenuItemStateAttribute_t3930598301 * __this, String_t* ___menuItem0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuItem0;
		SettingsMenuItemStateAttribute_set_MenuItem_m3482663431(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String HutongGames.PlayMaker.TitleAttribute::get_Text()
extern "C" IL2CPP_METHOD_ATTR String_t* TitleAttribute_get_Text_m3588190268 (TitleAttribute_t196394849 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_text_0();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.TitleAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TitleAttribute__ctor_m3733251107 (TitleAttribute_t196394849 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_text_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String HutongGames.PlayMaker.TooltipAttribute::get_Text()
extern "C" IL2CPP_METHOD_ATTR String_t* TooltipAttribute_get_Text_m547469819 (TooltipAttribute_t2411168496 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_text_0();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.TooltipAttribute::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m142368228 (TooltipAttribute_t2411168496 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___text0;
		__this->set_text_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// HutongGames.PlayMaker.UIHint HutongGames.PlayMaker.UIHintAttribute::get_Hint()
extern "C" IL2CPP_METHOD_ATTR int32_t UIHintAttribute_get_Hint_m1314708198 (UIHintAttribute_t2334957252 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_hint_0();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.UIHintAttribute::.ctor(HutongGames.PlayMaker.UIHint)
extern "C" IL2CPP_METHOD_ATTR void UIHintAttribute__ctor_m1324150835 (UIHintAttribute_t2334957252 * __this, int32_t ___hint0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___hint0;
		__this->set_hint_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.VariableTypeAttribute::get_Type()
extern "C" IL2CPP_METHOD_ATTR int32_t VariableTypeAttribute_get_Type_m2871586349 (VariableTypeAttribute_t3556842571 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_type_0();
		return L_0;
	}
}
// System.Void HutongGames.PlayMaker.VariableTypeAttribute::.ctor(HutongGames.PlayMaker.VariableType)
extern "C" IL2CPP_METHOD_ATTR void VariableTypeAttribute__ctor_m3714451030 (VariableTypeAttribute_t3556842571 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type0;
		__this->set_type_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HutongGames.PlayMaker.VariableTypeFilter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VariableTypeFilter__ctor_m147114783 (VariableTypeFilter_t3022214907 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean HutongGames.Utility.ColorUtils::Approximately(UnityEngine.Color,UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR bool ColorUtils_Approximately_m401312687 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___color10, Color_t2555686324  ___color21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ColorUtils_Approximately_m401312687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = ___color10;
		float L_1 = L_0.get_r_0();
		Color_t2555686324  L_2 = ___color21;
		float L_3 = L_2.get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		bool L_4 = Mathf_Approximately_m245805902(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		Color_t2555686324  L_5 = ___color10;
		float L_6 = L_5.get_g_1();
		Color_t2555686324  L_7 = ___color21;
		float L_8 = L_7.get_g_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		bool L_9 = Mathf_Approximately_m245805902(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004b;
		}
	}
	{
		Color_t2555686324  L_10 = ___color10;
		float L_11 = L_10.get_b_2();
		Color_t2555686324  L_12 = ___color21;
		float L_13 = L_12.get_b_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		bool L_14 = Mathf_Approximately_m245805902(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_004b;
		}
	}
	{
		Color_t2555686324  L_15 = ___color10;
		float L_16 = L_15.get_a_3();
		Color_t2555686324  L_17 = ___color21;
		float L_18 = L_17.get_a_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		bool L_19 = Mathf_Approximately_m245805902(NULL /*static, unused*/, L_16, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_004b:
	{
		return (bool)0;
	}
}
// UnityEngine.Color HutongGames.Utility.ColorUtils::FromIntRGBA(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  ColorUtils_FromIntRGBA_m1501037309 (RuntimeObject * __this /* static, unused */, int32_t ___r0, int32_t ___g1, int32_t ___b2, int32_t ___a3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___r0;
		int32_t L_1 = ___g1;
		int32_t L_2 = ___b2;
		int32_t L_3 = ___a3;
		Color_t2555686324  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m2943235014((&L_4), ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String HutongGames.Utility.StringUtils::IncrementStringCounter(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* StringUtils_IncrementStringCounter_m2169459698 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StringUtils_IncrementStringCounter_m2169459698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Match_t3408321083 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3657309853_il2cpp_TypeInfo_var);
		Match_t3408321083 * L_1 = Regex_Match_m2057380353(NULL /*static, unused*/, L_0, _stringLiteral3506861936, /*hidden argument*/NULL);
		V_0 = L_1;
		Match_t3408321083 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = Group_get_Success_m1492300455(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_4 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3937257545(NULL /*static, unused*/, L_4, _stringLiteral3451434880, /*hidden argument*/NULL);
		return L_5;
	}

IL_0020:
	{
		Match_t3408321083 * L_6 = V_0;
		NullCheck(L_6);
		GroupCollection_t69770484 * L_7 = VirtFuncInvoker0< GroupCollection_t69770484 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_6);
		NullCheck(L_7);
		Group_t2468205786 * L_8 = GroupCollection_get_Item_m3293401907(L_7, _stringLiteral1619056972, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Capture_get_Length_m4245536461(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Match_t3408321083 * L_10 = V_0;
		NullCheck(L_10);
		GroupCollection_t69770484 * L_11 = VirtFuncInvoker0< GroupCollection_t69770484 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_10);
		NullCheck(L_11);
		Group_t2468205786 * L_12 = GroupCollection_get_Item_m3293401907(L_11, _stringLiteral1619056972, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = Capture_get_Value_m538076933(L_12, /*hidden argument*/NULL);
		bool L_14 = Int32_TryParse_m2404707562(NULL /*static, unused*/, L_13, (int32_t*)(&V_2), /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0056;
		}
	}
	{
		V_2 = 1;
	}

IL_0056:
	{
		Match_t3408321083 * L_15 = V_0;
		NullCheck(L_15);
		GroupCollection_t69770484 * L_16 = VirtFuncInvoker0< GroupCollection_t69770484 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_15);
		NullCheck(L_16);
		Group_t2468205786 * L_17 = GroupCollection_get_Item_m3293401907(L_16, _stringLiteral62725275, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Capture_get_Value_m538076933(L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		int32_t L_20 = V_1;
		int32_t L_21 = L_20;
		RuntimeObject * L_22 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3452614620, L_22, /*hidden argument*/NULL);
		String_t* L_24 = Int32_ToString_m372259452((int32_t*)(&V_3), L_23, /*hidden argument*/NULL);
		String_t* L_25 = String_Concat_m3755062657(NULL /*static, unused*/, L_18, _stringLiteral3452614528, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerAnimatorIK::OnAnimatorIK(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerAnimatorIK_OnAnimatorIK_m3481914875 (PlayMakerAnimatorIK_t3219858232 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerAnimatorIK_OnAnimatorIK_m3481914875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleAnimatorIK_m1347802419(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		int32_t L_14 = ___layerIndex0;
		NullCheck(L_13);
		Fsm_OnAnimatorIK_m3666493791(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerAnimatorIK::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerAnimatorIK__ctor_m772973112 (PlayMakerAnimatorIK_t3219858232 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerAnimatorMove::OnAnimatorMove()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerAnimatorMove_OnAnimatorMove_m4158779815 (PlayMakerAnimatorMove_t426800548 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerAnimatorMove_OnAnimatorMove_m4158779815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0046;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleAnimatorMove_m659754095(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Fsm_OnAnimatorMove_m3967454478(L_13, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_15 = V_0;
		List_1_t3085084973 * L_16 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m2358759014(L_16, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerAnimatorMove::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerAnimatorMove__ctor_m702366847 (PlayMakerAnimatorMove_t426800548 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerApplicationEvents::OnApplicationFocus()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerApplicationEvents_OnApplicationFocus_m3055338962 (PlayMakerApplicationEvents_t286866203 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerApplicationEvents_OnApplicationFocus_m3055338962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_HandleApplicationEvents_m2776477914(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_ApplicationFocus_m4003945488(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerApplicationEvents::OnApplicationPause()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerApplicationEvents_OnApplicationPause_m789568602 (PlayMakerApplicationEvents_t286866203 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerApplicationEvents_OnApplicationPause_m789568602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_HandleApplicationEvents_m2776477914(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_ApplicationPause_m1164961934(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerApplicationEvents::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerApplicationEvents__ctor_m2600881995 (PlayMakerApplicationEvents_t286866203 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerCollisionEnter::OnCollisionEnter(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionEnter_OnCollisionEnter_m3832574366 (PlayMakerCollisionEnter_t3733719168 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerCollisionEnter_OnCollisionEnter_m3832574366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionEnter_m3593559450(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collision_t4262080450 * L_14 = ___collisionInfo0;
		NullCheck(L_13);
		Fsm_OnCollisionEnter_m1310005679(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collision_t4262080450 * L_19 = ___collisionInfo0;
		PlayMakerProxyBase_DoCollisionEventCallback_m2866947341(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionEnter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionEnter__ctor_m4064403326 (PlayMakerCollisionEnter_t3733719168 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerCollisionEnter2D::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionEnter2D_OnCollisionEnter2D_m1570857150 (PlayMakerCollisionEnter2D_t3463350103 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerCollisionEnter2D_OnCollisionEnter2D_m1570857150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionEnter2D_m3301833285(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collision2D_t2842956331 * L_14 = ___collisionInfo0;
		NullCheck(L_13);
		Fsm_OnCollisionEnter2D_m45124035(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collision2D_t2842956331 * L_19 = ___collisionInfo0;
		PlayMakerProxyBase_DoCollision2DEventCallback_m2085672570(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionEnter2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionEnter2D__ctor_m4031817533 (PlayMakerCollisionEnter2D_t3463350103 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerCollisionExit::OnCollisionExit(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionExit_OnCollisionExit_m4074297343 (PlayMakerCollisionExit_t3975926555 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerCollisionExit_OnCollisionExit_m4074297343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionExit_m2619444042(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collision_t4262080450 * L_14 = ___collisionInfo0;
		NullCheck(L_13);
		Fsm_OnCollisionExit_m3418540421(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collision_t4262080450 * L_19 = ___collisionInfo0;
		PlayMakerProxyBase_DoCollisionEventCallback_m2866947341(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionExit::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionExit__ctor_m404739019 (PlayMakerCollisionExit_t3975926555 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerCollisionExit2D::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionExit2D_OnCollisionExit2D_m2715138103 (PlayMakerCollisionExit2D_t2644894131 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerCollisionExit2D_OnCollisionExit2D_m2715138103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionExit2D_m2085212887(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collision2D_t2842956331 * L_14 = ___collisionInfo0;
		NullCheck(L_13);
		Fsm_OnCollisionExit2D_m2859712456(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collision2D_t2842956331 * L_19 = ___collisionInfo0;
		PlayMakerProxyBase_DoCollision2DEventCallback_m2085672570(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionExit2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionExit2D__ctor_m837181758 (PlayMakerCollisionExit2D_t2644894131 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerCollisionStay::OnCollisionStay(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionStay_OnCollisionStay_m405027212 (PlayMakerCollisionStay_t2320041465 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerCollisionStay_OnCollisionStay_m405027212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionStay_m137011183(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collision_t4262080450 * L_14 = ___collisionInfo0;
		NullCheck(L_13);
		Fsm_OnCollisionStay_m215705888(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collision_t4262080450 * L_19 = ___collisionInfo0;
		PlayMakerProxyBase_DoCollisionEventCallback_m2866947341(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionStay::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionStay__ctor_m3349021787 (PlayMakerCollisionStay_t2320041465 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerCollisionStay2D::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionStay2D_OnCollisionStay2D_m3386751790 (PlayMakerCollisionStay2D_t1641352069 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerCollisionStay2D_OnCollisionStay2D_m3386751790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionStay2D_m3813250687(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collision2D_t2842956331 * L_14 = ___collisionInfo0;
		NullCheck(L_13);
		Fsm_OnCollisionStay2D_m4291282023(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collision2D_t2842956331 * L_19 = ___collisionInfo0;
		PlayMakerProxyBase_DoCollision2DEventCallback_m2085672570(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionStay2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerCollisionStay2D__ctor_m680363696 (PlayMakerCollisionStay2D_t1641352069 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerControllerColliderHit::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerControllerColliderHit_OnControllerColliderHit_m48225694 (PlayMakerControllerColliderHit_t3864538305 * __this, ControllerColliderHit_t240592346 * ___hitCollider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerControllerColliderHit_OnControllerColliderHit_m48225694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleControllerColliderHit_m2106051336(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		ControllerColliderHit_t240592346 * L_14 = ___hitCollider0;
		NullCheck(L_13);
		Fsm_OnControllerColliderHit_m2433446129(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		ControllerColliderHit_t240592346 * L_19 = ___hitCollider0;
		PlayMakerProxyBase_DoControllerCollisionEventCallback_m1869161337(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerControllerColliderHit::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerControllerColliderHit__ctor_m495527214 (PlayMakerControllerColliderHit_t3864538305 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String PlayMakerFSM::get_VersionNotes()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerFSM_get_VersionNotes_m2298671410 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_VersionNotes_m2298671410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral757602046;
	}
}
// System.String PlayMakerFSM::get_VersionLabel()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerFSM_get_VersionLabel_m3699917828 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_VersionLabel_m3699917828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral757602046;
	}
}
// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern "C" IL2CPP_METHOD_ATTR List_1_t3085084973 * PlayMakerFSM_get_FsmList_m2691337861 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_FsmList_m2691337861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_0 = ((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->get_fsmList_4();
		return L_0;
	}
}
// PlayMakerFSM PlayMakerFSM::FindFsmOnGameObject(UnityEngine.GameObject,System.String)
extern "C" IL2CPP_METHOD_ATTR PlayMakerFSM_t1613010231 * PlayMakerFSM_FindFsmOnGameObject_m119364970 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, String_t* ___fsmName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_FindFsmOnGameObject_m119364970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t679361554  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	PlayMakerFSM_t1613010231 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_0 = ((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->get_fsmList_4();
		NullCheck(L_0);
		Enumerator_t679361554  L_1 = List_1_GetEnumerator_m417274155(L_0, /*hidden argument*/List_1_GetEnumerator_m417274155_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_000d:
		{
			PlayMakerFSM_t1613010231 * L_2 = Enumerator_get_Current_m827365246((Enumerator_t679361554 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m827365246_RuntimeMethod_var);
			V_1 = L_2;
			PlayMakerFSM_t1613010231 * L_3 = V_1;
			NullCheck(L_3);
			GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
			GameObject_t1113636619 * L_5 = ___go0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0035;
			}
		}

IL_0023:
		{
			PlayMakerFSM_t1613010231 * L_7 = V_1;
			NullCheck(L_7);
			String_t* L_8 = PlayMakerFSM_get_FsmName_m1388102387(L_7, /*hidden argument*/NULL);
			String_t* L_9 = ___fsmName1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0035;
			}
		}

IL_0031:
		{
			PlayMakerFSM_t1613010231 * L_11 = V_1;
			V_2 = L_11;
			IL2CPP_LEAVE(0x50, FINALLY_0040);
		}

IL_0035:
		{
			bool L_12 = Enumerator_MoveNext_m4053319692((Enumerator_t679361554 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m4053319692_RuntimeMethod_var);
			if (L_12)
			{
				goto IL_000d;
			}
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1625029548((Enumerator_t679361554 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m1625029548_RuntimeMethod_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_004e:
	{
		return (PlayMakerFSM_t1613010231 *)NULL;
	}

IL_0050:
	{
		PlayMakerFSM_t1613010231 * L_13 = V_2;
		return L_13;
	}
}
// FsmTemplate PlayMakerFSM::get_FsmTemplate()
extern "C" IL2CPP_METHOD_ATTR FsmTemplate_t1663266674 * PlayMakerFSM_get_FsmTemplate_m1888826107 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		FsmTemplate_t1663266674 * L_0 = __this->get_fsmTemplate_9();
		return L_0;
	}
}
// System.Boolean PlayMakerFSM::get_DrawGizmos()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerFSM_get_DrawGizmos_m1178323582 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_DrawGizmos_m1178323582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->get_U3CDrawGizmosU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_DrawGizmos(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_set_DrawGizmos_m1771254959 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_set_DrawGizmos_m1771254959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->set_U3CDrawGizmosU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void PlayMakerFSM::Reset()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Reset_m2803450298 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Reset_m2803450298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Fsm_t4127147824 * L_1 = (Fsm_t4127147824 *)il2cpp_codegen_object_new(Fsm_t4127147824_il2cpp_TypeInfo_var);
		Fsm__ctor_m818822609(L_1, /*hidden argument*/NULL);
		__this->set_fsm_8(L_1);
	}

IL_0013:
	{
		__this->set_fsmTemplate_9((FsmTemplate_t1663266674 *)NULL);
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		Fsm_Reset_m2312237609(L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Awake()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Awake_m3471102124 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Awake_m3471102124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_Initialize_m1152949201(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = PlayMakerGlobals_get_IsEditor_m4098855694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmLog_t3265252880_il2cpp_TypeInfo_var);
		FsmLog_set_LoggingEnabled_m3437905370(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		PlayMakerFSM_Init_m1246910327(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Preprocess()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Preprocess_m2246609862 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Preprocess_m2246609862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1663266674 * L_0 = __this->get_fsmTemplate_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		PlayMakerFSM_InitTemplate_m895909370(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		PlayMakerFSM_InitFsm_m3561656545(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		Fsm_Preprocess_m13838580(L_2, __this, /*hidden argument*/NULL);
		PlayMakerFSM_AddEventHandlerComponents_m2049794372(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Init()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Init_m1246910327 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Init_m1246910327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1663266674 * L_0 = __this->get_fsmTemplate_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		bool L_2 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		PlayMakerFSM_InitTemplate_m895909370(__this, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_001d:
	{
		PlayMakerFSM_InitFsm_m3561656545(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		bool L_3 = PlayMakerGlobals_get_IsEditor_m4098855694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		Fsm_t4127147824 * L_4 = __this->get_fsm_8();
		NullCheck(L_4);
		Fsm_set_Preprocessed_m534188963(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_eventHandlerComponentsAdded_10((bool)0);
	}

IL_003d:
	{
		Fsm_t4127147824 * L_5 = __this->get_fsm_8();
		NullCheck(L_5);
		Fsm_Init_m732989421(L_5, __this, /*hidden argument*/NULL);
		bool L_6 = __this->get_eventHandlerComponentsAdded_10();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		Fsm_t4127147824 * L_7 = __this->get_fsm_8();
		NullCheck(L_7);
		bool L_8 = Fsm_get_Preprocessed_m1827486853(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}

IL_005e:
	{
		PlayMakerFSM_AddEventHandlerComponents_m2049794372(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void PlayMakerFSM::InitTemplate()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_InitTemplate_m895909370 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_InitTemplate_m895909370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Name_m607141101(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		bool L_3 = L_2->get_EnableDebugFlow_35();
		V_1 = L_3;
		Fsm_t4127147824 * L_4 = __this->get_fsm_8();
		NullCheck(L_4);
		bool L_5 = L_4->get_EnableBreakpoints_36();
		V_2 = L_5;
		Fsm_t4127147824 * L_6 = __this->get_fsm_8();
		NullCheck(L_6);
		bool L_7 = Fsm_get_ShowStateLabel_m4206798333(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		FsmTemplate_t1663266674 * L_8 = __this->get_fsmTemplate_9();
		NullCheck(L_8);
		Fsm_t4127147824 * L_9 = L_8->get_fsm_5();
		Fsm_t4127147824 * L_10 = __this->get_fsm_8();
		NullCheck(L_10);
		FsmVariables_t3349589544 * L_11 = Fsm_get_Variables_m3361360136(L_10, /*hidden argument*/NULL);
		Fsm_t4127147824 * L_12 = (Fsm_t4127147824 *)il2cpp_codegen_object_new(Fsm_t4127147824_il2cpp_TypeInfo_var);
		Fsm__ctor_m2193636708(L_12, L_9, L_11, /*hidden argument*/NULL);
		Fsm_t4127147824 * L_13 = L_12;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		Fsm_set_Name_m3770397025(L_13, L_14, /*hidden argument*/NULL);
		Fsm_t4127147824 * L_15 = L_13;
		NullCheck(L_15);
		Fsm_set_UsedInTemplate_m3974031304(L_15, (FsmTemplate_t1663266674 *)NULL, /*hidden argument*/NULL);
		Fsm_t4127147824 * L_16 = L_15;
		bool L_17 = V_1;
		NullCheck(L_16);
		L_16->set_EnableDebugFlow_35(L_17);
		Fsm_t4127147824 * L_18 = L_16;
		bool L_19 = V_2;
		NullCheck(L_18);
		L_18->set_EnableBreakpoints_36(L_19);
		Fsm_t4127147824 * L_20 = L_18;
		bool L_21 = V_3;
		NullCheck(L_20);
		Fsm_set_ShowStateLabel_m1728801951(L_20, L_21, /*hidden argument*/NULL);
		__this->set_fsm_8(L_20);
		return;
	}
}
// System.Void PlayMakerFSM::InitFsm()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_InitFsm_m3561656545 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_InitFsm_m3561656545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		PlayMakerFSM_Reset_m2803450298(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		Fsm_t4127147824 * L_1 = __this->get_fsm_8();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, _stringLiteral82471179, /*hidden argument*/NULL);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void PlayMakerFSM::AddEventHandlerComponents()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_AddEventHandlerComponents_m2049794372 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_AddEventHandlerComponents_m2049794372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		bool L_0 = PlayMakerPrefs_get_LogPerformanceWarnings_m2853607083(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		bool L_1 = PlayMakerGlobals_get_IsEditor_m4098855694(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		String_t* L_3 = FsmUtility_GetFullFsmLabel_m2050711782(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2289476910, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		Fsm_t4127147824 * L_5 = __this->get_fsm_8();
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m1397553736(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t4026808530_m2525016204(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t4026808530_m2525016204_RuntimeMethod_var);
	}

IL_003b:
	{
		Fsm_t4127147824 * L_7 = __this->get_fsm_8();
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionEnter_m3593559450(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t3733719168_m1361982853(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t3733719168_m1361982853_RuntimeMethod_var);
	}

IL_004e:
	{
		Fsm_t4127147824 * L_9 = __this->get_fsm_8();
		NullCheck(L_9);
		bool L_10 = Fsm_get_HandleCollisionExit_m2619444042(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0061;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3975926555_m2854978248(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3975926555_m2854978248_RuntimeMethod_var);
	}

IL_0061:
	{
		Fsm_t4127147824 * L_11 = __this->get_fsm_8();
		NullCheck(L_11);
		bool L_12 = Fsm_get_HandleCollisionStay_m137011183(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0074;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2320041465_m1461421557(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2320041465_m1461421557_RuntimeMethod_var);
	}

IL_0074:
	{
		Fsm_t4127147824 * L_13 = __this->get_fsm_8();
		NullCheck(L_13);
		bool L_14 = Fsm_get_HandleTriggerEnter_m2907444739(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2165260292_m1613341182(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2165260292_m1613341182_RuntimeMethod_var);
	}

IL_0087:
	{
		Fsm_t4127147824 * L_15 = __this->get_fsm_8();
		NullCheck(L_15);
		bool L_16 = Fsm_get_HandleTriggerExit_m260601769(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_009a;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2127564928_m2522439781(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2127564928_m2522439781_RuntimeMethod_var);
	}

IL_009a:
	{
		Fsm_t4127147824 * L_17 = __this->get_fsm_8();
		NullCheck(L_17);
		bool L_18 = Fsm_get_HandleTriggerStay_m1648714122(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ad;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t1432841146_m3573483980(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t1432841146_m3573483980_RuntimeMethod_var);
	}

IL_00ad:
	{
		Fsm_t4127147824 * L_19 = __this->get_fsm_8();
		NullCheck(L_19);
		bool L_20 = Fsm_get_HandleCollisionEnter2D_m3301833285(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c0;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t3463350103_m1263814587(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t3463350103_m1263814587_RuntimeMethod_var);
	}

IL_00c0:
	{
		Fsm_t4127147824 * L_21 = __this->get_fsm_8();
		NullCheck(L_21);
		bool L_22 = Fsm_get_HandleCollisionExit2D_m2085212887(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00d3;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2644894131_m1687377651(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2644894131_m1687377651_RuntimeMethod_var);
	}

IL_00d3:
	{
		Fsm_t4127147824 * L_23 = __this->get_fsm_8();
		NullCheck(L_23);
		bool L_24 = Fsm_get_HandleCollisionStay2D_m3813250687(L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00e6;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1641352069_m3660820280(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t1641352069_m3660820280_RuntimeMethod_var);
	}

IL_00e6:
	{
		Fsm_t4127147824 * L_25 = __this->get_fsm_8();
		NullCheck(L_25);
		bool L_26 = Fsm_get_HandleTriggerEnter2D_m2359995862(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00f9;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t1611673000_m1561107553(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t1611673000_m1561107553_RuntimeMethod_var);
	}

IL_00f9:
	{
		Fsm_t4127147824 * L_27 = __this->get_fsm_8();
		NullCheck(L_27);
		bool L_28 = Fsm_get_HandleTriggerExit2D_m122397604(L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_010c;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t2899497211_m78230100(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t2899497211_m78230100_RuntimeMethod_var);
	}

IL_010c:
	{
		Fsm_t4127147824 * L_29 = __this->get_fsm_8();
		NullCheck(L_29);
		bool L_30 = Fsm_get_HandleTriggerStay2D_m2215176015(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_011f;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t2180132409_m3915672404(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t2180132409_m3915672404_RuntimeMethod_var);
	}

IL_011f:
	{
		Fsm_t4127147824 * L_31 = __this->get_fsm_8();
		NullCheck(L_31);
		bool L_32 = Fsm_get_HandleParticleCollision_m2694133425(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0132;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t716698883_m13872882(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t716698883_m13872882_RuntimeMethod_var);
	}

IL_0132:
	{
		Fsm_t4127147824 * L_33 = __this->get_fsm_8();
		NullCheck(L_33);
		bool L_34 = Fsm_get_HandleControllerColliderHit_m2106051336(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0145;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t3864538305_m317364569(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t3864538305_m317364569_RuntimeMethod_var);
	}

IL_0145:
	{
		Fsm_t4127147824 * L_35 = __this->get_fsm_8();
		NullCheck(L_35);
		bool L_36 = Fsm_get_HandleJointBreak_m145287290(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0158;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t32499873_m24598600(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t32499873_m24598600_RuntimeMethod_var);
	}

IL_0158:
	{
		Fsm_t4127147824 * L_37 = __this->get_fsm_8();
		NullCheck(L_37);
		bool L_38 = Fsm_get_HandleJointBreak2D_m621821210(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_016b;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t32499873_m24598600(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t32499873_m24598600_RuntimeMethod_var);
	}

IL_016b:
	{
		Fsm_t4127147824 * L_39 = __this->get_fsm_8();
		NullCheck(L_39);
		bool L_40 = Fsm_get_HandleFixedUpdate_m603463774(L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_017e;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t1242491047_m4120879459(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t1242491047_m4120879459_RuntimeMethod_var);
	}

IL_017e:
	{
		Fsm_t4127147824 * L_41 = __this->get_fsm_8();
		NullCheck(L_41);
		bool L_42 = Fsm_get_HandleLateUpdate_m1605664630(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0191;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerLateUpdate_t211890678_m3673586923(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerLateUpdate_t211890678_m3673586923_RuntimeMethod_var);
	}

IL_0191:
	{
		Fsm_t4127147824 * L_43 = __this->get_fsm_8();
		NullCheck(L_43);
		bool L_44 = Fsm_get_HandleOnGUI_m3287859215(L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_01bd;
		}
	}
	{
		PlayMakerOnGUI_t3357975613 * L_45 = Component_GetComponent_TisPlayMakerOnGUI_t3357975613_m1251119906(__this, /*hidden argument*/Component_GetComponent_TisPlayMakerOnGUI_t3357975613_m1251119906_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_46 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_45, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01bd;
		}
	}
	{
		GameObject_t1113636619 * L_47 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		PlayMakerOnGUI_t3357975613 * L_48 = GameObject_AddComponent_TisPlayMakerOnGUI_t3357975613_m2071952433(L_47, /*hidden argument*/GameObject_AddComponent_TisPlayMakerOnGUI_t3357975613_m2071952433_RuntimeMethod_var);
		NullCheck(L_48);
		L_48->set_playMakerFSM_4(__this);
	}

IL_01bd:
	{
		Fsm_t4127147824 * L_49 = __this->get_fsm_8();
		NullCheck(L_49);
		bool L_50 = Fsm_get_HandleApplicationEvents_m2776477914(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01d0;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t286866203_m4204577506(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t286866203_m4204577506_RuntimeMethod_var);
	}

IL_01d0:
	{
		Fsm_t4127147824 * L_51 = __this->get_fsm_8();
		NullCheck(L_51);
		bool L_52 = Fsm_get_HandleAnimatorMove_m659754095(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01e3;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t426800548_m490261559(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t426800548_m490261559_RuntimeMethod_var);
	}

IL_01e3:
	{
		Fsm_t4127147824 * L_53 = __this->get_fsm_8();
		NullCheck(L_53);
		bool L_54 = Fsm_get_HandleAnimatorIK_m1347802419(L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01f6;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t3219858232_m1067537140(__this, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t3219858232_m1067537140_RuntimeMethod_var);
	}

IL_01f6:
	{
		Fsm_t4127147824 * L_55 = __this->get_fsm_8();
		NullCheck(L_55);
		bool L_56 = Fsm_get_HandleLegacyNetworking_m1815348609(L_55, /*hidden argument*/NULL);
		if (L_56)
		{
			goto IL_0210;
		}
	}
	{
		Fsm_t4127147824 * L_57 = __this->get_fsm_8();
		NullCheck(L_57);
		int32_t L_58 = Fsm_get_HandleUiEvents_m3826291192(L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_021c;
		}
	}

IL_0210:
	{
		AddEventHandlerDelegate_t772649125 * L_59 = PlayMakerFSM_get_AddEventHandlers_m2329939143(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		AddEventHandlerDelegate_Invoke_m2843946657(L_59, __this, /*hidden argument*/NULL);
	}

IL_021c:
	{
		__this->set_eventHandlerComponentsAdded_10((bool)1);
		return;
	}
}
// PlayMakerFSM/AddEventHandlerDelegate PlayMakerFSM::get_AddEventHandlers()
extern "C" IL2CPP_METHOD_ATTR AddEventHandlerDelegate_t772649125 * PlayMakerFSM_get_AddEventHandlers_m2329939143 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_AddEventHandlers_m2329939143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		AddEventHandlerDelegate_t772649125 * L_0 = __this->get_addEventHandlers_12();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t2510826940_il2cpp_TypeInfo_var);
		Type_t * L_1 = ReflectionUtils_GetGlobalType_m1537385989(NULL /*static, unused*/, _stringLiteral900290204, /*hidden argument*/NULL);
		NullCheck(L_1);
		MethodInfo_t * L_2 = Type_GetMethod_m2019726356(L_1, _stringLiteral259155937, /*hidden argument*/NULL);
		V_0 = L_2;
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (AddEventHandlerDelegate_t772649125_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = V_0;
		Delegate_t1188392813 * L_6 = Delegate_CreateDelegate_m995503480(NULL /*static, unused*/, L_4, NULL, L_5, /*hidden argument*/NULL);
		__this->set_addEventHandlers_12(((AddEventHandlerDelegate_t772649125 *)CastclassSealed((RuntimeObject*)L_6, AddEventHandlerDelegate_t772649125_il2cpp_TypeInfo_var)));
	}

IL_0039:
	{
		AddEventHandlerDelegate_t772649125 * L_7 = __this->get_addEventHandlers_12();
		return L_7;
	}
}
// System.Void PlayMakerFSM::SetFsmTemplate(FsmTemplate)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_SetFsmTemplate_m1859692523 (PlayMakerFSM_t1613010231 * __this, FsmTemplate_t1663266674 * ___template0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_SetFsmTemplate_m1859692523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1663266674 * L_0 = ___template0;
		__this->set_fsmTemplate_9(L_0);
		Fsm_t4127147824 * L_1 = PlayMakerFSM_get_Fsm_m2313055607(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Fsm_Clear_m1271565575(L_1, __this, /*hidden argument*/NULL);
		FsmTemplate_t1663266674 * L_2 = ___template0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Fsm_t4127147824 * L_4 = PlayMakerFSM_get_Fsm_m2313055607(__this, /*hidden argument*/NULL);
		FsmTemplate_t1663266674 * L_5 = __this->get_fsmTemplate_9();
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = L_5->get_fsm_5();
		NullCheck(L_6);
		FsmVariables_t3349589544 * L_7 = Fsm_get_Variables_m3361360136(L_6, /*hidden argument*/NULL);
		FsmVariables_t3349589544 * L_8 = (FsmVariables_t3349589544 *)il2cpp_codegen_object_new(FsmVariables_t3349589544_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m3197115087(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Fsm_set_Variables_m4052446290(L_4, L_8, /*hidden argument*/NULL);
	}

IL_003c:
	{
		PlayMakerFSM_Init_m1246910327(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C" IL2CPP_METHOD_ATTR GUITexture_t951903601 * PlayMakerFSM_get_GuiTexture_m659311724 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_GuiTexture_m659311724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUITexture_t951903601 * L_0 = __this->get__guiTexture_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		GUITexture_t951903601 * L_2 = Component_GetComponent_TisGUITexture_t951903601_m2110957654(__this, /*hidden argument*/Component_GetComponent_TisGUITexture_t951903601_m2110957654_RuntimeMethod_var);
		__this->set__guiTexture_13(L_2);
	}

IL_001a:
	{
		GUITexture_t951903601 * L_3 = __this->get__guiTexture_13();
		return L_3;
	}
}
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C" IL2CPP_METHOD_ATTR GUIText_t402233326 * PlayMakerFSM_get_GuiText_m1330723702 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_GuiText_m1330723702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t402233326 * L_0 = __this->get__guiText_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		GUIText_t402233326 * L_2 = Component_GetComponent_TisGUIText_t402233326_m1400901891(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t402233326_m1400901891_RuntimeMethod_var);
		__this->set__guiText_14(L_2);
	}

IL_001a:
	{
		GUIText_t402233326 * L_3 = __this->get__guiText_14();
		return L_3;
	}
}
// System.Void PlayMakerFSM::Start()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Start_m2534068713 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Started_m3545277412(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		Fsm_Start_m1434361673(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnEnable_m246380954 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnEnable_m246380954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_0 = ((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->get_fsmList_4();
		NullCheck(L_0);
		List_1_Add_m1270336517(L_0, __this, /*hidden argument*/List_1_Add_m1270336517_RuntimeMethod_var);
		Fsm_t4127147824 * L_1 = __this->get_fsm_8();
		NullCheck(L_1);
		Fsm_OnEnable_m4038921800(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Update()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_Update_m3570796553 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Finished_m2412670668(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		bool L_3 = Fsm_get_ManualUpdate_m3466403006(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Fsm_t4127147824 * L_4 = __this->get_fsm_8();
		NullCheck(L_4);
		Fsm_Update_m2356052934(L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator PlayMakerFSM::DoCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* PlayMakerFSM_DoCoroutine_m1084357490 (PlayMakerFSM_t1613010231 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_DoCoroutine_m1084357490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDoCoroutineU3Ed__43_t4100907003 * L_0 = (U3CDoCoroutineU3Ed__43_t4100907003 *)il2cpp_codegen_object_new(U3CDoCoroutineU3Ed__43_t4100907003_il2cpp_TypeInfo_var);
		U3CDoCoroutineU3Ed__43__ctor_m3247423016(L_0, 0, /*hidden argument*/NULL);
		U3CDoCoroutineU3Ed__43_t4100907003 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CDoCoroutineU3Ed__43_t4100907003 * L_2 = L_1;
		RuntimeObject* L_3 = ___routine0;
		NullCheck(L_2);
		L_2->set_routine_3(L_3);
		return L_2;
	}
}
// System.Void PlayMakerFSM::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnDisable_m2124961948 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDisable_m2124961948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_1 = FsmEvent_get_Disable_m1758544398(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m1030970055(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_2 = ((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->get_fsmList_4();
		NullCheck(L_2);
		List_1_Remove_m379049447(L_2, __this, /*hidden argument*/List_1_Remove_m379049447_RuntimeMethod_var);
		Fsm_t4127147824 * L_3 = __this->get_fsm_8();
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Fsm_t4127147824 * L_4 = __this->get_fsm_8();
		NullCheck(L_4);
		bool L_5 = Fsm_get_Finished_m2412670668(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003c;
		}
	}
	{
		Fsm_t4127147824 * L_6 = __this->get_fsm_8();
		NullCheck(L_6);
		Fsm_OnDisable_m2295088278(L_6, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnDestroy_m4064784760 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDestroy_m4064784760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_0 = ((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->get_fsmList_4();
		NullCheck(L_0);
		List_1_Remove_m379049447(L_0, __this, /*hidden argument*/List_1_Remove_m379049447_RuntimeMethod_var);
		Fsm_t4127147824 * L_1 = __this->get_fsm_8();
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		Fsm_OnDestroy_m3254168318(L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnApplicationQuit()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnApplicationQuit_m3772709317 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnApplicationQuit_m3772709317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_1 = FsmEvent_get_ApplicationQuit_m922826774(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m1030970055(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->set_ApplicationIsQuitting_6((bool)1);
		return;
	}
}
// System.Void PlayMakerFSM::OnDrawGizmos()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnDrawGizmos_m2969669533 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Fsm_t4127147824 * L_1 = __this->get_fsm_8();
		NullCheck(L_1);
		Fsm_OnDrawGizmos_m1527377766(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerFSM::SetState(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_SetState_m3487266296 (PlayMakerFSM_t1613010231 * __this, String_t* ___stateName0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		String_t* L_1 = ___stateName0;
		NullCheck(L_0);
		Fsm_SetState_m401305471(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::ChangeState(HutongGames.PlayMaker.FsmEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_ChangeState_m3739590369 (PlayMakerFSM_t1613010231 * __this, FsmEvent_t3736299882 * ___fsmEvent0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		FsmEvent_t3736299882 * L_1 = ___fsmEvent0;
		NullCheck(L_0);
		Fsm_Event_m1030970055(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::ChangeState(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_ChangeState_m529772668 (PlayMakerFSM_t1613010231 * __this, String_t* ___eventName0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m2215699026(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_SendEvent_m3188875631 (PlayMakerFSM_t1613010231 * __this, String_t* ___eventName0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m2215699026(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendRemoteFsmEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_SendRemoteFsmEvent_m2675188863 (PlayMakerFSM_t1613010231 * __this, String_t* ___eventName0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m2215699026(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendRemoteFsmEventWithData(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_SendRemoteFsmEventWithData_m42325452 (PlayMakerFSM_t1613010231 * __this, String_t* ___eventName0, String_t* ___eventData1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_SendRemoteFsmEventWithData_m42325452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t4127147824_il2cpp_TypeInfo_var);
		FsmEventData_t1692568573 * L_0 = ((Fsm_t4127147824_StaticFields*)il2cpp_codegen_static_fields_for(Fsm_t4127147824_il2cpp_TypeInfo_var))->get_EventData_4();
		String_t* L_1 = ___eventData1;
		NullCheck(L_0);
		L_0->set_StringData_9(L_1);
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		String_t* L_3 = ___eventName0;
		NullCheck(L_2);
		Fsm_Event_m2215699026(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::BroadcastEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_BroadcastEvent_m1842675870 (RuntimeObject * __this /* static, unused */, String_t* ___fsmEventName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_BroadcastEvent_m1842675870_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___fsmEventName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_2 = ___fsmEventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_3 = FsmEvent_GetFsmEvent_m2862863017(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		PlayMakerFSM_BroadcastEvent_m1927056291(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_BroadcastEvent_m1927056291 (RuntimeObject * __this /* static, unused */, FsmEvent_t3736299882 * ___fsmEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_BroadcastEvent_m1927056291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t679361554  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_0 = PlayMakerFSM_get_FsmList_m2691337861(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t3085084973 * L_1 = (List_1_t3085084973 *)il2cpp_codegen_object_new(List_1_t3085084973_il2cpp_TypeInfo_var);
		List_1__ctor_m2406428141(L_1, L_0, /*hidden argument*/List_1__ctor_m2406428141_RuntimeMethod_var);
		NullCheck(L_1);
		Enumerator_t679361554  L_2 = List_1_GetEnumerator_m417274155(L_1, /*hidden argument*/List_1_GetEnumerator_m417274155_RuntimeMethod_var);
		V_0 = L_2;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_0012:
		{
			PlayMakerFSM_t1613010231 * L_3 = Enumerator_get_Current_m827365246((Enumerator_t679361554 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m827365246_RuntimeMethod_var);
			V_1 = L_3;
			PlayMakerFSM_t1613010231 * L_4 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
			if (L_5)
			{
				goto IL_0038;
			}
		}

IL_0023:
		{
			PlayMakerFSM_t1613010231 * L_6 = V_1;
			NullCheck(L_6);
			Fsm_t4127147824 * L_7 = PlayMakerFSM_get_Fsm_m2313055607(L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0038;
			}
		}

IL_002b:
		{
			PlayMakerFSM_t1613010231 * L_8 = V_1;
			NullCheck(L_8);
			Fsm_t4127147824 * L_9 = PlayMakerFSM_get_Fsm_m2313055607(L_8, /*hidden argument*/NULL);
			FsmEvent_t3736299882 * L_10 = ___fsmEvent0;
			NullCheck(L_9);
			Fsm_ProcessEvent_m3558743891(L_9, L_10, (FsmEventData_t1692568573 *)NULL, /*hidden argument*/NULL);
		}

IL_0038:
		{
			bool L_11 = Enumerator_MoveNext_m4053319692((Enumerator_t679361554 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m4053319692_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0012;
			}
		}

IL_0041:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1625029548((Enumerator_t679361554 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m1625029548_RuntimeMethod_var);
		IL2CPP_END_FINALLY(67)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0051:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnBecameVisible()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnBecameVisible_m3709742732 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnBecameVisible_m3709742732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_1 = FsmEvent_get_BecameVisible_m3680977252(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m1030970055(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnBecameInvisible()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnBecameInvisible_m3452402576 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnBecameInvisible_m3452402576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_1 = FsmEvent_get_BecameInvisible_m3526322890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m1030970055(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C" IL2CPP_METHOD_ATTR Fsm_t4127147824 * PlayMakerFSM_get_Fsm_m2313055607 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		Fsm_set_Owner_m1154650843(L_0, __this, /*hidden argument*/NULL);
		Fsm_t4127147824 * L_1 = __this->get_fsm_8();
		return L_1;
	}
}
// System.String PlayMakerFSM::get_FsmName()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerFSM_get_FsmName_m1388102387 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Name_m607141101(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::set_FsmName(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_set_FsmName_m1253687814 (PlayMakerFSM_t1613010231 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Fsm_set_Name_m3770397025(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerFSM::get_FsmDescription()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerFSM_get_FsmDescription_m3096682621 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Description_m439968824(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::set_FsmDescription(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_set_FsmDescription_m1950490846 (PlayMakerFSM_t1613010231 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Fsm_set_Description_m249187881(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayMakerFSM::get_Active()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerFSM_get_Active_m232505091 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Active_m3726403215(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PlayMakerFSM::get_ActiveStateName()
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerFSM_get_ActiveStateName_m2221793783 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_ActiveStateName_m2221793783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		FsmState_t4124398234 * L_1 = Fsm_get_ActiveState_m523704067(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		Fsm_t4127147824 * L_2 = __this->get_fsm_8();
		NullCheck(L_2);
		FsmState_t4124398234 * L_3 = Fsm_get_ActiveState_m523704067(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = FsmState_get_Name_m3784269523(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001e:
	{
		return _stringLiteral757602046;
	}
}
// HutongGames.PlayMaker.FsmState[] PlayMakerFSM::get_FsmStates()
extern "C" IL2CPP_METHOD_ATTR FsmStateU5BU5D_t3458670015* PlayMakerFSM_get_FsmStates_m1647717072 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		FsmStateU5BU5D_t3458670015* L_1 = Fsm_get_States_m1430473027(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmEvent[] PlayMakerFSM::get_FsmEvents()
extern "C" IL2CPP_METHOD_ATTR FsmEventU5BU5D_t2511618479* PlayMakerFSM_get_FsmEvents_m3937264347 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		FsmEventU5BU5D_t2511618479* L_1 = Fsm_get_Events_m3037061137(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmTransition[] PlayMakerFSM::get_FsmGlobalTransitions()
extern "C" IL2CPP_METHOD_ATTR FsmTransitionU5BU5D_t3577734832* PlayMakerFSM_get_FsmGlobalTransitions_m2467079223 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		FsmTransitionU5BU5D_t3577734832* L_1 = Fsm_get_GlobalTransitions_m215306635(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmVariables PlayMakerFSM::get_FsmVariables()
extern "C" IL2CPP_METHOD_ATTR FsmVariables_t3349589544 * PlayMakerFSM_get_FsmVariables_m1958803352 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		Fsm_t4127147824 * L_0 = __this->get_fsm_8();
		NullCheck(L_0);
		FsmVariables_t3349589544 * L_1 = Fsm_get_Variables_m3361360136(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PlayMakerFSM::get_UsesTemplate()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerFSM_get_UsesTemplate_m701935324 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_UsesTemplate_m701935324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1663266674 * L_0 = __this->get_fsmTemplate_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::OnBeforeSerialize()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnBeforeSerialize_m3325964376 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnAfterDeserialize()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM_OnAfterDeserialize_m3653043181 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnAfterDeserialize_m3653043181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->set_NotMainThread_7((bool)1);
		bool L_0 = PlayMakerGlobals_get_Initialized_m4216351915(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Fsm_t4127147824 * L_1 = __this->get_fsm_8();
		NullCheck(L_1);
		Fsm_InitData_m844382496(L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->set_NotMainThread_7((bool)0);
		return;
	}
}
// System.Void PlayMakerFSM::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM__ctor_m442300139 (PlayMakerFSM_t1613010231 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFSM__cctor_m3719180614 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM__cctor_m3719180614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3085084973 * L_0 = (List_1_t3085084973 *)il2cpp_codegen_object_new(List_1_t3085084973_il2cpp_TypeInfo_var);
		List_1__ctor_m1624985937(L_0, /*hidden argument*/List_1__ctor_m1624985937_RuntimeMethod_var);
		((PlayMakerFSM_t1613010231_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var))->set_fsmList_4(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerFSM/<DoCoroutine>d__43::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CDoCoroutineU3Ed__43__ctor_m3247423016 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__43::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CDoCoroutineU3Ed__43_System_IDisposable_Dispose_m3036373137 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean PlayMakerFSM/<DoCoroutine>d__43::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CDoCoroutineU3Ed__43_MoveNext_m2448439397 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoCoroutineU3Ed__43_MoveNext_m2448439397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_005a;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0017:
	{
		PlayMakerFSM_t1613010231 * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		Fsm_t4127147824 * L_4 = L_3->get_fsm_8();
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t2219150547_il2cpp_TypeInfo_var);
		FsmExecutionStack_PushFsm_m4001877049(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		RuntimeObject* L_5 = __this->get_routine_3();
		NullCheck(L_5);
		bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_5);
		if (L_6)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t2219150547_il2cpp_TypeInfo_var);
		FsmExecutionStack_PopFsm_m598675584(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t2219150547_il2cpp_TypeInfo_var);
		FsmExecutionStack_PopFsm_m598675584(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_7 = __this->get_routine_3();
		NullCheck(L_7);
		RuntimeObject * L_8 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_7);
		__this->set_U3CU3E2__current_1(L_8);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005a:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_0017;
	}
}
// System.Object PlayMakerFSM/<DoCoroutine>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CDoCoroutineU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1517931224 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__43::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CDoCoroutineU3Ed__43_System_Collections_IEnumerator_Reset_m2796445315 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoCoroutineU3Ed__43_System_Collections_IEnumerator_Reset_m2796445315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CDoCoroutineU3Ed__43_System_Collections_IEnumerator_Reset_m2796445315_RuntimeMethod_var);
	}
}
// System.Object PlayMakerFSM/<DoCoroutine>d__43::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CDoCoroutineU3Ed__43_System_Collections_IEnumerator_get_Current_m2484320891 (U3CDoCoroutineU3Ed__43_t4100907003 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerFSM/AddEventHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void AddEventHandlerDelegate__ctor_m290148163 (AddEventHandlerDelegate_t772649125 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerFSM/AddEventHandlerDelegate::Invoke(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR void AddEventHandlerDelegate_Invoke_m2843946657 (AddEventHandlerDelegate_t772649125 * __this, PlayMakerFSM_t1613010231 * ___fsm0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AddEventHandlerDelegate_Invoke_m2843946657((AddEventHandlerDelegate_t772649125 *)__this->get_prev_9(), ___fsm0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, PlayMakerFSM_t1613010231 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___fsm0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, PlayMakerFSM_t1613010231 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___fsm0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< PlayMakerFSM_t1613010231 * >::Invoke(targetMethod, targetThis, ___fsm0);
					else
						GenericVirtActionInvoker1< PlayMakerFSM_t1613010231 * >::Invoke(targetMethod, targetThis, ___fsm0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< PlayMakerFSM_t1613010231 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___fsm0);
					else
						VirtActionInvoker1< PlayMakerFSM_t1613010231 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___fsm0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, PlayMakerFSM_t1613010231 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___fsm0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___fsm0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___fsm0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___fsm0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___fsm0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (PlayMakerFSM_t1613010231 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___fsm0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerFSM/AddEventHandlerDelegate::BeginInvoke(PlayMakerFSM,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* AddEventHandlerDelegate_BeginInvoke_m1970695050 (AddEventHandlerDelegate_t772649125 * __this, PlayMakerFSM_t1613010231 * ___fsm0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___fsm0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerFSM/AddEventHandlerDelegate::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void AddEventHandlerDelegate_EndInvoke_m3098441096 (AddEventHandlerDelegate_t772649125 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerFixedUpdate::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFixedUpdate_FixedUpdate_m3896899329 (PlayMakerFixedUpdate_t1242491047 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFixedUpdate_FixedUpdate_m3896899329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0046;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleFixedUpdate_m603463774(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Fsm_FixedUpdate_m2018825486(L_13, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_15 = V_0;
		List_1_t3085084973 * L_16 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m2358759014(L_16, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerFixedUpdate::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerFixedUpdate__ctor_m2849005711 (PlayMakerFixedUpdate_t1242491047 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_EnableStateLabels_m1411432592 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_EnableStateLabels_m1411432592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m1546024309(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_1 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_3 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_3);
		bool L_4 = Behaviour_get_enabled_m753527255(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_5 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_5);
		bool L_6 = L_5->get_drawStateLabels_9();
		return L_6;
	}

IL_0030:
	{
		return (bool)0;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_7 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_7, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_9 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_9);
		bool L_10 = Behaviour_get_enabled_m753527255(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_11 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_11);
		bool L_12 = L_11->get_drawStateLabels_9();
		if (!L_12)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_13 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_13);
		bool L_14 = L_13->get_enableStateLabelsInBuilds_10();
		return L_14;
	}

IL_0062:
	{
		return (bool)0;
	}
}
// System.Void PlayMakerGUI::set_EnableStateLabels(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_EnableStateLabels_m3044022820 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_EnableStateLabels_m3044022820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m1546024309(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_2 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		bool L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_drawStateLabels_9(L_3);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean PlayMakerGUI::get_EnableStateLabelsInBuild()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_EnableStateLabelsInBuild_m1678793621 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_EnableStateLabelsInBuild_m1678793621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m1546024309(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_2 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m753527255(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_4 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_4);
		bool L_5 = L_4->get_enableStateLabelsInBuilds_10();
		return L_5;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void PlayMakerGUI::set_EnableStateLabelsInBuild(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_EnableStateLabelsInBuild_m2333001203 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_EnableStateLabelsInBuild_m2333001203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m1546024309(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_2 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		bool L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_enableStateLabelsInBuilds_10(L_3);
	}

IL_001d:
	{
		return;
	}
}
// System.Void PlayMakerGUI::InitInstance()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_InitInstance_m1546024309 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_InitInstance_m1546024309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_2 = { reinterpret_cast<intptr_t> (PlayMakerGUI_t1571180761_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_4 = Object_FindObjectOfType_m67275058(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_instance_20(((PlayMakerGUI_t1571180761 *)CastclassClass((RuntimeObject*)L_4, PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var)));
	}

IL_0026:
	{
		return;
	}
}
// PlayMakerGUI PlayMakerGUI::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayMakerGUI_t1571180761 * PlayMakerGUI_get_Instance_m1759058258 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_Instance_m1759058258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m1546024309(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1113636619 * L_2 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_2, _stringLiteral566356367, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayMakerGUI_t1571180761 * L_3 = GameObject_AddComponent_TisPlayMakerGUI_t1571180761_m3641687522(L_2, /*hidden argument*/GameObject_AddComponent_TisPlayMakerGUI_t1571180761_m3641687522_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_instance_20(L_3);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_4 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		return L_4;
	}
}
// System.Boolean PlayMakerGUI::get_Enabled()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_Enabled_m1033451335 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_Enabled_m1033451335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_2 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m753527255(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern "C" IL2CPP_METHOD_ATTR GUISkin_t1244372282 * PlayMakerGUI_get_GUISkin_m3406552685 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUISkin_m3406552685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_guiSkin_21();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUISkin(UnityEngine.GUISkin)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_GUISkin_m1528967737 (RuntimeObject * __this /* static, unused */, GUISkin_t1244372282 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUISkin_m1528967737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUISkin_t1244372282 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiSkin_21(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerGUI_get_GUIColor_m3847206507 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIColor_m3847206507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Color_t2555686324  L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_guiColor_22();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_GUIColor_m4066615607 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIColor_m4066615607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiColor_22(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerGUI_get_GUIBackgroundColor_m3544528621 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIBackgroundColor_m3544528621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Color_t2555686324  L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_guiBackgroundColor_23();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIBackgroundColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_GUIBackgroundColor_m3467957232 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIBackgroundColor_m3467957232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiBackgroundColor_23(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerGUI_get_GUIContentColor_m2658339020 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIContentColor_m2658339020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Color_t2555686324  L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_guiContentColor_24();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIContentColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_GUIContentColor_m1416786292 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIContentColor_m1416786292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2555686324  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiContentColor_24(L_0);
		return;
	}
}
// UnityEngine.Matrix4x4 PlayMakerGUI::get_GUIMatrix()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  PlayMakerGUI_get_GUIMatrix_m3062280553 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIMatrix_m3062280553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_guiMatrix_25();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_GUIMatrix_m1274160697 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIMatrix_m1274160697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Matrix4x4_t1817901843  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiMatrix_25(L_0);
		return;
	}
}
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern "C" IL2CPP_METHOD_ATTR Texture_t3661962703 * PlayMakerGUI_get_MouseCursor_m3451464712 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_MouseCursor_m3451464712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Texture_t3661962703 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_U3CMouseCursorU3Ek__BackingField_26();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_MouseCursor(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_MouseCursor_m747393345 (RuntimeObject * __this /* static, unused */, Texture_t3661962703 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_MouseCursor_m747393345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t3661962703 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_U3CMouseCursorU3Ek__BackingField_26(L_0);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_LockCursor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_LockCursor_m3827892467 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_LockCursor_m3827892467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_U3CLockCursorU3Ek__BackingField_27();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_LockCursor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_LockCursor_m2624587674 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_LockCursor_m2624587674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_U3CLockCursorU3Ek__BackingField_27(L_0);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_HideCursor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGUI_get_HideCursor_m1592595061 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_HideCursor_m1592595061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_U3CHideCursorU3Ek__BackingField_28();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_HideCursor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_set_HideCursor_m1860311390 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_HideCursor_m1860311390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_U3CHideCursorU3Ek__BackingField_28(L_0);
		return;
	}
}
// System.Void PlayMakerGUI::InitLabelStyle()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_InitLabelStyle_m44230268 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_InitLabelStyle_m44230268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Texture2D_t3840446185 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelBackground_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Texture2D_t3840446185 * L_2 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelBackground_31();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Texture2D_t3840446185 * L_3 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m373113269(L_3, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_stateLabelBackground_31(L_3);
		Texture2D_t3840446185 * L_4 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelBackground_31();
		Color_t2555686324  L_5 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Texture2D_SetPixel_m2984741184(L_4, 0, 0, L_5, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_6 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelBackground_31();
		NullCheck(L_6);
		Texture2D_Apply_m2271746283(L_6, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_7 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4038363858(L_7, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_8 = L_7;
		NullCheck(L_8);
		GUIStyleState_t1397964415 * L_9 = GUIStyle_get_normal_m729441812(L_8, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_10 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelBackground_31();
		NullCheck(L_9);
		GUIStyleState_set_background_m369476077(L_9, L_10, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_11 = L_8;
		NullCheck(L_11);
		GUIStyleState_t1397964415 * L_12 = GUIStyle_get_normal_m729441812(L_11, /*hidden argument*/NULL);
		Color_t2555686324  L_13 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIStyleState_set_textColor_m1105876047(L_12, L_13, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_14 = L_11;
		float L_15 = __this->get_labelScale_16();
		NullCheck(L_14);
		GUIStyle_set_fontSize_m1566850023(L_14, (((int32_t)((int32_t)((float)il2cpp_codegen_multiply((float)(10.0f), (float)L_15))))), /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_16 = L_14;
		NullCheck(L_16);
		GUIStyle_set_alignment_m3944619660(L_16, 3, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_17 = L_16;
		RectOffset_t1369453676 * L_18 = (RectOffset_t1369453676 *)il2cpp_codegen_object_new(RectOffset_t1369453676_il2cpp_TypeInfo_var);
		RectOffset__ctor_m732140021(L_18, 4, 4, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_17);
		GUIStyle_set_padding_m3302456044(L_17, L_18, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_stateLabelStyle_30(L_17);
		float L_19 = __this->get_labelScale_16();
		__this->set_initLabelScale_32(L_19);
		return;
	}
}
// System.Void PlayMakerGUI::DrawStateLabels()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_DrawStateLabels_m1713504128 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DrawStateLabels_m1713504128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	PlayMakerFSM_t1613010231 * V_2 = NULL;
	int32_t V_3 = 0;
	PlayMakerFSM_t1613010231 * V_4 = NULL;
	Comparison_1_t1387941410 * G_B7_0 = NULL;
	List_1_t3085084973 * G_B7_1 = NULL;
	Comparison_1_t1387941410 * G_B6_0 = NULL;
	List_1_t3085084973 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SortedFsmList_17();
		NullCheck(L_0);
		List_1_Clear_m246811976(L_0, /*hidden argument*/List_1_Clear_m246811976_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_1 = PlayMakerFSM_get_FsmList_m2691337861(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m2358759014(L_1, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		V_0 = L_2;
		V_1 = 0;
		goto IL_003c;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_3 = PlayMakerFSM_get_FsmList_m2691337861(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		PlayMakerFSM_t1613010231 * L_5 = List_1_get_Item_m587783994(L_3, L_4, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_2 = L_5;
		PlayMakerFSM_t1613010231 * L_6 = V_2;
		NullCheck(L_6);
		bool L_7 = PlayMakerFSM_get_Active_m232505091(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_8 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SortedFsmList_17();
		PlayMakerFSM_t1613010231 * L_9 = V_2;
		NullCheck(L_8);
		List_1_Add_m1270336517(L_8, L_9, /*hidden argument*/List_1_Add_m1270336517_RuntimeMethod_var);
	}

IL_0038:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_003c:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_13 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SortedFsmList_17();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var);
		Comparison_1_t1387941410 * L_14 = ((U3CU3Ec_t2300308631_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var))->get_U3CU3E9__65_0_1();
		Comparison_1_t1387941410 * L_15 = L_14;
		G_B6_0 = L_15;
		G_B6_1 = L_13;
		if (L_15)
		{
			G_B7_0 = L_15;
			G_B7_1 = L_13;
			goto IL_0064;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var);
		U3CU3Ec_t2300308631 * L_16 = ((U3CU3Ec_t2300308631_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		intptr_t L_17 = (intptr_t)U3CU3Ec_U3CDrawStateLabelsU3Eb__65_0_m66306034_RuntimeMethod_var;
		Comparison_1_t1387941410 * L_18 = (Comparison_1_t1387941410 *)il2cpp_codegen_object_new(Comparison_1_t1387941410_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m899976308(L_18, L_16, L_17, /*hidden argument*/Comparison_1__ctor_m899976308_RuntimeMethod_var);
		Comparison_1_t1387941410 * L_19 = L_18;
		((U3CU3Ec_t2300308631_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var))->set_U3CU3E9__65_0_1(L_19);
		G_B7_0 = L_19;
		G_B7_1 = G_B6_1;
	}

IL_0064:
	{
		NullCheck(G_B7_1);
		List_1_Sort_m450531192(G_B7_1, G_B7_0, /*hidden argument*/List_1_Sort_m450531192_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_labelGameObject_18((GameObject_t1113636619 *)NULL);
		List_1_t3085084973 * L_20 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SortedFsmList_17();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m2358759014(L_20, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		V_0 = L_21;
		V_3 = 0;
		goto IL_00a5;
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_22 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SortedFsmList_17();
		int32_t L_23 = V_3;
		NullCheck(L_22);
		PlayMakerFSM_t1613010231 * L_24 = List_1_get_Item_m587783994(L_22, L_23, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_4 = L_24;
		PlayMakerFSM_t1613010231 * L_25 = V_4;
		NullCheck(L_25);
		Fsm_t4127147824 * L_26 = PlayMakerFSM_get_Fsm_m2313055607(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_27 = Fsm_get_ShowStateLabel_m4206798333(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00a1;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_28 = V_4;
		PlayMakerGUI_DrawStateLabel_m1670404612(__this, L_28, /*hidden argument*/NULL);
	}

IL_00a1:
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_00a5:
	{
		int32_t L_30 = V_3;
		int32_t L_31 = V_0;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_007e;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_DrawStateLabel_m1670404612 (PlayMakerGUI_t1571180761 * __this, PlayMakerFSM_t1613010231 * ___fsm0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DrawStateLabel_m1670404612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t2555686324  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Color_t2555686324  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t2360479859  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Color_t2555686324  G_B26_0;
	memset(&G_B26_0, 0, sizeof(G_B26_0));
	Color_t2555686324  G_B25_0;
	memset(&G_B25_0, 0, sizeof(G_B25_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GUIStyle_t3956901511 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelStyle_30();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = __this->get_initLabelScale_32();
		float L_2 = __this->get_labelScale_16();
		float L_3 = fabsf(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
		if ((!(((float)L_3) > ((float)(0.1f)))))
		{
			goto IL_0026;
		}
	}

IL_0020:
	{
		PlayMakerGUI_InitLabelStyle_m44230268(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		Camera_t4157153871 * L_4 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		return;
	}

IL_0034:
	{
		PlayMakerFSM_t1613010231 * L_6 = ___fsm0;
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		Camera_t4157153871 * L_8 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		return;
	}

IL_0047:
	{
		PlayMakerFSM_t1613010231 * L_10 = ___fsm0;
		NullCheck(L_10);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_12 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_labelGameObject_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		float L_14 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_fsmLabelIndex_19();
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_fsmLabelIndex_19(((float)il2cpp_codegen_add((float)L_14, (float)(1.0f))));
		goto IL_0080;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_fsmLabelIndex_19((0.0f));
		PlayMakerFSM_t1613010231 * L_15 = ___fsm0;
		NullCheck(L_15);
		GameObject_t1113636619 * L_16 = Component_get_gameObject_m442555142(L_15, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_labelGameObject_18(L_16);
	}

IL_0080:
	{
		PlayMakerFSM_t1613010231 * L_17 = ___fsm0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		String_t* L_18 = PlayMakerGUI_GenerateStateLabel_m3421774616(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		return;
	}

IL_0090:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(Vector2_t2156229523 ));
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GUIContent_t3050628031 * L_21 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_labelContent_6();
		String_t* L_22 = V_0;
		NullCheck(L_21);
		GUIContent_set_text_m607297463(L_21, L_22, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_23 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelStyle_30();
		GUIContent_t3050628031 * L_24 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_labelContent_6();
		NullCheck(L_23);
		Vector2_t2156229523  L_25 = GUIStyle_CalcSize_m1046812636(L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		Vector2_t2156229523  L_26 = V_2;
		float L_27 = L_26.get_x_0();
		float L_28 = __this->get_labelScale_16();
		float L_29 = __this->get_labelScale_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_30 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_27, ((float)il2cpp_codegen_multiply((float)(10.0f), (float)L_28)), ((float)il2cpp_codegen_multiply((float)(200.0f), (float)L_29)), /*hidden argument*/NULL);
		(&V_2)->set_x_0(L_30);
		bool L_31 = __this->get_GUITextureStateLabels_11();
		if (!L_31)
		{
			goto IL_016b;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_32 = ___fsm0;
		NullCheck(L_32);
		GUITexture_t951903601 * L_33 = PlayMakerFSM_get_GuiTexture_m659311724(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_34 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_33, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_016b;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_35 = ___fsm0;
		NullCheck(L_35);
		GameObject_t1113636619 * L_36 = Component_get_gameObject_m442555142(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_t3600365921 * L_37 = GameObject_get_transform_m1369836730(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t3722313464  L_38 = Transform_get_position_m36019626(L_37, /*hidden argument*/NULL);
		float L_39 = L_38.get_x_2();
		int32_t L_40 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t1613010231 * L_41 = ___fsm0;
		NullCheck(L_41);
		GUITexture_t951903601 * L_42 = PlayMakerFSM_get_GuiTexture_m659311724(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Rect_t2360479859  L_43 = GUITexture_get_pixelInset_m2981475662(L_42, /*hidden argument*/NULL);
		V_6 = L_43;
		float L_44 = Rect_get_x_m3839990490((Rect_t2360479859 *)(&V_6), /*hidden argument*/NULL);
		(&V_1)->set_x_0(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_39, (float)(((float)((float)L_40))))), (float)L_44)));
		PlayMakerFSM_t1613010231 * L_45 = ___fsm0;
		NullCheck(L_45);
		GameObject_t1113636619 * L_46 = Component_get_gameObject_m442555142(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_t3600365921 * L_47 = GameObject_get_transform_m1369836730(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t3722313464  L_48 = Transform_get_position_m36019626(L_47, /*hidden argument*/NULL);
		float L_49 = L_48.get_y_3();
		int32_t L_50 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t1613010231 * L_51 = ___fsm0;
		NullCheck(L_51);
		GUITexture_t951903601 * L_52 = PlayMakerFSM_get_GuiTexture_m659311724(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Rect_t2360479859  L_53 = GUITexture_get_pixelInset_m2981475662(L_52, /*hidden argument*/NULL);
		V_6 = L_53;
		float L_54 = Rect_get_y_m1501338330((Rect_t2360479859 *)(&V_6), /*hidden argument*/NULL);
		(&V_1)->set_y_1(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_49, (float)(((float)((float)L_50))))), (float)L_54)));
		goto IL_0255;
	}

IL_016b:
	{
		bool L_55 = __this->get_GUITextStateLabels_12();
		if (!L_55)
		{
			goto IL_01cc;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_56 = ___fsm0;
		NullCheck(L_56);
		GUIText_t402233326 * L_57 = PlayMakerFSM_get_GuiText_m1330723702(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_58 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_57, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_01cc;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_59 = ___fsm0;
		NullCheck(L_59);
		GameObject_t1113636619 * L_60 = Component_get_gameObject_m442555142(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_t3600365921 * L_61 = GameObject_get_transform_m1369836730(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		Vector3_t3722313464  L_62 = Transform_get_position_m36019626(L_61, /*hidden argument*/NULL);
		float L_63 = L_62.get_x_2();
		int32_t L_64 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_x_0(((float)il2cpp_codegen_multiply((float)L_63, (float)(((float)((float)L_64))))));
		PlayMakerFSM_t1613010231 * L_65 = ___fsm0;
		NullCheck(L_65);
		GameObject_t1113636619 * L_66 = Component_get_gameObject_m442555142(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_t3600365921 * L_67 = GameObject_get_transform_m1369836730(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t3722313464  L_68 = Transform_get_position_m36019626(L_67, /*hidden argument*/NULL);
		float L_69 = L_68.get_y_3();
		int32_t L_70 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_y_1(((float)il2cpp_codegen_multiply((float)L_69, (float)(((float)((float)L_70))))));
		goto IL_0255;
	}

IL_01cc:
	{
		bool L_71 = __this->get_filterLabelsWithDistance_13();
		if (!L_71)
		{
			goto IL_01fc;
		}
	}
	{
		Camera_t4157153871 * L_72 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_72);
		Transform_t3600365921 * L_73 = Component_get_transform_m3162698980(L_72, /*hidden argument*/NULL);
		NullCheck(L_73);
		Vector3_t3722313464  L_74 = Transform_get_position_m36019626(L_73, /*hidden argument*/NULL);
		PlayMakerFSM_t1613010231 * L_75 = ___fsm0;
		NullCheck(L_75);
		Transform_t3600365921 * L_76 = Component_get_transform_m3162698980(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		Vector3_t3722313464  L_77 = Transform_get_position_m36019626(L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_78 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_74, L_77, /*hidden argument*/NULL);
		float L_79 = __this->get_maxLabelDistance_14();
		if ((!(((float)L_78) > ((float)L_79))))
		{
			goto IL_01fc;
		}
	}
	{
		return;
	}

IL_01fc:
	{
		Camera_t4157153871 * L_80 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_80);
		Transform_t3600365921 * L_81 = Component_get_transform_m3162698980(L_80, /*hidden argument*/NULL);
		PlayMakerFSM_t1613010231 * L_82 = ___fsm0;
		NullCheck(L_82);
		Transform_t3600365921 * L_83 = Component_get_transform_m3162698980(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		Vector3_t3722313464  L_84 = Transform_get_position_m36019626(L_83, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector3_t3722313464  L_85 = Transform_InverseTransformPoint_m1343916000(L_81, L_84, /*hidden argument*/NULL);
		float L_86 = L_85.get_z_4();
		if ((!(((float)L_86) <= ((float)(0.0f)))))
		{
			goto IL_0223;
		}
	}
	{
		return;
	}

IL_0223:
	{
		Camera_t4157153871 * L_87 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t1613010231 * L_88 = ___fsm0;
		NullCheck(L_88);
		Transform_t3600365921 * L_89 = Component_get_transform_m3162698980(L_88, /*hidden argument*/NULL);
		NullCheck(L_89);
		Vector3_t3722313464  L_90 = Transform_get_position_m36019626(L_89, /*hidden argument*/NULL);
		NullCheck(L_87);
		Vector3_t3722313464  L_91 = Camera_WorldToScreenPoint_m3726311023(L_87, L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_92 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		V_1 = L_92;
		float* L_93 = (&V_1)->get_address_of_x_0();
		float* L_94 = L_93;
		Vector2_t2156229523  L_95 = V_2;
		float L_96 = L_95.get_x_0();
		*((float*)(L_94)) = (float)((float)il2cpp_codegen_subtract((float)(*((float*)L_94)), (float)((float)il2cpp_codegen_multiply((float)L_96, (float)(0.5f)))));
	}

IL_0255:
	{
		int32_t L_97 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_98 = V_1;
		float L_99 = L_98.get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		float L_100 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_fsmLabelIndex_19();
		float L_101 = __this->get_labelScale_16();
		(&V_1)->set_y_1(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)(((float)((float)L_97))), (float)L_99)), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_100, (float)(15.0f))), (float)L_101)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		Color_t2555686324  L_102 = GUI_get_backgroundColor_m492704139(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_102;
		Color_t2555686324  L_103 = GUI_get_color_m4285352717(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = 0;
		PlayMakerFSM_t1613010231 * L_104 = ___fsm0;
		NullCheck(L_104);
		Fsm_t4127147824 * L_105 = PlayMakerFSM_get_Fsm_m2313055607(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		FsmState_t4124398234 * L_106 = Fsm_get_ActiveState_m523704067(L_105, /*hidden argument*/NULL);
		G_B25_0 = L_103;
		if (!L_106)
		{
			G_B26_0 = L_103;
			goto IL_02a9;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_107 = ___fsm0;
		NullCheck(L_107);
		Fsm_t4127147824 * L_108 = PlayMakerFSM_get_Fsm_m2313055607(L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		FsmState_t4124398234 * L_109 = Fsm_get_ActiveState_m523704067(L_108, /*hidden argument*/NULL);
		NullCheck(L_109);
		int32_t L_110 = FsmState_get_ColorIndex_m2169552874(L_109, /*hidden argument*/NULL);
		V_4 = L_110;
		G_B26_0 = G_B25_0;
	}

IL_02a9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_111 = PlayMakerPrefs_get_Colors_m3509984984(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_112 = V_4;
		NullCheck(L_111);
		int32_t L_113 = L_112;
		Color_t2555686324  L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		V_5 = L_114;
		Color_t2555686324  L_115 = V_5;
		float L_116 = L_115.get_r_0();
		Color_t2555686324  L_117 = V_5;
		float L_118 = L_117.get_g_1();
		Color_t2555686324  L_119 = V_5;
		float L_120 = L_119.get_b_2();
		Color_t2555686324  L_121;
		memset(&L_121, 0, sizeof(L_121));
		Color__ctor_m2943235014((&L_121), L_116, L_118, L_120, (0.5f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_backgroundColor_m2936584335(NULL /*static, unused*/, L_121, /*hidden argument*/NULL);
		Color_t2555686324  L_122 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
		Vector2_t2156229523  L_123 = V_1;
		float L_124 = L_123.get_x_0();
		Vector2_t2156229523  L_125 = V_1;
		float L_126 = L_125.get_y_1();
		Vector2_t2156229523  L_127 = V_2;
		float L_128 = L_127.get_x_0();
		Vector2_t2156229523  L_129 = V_2;
		float L_130 = L_129.get_y_1();
		Rect_t2360479859  L_131;
		memset(&L_131, 0, sizeof(L_131));
		Rect__ctor_m2614021312((&L_131), L_124, L_126, L_128, L_130, /*hidden argument*/NULL);
		String_t* L_132 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GUIStyle_t3956901511 * L_133 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_stateLabelStyle_30();
		GUI_Label_m2420537077(NULL /*static, unused*/, L_131, L_132, L_133, /*hidden argument*/NULL);
		Color_t2555686324  L_134 = V_3;
		GUI_set_backgroundColor_m2936584335(NULL /*static, unused*/, L_134, /*hidden argument*/NULL);
		GUI_set_color_m1028198571(NULL /*static, unused*/, G_B26_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR String_t* PlayMakerGUI_GenerateStateLabel_m3421774616 (RuntimeObject * __this /* static, unused */, PlayMakerFSM_t1613010231 * ___fsm0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_GenerateStateLabel_m3421774616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSM_t1613010231 * L_0 = ___fsm0;
		NullCheck(L_0);
		Fsm_t4127147824 * L_1 = PlayMakerFSM_get_Fsm_m2313055607(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmState_t4124398234 * L_2 = Fsm_get_ActiveState_m523704067(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return _stringLiteral60608109;
	}

IL_0013:
	{
		PlayMakerFSM_t1613010231 * L_3 = ___fsm0;
		NullCheck(L_3);
		Fsm_t4127147824 * L_4 = PlayMakerFSM_get_Fsm_m2313055607(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmState_t4124398234 * L_5 = Fsm_get_ActiveState_m523704067(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = FsmState_get_Name_m3784269523(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PlayMakerGUI::Awake()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_Awake_m2966660766 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_Awake_m2966660766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_instance_20(__this);
		return;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral586731623, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGUI::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_OnEnable_m128401886 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_OnGUI_m372789782 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnGUI_m372789782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	int32_t V_2 = 0;
	Fsm_t4127147824 * V_3 = NULL;
	Matrix4x4_t1817901843  G_B19_0;
	memset(&G_B19_0, 0, sizeof(G_B19_0));
	Matrix4x4_t1817901843  G_B18_0;
	memset(&G_B18_0, 0, sizeof(G_B18_0));
	Matrix4x4_t1817901843  G_B22_0;
	memset(&G_B22_0, 0, sizeof(G_B22_0));
	Matrix4x4_t1817901843  G_B20_0;
	memset(&G_B20_0, 0, sizeof(G_B20_0));
	Matrix4x4_t1817901843  G_B21_0;
	memset(&G_B21_0, 0, sizeof(G_B21_0));
	int32_t G_B26_0 = 0;
	{
		bool L_0 = __this->get_enableGUILayout_8();
		MonoBehaviour_set_useGUILayout_m3492031340(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = PlayMakerGUI_get_GUISkin_m3406552685(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_3 = PlayMakerGUI_get_GUISkin_m3406552685(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_skin_m3073574632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Color_t2555686324  L_4 = PlayMakerGUI_get_GUIColor_m3847206507(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_color_m1028198571(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Color_t2555686324  L_5 = PlayMakerGUI_get_GUIBackgroundColor_m3544528621(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_backgroundColor_m2936584335(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Color_t2555686324  L_6 = PlayMakerGUI_get_GUIContentColor_m2658339020(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m1403854338(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_previewOnGUI_7();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		bool L_8 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_DoEditGUI_m1724976334(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_9 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_fsmList_4();
		NullCheck(L_9);
		List_1_Clear_m246811976(L_9, /*hidden argument*/List_1_Clear_m246811976_RuntimeMethod_var);
		List_1_t3085084973 * L_10 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_fsmList_4();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t1613010231_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_11 = PlayMakerFSM_get_FsmList_m2691337861(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_AddRange_m1618812805(L_10, L_11, /*hidden argument*/List_1_AddRange_m1618812805_RuntimeMethod_var);
		V_0 = 0;
		goto IL_00ee;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_12 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_fsmList_4();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		PlayMakerFSM_t1613010231 * L_14 = List_1_get_Item_m587783994(L_12, L_13, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_14;
		PlayMakerFSM_t1613010231 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_15, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_17 = V_1;
		NullCheck(L_17);
		bool L_18 = PlayMakerFSM_get_Active_m232505091(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_19 = V_1;
		NullCheck(L_19);
		Fsm_t4127147824 * L_20 = PlayMakerFSM_get_Fsm_m2313055607(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmState_t4124398234 * L_21 = Fsm_get_ActiveState_m523704067(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_22 = V_1;
		NullCheck(L_22);
		Fsm_t4127147824 * L_23 = PlayMakerFSM_get_Fsm_m2313055607(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = Fsm_get_HandleOnGUI_m3287859215(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_25 = V_1;
		NullCheck(L_25);
		Fsm_t4127147824 * L_26 = PlayMakerFSM_get_Fsm_m2313055607(L_25, /*hidden argument*/NULL);
		PlayMakerGUI_CallOnGUI_m3709965242(__this, L_26, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00d7;
	}

IL_00ba:
	{
		PlayMakerFSM_t1613010231 * L_27 = V_1;
		NullCheck(L_27);
		Fsm_t4127147824 * L_28 = PlayMakerFSM_get_Fsm_m2313055607(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		List_1_t1304255270 * L_29 = Fsm_get_SubFsmList_m2810392772(L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_2;
		NullCheck(L_29);
		Fsm_t4127147824 * L_31 = List_1_get_Item_m3488709207(L_29, L_30, /*hidden argument*/List_1_get_Item_m3488709207_RuntimeMethod_var);
		V_3 = L_31;
		Fsm_t4127147824 * L_32 = V_3;
		PlayMakerGUI_CallOnGUI_m3709965242(__this, L_32, /*hidden argument*/NULL);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00d7:
	{
		int32_t L_34 = V_2;
		PlayMakerFSM_t1613010231 * L_35 = V_1;
		NullCheck(L_35);
		Fsm_t4127147824 * L_36 = PlayMakerFSM_get_Fsm_m2313055607(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		List_1_t1304255270 * L_37 = Fsm_get_SubFsmList_m2810392772(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Count_m2787267307(L_37, /*hidden argument*/List_1_get_Count_m2787267307_RuntimeMethod_var);
		if ((((int32_t)L_34) < ((int32_t)L_38)))
		{
			goto IL_00ba;
		}
	}

IL_00ea:
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
	}

IL_00ee:
	{
		int32_t L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		List_1_t3085084973 * L_41 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_fsmList_4();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m2358759014(L_41, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_40) < ((int32_t)L_42)))
		{
			goto IL_0073;
		}
	}
	{
		bool L_43 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01e1;
		}
	}
	{
		Event_t2956885303 * L_44 = Event_get_current_m2393892120(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45 = Event_get_type_m1370041809(L_44, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_45) == ((uint32_t)7))))
		{
			goto IL_01e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_46 = GUI_get_matrix_m2941988505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_47 = Matrix4x4_get_identity_m1406790249(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_matrix_m3965800372(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Texture_t3661962703 * L_48 = PlayMakerGUI_get_MouseCursor_m3451464712(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_48, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B18_0 = L_46;
		if (!L_49)
		{
			G_B19_0 = L_46;
			goto IL_0198;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_50 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_51 = L_50.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Texture_t3661962703 * L_52 = PlayMakerGUI_get_MouseCursor_m3451464712(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_52);
		int32_t L_54 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_55 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_56 = L_55.get_y_3();
		Texture_t3661962703 * L_57 = PlayMakerGUI_get_MouseCursor_m3451464712(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_57);
		Texture_t3661962703 * L_59 = PlayMakerGUI_get_MouseCursor_m3451464712(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_59);
		int32_t L_60 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_59);
		Texture_t3661962703 * L_61 = PlayMakerGUI_get_MouseCursor_m3451464712(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_61);
		int32_t L_62 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_61);
		Rect_t2360479859  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Rect__ctor_m2614021312((&L_63), ((float)il2cpp_codegen_subtract((float)L_51, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_53))), (float)(0.5f))))), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)(((float)((float)L_54))), (float)L_56)), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_58))), (float)(0.5f))))), (((float)((float)L_60))), (((float)((float)L_62))), /*hidden argument*/NULL);
		Texture_t3661962703 * L_64 = PlayMakerGUI_get_MouseCursor_m3451464712(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3124770796(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
	}

IL_0198:
	{
		bool L_65 = __this->get_drawStateLabels_9();
		G_B20_0 = G_B19_0;
		if (!L_65)
		{
			G_B22_0 = G_B19_0;
			goto IL_01ad;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		bool L_66 = PlayMakerGUI_get_EnableStateLabels_m1411432592(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B21_0 = G_B20_0;
		if (!L_66)
		{
			G_B22_0 = G_B20_0;
			goto IL_01ad;
		}
	}
	{
		PlayMakerGUI_DrawStateLabels_m1713504128(__this, /*hidden argument*/NULL);
		G_B22_0 = G_B21_0;
	}

IL_01ad:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_matrix_m3965800372(NULL /*static, unused*/, G_B22_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_67 = Matrix4x4_get_identity_m1406790249(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIMatrix_m1274160697(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		bool L_68 = __this->get_controlMouseCursor_15();
		if (!L_68)
		{
			goto IL_01e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		bool L_69 = PlayMakerGUI_get_LockCursor_m3827892467(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_69)
		{
			goto IL_01ce;
		}
	}
	{
		G_B26_0 = 0;
		goto IL_01cf;
	}

IL_01ce:
	{
		G_B26_0 = 1;
	}

IL_01cf:
	{
		Cursor_set_lockState_m831310062(NULL /*static, unused*/, G_B26_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		bool L_70 = PlayMakerGUI_get_HideCursor_m1592595061(NULL /*static, unused*/, /*hidden argument*/NULL);
		Cursor_set_visible_m2693238713(NULL /*static, unused*/, (bool)((((int32_t)L_70) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01e1:
	{
		return;
	}
}
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_CallOnGUI_m3709965242 (PlayMakerGUI_t1571180761 * __this, Fsm_t4127147824 * ___fsm0, const RuntimeMethod* method)
{
	FsmStateActionU5BU5D_t1918078376* V_0 = NULL;
	int32_t V_1 = 0;
	FsmStateAction_t3801304101 * V_2 = NULL;
	{
		Fsm_t4127147824 * L_0 = ___fsm0;
		NullCheck(L_0);
		FsmState_t4124398234 * L_1 = Fsm_get_ActiveState_m523704067(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		Fsm_t4127147824 * L_2 = ___fsm0;
		NullCheck(L_2);
		FsmState_t4124398234 * L_3 = Fsm_get_ActiveState_m523704067(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmStateActionU5BU5D_t1918078376* L_4 = FsmState_get_Actions_m2096890200(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		V_1 = 0;
		goto IL_002e;
	}

IL_0018:
	{
		FsmStateActionU5BU5D_t1918078376* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		FsmStateAction_t3801304101 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = L_8;
		FsmStateAction_t3801304101 * L_9 = V_2;
		NullCheck(L_9);
		bool L_10 = FsmStateAction_get_Active_m3504448199(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_002a;
		}
	}
	{
		FsmStateAction_t3801304101 * L_11 = V_2;
		NullCheck(L_11);
		VirtActionInvoker0::Invoke(35 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_11);
	}

IL_002a:
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_13 = V_1;
		FsmStateActionU5BU5D_t1918078376* L_14 = V_0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_14)->max_length)))))))
		{
			goto IL_0018;
		}
	}

IL_0034:
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_OnDisable_m2705404189 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnDisable_m2705404189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		PlayMakerGUI_t1571180761 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_instance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_instance_20((PlayMakerGUI_t1571180761 *)NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerGUI::DoEditGUI()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_DoEditGUI_m1724976334 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DoEditGUI_m1724976334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmState_t4124398234 * V_0 = NULL;
	FsmStateActionU5BU5D_t1918078376* V_1 = NULL;
	int32_t V_2 = 0;
	FsmStateAction_t3801304101 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Fsm_t4127147824 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SelectedFSM_5();
		if (!L_0)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Fsm_t4127147824 * L_1 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SelectedFSM_5();
		NullCheck(L_1);
		bool L_2 = Fsm_get_HandleOnGUI_m3287859215(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Fsm_t4127147824 * L_3 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SelectedFSM_5();
		NullCheck(L_3);
		FsmState_t4124398234 * L_4 = Fsm_get_EditState_m1641309013(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmState_t4124398234 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		FsmState_t4124398234 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = FsmState_get_IsInitialized_m496409664(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		FsmState_t4124398234 * L_8 = V_0;
		NullCheck(L_8);
		FsmStateActionU5BU5D_t1918078376* L_9 = FsmState_get_Actions_m2096890200(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		V_2 = 0;
		goto IL_004a;
	}

IL_0034:
	{
		FsmStateActionU5BU5D_t1918078376* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		FsmStateAction_t3801304101 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_3 = L_13;
		FsmStateAction_t3801304101 * L_14 = V_3;
		NullCheck(L_14);
		bool L_15 = FsmStateAction_get_Enabled_m3495869990(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0046;
		}
	}
	{
		FsmStateAction_t3801304101 * L_16 = V_3;
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(35 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_16);
	}

IL_0046:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_004a:
	{
		int32_t L_18 = V_2;
		FsmStateActionU5BU5D_t1918078376* L_19 = V_1;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))))))
		{
			goto IL_0034;
		}
	}

IL_0050:
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnApplicationQuit()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI_OnApplicationQuit_m1022255247 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnApplicationQuit_m1022255247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_instance_20((PlayMakerGUI_t1571180761 *)NULL);
		return;
	}
}
// System.Void PlayMakerGUI::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI__ctor_m4250132181 (PlayMakerGUI_t1571180761 * __this, const RuntimeMethod* method)
{
	{
		__this->set_previewOnGUI_7((bool)1);
		__this->set_enableGUILayout_8((bool)1);
		__this->set_drawStateLabels_9((bool)1);
		__this->set_maxLabelDistance_14((10.0f));
		__this->set_controlMouseCursor_15((bool)1);
		__this->set_labelScale_16((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGUI::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGUI__cctor_m3996719456 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI__cctor_m3996719456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3085084973 * L_0 = (List_1_t3085084973 *)il2cpp_codegen_object_new(List_1_t3085084973_il2cpp_TypeInfo_var);
		List_1__ctor_m1624985937(L_0, /*hidden argument*/List_1__ctor_m1624985937_RuntimeMethod_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_fsmList_4(L_0);
		GUIContent_t3050628031 * L_1 = (GUIContent_t3050628031 *)il2cpp_codegen_object_new(GUIContent_t3050628031_il2cpp_TypeInfo_var);
		GUIContent__ctor_m3360759894(L_1, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_labelContent_6(L_1);
		List_1_t3085084973 * L_2 = (List_1_t3085084973 *)il2cpp_codegen_object_new(List_1_t3085084973_il2cpp_TypeInfo_var);
		List_1__ctor_m1624985937(L_2, /*hidden argument*/List_1__ctor_m1624985937_RuntimeMethod_var);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_SortedFsmList_17(L_2);
		Color_t2555686324  L_3 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiColor_22(L_3);
		Color_t2555686324  L_4 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiBackgroundColor_23(L_4);
		Color_t2555686324  L_5 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiContentColor_24(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_6 = Matrix4x4_get_identity_m1406790249(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->set_guiMatrix_25(L_6);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerGUI/<>c::.cctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m1135028354 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m1135028354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2300308631 * L_0 = (U3CU3Ec_t2300308631 *)il2cpp_codegen_object_new(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m2492410959(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t2300308631_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2300308631_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void PlayMakerGUI/<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m2492410959 (U3CU3Ec_t2300308631 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PlayMakerGUI/<>c::<DrawStateLabels>b__65_0(PlayMakerFSM,PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CDrawStateLabelsU3Eb__65_0_m66306034 (U3CU3Ec_t2300308631 * __this, PlayMakerFSM_t1613010231 * ___x0, PlayMakerFSM_t1613010231 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CDrawStateLabelsU3Eb__65_0_m66306034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSM_t1613010231 * L_0 = ___x0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m4211327027(L_1, /*hidden argument*/NULL);
		PlayMakerFSM_t1613010231 * L_3 = ___y1;
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m4211327027(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_CompareOrdinal_m786132908(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean PlayMakerGlobals::get_Initialized()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_Initialized_m4216351915 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_Initialized_m4216351915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_U3CInitializedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_Initialized_m3402930854 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_Initialized_m3402930854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_U3CInitializedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsPlayingInEditor_m1115484028 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsPlayingInEditor_m1115484028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_U3CIsPlayingInEditorU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsPlayingInEditor_m3532164442 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsPlayingInEditor_m3532164442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_U3CIsPlayingInEditorU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsPlaying()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsPlaying_m3613842894 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsPlaying_m3613842894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_U3CIsPlayingU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsPlaying_m82396395 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsPlaying_m82396395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_U3CIsPlayingU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsEditor_m4098855694 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsEditor_m4098855694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_U3CIsEditorU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsEditor_m3248841557 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsEditor_m3248841557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_U3CIsEditorU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerGlobals_get_IsBuilding_m790260703 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsBuilding_m790260703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_U3CIsBuildingU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsBuilding(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_IsBuilding_m861315209 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsBuilding_m861315209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_U3CIsBuildingU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_InitApplicationFlags_m1738268013 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		PlayMakerGlobals_set_IsPlayingInEditor_m3532164442(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		bool L_2 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		bool L_3 = PlayMakerGlobals_get_IsBuilding_m790260703(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		PlayMakerGlobals_set_IsPlaying_m82396395(NULL /*static, unused*/, (bool)G_B6_0, /*hidden argument*/NULL);
		bool L_4 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGlobals_set_IsEditor_m3248841557(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGlobals::Initialize()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_Initialize_m1152949201 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_Initialize_m1152949201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	PlayMakerGlobals_t1863708399 * V_1 = NULL;
	{
		bool L_0 = PlayMakerGlobals_get_Initialized_m4216351915(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_008e;
		}
	}
	{
		PlayMakerGlobals_InitApplicationFlags_m1738268013(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_1 = { reinterpret_cast<intptr_t> (PlayMakerGlobals_t1863708399_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Object_t631007953 * L_3 = Resources_Load_m3480190876(NULL /*static, unused*/, _stringLiteral858884005, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Object_t631007953 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_007e;
		}
	}
	{
		bool L_6 = PlayMakerGlobals_get_IsPlayingInEditor_m1115484028(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0071;
		}
	}
	{
		Object_t631007953 * L_7 = V_0;
		V_1 = ((PlayMakerGlobals_t1863708399 *)CastclassClass((RuntimeObject*)L_7, PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var));
		PlayMakerGlobals_t1863708399 * L_8 = ScriptableObject_CreateInstance_TisPlayMakerGlobals_t1863708399_m1046660433(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerGlobals_t1863708399_m1046660433_RuntimeMethod_var);
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_instance_9(L_8);
		PlayMakerGlobals_t1863708399 * L_9 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_instance_9();
		PlayMakerGlobals_t1863708399 * L_10 = V_1;
		NullCheck(L_10);
		FsmVariables_t3349589544 * L_11 = L_10->get_variables_10();
		FsmVariables_t3349589544 * L_12 = (FsmVariables_t3349589544 *)il2cpp_codegen_object_new(FsmVariables_t3349589544_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m3197115087(L_12, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		PlayMakerGlobals_set_Variables_m1286583897(L_9, L_12, /*hidden argument*/NULL);
		PlayMakerGlobals_t1863708399 * L_13 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_instance_9();
		PlayMakerGlobals_t1863708399 * L_14 = V_1;
		NullCheck(L_14);
		List_1_t3319525431 * L_15 = PlayMakerGlobals_get_Events_m1555858756(L_14, /*hidden argument*/NULL);
		List_1_t3319525431 * L_16 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m216985474(L_16, L_15, /*hidden argument*/List_1__ctor_m216985474_RuntimeMethod_var);
		NullCheck(L_13);
		PlayMakerGlobals_set_Events_m1176162321(L_13, L_16, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0071:
	{
		Object_t631007953 * L_17 = V_0;
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_instance_9(((PlayMakerGlobals_t1863708399 *)IsInstClass((RuntimeObject*)L_17, PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var)));
		goto IL_0088;
	}

IL_007e:
	{
		PlayMakerGlobals_t1863708399 * L_18 = ScriptableObject_CreateInstance_TisPlayMakerGlobals_t1863708399_m1046660433(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerGlobals_t1863708399_m1046660433_RuntimeMethod_var);
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_instance_9(L_18);
	}

IL_0088:
	{
		PlayMakerGlobals_set_Initialized_m3402930854(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// PlayMakerGlobals PlayMakerGlobals::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayMakerGlobals_t1863708399 * PlayMakerGlobals_get_Instance_m782995651 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_Instance_m782995651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_Initialize_m1152949201(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGlobals_t1863708399 * L_0 = ((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->get_instance_9();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::ResetInstance()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_ResetInstance_m750794251 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_ResetInstance_m750794251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_instance_9((PlayMakerGlobals_t1863708399 *)NULL);
		return;
	}
}
// HutongGames.PlayMaker.FsmVariables PlayMakerGlobals::get_Variables()
extern "C" IL2CPP_METHOD_ATTR FsmVariables_t3349589544 * PlayMakerGlobals_get_Variables_m4187827348 (PlayMakerGlobals_t1863708399 * __this, const RuntimeMethod* method)
{
	{
		FsmVariables_t3349589544 * L_0 = __this->get_variables_10();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_Variables_m1286583897 (PlayMakerGlobals_t1863708399 * __this, FsmVariables_t3349589544 * ___value0, const RuntimeMethod* method)
{
	{
		FsmVariables_t3349589544 * L_0 = ___value0;
		__this->set_variables_10(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * PlayMakerGlobals_get_Events_m1555858756 (PlayMakerGlobals_t1863708399 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3319525431 * L_0 = __this->get_events_11();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_set_Events_m1176162321 (PlayMakerGlobals_t1863708399 * __this, List_1_t3319525431 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t3319525431 * L_0 = ___value0;
		__this->set_events_11(L_0);
		return;
	}
}
// HutongGames.PlayMaker.FsmEvent PlayMakerGlobals::AddEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR FsmEvent_t3736299882 * PlayMakerGlobals_AddEvent_m2652243670 (PlayMakerGlobals_t1863708399 * __this, String_t* ___eventName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_AddEvent_m2652243670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmEvent_t3736299882 * G_B4_0 = NULL;
	FsmEvent_t3736299882 * G_B3_0 = NULL;
	{
		List_1_t3319525431 * L_0 = __this->get_events_11();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m3607818559(L_0, L_1, /*hidden argument*/List_1_Contains_m3607818559_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		List_1_t3319525431 * L_3 = __this->get_events_11();
		String_t* L_4 = ___eventName0;
		NullCheck(L_3);
		List_1_Add_m1685793073(L_3, L_4, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
	}

IL_001a:
	{
		String_t* L_5 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_6 = FsmEvent_FindEvent_m3186976377(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		FsmEvent_t3736299882 * L_7 = L_6;
		G_B3_0 = L_7;
		if (L_7)
		{
			G_B4_0 = L_7;
			goto IL_002a;
		}
	}
	{
		String_t* L_8 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_9 = FsmEvent_GetFsmEvent_m2862863017(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		G_B4_0 = L_9;
	}

IL_002a:
	{
		FsmEvent_t3736299882 * L_10 = G_B4_0;
		NullCheck(L_10);
		FsmEvent_set_IsGlobal_m3065222085(L_10, (bool)1, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void PlayMakerGlobals::AddGlobalEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_AddGlobalEvent_m3539697052 (RuntimeObject * __this /* static, unused */, String_t* ___eventName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_AddGlobalEvent_m3539697052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_t1863708399 * L_0 = PlayMakerGlobals_get_Instance_m782995651(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = PlayMakerGlobals_get_Events_m1555858756(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___eventName0;
		NullCheck(L_1);
		bool L_3 = List_1_Contains_m3607818559(L_1, L_2, /*hidden argument*/List_1_Contains_m3607818559_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		PlayMakerGlobals_t1863708399 * L_4 = PlayMakerGlobals_get_Instance_m782995651(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_t3319525431 * L_5 = PlayMakerGlobals_get_Events_m1555858756(L_4, /*hidden argument*/NULL);
		String_t* L_6 = ___eventName0;
		NullCheck(L_5);
		List_1_Add_m1685793073(L_5, L_6, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		return;
	}
}
// System.Void PlayMakerGlobals::RemoveGlobalEvent(System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_RemoveGlobalEvent_m2206567853 (RuntimeObject * __this /* static, unused */, String_t* ___eventName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_RemoveGlobalEvent_m2206567853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass36_0_t1424664817 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass36_0_t1424664817 * L_0 = (U3CU3Ec__DisplayClass36_0_t1424664817 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass36_0_t1424664817_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass36_0__ctor_m1135129098(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass36_0_t1424664817 * L_1 = V_0;
		String_t* L_2 = ___eventName0;
		NullCheck(L_1);
		L_1->set_eventName_0(L_2);
		PlayMakerGlobals_t1863708399 * L_3 = PlayMakerGlobals_get_Instance_m782995651(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_t3319525431 * L_4 = L_3->get_events_11();
		U3CU3Ec__DisplayClass36_0_t1424664817 * L_5 = V_0;
		intptr_t L_6 = (intptr_t)U3CU3Ec__DisplayClass36_0_U3CRemoveGlobalEventU3Eb__0_m2080862436_RuntimeMethod_var;
		Predicate_1_t2672744813 * L_7 = (Predicate_1_t2672744813 *)il2cpp_codegen_object_new(Predicate_1_t2672744813_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m201911024(L_7, L_5, L_6, /*hidden argument*/Predicate_1__ctor_m201911024_RuntimeMethod_var);
		NullCheck(L_4);
		List_1_RemoveAll_m1452861271(L_4, L_7, /*hidden argument*/List_1_RemoveAll_m1452861271_RuntimeMethod_var);
		return;
	}
}
// System.Void PlayMakerGlobals::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_OnEnable_m2058067968 (PlayMakerGlobals_t1863708399 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PlayMakerGlobals::OnDestroy()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals_OnDestroy_m609605670 (PlayMakerGlobals_t1863708399 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_OnDestroy_m609605670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_set_Initialized_m3402930854(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		((PlayMakerGlobals_t1863708399_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGlobals_t1863708399_il2cpp_TypeInfo_var))->set_instance_9((PlayMakerGlobals_t1863708399 *)NULL);
		return;
	}
}
// System.Void PlayMakerGlobals::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerGlobals__ctor_m723307631 (PlayMakerGlobals_t1863708399 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals__ctor_m723307631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmVariables_t3349589544 * L_0 = (FsmVariables_t3349589544 *)il2cpp_codegen_object_new(FsmVariables_t3349589544_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m565553959(L_0, /*hidden argument*/NULL);
		__this->set_variables_10(L_0);
		List_1_t3319525431 * L_1 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_1, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		__this->set_events_11(L_1);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerGlobals/<>c__DisplayClass36_0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass36_0__ctor_m1135129098 (U3CU3Ec__DisplayClass36_0_t1424664817 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayMakerGlobals/<>c__DisplayClass36_0::<RemoveGlobalEvent>b__0(System.String)
extern "C" IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass36_0_U3CRemoveGlobalEventU3Eb__0_m2080862436 (U3CU3Ec__DisplayClass36_0_t1424664817 * __this, String_t* ___m0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass36_0_U3CRemoveGlobalEventU3Eb__0_m2080862436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___m0;
		String_t* L_1 = __this->get_eventName_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerJointBreak::OnJointBreak(System.Single)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerJointBreak_OnJointBreak_m792863235 (PlayMakerJointBreak_t32499873 * __this, float ___breakForce0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerJointBreak_OnJointBreak_m792863235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleJointBreak_m145287290(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		float L_14 = ___breakForce0;
		NullCheck(L_13);
		Fsm_OnJointBreak_m136230705(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerJointBreak::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerJointBreak__ctor_m119105392 (PlayMakerJointBreak_t32499873 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerJointBreak2D::OnJointBreak2D(UnityEngine.Joint2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerJointBreak2D_OnJointBreak2D_m3011672567 (PlayMakerJointBreak2D_t628918649 * __this, Joint2D_t4180440564 * ___brokenJoint0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerJointBreak2D_OnJointBreak2D_m3011672567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleJointBreak2D_m621821210(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Joint2D_t4180440564 * L_14 = ___brokenJoint0;
		NullCheck(L_13);
		Fsm_OnJointBreak2D_m1228224085(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerJointBreak2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerJointBreak2D__ctor_m2803510393 (PlayMakerJointBreak2D_t628918649 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerLateUpdate::LateUpdate()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerLateUpdate_LateUpdate_m333097745 (PlayMakerLateUpdate_t211890678 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerLateUpdate_LateUpdate_m333097745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0046;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleLateUpdate_m1605664630(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0042;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Fsm_LateUpdate_m354612717(L_13, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_15 = V_0;
		List_1_t3085084973 * L_16 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m2358759014(L_16, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerLateUpdate::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerLateUpdate__ctor_m3176117926 (PlayMakerLateUpdate_t211890678 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerMouseEvents::OnMouseEnter()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseEnter_m3395333956 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseEnter_m3395333956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseEnter_m1471820455(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseDown()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseDown_m436448441 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseDown_m436448441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseDown_m712643734(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseUp()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseUp_m2433996139 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseUp_m2433996139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_004e;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseUp_m1067072383(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t4127147824_il2cpp_TypeInfo_var);
		Fsm_set_LastClickedObject_m1315180585(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_004a:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_004e:
	{
		int32_t L_15 = V_0;
		List_1_t3085084973 * L_16 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m2358759014(L_16, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseUpAsButton()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseUpAsButton_m3452186487 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseUpAsButton_m3452186487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_004e;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004a;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseUpAsButton_m126250691(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_13 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t4127147824_il2cpp_TypeInfo_var);
		Fsm_set_LastClickedObject_m1315180585(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_004a:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_004e:
	{
		int32_t L_15 = V_0;
		List_1_t3085084973 * L_16 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m2358759014(L_16, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseExit()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseExit_m1940162432 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseExit_m1940162432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseExit_m878588781(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseDrag()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseDrag_m3560491499 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseDrag_m3560491499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseDrag_m2080773280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseOver()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents_OnMouseOver_m2287848480 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseOver_m2287848480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t4127147824 * L_8 = PlayMakerFSM_get_Fsm_m2313055607(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Fsm_get_MouseEvents_m1397553736(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_10 = V_1;
		NullCheck(L_10);
		Fsm_t4127147824 * L_11 = PlayMakerFSM_get_Fsm_m2313055607(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t3736299882_il2cpp_TypeInfo_var);
		FsmEvent_t3736299882 * L_12 = FsmEvent_get_MouseOver_m1174616455(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Fsm_Event_m1030970055(L_11, L_12, /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_0;
		List_1_t3085084973 * L_15 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_15);
		int32_t L_16 = List_1_get_Count_m2358759014(L_15, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerMouseEvents__ctor_m2862058348 (PlayMakerMouseEvents_t4026808530 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerOnGUI::Start()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerOnGUI_Start_m2796628171 (PlayMakerOnGUI_t3357975613 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_Start_m2796628171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSM_t1613010231 * L_0 = __this->get_playMakerFSM_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_2 = __this->get_playMakerFSM_4();
		NullCheck(L_2);
		Fsm_t4127147824 * L_3 = PlayMakerFSM_get_Fsm_m2313055607(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Fsm_set_HandleOnGUI_m3034596896(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerOnGUI_OnGUI_m2523943160 (PlayMakerOnGUI_t3357975613 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_OnGUI_m2523943160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_previewInEditMode_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m100394690(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		PlayMakerOnGUI_DoEditGUI_m1319786893(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		PlayMakerFSM_t1613010231 * L_2 = __this->get_playMakerFSM_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_4 = __this->get_playMakerFSM_4();
		NullCheck(L_4);
		Fsm_t4127147824 * L_5 = PlayMakerFSM_get_Fsm_m2313055607(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_6 = __this->get_playMakerFSM_4();
		NullCheck(L_6);
		Fsm_t4127147824 * L_7 = PlayMakerFSM_get_Fsm_m2313055607(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleOnGUI_m3287859215(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = __this->get_playMakerFSM_4();
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_OnGUI_m715957636(L_10, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::DoEditGUI()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerOnGUI_DoEditGUI_m1319786893 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_DoEditGUI_m1319786893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmState_t4124398234 * V_0 = NULL;
	FsmStateActionU5BU5D_t1918078376* V_1 = NULL;
	int32_t V_2 = 0;
	FsmStateAction_t3801304101 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Fsm_t4127147824 * L_0 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SelectedFSM_5();
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var);
		Fsm_t4127147824 * L_1 = ((PlayMakerGUI_t1571180761_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerGUI_t1571180761_il2cpp_TypeInfo_var))->get_SelectedFSM_5();
		NullCheck(L_1);
		FsmState_t4124398234 * L_2 = Fsm_get_EditState_m1641309013(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmState_t4124398234 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		FsmState_t4124398234 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = FsmState_get_IsInitialized_m496409664(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		FsmState_t4124398234 * L_6 = V_0;
		NullCheck(L_6);
		FsmStateActionU5BU5D_t1918078376* L_7 = FsmState_get_Actions_m2096890200(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_003e;
	}

IL_0028:
	{
		FsmStateActionU5BU5D_t1918078376* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		FsmStateAction_t3801304101 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		FsmStateAction_t3801304101 * L_12 = V_3;
		NullCheck(L_12);
		bool L_13 = FsmStateAction_get_Active_m3504448199(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_t3801304101 * L_14 = V_3;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(35 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_14);
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_003e:
	{
		int32_t L_16 = V_2;
		FsmStateActionU5BU5D_t1918078376* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_0028;
		}
	}

IL_0044:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerOnGUI__ctor_m1459522343 (PlayMakerOnGUI_t3357975613 * __this, const RuntimeMethod* method)
{
	{
		__this->set_previewInEditMode_5((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerParticleCollision::OnParticleCollision(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerParticleCollision_OnParticleCollision_m1117432973 (PlayMakerParticleCollision_t716698883 * __this, GameObject_t1113636619 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerParticleCollision_OnParticleCollision_m1117432973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleParticleCollision_m2694133425(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnParticleCollision_m3691854700(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerParticleCollision::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerParticleCollision__ctor_m3571827361 (PlayMakerParticleCollision_t716698883 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern "C" IL2CPP_METHOD_ATTR PlayMakerPrefs_t2834574540 * PlayMakerPrefs_get_Instance_m4280752655 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_Instance_m4280752655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Object_t631007953 * L_2 = Resources_Load_m3880010804(NULL /*static, unused*/, _stringLiteral1829750146, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->set_instance_4(((PlayMakerPrefs_t2834574540 *)IsInstClass((RuntimeObject*)L_2, PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var)));
		PlayMakerPrefs_t2834574540 * L_3 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		PlayMakerPrefs_t2834574540 * L_5 = ScriptableObject_CreateInstance_TisPlayMakerPrefs_t2834574540_m1074476695(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerPrefs_t2834574540_m1074476695_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->set_instance_4(L_5);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_6 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_instance_4();
		return L_6;
	}
}
// System.Boolean PlayMakerPrefs::get_LogPerformanceWarnings()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerPrefs_get_LogPerformanceWarnings_m2853607083 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_LogPerformanceWarnings_m2853607083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logPerformanceWarnings_7();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_LogPerformanceWarnings(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_set_LogPerformanceWarnings_m3158938153 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_LogPerformanceWarnings_m3158938153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_logPerformanceWarnings_7(L_1);
		return;
	}
}
// System.Boolean PlayMakerPrefs::get_ShowEventHandlerComponents()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerPrefs_get_ShowEventHandlerComponents_m947219729 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_ShowEventHandlerComponents_m947219729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_showEventHandlerComponents_8();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_ShowEventHandlerComponents(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_set_ShowEventHandlerComponents_m2653779661 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_ShowEventHandlerComponents_m2653779661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_showEventHandlerComponents_8(L_1);
		return;
	}
}
// UnityEngine.Color PlayMakerPrefs::get_TweenFromColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerPrefs_get_TweenFromColor_m2505016700 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_TweenFromColor_m2505016700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Color_t2555686324  L_1 = L_0->get_tweenFromColor_11();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_TweenFromColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_set_TweenFromColor_m1247798575 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_TweenFromColor_m1247798575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2555686324  L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_tweenFromColor_11(L_1);
		return;
	}
}
// UnityEngine.Color PlayMakerPrefs::get_TweenToColor()
extern "C" IL2CPP_METHOD_ATTR Color_t2555686324  PlayMakerPrefs_get_TweenToColor_m2388405964 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_TweenToColor_m2388405964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Color_t2555686324  L_1 = L_0->get_tweenToColor_12();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_TweenToColor(UnityEngine.Color)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_set_TweenToColor_m2761377687 (RuntimeObject * __this /* static, unused */, Color_t2555686324  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_TweenToColor_m2761377687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2555686324  L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_tweenToColor_12(L_1);
		return;
	}
}
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t941916413* PlayMakerPrefs_get_Colors_m3509984984 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_Colors_m3509984984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ColorU5BU5D_t941916413* L_1 = L_0->get_colors_9();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_Colors(UnityEngine.Color[])
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_set_Colors_m891229754 (RuntimeObject * __this /* static, unused */, ColorU5BU5D_t941916413* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_Colors_m891229754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorU5BU5D_t941916413* L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_colors_9(L_1);
		return;
	}
}
// System.String[] PlayMakerPrefs::get_ColorNames()
extern "C" IL2CPP_METHOD_ATTR StringU5BU5D_t1281789340* PlayMakerPrefs_get_ColorNames_m556735336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_ColorNames_m556735336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringU5BU5D_t1281789340* L_1 = L_0->get_colorNames_10();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_ColorNames(System.String[])
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_set_ColorNames_m1678819117 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_ColorNames_m1678819117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t2834574540 * L_0 = PlayMakerPrefs_get_Instance_m4280752655(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringU5BU5D_t1281789340* L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_colorNames_10(L_1);
		return;
	}
}
// System.Void PlayMakerPrefs::ResetDefaultColors()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_ResetDefaultColors_m1044259044 (PlayMakerPrefs_t2834574540 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_ResetDefaultColors_m1044259044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m286683560((&L_0), (0.9372549f), (0.345098048f), (0.007843138f), /*hidden argument*/NULL);
		__this->set_tweenFromColor_11(L_0);
		Color_t2555686324  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m286683560((&L_1), (0.992156863f), (0.5882353f), (0.0156862754f), /*hidden argument*/NULL);
		__this->set_tweenToColor_12(L_1);
		V_0 = 0;
		goto IL_0062;
	}

IL_0038:
	{
		ColorU5BU5D_t941916413* L_2 = __this->get_colors_9();
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_4 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_defaultColors_5();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Color_t2555686324  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Color_t2555686324 )L_7);
		StringU5BU5D_t1281789340* L_8 = __this->get_colorNames_10();
		int32_t L_9 = V_0;
		StringU5BU5D_t1281789340* L_10 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_defaultColorNames_6();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_13);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_13);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0062:
	{
		int32_t L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_16 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_defaultColors_5();
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))))))
		{
			goto IL_0038;
		}
	}
	{
		return;
	}
}
// UnityEngine.Color[] PlayMakerPrefs::get_MinimapColors()
extern "C" IL2CPP_METHOD_ATTR ColorU5BU5D_t941916413* PlayMakerPrefs_get_MinimapColors_m3269389195 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_MinimapColors_m3269389195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_0 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_minimapColors_13();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_UpdateMinimapColors_m3596199612(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_1 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_minimapColors_13();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::SaveChanges()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_SaveChanges_m571565725 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_SaveChanges_m571565725_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		PlayMakerPrefs_UpdateMinimapColors_m3596199612(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs_UpdateMinimapColors_m3596199612 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_UpdateMinimapColors_m3596199612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_0 = PlayMakerPrefs_get_Colors_m3509984984(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ColorU5BU5D_t941916413* L_1 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))));
		((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->set_minimapColors_13(L_1);
		V_0 = 0;
		goto IL_004c;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_2 = PlayMakerPrefs_get_Colors_m3509984984(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Color_t2555686324  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		ColorU5BU5D_t941916413* L_6 = ((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->get_minimapColors_13();
		int32_t L_7 = V_0;
		Color_t2555686324  L_8 = V_1;
		float L_9 = L_8.get_r_0();
		Color_t2555686324  L_10 = V_1;
		float L_11 = L_10.get_g_1();
		Color_t2555686324  L_12 = V_1;
		float L_13 = L_12.get_b_2();
		Color_t2555686324  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Color__ctor_m2943235014((&L_14), L_9, L_11, L_13, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (Color_t2555686324 )L_14);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_004c:
	{
		int32_t L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var);
		ColorU5BU5D_t941916413* L_17 = PlayMakerPrefs_get_Colors_m3509984984(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_17)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerPrefs::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs__ctor_m3564739458 (PlayMakerPrefs_t2834574540 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs__ctor_m3564739458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_logPerformanceWarnings_7((bool)1);
		ColorU5BU5D_t941916413* L_0 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		ColorU5BU5D_t941916413* L_1 = L_0;
		Color_t2555686324  L_2 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Color_t2555686324 )L_2);
		ColorU5BU5D_t941916413* L_3 = L_1;
		Color_t2555686324  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m286683560((&L_4), (0.545098066f), (0.670588255f), (0.9411765f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Color_t2555686324 )L_4);
		ColorU5BU5D_t941916413* L_5 = L_3;
		Color_t2555686324  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m286683560((&L_6), (0.243137255f), (0.7607843f), (0.6901961f), /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Color_t2555686324 )L_6);
		ColorU5BU5D_t941916413* L_7 = L_5;
		Color_t2555686324  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m286683560((&L_8), (0.431372553f), (0.7607843f), (0.243137255f), /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Color_t2555686324 )L_8);
		ColorU5BU5D_t941916413* L_9 = L_7;
		Color_t2555686324  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m286683560((&L_10), (1.0f), (0.8745098f), (0.1882353f), /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (Color_t2555686324 )L_10);
		ColorU5BU5D_t941916413* L_11 = L_9;
		Color_t2555686324  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color__ctor_m286683560((&L_12), (1.0f), (0.5529412f), (0.1882353f), /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Color_t2555686324 )L_12);
		ColorU5BU5D_t941916413* L_13 = L_11;
		Color_t2555686324  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Color__ctor_m286683560((&L_14), (0.7607843f), (0.243137255f), (0.2509804f), /*hidden argument*/NULL);
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Color_t2555686324 )L_14);
		ColorU5BU5D_t941916413* L_15 = L_13;
		Color_t2555686324  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color__ctor_m286683560((&L_16), (0.545098066f), (0.243137255f), (0.7607843f), /*hidden argument*/NULL);
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(7), (Color_t2555686324 )L_16);
		ColorU5BU5D_t941916413* L_17 = L_15;
		Color_t2555686324  L_18 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Color_t2555686324 )L_18);
		ColorU5BU5D_t941916413* L_19 = L_17;
		Color_t2555686324  L_20 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Color_t2555686324 )L_20);
		ColorU5BU5D_t941916413* L_21 = L_19;
		Color_t2555686324  L_22 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Color_t2555686324 )L_22);
		ColorU5BU5D_t941916413* L_23 = L_21;
		Color_t2555686324  L_24 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Color_t2555686324 )L_24);
		ColorU5BU5D_t941916413* L_25 = L_23;
		Color_t2555686324  L_26 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Color_t2555686324 )L_26);
		ColorU5BU5D_t941916413* L_27 = L_25;
		Color_t2555686324  L_28 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Color_t2555686324 )L_28);
		ColorU5BU5D_t941916413* L_29 = L_27;
		Color_t2555686324  L_30 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Color_t2555686324 )L_30);
		ColorU5BU5D_t941916413* L_31 = L_29;
		Color_t2555686324  L_32 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Color_t2555686324 )L_32);
		ColorU5BU5D_t941916413* L_33 = L_31;
		Color_t2555686324  L_34 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Color_t2555686324 )L_34);
		ColorU5BU5D_t941916413* L_35 = L_33;
		Color_t2555686324  L_36 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Color_t2555686324 )L_36);
		ColorU5BU5D_t941916413* L_37 = L_35;
		Color_t2555686324  L_38 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Color_t2555686324 )L_38);
		ColorU5BU5D_t941916413* L_39 = L_37;
		Color_t2555686324  L_40 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Color_t2555686324 )L_40);
		ColorU5BU5D_t941916413* L_41 = L_39;
		Color_t2555686324  L_42 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Color_t2555686324 )L_42);
		ColorU5BU5D_t941916413* L_43 = L_41;
		Color_t2555686324  L_44 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_43);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Color_t2555686324 )L_44);
		ColorU5BU5D_t941916413* L_45 = L_43;
		Color_t2555686324  L_46 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Color_t2555686324 )L_46);
		ColorU5BU5D_t941916413* L_47 = L_45;
		Color_t2555686324  L_48 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Color_t2555686324 )L_48);
		__this->set_colors_9(L_47);
		StringU5BU5D_t1281789340* L_49 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		StringU5BU5D_t1281789340* L_50 = L_49;
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, _stringLiteral1948333211);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1948333211);
		StringU5BU5D_t1281789340* L_51 = L_50;
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, _stringLiteral4001789887);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral4001789887);
		StringU5BU5D_t1281789340* L_52 = L_51;
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, _stringLiteral1580592898);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1580592898);
		StringU5BU5D_t1281789340* L_53 = L_52;
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, _stringLiteral3552476482);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3552476482);
		StringU5BU5D_t1281789340* L_54 = L_53;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, _stringLiteral4876274);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4876274);
		StringU5BU5D_t1281789340* L_55 = L_54;
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, _stringLiteral423668695);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral423668695);
		StringU5BU5D_t1281789340* L_56 = L_55;
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, _stringLiteral3265678564);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3265678564);
		StringU5BU5D_t1281789340* L_57 = L_56;
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, _stringLiteral811194538);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral811194538);
		StringU5BU5D_t1281789340* L_58 = L_57;
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, _stringLiteral757602046);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_59 = L_58;
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, _stringLiteral757602046);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_60 = L_59;
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, _stringLiteral757602046);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_61 = L_60;
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, _stringLiteral757602046);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_62 = L_61;
		NullCheck(L_62);
		ArrayElementTypeCheck (L_62, _stringLiteral757602046);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_63 = L_62;
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, _stringLiteral757602046);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_64 = L_63;
		NullCheck(L_64);
		ArrayElementTypeCheck (L_64, _stringLiteral757602046);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_65 = L_64;
		NullCheck(L_65);
		ArrayElementTypeCheck (L_65, _stringLiteral757602046);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_66 = L_65;
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, _stringLiteral757602046);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_67 = L_66;
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, _stringLiteral757602046);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_68 = L_67;
		NullCheck(L_68);
		ArrayElementTypeCheck (L_68, _stringLiteral757602046);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_69 = L_68;
		NullCheck(L_69);
		ArrayElementTypeCheck (L_69, _stringLiteral757602046);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_70 = L_69;
		NullCheck(L_70);
		ArrayElementTypeCheck (L_70, _stringLiteral757602046);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_71 = L_70;
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, _stringLiteral757602046);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_72 = L_71;
		NullCheck(L_72);
		ArrayElementTypeCheck (L_72, _stringLiteral757602046);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (String_t*)_stringLiteral757602046);
		StringU5BU5D_t1281789340* L_73 = L_72;
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, _stringLiteral757602046);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (String_t*)_stringLiteral757602046);
		__this->set_colorNames_10(L_73);
		Color_t2555686324  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Color__ctor_m286683560((&L_74), (0.007843138f), (0.4117647f), (0.9843137f), /*hidden argument*/NULL);
		__this->set_tweenFromColor_11(L_74);
		Color_t2555686324  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Color__ctor_m286683560((&L_75), (0.992156863f), (0.5882353f), (0.0156862754f), /*hidden argument*/NULL);
		__this->set_tweenToColor_12(L_75);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerPrefs::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerPrefs__cctor_m3014713217 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs__cctor_m3014713217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ColorU5BU5D_t941916413* L_0 = (ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)8);
		ColorU5BU5D_t941916413* L_1 = L_0;
		Color_t2555686324  L_2 = Color_get_grey_m3440705476(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Color_t2555686324 )L_2);
		ColorU5BU5D_t941916413* L_3 = L_1;
		Color_t2555686324  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m286683560((&L_4), (0.545098066f), (0.670588255f), (0.9411765f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Color_t2555686324 )L_4);
		ColorU5BU5D_t941916413* L_5 = L_3;
		Color_t2555686324  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m286683560((&L_6), (0.243137255f), (0.7607843f), (0.6901961f), /*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Color_t2555686324 )L_6);
		ColorU5BU5D_t941916413* L_7 = L_5;
		Color_t2555686324  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m286683560((&L_8), (0.431372553f), (0.7607843f), (0.243137255f), /*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Color_t2555686324 )L_8);
		ColorU5BU5D_t941916413* L_9 = L_7;
		Color_t2555686324  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m286683560((&L_10), (1.0f), (0.8745098f), (0.1882353f), /*hidden argument*/NULL);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (Color_t2555686324 )L_10);
		ColorU5BU5D_t941916413* L_11 = L_9;
		Color_t2555686324  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color__ctor_m286683560((&L_12), (1.0f), (0.5529412f), (0.1882353f), /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Color_t2555686324 )L_12);
		ColorU5BU5D_t941916413* L_13 = L_11;
		Color_t2555686324  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Color__ctor_m286683560((&L_14), (0.7607843f), (0.243137255f), (0.2509804f), /*hidden argument*/NULL);
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Color_t2555686324 )L_14);
		ColorU5BU5D_t941916413* L_15 = L_13;
		Color_t2555686324  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color__ctor_m286683560((&L_16), (0.545098066f), (0.243137255f), (0.7607843f), /*hidden argument*/NULL);
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(7), (Color_t2555686324 )L_16);
		((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->set_defaultColors_5(L_15);
		StringU5BU5D_t1281789340* L_17 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)8);
		StringU5BU5D_t1281789340* L_18 = L_17;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral1948333211);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1948333211);
		StringU5BU5D_t1281789340* L_19 = L_18;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral4001789887);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral4001789887);
		StringU5BU5D_t1281789340* L_20 = L_19;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1580592898);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1580592898);
		StringU5BU5D_t1281789340* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral3552476482);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3552476482);
		StringU5BU5D_t1281789340* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral4876274);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral4876274);
		StringU5BU5D_t1281789340* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral423668695);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral423668695);
		StringU5BU5D_t1281789340* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral3265678564);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3265678564);
		StringU5BU5D_t1281789340* L_25 = L_24;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral811194538);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral811194538);
		((PlayMakerPrefs_t2834574540_StaticFields*)il2cpp_codegen_static_fields_for(PlayMakerPrefs_t2834574540_il2cpp_TypeInfo_var))->set_defaultColorNames_6(L_25);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// PlayMakerFSM[] PlayMakerProxyBase::get_playMakerFSMs()
extern "C" IL2CPP_METHOD_ATTR PlayMakerFSMU5BU5D_t560886798* PlayMakerProxyBase_get_playMakerFSMs_m3489375989 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_get_playMakerFSMs_m3489375989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3085084973 * L_0 = __this->get_TargetFSMs_4();
		NullCheck(L_0);
		PlayMakerFSMU5BU5D_t560886798* L_1 = List_1_ToArray_m3961749393(L_0, /*hidden argument*/List_1_ToArray_m3961749393_RuntimeMethod_var);
		return L_1;
	}
}
// System.Void PlayMakerProxyBase::add_TriggerEventCallback(PlayMakerProxyBase/TriggerEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_TriggerEventCallback_m3172485254 (PlayMakerProxyBase_t90512809 * __this, TriggerEvent_t1933226311 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_add_TriggerEventCallback_m3172485254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TriggerEvent_t1933226311 * V_0 = NULL;
	TriggerEvent_t1933226311 * V_1 = NULL;
	TriggerEvent_t1933226311 * V_2 = NULL;
	{
		TriggerEvent_t1933226311 * L_0 = __this->get_TriggerEventCallback_5();
		V_0 = L_0;
	}

IL_0007:
	{
		TriggerEvent_t1933226311 * L_1 = V_0;
		V_1 = L_1;
		TriggerEvent_t1933226311 * L_2 = V_1;
		TriggerEvent_t1933226311 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((TriggerEvent_t1933226311 *)CastclassSealed((RuntimeObject*)L_4, TriggerEvent_t1933226311_il2cpp_TypeInfo_var));
		TriggerEvent_t1933226311 ** L_5 = __this->get_address_of_TriggerEventCallback_5();
		TriggerEvent_t1933226311 * L_6 = V_2;
		TriggerEvent_t1933226311 * L_7 = V_1;
		TriggerEvent_t1933226311 * L_8 = InterlockedCompareExchangeImpl<TriggerEvent_t1933226311 *>((TriggerEvent_t1933226311 **)L_5, L_6, L_7);
		V_0 = L_8;
		TriggerEvent_t1933226311 * L_9 = V_0;
		TriggerEvent_t1933226311 * L_10 = V_1;
		if ((!(((RuntimeObject*)(TriggerEvent_t1933226311 *)L_9) == ((RuntimeObject*)(TriggerEvent_t1933226311 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::remove_TriggerEventCallback(PlayMakerProxyBase/TriggerEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_TriggerEventCallback_m1155547507 (PlayMakerProxyBase_t90512809 * __this, TriggerEvent_t1933226311 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_remove_TriggerEventCallback_m1155547507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TriggerEvent_t1933226311 * V_0 = NULL;
	TriggerEvent_t1933226311 * V_1 = NULL;
	TriggerEvent_t1933226311 * V_2 = NULL;
	{
		TriggerEvent_t1933226311 * L_0 = __this->get_TriggerEventCallback_5();
		V_0 = L_0;
	}

IL_0007:
	{
		TriggerEvent_t1933226311 * L_1 = V_0;
		V_1 = L_1;
		TriggerEvent_t1933226311 * L_2 = V_1;
		TriggerEvent_t1933226311 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((TriggerEvent_t1933226311 *)CastclassSealed((RuntimeObject*)L_4, TriggerEvent_t1933226311_il2cpp_TypeInfo_var));
		TriggerEvent_t1933226311 ** L_5 = __this->get_address_of_TriggerEventCallback_5();
		TriggerEvent_t1933226311 * L_6 = V_2;
		TriggerEvent_t1933226311 * L_7 = V_1;
		TriggerEvent_t1933226311 * L_8 = InterlockedCompareExchangeImpl<TriggerEvent_t1933226311 *>((TriggerEvent_t1933226311 **)L_5, L_6, L_7);
		V_0 = L_8;
		TriggerEvent_t1933226311 * L_9 = V_0;
		TriggerEvent_t1933226311 * L_10 = V_1;
		if ((!(((RuntimeObject*)(TriggerEvent_t1933226311 *)L_9) == ((RuntimeObject*)(TriggerEvent_t1933226311 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::add_CollisionEventCallback(PlayMakerProxyBase/CollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_CollisionEventCallback_m555388545 (PlayMakerProxyBase_t90512809 * __this, CollisionEvent_t2644514664 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_add_CollisionEventCallback_m555388545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CollisionEvent_t2644514664 * V_0 = NULL;
	CollisionEvent_t2644514664 * V_1 = NULL;
	CollisionEvent_t2644514664 * V_2 = NULL;
	{
		CollisionEvent_t2644514664 * L_0 = __this->get_CollisionEventCallback_6();
		V_0 = L_0;
	}

IL_0007:
	{
		CollisionEvent_t2644514664 * L_1 = V_0;
		V_1 = L_1;
		CollisionEvent_t2644514664 * L_2 = V_1;
		CollisionEvent_t2644514664 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((CollisionEvent_t2644514664 *)CastclassSealed((RuntimeObject*)L_4, CollisionEvent_t2644514664_il2cpp_TypeInfo_var));
		CollisionEvent_t2644514664 ** L_5 = __this->get_address_of_CollisionEventCallback_6();
		CollisionEvent_t2644514664 * L_6 = V_2;
		CollisionEvent_t2644514664 * L_7 = V_1;
		CollisionEvent_t2644514664 * L_8 = InterlockedCompareExchangeImpl<CollisionEvent_t2644514664 *>((CollisionEvent_t2644514664 **)L_5, L_6, L_7);
		V_0 = L_8;
		CollisionEvent_t2644514664 * L_9 = V_0;
		CollisionEvent_t2644514664 * L_10 = V_1;
		if ((!(((RuntimeObject*)(CollisionEvent_t2644514664 *)L_9) == ((RuntimeObject*)(CollisionEvent_t2644514664 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::remove_CollisionEventCallback(PlayMakerProxyBase/CollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_CollisionEventCallback_m3921268190 (PlayMakerProxyBase_t90512809 * __this, CollisionEvent_t2644514664 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_remove_CollisionEventCallback_m3921268190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CollisionEvent_t2644514664 * V_0 = NULL;
	CollisionEvent_t2644514664 * V_1 = NULL;
	CollisionEvent_t2644514664 * V_2 = NULL;
	{
		CollisionEvent_t2644514664 * L_0 = __this->get_CollisionEventCallback_6();
		V_0 = L_0;
	}

IL_0007:
	{
		CollisionEvent_t2644514664 * L_1 = V_0;
		V_1 = L_1;
		CollisionEvent_t2644514664 * L_2 = V_1;
		CollisionEvent_t2644514664 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((CollisionEvent_t2644514664 *)CastclassSealed((RuntimeObject*)L_4, CollisionEvent_t2644514664_il2cpp_TypeInfo_var));
		CollisionEvent_t2644514664 ** L_5 = __this->get_address_of_CollisionEventCallback_6();
		CollisionEvent_t2644514664 * L_6 = V_2;
		CollisionEvent_t2644514664 * L_7 = V_1;
		CollisionEvent_t2644514664 * L_8 = InterlockedCompareExchangeImpl<CollisionEvent_t2644514664 *>((CollisionEvent_t2644514664 **)L_5, L_6, L_7);
		V_0 = L_8;
		CollisionEvent_t2644514664 * L_9 = V_0;
		CollisionEvent_t2644514664 * L_10 = V_1;
		if ((!(((RuntimeObject*)(CollisionEvent_t2644514664 *)L_9) == ((RuntimeObject*)(CollisionEvent_t2644514664 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::add_ParticleCollisionEventCallback(PlayMakerProxyBase/ParticleCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_ParticleCollisionEventCallback_m371344463 (PlayMakerProxyBase_t90512809 * __this, ParticleCollisionEvent_t3984792766 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_add_ParticleCollisionEventCallback_m371344463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleCollisionEvent_t3984792766 * V_0 = NULL;
	ParticleCollisionEvent_t3984792766 * V_1 = NULL;
	ParticleCollisionEvent_t3984792766 * V_2 = NULL;
	{
		ParticleCollisionEvent_t3984792766 * L_0 = __this->get_ParticleCollisionEventCallback_7();
		V_0 = L_0;
	}

IL_0007:
	{
		ParticleCollisionEvent_t3984792766 * L_1 = V_0;
		V_1 = L_1;
		ParticleCollisionEvent_t3984792766 * L_2 = V_1;
		ParticleCollisionEvent_t3984792766 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((ParticleCollisionEvent_t3984792766 *)CastclassSealed((RuntimeObject*)L_4, ParticleCollisionEvent_t3984792766_il2cpp_TypeInfo_var));
		ParticleCollisionEvent_t3984792766 ** L_5 = __this->get_address_of_ParticleCollisionEventCallback_7();
		ParticleCollisionEvent_t3984792766 * L_6 = V_2;
		ParticleCollisionEvent_t3984792766 * L_7 = V_1;
		ParticleCollisionEvent_t3984792766 * L_8 = InterlockedCompareExchangeImpl<ParticleCollisionEvent_t3984792766 *>((ParticleCollisionEvent_t3984792766 **)L_5, L_6, L_7);
		V_0 = L_8;
		ParticleCollisionEvent_t3984792766 * L_9 = V_0;
		ParticleCollisionEvent_t3984792766 * L_10 = V_1;
		if ((!(((RuntimeObject*)(ParticleCollisionEvent_t3984792766 *)L_9) == ((RuntimeObject*)(ParticleCollisionEvent_t3984792766 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::remove_ParticleCollisionEventCallback(PlayMakerProxyBase/ParticleCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_ParticleCollisionEventCallback_m2118262247 (PlayMakerProxyBase_t90512809 * __this, ParticleCollisionEvent_t3984792766 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_remove_ParticleCollisionEventCallback_m2118262247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleCollisionEvent_t3984792766 * V_0 = NULL;
	ParticleCollisionEvent_t3984792766 * V_1 = NULL;
	ParticleCollisionEvent_t3984792766 * V_2 = NULL;
	{
		ParticleCollisionEvent_t3984792766 * L_0 = __this->get_ParticleCollisionEventCallback_7();
		V_0 = L_0;
	}

IL_0007:
	{
		ParticleCollisionEvent_t3984792766 * L_1 = V_0;
		V_1 = L_1;
		ParticleCollisionEvent_t3984792766 * L_2 = V_1;
		ParticleCollisionEvent_t3984792766 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((ParticleCollisionEvent_t3984792766 *)CastclassSealed((RuntimeObject*)L_4, ParticleCollisionEvent_t3984792766_il2cpp_TypeInfo_var));
		ParticleCollisionEvent_t3984792766 ** L_5 = __this->get_address_of_ParticleCollisionEventCallback_7();
		ParticleCollisionEvent_t3984792766 * L_6 = V_2;
		ParticleCollisionEvent_t3984792766 * L_7 = V_1;
		ParticleCollisionEvent_t3984792766 * L_8 = InterlockedCompareExchangeImpl<ParticleCollisionEvent_t3984792766 *>((ParticleCollisionEvent_t3984792766 **)L_5, L_6, L_7);
		V_0 = L_8;
		ParticleCollisionEvent_t3984792766 * L_9 = V_0;
		ParticleCollisionEvent_t3984792766 * L_10 = V_1;
		if ((!(((RuntimeObject*)(ParticleCollisionEvent_t3984792766 *)L_9) == ((RuntimeObject*)(ParticleCollisionEvent_t3984792766 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::add_ControllerCollisionEventCallback(PlayMakerProxyBase/ControllerCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_ControllerCollisionEventCallback_m399454418 (PlayMakerProxyBase_t90512809 * __this, ControllerCollisionEvent_t2755027817 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_add_ControllerCollisionEventCallback_m399454418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ControllerCollisionEvent_t2755027817 * V_0 = NULL;
	ControllerCollisionEvent_t2755027817 * V_1 = NULL;
	ControllerCollisionEvent_t2755027817 * V_2 = NULL;
	{
		ControllerCollisionEvent_t2755027817 * L_0 = __this->get_ControllerCollisionEventCallback_8();
		V_0 = L_0;
	}

IL_0007:
	{
		ControllerCollisionEvent_t2755027817 * L_1 = V_0;
		V_1 = L_1;
		ControllerCollisionEvent_t2755027817 * L_2 = V_1;
		ControllerCollisionEvent_t2755027817 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((ControllerCollisionEvent_t2755027817 *)CastclassSealed((RuntimeObject*)L_4, ControllerCollisionEvent_t2755027817_il2cpp_TypeInfo_var));
		ControllerCollisionEvent_t2755027817 ** L_5 = __this->get_address_of_ControllerCollisionEventCallback_8();
		ControllerCollisionEvent_t2755027817 * L_6 = V_2;
		ControllerCollisionEvent_t2755027817 * L_7 = V_1;
		ControllerCollisionEvent_t2755027817 * L_8 = InterlockedCompareExchangeImpl<ControllerCollisionEvent_t2755027817 *>((ControllerCollisionEvent_t2755027817 **)L_5, L_6, L_7);
		V_0 = L_8;
		ControllerCollisionEvent_t2755027817 * L_9 = V_0;
		ControllerCollisionEvent_t2755027817 * L_10 = V_1;
		if ((!(((RuntimeObject*)(ControllerCollisionEvent_t2755027817 *)L_9) == ((RuntimeObject*)(ControllerCollisionEvent_t2755027817 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::remove_ControllerCollisionEventCallback(PlayMakerProxyBase/ControllerCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_ControllerCollisionEventCallback_m2917022917 (PlayMakerProxyBase_t90512809 * __this, ControllerCollisionEvent_t2755027817 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_remove_ControllerCollisionEventCallback_m2917022917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ControllerCollisionEvent_t2755027817 * V_0 = NULL;
	ControllerCollisionEvent_t2755027817 * V_1 = NULL;
	ControllerCollisionEvent_t2755027817 * V_2 = NULL;
	{
		ControllerCollisionEvent_t2755027817 * L_0 = __this->get_ControllerCollisionEventCallback_8();
		V_0 = L_0;
	}

IL_0007:
	{
		ControllerCollisionEvent_t2755027817 * L_1 = V_0;
		V_1 = L_1;
		ControllerCollisionEvent_t2755027817 * L_2 = V_1;
		ControllerCollisionEvent_t2755027817 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((ControllerCollisionEvent_t2755027817 *)CastclassSealed((RuntimeObject*)L_4, ControllerCollisionEvent_t2755027817_il2cpp_TypeInfo_var));
		ControllerCollisionEvent_t2755027817 ** L_5 = __this->get_address_of_ControllerCollisionEventCallback_8();
		ControllerCollisionEvent_t2755027817 * L_6 = V_2;
		ControllerCollisionEvent_t2755027817 * L_7 = V_1;
		ControllerCollisionEvent_t2755027817 * L_8 = InterlockedCompareExchangeImpl<ControllerCollisionEvent_t2755027817 *>((ControllerCollisionEvent_t2755027817 **)L_5, L_6, L_7);
		V_0 = L_8;
		ControllerCollisionEvent_t2755027817 * L_9 = V_0;
		ControllerCollisionEvent_t2755027817 * L_10 = V_1;
		if ((!(((RuntimeObject*)(ControllerCollisionEvent_t2755027817 *)L_9) == ((RuntimeObject*)(ControllerCollisionEvent_t2755027817 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::add_Trigger2DEventCallback(PlayMakerProxyBase/Trigger2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_Trigger2DEventCallback_m3684239397 (PlayMakerProxyBase_t90512809 * __this, Trigger2DEvent_t2411364400 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_add_Trigger2DEventCallback_m3684239397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Trigger2DEvent_t2411364400 * V_0 = NULL;
	Trigger2DEvent_t2411364400 * V_1 = NULL;
	Trigger2DEvent_t2411364400 * V_2 = NULL;
	{
		Trigger2DEvent_t2411364400 * L_0 = __this->get_Trigger2DEventCallback_9();
		V_0 = L_0;
	}

IL_0007:
	{
		Trigger2DEvent_t2411364400 * L_1 = V_0;
		V_1 = L_1;
		Trigger2DEvent_t2411364400 * L_2 = V_1;
		Trigger2DEvent_t2411364400 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Trigger2DEvent_t2411364400 *)CastclassSealed((RuntimeObject*)L_4, Trigger2DEvent_t2411364400_il2cpp_TypeInfo_var));
		Trigger2DEvent_t2411364400 ** L_5 = __this->get_address_of_Trigger2DEventCallback_9();
		Trigger2DEvent_t2411364400 * L_6 = V_2;
		Trigger2DEvent_t2411364400 * L_7 = V_1;
		Trigger2DEvent_t2411364400 * L_8 = InterlockedCompareExchangeImpl<Trigger2DEvent_t2411364400 *>((Trigger2DEvent_t2411364400 **)L_5, L_6, L_7);
		V_0 = L_8;
		Trigger2DEvent_t2411364400 * L_9 = V_0;
		Trigger2DEvent_t2411364400 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Trigger2DEvent_t2411364400 *)L_9) == ((RuntimeObject*)(Trigger2DEvent_t2411364400 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::remove_Trigger2DEventCallback(PlayMakerProxyBase/Trigger2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_Trigger2DEventCallback_m3473419264 (PlayMakerProxyBase_t90512809 * __this, Trigger2DEvent_t2411364400 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_remove_Trigger2DEventCallback_m3473419264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Trigger2DEvent_t2411364400 * V_0 = NULL;
	Trigger2DEvent_t2411364400 * V_1 = NULL;
	Trigger2DEvent_t2411364400 * V_2 = NULL;
	{
		Trigger2DEvent_t2411364400 * L_0 = __this->get_Trigger2DEventCallback_9();
		V_0 = L_0;
	}

IL_0007:
	{
		Trigger2DEvent_t2411364400 * L_1 = V_0;
		V_1 = L_1;
		Trigger2DEvent_t2411364400 * L_2 = V_1;
		Trigger2DEvent_t2411364400 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Trigger2DEvent_t2411364400 *)CastclassSealed((RuntimeObject*)L_4, Trigger2DEvent_t2411364400_il2cpp_TypeInfo_var));
		Trigger2DEvent_t2411364400 ** L_5 = __this->get_address_of_Trigger2DEventCallback_9();
		Trigger2DEvent_t2411364400 * L_6 = V_2;
		Trigger2DEvent_t2411364400 * L_7 = V_1;
		Trigger2DEvent_t2411364400 * L_8 = InterlockedCompareExchangeImpl<Trigger2DEvent_t2411364400 *>((Trigger2DEvent_t2411364400 **)L_5, L_6, L_7);
		V_0 = L_8;
		Trigger2DEvent_t2411364400 * L_9 = V_0;
		Trigger2DEvent_t2411364400 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Trigger2DEvent_t2411364400 *)L_9) == ((RuntimeObject*)(Trigger2DEvent_t2411364400 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::add_Collision2DEventCallback(PlayMakerProxyBase/Collision2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_add_Collision2DEventCallback_m1748445263 (PlayMakerProxyBase_t90512809 * __this, Collision2DEvent_t1483235594 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_add_Collision2DEventCallback_m1748445263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collision2DEvent_t1483235594 * V_0 = NULL;
	Collision2DEvent_t1483235594 * V_1 = NULL;
	Collision2DEvent_t1483235594 * V_2 = NULL;
	{
		Collision2DEvent_t1483235594 * L_0 = __this->get_Collision2DEventCallback_10();
		V_0 = L_0;
	}

IL_0007:
	{
		Collision2DEvent_t1483235594 * L_1 = V_0;
		V_1 = L_1;
		Collision2DEvent_t1483235594 * L_2 = V_1;
		Collision2DEvent_t1483235594 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Collision2DEvent_t1483235594 *)CastclassSealed((RuntimeObject*)L_4, Collision2DEvent_t1483235594_il2cpp_TypeInfo_var));
		Collision2DEvent_t1483235594 ** L_5 = __this->get_address_of_Collision2DEventCallback_10();
		Collision2DEvent_t1483235594 * L_6 = V_2;
		Collision2DEvent_t1483235594 * L_7 = V_1;
		Collision2DEvent_t1483235594 * L_8 = InterlockedCompareExchangeImpl<Collision2DEvent_t1483235594 *>((Collision2DEvent_t1483235594 **)L_5, L_6, L_7);
		V_0 = L_8;
		Collision2DEvent_t1483235594 * L_9 = V_0;
		Collision2DEvent_t1483235594 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Collision2DEvent_t1483235594 *)L_9) == ((RuntimeObject*)(Collision2DEvent_t1483235594 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::remove_Collision2DEventCallback(PlayMakerProxyBase/Collision2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_remove_Collision2DEventCallback_m727498677 (PlayMakerProxyBase_t90512809 * __this, Collision2DEvent_t1483235594 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_remove_Collision2DEventCallback_m727498677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collision2DEvent_t1483235594 * V_0 = NULL;
	Collision2DEvent_t1483235594 * V_1 = NULL;
	Collision2DEvent_t1483235594 * V_2 = NULL;
	{
		Collision2DEvent_t1483235594 * L_0 = __this->get_Collision2DEventCallback_10();
		V_0 = L_0;
	}

IL_0007:
	{
		Collision2DEvent_t1483235594 * L_1 = V_0;
		V_1 = L_1;
		Collision2DEvent_t1483235594 * L_2 = V_1;
		Collision2DEvent_t1483235594 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Collision2DEvent_t1483235594 *)CastclassSealed((RuntimeObject*)L_4, Collision2DEvent_t1483235594_il2cpp_TypeInfo_var));
		Collision2DEvent_t1483235594 ** L_5 = __this->get_address_of_Collision2DEventCallback_10();
		Collision2DEvent_t1483235594 * L_6 = V_2;
		Collision2DEvent_t1483235594 * L_7 = V_1;
		Collision2DEvent_t1483235594 * L_8 = InterlockedCompareExchangeImpl<Collision2DEvent_t1483235594 *>((Collision2DEvent_t1483235594 **)L_5, L_6, L_7);
		V_0 = L_8;
		Collision2DEvent_t1483235594 * L_9 = V_0;
		Collision2DEvent_t1483235594 * L_10 = V_1;
		if ((!(((RuntimeObject*)(Collision2DEvent_t1483235594 *)L_9) == ((RuntimeObject*)(Collision2DEvent_t1483235594 *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::AddTarget(PlayMakerFSM)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddTarget_m3229991323 (PlayMakerProxyBase_t90512809 * __this, PlayMakerFSM_t1613010231 * ___fsmTarget0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_AddTarget_m3229991323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3085084973 * L_0 = __this->get_TargetFSMs_4();
		PlayMakerFSM_t1613010231 * L_1 = ___fsmTarget0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m2497552032(L_0, L_1, /*hidden argument*/List_1_Contains_m2497552032_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		List_1_t3085084973 * L_3 = __this->get_TargetFSMs_4();
		PlayMakerFSM_t1613010231 * L_4 = ___fsmTarget0;
		NullCheck(L_3);
		List_1_Add_m1270336517(L_3, L_4, /*hidden argument*/List_1_Add_m1270336517_RuntimeMethod_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean PlayMakerProxyBase::HasTriggerEventDelegates()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerProxyBase_HasTriggerEventDelegates_m1099719574 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	{
		TriggerEvent_t1933226311 * L_0 = __this->get_TriggerEventCallback_5();
		return (bool)((!(((RuntimeObject*)(TriggerEvent_t1933226311 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void PlayMakerProxyBase::AddTriggerEventCallback(PlayMakerProxyBase/TriggerEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddTriggerEventCallback_m2185791415 (PlayMakerProxyBase_t90512809 * __this, TriggerEvent_t1933226311 * ___triggerEvent0, const RuntimeMethod* method)
{
	{
		TriggerEvent_t1933226311 * L_0 = ___triggerEvent0;
		PlayMakerProxyBase_add_TriggerEventCallback_m3172485254(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::RemoveTriggerEventCallback(PlayMakerProxyBase/TriggerEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_RemoveTriggerEventCallback_m3572569115 (PlayMakerProxyBase_t90512809 * __this, TriggerEvent_t1933226311 * ___triggerEvent0, const RuntimeMethod* method)
{
	{
		TriggerEvent_t1933226311 * L_0 = ___triggerEvent0;
		PlayMakerProxyBase_remove_TriggerEventCallback_m1155547507(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::DoTriggerEventCallback(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoTriggerEventCallback_m3149201495 (PlayMakerProxyBase_t90512809 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	{
		TriggerEvent_t1933226311 * L_0 = __this->get_TriggerEventCallback_5();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TriggerEvent_t1933226311 * L_1 = __this->get_TriggerEventCallback_5();
		Collider_t1773347010 * L_2 = ___other0;
		NullCheck(L_1);
		TriggerEvent_Invoke_m3191717391(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Boolean PlayMakerProxyBase::HasTrigger2DEventDelegates()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerProxyBase_HasTrigger2DEventDelegates_m3366368289 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	{
		Trigger2DEvent_t2411364400 * L_0 = __this->get_Trigger2DEventCallback_9();
		return (bool)((!(((RuntimeObject*)(Trigger2DEvent_t2411364400 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void PlayMakerProxyBase::AddTrigger2DEventCallback(PlayMakerProxyBase/Trigger2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddTrigger2DEventCallback_m3720751470 (PlayMakerProxyBase_t90512809 * __this, Trigger2DEvent_t2411364400 * ___triggerEvent0, const RuntimeMethod* method)
{
	{
		Trigger2DEvent_t2411364400 * L_0 = ___triggerEvent0;
		PlayMakerProxyBase_add_Trigger2DEventCallback_m3684239397(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::RemoveTrigger2DEventCallback(PlayMakerProxyBase/Trigger2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_RemoveTrigger2DEventCallback_m3517238443 (PlayMakerProxyBase_t90512809 * __this, Trigger2DEvent_t2411364400 * ___triggerEvent0, const RuntimeMethod* method)
{
	{
		Trigger2DEvent_t2411364400 * L_0 = ___triggerEvent0;
		PlayMakerProxyBase_remove_Trigger2DEventCallback_m3473419264(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::DoTrigger2DEventCallback(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoTrigger2DEventCallback_m3967730350 (PlayMakerProxyBase_t90512809 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	{
		Trigger2DEvent_t2411364400 * L_0 = __this->get_Trigger2DEventCallback_9();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Trigger2DEvent_t2411364400 * L_1 = __this->get_Trigger2DEventCallback_9();
		Collider2D_t2806799626 * L_2 = ___other0;
		NullCheck(L_1);
		Trigger2DEvent_Invoke_m850316776(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Boolean PlayMakerProxyBase::HasCollisionEventDelegates()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerProxyBase_HasCollisionEventDelegates_m3211919691 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	{
		CollisionEvent_t2644514664 * L_0 = __this->get_CollisionEventCallback_6();
		return (bool)((!(((RuntimeObject*)(CollisionEvent_t2644514664 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void PlayMakerProxyBase::AddCollisionEventCallback(PlayMakerProxyBase/CollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddCollisionEventCallback_m3956002933 (PlayMakerProxyBase_t90512809 * __this, CollisionEvent_t2644514664 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		CollisionEvent_t2644514664 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_add_CollisionEventCallback_m555388545(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::RemoveCollisionEventCallback(PlayMakerProxyBase/CollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_RemoveCollisionEventCallback_m2059578943 (PlayMakerProxyBase_t90512809 * __this, CollisionEvent_t2644514664 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		CollisionEvent_t2644514664 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_remove_CollisionEventCallback_m3921268190(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::DoCollisionEventCallback(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoCollisionEventCallback_m2866947341 (PlayMakerProxyBase_t90512809 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method)
{
	{
		CollisionEvent_t2644514664 * L_0 = __this->get_CollisionEventCallback_6();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		CollisionEvent_t2644514664 * L_1 = __this->get_CollisionEventCallback_6();
		Collision_t4262080450 * L_2 = ___collisionInfo0;
		NullCheck(L_1);
		CollisionEvent_Invoke_m4155047233(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Boolean PlayMakerProxyBase::HasCollision2DEventDelegates()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerProxyBase_HasCollision2DEventDelegates_m2141931333 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	{
		Collision2DEvent_t1483235594 * L_0 = __this->get_Collision2DEventCallback_10();
		return (bool)((!(((RuntimeObject*)(Collision2DEvent_t1483235594 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void PlayMakerProxyBase::AddCollision2DEventCallback(PlayMakerProxyBase/Collision2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddCollision2DEventCallback_m1099308022 (PlayMakerProxyBase_t90512809 * __this, Collision2DEvent_t1483235594 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		Collision2DEvent_t1483235594 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_add_Collision2DEventCallback_m1748445263(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::RemoveCollision2DEventCallback(PlayMakerProxyBase/Collision2DEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_RemoveCollision2DEventCallback_m951683480 (PlayMakerProxyBase_t90512809 * __this, Collision2DEvent_t1483235594 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		Collision2DEvent_t1483235594 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_remove_Collision2DEventCallback_m727498677(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::DoCollision2DEventCallback(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoCollision2DEventCallback_m2085672570 (PlayMakerProxyBase_t90512809 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method)
{
	{
		Collision2DEvent_t1483235594 * L_0 = __this->get_Collision2DEventCallback_10();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Collision2DEvent_t1483235594 * L_1 = __this->get_Collision2DEventCallback_10();
		Collision2D_t2842956331 * L_2 = ___collisionInfo0;
		NullCheck(L_1);
		Collision2DEvent_Invoke_m349239800(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Boolean PlayMakerProxyBase::HasParticleCollisionEventDelegates()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerProxyBase_HasParticleCollisionEventDelegates_m1008501213 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	{
		ParticleCollisionEvent_t3984792766 * L_0 = __this->get_ParticleCollisionEventCallback_7();
		return (bool)((!(((RuntimeObject*)(ParticleCollisionEvent_t3984792766 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void PlayMakerProxyBase::AddParticleCollisionEventCallback(PlayMakerProxyBase/ParticleCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddParticleCollisionEventCallback_m1957404749 (PlayMakerProxyBase_t90512809 * __this, ParticleCollisionEvent_t3984792766 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		ParticleCollisionEvent_t3984792766 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_add_ParticleCollisionEventCallback_m371344463(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::RemoveParticleCollisionEventCallback(PlayMakerProxyBase/ParticleCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_RemoveParticleCollisionEventCallback_m2248582515 (PlayMakerProxyBase_t90512809 * __this, ParticleCollisionEvent_t3984792766 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		ParticleCollisionEvent_t3984792766 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_remove_ParticleCollisionEventCallback_m2118262247(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::DoParticleCollisionEventCallback(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoParticleCollisionEventCallback_m1672057094 (PlayMakerProxyBase_t90512809 * __this, GameObject_t1113636619 * ___go0, const RuntimeMethod* method)
{
	{
		ParticleCollisionEvent_t3984792766 * L_0 = __this->get_ParticleCollisionEventCallback_7();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		ParticleCollisionEvent_t3984792766 * L_1 = __this->get_ParticleCollisionEventCallback_7();
		GameObject_t1113636619 * L_2 = ___go0;
		NullCheck(L_1);
		ParticleCollisionEvent_Invoke_m304638276(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Boolean PlayMakerProxyBase::HasControllerCollisionEventDelegates()
extern "C" IL2CPP_METHOD_ATTR bool PlayMakerProxyBase_HasControllerCollisionEventDelegates_m1222815411 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	{
		ControllerCollisionEvent_t2755027817 * L_0 = __this->get_ControllerCollisionEventCallback_8();
		return (bool)((!(((RuntimeObject*)(ControllerCollisionEvent_t2755027817 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}
}
// System.Void PlayMakerProxyBase::AddControllerCollisionEventCallback(PlayMakerProxyBase/ControllerCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_AddControllerCollisionEventCallback_m1835323668 (PlayMakerProxyBase_t90512809 * __this, ControllerCollisionEvent_t2755027817 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		ControllerCollisionEvent_t2755027817 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_add_ControllerCollisionEventCallback_m399454418(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::RemoveControllerCollisionEventCallback(PlayMakerProxyBase/ControllerCollisionEvent)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_RemoveControllerCollisionEventCallback_m4159684145 (PlayMakerProxyBase_t90512809 * __this, ControllerCollisionEvent_t2755027817 * ___collisionEvent0, const RuntimeMethod* method)
{
	{
		ControllerCollisionEvent_t2755027817 * L_0 = ___collisionEvent0;
		PlayMakerProxyBase_remove_ControllerCollisionEventCallback_m2917022917(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::DoControllerCollisionEventCallback(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase_DoControllerCollisionEventCallback_m1869161337 (PlayMakerProxyBase_t90512809 * __this, ControllerColliderHit_t240592346 * ___hitCollider0, const RuntimeMethod* method)
{
	{
		ControllerCollisionEvent_t2755027817 * L_0 = __this->get_ControllerCollisionEventCallback_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		ControllerCollisionEvent_t2755027817 * L_1 = __this->get_ControllerCollisionEventCallback_8();
		ControllerColliderHit_t240592346 * L_2 = ___hitCollider0;
		NullCheck(L_1);
		ControllerCollisionEvent_Invoke_m4119380781(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void PlayMakerProxyBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerProxyBase__ctor_m2965400855 (PlayMakerProxyBase_t90512809 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase__ctor_m2965400855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3085084973 * L_0 = (List_1_t3085084973 *)il2cpp_codegen_object_new(List_1_t3085084973_il2cpp_TypeInfo_var);
		List_1__ctor_m1624985937(L_0, /*hidden argument*/List_1__ctor_m1624985937_RuntimeMethod_var);
		__this->set_TargetFSMs_4(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerProxyBase/Collision2DEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Collision2DEvent__ctor_m1783112607 (Collision2DEvent_t1483235594 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerProxyBase/Collision2DEvent::Invoke(UnityEngine.Collision2D)
extern "C" IL2CPP_METHOD_ATTR void Collision2DEvent_Invoke_m349239800 (Collision2DEvent_t1483235594 * __this, Collision2D_t2842956331 * ___collisionInfo0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Collision2DEvent_Invoke_m349239800((Collision2DEvent_t1483235594 *)__this->get_prev_9(), ___collisionInfo0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, Collision2D_t2842956331 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___collisionInfo0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, Collision2D_t2842956331 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___collisionInfo0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Collision2D_t2842956331 * >::Invoke(targetMethod, targetThis, ___collisionInfo0);
					else
						GenericVirtActionInvoker1< Collision2D_t2842956331 * >::Invoke(targetMethod, targetThis, ___collisionInfo0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Collision2D_t2842956331 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___collisionInfo0);
					else
						VirtActionInvoker1< Collision2D_t2842956331 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___collisionInfo0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Collision2D_t2842956331 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___collisionInfo0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___collisionInfo0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___collisionInfo0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___collisionInfo0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___collisionInfo0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Collision2D_t2842956331 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___collisionInfo0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerProxyBase/Collision2DEvent::BeginInvoke(UnityEngine.Collision2D,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Collision2DEvent_BeginInvoke_m2103853529 (Collision2DEvent_t1483235594 * __this, Collision2D_t2842956331 * ___collisionInfo0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___collisionInfo0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerProxyBase/Collision2DEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void Collision2DEvent_EndInvoke_m459156775 (Collision2DEvent_t1483235594 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerProxyBase/CollisionEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void CollisionEvent__ctor_m741819270 (CollisionEvent_t2644514664 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerProxyBase/CollisionEvent::Invoke(UnityEngine.Collision)
extern "C" IL2CPP_METHOD_ATTR void CollisionEvent_Invoke_m4155047233 (CollisionEvent_t2644514664 * __this, Collision_t4262080450 * ___collisionInfo0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CollisionEvent_Invoke_m4155047233((CollisionEvent_t2644514664 *)__this->get_prev_9(), ___collisionInfo0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, Collision_t4262080450 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___collisionInfo0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, Collision_t4262080450 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___collisionInfo0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Collision_t4262080450 * >::Invoke(targetMethod, targetThis, ___collisionInfo0);
					else
						GenericVirtActionInvoker1< Collision_t4262080450 * >::Invoke(targetMethod, targetThis, ___collisionInfo0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Collision_t4262080450 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___collisionInfo0);
					else
						VirtActionInvoker1< Collision_t4262080450 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___collisionInfo0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Collision_t4262080450 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___collisionInfo0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___collisionInfo0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___collisionInfo0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___collisionInfo0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___collisionInfo0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Collision_t4262080450 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___collisionInfo0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerProxyBase/CollisionEvent::BeginInvoke(UnityEngine.Collision,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* CollisionEvent_BeginInvoke_m1323125034 (CollisionEvent_t2644514664 * __this, Collision_t4262080450 * ___collisionInfo0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___collisionInfo0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerProxyBase/CollisionEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void CollisionEvent_EndInvoke_m3564874630 (CollisionEvent_t2644514664 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerProxyBase/ControllerCollisionEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ControllerCollisionEvent__ctor_m3686592667 (ControllerCollisionEvent_t2755027817 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerProxyBase/ControllerCollisionEvent::Invoke(UnityEngine.ControllerColliderHit)
extern "C" IL2CPP_METHOD_ATTR void ControllerCollisionEvent_Invoke_m4119380781 (ControllerCollisionEvent_t2755027817 * __this, ControllerColliderHit_t240592346 * ___hitCollider0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ControllerCollisionEvent_Invoke_m4119380781((ControllerCollisionEvent_t2755027817 *)__this->get_prev_9(), ___hitCollider0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, ControllerColliderHit_t240592346 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___hitCollider0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, ControllerColliderHit_t240592346 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___hitCollider0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< ControllerColliderHit_t240592346 * >::Invoke(targetMethod, targetThis, ___hitCollider0);
					else
						GenericVirtActionInvoker1< ControllerColliderHit_t240592346 * >::Invoke(targetMethod, targetThis, ___hitCollider0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< ControllerColliderHit_t240592346 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___hitCollider0);
					else
						VirtActionInvoker1< ControllerColliderHit_t240592346 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___hitCollider0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, ControllerColliderHit_t240592346 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___hitCollider0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___hitCollider0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___hitCollider0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___hitCollider0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___hitCollider0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (ControllerColliderHit_t240592346 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___hitCollider0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerProxyBase/ControllerCollisionEvent::BeginInvoke(UnityEngine.ControllerColliderHit,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ControllerCollisionEvent_BeginInvoke_m963638676 (ControllerCollisionEvent_t2755027817 * __this, ControllerColliderHit_t240592346 * ___hitCollider0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___hitCollider0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerProxyBase/ControllerCollisionEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void ControllerCollisionEvent_EndInvoke_m3095406734 (ControllerCollisionEvent_t2755027817 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerProxyBase/ParticleCollisionEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void ParticleCollisionEvent__ctor_m1105270539 (ParticleCollisionEvent_t3984792766 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerProxyBase/ParticleCollisionEvent::Invoke(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void ParticleCollisionEvent_Invoke_m304638276 (ParticleCollisionEvent_t3984792766 * __this, GameObject_t1113636619 * ___gameObject0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ParticleCollisionEvent_Invoke_m304638276((ParticleCollisionEvent_t3984792766 *)__this->get_prev_9(), ___gameObject0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, GameObject_t1113636619 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___gameObject0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, GameObject_t1113636619 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___gameObject0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< GameObject_t1113636619 * >::Invoke(targetMethod, targetThis, ___gameObject0);
					else
						GenericVirtActionInvoker1< GameObject_t1113636619 * >::Invoke(targetMethod, targetThis, ___gameObject0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< GameObject_t1113636619 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___gameObject0);
					else
						VirtActionInvoker1< GameObject_t1113636619 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___gameObject0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, GameObject_t1113636619 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___gameObject0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___gameObject0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___gameObject0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___gameObject0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___gameObject0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (GameObject_t1113636619 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___gameObject0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerProxyBase/ParticleCollisionEvent::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* ParticleCollisionEvent_BeginInvoke_m1200920531 (ParticleCollisionEvent_t3984792766 * __this, GameObject_t1113636619 * ___gameObject0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___gameObject0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerProxyBase/ParticleCollisionEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void ParticleCollisionEvent_EndInvoke_m544408050 (ParticleCollisionEvent_t3984792766 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerProxyBase/Trigger2DEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Trigger2DEvent__ctor_m4187612268 (Trigger2DEvent_t2411364400 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerProxyBase/Trigger2DEvent::Invoke(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Trigger2DEvent_Invoke_m850316776 (Trigger2DEvent_t2411364400 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Trigger2DEvent_Invoke_m850316776((Trigger2DEvent_t2411364400 *)__this->get_prev_9(), ___other0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, Collider2D_t2806799626 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___other0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, Collider2D_t2806799626 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___other0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Collider2D_t2806799626 * >::Invoke(targetMethod, targetThis, ___other0);
					else
						GenericVirtActionInvoker1< Collider2D_t2806799626 * >::Invoke(targetMethod, targetThis, ___other0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Collider2D_t2806799626 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___other0);
					else
						VirtActionInvoker1< Collider2D_t2806799626 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___other0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Collider2D_t2806799626 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___other0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___other0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___other0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___other0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___other0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Collider2D_t2806799626 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___other0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerProxyBase/Trigger2DEvent::BeginInvoke(UnityEngine.Collider2D,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Trigger2DEvent_BeginInvoke_m375744773 (Trigger2DEvent_t2411364400 * __this, Collider2D_t2806799626 * ___other0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___other0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerProxyBase/Trigger2DEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void Trigger2DEvent_EndInvoke_m534738640 (Trigger2DEvent_t2411364400 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerProxyBase/TriggerEvent::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TriggerEvent__ctor_m3871940368 (TriggerEvent_t1933226311 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayMakerProxyBase/TriggerEvent::Invoke(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void TriggerEvent_Invoke_m3191717391 (TriggerEvent_t1933226311 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TriggerEvent_Invoke_m3191717391((TriggerEvent_t1933226311 *)__this->get_prev_9(), ___other0, method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, Collider_t1773347010 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, ___other0, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, Collider_t1773347010 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___other0, targetMethod);
			}
		}
	}
	else
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Collider_t1773347010 * >::Invoke(targetMethod, targetThis, ___other0);
					else
						GenericVirtActionInvoker1< Collider_t1773347010 * >::Invoke(targetMethod, targetThis, ___other0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Collider_t1773347010 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___other0);
					else
						VirtActionInvoker1< Collider_t1773347010 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___other0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, Collider_t1773347010 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___other0, targetMethod);
			}
		}
		else
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___other0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___other0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___other0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___other0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Collider_t1773347010 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___other0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult PlayMakerProxyBase/TriggerEvent::BeginInvoke(UnityEngine.Collider,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TriggerEvent_BeginInvoke_m3075195832 (TriggerEvent_t1933226311 * __this, Collider_t1773347010 * ___other0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___other0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void PlayMakerProxyBase/TriggerEvent::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TriggerEvent_EndInvoke_m1250808080 (TriggerEvent_t1933226311 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerEnter_OnTriggerEnter_m1486868521 (PlayMakerTriggerEnter_t2165260292 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerTriggerEnter_OnTriggerEnter_m1486868521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleTriggerEnter_m2907444739(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collider_t1773347010 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnTriggerEnter_m979455358(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collider_t1773347010 * L_19 = ___other0;
		PlayMakerProxyBase_DoTriggerEventCallback_m3149201495(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerEnter::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerEnter__ctor_m1598168990 (PlayMakerTriggerEnter_t2165260292 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerTriggerEnter2D::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerEnter2D_OnTriggerEnter2D_m1049597878 (PlayMakerTriggerEnter2D_t1611673000 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerTriggerEnter2D_OnTriggerEnter2D_m1049597878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleTriggerEnter2D_m2359995862(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collider2D_t2806799626 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnTriggerEnter2D_m672290962(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collider2D_t2806799626 * L_19 = ___other0;
		PlayMakerProxyBase_DoTrigger2DEventCallback_m3967730350(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerEnter2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerEnter2D__ctor_m1321873113 (PlayMakerTriggerEnter2D_t1611673000 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerTriggerExit::OnTriggerExit(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerExit_OnTriggerExit_m602018866 (PlayMakerTriggerExit_t2127564928 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerTriggerExit_OnTriggerExit_m602018866_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleTriggerExit_m260601769(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collider_t1773347010 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnTriggerExit_m2908555696(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collider_t1773347010 * L_19 = ___other0;
		PlayMakerProxyBase_DoTriggerEventCallback_m3149201495(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerExit::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerExit__ctor_m3399216023 (PlayMakerTriggerExit_t2127564928 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerTriggerExit2D::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerExit2D_OnTriggerExit2D_m3700787535 (PlayMakerTriggerExit2D_t2899497211 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerTriggerExit2D_OnTriggerExit2D_m3700787535_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleTriggerExit2D_m122397604(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collider2D_t2806799626 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnTriggerExit2D_m923717752(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collider2D_t2806799626 * L_19 = ___other0;
		PlayMakerProxyBase_DoTrigger2DEventCallback_m3967730350(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerExit2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerExit2D__ctor_m156349211 (PlayMakerTriggerExit2D_t2899497211 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerTriggerStay::OnTriggerStay(UnityEngine.Collider)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerStay_OnTriggerStay_m3827530242 (PlayMakerTriggerStay_t1432841146 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerTriggerStay_OnTriggerStay_m3827530242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleTriggerStay_m1648714122(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collider_t1773347010 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnTriggerStay_m2187419734(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collider_t1773347010 * L_19 = ___other0;
		PlayMakerProxyBase_DoTriggerEventCallback_m3149201495(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerStay::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerStay__ctor_m2339140259 (PlayMakerTriggerStay_t1432841146 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerTriggerStay2D::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerStay2D_OnTriggerStay2D_m4238132012 (PlayMakerTriggerStay2D_t2180132409 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerTriggerStay2D_OnTriggerStay2D_m4238132012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t1613010231 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0004:
	{
		List_1_t3085084973 * L_0 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PlayMakerFSM_t1613010231 * L_2 = List_1_get_Item_m587783994(L_0, L_1, /*hidden argument*/List_1_get_Item_m587783994_RuntimeMethod_var);
		V_1 = L_2;
		PlayMakerFSM_t1613010231 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_5 = V_1;
		NullCheck(L_5);
		Fsm_t4127147824 * L_6 = PlayMakerFSM_get_Fsm_m2313055607(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = PlayMakerFSM_get_Active_m232505091(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t4127147824 * L_10 = PlayMakerFSM_get_Fsm_m2313055607(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleTriggerStay2D_m2215176015(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0043;
		}
	}
	{
		PlayMakerFSM_t1613010231 * L_12 = V_1;
		NullCheck(L_12);
		Fsm_t4127147824 * L_13 = PlayMakerFSM_get_Fsm_m2313055607(L_12, /*hidden argument*/NULL);
		Collider2D_t2806799626 * L_14 = ___other0;
		NullCheck(L_13);
		Fsm_OnTriggerStay2D_m3259809030(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_0;
		List_1_t3085084973 * L_17 = ((PlayMakerProxyBase_t90512809 *)__this)->get_TargetFSMs_4();
		NullCheck(L_17);
		int32_t L_18 = List_1_get_Count_m2358759014(L_17, /*hidden argument*/List_1_get_Count_m2358759014_RuntimeMethod_var);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_0004;
		}
	}
	{
		Collider2D_t2806799626 * L_19 = ___other0;
		PlayMakerProxyBase_DoTrigger2DEventCallback_m3967730350(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerStay2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayMakerTriggerStay2D__ctor_m153769323 (PlayMakerTriggerStay2D_t2180132409 * __this, const RuntimeMethod* method)
{
	{
		PlayMakerProxyBase__ctor_m2965400855(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
