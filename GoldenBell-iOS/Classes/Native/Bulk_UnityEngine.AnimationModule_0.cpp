﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t1578797820;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.InvalidCastException
struct InvalidCastException_t3927145244;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.Animation
struct Animation_t3648466861;
// UnityEngine.Animation/Enumerator
struct Enumerator_t1136361084;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.AnimationEvent
struct AnimationEvent_t1536042487;
// UnityEngine.AnimationState
struct AnimationState_t1108360407;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AnimatorOverrideController
struct AnimatorOverrideController_t3582062219;
// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct OnOverrideControllerDirtyCallback_t1307045488;
// UnityEngine.AvatarMask
struct AvatarMask_t1182482518;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Motion
struct Motion_t1110556653;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Playables.PlayableBinding/CreateOutputMethod
struct CreateOutputMethod_t2301811773;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t829358056;
// UnityEngine.TrackedReference
struct TrackedReference_t1199777556;
// UnityEngine.Transform
struct Transform_t3600365921;

extern RuntimeClass* AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimationPlayableBinding_t1369139502_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimationScriptPlayable_t1303525964_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimatorControllerPlayable_t1015767841_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern RuntimeClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern RuntimeClass* CreateOutputMethod_t2301811773_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Enumerator_t1136361084_il2cpp_TypeInfo_var;
extern RuntimeClass* HumanBodyBones_t600150811_il2cpp_TypeInfo_var;
extern RuntimeClass* IndexOutOfRangeException_t1578797820_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* InvalidCastException_t3927145244_il2cpp_TypeInfo_var;
extern RuntimeClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayableBinding_t354260709_il2cpp_TypeInfo_var;
extern RuntimeClass* PlayableHandle_t1095853803_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32_t2560061978_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1913092913;
extern String_t* _stringLiteral2566125115;
extern String_t* _stringLiteral2761492165;
extern String_t* _stringLiteral3049507974;
extern String_t* _stringLiteral3192108539;
extern String_t* _stringLiteral3380224547;
extern String_t* _stringLiteral3596080318;
extern String_t* _stringLiteral3707822210;
extern String_t* _stringLiteral3786669396;
extern String_t* _stringLiteral4251651911;
extern String_t* _stringLiteral455380590;
extern String_t* _stringLiteral757602046;
extern String_t* _stringLiteral783105886;
extern String_t* _stringLiteral78940560;
extern String_t* _stringLiteral795769502;
extern const RuntimeMethod* AnimationClipPlayable__ctor_m2913235724_RuntimeMethod_var;
extern const RuntimeMethod* AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514_RuntimeMethod_var;
extern const RuntimeMethod* AnimationLayerMixerPlayable__ctor_m860721801_RuntimeMethod_var;
extern const RuntimeMethod* AnimationMixerPlayable__ctor_m848068859_RuntimeMethod_var;
extern const RuntimeMethod* AnimationMotionXToDeltaPlayable__ctor_m1725027713_RuntimeMethod_var;
extern const RuntimeMethod* AnimationOffsetPlayable__ctor_m577157274_RuntimeMethod_var;
extern const RuntimeMethod* AnimationPlayableBinding_CreateAnimationOutput_m719933912_RuntimeMethod_var;
extern const RuntimeMethod* AnimationPlayableOutput__ctor_m457905557_RuntimeMethod_var;
extern const RuntimeMethod* AnimationScriptPlayable__ctor_m1047518474_RuntimeMethod_var;
extern const RuntimeMethod* AnimatorControllerPlayable_SetHandle_m2260644454_RuntimeMethod_var;
extern const RuntimeMethod* Animator_GetBoneTransform_m929462223_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t3189118652_m4201087276_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t3631223897_m201603007_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t821371386_m4108826000_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationMotionXToDeltaPlayable_t272231551_m1123153091_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t2887420414_m2033286094_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimationScriptPlayable_t1303525964_m1992139667_RuntimeMethod_var;
extern const RuntimeMethod* PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t1015767841_m3416945299_RuntimeMethod_var;
extern const RuntimeMethod* PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t1918618239_m3383247624_RuntimeMethod_var;
extern const RuntimeType* Animator_t434523843_0_0_0_var;
extern const uint32_t AnimationClipPlayable__ctor_m2913235724_MetadataUsageId;
extern const uint32_t AnimationEvent__ctor_m234009954_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_CreateHandleInternal_m2872369648_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_CreateHandle_m2143407042_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_Create_m286498210_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable__cctor_m4086299972_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable__ctor_m860721801_MetadataUsageId;
extern const uint32_t AnimationLayerMixerPlayable_get_Null_m1596956097_MetadataUsageId;
extern const uint32_t AnimationMixerPlayable__ctor_m848068859_MetadataUsageId;
extern const uint32_t AnimationMotionXToDeltaPlayable__ctor_m1725027713_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_CreateHandleInternal_m904339466_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_CreateHandle_m2427170463_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_Create_m4216231881_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_Equals_m2902253045_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetPositionInternal_m3429692954_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetPosition_m3423307712_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetRotationInternal_m1801279867_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable_GetRotation_m259451460_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable__cctor_m1029460816_MetadataUsageId;
extern const uint32_t AnimationOffsetPlayable__ctor_m577157274_MetadataUsageId;
extern const uint32_t AnimationPlayableBinding_Create_m3080379501_MetadataUsageId;
extern const uint32_t AnimationPlayableOutput__ctor_m457905557_MetadataUsageId;
extern const uint32_t AnimationScriptPlayable__cctor_m1482222037_MetadataUsageId;
extern const uint32_t AnimationScriptPlayable__ctor_m1047518474_MetadataUsageId;
extern const uint32_t Animation_GetEnumerator_m1233868201_MetadataUsageId;
extern const uint32_t AnimatorControllerPlayable_SetHandle_m2260644454_MetadataUsageId;
extern const uint32_t AnimatorControllerPlayable__cctor_m3439442711_MetadataUsageId;
extern const uint32_t Animator_CheckIfInIKPass_m1701567706_MetadataUsageId;
extern const uint32_t Animator_GetBoneTransform_m929462223_MetadataUsageId;
extern const uint32_t Motion__ctor_m1337281595_MetadataUsageId;
struct Object_t631007953;;
struct Object_t631007953_marshaled_com;
struct Object_t631007953_marshaled_com;;
struct Object_t631007953_marshaled_pinvoke;
struct Object_t631007953_marshaled_pinvoke;;



#ifndef U3CMODULEU3E_T692745537_H
#define U3CMODULEU3E_T692745537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745537 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745537_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUMERATOR_T1136361084_H
#define ENUMERATOR_T1136361084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation/Enumerator
struct  Enumerator_t1136361084  : public RuntimeObject
{
public:
	// UnityEngine.Animation UnityEngine.Animation/Enumerator::m_Outer
	Animation_t3648466861 * ___m_Outer_0;
	// System.Int32 UnityEngine.Animation/Enumerator::m_CurrentIndex
	int32_t ___m_CurrentIndex_1;

public:
	inline static int32_t get_offset_of_m_Outer_0() { return static_cast<int32_t>(offsetof(Enumerator_t1136361084, ___m_Outer_0)); }
	inline Animation_t3648466861 * get_m_Outer_0() const { return ___m_Outer_0; }
	inline Animation_t3648466861 ** get_address_of_m_Outer_0() { return &___m_Outer_0; }
	inline void set_m_Outer_0(Animation_t3648466861 * value)
	{
		___m_Outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Outer_0), value);
	}

	inline static int32_t get_offset_of_m_CurrentIndex_1() { return static_cast<int32_t>(offsetof(Enumerator_t1136361084, ___m_CurrentIndex_1)); }
	inline int32_t get_m_CurrentIndex_1() const { return ___m_CurrentIndex_1; }
	inline int32_t* get_address_of_m_CurrentIndex_1() { return &___m_CurrentIndex_1; }
	inline void set_m_CurrentIndex_1(int32_t value)
	{
		___m_CurrentIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1136361084_H
#ifndef ANIMATIONPLAYABLEBINDING_T1369139502_H
#define ANIMATIONPLAYABLEBINDING_T1369139502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableBinding
struct  AnimationPlayableBinding_t1369139502  : public RuntimeObject
{
public:

public:
};

struct AnimationPlayableBinding_t1369139502_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding/CreateOutputMethod UnityEngine.Animations.AnimationPlayableBinding::<>f__mg$cache0
	CreateOutputMethod_t2301811773 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(AnimationPlayableBinding_t1369139502_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline CreateOutputMethod_t2301811773 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline CreateOutputMethod_t2301811773 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(CreateOutputMethod_t2301811773 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEBINDING_T1369139502_H
#ifndef ANIMATIONPLAYABLEEXTENSIONS_T514468210_H
#define ANIMATIONPLAYABLEEXTENSIONS_T514468210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableExtensions
struct  AnimationPlayableExtensions_t514468210  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEEXTENSIONS_T514468210_H
#ifndef ANIMATIONPLAYABLEGRAPHEXTENSIONS_T3835522072_H
#define ANIMATIONPLAYABLEGRAPHEXTENSIONS_T3835522072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableGraphExtensions
struct  AnimationPlayableGraphExtensions_t3835522072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEGRAPHEXTENSIONS_T3835522072_H
#ifndef HUMANTRAIT_T2649476137_H
#define HUMANTRAIT_T2649476137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HumanTrait
struct  HumanTrait_t2649476137  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUMANTRAIT_T2649476137_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ANIMATORCLIPINFO_T3156717155_H
#define ANIMATORCLIPINFO_T3156717155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorClipInfo
struct  AnimatorClipInfo_t3156717155 
{
public:
	// System.Int32 UnityEngine.AnimatorClipInfo::m_ClipInstanceID
	int32_t ___m_ClipInstanceID_0;
	// System.Single UnityEngine.AnimatorClipInfo::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_ClipInstanceID_0() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t3156717155, ___m_ClipInstanceID_0)); }
	inline int32_t get_m_ClipInstanceID_0() const { return ___m_ClipInstanceID_0; }
	inline int32_t* get_address_of_m_ClipInstanceID_0() { return &___m_ClipInstanceID_0; }
	inline void set_m_ClipInstanceID_0(int32_t value)
	{
		___m_ClipInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(AnimatorClipInfo_t3156717155, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCLIPINFO_T3156717155_H
#ifndef ANIMATORSTATEINFO_T509032636_H
#define ANIMATORSTATEINFO_T509032636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t509032636 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T509032636_H
#ifndef ANIMATORTRANSITIONINFO_T2534804151_H
#define ANIMATORTRANSITIONINFO_T2534804151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorTransitionInfo
struct  AnimatorTransitionInfo_t2534804151 
{
public:
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_FullPath
	int32_t ___m_FullPath_0;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_UserName
	int32_t ___m_UserName_1;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_Name
	int32_t ___m_Name_2;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_HasFixedDuration
	bool ___m_HasFixedDuration_3;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_Duration
	float ___m_Duration_4;
	// System.Single UnityEngine.AnimatorTransitionInfo::m_NormalizedTime
	float ___m_NormalizedTime_5;
	// System.Boolean UnityEngine.AnimatorTransitionInfo::m_AnyState
	bool ___m_AnyState_6;
	// System.Int32 UnityEngine.AnimatorTransitionInfo::m_TransitionType
	int32_t ___m_TransitionType_7;

public:
	inline static int32_t get_offset_of_m_FullPath_0() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_FullPath_0)); }
	inline int32_t get_m_FullPath_0() const { return ___m_FullPath_0; }
	inline int32_t* get_address_of_m_FullPath_0() { return &___m_FullPath_0; }
	inline void set_m_FullPath_0(int32_t value)
	{
		___m_FullPath_0 = value;
	}

	inline static int32_t get_offset_of_m_UserName_1() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_UserName_1)); }
	inline int32_t get_m_UserName_1() const { return ___m_UserName_1; }
	inline int32_t* get_address_of_m_UserName_1() { return &___m_UserName_1; }
	inline void set_m_UserName_1(int32_t value)
	{
		___m_UserName_1 = value;
	}

	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_Name_2)); }
	inline int32_t get_m_Name_2() const { return ___m_Name_2; }
	inline int32_t* get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(int32_t value)
	{
		___m_Name_2 = value;
	}

	inline static int32_t get_offset_of_m_HasFixedDuration_3() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_HasFixedDuration_3)); }
	inline bool get_m_HasFixedDuration_3() const { return ___m_HasFixedDuration_3; }
	inline bool* get_address_of_m_HasFixedDuration_3() { return &___m_HasFixedDuration_3; }
	inline void set_m_HasFixedDuration_3(bool value)
	{
		___m_HasFixedDuration_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_5() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_NormalizedTime_5)); }
	inline float get_m_NormalizedTime_5() const { return ___m_NormalizedTime_5; }
	inline float* get_address_of_m_NormalizedTime_5() { return &___m_NormalizedTime_5; }
	inline void set_m_NormalizedTime_5(float value)
	{
		___m_NormalizedTime_5 = value;
	}

	inline static int32_t get_offset_of_m_AnyState_6() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_AnyState_6)); }
	inline bool get_m_AnyState_6() const { return ___m_AnyState_6; }
	inline bool* get_address_of_m_AnyState_6() { return &___m_AnyState_6; }
	inline void set_m_AnyState_6(bool value)
	{
		___m_AnyState_6 = value;
	}

	inline static int32_t get_offset_of_m_TransitionType_7() { return static_cast<int32_t>(offsetof(AnimatorTransitionInfo_t2534804151, ___m_TransitionType_7)); }
	inline int32_t get_m_TransitionType_7() const { return ___m_TransitionType_7; }
	inline int32_t* get_address_of_m_TransitionType_7() { return &___m_TransitionType_7; }
	inline void set_m_TransitionType_7(int32_t value)
	{
		___m_TransitionType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t2534804151_marshaled_pinvoke
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};
// Native definition for COM marshalling of UnityEngine.AnimatorTransitionInfo
struct AnimatorTransitionInfo_t2534804151_marshaled_com
{
	int32_t ___m_FullPath_0;
	int32_t ___m_UserName_1;
	int32_t ___m_Name_2;
	int32_t ___m_HasFixedDuration_3;
	float ___m_Duration_4;
	float ___m_NormalizedTime_5;
	int32_t ___m_AnyState_6;
	int32_t ___m_TransitionType_7;
};
#endif // ANIMATORTRANSITIONINFO_T2534804151_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef SHAREDBETWEENANIMATORSATTRIBUTE_T2857104338_H
#define SHAREDBETWEENANIMATORSATTRIBUTE_T2857104338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SharedBetweenAnimatorsAttribute
struct  SharedBetweenAnimatorsAttribute_t2857104338  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDBETWEENANIMATORSATTRIBUTE_T2857104338_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ARGUMENTEXCEPTION_T132251570_H
#define ARGUMENTEXCEPTION_T132251570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_t132251570  : public SystemException_t176217640
{
public:
	// System.String System.ArgumentException::param_name
	String_t* ___param_name_12;

public:
	inline static int32_t get_offset_of_param_name_12() { return static_cast<int32_t>(offsetof(ArgumentException_t132251570, ___param_name_12)); }
	inline String_t* get_param_name_12() const { return ___param_name_12; }
	inline String_t** get_address_of_param_name_12() { return &___param_name_12; }
	inline void set_param_name_12(String_t* value)
	{
		___param_name_12 = value;
		Il2CppCodeGenWriteBarrier((&___param_name_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_T132251570_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#define INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t1578797820  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T1578797820_H
#ifndef INVALIDCASTEXCEPTION_T3927145244_H
#define INVALIDCASTEXCEPTION_T3927145244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t3927145244  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T3927145244_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef ANIMATIONBLENDMODE_T656007753_H
#define ANIMATIONBLENDMODE_T656007753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationBlendMode
struct  AnimationBlendMode_t656007753 
{
public:
	// System.Int32 UnityEngine.AnimationBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationBlendMode_t656007753, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONBLENDMODE_T656007753_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef ANIMATIONEVENTSOURCE_T3045280095_H
#define ANIMATIONEVENTSOURCE_T3045280095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationEventSource
struct  AnimationEventSource_t3045280095 
{
public:
	// System.Int32 UnityEngine.AnimationEventSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimationEventSource_t3045280095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONEVENTSOURCE_T3045280095_H
#ifndef ANIMATORCULLINGMODE_T1291874113_H
#define ANIMATORCULLINGMODE_T1291874113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorCullingMode
struct  AnimatorCullingMode_t1291874113 
{
public:
	// System.Int32 UnityEngine.AnimatorCullingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnimatorCullingMode_t1291874113, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCULLINGMODE_T1291874113_H
#ifndef AVATARIKGOAL_T2138471376_H
#define AVATARIKGOAL_T2138471376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarIKGoal
struct  AvatarIKGoal_t2138471376 
{
public:
	// System.Int32 UnityEngine.AvatarIKGoal::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AvatarIKGoal_t2138471376, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARIKGOAL_T2138471376_H
#ifndef AVATARTARGET_T2782276764_H
#define AVATARTARGET_T2782276764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarTarget
struct  AvatarTarget_t2782276764 
{
public:
	// System.Int32 UnityEngine.AvatarTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AvatarTarget_t2782276764, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARTARGET_T2782276764_H
#ifndef ANIMATIONHUMANSTREAM_T4010990717_H
#define ANIMATIONHUMANSTREAM_T4010990717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Animations.AnimationHumanStream
struct  AnimationHumanStream_t4010990717 
{
public:
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationHumanStream::stream
	intptr_t ___stream_0;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(AnimationHumanStream_t4010990717, ___stream_0)); }
	inline intptr_t get_stream_0() const { return ___stream_0; }
	inline intptr_t* get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(intptr_t value)
	{
		___stream_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONHUMANSTREAM_T4010990717_H
#ifndef ANIMATIONSTREAM_T3771784418_H
#define ANIMATIONSTREAM_T3771784418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Animations.AnimationStream
struct  AnimationStream_t3771784418 
{
public:
	// System.UInt32 UnityEngine.Experimental.Animations.AnimationStream::m_AnimatorBindingsVersion
	uint32_t ___m_AnimatorBindingsVersion_0;
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationStream::constant
	intptr_t ___constant_1;
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationStream::input
	intptr_t ___input_2;
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationStream::output
	intptr_t ___output_3;
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationStream::workspace
	intptr_t ___workspace_4;
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationStream::inputStreamAccessor
	intptr_t ___inputStreamAccessor_5;
	// System.IntPtr UnityEngine.Experimental.Animations.AnimationStream::animationHandleBinder
	intptr_t ___animationHandleBinder_6;

public:
	inline static int32_t get_offset_of_m_AnimatorBindingsVersion_0() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___m_AnimatorBindingsVersion_0)); }
	inline uint32_t get_m_AnimatorBindingsVersion_0() const { return ___m_AnimatorBindingsVersion_0; }
	inline uint32_t* get_address_of_m_AnimatorBindingsVersion_0() { return &___m_AnimatorBindingsVersion_0; }
	inline void set_m_AnimatorBindingsVersion_0(uint32_t value)
	{
		___m_AnimatorBindingsVersion_0 = value;
	}

	inline static int32_t get_offset_of_constant_1() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___constant_1)); }
	inline intptr_t get_constant_1() const { return ___constant_1; }
	inline intptr_t* get_address_of_constant_1() { return &___constant_1; }
	inline void set_constant_1(intptr_t value)
	{
		___constant_1 = value;
	}

	inline static int32_t get_offset_of_input_2() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___input_2)); }
	inline intptr_t get_input_2() const { return ___input_2; }
	inline intptr_t* get_address_of_input_2() { return &___input_2; }
	inline void set_input_2(intptr_t value)
	{
		___input_2 = value;
	}

	inline static int32_t get_offset_of_output_3() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___output_3)); }
	inline intptr_t get_output_3() const { return ___output_3; }
	inline intptr_t* get_address_of_output_3() { return &___output_3; }
	inline void set_output_3(intptr_t value)
	{
		___output_3 = value;
	}

	inline static int32_t get_offset_of_workspace_4() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___workspace_4)); }
	inline intptr_t get_workspace_4() const { return ___workspace_4; }
	inline intptr_t* get_address_of_workspace_4() { return &___workspace_4; }
	inline void set_workspace_4(intptr_t value)
	{
		___workspace_4 = value;
	}

	inline static int32_t get_offset_of_inputStreamAccessor_5() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___inputStreamAccessor_5)); }
	inline intptr_t get_inputStreamAccessor_5() const { return ___inputStreamAccessor_5; }
	inline intptr_t* get_address_of_inputStreamAccessor_5() { return &___inputStreamAccessor_5; }
	inline void set_inputStreamAccessor_5(intptr_t value)
	{
		___inputStreamAccessor_5 = value;
	}

	inline static int32_t get_offset_of_animationHandleBinder_6() { return static_cast<int32_t>(offsetof(AnimationStream_t3771784418, ___animationHandleBinder_6)); }
	inline intptr_t get_animationHandleBinder_6() const { return ___animationHandleBinder_6; }
	inline intptr_t* get_address_of_animationHandleBinder_6() { return &___animationHandleBinder_6; }
	inline void set_animationHandleBinder_6(intptr_t value)
	{
		___animationHandleBinder_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTREAM_T3771784418_H
#ifndef HUMANBODYBONES_T600150811_H
#define HUMANBODYBONES_T600150811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HumanBodyBones
struct  HumanBodyBones_t600150811 
{
public:
	// System.Int32 UnityEngine.HumanBodyBones::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HumanBodyBones_t600150811, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUMANBODYBONES_T600150811_H
#ifndef HUMANLIMIT_T2901552972_H
#define HUMANLIMIT_T2901552972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HumanLimit
struct  HumanLimit_t2901552972 
{
public:
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Min
	Vector3_t3722313464  ___m_Min_0;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Max
	Vector3_t3722313464  ___m_Max_1;
	// UnityEngine.Vector3 UnityEngine.HumanLimit::m_Center
	Vector3_t3722313464  ___m_Center_2;
	// System.Single UnityEngine.HumanLimit::m_AxisLength
	float ___m_AxisLength_3;
	// System.Int32 UnityEngine.HumanLimit::m_UseDefaultValues
	int32_t ___m_UseDefaultValues_4;

public:
	inline static int32_t get_offset_of_m_Min_0() { return static_cast<int32_t>(offsetof(HumanLimit_t2901552972, ___m_Min_0)); }
	inline Vector3_t3722313464  get_m_Min_0() const { return ___m_Min_0; }
	inline Vector3_t3722313464 * get_address_of_m_Min_0() { return &___m_Min_0; }
	inline void set_m_Min_0(Vector3_t3722313464  value)
	{
		___m_Min_0 = value;
	}

	inline static int32_t get_offset_of_m_Max_1() { return static_cast<int32_t>(offsetof(HumanLimit_t2901552972, ___m_Max_1)); }
	inline Vector3_t3722313464  get_m_Max_1() const { return ___m_Max_1; }
	inline Vector3_t3722313464 * get_address_of_m_Max_1() { return &___m_Max_1; }
	inline void set_m_Max_1(Vector3_t3722313464  value)
	{
		___m_Max_1 = value;
	}

	inline static int32_t get_offset_of_m_Center_2() { return static_cast<int32_t>(offsetof(HumanLimit_t2901552972, ___m_Center_2)); }
	inline Vector3_t3722313464  get_m_Center_2() const { return ___m_Center_2; }
	inline Vector3_t3722313464 * get_address_of_m_Center_2() { return &___m_Center_2; }
	inline void set_m_Center_2(Vector3_t3722313464  value)
	{
		___m_Center_2 = value;
	}

	inline static int32_t get_offset_of_m_AxisLength_3() { return static_cast<int32_t>(offsetof(HumanLimit_t2901552972, ___m_AxisLength_3)); }
	inline float get_m_AxisLength_3() const { return ___m_AxisLength_3; }
	inline float* get_address_of_m_AxisLength_3() { return &___m_AxisLength_3; }
	inline void set_m_AxisLength_3(float value)
	{
		___m_AxisLength_3 = value;
	}

	inline static int32_t get_offset_of_m_UseDefaultValues_4() { return static_cast<int32_t>(offsetof(HumanLimit_t2901552972, ___m_UseDefaultValues_4)); }
	inline int32_t get_m_UseDefaultValues_4() const { return ___m_UseDefaultValues_4; }
	inline int32_t* get_address_of_m_UseDefaultValues_4() { return &___m_UseDefaultValues_4; }
	inline void set_m_UseDefaultValues_4(int32_t value)
	{
		___m_UseDefaultValues_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUMANLIMIT_T2901552972_H
#ifndef MATCHTARGETWEIGHTMASK_T554846203_H
#define MATCHTARGETWEIGHTMASK_T554846203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MatchTargetWeightMask
struct  MatchTargetWeightMask_t554846203 
{
public:
	// UnityEngine.Vector3 UnityEngine.MatchTargetWeightMask::m_PositionXYZWeight
	Vector3_t3722313464  ___m_PositionXYZWeight_0;
	// System.Single UnityEngine.MatchTargetWeightMask::m_RotationWeight
	float ___m_RotationWeight_1;

public:
	inline static int32_t get_offset_of_m_PositionXYZWeight_0() { return static_cast<int32_t>(offsetof(MatchTargetWeightMask_t554846203, ___m_PositionXYZWeight_0)); }
	inline Vector3_t3722313464  get_m_PositionXYZWeight_0() const { return ___m_PositionXYZWeight_0; }
	inline Vector3_t3722313464 * get_address_of_m_PositionXYZWeight_0() { return &___m_PositionXYZWeight_0; }
	inline void set_m_PositionXYZWeight_0(Vector3_t3722313464  value)
	{
		___m_PositionXYZWeight_0 = value;
	}

	inline static int32_t get_offset_of_m_RotationWeight_1() { return static_cast<int32_t>(offsetof(MatchTargetWeightMask_t554846203, ___m_RotationWeight_1)); }
	inline float get_m_RotationWeight_1() const { return ___m_RotationWeight_1; }
	inline float* get_address_of_m_RotationWeight_1() { return &___m_RotationWeight_1; }
	inline void set_m_RotationWeight_1(float value)
	{
		___m_RotationWeight_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETWEIGHTMASK_T554846203_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PLAYMODE_T3051407859_H
#define PLAYMODE_T3051407859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PlayMode
struct  PlayMode_t3051407859 
{
public:
	// System.Int32 UnityEngine.PlayMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlayMode_t3051407859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODE_T3051407859_H
#ifndef PLAYABLEGRAPH_T3515989261_H
#define PLAYABLEGRAPH_T3515989261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t3515989261 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableGraph::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t3515989261, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t3515989261, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEGRAPH_T3515989261_H
#ifndef PLAYABLEHANDLE_T1095853803_H
#define PLAYABLEHANDLE_T1095853803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1095853803 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1095853803_H
#ifndef PLAYABLEOUTPUTHANDLE_T4208053793_H
#define PLAYABLEOUTPUTHANDLE_T4208053793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t4208053793 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t4208053793, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t4208053793, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T4208053793_H
#ifndef SKELETONBONE_T4134054672_H
#define SKELETONBONE_T4134054672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SkeletonBone
struct  SkeletonBone_t4134054672 
{
public:
	// System.String UnityEngine.SkeletonBone::name
	String_t* ___name_0;
	// System.String UnityEngine.SkeletonBone::parentName
	String_t* ___parentName_1;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::position
	Vector3_t3722313464  ___position_2;
	// UnityEngine.Quaternion UnityEngine.SkeletonBone::rotation
	Quaternion_t2301928331  ___rotation_3;
	// UnityEngine.Vector3 UnityEngine.SkeletonBone::scale
	Vector3_t3722313464  ___scale_4;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonBone_t4134054672, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_parentName_1() { return static_cast<int32_t>(offsetof(SkeletonBone_t4134054672, ___parentName_1)); }
	inline String_t* get_parentName_1() const { return ___parentName_1; }
	inline String_t** get_address_of_parentName_1() { return &___parentName_1; }
	inline void set_parentName_1(String_t* value)
	{
		___parentName_1 = value;
		Il2CppCodeGenWriteBarrier((&___parentName_1), value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(SkeletonBone_t4134054672, ___position_2)); }
	inline Vector3_t3722313464  get_position_2() const { return ___position_2; }
	inline Vector3_t3722313464 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_t3722313464  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(SkeletonBone_t4134054672, ___rotation_3)); }
	inline Quaternion_t2301928331  get_rotation_3() const { return ___rotation_3; }
	inline Quaternion_t2301928331 * get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(Quaternion_t2301928331  value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(SkeletonBone_t4134054672, ___scale_4)); }
	inline Vector3_t3722313464  get_scale_4() const { return ___scale_4; }
	inline Vector3_t3722313464 * get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(Vector3_t3722313464  value)
	{
		___scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_t4134054672_marshaled_pinvoke
{
	char* ___name_0;
	char* ___parentName_1;
	Vector3_t3722313464  ___position_2;
	Quaternion_t2301928331  ___rotation_3;
	Vector3_t3722313464  ___scale_4;
};
// Native definition for COM marshalling of UnityEngine.SkeletonBone
struct SkeletonBone_t4134054672_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___parentName_1;
	Vector3_t3722313464  ___position_2;
	Quaternion_t2301928331  ___rotation_3;
	Vector3_t3722313464  ___scale_4;
};
#endif // SKELETONBONE_T4134054672_H
#ifndef STATEINFOINDEX_T2694690729_H
#define STATEINFOINDEX_T2694690729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateInfoIndex
struct  StateInfoIndex_t2694690729 
{
public:
	// System.Int32 UnityEngine.StateInfoIndex::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StateInfoIndex_t2694690729, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEINFOINDEX_T2694690729_H
#ifndef TRACKEDREFERENCE_T1199777556_H
#define TRACKEDREFERENCE_T1199777556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t1199777556  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t1199777556, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t1199777556_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t1199777556_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T1199777556_H
#ifndef WRAPMODE_T730450702_H
#define WRAPMODE_T730450702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t730450702 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WrapMode_t730450702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T730450702_H
#ifndef ARGUMENTNULLEXCEPTION_T1615371798_H
#define ARGUMENTNULLEXCEPTION_T1615371798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentNullException
struct  ArgumentNullException_t1615371798  : public ArgumentException_t132251570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTNULLEXCEPTION_T1615371798_H
#ifndef ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#define ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t777629997  : public ArgumentException_t132251570
{
public:
	// System.Object System.ArgumentOutOfRangeException::actual_value
	RuntimeObject * ___actual_value_13;

public:
	inline static int32_t get_offset_of_actual_value_13() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t777629997, ___actual_value_13)); }
	inline RuntimeObject * get_actual_value_13() const { return ___actual_value_13; }
	inline RuntimeObject ** get_address_of_actual_value_13() { return &___actual_value_13; }
	inline void set_actual_value_13(RuntimeObject * value)
	{
		___actual_value_13 = value;
		Il2CppCodeGenWriteBarrier((&___actual_value_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTOUTOFRANGEEXCEPTION_T777629997_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef ANIMATIONEVENT_T1536042487_H
#define ANIMATIONEVENT_T1536042487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationEvent
struct  AnimationEvent_t1536042487  : public RuntimeObject
{
public:
	// System.Single UnityEngine.AnimationEvent::m_Time
	float ___m_Time_0;
	// System.String UnityEngine.AnimationEvent::m_FunctionName
	String_t* ___m_FunctionName_1;
	// System.String UnityEngine.AnimationEvent::m_StringParameter
	String_t* ___m_StringParameter_2;
	// UnityEngine.Object UnityEngine.AnimationEvent::m_ObjectReferenceParameter
	Object_t631007953 * ___m_ObjectReferenceParameter_3;
	// System.Single UnityEngine.AnimationEvent::m_FloatParameter
	float ___m_FloatParameter_4;
	// System.Int32 UnityEngine.AnimationEvent::m_IntParameter
	int32_t ___m_IntParameter_5;
	// System.Int32 UnityEngine.AnimationEvent::m_MessageOptions
	int32_t ___m_MessageOptions_6;
	// UnityEngine.AnimationEventSource UnityEngine.AnimationEvent::m_Source
	int32_t ___m_Source_7;
	// UnityEngine.AnimationState UnityEngine.AnimationEvent::m_StateSender
	AnimationState_t1108360407 * ___m_StateSender_8;
	// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::m_AnimatorStateInfo
	AnimatorStateInfo_t509032636  ___m_AnimatorStateInfo_9;
	// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::m_AnimatorClipInfo
	AnimatorClipInfo_t3156717155  ___m_AnimatorClipInfo_10;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_FunctionName_1() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_FunctionName_1)); }
	inline String_t* get_m_FunctionName_1() const { return ___m_FunctionName_1; }
	inline String_t** get_address_of_m_FunctionName_1() { return &___m_FunctionName_1; }
	inline void set_m_FunctionName_1(String_t* value)
	{
		___m_FunctionName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FunctionName_1), value);
	}

	inline static int32_t get_offset_of_m_StringParameter_2() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_StringParameter_2)); }
	inline String_t* get_m_StringParameter_2() const { return ___m_StringParameter_2; }
	inline String_t** get_address_of_m_StringParameter_2() { return &___m_StringParameter_2; }
	inline void set_m_StringParameter_2(String_t* value)
	{
		___m_StringParameter_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_StringParameter_2), value);
	}

	inline static int32_t get_offset_of_m_ObjectReferenceParameter_3() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_ObjectReferenceParameter_3)); }
	inline Object_t631007953 * get_m_ObjectReferenceParameter_3() const { return ___m_ObjectReferenceParameter_3; }
	inline Object_t631007953 ** get_address_of_m_ObjectReferenceParameter_3() { return &___m_ObjectReferenceParameter_3; }
	inline void set_m_ObjectReferenceParameter_3(Object_t631007953 * value)
	{
		___m_ObjectReferenceParameter_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObjectReferenceParameter_3), value);
	}

	inline static int32_t get_offset_of_m_FloatParameter_4() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_FloatParameter_4)); }
	inline float get_m_FloatParameter_4() const { return ___m_FloatParameter_4; }
	inline float* get_address_of_m_FloatParameter_4() { return &___m_FloatParameter_4; }
	inline void set_m_FloatParameter_4(float value)
	{
		___m_FloatParameter_4 = value;
	}

	inline static int32_t get_offset_of_m_IntParameter_5() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_IntParameter_5)); }
	inline int32_t get_m_IntParameter_5() const { return ___m_IntParameter_5; }
	inline int32_t* get_address_of_m_IntParameter_5() { return &___m_IntParameter_5; }
	inline void set_m_IntParameter_5(int32_t value)
	{
		___m_IntParameter_5 = value;
	}

	inline static int32_t get_offset_of_m_MessageOptions_6() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_MessageOptions_6)); }
	inline int32_t get_m_MessageOptions_6() const { return ___m_MessageOptions_6; }
	inline int32_t* get_address_of_m_MessageOptions_6() { return &___m_MessageOptions_6; }
	inline void set_m_MessageOptions_6(int32_t value)
	{
		___m_MessageOptions_6 = value;
	}

	inline static int32_t get_offset_of_m_Source_7() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_Source_7)); }
	inline int32_t get_m_Source_7() const { return ___m_Source_7; }
	inline int32_t* get_address_of_m_Source_7() { return &___m_Source_7; }
	inline void set_m_Source_7(int32_t value)
	{
		___m_Source_7 = value;
	}

	inline static int32_t get_offset_of_m_StateSender_8() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_StateSender_8)); }
	inline AnimationState_t1108360407 * get_m_StateSender_8() const { return ___m_StateSender_8; }
	inline AnimationState_t1108360407 ** get_address_of_m_StateSender_8() { return &___m_StateSender_8; }
	inline void set_m_StateSender_8(AnimationState_t1108360407 * value)
	{
		___m_StateSender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_StateSender_8), value);
	}

	inline static int32_t get_offset_of_m_AnimatorStateInfo_9() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_AnimatorStateInfo_9)); }
	inline AnimatorStateInfo_t509032636  get_m_AnimatorStateInfo_9() const { return ___m_AnimatorStateInfo_9; }
	inline AnimatorStateInfo_t509032636 * get_address_of_m_AnimatorStateInfo_9() { return &___m_AnimatorStateInfo_9; }
	inline void set_m_AnimatorStateInfo_9(AnimatorStateInfo_t509032636  value)
	{
		___m_AnimatorStateInfo_9 = value;
	}

	inline static int32_t get_offset_of_m_AnimatorClipInfo_10() { return static_cast<int32_t>(offsetof(AnimationEvent_t1536042487, ___m_AnimatorClipInfo_10)); }
	inline AnimatorClipInfo_t3156717155  get_m_AnimatorClipInfo_10() const { return ___m_AnimatorClipInfo_10; }
	inline AnimatorClipInfo_t3156717155 * get_address_of_m_AnimatorClipInfo_10() { return &___m_AnimatorClipInfo_10; }
	inline void set_m_AnimatorClipInfo_10(AnimatorClipInfo_t3156717155  value)
	{
		___m_AnimatorClipInfo_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t1536042487_marshaled_pinvoke
{
	float ___m_Time_0;
	char* ___m_FunctionName_1;
	char* ___m_StringParameter_2;
	Object_t631007953_marshaled_pinvoke ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_t1108360407 * ___m_StateSender_8;
	AnimatorStateInfo_t509032636  ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t3156717155  ___m_AnimatorClipInfo_10;
};
// Native definition for COM marshalling of UnityEngine.AnimationEvent
struct AnimationEvent_t1536042487_marshaled_com
{
	float ___m_Time_0;
	Il2CppChar* ___m_FunctionName_1;
	Il2CppChar* ___m_StringParameter_2;
	Object_t631007953_marshaled_com* ___m_ObjectReferenceParameter_3;
	float ___m_FloatParameter_4;
	int32_t ___m_IntParameter_5;
	int32_t ___m_MessageOptions_6;
	int32_t ___m_Source_7;
	AnimationState_t1108360407 * ___m_StateSender_8;
	AnimatorStateInfo_t509032636  ___m_AnimatorStateInfo_9;
	AnimatorClipInfo_t3156717155  ___m_AnimatorClipInfo_10;
};
#endif // ANIMATIONEVENT_T1536042487_H
#ifndef ANIMATIONSTATE_T1108360407_H
#define ANIMATIONSTATE_T1108360407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationState
struct  AnimationState_t1108360407  : public TrackedReference_t1199777556
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T1108360407_H
#ifndef ANIMATIONCLIPPLAYABLE_T3189118652_H
#define ANIMATIONCLIPPLAYABLE_T3189118652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationClipPlayable
struct  AnimationClipPlayable_t3189118652 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationClipPlayable_t3189118652, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPPLAYABLE_T3189118652_H
#ifndef ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#define ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct  AnimationLayerMixerPlayable_t3631223897 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t3631223897, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationLayerMixerPlayable_t3631223897_StaticFields
{
public:
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_t3631223897  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t3631223897_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationLayerMixerPlayable_t3631223897  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationLayerMixerPlayable_t3631223897 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationLayerMixerPlayable_t3631223897  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#ifndef ANIMATIONMIXERPLAYABLE_T821371386_H
#define ANIMATIONMIXERPLAYABLE_T821371386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationMixerPlayable
struct  AnimationMixerPlayable_t821371386 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationMixerPlayable_t821371386, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONMIXERPLAYABLE_T821371386_H
#ifndef ANIMATIONMOTIONXTODELTAPLAYABLE_T272231551_H
#define ANIMATIONMOTIONXTODELTAPLAYABLE_T272231551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationMotionXToDeltaPlayable
struct  AnimationMotionXToDeltaPlayable_t272231551 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationMotionXToDeltaPlayable_t272231551, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONMOTIONXTODELTAPLAYABLE_T272231551_H
#ifndef ANIMATIONOFFSETPLAYABLE_T2887420414_H
#define ANIMATIONOFFSETPLAYABLE_T2887420414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationOffsetPlayable
struct  AnimationOffsetPlayable_t2887420414 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t2887420414, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationOffsetPlayable_t2887420414_StaticFields
{
public:
	// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::m_NullPlayable
	AnimationOffsetPlayable_t2887420414  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationOffsetPlayable_t2887420414_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationOffsetPlayable_t2887420414  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationOffsetPlayable_t2887420414 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationOffsetPlayable_t2887420414  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONOFFSETPLAYABLE_T2887420414_H
#ifndef ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#define ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_t1918618239 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t4208053793  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_t1918618239, ___m_Handle_0)); }
	inline PlayableOutputHandle_t4208053793  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t4208053793 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t4208053793  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#ifndef ANIMATORCONTROLLERPLAYABLE_T1015767841_H
#define ANIMATORCONTROLLERPLAYABLE_T1015767841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimatorControllerPlayable
struct  AnimatorControllerPlayable_t1015767841 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimatorControllerPlayable_t1015767841, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimatorControllerPlayable_t1015767841_StaticFields
{
public:
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_t1015767841  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimatorControllerPlayable_t1015767841_StaticFields, ___m_NullPlayable_1)); }
	inline AnimatorControllerPlayable_t1015767841  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimatorControllerPlayable_t1015767841 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimatorControllerPlayable_t1015767841  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCONTROLLERPLAYABLE_T1015767841_H
#ifndef AVATARMASK_T1182482518_H
#define AVATARMASK_T1182482518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AvatarMask
struct  AvatarMask_t1182482518  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATARMASK_T1182482518_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ANIMATIONSCRIPTPLAYABLE_T1303525964_H
#define ANIMATIONSCRIPTPLAYABLE_T1303525964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Animations.AnimationScriptPlayable
struct  AnimationScriptPlayable_t1303525964 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Animations.AnimationScriptPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationScriptPlayable_t1303525964, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationScriptPlayable_t1303525964_StaticFields
{
public:
	// UnityEngine.Experimental.Animations.AnimationScriptPlayable UnityEngine.Experimental.Animations.AnimationScriptPlayable::m_NullPlayable
	AnimationScriptPlayable_t1303525964  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationScriptPlayable_t1303525964_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationScriptPlayable_t1303525964  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationScriptPlayable_t1303525964 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationScriptPlayable_t1303525964  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSCRIPTPLAYABLE_T1303525964_H
#ifndef HUMANBONE_T2465339518_H
#define HUMANBONE_T2465339518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HumanBone
struct  HumanBone_t2465339518 
{
public:
	// System.String UnityEngine.HumanBone::m_BoneName
	String_t* ___m_BoneName_0;
	// System.String UnityEngine.HumanBone::m_HumanName
	String_t* ___m_HumanName_1;
	// UnityEngine.HumanLimit UnityEngine.HumanBone::limit
	HumanLimit_t2901552972  ___limit_2;

public:
	inline static int32_t get_offset_of_m_BoneName_0() { return static_cast<int32_t>(offsetof(HumanBone_t2465339518, ___m_BoneName_0)); }
	inline String_t* get_m_BoneName_0() const { return ___m_BoneName_0; }
	inline String_t** get_address_of_m_BoneName_0() { return &___m_BoneName_0; }
	inline void set_m_BoneName_0(String_t* value)
	{
		___m_BoneName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoneName_0), value);
	}

	inline static int32_t get_offset_of_m_HumanName_1() { return static_cast<int32_t>(offsetof(HumanBone_t2465339518, ___m_HumanName_1)); }
	inline String_t* get_m_HumanName_1() const { return ___m_HumanName_1; }
	inline String_t** get_address_of_m_HumanName_1() { return &___m_HumanName_1; }
	inline void set_m_HumanName_1(String_t* value)
	{
		___m_HumanName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_HumanName_1), value);
	}

	inline static int32_t get_offset_of_limit_2() { return static_cast<int32_t>(offsetof(HumanBone_t2465339518, ___limit_2)); }
	inline HumanLimit_t2901552972  get_limit_2() const { return ___limit_2; }
	inline HumanLimit_t2901552972 * get_address_of_limit_2() { return &___limit_2; }
	inline void set_limit_2(HumanLimit_t2901552972  value)
	{
		___limit_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.HumanBone
struct HumanBone_t2465339518_marshaled_pinvoke
{
	char* ___m_BoneName_0;
	char* ___m_HumanName_1;
	HumanLimit_t2901552972  ___limit_2;
};
// Native definition for COM marshalling of UnityEngine.HumanBone
struct HumanBone_t2465339518_marshaled_com
{
	Il2CppChar* ___m_BoneName_0;
	Il2CppChar* ___m_HumanName_1;
	HumanLimit_t2901552972  ___limit_2;
};
#endif // HUMANBONE_T2465339518_H
#ifndef MOTION_T1110556653_H
#define MOTION_T1110556653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Motion
struct  Motion_t1110556653  : public Object_t631007953
{
public:
	// System.Boolean UnityEngine.Motion::<isAnimatorMotion>k__BackingField
	bool ___U3CisAnimatorMotionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisAnimatorMotionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Motion_t1110556653, ___U3CisAnimatorMotionU3Ek__BackingField_4)); }
	inline bool get_U3CisAnimatorMotionU3Ek__BackingField_4() const { return ___U3CisAnimatorMotionU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisAnimatorMotionU3Ek__BackingField_4() { return &___U3CisAnimatorMotionU3Ek__BackingField_4; }
	inline void set_U3CisAnimatorMotionU3Ek__BackingField_4(bool value)
	{
		___U3CisAnimatorMotionU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTION_T1110556653_H
#ifndef PLAYABLE_T459825607_H
#define PLAYABLE_T459825607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t459825607 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t459825607, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t459825607_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t459825607  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t459825607_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t459825607  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t459825607 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t459825607  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T459825607_H
#ifndef PLAYABLEBINDING_T354260709_H
#define PLAYABLEBINDING_T354260709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t354260709 
{
public:
	// System.String UnityEngine.Playables.PlayableBinding::m_StreamName
	String_t* ___m_StreamName_0;
	// UnityEngine.Object UnityEngine.Playables.PlayableBinding::m_SourceObject
	Object_t631007953 * ___m_SourceObject_1;
	// System.Type UnityEngine.Playables.PlayableBinding::m_SourceBindingType
	Type_t * ___m_SourceBindingType_2;
	// UnityEngine.Playables.PlayableBinding/CreateOutputMethod UnityEngine.Playables.PlayableBinding::m_CreateOutputMethod
	CreateOutputMethod_t2301811773 * ___m_CreateOutputMethod_3;

public:
	inline static int32_t get_offset_of_m_StreamName_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_StreamName_0)); }
	inline String_t* get_m_StreamName_0() const { return ___m_StreamName_0; }
	inline String_t** get_address_of_m_StreamName_0() { return &___m_StreamName_0; }
	inline void set_m_StreamName_0(String_t* value)
	{
		___m_StreamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_StreamName_0), value);
	}

	inline static int32_t get_offset_of_m_SourceObject_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_SourceObject_1)); }
	inline Object_t631007953 * get_m_SourceObject_1() const { return ___m_SourceObject_1; }
	inline Object_t631007953 ** get_address_of_m_SourceObject_1() { return &___m_SourceObject_1; }
	inline void set_m_SourceObject_1(Object_t631007953 * value)
	{
		___m_SourceObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceObject_1), value);
	}

	inline static int32_t get_offset_of_m_SourceBindingType_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_SourceBindingType_2)); }
	inline Type_t * get_m_SourceBindingType_2() const { return ___m_SourceBindingType_2; }
	inline Type_t ** get_address_of_m_SourceBindingType_2() { return &___m_SourceBindingType_2; }
	inline void set_m_SourceBindingType_2(Type_t * value)
	{
		___m_SourceBindingType_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceBindingType_2), value);
	}

	inline static int32_t get_offset_of_m_CreateOutputMethod_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_CreateOutputMethod_3)); }
	inline CreateOutputMethod_t2301811773 * get_m_CreateOutputMethod_3() const { return ___m_CreateOutputMethod_3; }
	inline CreateOutputMethod_t2301811773 ** get_address_of_m_CreateOutputMethod_3() { return &___m_CreateOutputMethod_3; }
	inline void set_m_CreateOutputMethod_3(CreateOutputMethod_t2301811773 * value)
	{
		___m_CreateOutputMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreateOutputMethod_3), value);
	}
};

struct PlayableBinding_t354260709_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t829358056* ___None_4;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_5;

public:
	inline static int32_t get_offset_of_None_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709_StaticFields, ___None_4)); }
	inline PlayableBindingU5BU5D_t829358056* get_None_4() const { return ___None_4; }
	inline PlayableBindingU5BU5D_t829358056** get_address_of_None_4() { return &___None_4; }
	inline void set_None_4(PlayableBindingU5BU5D_t829358056* value)
	{
		___None_4 = value;
		Il2CppCodeGenWriteBarrier((&___None_4), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709_StaticFields, ___DefaultDuration_5)); }
	inline double get_DefaultDuration_5() const { return ___DefaultDuration_5; }
	inline double* get_address_of_DefaultDuration_5() { return &___DefaultDuration_5; }
	inline void set_DefaultDuration_5(double value)
	{
		___DefaultDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t354260709_marshaled_pinvoke
{
	char* ___m_StreamName_0;
	Object_t631007953_marshaled_pinvoke ___m_SourceObject_1;
	Type_t * ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t354260709_marshaled_com
{
	Il2CppChar* ___m_StreamName_0;
	Object_t631007953_marshaled_com* ___m_SourceObject_1;
	Type_t * ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
#endif // PLAYABLEBINDING_T354260709_H
#ifndef PLAYABLEOUTPUT_T3179894105_H
#define PLAYABLEOUTPUT_T3179894105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutput
struct  PlayableOutput_t3179894105 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::m_Handle
	PlayableOutputHandle_t4208053793  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutput_t3179894105, ___m_Handle_0)); }
	inline PlayableOutputHandle_t4208053793  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t4208053793 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t4208053793  value)
	{
		___m_Handle_0 = value;
	}
};

struct PlayableOutput_t3179894105_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableOutput::m_NullPlayableOutput
	PlayableOutput_t3179894105  ___m_NullPlayableOutput_1;

public:
	inline static int32_t get_offset_of_m_NullPlayableOutput_1() { return static_cast<int32_t>(offsetof(PlayableOutput_t3179894105_StaticFields, ___m_NullPlayableOutput_1)); }
	inline PlayableOutput_t3179894105  get_m_NullPlayableOutput_1() const { return ___m_NullPlayableOutput_1; }
	inline PlayableOutput_t3179894105 * get_address_of_m_NullPlayableOutput_1() { return &___m_NullPlayableOutput_1; }
	inline void set_m_NullPlayableOutput_1(PlayableOutput_t3179894105  value)
	{
		___m_NullPlayableOutput_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUT_T3179894105_H
#ifndef RUNTIMEANIMATORCONTROLLER_T2933699135_H
#define RUNTIMEANIMATORCONTROLLER_T2933699135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimeAnimatorController
struct  RuntimeAnimatorController_t2933699135  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEANIMATORCONTROLLER_T2933699135_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef ANIMATIONCLIP_T2318505987_H
#define ANIMATIONCLIP_T2318505987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationClip
struct  AnimationClip_t2318505987  : public Motion_t1110556653
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIP_T2318505987_H
#ifndef ANIMATOROVERRIDECONTROLLER_T3582062219_H
#define ANIMATOROVERRIDECONTROLLER_T3582062219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorOverrideController
struct  AnimatorOverrideController_t3582062219  : public RuntimeAnimatorController_t2933699135
{
public:
	// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback UnityEngine.AnimatorOverrideController::OnOverrideControllerDirty
	OnOverrideControllerDirtyCallback_t1307045488 * ___OnOverrideControllerDirty_4;

public:
	inline static int32_t get_offset_of_OnOverrideControllerDirty_4() { return static_cast<int32_t>(offsetof(AnimatorOverrideController_t3582062219, ___OnOverrideControllerDirty_4)); }
	inline OnOverrideControllerDirtyCallback_t1307045488 * get_OnOverrideControllerDirty_4() const { return ___OnOverrideControllerDirty_4; }
	inline OnOverrideControllerDirtyCallback_t1307045488 ** get_address_of_OnOverrideControllerDirty_4() { return &___OnOverrideControllerDirty_4; }
	inline void set_OnOverrideControllerDirty_4(OnOverrideControllerDirtyCallback_t1307045488 * value)
	{
		___OnOverrideControllerDirty_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnOverrideControllerDirty_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOROVERRIDECONTROLLER_T3582062219_H
#ifndef ONOVERRIDECONTROLLERDIRTYCALLBACK_T1307045488_H
#define ONOVERRIDECONTROLLERDIRTYCALLBACK_T1307045488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback
struct  OnOverrideControllerDirtyCallback_t1307045488  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONOVERRIDECONTROLLERDIRTYCALLBACK_T1307045488_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CREATEOUTPUTMETHOD_T2301811773_H
#define CREATEOUTPUTMETHOD_T2301811773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding/CreateOutputMethod
struct  CreateOutputMethod_t2301811773  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEOUTPUTMETHOD_T2301811773_H
#ifndef STATEMACHINEBEHAVIOUR_T957311111_H
#define STATEMACHINEBEHAVIOUR_T957311111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t957311111  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T957311111_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ANIMATION_T3648466861_H
#define ANIMATION_T3648466861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t3648466861  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T3648466861_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H

extern "C" void Object_t631007953_marshal_pinvoke(const Object_t631007953& unmarshaled, Object_t631007953_marshaled_pinvoke& marshaled);
extern "C" void Object_t631007953_marshal_pinvoke_back(const Object_t631007953_marshaled_pinvoke& marshaled, Object_t631007953& unmarshaled);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(Object_t631007953_marshaled_pinvoke& marshaled);
extern "C" void Object_t631007953_marshal_com(const Object_t631007953& unmarshaled, Object_t631007953_marshaled_com& marshaled);
extern "C" void Object_t631007953_marshal_com_back(const Object_t631007953_marshaled_com& marshaled, Object_t631007953& unmarshaled);
extern "C" void Object_t631007953_marshal_com_cleanup(Object_t631007953_marshaled_com& marshaled);

// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationClipPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t3189118652_m4201087276_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationLayerMixerPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t3631223897_m201603007_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationMixerPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t821371386_m4108826000_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationMotionXToDeltaPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimationMotionXToDeltaPlayable_t272231551_m1123153091_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationOffsetPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t2887420414_m2033286094_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Animations.AnimationPlayableOutput>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t1918618239_m3383247624_gshared (PlayableOutputHandle_t4208053793 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimatorControllerPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t1015767841_m3416945299_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Experimental.Animations.AnimationScriptPlayable>()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsPlayableOfType_TisAnimationScriptPlayable_t1303525964_m1992139667_gshared (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C" IL2CPP_METHOD_ATTR void Animation_INTERNAL_CALL_Stop_m1587721271 (RuntimeObject * __this /* static, unused */, Animation_t3648466861 * ___self0, const RuntimeMethod* method);
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_Internal_StopByName_m2571804745 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_Internal_RewindByName_m3260795103 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C" IL2CPP_METHOD_ATTR AnimationState_t1108360407 * Animation_GetState_m2252204496 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C" IL2CPP_METHOD_ATTR bool Animation_Play_m10633183 (Animation_t3648466861 * __this, String_t* ___animation0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animation_AddClip_m131581775 (Animation_t3648466861 * __this, AnimationClip_t2318505987 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, const RuntimeMethod* method);
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animation_AddClip_m2043891899 (Animation_t3648466861 * __this, AnimationClip_t2318505987 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, bool ___addLoopFrame4, const RuntimeMethod* method);
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C" IL2CPP_METHOD_ATTR void Enumerator__ctor_m1737537150 (Enumerator_t1136361084 * __this, Animation_t3648466861 * ___outer0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimationState_t1108360407 * Animation_GetStateAtIndex_m3906320186 (Animation_t3648466861 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Animation_GetStateCount_m3809146648 (Animation_t3648466861 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Motion::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Motion__ctor_m1337281595 (Motion_t1110556653 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C" IL2CPP_METHOD_ATTR void AnimationClip_Internal_CreateAnimationClip_m2711711193 (RuntimeObject * __this /* static, unused */, AnimationClip_t2318505987 * ___self0, const RuntimeMethod* method);
// System.Void UnityEngine.TrackedReference::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TrackedReference__ctor_m3086593239 (TrackedReference_t1199777556 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_AddMixingTransform_m3342983193 (AnimationState_t1108360407 * __this, Transform_t3600365921 * ___mix0, bool ___recursive1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_IsValid_m777349566 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationClipPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t3189118652_m4201087276 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t3189118652_m4201087276_gshared)(__this, method);
}
// System.Void System.InvalidCastException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void InvalidCastException__ctor_m318645277 (InvalidCastException_t3927145244 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationClipPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationClipPlayable__ctor_m2913235724 (AnimationClipPlayable_t3189118652 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationClipPlayable_CreateHandle_m999922984 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, AnimationClip_t2318505987 * ___clip1, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  PlayableHandle_get_Null_m218234861 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_CreateHandleInternal_m3627497527 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, AnimationClip_t2318505987 * ___clip1, PlayableHandle_t1095853803 * ___handle2, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationClipPlayable_GetHandle_m3441351653 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void Playable__ctor_m3175303195 (Playable_t459825607 * __this, PlayableHandle_t1095853803  p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR bool PlayableHandle_op_Equality_m3344837515 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803  p0, PlayableHandle_t1095853803  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::Equals(UnityEngine.Animations.AnimationClipPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_Equals_m1031707451 (AnimationClipPlayable_t3189118652 * __this, AnimationClipPlayable_t3189118652  ___other0, const RuntimeMethod* method);
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClipInternal_m2826345587 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method);
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClip()
extern "C" IL2CPP_METHOD_ATTR AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClip_m824553280 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyFootIKInternal_m1617064962 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIK()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyFootIK_m1549118896 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyPlayableIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyPlayableIKInternal_m2641340007 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyPlayableIK()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyPlayableIK_m3535159484 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetRemoveStartOffsetInternal_m1929113867 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffset()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetRemoveStartOffset_m1243433047 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationClipPlayable_SetRemoveStartOffsetInternal_m332890011 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffset(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationClipPlayable_SetRemoveStartOffset_m149988838 (AnimationClipPlayable_t3189118652 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_CreateHandleInternal_Injected_m2693506418 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, AnimationClip_t2318505987 * ___clip1, PlayableHandle_t1095853803 * ___handle2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationLayerMixerPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t3631223897_m201603007 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t3631223897_m201603007_gshared)(__this, method);
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable__ctor_m860721801 (AnimationLayerMixerPlayable_t3631223897 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationLayerMixerPlayable_CreateHandle_m2143407042 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationLayerMixerPlayable_CreateHandleInternal_m2872369648 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableHandle::SetInputCount(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayableHandle_SetInputCount_m2596808857 (PlayableHandle_t1095853803 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationLayerMixerPlayable_GetHandle_m3854437798 (AnimationLayerMixerPlayable_t3631223897 * __this, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  Playable_GetHandle_m98909670 (Playable_t459825607 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::Equals(UnityEngine.Animations.AnimationLayerMixerPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationLayerMixerPlayable_Equals_m4271416323 (AnimationLayerMixerPlayable_t3631223897 * __this, AnimationLayerMixerPlayable_t3631223897  ___other0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Playables.PlayableHandle::GetInputCount()
extern "C" IL2CPP_METHOD_ATTR int32_t PlayableHandle_GetInputCount_m888565651 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m282481429 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m3149220962 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, uint32_t ___layerIndex1, AvatarMask_t1182482518 * ___mask2, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMask(System.UInt32,UnityEngine.AvatarMask)
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514 (AnimationLayerMixerPlayable_t3631223897 * __this, uint32_t ___layerIndex0, AvatarMask_t1182482518 * ___mask1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationLayerMixerPlayable_CreateHandleInternal_Injected_m1824026698 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationMixerPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t821371386_m4108826000 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t821371386_m4108826000_gshared)(__this, method);
}
// System.Void UnityEngine.Animations.AnimationMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationMixerPlayable__ctor_m848068859 (AnimationMixerPlayable_t821371386 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMixerPlayable_CreateHandle_m3482410404 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMixerPlayable_CreateHandleInternal_m3818145798 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMixerPlayable_GetHandle_m2198358075 (AnimationMixerPlayable_t821371386 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::Equals(UnityEngine.Animations.AnimationMixerPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMixerPlayable_Equals_m3971478909 (AnimationMixerPlayable_t821371386 * __this, AnimationMixerPlayable_t821371386  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMixerPlayable_CreateHandleInternal_Injected_m3885529074 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationMotionXToDeltaPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimationMotionXToDeltaPlayable_t272231551_m1123153091 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationMotionXToDeltaPlayable_t272231551_m1123153091_gshared)(__this, method);
}
// System.Void UnityEngine.Animations.AnimationMotionXToDeltaPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationMotionXToDeltaPlayable__ctor_m1725027713 (AnimationMotionXToDeltaPlayable_t272231551 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMotionXToDeltaPlayable_CreateHandle_m3954964130 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMotionXToDeltaPlayable_CreateHandleInternal_m3250489882 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMotionXToDeltaPlayable_GetHandle_m1319190040 (AnimationMotionXToDeltaPlayable_t272231551 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationMotionXToDeltaPlayable::Equals(UnityEngine.Animations.AnimationMotionXToDeltaPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMotionXToDeltaPlayable_Equals_m3896842955 (AnimationMotionXToDeltaPlayable_t272231551 * __this, AnimationMotionXToDeltaPlayable_t272231551  ___other0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMotionXToDeltaPlayable_CreateHandleInternal_Injected_m1129764157 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimationOffsetPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t2887420414_m2033286094 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t2887420414_m2033286094_gshared)(__this, method);
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable__ctor_m577157274 (AnimationOffsetPlayable_t2887420414 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationOffsetPlayable_CreateHandle_m2427170463 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, int32_t ___inputCount3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationOffsetPlayable_CreateHandleInternal_m904339466 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationOffsetPlayable_GetHandle_m449079993 (AnimationOffsetPlayable_t2887420414 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::Equals(UnityEngine.Animations.AnimationOffsetPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationOffsetPlayable_Equals_m2902253045 (AnimationOffsetPlayable_t2887420414 * __this, AnimationOffsetPlayable_t2887420414  ___other0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  AnimationOffsetPlayable_GetPositionInternal_m3429692954 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  AnimationOffsetPlayable_GetPosition_m3423307712 (AnimationOffsetPlayable_t2887420414 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  AnimationOffsetPlayable_GetRotationInternal_m1801279867 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  AnimationOffsetPlayable_GetRotation_m259451460 (AnimationOffsetPlayable_t2887420414 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationOffsetPlayable_CreateHandleInternal_Injected_m3452138878 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, Vector3_t3722313464 * ___position1, Quaternion_t2301928331 * ___rotation2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal_Injected(UnityEngine.Playables.PlayableHandle&,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable_GetPositionInternal_Injected_m215513770 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, Vector3_t3722313464 * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal_Injected(UnityEngine.Playables.PlayableHandle&,UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable_GetRotationInternal_Injected_m1881104651 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, Quaternion_t2301928331 * ___ret1, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableBinding/CreateOutputMethod::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void CreateOutputMethod__ctor_m3884516706 (CreateOutputMethod_t2301811773 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableBinding UnityEngine.Playables.PlayableBinding::CreateInternal(System.String,UnityEngine.Object,System.Type,UnityEngine.Playables.PlayableBinding/CreateOutputMethod)
extern "C" IL2CPP_METHOD_ATTR PlayableBinding_t354260709  PlayableBinding_CreateInternal_m1294529348 (RuntimeObject * __this /* static, unused */, String_t* p0, Object_t631007953 * p1, Type_t * p2, CreateOutputMethod_t2301811773 * p3, const RuntimeMethod* method);
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::Create(UnityEngine.Playables.PlayableGraph,System.String,UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR AnimationPlayableOutput_t1918618239  AnimationPlayableOutput_Create_m1296576143 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, String_t* ___name1, Animator_t434523843 * ___target2, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableOutput UnityEngine.Animations.AnimationPlayableOutput::op_Implicit(UnityEngine.Animations.AnimationPlayableOutput)
extern "C" IL2CPP_METHOD_ATTR PlayableOutput_t3179894105  AnimationPlayableOutput_op_Implicit_m2440191068 (RuntimeObject * __this /* static, unused */, AnimationPlayableOutput_t1918618239  ___output0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsValid()
extern "C" IL2CPP_METHOD_ATTR bool PlayableOutputHandle_IsValid_m3926512399 (PlayableOutputHandle_t4208053793 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableOutputHandle::IsPlayableOutputOfType<UnityEngine.Animations.AnimationPlayableOutput>()
inline bool PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t1918618239_m3383247624 (PlayableOutputHandle_t4208053793 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableOutputHandle_t4208053793 *, const RuntimeMethod*))PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t1918618239_m3383247624_gshared)(__this, method);
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableOutput__ctor_m457905557 (AnimationPlayableOutput_t1918618239 * __this, PlayableOutputHandle_t4208053793  ___handle0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m28371369 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, String_t* ___name1, PlayableOutputHandle_t4208053793 * ___handle2, const RuntimeMethod* method);
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::get_Null()
extern "C" IL2CPP_METHOD_ATTR AnimationPlayableOutput_t1918618239  AnimationPlayableOutput_get_Null_m927234140 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationPlayableOutput::SetTarget(UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableOutput_SetTarget_m4167975687 (AnimationPlayableOutput_t1918618239 * __this, Animator_t434523843 * ___value0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern "C" IL2CPP_METHOD_ATTR PlayableOutputHandle_t4208053793  PlayableOutputHandle_get_Null_m1200584339 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableOutputHandle_t4208053793  AnimationPlayableOutput_GetHandle_m644109061 (AnimationPlayableOutput_t1918618239 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C" IL2CPP_METHOD_ATTR void PlayableOutput__ctor_m3330119218 (PlayableOutput_t3179894105 * __this, PlayableOutputHandle_t4208053793  p0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableOutputHandle_t4208053793  PlayableOutput_GetHandle_m777137769 (PlayableOutput_t3179894105 * __this, const RuntimeMethod* method);
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C" IL2CPP_METHOD_ATTR Animator_t434523843 * AnimationPlayableOutput_InternalGetTarget_m2511702932 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t4208053793 * ___handle0, const RuntimeMethod* method);
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::GetTarget()
extern "C" IL2CPP_METHOD_ATTR Animator_t434523843 * AnimationPlayableOutput_GetTarget_m58157427 (AnimationPlayableOutput_t1918618239 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimationPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableOutput_InternalSetTarget_m860681215 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t4208053793 * ___handle0, Animator_t434523843 * ___target1, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::SetHandle(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimatorControllerPlayable_SetHandle_m2260644454 (AnimatorControllerPlayable_t1015767841 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimatorControllerPlayable__ctor_m3584989806 (AnimatorControllerPlayable_t1015767841 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimatorControllerPlayable_GetHandle_m2425630109 (AnimatorControllerPlayable_t1015767841 * __this, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Animations.AnimatorControllerPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t1015767841_m3416945299 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t1015767841_m3416945299_gshared)(__this, method);
}
// System.Boolean UnityEngine.Animations.AnimatorControllerPlayable::Equals(UnityEngine.Animations.AnimatorControllerPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorControllerPlayable_Equals_m3421642688 (AnimatorControllerPlayable_t1015767841 * __this, AnimatorControllerPlayable_t1015767841  ___other0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Behaviour__ctor_m346897018 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetFloatID_m3658774233 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloatID_m759961653 (Animator_t434523843 * __this, int32_t ___id0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloatIDDamp_m1611756056 (Animator_t434523843 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Animator_GetBoolID_m3211650753 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetBoolID_m2106676274 (Animator_t434523843 * __this, int32_t ___id0, bool ___value1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_GetIntegerID_m3539387357 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetIntegerID_m1197891907 (Animator_t434523843 * __this, int32_t ___id0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetTriggerString_m2612407758 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_ResetTriggerString_m394341254 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Animator_IsParameterControlledByCurveString_m3449841540 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_deltaPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_deltaPosition_Injected_m3057738582 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_deltaRotation_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_deltaRotation_Injected_m2558147056 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_rootPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_rootPosition_Injected_m2912740584 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_rootRotation_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_rootRotation_Injected_m1378784712 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern "C" IL2CPP_METHOD_ATTR void Animator_CheckIfInIKPass_m1701567706 (Animator_t434523843 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPositionInternal()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_bodyPositionInternal_m3052787056 (Animator_t434523843 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_bodyPositionInternal(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyPositionInternal_m667443125 (Animator_t434523843 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_bodyPositionInternal_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_bodyPositionInternal_Injected_m4243447051 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_bodyPositionInternal_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyPositionInternal_Injected_m1654945682 (Animator_t434523843 * __this, Vector3_t3722313464 * ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotationInternal()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_get_bodyRotationInternal_m3152232923 (Animator_t434523843 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_bodyRotationInternal(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyRotationInternal_m1688178068 (Animator_t434523843 * __this, Quaternion_t2301928331  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_bodyRotationInternal_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_bodyRotationInternal_Injected_m1688249227 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::set_bodyRotationInternal_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyRotationInternal_Injected_m139719495 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animator::GetGoalPosition(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_GetGoalPosition_m3762162859 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::GetGoalPosition_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetGoalPosition_Injected_m3263093872 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464 * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetGoalPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalPosition_m3669207002 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464  ___goalPosition1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetGoalPosition_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalPosition_Injected_m3736935077 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464 * ___goalPosition1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Animator::GetGoalRotation(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_GetGoalRotation_m1869366523 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::GetGoalRotation_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetGoalRotation_Injected_m456765294 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331 * ___ret1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetGoalRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalRotation_m1125692758 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331  ___goalRotation1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetGoalRotation_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalRotation_Injected_m2163733550 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331 * ___goalRotation1, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetGoalWeightPosition(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetGoalWeightPosition_m1730430196 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetGoalWeightPosition(UnityEngine.AvatarIKGoal,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalWeightPosition_m3736882473 (Animator_t434523843 * __this, int32_t ___goal0, float ___value1, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetGoalWeightRotation(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetGoalWeightRotation_m2450456186 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetGoalWeightRotation(UnityEngine.AvatarIKGoal,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalWeightRotation_m1438054163 (Animator_t434523843 * __this, int32_t ___goal0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtPositionInternal_m1039545480 (Animator_t434523843 * __this, Vector3_t3722313464  ___lookAtPosition0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetLookAtPositionInternal_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtPositionInternal_Injected_m733885560 (Animator_t434523843 * __this, Vector3_t3722313464 * ___lookAtPosition0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeightInternal_m1957339916 (Animator_t434523843 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::GetAnimatorStateInfo(System.Int32,UnityEngine.StateInfoIndex,UnityEngine.AnimatorStateInfo&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetAnimatorStateInfo_m2045721220 (Animator_t434523843 * __this, int32_t ___layerIndex0, int32_t ___stateInfoIndex1, AnimatorStateInfo_t509032636 * ___info2, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32,UnityEngine.AnimatorTransitionInfo&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetAnimatorTransitionInfo_m4215243493 (Animator_t434523843 * __this, int32_t ___layerIndex0, AnimatorTransitionInfo_t2534804151 * ___info1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_pivotPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_pivotPosition_Injected_m3973723677 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::MatchTarget_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_MatchTarget_Injected_m291306687 (Animator_t434523843 * __this, Vector3_t3722313464 * ___matchPosition0, Quaternion_t2301928331 * ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t554846203 * ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_MatchTarget_m2861954881 (Animator_t434523843 * __this, Vector3_t3722313464  ___matchPosition0, Quaternion_t2301928331  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t554846203  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_CrossFade_m633499242 (Animator_t434523843 * __this, String_t* ___stateName0, float ___normalizedTransitionDuration1, int32_t ___layer2, float ___normalizedTimeOffset3, float ___normalizedTransitionTime4, const RuntimeMethod* method);
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_StringToHash_m1666053228 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_CrossFade_m2328649388 (Animator_t434523843 * __this, int32_t ___stateHashName0, float ___normalizedTransitionDuration1, int32_t ___layer2, float ___normalizedTimeOffset3, float ___normalizedTransitionTime4, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m2835034014 (Animator_t434523843 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m1207279914 (Animator_t434523843 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_targetPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_targetPosition_Injected_m1456028510 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::get_targetRotation_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_targetRotation_Injected_m2584699430 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void IndexOutOfRangeException__ctor_m3408750441 (IndexOutOfRangeException_t1578797820 * __this, String_t* p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.HumanTrait::GetBoneIndexFromMono(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t HumanTrait_GetBoneIndexFromMono_m1872291878 (RuntimeObject * __this /* static, unused */, int32_t ___humanId0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransformInternal(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Animator_GetBoneTransformInternal_m528051545 (Animator_t434523843 * __this, int32_t ___humanBoneId0, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetRecorderStartTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_GetRecorderStartTime_m2061949189 (Animator_t434523843 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Animator::GetRecorderStopTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_GetRecorderStopTime_m2000074451 (Animator_t434523843 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_logWarnings_m3175887280 (Animator_t434523843 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animator::IsInIKPass()
extern "C" IL2CPP_METHOD_ATTR bool Animator_IsInIKPass_m1033564951 (Animator_t434523843 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback::Invoke()
extern "C" IL2CPP_METHOD_ATTR void OnOverrideControllerDirtyCallback_Invoke_m764065723 (OnOverrideControllerDirtyCallback_t1307045488 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_IsName_m3393819976 (AnimatorStateInfo_t509032636 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorStateInfo_get_fullPathHash_m2978085309 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorStateInfo_get_shortNameHash_m3578045446 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" IL2CPP_METHOD_ATTR float AnimatorStateInfo_get_normalizedTime_m2701390466 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" IL2CPP_METHOD_ATTR float AnimatorStateInfo_get_length_m130221364 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorStateInfo_get_tagHash_m1760199943 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_IsTag_m1942726747 (AnimatorStateInfo_t509032636 * __this, String_t* ___tag0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_get_loop_m3780967345 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorTransitionInfo_IsName_m399618195 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorTransitionInfo_IsUserName_m1292791005 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorTransitionInfo_get_nameHash_m3336626857 (AnimatorTransitionInfo_t2534804151 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorTransitionInfo_get_userNameHash_m2248997669 (AnimatorTransitionInfo_t2534804151 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" IL2CPP_METHOD_ATTR float AnimatorTransitionInfo_get_normalizedTime_m1885586090 (AnimatorTransitionInfo_t2534804151 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType<UnityEngine.Experimental.Animations.AnimationScriptPlayable>()
inline bool PlayableHandle_IsPlayableOfType_TisAnimationScriptPlayable_t1303525964_m1992139667 (PlayableHandle_t1095853803 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableHandle_t1095853803 *, const RuntimeMethod*))PlayableHandle_IsPlayableOfType_TisAnimationScriptPlayable_t1303525964_m1992139667_gshared)(__this, method);
}
// System.Void UnityEngine.Experimental.Animations.AnimationScriptPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationScriptPlayable__ctor_m1047518474 (AnimationScriptPlayable_t1303525964 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Animations.AnimationScriptPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationScriptPlayable_GetHandle_m632811930 (AnimationScriptPlayable_t1303525964 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Experimental.Animations.AnimationScriptPlayable::Equals(UnityEngine.Experimental.Animations.AnimationScriptPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationScriptPlayable_Equals_m534223357 (AnimationScriptPlayable_t1303525964 * __this, AnimationScriptPlayable_t1303525964  ___other0, const RuntimeMethod* method);
// System.Void UnityEngine.MatchTargetWeightMask::.ctor(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MatchTargetWeightMask__ctor_m4251631450 (MatchTargetWeightMask_t554846203 * __this, Vector3_t3722313464  ___positionXYZWeight0, float ___rotationWeight1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m1087895580 (Object_t631007953 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animation::Stop()
extern "C" IL2CPP_METHOD_ATTR void Animation_Stop_m195768787 (Animation_t3648466861 * __this, const RuntimeMethod* method)
{
	{
		Animation_INTERNAL_CALL_Stop_m1587721271(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C" IL2CPP_METHOD_ATTR void Animation_INTERNAL_CALL_Stop_m1587721271 (RuntimeObject * __this /* static, unused */, Animation_t3648466861 * ___self0, const RuntimeMethod* method)
{
	typedef void (*Animation_INTERNAL_CALL_Stop_m1587721271_ftn) (Animation_t3648466861 *);
	static Animation_INTERNAL_CALL_Stop_m1587721271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Stop_m1587721271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Animation::Stop(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_Stop_m126566212 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_StopByName_m2571804745(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_Internal_StopByName_m2571804745 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animation_Internal_StopByName_m2571804745_ftn) (Animation_t3648466861 *, String_t*);
	static Animation_Internal_StopByName_m2571804745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_StopByName_m2571804745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_StopByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animation::Rewind(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_Rewind_m1690774295 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_RewindByName_m3260795103(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_Internal_RewindByName_m3260795103 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animation_Internal_RewindByName_m3260795103_ftn) (Animation_t3648466861 *, String_t*);
	static Animation_Internal_RewindByName_m3260795103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_RewindByName_m3260795103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_RewindByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C" IL2CPP_METHOD_ATTR AnimationState_t1108360407 * Animation_get_Item_m2435218778 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	AnimationState_t1108360407 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		AnimationState_t1108360407 * L_1 = Animation_GetState_m2252204496(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		AnimationState_t1108360407 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C" IL2CPP_METHOD_ATTR bool Animation_Play_m10633183 (Animation_t3648466861 * __this, String_t* ___animation0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef bool (*Animation_Play_m10633183_ftn) (Animation_t3648466861 *, String_t*, int32_t);
	static Animation_Play_m10633183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Play_m10633183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)");
	bool retVal = _il2cpp_icall_func(__this, ___animation0, ___mode1);
	return retVal;
}
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Animation_Play_m27531216 (Animation_t3648466861 * __this, String_t* ___animation0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		String_t* L_0 = ___animation0;
		int32_t L_1 = V_0;
		bool L_2 = Animation_Play_m10633183(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C" IL2CPP_METHOD_ATTR void Animation_CrossFade_m3062450341 (Animation_t3648466861 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___mode2, const RuntimeMethod* method)
{
	typedef void (*Animation_CrossFade_m3062450341_ftn) (Animation_t3648466861 *, String_t*, float, int32_t);
	static Animation_CrossFade_m3062450341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_CrossFade_m3062450341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)");
	_il2cpp_icall_func(__this, ___animation0, ___fadeLength1, ___mode2);
}
// System.Void UnityEngine.Animation::Blend(System.String,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animation_Blend_m1353791016 (Animation_t3648466861 * __this, String_t* ___animation0, float ___targetWeight1, float ___fadeLength2, const RuntimeMethod* method)
{
	typedef void (*Animation_Blend_m1353791016_ftn) (Animation_t3648466861 *, String_t*, float, float);
	static Animation_Blend_m1353791016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Blend_m1353791016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Blend(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___animation0, ___targetWeight1, ___fadeLength2);
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String)
extern "C" IL2CPP_METHOD_ATTR void Animation_AddClip_m2720317275 (Animation_t3648466861 * __this, AnimationClip_t2318505987 * ___clip0, String_t* ___newName1, const RuntimeMethod* method)
{
	{
		AnimationClip_t2318505987 * L_0 = ___clip0;
		String_t* L_1 = ___newName1;
		Animation_AddClip_m131581775(__this, L_0, L_1, ((int32_t)-2147483648LL), ((int32_t)2147483647LL), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animation_AddClip_m2043891899 (Animation_t3648466861 * __this, AnimationClip_t2318505987 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, bool ___addLoopFrame4, const RuntimeMethod* method)
{
	typedef void (*Animation_AddClip_m2043891899_ftn) (Animation_t3648466861 *, AnimationClip_t2318505987 *, String_t*, int32_t, int32_t, bool);
	static Animation_AddClip_m2043891899_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_AddClip_m2043891899_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___clip0, ___newName1, ___firstFrame2, ___lastFrame3, ___addLoopFrame4);
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animation_AddClip_m131581775 (Animation_t3648466861 * __this, AnimationClip_t2318505987 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		AnimationClip_t2318505987 * L_0 = ___clip0;
		String_t* L_1 = ___newName1;
		int32_t L_2 = ___firstFrame2;
		int32_t L_3 = ___lastFrame3;
		bool L_4 = V_0;
		Animation_AddClip_m2043891899(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Animation_GetEnumerator_m1233868201 (Animation_t3648466861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animation_GetEnumerator_m1233868201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Enumerator_t1136361084 * L_0 = (Enumerator_t1136361084 *)il2cpp_codegen_object_new(Enumerator_t1136361084_il2cpp_TypeInfo_var);
		Enumerator__ctor_m1737537150(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C" IL2CPP_METHOD_ATTR AnimationState_t1108360407 * Animation_GetState_m2252204496 (Animation_t3648466861 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef AnimationState_t1108360407 * (*Animation_GetState_m2252204496_ftn) (Animation_t3648466861 *, String_t*);
	static Animation_GetState_m2252204496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetState_m2252204496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetState(System.String)");
	AnimationState_t1108360407 * retVal = _il2cpp_icall_func(__this, ___name0);
	return retVal;
}
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimationState_t1108360407 * Animation_GetStateAtIndex_m3906320186 (Animation_t3648466861 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef AnimationState_t1108360407 * (*Animation_GetStateAtIndex_m3906320186_ftn) (Animation_t3648466861 *, int32_t);
	static Animation_GetStateAtIndex_m3906320186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateAtIndex_m3906320186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateAtIndex(System.Int32)");
	AnimationState_t1108360407 * retVal = _il2cpp_icall_func(__this, ___index0);
	return retVal;
}
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Animation_GetStateCount_m3809146648 (Animation_t3648466861 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Animation_GetStateCount_m3809146648_ftn) (Animation_t3648466861 *);
	static Animation_GetStateCount_m3809146648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateCount_m3809146648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C" IL2CPP_METHOD_ATTR void Enumerator__ctor_m1737537150 (Enumerator_t1136361084 * __this, Animation_t3648466861 * ___outer0, const RuntimeMethod* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Animation_t3648466861 * L_0 = ___outer0;
		__this->set_m_Outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m3574888872 (Enumerator_t1136361084 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Animation_t3648466861 * L_0 = __this->get_m_Outer_0();
		int32_t L_1 = __this->get_m_CurrentIndex_1();
		NullCheck(L_0);
		AnimationState_t1108360407 * L_2 = Animation_GetStateAtIndex_m3906320186(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m4249399726 (Enumerator_t1136361084 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		Animation_t3648466861 * L_0 = __this->get_m_Outer_0();
		NullCheck(L_0);
		int32_t L_1 = Animation_GetStateCount_m3809146648(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_m_CurrentIndex_1();
		__this->set_m_CurrentIndex_1(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1)));
		int32_t L_3 = __this->get_m_CurrentIndex_1();
		int32_t L_4 = V_0;
		V_1 = (bool)((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
		goto IL_002a;
	}

IL_002a:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.Animation/Enumerator::Reset()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Reset_m2475628812 (Enumerator_t1136361084 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AnimationClip::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationClip__ctor_m583690604 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method)
{
	{
		Motion__ctor_m1337281595(__this, /*hidden argument*/NULL);
		AnimationClip_Internal_CreateAnimationClip_m2711711193(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C" IL2CPP_METHOD_ATTR void AnimationClip_Internal_CreateAnimationClip_m2711711193 (RuntimeObject * __this /* static, unused */, AnimationClip_t2318505987 * ___self0, const RuntimeMethod* method)
{
	typedef void (*AnimationClip_Internal_CreateAnimationClip_m2711711193_ftn) (AnimationClip_t2318505987 *);
	static AnimationClip_Internal_CreateAnimationClip_m2711711193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_Internal_CreateAnimationClip_m2711711193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)");
	_il2cpp_icall_func(___self0);
}
// System.Single UnityEngine.AnimationClip::get_length()
extern "C" IL2CPP_METHOD_ATTR float AnimationClip_get_length_m3296085482 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationClip_get_length_m3296085482_ftn) (AnimationClip_t2318505987 *);
	static AnimationClip_get_length_m3296085482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_length_m3296085482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_length()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.AnimationClip::get_frameRate()
extern "C" IL2CPP_METHOD_ATTR float AnimationClip_get_frameRate_m1820704095 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationClip_get_frameRate_m1820704095_ftn) (AnimationClip_t2318505987 *);
	static AnimationClip_get_frameRate_m1820704095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_frameRate_m1820704095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_frameRate()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
extern "C" IL2CPP_METHOD_ATTR void AnimationClip_SetCurve_m2277705262 (AnimationClip_t2318505987 * __this, String_t* ___relativePath0, Type_t * ___type1, String_t* ___propertyName2, AnimationCurve_t3046754366 * ___curve3, const RuntimeMethod* method)
{
	typedef void (*AnimationClip_SetCurve_m2277705262_ftn) (AnimationClip_t2318505987 *, String_t*, Type_t *, String_t*, AnimationCurve_t3046754366 *);
	static AnimationClip_SetCurve_m2277705262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_SetCurve_m2277705262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)");
	_il2cpp_icall_func(__this, ___relativePath0, ___type1, ___propertyName2, ___curve3);
}
// System.Boolean UnityEngine.AnimationClip::get_legacy()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClip_get_legacy_m310696710 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationClip_get_legacy_m310696710_ftn) (AnimationClip_t2318505987 *);
	static AnimationClip_get_legacy_m310696710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_legacy_m310696710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_legacy()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationClip::set_legacy(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationClip_set_legacy_m1671359110 (AnimationClip_t2318505987 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationClip_set_legacy_m1671359110_ftn) (AnimationClip_t2318505987 *, bool);
	static AnimationClip_set_legacy_m1671359110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_set_legacy_m1671359110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::set_legacy(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AnimationClip::get_empty()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClip_get_empty_m1093497191 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationClip_get_empty_m1093497191_ftn) (AnimationClip_t2318505987 *);
	static AnimationClip_get_empty_m1093497191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_empty_m1093497191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_empty()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.AnimationClip::get_hasRootMotion()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClip_get_hasRootMotion_m3285071990 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationClip_get_hasRootMotion_m3285071990_ftn) (AnimationClip_t2318505987 *);
	static AnimationClip_get_hasRootMotion_m3285071990_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_get_hasRootMotion_m3285071990_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::get_hasRootMotion()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(const AnimationEvent_t1536042487& unmarshaled, AnimationEvent_t1536042487_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception, NULL, NULL);
}
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(const AnimationEvent_t1536042487_marshaled_pinvoke& marshaled, AnimationEvent_t1536042487& unmarshaled)
{
	Exception_t* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(AnimationEvent_t1536042487_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_com(const AnimationEvent_t1536042487& unmarshaled, AnimationEvent_t1536042487_marshaled_com& marshaled)
{
	Exception_t* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception, NULL, NULL);
}
extern "C" void AnimationEvent_t1536042487_marshal_com_back(const AnimationEvent_t1536042487_marshaled_com& marshaled, AnimationEvent_t1536042487& unmarshaled)
{
	Exception_t* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_com_cleanup(AnimationEvent_t1536042487_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationEvent__ctor_m234009954 (AnimationEvent_t1536042487 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent__ctor_m234009954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_m_Time_0((0.0f));
		__this->set_m_FunctionName_1(_stringLiteral757602046);
		__this->set_m_StringParameter_2(_stringLiteral757602046);
		__this->set_m_ObjectReferenceParameter_3((Object_t631007953 *)NULL);
		__this->set_m_FloatParameter_4((0.0f));
		__this->set_m_IntParameter_5(0);
		__this->set_m_MessageOptions_6(0);
		__this->set_m_Source_7(0);
		__this->set_m_StateSender_8((AnimationState_t1108360407 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AnimationState::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationState__ctor_m1160185825 (AnimationState_t1108360407 * __this, const RuntimeMethod* method)
{
	{
		TrackedReference__ctor_m3086593239(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.AnimationState::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool AnimationState_get_enabled_m1409471819 (AnimationState_t1108360407 * __this, const RuntimeMethod* method)
{
	typedef bool (*AnimationState_get_enabled_m1409471819_ftn) (AnimationState_t1108360407 *);
	static AnimationState_get_enabled_m1409471819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_enabled_m1409471819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_enabled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_enabled_m2387746852 (AnimationState_t1108360407 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_enabled_m2387746852_ftn) (AnimationState_t1108360407 *, bool);
	static AnimationState_set_enabled_m2387746852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_enabled_m2387746852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_weight(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_weight_m907168059 (AnimationState_t1108360407 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_weight_m907168059_ftn) (AnimationState_t1108360407 *, float);
	static AnimationState_set_weight_m907168059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_weight_m907168059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_weight(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WrapMode UnityEngine.AnimationState::get_wrapMode()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimationState_get_wrapMode_m1186389710 (AnimationState_t1108360407 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*AnimationState_get_wrapMode_m1186389710_ftn) (AnimationState_t1108360407 *);
	static AnimationState_get_wrapMode_m1186389710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_wrapMode_m1186389710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_wrapMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_wrapMode_m932751232 (AnimationState_t1108360407 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_wrapMode_m932751232_ftn) (AnimationState_t1108360407 *, int32_t);
	static AnimationState_set_wrapMode_m932751232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_wrapMode_m932751232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_time()
extern "C" IL2CPP_METHOD_ATTR float AnimationState_get_time_m2889734845 (AnimationState_t1108360407 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationState_get_time_m2889734845_ftn) (AnimationState_t1108360407 *);
	static AnimationState_get_time_m2889734845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_time_m2889734845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_time()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_time_m3753967308 (AnimationState_t1108360407 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_time_m3753967308_ftn) (AnimationState_t1108360407 *, float);
	static AnimationState_set_time_m3753967308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_time_m3753967308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_normalizedTime_m1781758901 (AnimationState_t1108360407 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_normalizedTime_m1781758901_ftn) (AnimationState_t1108360407 *, float);
	static AnimationState_set_normalizedTime_m1781758901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_normalizedTime_m1781758901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_normalizedTime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_speed_m2787468711 (AnimationState_t1108360407 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_speed_m2787468711_ftn) (AnimationState_t1108360407 *, float);
	static AnimationState_set_speed_m2787468711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_speed_m2787468711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_length()
extern "C" IL2CPP_METHOD_ATTR float AnimationState_get_length_m2177740858 (AnimationState_t1108360407 * __this, const RuntimeMethod* method)
{
	typedef float (*AnimationState_get_length_m2177740858_ftn) (AnimationState_t1108360407 *);
	static AnimationState_get_length_m2177740858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_length_m2177740858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_length()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_layer(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_layer_m2184847998 (AnimationState_t1108360407 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_layer_m2184847998_ftn) (AnimationState_t1108360407 *, int32_t);
	static AnimationState_set_layer_m2184847998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_layer_m2184847998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_AddMixingTransform_m3342983193 (AnimationState_t1108360407 * __this, Transform_t3600365921 * ___mix0, bool ___recursive1, const RuntimeMethod* method)
{
	typedef void (*AnimationState_AddMixingTransform_m3342983193_ftn) (AnimationState_t1108360407 *, Transform_t3600365921 *, bool);
	static AnimationState_AddMixingTransform_m3342983193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_AddMixingTransform_m3342983193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___mix0, ___recursive1);
}
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_AddMixingTransform_m466153312 (AnimationState_t1108360407 * __this, Transform_t3600365921 * ___mix0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		Transform_t3600365921 * L_0 = ___mix0;
		bool L_1 = V_0;
		AnimationState_AddMixingTransform_m3342983193(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.AnimationState::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* AnimationState_get_name_m2426172525 (AnimationState_t1108360407 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*AnimationState_get_name_m2426172525_ftn) (AnimationState_t1108360407 *);
	static AnimationState_get_name_m2426172525_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_name_m2426172525_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_name()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
extern "C" IL2CPP_METHOD_ATTR void AnimationState_set_blendMode_m3481502353 (AnimationState_t1108360407 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*AnimationState_set_blendMode_m3481502353_ftn) (AnimationState_t1108360407 *, int32_t);
	static AnimationState_set_blendMode_m3481502353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_blendMode_m3481502353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationClipPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationClipPlayable__ctor_m2913235724 (AnimationClipPlayable_t3189118652 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationClipPlayable__ctor_m2913235724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t3189118652_m4201087276((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationClipPlayable_t3189118652_m4201087276_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral2566125115, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationClipPlayable__ctor_m2913235724_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t1095853803  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationClipPlayable__ctor_m2913235724_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	AnimationClipPlayable__ctor_m2913235724(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animations.AnimationClipPlayable UnityEngine.Animations.AnimationClipPlayable::Create(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
extern "C" IL2CPP_METHOD_ATTR AnimationClipPlayable_t3189118652  AnimationClipPlayable_Create_m2827953749 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, AnimationClip_t2318505987 * ___clip1, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationClipPlayable_t3189118652  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t3515989261  L_0 = ___graph0;
		AnimationClip_t2318505987 * L_1 = ___clip1;
		PlayableHandle_t1095853803  L_2 = AnimationClipPlayable_CreateHandle_m999922984(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PlayableHandle_t1095853803  L_3 = V_0;
		AnimationClipPlayable_t3189118652  L_4;
		memset(&L_4, 0, sizeof(L_4));
		AnimationClipPlayable__ctor_m2913235724((&L_4), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		AnimationClipPlayable_t3189118652  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationClipPlayable_CreateHandle_m999922984 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, AnimationClip_t2318505987 * ___clip1, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1095853803  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t3515989261  L_1 = ___graph0;
		AnimationClip_t2318505987 * L_2 = ___clip1;
		bool L_3 = AnimationClipPlayable_CreateHandleInternal_m3627497527(NULL /*static, unused*/, L_1, L_2, (PlayableHandle_t1095853803 *)(&V_0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		PlayableHandle_t1095853803  L_4 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0027;
	}

IL_0020:
	{
		PlayableHandle_t1095853803  L_5 = V_0;
		V_1 = L_5;
		goto IL_0027;
	}

IL_0027:
	{
		PlayableHandle_t1095853803  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationClipPlayable_GetHandle_m3441351653 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimationClipPlayable_GetHandle_m3441351653_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	return AnimationClipPlayable_GetHandle_m3441351653(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationClipPlayable::op_Implicit(UnityEngine.Animations.AnimationClipPlayable)
extern "C" IL2CPP_METHOD_ATTR Playable_t459825607  AnimationClipPlayable_op_Implicit_m3592598016 (RuntimeObject * __this /* static, unused */, AnimationClipPlayable_t3189118652  ___playable0, const RuntimeMethod* method)
{
	Playable_t459825607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = AnimationClipPlayable_GetHandle_m3441351653((AnimationClipPlayable_t3189118652 *)(&___playable0), /*hidden argument*/NULL);
		Playable_t459825607  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m3175303195((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t459825607  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::Equals(UnityEngine.Animations.AnimationClipPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_Equals_m1031707451 (AnimationClipPlayable_t3189118652 * __this, AnimationClipPlayable_t3189118652  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimationClipPlayable_GetHandle_m3441351653((AnimationClipPlayable_t3189118652 *)__this, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = AnimationClipPlayable_GetHandle_m3441351653((AnimationClipPlayable_t3189118652 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m3344837515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationClipPlayable_Equals_m1031707451_AdjustorThunk (RuntimeObject * __this, AnimationClipPlayable_t3189118652  ___other0, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	return AnimationClipPlayable_Equals_m1031707451(_thisAdjusted, ___other0, method);
}
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClip()
extern "C" IL2CPP_METHOD_ATTR AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClip_m824553280 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method)
{
	AnimationClip_t2318505987 * V_0 = NULL;
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		AnimationClip_t2318505987 * L_1 = AnimationClipPlayable_GetAnimationClipInternal_m2826345587(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AnimationClip_t2318505987 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClip_m824553280_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	return AnimationClipPlayable_GetAnimationClip_m824553280(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIK()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyFootIK_m1549118896 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AnimationClipPlayable_GetApplyFootIKInternal_m1617064962(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AnimationClipPlayable_GetApplyFootIK_m1549118896_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	return AnimationClipPlayable_GetApplyFootIK_m1549118896(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyPlayableIK()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyPlayableIK_m3535159484 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AnimationClipPlayable_GetApplyPlayableIKInternal_m2641340007(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AnimationClipPlayable_GetApplyPlayableIK_m3535159484_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	return AnimationClipPlayable_GetApplyPlayableIK_m3535159484(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffset()
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetRemoveStartOffset_m1243433047 (AnimationClipPlayable_t3189118652 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = AnimationClipPlayable_GetRemoveStartOffsetInternal_m1929113867(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffset_m1243433047_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	return AnimationClipPlayable_GetRemoveStartOffset_m1243433047(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffset(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationClipPlayable_SetRemoveStartOffset_m149988838 (AnimationClipPlayable_t3189118652 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = ___value0;
		AnimationClipPlayable_SetRemoveStartOffsetInternal_m332890011(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimationClipPlayable_SetRemoveStartOffset_m149988838_AdjustorThunk (RuntimeObject * __this, bool ___value0, const RuntimeMethod* method)
{
	AnimationClipPlayable_t3189118652 * _thisAdjusted = reinterpret_cast<AnimationClipPlayable_t3189118652 *>(__this + 1);
	AnimationClipPlayable_SetRemoveStartOffset_m149988838(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_CreateHandleInternal_m3627497527 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, AnimationClip_t2318505987 * ___clip1, PlayableHandle_t1095853803 * ___handle2, const RuntimeMethod* method)
{
	{
		AnimationClip_t2318505987 * L_0 = ___clip1;
		PlayableHandle_t1095853803 * L_1 = ___handle2;
		bool L_2 = AnimationClipPlayable_CreateHandleInternal_Injected_m2693506418(NULL /*static, unused*/, (PlayableGraph_t3515989261 *)(&___graph0), L_0, (PlayableHandle_t1095853803 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AnimationClip UnityEngine.Animations.AnimationClipPlayable::GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClipInternal_m2826345587 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method)
{
	typedef AnimationClip_t2318505987 * (*AnimationClipPlayable_GetAnimationClipInternal_m2826345587_ftn) (PlayableHandle_t1095853803 *);
	static AnimationClipPlayable_GetAnimationClipInternal_m2826345587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_GetAnimationClipInternal_m2826345587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::GetAnimationClipInternal(UnityEngine.Playables.PlayableHandle&)");
	AnimationClip_t2318505987 * retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyFootIKInternal_m1617064962 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_GetApplyFootIKInternal_m1617064962_ftn) (PlayableHandle_t1095853803 *);
	static AnimationClipPlayable_GetApplyFootIKInternal_m1617064962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_GetApplyFootIKInternal_m1617064962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::GetApplyFootIKInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetApplyPlayableIKInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetApplyPlayableIKInternal_m2641340007 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_GetApplyPlayableIKInternal_m2641340007_ftn) (PlayableHandle_t1095853803 *);
	static AnimationClipPlayable_GetApplyPlayableIKInternal_m2641340007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_GetApplyPlayableIKInternal_m2641340007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::GetApplyPlayableIKInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_GetRemoveStartOffsetInternal_m1929113867 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_GetRemoveStartOffsetInternal_m1929113867_ftn) (PlayableHandle_t1095853803 *);
	static AnimationClipPlayable_GetRemoveStartOffsetInternal_m1929113867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_GetRemoveStartOffsetInternal_m1929113867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::GetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnimationClipPlayable_SetRemoveStartOffsetInternal_m332890011 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*AnimationClipPlayable_SetRemoveStartOffsetInternal_m332890011_ftn) (PlayableHandle_t1095853803 *, bool);
	static AnimationClipPlayable_SetRemoveStartOffsetInternal_m332890011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_SetRemoveStartOffsetInternal_m332890011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::SetRemoveStartOffsetInternal(UnityEngine.Playables.PlayableHandle&,System.Boolean)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// System.Boolean UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationClipPlayable_CreateHandleInternal_Injected_m2693506418 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, AnimationClip_t2318505987 * ___clip1, PlayableHandle_t1095853803 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*AnimationClipPlayable_CreateHandleInternal_Injected_m2693506418_ftn) (PlayableGraph_t3515989261 *, AnimationClip_t2318505987 *, PlayableHandle_t1095853803 *);
	static AnimationClipPlayable_CreateHandleInternal_Injected_m2693506418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_CreateHandleInternal_Injected_m2693506418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationClipPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.AnimationClip,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___clip1, ___handle2);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable__ctor_m860721801 (AnimationLayerMixerPlayable_t3631223897 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable__ctor_m860721801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t3631223897_m201603007((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationLayerMixerPlayable_t3631223897_m201603007_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral1913092913, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationLayerMixerPlayable__ctor_m860721801_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t1095853803  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationLayerMixerPlayable__ctor_m860721801_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t3631223897 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t3631223897 *>(__this + 1);
	AnimationLayerMixerPlayable__ctor_m860721801(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::get_Null()
extern "C" IL2CPP_METHOD_ATTR AnimationLayerMixerPlayable_t3631223897  AnimationLayerMixerPlayable_get_Null_m1596956097 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_get_Null_m1596956097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimationLayerMixerPlayable_t3631223897  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var);
		AnimationLayerMixerPlayable_t3631223897  L_0 = ((AnimationLayerMixerPlayable_t3631223897_StaticFields*)il2cpp_codegen_static_fields_for(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var))->get_m_NullPlayable_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		AnimationLayerMixerPlayable_t3631223897  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::Create(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimationLayerMixerPlayable_t3631223897  AnimationLayerMixerPlayable_Create_m286498210 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_Create_m286498210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationLayerMixerPlayable_t3631223897  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t3515989261  L_0 = ___graph0;
		int32_t L_1 = ___inputCount1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var);
		PlayableHandle_t1095853803  L_2 = AnimationLayerMixerPlayable_CreateHandle_m2143407042(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		PlayableHandle_t1095853803  L_3 = V_0;
		AnimationLayerMixerPlayable_t3631223897  L_4;
		memset(&L_4, 0, sizeof(L_4));
		AnimationLayerMixerPlayable__ctor_m860721801((&L_4), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		AnimationLayerMixerPlayable_t3631223897  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationLayerMixerPlayable_CreateHandle_m2143407042 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_CreateHandle_m2143407042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1095853803  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t3515989261  L_1 = ___graph0;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var);
		bool L_2 = AnimationLayerMixerPlayable_CreateHandleInternal_m2872369648(NULL /*static, unused*/, L_1, (PlayableHandle_t1095853803 *)(&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		PlayableHandle_t1095853803  L_3 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_002e;
	}

IL_001f:
	{
		int32_t L_4 = ___inputCount1;
		PlayableHandle_SetInputCount_m2596808857((PlayableHandle_t1095853803 *)(&V_0), L_4, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_5 = V_0;
		V_1 = L_5;
		goto IL_002e;
	}

IL_002e:
	{
		PlayableHandle_t1095853803  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationLayerMixerPlayable_GetHandle_m3854437798 (AnimationLayerMixerPlayable_t3631223897 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimationLayerMixerPlayable_GetHandle_m3854437798_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t3631223897 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t3631223897 *>(__this + 1);
	return AnimationLayerMixerPlayable_GetHandle_m3854437798(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationLayerMixerPlayable::op_Implicit(UnityEngine.Animations.AnimationLayerMixerPlayable)
extern "C" IL2CPP_METHOD_ATTR Playable_t459825607  AnimationLayerMixerPlayable_op_Implicit_m264040223 (RuntimeObject * __this /* static, unused */, AnimationLayerMixerPlayable_t3631223897  ___playable0, const RuntimeMethod* method)
{
	Playable_t459825607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = AnimationLayerMixerPlayable_GetHandle_m3854437798((AnimationLayerMixerPlayable_t3631223897 *)(&___playable0), /*hidden argument*/NULL);
		Playable_t459825607  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m3175303195((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t459825607  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::op_Explicit(UnityEngine.Playables.Playable)
extern "C" IL2CPP_METHOD_ATTR AnimationLayerMixerPlayable_t3631223897  AnimationLayerMixerPlayable_op_Explicit_m2722087125 (RuntimeObject * __this /* static, unused */, Playable_t459825607  ___playable0, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t3631223897  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = Playable_GetHandle_m98909670((Playable_t459825607 *)(&___playable0), /*hidden argument*/NULL);
		AnimationLayerMixerPlayable_t3631223897  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationLayerMixerPlayable__ctor_m860721801((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		AnimationLayerMixerPlayable_t3631223897  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::Equals(UnityEngine.Animations.AnimationLayerMixerPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationLayerMixerPlayable_Equals_m4271416323 (AnimationLayerMixerPlayable_t3631223897 * __this, AnimationLayerMixerPlayable_t3631223897  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimationLayerMixerPlayable_GetHandle_m3854437798((AnimationLayerMixerPlayable_t3631223897 *)__this, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = AnimationLayerMixerPlayable_GetHandle_m3854437798((AnimationLayerMixerPlayable_t3631223897 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m3344837515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationLayerMixerPlayable_Equals_m4271416323_AdjustorThunk (RuntimeObject * __this, AnimationLayerMixerPlayable_t3631223897  ___other0, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t3631223897 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t3631223897 *>(__this + 1);
	return AnimationLayerMixerPlayable_Equals_m4271416323(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMask(System.UInt32,UnityEngine.AvatarMask)
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514 (AnimationLayerMixerPlayable_t3631223897 * __this, uint32_t ___layerIndex0, AvatarMask_t1182482518 * ___mask1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint32_t L_0 = ___layerIndex0;
		PlayableHandle_t1095853803 * L_1 = __this->get_address_of_m_Handle_0();
		int32_t L_2 = PlayableHandle_GetInputCount_m888565651((PlayableHandle_t1095853803 *)L_1, /*hidden argument*/NULL);
		if ((((int64_t)(((int64_t)((uint64_t)L_0)))) < ((int64_t)(((int64_t)((int64_t)L_2))))))
		{
			goto IL_0041;
		}
	}
	{
		uint32_t L_3 = ___layerIndex0;
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_t2560061978_il2cpp_TypeInfo_var, &L_4);
		PlayableHandle_t1095853803 * L_6 = __this->get_address_of_m_Handle_0();
		int32_t L_7 = PlayableHandle_GetInputCount_m888565651((PlayableHandle_t1095853803 *)L_6, /*hidden argument*/NULL);
		int32_t L_8 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1));
		RuntimeObject * L_9 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral795769502, L_5, L_9, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t777629997 * L_11 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m282481429(L_11, _stringLiteral4251651911, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, NULL, AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514_RuntimeMethod_var);
	}

IL_0041:
	{
		AvatarMask_t1182482518 * L_12 = ___mask1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_12, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0058;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_14 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_14, _stringLiteral455380590, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, NULL, AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514_RuntimeMethod_var);
	}

IL_0058:
	{
		PlayableHandle_t1095853803 * L_15 = __this->get_address_of_m_Handle_0();
		uint32_t L_16 = ___layerIndex0;
		AvatarMask_t1182482518 * L_17 = ___mask1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var);
		AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m3149220962(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_15, L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514_AdjustorThunk (RuntimeObject * __this, uint32_t ___layerIndex0, AvatarMask_t1182482518 * ___mask1, const RuntimeMethod* method)
{
	AnimationLayerMixerPlayable_t3631223897 * _thisAdjusted = reinterpret_cast<AnimationLayerMixerPlayable_t3631223897 *>(__this + 1);
	AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMask_m2316966514(_thisAdjusted, ___layerIndex0, ___mask1, method);
}
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationLayerMixerPlayable_CreateHandleInternal_m2872369648 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable_CreateHandleInternal_m2872369648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803 * L_0 = ___handle1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var);
		bool L_1 = AnimationLayerMixerPlayable_CreateHandleInternal_Injected_m1824026698(NULL /*static, unused*/, (PlayableGraph_t3515989261 *)(&___graph0), (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m3149220962 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, uint32_t ___layerIndex1, AvatarMask_t1182482518 * ___mask2, const RuntimeMethod* method)
{
	typedef void (*AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m3149220962_ftn) (PlayableHandle_t1095853803 *, uint32_t, AvatarMask_t1182482518 *);
	static AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m3149220962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationLayerMixerPlayable_SetLayerMaskFromAvatarMaskInternal_m3149220962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationLayerMixerPlayable::SetLayerMaskFromAvatarMaskInternal(UnityEngine.Playables.PlayableHandle&,System.UInt32,UnityEngine.AvatarMask)");
	_il2cpp_icall_func(___handle0, ___layerIndex1, ___mask2);
}
// System.Void UnityEngine.Animations.AnimationLayerMixerPlayable::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationLayerMixerPlayable__cctor_m4086299972 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationLayerMixerPlayable__cctor_m4086299972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationLayerMixerPlayable_t3631223897  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationLayerMixerPlayable__ctor_m860721801((&L_1), L_0, /*hidden argument*/NULL);
		((AnimationLayerMixerPlayable_t3631223897_StaticFields*)il2cpp_codegen_static_fields_for(AnimationLayerMixerPlayable_t3631223897_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Boolean UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationLayerMixerPlayable_CreateHandleInternal_Injected_m1824026698 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method)
{
	typedef bool (*AnimationLayerMixerPlayable_CreateHandleInternal_Injected_m1824026698_ftn) (PlayableGraph_t3515989261 *, PlayableHandle_t1095853803 *);
	static AnimationLayerMixerPlayable_CreateHandleInternal_Injected_m1824026698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationLayerMixerPlayable_CreateHandleInternal_Injected_m1824026698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationLayerMixerPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___handle1);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationMixerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationMixerPlayable__ctor_m848068859 (AnimationMixerPlayable_t821371386 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationMixerPlayable__ctor_m848068859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t821371386_m4108826000((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationMixerPlayable_t821371386_m4108826000_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral3049507974, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationMixerPlayable__ctor_m848068859_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t1095853803  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationMixerPlayable__ctor_m848068859_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimationMixerPlayable_t821371386 * _thisAdjusted = reinterpret_cast<AnimationMixerPlayable_t821371386 *>(__this + 1);
	AnimationMixerPlayable__ctor_m848068859(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::Create(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR AnimationMixerPlayable_t821371386  AnimationMixerPlayable_Create_m3089667912 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationMixerPlayable_t821371386  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t3515989261  L_0 = ___graph0;
		int32_t L_1 = ___inputCount1;
		bool L_2 = ___normalizeWeights2;
		PlayableHandle_t1095853803  L_3 = AnimationMixerPlayable_CreateHandle_m3482410404(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PlayableHandle_t1095853803  L_4 = V_0;
		AnimationMixerPlayable_t821371386  L_5;
		memset(&L_5, 0, sizeof(L_5));
		AnimationMixerPlayable__ctor_m848068859((&L_5), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		AnimationMixerPlayable_t821371386  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMixerPlayable_CreateHandle_m3482410404 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1095853803  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t3515989261  L_1 = ___graph0;
		int32_t L_2 = ___inputCount1;
		bool L_3 = ___normalizeWeights2;
		bool L_4 = AnimationMixerPlayable_CreateHandleInternal_m3818145798(NULL /*static, unused*/, L_1, L_2, L_3, (PlayableHandle_t1095853803 *)(&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		PlayableHandle_t1095853803  L_5 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0030;
	}

IL_0021:
	{
		int32_t L_6 = ___inputCount1;
		PlayableHandle_SetInputCount_m2596808857((PlayableHandle_t1095853803 *)(&V_0), L_6, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_7 = V_0;
		V_1 = L_7;
		goto IL_0030;
	}

IL_0030:
	{
		PlayableHandle_t1095853803  L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMixerPlayable_GetHandle_m2198358075 (AnimationMixerPlayable_t821371386 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimationMixerPlayable_GetHandle_m2198358075_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationMixerPlayable_t821371386 * _thisAdjusted = reinterpret_cast<AnimationMixerPlayable_t821371386 *>(__this + 1);
	return AnimationMixerPlayable_GetHandle_m2198358075(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationMixerPlayable::op_Implicit(UnityEngine.Animations.AnimationMixerPlayable)
extern "C" IL2CPP_METHOD_ATTR Playable_t459825607  AnimationMixerPlayable_op_Implicit_m3380008075 (RuntimeObject * __this /* static, unused */, AnimationMixerPlayable_t821371386  ___playable0, const RuntimeMethod* method)
{
	Playable_t459825607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = AnimationMixerPlayable_GetHandle_m2198358075((AnimationMixerPlayable_t821371386 *)(&___playable0), /*hidden argument*/NULL);
		Playable_t459825607  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m3175303195((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t459825607  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::Equals(UnityEngine.Animations.AnimationMixerPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMixerPlayable_Equals_m3971478909 (AnimationMixerPlayable_t821371386 * __this, AnimationMixerPlayable_t821371386  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimationMixerPlayable_GetHandle_m2198358075((AnimationMixerPlayable_t821371386 *)__this, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = AnimationMixerPlayable_GetHandle_m2198358075((AnimationMixerPlayable_t821371386 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m3344837515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationMixerPlayable_Equals_m3971478909_AdjustorThunk (RuntimeObject * __this, AnimationMixerPlayable_t821371386  ___other0, const RuntimeMethod* method)
{
	AnimationMixerPlayable_t821371386 * _thisAdjusted = reinterpret_cast<AnimationMixerPlayable_t821371386 *>(__this + 1);
	return AnimationMixerPlayable_Equals_m3971478909(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMixerPlayable_CreateHandleInternal_m3818145798 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___inputCount1;
		bool L_1 = ___normalizeWeights2;
		PlayableHandle_t1095853803 * L_2 = ___handle3;
		bool L_3 = AnimationMixerPlayable_CreateHandleInternal_Injected_m3885529074(NULL /*static, unused*/, (PlayableGraph_t3515989261 *)(&___graph0), L_0, L_1, (PlayableHandle_t1095853803 *)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMixerPlayable_CreateHandleInternal_Injected_m3885529074 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method)
{
	typedef bool (*AnimationMixerPlayable_CreateHandleInternal_Injected_m3885529074_ftn) (PlayableGraph_t3515989261 *, int32_t, bool, PlayableHandle_t1095853803 *);
	static AnimationMixerPlayable_CreateHandleInternal_Injected_m3885529074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationMixerPlayable_CreateHandleInternal_Injected_m3885529074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationMixerPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,System.Int32,System.Boolean,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___inputCount1, ___normalizeWeights2, ___handle3);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationMotionXToDeltaPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationMotionXToDeltaPlayable__ctor_m1725027713 (AnimationMotionXToDeltaPlayable_t272231551 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationMotionXToDeltaPlayable__ctor_m1725027713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationMotionXToDeltaPlayable_t272231551_m1123153091((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationMotionXToDeltaPlayable_t272231551_m1123153091_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral3707822210, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationMotionXToDeltaPlayable__ctor_m1725027713_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t1095853803  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationMotionXToDeltaPlayable__ctor_m1725027713_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimationMotionXToDeltaPlayable_t272231551 * _thisAdjusted = reinterpret_cast<AnimationMotionXToDeltaPlayable_t272231551 *>(__this + 1);
	AnimationMotionXToDeltaPlayable__ctor_m1725027713(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animations.AnimationMotionXToDeltaPlayable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::Create(UnityEngine.Playables.PlayableGraph)
extern "C" IL2CPP_METHOD_ATTR AnimationMotionXToDeltaPlayable_t272231551  AnimationMotionXToDeltaPlayable_Create_m2837395996 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationMotionXToDeltaPlayable_t272231551  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t3515989261  L_0 = ___graph0;
		PlayableHandle_t1095853803  L_1 = AnimationMotionXToDeltaPlayable_CreateHandle_m3954964130(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PlayableHandle_t1095853803  L_2 = V_0;
		AnimationMotionXToDeltaPlayable_t272231551  L_3;
		memset(&L_3, 0, sizeof(L_3));
		AnimationMotionXToDeltaPlayable__ctor_m1725027713((&L_3), L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		AnimationMotionXToDeltaPlayable_t272231551  L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMotionXToDeltaPlayable_CreateHandle_m3954964130 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1095853803  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t3515989261  L_1 = ___graph0;
		bool L_2 = AnimationMotionXToDeltaPlayable_CreateHandleInternal_m3250489882(NULL /*static, unused*/, L_1, (PlayableHandle_t1095853803 *)(&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		PlayableHandle_t1095853803  L_3 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_002e;
	}

IL_001f:
	{
		PlayableHandle_SetInputCount_m2596808857((PlayableHandle_t1095853803 *)(&V_0), 1, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_4 = V_0;
		V_1 = L_4;
		goto IL_002e;
	}

IL_002e:
	{
		PlayableHandle_t1095853803  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMotionXToDeltaPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationMotionXToDeltaPlayable_GetHandle_m1319190040 (AnimationMotionXToDeltaPlayable_t272231551 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimationMotionXToDeltaPlayable_GetHandle_m1319190040_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationMotionXToDeltaPlayable_t272231551 * _thisAdjusted = reinterpret_cast<AnimationMotionXToDeltaPlayable_t272231551 *>(__this + 1);
	return AnimationMotionXToDeltaPlayable_GetHandle_m1319190040(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationMotionXToDeltaPlayable::op_Implicit(UnityEngine.Animations.AnimationMotionXToDeltaPlayable)
extern "C" IL2CPP_METHOD_ATTR Playable_t459825607  AnimationMotionXToDeltaPlayable_op_Implicit_m266601885 (RuntimeObject * __this /* static, unused */, AnimationMotionXToDeltaPlayable_t272231551  ___playable0, const RuntimeMethod* method)
{
	Playable_t459825607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = AnimationMotionXToDeltaPlayable_GetHandle_m1319190040((AnimationMotionXToDeltaPlayable_t272231551 *)(&___playable0), /*hidden argument*/NULL);
		Playable_t459825607  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m3175303195((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t459825607  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationMotionXToDeltaPlayable::Equals(UnityEngine.Animations.AnimationMotionXToDeltaPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMotionXToDeltaPlayable_Equals_m3896842955 (AnimationMotionXToDeltaPlayable_t272231551 * __this, AnimationMotionXToDeltaPlayable_t272231551  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimationMotionXToDeltaPlayable_GetHandle_m1319190040((AnimationMotionXToDeltaPlayable_t272231551 *)__this, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = AnimationMotionXToDeltaPlayable_GetHandle_m1319190040((AnimationMotionXToDeltaPlayable_t272231551 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m3344837515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationMotionXToDeltaPlayable_Equals_m3896842955_AdjustorThunk (RuntimeObject * __this, AnimationMotionXToDeltaPlayable_t272231551  ___other0, const RuntimeMethod* method)
{
	AnimationMotionXToDeltaPlayable_t272231551 * _thisAdjusted = reinterpret_cast<AnimationMotionXToDeltaPlayable_t272231551 *>(__this + 1);
	return AnimationMotionXToDeltaPlayable_Equals_m3896842955(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMotionXToDeltaPlayable_CreateHandleInternal_m3250489882 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1095853803 * L_0 = ___handle1;
		bool L_1 = AnimationMotionXToDeltaPlayable_CreateHandleInternal_Injected_m1129764157(NULL /*static, unused*/, (PlayableGraph_t3515989261 *)(&___graph0), (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationMotionXToDeltaPlayable_CreateHandleInternal_Injected_m1129764157 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, PlayableHandle_t1095853803 * ___handle1, const RuntimeMethod* method)
{
	typedef bool (*AnimationMotionXToDeltaPlayable_CreateHandleInternal_Injected_m1129764157_ftn) (PlayableGraph_t3515989261 *, PlayableHandle_t1095853803 *);
	static AnimationMotionXToDeltaPlayable_CreateHandleInternal_Injected_m1129764157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationMotionXToDeltaPlayable_CreateHandleInternal_Injected_m1129764157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationMotionXToDeltaPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___handle1);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable__ctor_m577157274 (AnimationOffsetPlayable_t2887420414 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable__ctor_m577157274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t2887420414_m2033286094((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationOffsetPlayable_t2887420414_m2033286094_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral783105886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationOffsetPlayable__ctor_m577157274_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t1095853803  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationOffsetPlayable__ctor_m577157274_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t2887420414 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t2887420414 *>(__this + 1);
	AnimationOffsetPlayable__ctor_m577157274(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animations.AnimationOffsetPlayable UnityEngine.Animations.AnimationOffsetPlayable::Create(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimationOffsetPlayable_t2887420414  AnimationOffsetPlayable_Create_m4216231881 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, int32_t ___inputCount3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_Create_m4216231881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationOffsetPlayable_t2887420414  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableGraph_t3515989261  L_0 = ___graph0;
		Vector3_t3722313464  L_1 = ___position1;
		Quaternion_t2301928331  L_2 = ___rotation2;
		int32_t L_3 = ___inputCount3;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		PlayableHandle_t1095853803  L_4 = AnimationOffsetPlayable_CreateHandle_m2427170463(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		PlayableHandle_t1095853803  L_5 = V_0;
		AnimationOffsetPlayable_t2887420414  L_6;
		memset(&L_6, 0, sizeof(L_6));
		AnimationOffsetPlayable__ctor_m577157274((&L_6), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		AnimationOffsetPlayable_t2887420414  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::CreateHandle(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationOffsetPlayable_CreateHandle_m2427170463 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, int32_t ___inputCount3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_CreateHandle_m2427170463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PlayableHandle_t1095853803  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		PlayableGraph_t3515989261  L_1 = ___graph0;
		Vector3_t3722313464  L_2 = ___position1;
		Quaternion_t2301928331  L_3 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		bool L_4 = AnimationOffsetPlayable_CreateHandleInternal_m904339466(NULL /*static, unused*/, L_1, L_2, L_3, (PlayableHandle_t1095853803 *)(&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		PlayableHandle_t1095853803  L_5 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0030;
	}

IL_0021:
	{
		int32_t L_6 = ___inputCount3;
		PlayableHandle_SetInputCount_m2596808857((PlayableHandle_t1095853803 *)(&V_0), L_6, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_7 = V_0;
		V_1 = L_7;
		goto IL_0030;
	}

IL_0030:
	{
		PlayableHandle_t1095853803  L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationOffsetPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationOffsetPlayable_GetHandle_m449079993 (AnimationOffsetPlayable_t2887420414 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimationOffsetPlayable_GetHandle_m449079993_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t2887420414 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t2887420414 *>(__this + 1);
	return AnimationOffsetPlayable_GetHandle_m449079993(_thisAdjusted, method);
}
// UnityEngine.Playables.Playable UnityEngine.Animations.AnimationOffsetPlayable::op_Implicit(UnityEngine.Animations.AnimationOffsetPlayable)
extern "C" IL2CPP_METHOD_ATTR Playable_t459825607  AnimationOffsetPlayable_op_Implicit_m1915497722 (RuntimeObject * __this /* static, unused */, AnimationOffsetPlayable_t2887420414  ___playable0, const RuntimeMethod* method)
{
	Playable_t459825607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = AnimationOffsetPlayable_GetHandle_m449079993((AnimationOffsetPlayable_t2887420414 *)(&___playable0), /*hidden argument*/NULL);
		Playable_t459825607  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Playable__ctor_m3175303195((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		Playable_t459825607  L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::Equals(UnityEngine.Animations.AnimationOffsetPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationOffsetPlayable_Equals_m2902253045 (AnimationOffsetPlayable_t2887420414 * __this, AnimationOffsetPlayable_t2887420414  ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_Equals_m2902253045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimationOffsetPlayable_GetHandle_m449079993((AnimationOffsetPlayable_t2887420414 *)(&___other0), /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = L_0;
		RuntimeObject * L_2 = Box(PlayableHandle_t1095853803_il2cpp_TypeInfo_var, &L_1);
		RuntimeObject * L_3 = Box(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var, __this);
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_3, L_2);
		*__this = *(AnimationOffsetPlayable_t2887420414 *)UnBox(L_3);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
extern "C"  bool AnimationOffsetPlayable_Equals_m2902253045_AdjustorThunk (RuntimeObject * __this, AnimationOffsetPlayable_t2887420414  ___other0, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t2887420414 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t2887420414 *>(__this + 1);
	return AnimationOffsetPlayable_Equals_m2902253045(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  AnimationOffsetPlayable_GetPosition_m3423307712 (AnimationOffsetPlayable_t2887420414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetPosition_m3423307712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = AnimationOffsetPlayable_GetPositionInternal_m3429692954(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3722313464  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector3_t3722313464  AnimationOffsetPlayable_GetPosition_m3423307712_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t2887420414 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t2887420414 *>(__this + 1);
	return AnimationOffsetPlayable_GetPosition_m3423307712(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  AnimationOffsetPlayable_GetRotation_m259451460 (AnimationOffsetPlayable_t2887420414 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetRotation_m259451460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_1 = AnimationOffsetPlayable_GetRotationInternal_m1801279867(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Quaternion_t2301928331  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Quaternion_t2301928331  AnimationOffsetPlayable_GetRotation_m259451460_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationOffsetPlayable_t2887420414 * _thisAdjusted = reinterpret_cast<AnimationOffsetPlayable_t2887420414 *>(__this + 1);
	return AnimationOffsetPlayable_GetRotation_m259451460(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal(UnityEngine.Playables.PlayableGraph,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationOffsetPlayable_CreateHandleInternal_m904339466 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_CreateHandleInternal_m904339466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803 * L_0 = ___handle3;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		bool L_1 = AnimationOffsetPlayable_CreateHandleInternal_Injected_m3452138878(NULL /*static, unused*/, (PlayableGraph_t3515989261 *)(&___graph0), (Vector3_t3722313464 *)(&___position1), (Quaternion_t2301928331 *)(&___rotation2), (PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  AnimationOffsetPlayable_GetPositionInternal_m3429692954 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetPositionInternal_m3429692954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803 * L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		AnimationOffsetPlayable_GetPositionInternal_Injected_m215513770(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal(UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  AnimationOffsetPlayable_GetRotationInternal_m1801279867 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable_GetRotationInternal_m1801279867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803 * L_0 = ___handle0;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var);
		AnimationOffsetPlayable_GetRotationInternal_Injected_m1881104651(NULL /*static, unused*/, (PlayableHandle_t1095853803 *)L_0, (Quaternion_t2301928331 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable__cctor_m1029460816 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationOffsetPlayable__cctor_m1029460816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationOffsetPlayable_t2887420414  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationOffsetPlayable__ctor_m577157274((&L_1), L_0, /*hidden argument*/NULL);
		((AnimationOffsetPlayable_t2887420414_StaticFields*)il2cpp_codegen_static_fields_for(AnimationOffsetPlayable_t2887420414_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
// System.Boolean UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Playables.PlayableHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationOffsetPlayable_CreateHandleInternal_Injected_m3452138878 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, Vector3_t3722313464 * ___position1, Quaternion_t2301928331 * ___rotation2, PlayableHandle_t1095853803 * ___handle3, const RuntimeMethod* method)
{
	typedef bool (*AnimationOffsetPlayable_CreateHandleInternal_Injected_m3452138878_ftn) (PlayableGraph_t3515989261 *, Vector3_t3722313464 *, Quaternion_t2301928331 *, PlayableHandle_t1095853803 *);
	static AnimationOffsetPlayable_CreateHandleInternal_Injected_m3452138878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationOffsetPlayable_CreateHandleInternal_Injected_m3452138878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationOffsetPlayable::CreateHandleInternal_Injected(UnityEngine.Playables.PlayableGraph&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Playables.PlayableHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___position1, ___rotation2, ___handle3);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal_Injected(UnityEngine.Playables.PlayableHandle&,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable_GetPositionInternal_Injected_m215513770 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, Vector3_t3722313464 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*AnimationOffsetPlayable_GetPositionInternal_Injected_m215513770_ftn) (PlayableHandle_t1095853803 *, Vector3_t3722313464 *);
	static AnimationOffsetPlayable_GetPositionInternal_Injected_m215513770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationOffsetPlayable_GetPositionInternal_Injected_m215513770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationOffsetPlayable::GetPositionInternal_Injected(UnityEngine.Playables.PlayableHandle&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___handle0, ___ret1);
}
// System.Void UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal_Injected(UnityEngine.Playables.PlayableHandle&,UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void AnimationOffsetPlayable_GetRotationInternal_Injected_m1881104651 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___handle0, Quaternion_t2301928331 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*AnimationOffsetPlayable_GetRotationInternal_Injected_m1881104651_ftn) (PlayableHandle_t1095853803 *, Quaternion_t2301928331 *);
	static AnimationOffsetPlayable_GetRotationInternal_Injected_m1881104651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationOffsetPlayable_GetRotationInternal_Injected_m1881104651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationOffsetPlayable::GetRotationInternal_Injected(UnityEngine.Playables.PlayableHandle&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___handle0, ___ret1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Playables.PlayableBinding UnityEngine.Animations.AnimationPlayableBinding::Create(System.String,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR PlayableBinding_t354260709  AnimationPlayableBinding_Create_m3080379501 (RuntimeObject * __this /* static, unused */, String_t* ___name0, Object_t631007953 * ___key1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayableBinding_Create_m3080379501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayableBinding_t354260709  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Type_t * G_B2_0 = NULL;
	Object_t631007953 * G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	Type_t * G_B1_0 = NULL;
	Object_t631007953 * G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	{
		String_t* L_0 = ___name0;
		Object_t631007953 * L_1 = ___key1;
		RuntimeTypeHandle_t3027515415  L_2 = { reinterpret_cast<intptr_t> (Animator_t434523843_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		CreateOutputMethod_t2301811773 * L_4 = ((AnimationPlayableBinding_t1369139502_StaticFields*)il2cpp_codegen_static_fields_for(AnimationPlayableBinding_t1369139502_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		G_B1_0 = L_3;
		G_B1_1 = L_1;
		G_B1_2 = L_0;
		if (L_4)
		{
			G_B2_0 = L_3;
			G_B2_1 = L_1;
			G_B2_2 = L_0;
			goto IL_0025;
		}
	}
	{
		intptr_t L_5 = (intptr_t)AnimationPlayableBinding_CreateAnimationOutput_m719933912_RuntimeMethod_var;
		CreateOutputMethod_t2301811773 * L_6 = (CreateOutputMethod_t2301811773 *)il2cpp_codegen_object_new(CreateOutputMethod_t2301811773_il2cpp_TypeInfo_var);
		CreateOutputMethod__ctor_m3884516706(L_6, NULL, L_5, /*hidden argument*/NULL);
		((AnimationPlayableBinding_t1369139502_StaticFields*)il2cpp_codegen_static_fields_for(AnimationPlayableBinding_t1369139502_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_0(L_6);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0025:
	{
		CreateOutputMethod_t2301811773 * L_7 = ((AnimationPlayableBinding_t1369139502_StaticFields*)il2cpp_codegen_static_fields_for(AnimationPlayableBinding_t1369139502_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		IL2CPP_RUNTIME_CLASS_INIT(PlayableBinding_t354260709_il2cpp_TypeInfo_var);
		PlayableBinding_t354260709  L_8 = PlayableBinding_CreateInternal_m1294529348(NULL /*static, unused*/, G_B2_2, G_B2_1, G_B2_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0035;
	}

IL_0035:
	{
		PlayableBinding_t354260709  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Playables.PlayableOutput UnityEngine.Animations.AnimationPlayableBinding::CreateAnimationOutput(UnityEngine.Playables.PlayableGraph,System.String)
extern "C" IL2CPP_METHOD_ATTR PlayableOutput_t3179894105  AnimationPlayableBinding_CreateAnimationOutput_m719933912 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, String_t* ___name1, const RuntimeMethod* method)
{
	PlayableOutput_t3179894105  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableGraph_t3515989261  L_0 = ___graph0;
		String_t* L_1 = ___name1;
		AnimationPlayableOutput_t1918618239  L_2 = AnimationPlayableOutput_Create_m1296576143(NULL /*static, unused*/, L_0, L_1, (Animator_t434523843 *)NULL, /*hidden argument*/NULL);
		PlayableOutput_t3179894105  L_3 = AnimationPlayableOutput_op_Implicit_m2440191068(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		PlayableOutput_t3179894105  L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationPlayableExtensions::SetAnimatedPropertiesInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.AnimationClip)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableExtensions_SetAnimatedPropertiesInternal_m526013803 (RuntimeObject * __this /* static, unused */, PlayableHandle_t1095853803 * ___playable0, AnimationClip_t2318505987 * ___animatedProperties1, const RuntimeMethod* method)
{
	typedef void (*AnimationPlayableExtensions_SetAnimatedPropertiesInternal_m526013803_ftn) (PlayableHandle_t1095853803 *, AnimationClip_t2318505987 *);
	static AnimationPlayableExtensions_SetAnimatedPropertiesInternal_m526013803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableExtensions_SetAnimatedPropertiesInternal_m526013803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableExtensions::SetAnimatedPropertiesInternal(UnityEngine.Playables.PlayableHandle&,UnityEngine.AnimationClip)");
	_il2cpp_icall_func(___playable0, ___animatedProperties1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)
extern "C" IL2CPP_METHOD_ATTR bool AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m28371369 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261 * ___graph0, String_t* ___name1, PlayableOutputHandle_t4208053793 * ___handle2, const RuntimeMethod* method)
{
	typedef bool (*AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m28371369_ftn) (PlayableGraph_t3515989261 *, String_t*, PlayableOutputHandle_t4208053793 *);
	static AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m28371369_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m28371369_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableGraphExtensions::InternalCreateAnimationOutput(UnityEngine.Playables.PlayableGraph&,System.String,UnityEngine.Playables.PlayableOutputHandle&)");
	bool retVal = _il2cpp_icall_func(___graph0, ___name1, ___handle2);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimationPlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableOutput__ctor_m457905557 (AnimationPlayableOutput_t1918618239 * __this, PlayableOutputHandle_t4208053793  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationPlayableOutput__ctor_m457905557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableOutputHandle_IsValid_m3926512399((PlayableOutputHandle_t4208053793 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t1918618239_m3383247624((PlayableOutputHandle_t4208053793 *)(&___handle0), /*hidden argument*/PlayableOutputHandle_IsPlayableOutputOfType_TisAnimationPlayableOutput_t1918618239_m3383247624_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral3596080318, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationPlayableOutput__ctor_m457905557_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableOutputHandle_t4208053793  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationPlayableOutput__ctor_m457905557_AdjustorThunk (RuntimeObject * __this, PlayableOutputHandle_t4208053793  ___handle0, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t1918618239 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t1918618239 *>(__this + 1);
	AnimationPlayableOutput__ctor_m457905557(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::Create(UnityEngine.Playables.PlayableGraph,System.String,UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR AnimationPlayableOutput_t1918618239  AnimationPlayableOutput_Create_m1296576143 (RuntimeObject * __this /* static, unused */, PlayableGraph_t3515989261  ___graph0, String_t* ___name1, Animator_t434523843 * ___target2, const RuntimeMethod* method)
{
	PlayableOutputHandle_t4208053793  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimationPlayableOutput_t1918618239  V_1;
	memset(&V_1, 0, sizeof(V_1));
	AnimationPlayableOutput_t1918618239  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		String_t* L_0 = ___name1;
		bool L_1 = AnimationPlayableGraphExtensions_InternalCreateAnimationOutput_m28371369(NULL /*static, unused*/, (PlayableGraph_t3515989261 *)(&___graph0), L_0, (PlayableOutputHandle_t4208053793 *)(&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		AnimationPlayableOutput_t1918618239  L_2 = AnimationPlayableOutput_get_Null_m927234140(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0032;
	}

IL_001b:
	{
		PlayableOutputHandle_t4208053793  L_3 = V_0;
		AnimationPlayableOutput__ctor_m457905557((AnimationPlayableOutput_t1918618239 *)(&V_2), L_3, /*hidden argument*/NULL);
		Animator_t434523843 * L_4 = ___target2;
		AnimationPlayableOutput_SetTarget_m4167975687((AnimationPlayableOutput_t1918618239 *)(&V_2), L_4, /*hidden argument*/NULL);
		AnimationPlayableOutput_t1918618239  L_5 = V_2;
		V_1 = L_5;
		goto IL_0032;
	}

IL_0032:
	{
		AnimationPlayableOutput_t1918618239  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::get_Null()
extern "C" IL2CPP_METHOD_ATTR AnimationPlayableOutput_t1918618239  AnimationPlayableOutput_get_Null_m927234140 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t1918618239  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t4208053793  L_0 = PlayableOutputHandle_get_Null_m1200584339(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationPlayableOutput_t1918618239  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationPlayableOutput__ctor_m457905557((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		AnimationPlayableOutput_t1918618239  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableOutputHandle_t4208053793  AnimationPlayableOutput_GetHandle_m644109061 (AnimationPlayableOutput_t1918618239 * __this, const RuntimeMethod* method)
{
	PlayableOutputHandle_t4208053793  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t4208053793  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableOutputHandle_t4208053793  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableOutputHandle_t4208053793  AnimationPlayableOutput_GetHandle_m644109061_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t1918618239 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t1918618239 *>(__this + 1);
	return AnimationPlayableOutput_GetHandle_m644109061(_thisAdjusted, method);
}
// UnityEngine.Playables.PlayableOutput UnityEngine.Animations.AnimationPlayableOutput::op_Implicit(UnityEngine.Animations.AnimationPlayableOutput)
extern "C" IL2CPP_METHOD_ATTR PlayableOutput_t3179894105  AnimationPlayableOutput_op_Implicit_m2440191068 (RuntimeObject * __this /* static, unused */, AnimationPlayableOutput_t1918618239  ___output0, const RuntimeMethod* method)
{
	PlayableOutput_t3179894105  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t4208053793  L_0 = AnimationPlayableOutput_GetHandle_m644109061((AnimationPlayableOutput_t1918618239 *)(&___output0), /*hidden argument*/NULL);
		PlayableOutput_t3179894105  L_1;
		memset(&L_1, 0, sizeof(L_1));
		PlayableOutput__ctor_m3330119218((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		PlayableOutput_t3179894105  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::op_Explicit(UnityEngine.Playables.PlayableOutput)
extern "C" IL2CPP_METHOD_ATTR AnimationPlayableOutput_t1918618239  AnimationPlayableOutput_op_Explicit_m3644661077 (RuntimeObject * __this /* static, unused */, PlayableOutput_t3179894105  ___output0, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t1918618239  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableOutputHandle_t4208053793  L_0 = PlayableOutput_GetHandle_m777137769((PlayableOutput_t3179894105 *)(&___output0), /*hidden argument*/NULL);
		AnimationPlayableOutput_t1918618239  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationPlayableOutput__ctor_m457905557((&L_1), L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		AnimationPlayableOutput_t1918618239  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::GetTarget()
extern "C" IL2CPP_METHOD_ATTR Animator_t434523843 * AnimationPlayableOutput_GetTarget_m58157427 (AnimationPlayableOutput_t1918618239 * __this, const RuntimeMethod* method)
{
	Animator_t434523843 * V_0 = NULL;
	{
		PlayableOutputHandle_t4208053793 * L_0 = __this->get_address_of_m_Handle_0();
		Animator_t434523843 * L_1 = AnimationPlayableOutput_InternalGetTarget_m2511702932(NULL /*static, unused*/, (PlayableOutputHandle_t4208053793 *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Animator_t434523843 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Animator_t434523843 * AnimationPlayableOutput_GetTarget_m58157427_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t1918618239 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t1918618239 *>(__this + 1);
	return AnimationPlayableOutput_GetTarget_m58157427(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::SetTarget(UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableOutput_SetTarget_m4167975687 (AnimationPlayableOutput_t1918618239 * __this, Animator_t434523843 * ___value0, const RuntimeMethod* method)
{
	{
		PlayableOutputHandle_t4208053793 * L_0 = __this->get_address_of_m_Handle_0();
		Animator_t434523843 * L_1 = ___value0;
		AnimationPlayableOutput_InternalSetTarget_m860681215(NULL /*static, unused*/, (PlayableOutputHandle_t4208053793 *)L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimationPlayableOutput_SetTarget_m4167975687_AdjustorThunk (RuntimeObject * __this, Animator_t434523843 * ___value0, const RuntimeMethod* method)
{
	AnimationPlayableOutput_t1918618239 * _thisAdjusted = reinterpret_cast<AnimationPlayableOutput_t1918618239 *>(__this + 1);
	AnimationPlayableOutput_SetTarget_m4167975687(_thisAdjusted, ___value0, method);
}
// UnityEngine.Animator UnityEngine.Animations.AnimationPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)
extern "C" IL2CPP_METHOD_ATTR Animator_t434523843 * AnimationPlayableOutput_InternalGetTarget_m2511702932 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t4208053793 * ___handle0, const RuntimeMethod* method)
{
	typedef Animator_t434523843 * (*AnimationPlayableOutput_InternalGetTarget_m2511702932_ftn) (PlayableOutputHandle_t4208053793 *);
	static AnimationPlayableOutput_InternalGetTarget_m2511702932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableOutput_InternalGetTarget_m2511702932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableOutput::InternalGetTarget(UnityEngine.Playables.PlayableOutputHandle&)");
	Animator_t434523843 * retVal = _il2cpp_icall_func(___handle0);
	return retVal;
}
// System.Void UnityEngine.Animations.AnimationPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void AnimationPlayableOutput_InternalSetTarget_m860681215 (RuntimeObject * __this /* static, unused */, PlayableOutputHandle_t4208053793 * ___handle0, Animator_t434523843 * ___target1, const RuntimeMethod* method)
{
	typedef void (*AnimationPlayableOutput_InternalSetTarget_m860681215_ftn) (PlayableOutputHandle_t4208053793 *, Animator_t434523843 *);
	static AnimationPlayableOutput_InternalSetTarget_m860681215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationPlayableOutput_InternalSetTarget_m860681215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animations.AnimationPlayableOutput::InternalSetTarget(UnityEngine.Playables.PlayableOutputHandle&,UnityEngine.Animator)");
	_il2cpp_icall_func(___handle0, ___target1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimatorControllerPlayable__ctor_m3584989806 (AnimatorControllerPlayable_t1015767841 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Handle_0(L_0);
		PlayableHandle_t1095853803  L_1 = ___handle0;
		AnimatorControllerPlayable_SetHandle_m2260644454((AnimatorControllerPlayable_t1015767841 *)__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void AnimatorControllerPlayable__ctor_m3584989806_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t1015767841 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t1015767841 *>(__this + 1);
	AnimatorControllerPlayable__ctor_m3584989806(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimatorControllerPlayable_GetHandle_m2425630109 (AnimatorControllerPlayable_t1015767841 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimatorControllerPlayable_GetHandle_m2425630109_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t1015767841 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t1015767841 *>(__this + 1);
	return AnimatorControllerPlayable_GetHandle_m2425630109(_thisAdjusted, method);
}
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::SetHandle(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimatorControllerPlayable_SetHandle_m2260644454 (AnimatorControllerPlayable_t1015767841 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControllerPlayable_SetHandle_m2260644454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803 * L_0 = __this->get_address_of_m_Handle_0();
		bool L_1 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t56020091 * L_2 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_2, _stringLiteral3786669396, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimatorControllerPlayable_SetHandle_m2260644454_RuntimeMethod_var);
	}

IL_001c:
	{
		bool L_3 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		bool L_4 = PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t1015767841_m3416945299((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimatorControllerPlayable_t1015767841_m3416945299_RuntimeMethod_var);
		if (L_4)
		{
			goto IL_0040;
		}
	}
	{
		InvalidCastException_t3927145244 * L_5 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_5, _stringLiteral2761492165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, AnimatorControllerPlayable_SetHandle_m2260644454_RuntimeMethod_var);
	}

IL_0040:
	{
	}

IL_0041:
	{
		PlayableHandle_t1095853803  L_6 = ___handle0;
		__this->set_m_Handle_0(L_6);
		return;
	}
}
extern "C"  void AnimatorControllerPlayable_SetHandle_m2260644454_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t1015767841 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t1015767841 *>(__this + 1);
	AnimatorControllerPlayable_SetHandle_m2260644454(_thisAdjusted, ___handle0, method);
}
// System.Boolean UnityEngine.Animations.AnimatorControllerPlayable::Equals(UnityEngine.Animations.AnimatorControllerPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorControllerPlayable_Equals_m3421642688 (AnimatorControllerPlayable_t1015767841 * __this, AnimatorControllerPlayable_t1015767841  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimatorControllerPlayable_GetHandle_m2425630109((AnimatorControllerPlayable_t1015767841 *)__this, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = AnimatorControllerPlayable_GetHandle_m2425630109((AnimatorControllerPlayable_t1015767841 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m3344837515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorControllerPlayable_Equals_m3421642688_AdjustorThunk (RuntimeObject * __this, AnimatorControllerPlayable_t1015767841  ___other0, const RuntimeMethod* method)
{
	AnimatorControllerPlayable_t1015767841 * _thisAdjusted = reinterpret_cast<AnimatorControllerPlayable_t1015767841 *>(__this + 1);
	return AnimatorControllerPlayable_Equals_m3421642688(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Animations.AnimatorControllerPlayable::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AnimatorControllerPlayable__cctor_m3439442711 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControllerPlayable__cctor_m3439442711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimatorControllerPlayable_t1015767841  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimatorControllerPlayable__ctor_m3584989806((&L_1), L_0, /*hidden argument*/NULL);
		((AnimatorControllerPlayable_t1015767841_StaticFields*)il2cpp_codegen_static_fields_for(AnimatorControllerPlayable_t1015767841_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Animator::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Animator__ctor_m4069340359 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m346897018(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::get_isHuman()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_isHuman_m3044300138 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_isHuman_m3044300138_ftn) (Animator_t434523843 *);
	static Animator_get_isHuman_m3044300138_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_isHuman_m3044300138_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_isHuman()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Animator::get_humanScale()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_humanScale_m2669489364 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_humanScale_m2669489364_ftn) (Animator_t434523843 *);
	static Animator_get_humanScale_m2669489364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_humanScale_m2669489364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_humanScale()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Animator::GetFloat(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetFloat_m2180732288 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___id0;
		float L_1 = Animator_GetFloatID_m3658774233(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloat_m2677061968 (Animator_t434523843 * __this, int32_t ___id0, float ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___value1;
		Animator_SetFloatID_m759961653(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloat_m2629623791 (Animator_t434523843 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___value1;
		float L_2 = ___dampTime2;
		float L_3 = ___deltaTime3;
		Animator_SetFloatIDDamp_m1611756056(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::GetBool(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Animator_GetBool_m2974171932 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = ___id0;
		bool L_1 = Animator_GetBoolID_m3211650753(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetBool_m2873262441 (Animator_t434523843 * __this, int32_t ___id0, bool ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		bool L_1 = ___value1;
		Animator_SetBoolID_m2106676274(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Animator::GetInteger(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_GetInteger_m2539041872 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = Animator_GetIntegerID_m3539387357(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Animator::SetInteger(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetInteger_m1764346092 (Animator_t434523843 * __this, int32_t ___id0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = ___value1;
		Animator_SetIntegerID_m1197891907(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetTrigger_m2134052629 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_SetTriggerString_m2612407758(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_ResetTrigger_m2321267720 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_ResetTriggerString_m394341254(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Animator_IsParameterControlledByCurve_m4249345953 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		bool L_1 = Animator_IsParameterControlledByCurveString_m3449841540(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_deltaPosition_m3159651528 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_deltaPosition_Injected_m3057738582(__this, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::get_deltaRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_get_deltaRotation_m2491030442 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_deltaRotation_Injected_m2558147056(__this, (Quaternion_t2301928331 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_rootPosition_m3382778530 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_rootPosition_Injected_m2912740584(__this, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_get_rootRotation_m1774376365 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_rootRotation_Injected_m1378784712(__this, (Quaternion_t2301928331 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.Animator::get_applyRootMotion()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_applyRootMotion_m1347111243 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_applyRootMotion_m1347111243_ftn) (Animator_t434523843 *);
	static Animator_get_applyRootMotion_m1347111243_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_applyRootMotion_m1347111243_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_applyRootMotion()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_applyRootMotion_m3182098314 (Animator_t434523843 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_applyRootMotion_m3182098314_ftn) (Animator_t434523843 *, bool);
	static Animator_set_applyRootMotion_m3182098314_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_applyRootMotion_m3182098314_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_applyRootMotion(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_gravityWeight()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_gravityWeight_m1523032652 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_gravityWeight_m1523032652_ftn) (Animator_t434523843 *);
	static Animator_get_gravityWeight_m1523032652_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_gravityWeight_m1523032652_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_gravityWeight()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_bodyPosition_m2940112464 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = Animator_get_bodyPositionInternal_m3052787056(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::set_bodyPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyPosition_m1171301473 (Animator_t434523843 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = ___value0;
		Animator_set_bodyPositionInternal_m667443125(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPositionInternal()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_bodyPositionInternal_m3052787056 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_bodyPositionInternal_Injected_m4243447051(__this, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::set_bodyPositionInternal(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyPositionInternal_m667443125 (Animator_t434523843 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Animator_set_bodyPositionInternal_Injected_m1654945682(__this, (Vector3_t3722313464 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_get_bodyRotation_m2416179877 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = Animator_get_bodyRotationInternal_m3152232923(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t2301928331  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::set_bodyRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyRotation_m252641853 (Animator_t434523843 * __this, Quaternion_t2301928331  ___value0, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = ___value0;
		Animator_set_bodyRotationInternal_m1688178068(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotationInternal()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_get_bodyRotationInternal_m3152232923 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_bodyRotationInternal_Injected_m1688249227(__this, (Quaternion_t2301928331 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::set_bodyRotationInternal(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyRotationInternal_m1688178068 (Animator_t434523843 * __this, Quaternion_t2301928331  ___value0, const RuntimeMethod* method)
{
	{
		Animator_set_bodyRotationInternal_Injected_m139719495(__this, (Quaternion_t2301928331 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPosition(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_GetIKPosition_m1659679920 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Vector3_t3722313464  L_1 = Animator_GetGoalPosition_m3762162859(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Vector3_t3722313464  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::GetGoalPosition(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_GetGoalPosition_m3762162859 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___goal0;
		Animator_GetGoalPosition_Injected_m3263093872(__this, L_0, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetIKPosition_m4054460200 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464  ___goalPosition1, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Vector3_t3722313464  L_1 = ___goalPosition1;
		Animator_SetGoalPosition_m3669207002(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetGoalPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalPosition_m3669207002 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464  ___goalPosition1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___goal0;
		Animator_SetGoalPosition_Injected_m3736935077(__this, L_0, (Vector3_t3722313464 *)(&___goalPosition1), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotation(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_GetIKRotation_m2892151993 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Quaternion_t2301928331  L_1 = Animator_GetGoalRotation_m1869366523(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t2301928331  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::GetGoalRotation(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_GetGoalRotation_m1869366523 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___goal0;
		Animator_GetGoalRotation_Injected_m456765294(__this, L_0, (Quaternion_t2301928331 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetIKRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetIKRotation_m1010797409 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331  ___goalRotation1, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Quaternion_t2301928331  L_1 = ___goalRotation1;
		Animator_SetGoalRotation_m1125692758(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetGoalRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalRotation_m1125692758 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331  ___goalRotation1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___goal0;
		Animator_SetGoalRotation_Injected_m2163733550(__this, L_0, (Quaternion_t2301928331 *)(&___goalRotation1), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Animator::GetIKPositionWeight(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetIKPositionWeight_m419253531 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = Animator_GetGoalWeightPosition_m1730430196(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Animator::GetGoalWeightPosition(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetGoalWeightPosition_m1730430196 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	typedef float (*Animator_GetGoalWeightPosition_m1730430196_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetGoalWeightPosition_m1730430196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetGoalWeightPosition_m1730430196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetGoalWeightPosition(UnityEngine.AvatarIKGoal)");
	float retVal = _il2cpp_icall_func(__this, ___goal0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetIKPositionWeight_m2531330339 (Animator_t434523843 * __this, int32_t ___goal0, float ___value1, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = ___value1;
		Animator_SetGoalWeightPosition_m3736882473(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetGoalWeightPosition(UnityEngine.AvatarIKGoal,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalWeightPosition_m3736882473 (Animator_t434523843 * __this, int32_t ___goal0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetGoalWeightPosition_m3736882473_ftn) (Animator_t434523843 *, int32_t, float);
	static Animator_SetGoalWeightPosition_m3736882473_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetGoalWeightPosition_m3736882473_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetGoalWeightPosition(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal0, ___value1);
}
// System.Single UnityEngine.Animator::GetIKRotationWeight(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetIKRotationWeight_m1582869181 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = Animator_GetGoalWeightRotation_m2450456186(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Animator::GetGoalWeightRotation(UnityEngine.AvatarIKGoal)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetGoalWeightRotation_m2450456186 (Animator_t434523843 * __this, int32_t ___goal0, const RuntimeMethod* method)
{
	typedef float (*Animator_GetGoalWeightRotation_m2450456186_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetGoalWeightRotation_m2450456186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetGoalWeightRotation_m2450456186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetGoalWeightRotation(UnityEngine.AvatarIKGoal)");
	float retVal = _il2cpp_icall_func(__this, ___goal0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetIKRotationWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetIKRotationWeight_m1678977726 (Animator_t434523843 * __this, int32_t ___goal0, float ___value1, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = ___value1;
		Animator_SetGoalWeightRotation_m1438054163(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetGoalWeightRotation(UnityEngine.AvatarIKGoal,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalWeightRotation_m1438054163 (Animator_t434523843 * __this, int32_t ___goal0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetGoalWeightRotation_m1438054163_ftn) (Animator_t434523843 *, int32_t, float);
	static Animator_SetGoalWeightRotation_m1438054163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetGoalWeightRotation_m1438054163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetGoalWeightRotation(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal0, ___value1);
}
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtPosition_m2184651521 (Animator_t434523843 * __this, Vector3_t3722313464  ___lookAtPosition0, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = ___lookAtPosition0;
		Animator_SetLookAtPositionInternal_m1039545480(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtPositionInternal_m1039545480 (Animator_t434523843 * __this, Vector3_t3722313464  ___lookAtPosition0, const RuntimeMethod* method)
{
	{
		Animator_SetLookAtPositionInternal_Injected_m733885560(__this, (Vector3_t3722313464 *)(&___lookAtPosition0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeight_m1562921440 (Animator_t434523843 * __this, float ___weight0, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		Animator_SetLookAtWeightInternal_m1957339916(__this, L_0, (0.0f), (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeight_m642062228 (Animator_t434523843 * __this, float ___weight0, float ___bodyWeight1, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		Animator_SetLookAtWeightInternal_m1957339916(__this, L_0, L_1, (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeight_m3252672319 (Animator_t434523843 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		Animator_SetLookAtWeightInternal_m1957339916(__this, L_0, L_1, L_2, (0.0f), (0.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeight_m2190134726 (Animator_t434523843 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = ___eyesWeight3;
		Animator_SetLookAtWeightInternal_m1957339916(__this, L_0, L_1, L_2, L_3, (0.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeight_m4069750608 (Animator_t434523843 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const RuntimeMethod* method)
{
	{
		Animator_CheckIfInIKPass_m1701567706(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = ___eyesWeight3;
		float L_4 = ___clampWeight4;
		Animator_SetLookAtWeightInternal_m1957339916(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtWeightInternal_m1957339916 (Animator_t434523843 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const RuntimeMethod* method)
{
	typedef void (*Animator_SetLookAtWeightInternal_m1957339916_ftn) (Animator_t434523843 *, float, float, float, float, float);
	static Animator_SetLookAtWeightInternal_m1957339916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLookAtWeightInternal_m1957339916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___weight0, ___bodyWeight1, ___headWeight2, ___eyesWeight3, ___clampWeight4);
}
// System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_stabilizeFeet_m3335720898 (Animator_t434523843 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_stabilizeFeet_m3335720898_ftn) (Animator_t434523843 *, bool);
	static Animator_set_stabilizeFeet_m3335720898_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_stabilizeFeet_m3335720898_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_stabilizeFeet(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_get_layerCount_m3408248017 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Animator_get_layerCount_m3408248017_ftn) (Animator_t434523843 *);
	static Animator_get_layerCount_m3408248017_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layerCount_m3408248017_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layerCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.String UnityEngine.Animator::GetLayerName(System.Int32)
extern "C" IL2CPP_METHOD_ATTR String_t* Animator_GetLayerName_m3348632109 (Animator_t434523843 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	typedef String_t* (*Animator_GetLayerName_m3348632109_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetLayerName_m3348632109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerName_m3348632109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerName(System.Int32)");
	String_t* retVal = _il2cpp_icall_func(__this, ___layerIndex0);
	return retVal;
}
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetLayerWeight_m2452831872 (Animator_t434523843 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	typedef float (*Animator_GetLayerWeight_m2452831872_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetLayerWeight_m2452831872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerWeight_m2452831872_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerWeight(System.Int32)");
	float retVal = _il2cpp_icall_func(__this, ___layerIndex0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLayerWeight_m2632303547 (Animator_t434523843 * __this, int32_t ___layerIndex0, float ___weight1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetLayerWeight_m2632303547_ftn) (Animator_t434523843 *, int32_t, float);
	static Animator_SetLayerWeight_m2632303547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLayerWeight_m2632303547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___layerIndex0, ___weight1);
}
// System.Void UnityEngine.Animator::GetAnimatorStateInfo(System.Int32,UnityEngine.StateInfoIndex,UnityEngine.AnimatorStateInfo&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetAnimatorStateInfo_m2045721220 (Animator_t434523843 * __this, int32_t ___layerIndex0, int32_t ___stateInfoIndex1, AnimatorStateInfo_t509032636 * ___info2, const RuntimeMethod* method)
{
	typedef void (*Animator_GetAnimatorStateInfo_m2045721220_ftn) (Animator_t434523843 *, int32_t, int32_t, AnimatorStateInfo_t509032636 *);
	static Animator_GetAnimatorStateInfo_m2045721220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetAnimatorStateInfo_m2045721220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetAnimatorStateInfo(System.Int32,UnityEngine.StateInfoIndex,UnityEngine.AnimatorStateInfo&)");
	_il2cpp_icall_func(__this, ___layerIndex0, ___stateInfoIndex1, ___info2);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimatorStateInfo_t509032636  Animator_GetCurrentAnimatorStateInfo_m18694920 (Animator_t434523843 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimatorStateInfo_t509032636  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerIndex0;
		Animator_GetAnimatorStateInfo_m2045721220(__this, L_0, 0, (AnimatorStateInfo_t509032636 *)(&V_0), /*hidden argument*/NULL);
		AnimatorStateInfo_t509032636  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AnimatorStateInfo_t509032636  L_2 = V_1;
		return L_2;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimatorStateInfo_t509032636  Animator_GetNextAnimatorStateInfo_m3619397528 (Animator_t434523843 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimatorStateInfo_t509032636  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerIndex0;
		Animator_GetAnimatorStateInfo_m2045721220(__this, L_0, 1, (AnimatorStateInfo_t509032636 *)(&V_0), /*hidden argument*/NULL);
		AnimatorStateInfo_t509032636  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AnimatorStateInfo_t509032636  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32,UnityEngine.AnimatorTransitionInfo&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetAnimatorTransitionInfo_m4215243493 (Animator_t434523843 * __this, int32_t ___layerIndex0, AnimatorTransitionInfo_t2534804151 * ___info1, const RuntimeMethod* method)
{
	typedef void (*Animator_GetAnimatorTransitionInfo_m4215243493_ftn) (Animator_t434523843 *, int32_t, AnimatorTransitionInfo_t2534804151 *);
	static Animator_GetAnimatorTransitionInfo_m4215243493_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetAnimatorTransitionInfo_m4215243493_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32,UnityEngine.AnimatorTransitionInfo&)");
	_il2cpp_icall_func(__this, ___layerIndex0, ___info1);
}
// UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
extern "C" IL2CPP_METHOD_ATTR AnimatorTransitionInfo_t2534804151  Animator_GetAnimatorTransitionInfo_m2361892554 (Animator_t434523843 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	AnimatorTransitionInfo_t2534804151  V_0;
	memset(&V_0, 0, sizeof(V_0));
	AnimatorTransitionInfo_t2534804151  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerIndex0;
		Animator_GetAnimatorTransitionInfo_m4215243493(__this, L_0, (AnimatorTransitionInfo_t2534804151 *)(&V_0), /*hidden argument*/NULL);
		AnimatorTransitionInfo_t2534804151  L_1 = V_0;
		V_1 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		AnimatorTransitionInfo_t2534804151  L_2 = V_1;
		return L_2;
	}
}
// System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Animator_IsInTransition_m2711427730 (Animator_t434523843 * __this, int32_t ___layerIndex0, const RuntimeMethod* method)
{
	typedef bool (*Animator_IsInTransition_m2711427730_ftn) (Animator_t434523843 *, int32_t);
	static Animator_IsInTransition_m2711427730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsInTransition_m2711427730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsInTransition(System.Int32)");
	bool retVal = _il2cpp_icall_func(__this, ___layerIndex0);
	return retVal;
}
// System.Single UnityEngine.Animator::get_feetPivotActive()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_feetPivotActive_m1880684134 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_feetPivotActive_m1880684134_ftn) (Animator_t434523843 *);
	static Animator_get_feetPivotActive_m1880684134_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_feetPivotActive_m1880684134_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_feetPivotActive()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_feetPivotActive_m195834639 (Animator_t434523843 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_feetPivotActive_m195834639_ftn) (Animator_t434523843 *, float);
	static Animator_set_feetPivotActive_m195834639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_feetPivotActive_m195834639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_feetPivotActive(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_pivotWeight()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_pivotWeight_m2747931764 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_pivotWeight_m2747931764_ftn) (Animator_t434523843 *);
	static Animator_get_pivotWeight_m2747931764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_pivotWeight_m2747931764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_pivotWeight()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.Vector3 UnityEngine.Animator::get_pivotPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_pivotPosition_m1592483982 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_pivotPosition_Injected_m3973723677(__this, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_MatchTarget_m2861954881 (Animator_t434523843 * __this, Vector3_t3722313464  ___matchPosition0, Quaternion_t2301928331  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t554846203  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___targetBodyPart2;
		float L_1 = ___startNormalizedTime4;
		float L_2 = ___targetNormalizedTime5;
		Animator_MatchTarget_Injected_m291306687(__this, (Vector3_t3722313464 *)(&___matchPosition0), (Quaternion_t2301928331 *)(&___matchRotation1), L_0, (MatchTargetWeightMask_t554846203 *)(&___weightMask3), L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_MatchTarget_m606907320 (Animator_t434523843 * __this, Vector3_t3722313464  ___matchPosition0, Quaternion_t2301928331  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t554846203  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___matchPosition0;
		Quaternion_t2301928331  L_1 = ___matchRotation1;
		int32_t L_2 = ___targetBodyPart2;
		MatchTargetWeightMask_t554846203  L_3 = ___weightMask3;
		float L_4 = ___startNormalizedTime4;
		float L_5 = ___targetNormalizedTime5;
		Animator_MatchTarget_m2861954881(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_InterruptMatchTarget_m1619794578 (Animator_t434523843 * __this, bool ___completeMatch0, const RuntimeMethod* method)
{
	typedef void (*Animator_InterruptMatchTarget_m1619794578_ftn) (Animator_t434523843 *, bool);
	static Animator_InterruptMatchTarget_m1619794578_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_InterruptMatchTarget_m1619794578_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::InterruptMatchTarget(System.Boolean)");
	_il2cpp_icall_func(__this, ___completeMatch0);
}
// System.Boolean UnityEngine.Animator::get_isMatchingTarget()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_isMatchingTarget_m2727169323 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_isMatchingTarget_m2727169323_ftn) (Animator_t434523843 *);
	static Animator_get_isMatchingTarget_m2727169323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_isMatchingTarget_m2727169323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_isMatchingTarget()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Animator::get_speed()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_speed_m2084108235 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_speed_m2084108235_ftn) (Animator_t434523843 *);
	static Animator_get_speed_m2084108235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_speed_m2084108235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_speed()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_speed_m1181320995 (Animator_t434523843 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_speed_m1181320995_ftn) (Animator_t434523843 *, float);
	static Animator_set_speed_m1181320995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_speed_m1181320995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_CrossFade_m2614286674 (Animator_t434523843 * __this, String_t* ___stateName0, float ___normalizedTransitionDuration1, int32_t ___layer2, float ___normalizedTimeOffset3, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		String_t* L_0 = ___stateName0;
		float L_1 = ___normalizedTransitionDuration1;
		int32_t L_2 = ___layer2;
		float L_3 = ___normalizedTimeOffset3;
		float L_4 = V_0;
		Animator_CrossFade_m633499242(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_CrossFade_m633499242 (Animator_t434523843 * __this, String_t* ___stateName0, float ___normalizedTransitionDuration1, int32_t ___layer2, float ___normalizedTimeOffset3, float ___normalizedTransitionTime4, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___normalizedTransitionDuration1;
		int32_t L_3 = ___layer2;
		float L_4 = ___normalizedTimeOffset3;
		float L_5 = ___normalizedTransitionTime4;
		Animator_CrossFade_m2328649388(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_CrossFade_m2328649388 (Animator_t434523843 * __this, int32_t ___stateHashName0, float ___normalizedTransitionDuration1, int32_t ___layer2, float ___normalizedTimeOffset3, float ___normalizedTransitionTime4, const RuntimeMethod* method)
{
	typedef void (*Animator_CrossFade_m2328649388_ftn) (Animator_t434523843 *, int32_t, float, int32_t, float, float);
	static Animator_CrossFade_m2328649388_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_CrossFade_m2328649388_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___stateHashName0, ___normalizedTransitionDuration1, ___layer2, ___normalizedTimeOffset3, ___normalizedTransitionTime4);
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m2116509493 (Animator_t434523843 * __this, String_t* ___stateName0, int32_t ___layer1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		String_t* L_0 = ___stateName0;
		int32_t L_1 = ___layer1;
		float L_2 = V_0;
		Animator_Play_m2835034014(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m1697843332 (Animator_t434523843 * __this, String_t* ___stateName0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		V_1 = (-1);
		String_t* L_0 = ___stateName0;
		int32_t L_1 = V_1;
		float L_2 = V_0;
		Animator_Play_m2835034014(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m2835034014 (Animator_t434523843 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer1;
		float L_3 = ___normalizedTime2;
		Animator_Play_m1207279914(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m1207279914 (Animator_t434523843 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const RuntimeMethod* method)
{
	typedef void (*Animator_Play_m1207279914_ftn) (Animator_t434523843 *, int32_t, int32_t, float);
	static Animator_Play_m1207279914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_Play_m1207279914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___layer1, ___normalizedTime2);
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m4074388123 (Animator_t434523843 * __this, int32_t ___stateNameHash0, int32_t ___layer1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___stateNameHash0;
		int32_t L_1 = ___layer1;
		float L_2 = V_0;
		Animator_Play_m1207279914(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_Play_m3471924166 (Animator_t434523843 * __this, int32_t ___stateNameHash0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		V_1 = (-1);
		int32_t L_0 = ___stateNameHash0;
		int32_t L_1 = V_1;
		float L_2 = V_0;
		Animator_Play_m1207279914(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetTarget_m3687197238 (Animator_t434523843 * __this, int32_t ___targetIndex0, float ___targetNormalizedTime1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetTarget_m3687197238_ftn) (Animator_t434523843 *, int32_t, float);
	static Animator_SetTarget_m3687197238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTarget_m3687197238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)");
	_il2cpp_icall_func(__this, ___targetIndex0, ___targetNormalizedTime1);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_targetPosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Animator_get_targetPosition_m2702492079 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_targetPosition_Injected_m1456028510(__this, (Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::get_targetRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Animator_get_targetRotation_m4239723054 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_get_targetRotation_Injected_m2584699430(__this, (Quaternion_t2301928331 *)(&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Animator_GetBoneTransform_m929462223 (Animator_t434523843 * __this, int32_t ___humanBoneId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animator_GetBoneTransform_m929462223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	{
		int32_t L_0 = ___humanBoneId0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ___humanBoneId0;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)55))))
		{
			goto IL_0027;
		}
	}

IL_0010:
	{
		int32_t L_2 = ((int32_t)((int32_t)55));
		RuntimeObject * L_3 = Box(HumanBodyBones_t600150811_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral78940560, L_3, /*hidden argument*/NULL);
		IndexOutOfRangeException_t1578797820 * L_5 = (IndexOutOfRangeException_t1578797820 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t1578797820_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3408750441(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, NULL, Animator_GetBoneTransform_m929462223_RuntimeMethod_var);
	}

IL_0027:
	{
		int32_t L_6 = ___humanBoneId0;
		int32_t L_7 = HumanTrait_GetBoneIndexFromMono_m1872291878(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = Animator_GetBoneTransformInternal_m528051545(__this, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0039;
	}

IL_0039:
	{
		Transform_t3600365921 * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransformInternal(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Animator_GetBoneTransformInternal_m528051545 (Animator_t434523843 * __this, int32_t ___humanBoneId0, const RuntimeMethod* method)
{
	typedef Transform_t3600365921 * (*Animator_GetBoneTransformInternal_m528051545_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetBoneTransformInternal_m528051545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoneTransformInternal_m528051545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoneTransformInternal(System.Int32)");
	Transform_t3600365921 * retVal = _il2cpp_icall_func(__this, ___humanBoneId0);
	return retVal;
}
// UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_get_cullingMode_m1463878721 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Animator_get_cullingMode_m1463878721_ftn) (Animator_t434523843 *);
	static Animator_get_cullingMode_m1463878721_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_cullingMode_m1463878721_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_cullingMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_cullingMode_m1630341969 (Animator_t434523843 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_cullingMode_m1630341969_ftn) (Animator_t434523843 *, int32_t);
	static Animator_set_cullingMode_m1630341969_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_cullingMode_m1630341969_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::StartPlayback()
extern "C" IL2CPP_METHOD_ATTR void Animator_StartPlayback_m3123690249 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef void (*Animator_StartPlayback_m3123690249_ftn) (Animator_t434523843 *);
	static Animator_StartPlayback_m3123690249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StartPlayback_m3123690249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StartPlayback()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::StopPlayback()
extern "C" IL2CPP_METHOD_ATTR void Animator_StopPlayback_m3399889712 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef void (*Animator_StopPlayback_m3399889712_ftn) (Animator_t434523843 *);
	static Animator_StopPlayback_m3399889712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StopPlayback_m3399889712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StopPlayback()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_playbackTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_playbackTime_m3857839613 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_playbackTime_m3857839613_ftn) (Animator_t434523843 *);
	static Animator_get_playbackTime_m3857839613_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_playbackTime_m3857839613_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_playbackTime()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::set_playbackTime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_playbackTime_m2922241191 (Animator_t434523843 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_playbackTime_m2922241191_ftn) (Animator_t434523843 *, float);
	static Animator_set_playbackTime_m2922241191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_playbackTime_m2922241191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_playbackTime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::StartRecording(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_StartRecording_m2219572212 (Animator_t434523843 * __this, int32_t ___frameCount0, const RuntimeMethod* method)
{
	typedef void (*Animator_StartRecording_m2219572212_ftn) (Animator_t434523843 *, int32_t);
	static Animator_StartRecording_m2219572212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StartRecording_m2219572212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StartRecording(System.Int32)");
	_il2cpp_icall_func(__this, ___frameCount0);
}
// System.Void UnityEngine.Animator::StopRecording()
extern "C" IL2CPP_METHOD_ATTR void Animator_StopRecording_m1316699595 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef void (*Animator_StopRecording_m1316699595_ftn) (Animator_t434523843 *);
	static Animator_StopRecording_m1316699595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StopRecording_m1316699595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StopRecording()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_recorderStartTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_recorderStartTime_m3664365018 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Animator_GetRecorderStartTime_m2061949189(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Animator::GetRecorderStartTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_GetRecorderStartTime_m2061949189 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_GetRecorderStartTime_m2061949189_ftn) (Animator_t434523843 *);
	static Animator_GetRecorderStartTime_m2061949189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetRecorderStartTime_m2061949189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetRecorderStartTime()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Animator::get_recorderStopTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_recorderStopTime_m2793978137 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Animator_GetRecorderStopTime_m2000074451(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Animator::GetRecorderStopTime()
extern "C" IL2CPP_METHOD_ATTR float Animator_GetRecorderStopTime_m2000074451 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_GetRecorderStopTime_m2000074451_ftn) (Animator_t434523843 *);
	static Animator_GetRecorderStopTime_m2000074451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetRecorderStopTime_m2000074451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetRecorderStopTime()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_hasBoundPlayables_m2940726619 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_hasBoundPlayables_m2940726619_ftn) (Animator_t434523843 *);
	static Animator_get_hasBoundPlayables_m2940726619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_hasBoundPlayables_m2940726619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_hasBoundPlayables()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_StringToHash_m1666053228 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	typedef int32_t (*Animator_StringToHash_m1666053228_ftn) (String_t*);
	static Animator_StringToHash_m1666053228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m1666053228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	int32_t retVal = _il2cpp_icall_func(___name0);
	return retVal;
}
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern "C" IL2CPP_METHOD_ATTR void Animator_CheckIfInIKPass_m1701567706 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Animator_CheckIfInIKPass_m1701567706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Animator_get_logWarnings_m3175887280(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = Animator_IsInIKPass_m1033564951(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral3380224547, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Boolean UnityEngine.Animator::IsInIKPass()
extern "C" IL2CPP_METHOD_ATTR bool Animator_IsInIKPass_m1033564951 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_IsInIKPass_m1033564951_ftn) (Animator_t434523843 *);
	static Animator_IsInIKPass_m1033564951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsInIKPass_m1033564951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsInIKPass()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloatID_m759961653 (Animator_t434523843 * __this, int32_t ___id0, float ___value1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetFloatID_m759961653_ftn) (Animator_t434523843 *, int32_t, float);
	static Animator_SetFloatID_m759961653_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatID_m759961653_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatID(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float Animator_GetFloatID_m3658774233 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	typedef float (*Animator_GetFloatID_m3658774233_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetFloatID_m3658774233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetFloatID_m3658774233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetFloatID(System.Int32)");
	float retVal = _il2cpp_icall_func(__this, ___id0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetBoolID_m2106676274 (Animator_t434523843 * __this, int32_t ___id0, bool ___value1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetBoolID_m2106676274_ftn) (Animator_t434523843 *, int32_t, bool);
	static Animator_SetBoolID_m2106676274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolID_m2106676274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Animator_GetBoolID_m3211650753 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	typedef bool (*Animator_GetBoolID_m3211650753_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetBoolID_m3211650753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoolID_m3211650753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoolID(System.Int32)");
	bool retVal = _il2cpp_icall_func(__this, ___id0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetIntegerID_m1197891907 (Animator_t434523843 * __this, int32_t ___id0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetIntegerID_m1197891907_ftn) (Animator_t434523843 *, int32_t, int32_t);
	static Animator_SetIntegerID_m1197891907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIntegerID_m1197891907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Animator_GetIntegerID_m3539387357 (Animator_t434523843 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	typedef int32_t (*Animator_GetIntegerID_m3539387357_ftn) (Animator_t434523843 *, int32_t);
	static Animator_GetIntegerID_m3539387357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIntegerID_m3539387357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIntegerID(System.Int32)");
	int32_t retVal = _il2cpp_icall_func(__this, ___id0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetTriggerString_m2612407758 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animator_SetTriggerString_m2612407758_ftn) (Animator_t434523843 *, String_t*);
	static Animator_SetTriggerString_m2612407758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m2612407758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_ResetTriggerString_m394341254 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef void (*Animator_ResetTriggerString_m394341254_ftn) (Animator_t434523843 *, String_t*);
	static Animator_ResetTriggerString_m394341254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m394341254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Animator_IsParameterControlledByCurveString_m3449841540 (Animator_t434523843 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	typedef bool (*Animator_IsParameterControlledByCurveString_m3449841540_ftn) (Animator_t434523843 *, String_t*);
	static Animator_IsParameterControlledByCurveString_m3449841540_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsParameterControlledByCurveString_m3449841540_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsParameterControlledByCurveString(System.String)");
	bool retVal = _il2cpp_icall_func(__this, ___name0);
	return retVal;
}
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetFloatIDDamp_m1611756056 (Animator_t434523843 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const RuntimeMethod* method)
{
	typedef void (*Animator_SetFloatIDDamp_m1611756056_ftn) (Animator_t434523843 *, int32_t, float, float, float);
	static Animator_SetFloatIDDamp_m1611756056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatIDDamp_m1611756056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___id0, ___value1, ___dampTime2, ___deltaTime3);
}
// System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_layersAffectMassCenter_m2154164557 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_layersAffectMassCenter_m2154164557_ftn) (Animator_t434523843 *);
	static Animator_get_layersAffectMassCenter_m2154164557_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layersAffectMassCenter_m2154164557_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layersAffectMassCenter()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_layersAffectMassCenter_m1998188045 (Animator_t434523843 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_layersAffectMassCenter_m1998188045_ftn) (Animator_t434523843 *, bool);
	static Animator_set_layersAffectMassCenter_m1998188045_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_layersAffectMassCenter_m1998188045_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_leftFeetBottomHeight_m425469140 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_leftFeetBottomHeight_m425469140_ftn) (Animator_t434523843 *);
	static Animator_get_leftFeetBottomHeight_m425469140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_leftFeetBottomHeight_m425469140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_leftFeetBottomHeight()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
extern "C" IL2CPP_METHOD_ATTR float Animator_get_rightFeetBottomHeight_m2289958331 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef float (*Animator_get_rightFeetBottomHeight_m2289958331_ftn) (Animator_t434523843 *);
	static Animator_get_rightFeetBottomHeight_m2289958331_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_rightFeetBottomHeight_m2289958331_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_rightFeetBottomHeight()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C" IL2CPP_METHOD_ATTR bool Animator_get_logWarnings_m3175887280 (Animator_t434523843 * __this, const RuntimeMethod* method)
{
	typedef bool (*Animator_get_logWarnings_m3175887280_ftn) (Animator_t434523843 *);
	static Animator_get_logWarnings_m3175887280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_logWarnings_m3175887280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_logWarnings()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Animator::get_deltaPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_deltaPosition_Injected_m3057738582 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_deltaPosition_Injected_m3057738582_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_get_deltaPosition_Injected_m3057738582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_deltaPosition_Injected_m3057738582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_deltaPosition_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::get_deltaRotation_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_deltaRotation_Injected_m2558147056 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_deltaRotation_Injected_m2558147056_ftn) (Animator_t434523843 *, Quaternion_t2301928331 *);
	static Animator_get_deltaRotation_Injected_m2558147056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_deltaRotation_Injected_m2558147056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_deltaRotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::get_rootPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_rootPosition_Injected_m2912740584 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_rootPosition_Injected_m2912740584_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_get_rootPosition_Injected_m2912740584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_rootPosition_Injected_m2912740584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_rootPosition_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::get_rootRotation_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_rootRotation_Injected_m1378784712 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_rootRotation_Injected_m1378784712_ftn) (Animator_t434523843 *, Quaternion_t2301928331 *);
	static Animator_get_rootRotation_Injected_m1378784712_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_rootRotation_Injected_m1378784712_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_rootRotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::get_bodyPositionInternal_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_bodyPositionInternal_Injected_m4243447051 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_bodyPositionInternal_Injected_m4243447051_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_get_bodyPositionInternal_Injected_m4243447051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_bodyPositionInternal_Injected_m4243447051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_bodyPositionInternal_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::set_bodyPositionInternal_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyPositionInternal_Injected_m1654945682 (Animator_t434523843 * __this, Vector3_t3722313464 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_bodyPositionInternal_Injected_m1654945682_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_set_bodyPositionInternal_Injected_m1654945682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_bodyPositionInternal_Injected_m1654945682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_bodyPositionInternal_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::get_bodyRotationInternal_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_bodyRotationInternal_Injected_m1688249227 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_bodyRotationInternal_Injected_m1688249227_ftn) (Animator_t434523843 *, Quaternion_t2301928331 *);
	static Animator_get_bodyRotationInternal_Injected_m1688249227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_bodyRotationInternal_Injected_m1688249227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_bodyRotationInternal_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::set_bodyRotationInternal_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_set_bodyRotationInternal_Injected_m139719495 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Animator_set_bodyRotationInternal_Injected_m139719495_ftn) (Animator_t434523843 *, Quaternion_t2301928331 *);
	static Animator_set_bodyRotationInternal_Injected_m139719495_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_bodyRotationInternal_Injected_m139719495_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_bodyRotationInternal_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::GetGoalPosition_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetGoalPosition_Injected_m3263093872 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*Animator_GetGoalPosition_Injected_m3263093872_ftn) (Animator_t434523843 *, int32_t, Vector3_t3722313464 *);
	static Animator_GetGoalPosition_Injected_m3263093872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetGoalPosition_Injected_m3263093872_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetGoalPosition_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___goal0, ___ret1);
}
// System.Void UnityEngine.Animator::SetGoalPosition_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalPosition_Injected_m3736935077 (Animator_t434523843 * __this, int32_t ___goal0, Vector3_t3722313464 * ___goalPosition1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetGoalPosition_Injected_m3736935077_ftn) (Animator_t434523843 *, int32_t, Vector3_t3722313464 *);
	static Animator_SetGoalPosition_Injected_m3736935077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetGoalPosition_Injected_m3736935077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetGoalPosition_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___goal0, ___goalPosition1);
}
// System.Void UnityEngine.Animator::GetGoalRotation_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_GetGoalRotation_Injected_m456765294 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331 * ___ret1, const RuntimeMethod* method)
{
	typedef void (*Animator_GetGoalRotation_Injected_m456765294_ftn) (Animator_t434523843 *, int32_t, Quaternion_t2301928331 *);
	static Animator_GetGoalRotation_Injected_m456765294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetGoalRotation_Injected_m456765294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetGoalRotation_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___goal0, ___ret1);
}
// System.Void UnityEngine.Animator::SetGoalRotation_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetGoalRotation_Injected_m2163733550 (Animator_t434523843 * __this, int32_t ___goal0, Quaternion_t2301928331 * ___goalRotation1, const RuntimeMethod* method)
{
	typedef void (*Animator_SetGoalRotation_Injected_m2163733550_ftn) (Animator_t434523843 *, int32_t, Quaternion_t2301928331 *);
	static Animator_SetGoalRotation_Injected_m2163733550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetGoalRotation_Injected_m2163733550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetGoalRotation_Injected(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___goal0, ___goalRotation1);
}
// System.Void UnityEngine.Animator::SetLookAtPositionInternal_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetLookAtPositionInternal_Injected_m733885560 (Animator_t434523843 * __this, Vector3_t3722313464 * ___lookAtPosition0, const RuntimeMethod* method)
{
	typedef void (*Animator_SetLookAtPositionInternal_Injected_m733885560_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_SetLookAtPositionInternal_Injected_m733885560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLookAtPositionInternal_Injected_m733885560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLookAtPositionInternal_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___lookAtPosition0);
}
// System.Void UnityEngine.Animator::get_pivotPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_pivotPosition_Injected_m3973723677 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_pivotPosition_Injected_m3973723677_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_get_pivotPosition_Injected_m3973723677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_pivotPosition_Injected_m3973723677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_pivotPosition_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::MatchTarget_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Animator_MatchTarget_Injected_m291306687 (Animator_t434523843 * __this, Vector3_t3722313464 * ___matchPosition0, Quaternion_t2301928331 * ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t554846203 * ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const RuntimeMethod* method)
{
	typedef void (*Animator_MatchTarget_Injected_m291306687_ftn) (Animator_t434523843 *, Vector3_t3722313464 *, Quaternion_t2301928331 *, int32_t, MatchTargetWeightMask_t554846203 *, float, float);
	static Animator_MatchTarget_Injected_m291306687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_MatchTarget_Injected_m291306687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::MatchTarget_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___matchPosition0, ___matchRotation1, ___targetBodyPart2, ___weightMask3, ___startNormalizedTime4, ___targetNormalizedTime5);
}
// System.Void UnityEngine.Animator::get_targetPosition_Injected(UnityEngine.Vector3&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_targetPosition_Injected_m1456028510 (Animator_t434523843 * __this, Vector3_t3722313464 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_targetPosition_Injected_m1456028510_ftn) (Animator_t434523843 *, Vector3_t3722313464 *);
	static Animator_get_targetPosition_Injected_m1456028510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_targetPosition_Injected_m1456028510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_targetPosition_Injected(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Animator::get_targetRotation_Injected(UnityEngine.Quaternion&)
extern "C" IL2CPP_METHOD_ATTR void Animator_get_targetRotation_Injected_m2584699430 (Animator_t434523843 * __this, Quaternion_t2301928331 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Animator_get_targetRotation_Injected_m2584699430_ftn) (Animator_t434523843 *, Quaternion_t2301928331 *);
	static Animator_get_targetRotation_Injected_m2584699430_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_targetRotation_Injected_m2584699430_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_targetRotation_Injected(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___ret0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AnimatorOverrideController::OnInvalidateOverrideController(UnityEngine.AnimatorOverrideController)
extern "C" IL2CPP_METHOD_ATTR void AnimatorOverrideController_OnInvalidateOverrideController_m3380737075 (RuntimeObject * __this /* static, unused */, AnimatorOverrideController_t3582062219 * ___controller0, const RuntimeMethod* method)
{
	{
		AnimatorOverrideController_t3582062219 * L_0 = ___controller0;
		NullCheck(L_0);
		OnOverrideControllerDirtyCallback_t1307045488 * L_1 = L_0->get_OnOverrideControllerDirty_4();
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		AnimatorOverrideController_t3582062219 * L_2 = ___controller0;
		NullCheck(L_2);
		OnOverrideControllerDirtyCallback_t1307045488 * L_3 = L_2->get_OnOverrideControllerDirty_4();
		NullCheck(L_3);
		OnOverrideControllerDirtyCallback_Invoke_m764065723(L_3, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_OnOverrideControllerDirtyCallback_t1307045488 (OnOverrideControllerDirtyCallback_t1307045488 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnOverrideControllerDirtyCallback__ctor_m2479189580 (OnOverrideControllerDirtyCallback_t1307045488 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback::Invoke()
extern "C" IL2CPP_METHOD_ATTR void OnOverrideControllerDirtyCallback_Invoke_m764065723 (OnOverrideControllerDirtyCallback_t1307045488 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnOverrideControllerDirtyCallback_Invoke_m764065723((OnOverrideControllerDirtyCallback_t1307045488 *)__this->get_prev_9(), method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
		{
			// open
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
			}
		}
		else
		{
			// closed
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
			}
		}
	}
	else
	{
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnOverrideControllerDirtyCallback_BeginInvoke_m3152352859 (OnOverrideControllerDirtyCallback_t1307045488 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnOverrideControllerDirtyCallback_EndInvoke_m2432606376 (OnOverrideControllerDirtyCallback_t1307045488 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_IsName_m3393819976 (AnimatorStateInfo_t509032636 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_m_FullPath_2();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_m_Name_0();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_m_Path_1();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		V_1 = (bool)G_B4_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
extern "C"  bool AnimatorStateInfo_IsName_m3393819976_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_IsName_m3393819976(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorStateInfo_get_fullPathHash_m2978085309 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FullPath_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m2978085309_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_fullPathHash_m2978085309(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorStateInfo_get_shortNameHash_m3578045446 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Name_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m3578045446_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_shortNameHash_m3578045446(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" IL2CPP_METHOD_ATTR float AnimatorStateInfo_get_normalizedTime_m2701390466 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_normalizedTime_m2701390466_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_normalizedTime_m2701390466(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" IL2CPP_METHOD_ATTR float AnimatorStateInfo_get_length_m130221364 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Length_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_length_m130221364_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_length_m130221364(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorStateInfo_get_tagHash_m1760199943 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Tag_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m1760199943_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_tagHash_m1760199943(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_IsTag_m1942726747 (AnimatorStateInfo_t509032636 * __this, String_t* ___tag0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___tag0;
		int32_t L_1 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Tag_7();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorStateInfo_IsTag_m1942726747_AdjustorThunk (RuntimeObject * __this, String_t* ___tag0, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_IsTag_m1942726747(_thisAdjusted, ___tag0, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" IL2CPP_METHOD_ATTR bool AnimatorStateInfo_get_loop_m3780967345 (AnimatorStateInfo_t509032636 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Loop_8();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorStateInfo_get_loop_m3780967345_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_loop_m3780967345(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(const AnimatorTransitionInfo_t2534804151& unmarshaled, AnimatorTransitionInfo_t2534804151_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_HasFixedDuration_3 = static_cast<int32_t>(unmarshaled.get_m_HasFixedDuration_3());
	marshaled.___m_Duration_4 = unmarshaled.get_m_Duration_4();
	marshaled.___m_NormalizedTime_5 = unmarshaled.get_m_NormalizedTime_5();
	marshaled.___m_AnyState_6 = static_cast<int32_t>(unmarshaled.get_m_AnyState_6());
	marshaled.___m_TransitionType_7 = unmarshaled.get_m_TransitionType_7();
}
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(const AnimatorTransitionInfo_t2534804151_marshaled_pinvoke& marshaled, AnimatorTransitionInfo_t2534804151& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	bool unmarshaled_m_HasFixedDuration_temp_3 = false;
	unmarshaled_m_HasFixedDuration_temp_3 = static_cast<bool>(marshaled.___m_HasFixedDuration_3);
	unmarshaled.set_m_HasFixedDuration_3(unmarshaled_m_HasFixedDuration_temp_3);
	float unmarshaled_m_Duration_temp_4 = 0.0f;
	unmarshaled_m_Duration_temp_4 = marshaled.___m_Duration_4;
	unmarshaled.set_m_Duration_4(unmarshaled_m_Duration_temp_4);
	float unmarshaled_m_NormalizedTime_temp_5 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_5 = marshaled.___m_NormalizedTime_5;
	unmarshaled.set_m_NormalizedTime_5(unmarshaled_m_NormalizedTime_temp_5);
	bool unmarshaled_m_AnyState_temp_6 = false;
	unmarshaled_m_AnyState_temp_6 = static_cast<bool>(marshaled.___m_AnyState_6);
	unmarshaled.set_m_AnyState_6(unmarshaled_m_AnyState_temp_6);
	int32_t unmarshaled_m_TransitionType_temp_7 = 0;
	unmarshaled_m_TransitionType_temp_7 = marshaled.___m_TransitionType_7;
	unmarshaled.set_m_TransitionType_7(unmarshaled_m_TransitionType_temp_7);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(AnimatorTransitionInfo_t2534804151_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_com(const AnimatorTransitionInfo_t2534804151& unmarshaled, AnimatorTransitionInfo_t2534804151_marshaled_com& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_HasFixedDuration_3 = static_cast<int32_t>(unmarshaled.get_m_HasFixedDuration_3());
	marshaled.___m_Duration_4 = unmarshaled.get_m_Duration_4();
	marshaled.___m_NormalizedTime_5 = unmarshaled.get_m_NormalizedTime_5();
	marshaled.___m_AnyState_6 = static_cast<int32_t>(unmarshaled.get_m_AnyState_6());
	marshaled.___m_TransitionType_7 = unmarshaled.get_m_TransitionType_7();
}
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_com_back(const AnimatorTransitionInfo_t2534804151_marshaled_com& marshaled, AnimatorTransitionInfo_t2534804151& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	bool unmarshaled_m_HasFixedDuration_temp_3 = false;
	unmarshaled_m_HasFixedDuration_temp_3 = static_cast<bool>(marshaled.___m_HasFixedDuration_3);
	unmarshaled.set_m_HasFixedDuration_3(unmarshaled_m_HasFixedDuration_temp_3);
	float unmarshaled_m_Duration_temp_4 = 0.0f;
	unmarshaled_m_Duration_temp_4 = marshaled.___m_Duration_4;
	unmarshaled.set_m_Duration_4(unmarshaled_m_Duration_temp_4);
	float unmarshaled_m_NormalizedTime_temp_5 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_5 = marshaled.___m_NormalizedTime_5;
	unmarshaled.set_m_NormalizedTime_5(unmarshaled_m_NormalizedTime_temp_5);
	bool unmarshaled_m_AnyState_temp_6 = false;
	unmarshaled_m_AnyState_temp_6 = static_cast<bool>(marshaled.___m_AnyState_6);
	unmarshaled.set_m_AnyState_6(unmarshaled_m_AnyState_temp_6);
	int32_t unmarshaled_m_TransitionType_temp_7 = 0;
	unmarshaled_m_TransitionType_temp_7 = marshaled.___m_TransitionType_7;
	unmarshaled.set_m_TransitionType_7(unmarshaled_m_TransitionType_temp_7);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_com_cleanup(AnimatorTransitionInfo_t2534804151_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorTransitionInfo_IsName_m399618195 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Name_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___name0;
		int32_t L_4 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_FullPath_0();
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 1;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsName_m399618195_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_IsName_m399618195(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" IL2CPP_METHOD_ATTR bool AnimatorTransitionInfo_IsUserName_m1292791005 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m1666053228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_UserName_1();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsUserName_m1292791005_AdjustorThunk (RuntimeObject * __this, String_t* ___name0, const RuntimeMethod* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_IsUserName_m1292791005(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorTransitionInfo_get_nameHash_m3336626857 (AnimatorTransitionInfo_t2534804151 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Name_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m3336626857_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_nameHash_m3336626857(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" IL2CPP_METHOD_ATTR int32_t AnimatorTransitionInfo_get_userNameHash_m2248997669 (AnimatorTransitionInfo_t2534804151 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_UserName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m2248997669_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_userNameHash_m2248997669(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" IL2CPP_METHOD_ATTR float AnimatorTransitionInfo_get_normalizedTime_m1885586090 (AnimatorTransitionInfo_t2534804151 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_NormalizedTime_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m1885586090_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_normalizedTime_m1885586090(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Experimental.Animations.AnimationScriptPlayable::.ctor(UnityEngine.Playables.PlayableHandle)
extern "C" IL2CPP_METHOD_ATTR void AnimationScriptPlayable__ctor_m1047518474 (AnimationScriptPlayable_t1303525964 * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationScriptPlayable__ctor_m1047518474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PlayableHandle_IsValid_m777349566((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = PlayableHandle_IsPlayableOfType_TisAnimationScriptPlayable_t1303525964_m1992139667((PlayableHandle_t1095853803 *)(&___handle0), /*hidden argument*/PlayableHandle_IsPlayableOfType_TisAnimationScriptPlayable_t1303525964_m1992139667_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		InvalidCastException_t3927145244 * L_2 = (InvalidCastException_t3927145244 *)il2cpp_codegen_object_new(InvalidCastException_t3927145244_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m318645277(L_2, _stringLiteral3192108539, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, AnimationScriptPlayable__ctor_m1047518474_RuntimeMethod_var);
	}

IL_0025:
	{
	}

IL_0026:
	{
		PlayableHandle_t1095853803  L_3 = ___handle0;
		__this->set_m_Handle_0(L_3);
		return;
	}
}
extern "C"  void AnimationScriptPlayable__ctor_m1047518474_AdjustorThunk (RuntimeObject * __this, PlayableHandle_t1095853803  ___handle0, const RuntimeMethod* method)
{
	AnimationScriptPlayable_t1303525964 * _thisAdjusted = reinterpret_cast<AnimationScriptPlayable_t1303525964 *>(__this + 1);
	AnimationScriptPlayable__ctor_m1047518474(_thisAdjusted, ___handle0, method);
}
// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Animations.AnimationScriptPlayable::GetHandle()
extern "C" IL2CPP_METHOD_ATTR PlayableHandle_t1095853803  AnimationScriptPlayable_GetHandle_m632811930 (AnimationScriptPlayable_t1303525964 * __this, const RuntimeMethod* method)
{
	PlayableHandle_t1095853803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		PlayableHandle_t1095853803  L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		PlayableHandle_t1095853803  L_1 = V_0;
		return L_1;
	}
}
extern "C"  PlayableHandle_t1095853803  AnimationScriptPlayable_GetHandle_m632811930_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	AnimationScriptPlayable_t1303525964 * _thisAdjusted = reinterpret_cast<AnimationScriptPlayable_t1303525964 *>(__this + 1);
	return AnimationScriptPlayable_GetHandle_m632811930(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Experimental.Animations.AnimationScriptPlayable::Equals(UnityEngine.Experimental.Animations.AnimationScriptPlayable)
extern "C" IL2CPP_METHOD_ATTR bool AnimationScriptPlayable_Equals_m534223357 (AnimationScriptPlayable_t1303525964 * __this, AnimationScriptPlayable_t1303525964  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t1095853803  L_0 = AnimationScriptPlayable_GetHandle_m632811930((AnimationScriptPlayable_t1303525964 *)__this, /*hidden argument*/NULL);
		PlayableHandle_t1095853803  L_1 = AnimationScriptPlayable_GetHandle_m632811930((AnimationScriptPlayable_t1303525964 *)(&___other0), /*hidden argument*/NULL);
		bool L_2 = PlayableHandle_op_Equality_m3344837515(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimationScriptPlayable_Equals_m534223357_AdjustorThunk (RuntimeObject * __this, AnimationScriptPlayable_t1303525964  ___other0, const RuntimeMethod* method)
{
	AnimationScriptPlayable_t1303525964 * _thisAdjusted = reinterpret_cast<AnimationScriptPlayable_t1303525964 *>(__this + 1);
	return AnimationScriptPlayable_Equals_m534223357(_thisAdjusted, ___other0, method);
}
// System.Void UnityEngine.Experimental.Animations.AnimationScriptPlayable::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AnimationScriptPlayable__cctor_m1482222037 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationScriptPlayable__cctor_m1482222037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayableHandle_t1095853803  L_0 = PlayableHandle_get_Null_m218234861(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnimationScriptPlayable_t1303525964  L_1;
		memset(&L_1, 0, sizeof(L_1));
		AnimationScriptPlayable__ctor_m1047518474((&L_1), L_0, /*hidden argument*/NULL);
		((AnimationScriptPlayable_t1303525964_StaticFields*)il2cpp_codegen_static_fields_for(AnimationScriptPlayable_t1303525964_il2cpp_TypeInfo_var))->set_m_NullPlayable_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t2465339518_marshal_pinvoke(const HumanBone_t2465339518& unmarshaled, HumanBone_t2465339518_marshaled_pinvoke& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_BoneName_0());
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_m_HumanName_1());
	marshaled.___limit_2 = unmarshaled.get_limit_2();
}
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(const HumanBone_t2465339518_marshaled_pinvoke& marshaled, HumanBone_t2465339518& unmarshaled)
{
	unmarshaled.set_m_BoneName_0(il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0));
	unmarshaled.set_m_HumanName_1(il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1));
	HumanLimit_t2901552972  unmarshaled_limit_temp_2;
	memset(&unmarshaled_limit_temp_2, 0, sizeof(unmarshaled_limit_temp_2));
	unmarshaled_limit_temp_2 = marshaled.___limit_2;
	unmarshaled.set_limit_2(unmarshaled_limit_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(HumanBone_t2465339518_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t2465339518_marshal_com(const HumanBone_t2465339518& unmarshaled, HumanBone_t2465339518_marshaled_com& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_BoneName_0());
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_HumanName_1());
	marshaled.___limit_2 = unmarshaled.get_limit_2();
}
extern "C" void HumanBone_t2465339518_marshal_com_back(const HumanBone_t2465339518_marshaled_com& marshaled, HumanBone_t2465339518& unmarshaled)
{
	unmarshaled.set_m_BoneName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_BoneName_0));
	unmarshaled.set_m_HumanName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___m_HumanName_1));
	HumanLimit_t2901552972  unmarshaled_limit_temp_2;
	memset(&unmarshaled_limit_temp_2, 0, sizeof(unmarshaled_limit_temp_2));
	unmarshaled_limit_temp_2 = marshaled.___limit_2;
	unmarshaled.set_limit_2(unmarshaled_limit_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t2465339518_marshal_com_cleanup(HumanBone_t2465339518_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UnityEngine.HumanTrait::GetBoneIndexFromMono(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t HumanTrait_GetBoneIndexFromMono_m1872291878 (RuntimeObject * __this /* static, unused */, int32_t ___humanId0, const RuntimeMethod* method)
{
	typedef int32_t (*HumanTrait_GetBoneIndexFromMono_m1872291878_ftn) (int32_t);
	static HumanTrait_GetBoneIndexFromMono_m1872291878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HumanTrait_GetBoneIndexFromMono_m1872291878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HumanTrait::GetBoneIndexFromMono(System.Int32)");
	int32_t retVal = _il2cpp_icall_func(___humanId0);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.MatchTargetWeightMask::.ctor(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MatchTargetWeightMask__ctor_m4251631450 (MatchTargetWeightMask_t554846203 * __this, Vector3_t3722313464  ___positionXYZWeight0, float ___rotationWeight1, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___positionXYZWeight0;
		__this->set_m_PositionXYZWeight_0(L_0);
		float L_1 = ___rotationWeight1;
		__this->set_m_RotationWeight_1(L_1);
		return;
	}
}
extern "C"  void MatchTargetWeightMask__ctor_m4251631450_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ___positionXYZWeight0, float ___rotationWeight1, const RuntimeMethod* method)
{
	MatchTargetWeightMask_t554846203 * _thisAdjusted = reinterpret_cast<MatchTargetWeightMask_t554846203 *>(__this + 1);
	MatchTargetWeightMask__ctor_m4251631450(_thisAdjusted, ___positionXYZWeight0, ___rotationWeight1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Motion::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Motion__ctor_m1337281595 (Motion_t1110556653 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Motion__ctor_m1337281595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object__ctor_m1087895580(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Motion::get_isLooping()
extern "C" IL2CPP_METHOD_ATTR bool Motion_get_isLooping_m3446136793 (Motion_t1110556653 * __this, const RuntimeMethod* method)
{
	typedef bool (*Motion_get_isLooping_m3446136793_ftn) (Motion_t1110556653 *);
	static Motion_get_isLooping_m3446136793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Motion_get_isLooping_m3446136793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Motion::get_isLooping()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(const SkeletonBone_t4134054672& unmarshaled, SkeletonBone_t4134054672_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(const SkeletonBone_t4134054672_marshaled_pinvoke& marshaled, SkeletonBone_t4134054672& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_string_result(marshaled.___parentName_1));
	Vector3_t3722313464  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t2301928331  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t3722313464  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(SkeletonBone_t4134054672_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t4134054672_marshal_com(const SkeletonBone_t4134054672& unmarshaled, SkeletonBone_t4134054672_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	marshaled.___parentName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_parentName_1());
	marshaled.___position_2 = unmarshaled.get_position_2();
	marshaled.___rotation_3 = unmarshaled.get_rotation_3();
	marshaled.___scale_4 = unmarshaled.get_scale_4();
}
extern "C" void SkeletonBone_t4134054672_marshal_com_back(const SkeletonBone_t4134054672_marshaled_com& marshaled, SkeletonBone_t4134054672& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	unmarshaled.set_parentName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___parentName_1));
	Vector3_t3722313464  unmarshaled_position_temp_2;
	memset(&unmarshaled_position_temp_2, 0, sizeof(unmarshaled_position_temp_2));
	unmarshaled_position_temp_2 = marshaled.___position_2;
	unmarshaled.set_position_2(unmarshaled_position_temp_2);
	Quaternion_t2301928331  unmarshaled_rotation_temp_3;
	memset(&unmarshaled_rotation_temp_3, 0, sizeof(unmarshaled_rotation_temp_3));
	unmarshaled_rotation_temp_3 = marshaled.___rotation_3;
	unmarshaled.set_rotation_3(unmarshaled_rotation_temp_3);
	Vector3_t3722313464  unmarshaled_scale_temp_4;
	memset(&unmarshaled_scale_temp_4, 0, sizeof(unmarshaled_scale_temp_4));
	unmarshaled_scale_temp_4 = marshaled.___scale_4;
	unmarshaled.set_scale_4(unmarshaled_scale_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t4134054672_marshal_com_cleanup(SkeletonBone_t4134054672_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___parentName_1);
	marshaled.___parentName_1 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
