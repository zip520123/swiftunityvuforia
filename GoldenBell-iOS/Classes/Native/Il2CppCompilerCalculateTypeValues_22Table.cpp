﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// FsmTemplate
struct FsmTemplate_t1663266674;
// HutongGames.PlayMaker.ActionData
struct ActionData_t1922786972;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t2002857066;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t1155011270;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t1111370293;
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t1010932050;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1692568573;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t919699796;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2511618479;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t719957643;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2904461592;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t3265252880;
// HutongGames.PlayMaker.FsmLogEntry[]
struct FsmLogEntryU5BU5D_t3257799173;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t3870809373;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t635502296;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t495607758;
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t3969342952;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t3801304101;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1918078376;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t3458670015;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t2313143784;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1340699069;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t3577734832;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t3349589544;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t1097319912;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t402703848;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Action`1<HutongGames.PlayMaker.FsmState>
struct Action_1_t1898533;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>
struct Dictionary_2_t4026181381;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// System.Collections.Generic.Dictionary`2<System.String,HutongGames.PlayMaker.FsmEvent>
struct Dictionary_2_t3521556181;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent>
struct List_1_t1164733674;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct List_1_t1304255270;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>
struct List_1_t913407328;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLog>
struct List_1_t442360326;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry>
struct List_1_t517607234;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction>
struct List_1_t978411547;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t3491343620;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t1327982029;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2475741330;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t521873611;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct List_1_t2329214678;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// System.Comparison`1<UnityEngine.RaycastHit>
struct Comparison_1_t830933145;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t1896830045;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t2792222854;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Text.UTF8Encoding
struct UTF8Encoding_t3956466879;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.Collision2D
struct Collision2D_t2842956331;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t2019268878;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t3867320123;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t1977848392;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t2658898854;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t3373214253;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t972960537;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t2311673543;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t3277009892;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t3587542510;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t3912835512;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t3111972472;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t64614563;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t3995630009;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t2867327688;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t3256600500;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t2886331738;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t955952873;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t1475332338;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t2950825503;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t3190347560;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t384203932;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Joint2D
struct Joint2D_t4180440564;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t3571286806;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t1121741130;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t1856710240;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t231414508;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;




#ifndef U3CMODULEU3E_T692745553_H
#define U3CMODULEU3E_T692745553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745553 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745553_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef RECTEXTENSIONS_T2063872102_H
#define RECTEXTENSIONS_T2063872102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Extensions.RectExtensions
struct  RectExtensions_t2063872102  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTEXTENSIONS_T2063872102_H
#ifndef TEXTUREEXTENSIONS_T1673782326_H
#define TEXTUREEXTENSIONS_T1673782326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Extensions.TextureExtensions
struct  TextureExtensions_t1673782326  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREEXTENSIONS_T1673782326_H
#ifndef FSMEVENT_T3736299882_H
#define FSMEVENT_T3736299882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEvent
struct  FsmEvent_t3736299882  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmEvent::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmEvent::isSystemEvent
	bool ___isSystemEvent_3;
	// System.Boolean HutongGames.PlayMaker.FsmEvent::isGlobal
	bool ___isGlobal_4;
	// System.String HutongGames.PlayMaker.FsmEvent::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_isSystemEvent_3() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___isSystemEvent_3)); }
	inline bool get_isSystemEvent_3() const { return ___isSystemEvent_3; }
	inline bool* get_address_of_isSystemEvent_3() { return &___isSystemEvent_3; }
	inline void set_isSystemEvent_3(bool value)
	{
		___isSystemEvent_3 = value;
	}

	inline static int32_t get_offset_of_isGlobal_4() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___isGlobal_4)); }
	inline bool get_isGlobal_4() const { return ___isGlobal_4; }
	inline bool* get_address_of_isGlobal_4() { return &___isGlobal_4; }
	inline void set_isGlobal_4(bool value)
	{
		___isGlobal_4 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882, ___U3CPathU3Ek__BackingField_5)); }
	inline String_t* get_U3CPathU3Ek__BackingField_5() const { return ___U3CPathU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_5() { return &___U3CPathU3Ek__BackingField_5; }
	inline void set_U3CPathU3Ek__BackingField_5(String_t* value)
	{
		___U3CPathU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_5), value);
	}
};

struct FsmEvent_t3736299882_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.FsmEvent::_eventLookup
	Dictionary_2_t3521556181 * ____eventLookup_0;
	// System.Object HutongGames.PlayMaker.FsmEvent::syncObj
	RuntimeObject * ___syncObj_1;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<BecameInvisible>k__BackingField
	FsmEvent_t3736299882 * ___U3CBecameInvisibleU3Ek__BackingField_6;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<BecameVisible>k__BackingField
	FsmEvent_t3736299882 * ___U3CBecameVisibleU3Ek__BackingField_7;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionEnterU3Ek__BackingField_8;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionExitU3Ek__BackingField_9;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionStay>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionStayU3Ek__BackingField_10;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionEnter2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionEnter2DU3Ek__BackingField_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionExit2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionExit2DU3Ek__BackingField_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<CollisionStay2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CCollisionStay2DU3Ek__BackingField_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ControllerColliderHit>k__BackingField
	FsmEvent_t3736299882 * ___U3CControllerColliderHitU3Ek__BackingField_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<Finished>k__BackingField
	FsmEvent_t3736299882 * ___U3CFinishedU3Ek__BackingField_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<LevelLoaded>k__BackingField
	FsmEvent_t3736299882 * ___U3CLevelLoadedU3Ek__BackingField_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseDown>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseDownU3Ek__BackingField_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseDragU3Ek__BackingField_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseEnterU3Ek__BackingField_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseExitU3Ek__BackingField_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseOver>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseOverU3Ek__BackingField_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseUp>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseUpU3Ek__BackingField_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MouseUpAsButton>k__BackingField
	FsmEvent_t3736299882 * ___U3CMouseUpAsButtonU3Ek__BackingField_23;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerEnterU3Ek__BackingField_24;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerExitU3Ek__BackingField_25;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerStay>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerStayU3Ek__BackingField_26;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerEnter2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerEnter2DU3Ek__BackingField_27;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerExit2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerExit2DU3Ek__BackingField_28;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<TriggerStay2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CTriggerStay2DU3Ek__BackingField_29;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ApplicationFocus>k__BackingField
	FsmEvent_t3736299882 * ___U3CApplicationFocusU3Ek__BackingField_30;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ApplicationPause>k__BackingField
	FsmEvent_t3736299882 * ___U3CApplicationPauseU3Ek__BackingField_31;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ApplicationQuit>k__BackingField
	FsmEvent_t3736299882 * ___U3CApplicationQuitU3Ek__BackingField_32;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ParticleCollision>k__BackingField
	FsmEvent_t3736299882 * ___U3CParticleCollisionU3Ek__BackingField_33;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<JointBreak>k__BackingField
	FsmEvent_t3736299882 * ___U3CJointBreakU3Ek__BackingField_34;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<JointBreak2D>k__BackingField
	FsmEvent_t3736299882 * ___U3CJointBreak2DU3Ek__BackingField_35;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<Disable>k__BackingField
	FsmEvent_t3736299882 * ___U3CDisableU3Ek__BackingField_36;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<PlayerConnected>k__BackingField
	FsmEvent_t3736299882 * ___U3CPlayerConnectedU3Ek__BackingField_37;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ServerInitialized>k__BackingField
	FsmEvent_t3736299882 * ___U3CServerInitializedU3Ek__BackingField_38;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<ConnectedToServer>k__BackingField
	FsmEvent_t3736299882 * ___U3CConnectedToServerU3Ek__BackingField_39;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<PlayerDisconnected>k__BackingField
	FsmEvent_t3736299882 * ___U3CPlayerDisconnectedU3Ek__BackingField_40;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<DisconnectedFromServer>k__BackingField
	FsmEvent_t3736299882 * ___U3CDisconnectedFromServerU3Ek__BackingField_41;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<FailedToConnect>k__BackingField
	FsmEvent_t3736299882 * ___U3CFailedToConnectU3Ek__BackingField_42;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<FailedToConnectToMasterServer>k__BackingField
	FsmEvent_t3736299882 * ___U3CFailedToConnectToMasterServerU3Ek__BackingField_43;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<MasterServerEvent>k__BackingField
	FsmEvent_t3736299882 * ___U3CMasterServerEventU3Ek__BackingField_44;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<NetworkInstantiate>k__BackingField
	FsmEvent_t3736299882 * ___U3CNetworkInstantiateU3Ek__BackingField_45;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiBeginDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiBeginDragU3Ek__BackingField_46;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiDragU3Ek__BackingField_47;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiEndDrag>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiEndDragU3Ek__BackingField_48;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiClick>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiClickU3Ek__BackingField_49;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiDrop>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiDropU3Ek__BackingField_50;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerClick>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerClickU3Ek__BackingField_51;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerDown>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerDownU3Ek__BackingField_52;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerEnter>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerEnterU3Ek__BackingField_53;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerExit>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerExitU3Ek__BackingField_54;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiPointerUp>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiPointerUpU3Ek__BackingField_55;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiBoolValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiBoolValueChangedU3Ek__BackingField_56;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiFloatValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiFloatValueChangedU3Ek__BackingField_57;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiIntValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiIntValueChangedU3Ek__BackingField_58;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiVector2ValueChanged>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiVector2ValueChangedU3Ek__BackingField_59;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::<UiEndEdit>k__BackingField
	FsmEvent_t3736299882 * ___U3CUiEndEditU3Ek__BackingField_60;

public:
	inline static int32_t get_offset_of__eventLookup_0() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ____eventLookup_0)); }
	inline Dictionary_2_t3521556181 * get__eventLookup_0() const { return ____eventLookup_0; }
	inline Dictionary_2_t3521556181 ** get_address_of__eventLookup_0() { return &____eventLookup_0; }
	inline void set__eventLookup_0(Dictionary_2_t3521556181 * value)
	{
		____eventLookup_0 = value;
		Il2CppCodeGenWriteBarrier((&____eventLookup_0), value);
	}

	inline static int32_t get_offset_of_syncObj_1() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___syncObj_1)); }
	inline RuntimeObject * get_syncObj_1() const { return ___syncObj_1; }
	inline RuntimeObject ** get_address_of_syncObj_1() { return &___syncObj_1; }
	inline void set_syncObj_1(RuntimeObject * value)
	{
		___syncObj_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncObj_1), value);
	}

	inline static int32_t get_offset_of_U3CBecameInvisibleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CBecameInvisibleU3Ek__BackingField_6)); }
	inline FsmEvent_t3736299882 * get_U3CBecameInvisibleU3Ek__BackingField_6() const { return ___U3CBecameInvisibleU3Ek__BackingField_6; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CBecameInvisibleU3Ek__BackingField_6() { return &___U3CBecameInvisibleU3Ek__BackingField_6; }
	inline void set_U3CBecameInvisibleU3Ek__BackingField_6(FsmEvent_t3736299882 * value)
	{
		___U3CBecameInvisibleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBecameInvisibleU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CBecameVisibleU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CBecameVisibleU3Ek__BackingField_7)); }
	inline FsmEvent_t3736299882 * get_U3CBecameVisibleU3Ek__BackingField_7() const { return ___U3CBecameVisibleU3Ek__BackingField_7; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CBecameVisibleU3Ek__BackingField_7() { return &___U3CBecameVisibleU3Ek__BackingField_7; }
	inline void set_U3CBecameVisibleU3Ek__BackingField_7(FsmEvent_t3736299882 * value)
	{
		___U3CBecameVisibleU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBecameVisibleU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CCollisionEnterU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionEnterU3Ek__BackingField_8)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionEnterU3Ek__BackingField_8() const { return ___U3CCollisionEnterU3Ek__BackingField_8; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionEnterU3Ek__BackingField_8() { return &___U3CCollisionEnterU3Ek__BackingField_8; }
	inline void set_U3CCollisionEnterU3Ek__BackingField_8(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionEnterU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionEnterU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CCollisionExitU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionExitU3Ek__BackingField_9)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionExitU3Ek__BackingField_9() const { return ___U3CCollisionExitU3Ek__BackingField_9; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionExitU3Ek__BackingField_9() { return &___U3CCollisionExitU3Ek__BackingField_9; }
	inline void set_U3CCollisionExitU3Ek__BackingField_9(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionExitU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionExitU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCollisionStayU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionStayU3Ek__BackingField_10)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionStayU3Ek__BackingField_10() const { return ___U3CCollisionStayU3Ek__BackingField_10; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionStayU3Ek__BackingField_10() { return &___U3CCollisionStayU3Ek__BackingField_10; }
	inline void set_U3CCollisionStayU3Ek__BackingField_10(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionStayU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionStayU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CCollisionEnter2DU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionEnter2DU3Ek__BackingField_11)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionEnter2DU3Ek__BackingField_11() const { return ___U3CCollisionEnter2DU3Ek__BackingField_11; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionEnter2DU3Ek__BackingField_11() { return &___U3CCollisionEnter2DU3Ek__BackingField_11; }
	inline void set_U3CCollisionEnter2DU3Ek__BackingField_11(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionEnter2DU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionEnter2DU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CCollisionExit2DU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionExit2DU3Ek__BackingField_12)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionExit2DU3Ek__BackingField_12() const { return ___U3CCollisionExit2DU3Ek__BackingField_12; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionExit2DU3Ek__BackingField_12() { return &___U3CCollisionExit2DU3Ek__BackingField_12; }
	inline void set_U3CCollisionExit2DU3Ek__BackingField_12(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionExit2DU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionExit2DU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CCollisionStay2DU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CCollisionStay2DU3Ek__BackingField_13)); }
	inline FsmEvent_t3736299882 * get_U3CCollisionStay2DU3Ek__BackingField_13() const { return ___U3CCollisionStay2DU3Ek__BackingField_13; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CCollisionStay2DU3Ek__BackingField_13() { return &___U3CCollisionStay2DU3Ek__BackingField_13; }
	inline void set_U3CCollisionStay2DU3Ek__BackingField_13(FsmEvent_t3736299882 * value)
	{
		___U3CCollisionStay2DU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionStay2DU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CControllerColliderHitU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CControllerColliderHitU3Ek__BackingField_14)); }
	inline FsmEvent_t3736299882 * get_U3CControllerColliderHitU3Ek__BackingField_14() const { return ___U3CControllerColliderHitU3Ek__BackingField_14; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CControllerColliderHitU3Ek__BackingField_14() { return &___U3CControllerColliderHitU3Ek__BackingField_14; }
	inline void set_U3CControllerColliderHitU3Ek__BackingField_14(FsmEvent_t3736299882 * value)
	{
		___U3CControllerColliderHitU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerColliderHitU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CFinishedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CFinishedU3Ek__BackingField_15)); }
	inline FsmEvent_t3736299882 * get_U3CFinishedU3Ek__BackingField_15() const { return ___U3CFinishedU3Ek__BackingField_15; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CFinishedU3Ek__BackingField_15() { return &___U3CFinishedU3Ek__BackingField_15; }
	inline void set_U3CFinishedU3Ek__BackingField_15(FsmEvent_t3736299882 * value)
	{
		___U3CFinishedU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinishedU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CLevelLoadedU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CLevelLoadedU3Ek__BackingField_16)); }
	inline FsmEvent_t3736299882 * get_U3CLevelLoadedU3Ek__BackingField_16() const { return ___U3CLevelLoadedU3Ek__BackingField_16; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CLevelLoadedU3Ek__BackingField_16() { return &___U3CLevelLoadedU3Ek__BackingField_16; }
	inline void set_U3CLevelLoadedU3Ek__BackingField_16(FsmEvent_t3736299882 * value)
	{
		___U3CLevelLoadedU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLevelLoadedU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CMouseDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseDownU3Ek__BackingField_17)); }
	inline FsmEvent_t3736299882 * get_U3CMouseDownU3Ek__BackingField_17() const { return ___U3CMouseDownU3Ek__BackingField_17; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseDownU3Ek__BackingField_17() { return &___U3CMouseDownU3Ek__BackingField_17; }
	inline void set_U3CMouseDownU3Ek__BackingField_17(FsmEvent_t3736299882 * value)
	{
		___U3CMouseDownU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseDownU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CMouseDragU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseDragU3Ek__BackingField_18)); }
	inline FsmEvent_t3736299882 * get_U3CMouseDragU3Ek__BackingField_18() const { return ___U3CMouseDragU3Ek__BackingField_18; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseDragU3Ek__BackingField_18() { return &___U3CMouseDragU3Ek__BackingField_18; }
	inline void set_U3CMouseDragU3Ek__BackingField_18(FsmEvent_t3736299882 * value)
	{
		___U3CMouseDragU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseDragU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CMouseEnterU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseEnterU3Ek__BackingField_19)); }
	inline FsmEvent_t3736299882 * get_U3CMouseEnterU3Ek__BackingField_19() const { return ___U3CMouseEnterU3Ek__BackingField_19; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseEnterU3Ek__BackingField_19() { return &___U3CMouseEnterU3Ek__BackingField_19; }
	inline void set_U3CMouseEnterU3Ek__BackingField_19(FsmEvent_t3736299882 * value)
	{
		___U3CMouseEnterU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseEnterU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CMouseExitU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseExitU3Ek__BackingField_20)); }
	inline FsmEvent_t3736299882 * get_U3CMouseExitU3Ek__BackingField_20() const { return ___U3CMouseExitU3Ek__BackingField_20; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseExitU3Ek__BackingField_20() { return &___U3CMouseExitU3Ek__BackingField_20; }
	inline void set_U3CMouseExitU3Ek__BackingField_20(FsmEvent_t3736299882 * value)
	{
		___U3CMouseExitU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseExitU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CMouseOverU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseOverU3Ek__BackingField_21)); }
	inline FsmEvent_t3736299882 * get_U3CMouseOverU3Ek__BackingField_21() const { return ___U3CMouseOverU3Ek__BackingField_21; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseOverU3Ek__BackingField_21() { return &___U3CMouseOverU3Ek__BackingField_21; }
	inline void set_U3CMouseOverU3Ek__BackingField_21(FsmEvent_t3736299882 * value)
	{
		___U3CMouseOverU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseOverU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CMouseUpU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseUpU3Ek__BackingField_22)); }
	inline FsmEvent_t3736299882 * get_U3CMouseUpU3Ek__BackingField_22() const { return ___U3CMouseUpU3Ek__BackingField_22; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseUpU3Ek__BackingField_22() { return &___U3CMouseUpU3Ek__BackingField_22; }
	inline void set_U3CMouseUpU3Ek__BackingField_22(FsmEvent_t3736299882 * value)
	{
		___U3CMouseUpU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseUpU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CMouseUpAsButtonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMouseUpAsButtonU3Ek__BackingField_23)); }
	inline FsmEvent_t3736299882 * get_U3CMouseUpAsButtonU3Ek__BackingField_23() const { return ___U3CMouseUpAsButtonU3Ek__BackingField_23; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMouseUpAsButtonU3Ek__BackingField_23() { return &___U3CMouseUpAsButtonU3Ek__BackingField_23; }
	inline void set_U3CMouseUpAsButtonU3Ek__BackingField_23(FsmEvent_t3736299882 * value)
	{
		___U3CMouseUpAsButtonU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMouseUpAsButtonU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CTriggerEnterU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerEnterU3Ek__BackingField_24)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerEnterU3Ek__BackingField_24() const { return ___U3CTriggerEnterU3Ek__BackingField_24; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerEnterU3Ek__BackingField_24() { return &___U3CTriggerEnterU3Ek__BackingField_24; }
	inline void set_U3CTriggerEnterU3Ek__BackingField_24(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerEnterU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerEnterU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CTriggerExitU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerExitU3Ek__BackingField_25)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerExitU3Ek__BackingField_25() const { return ___U3CTriggerExitU3Ek__BackingField_25; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerExitU3Ek__BackingField_25() { return &___U3CTriggerExitU3Ek__BackingField_25; }
	inline void set_U3CTriggerExitU3Ek__BackingField_25(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerExitU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerExitU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CTriggerStayU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerStayU3Ek__BackingField_26)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerStayU3Ek__BackingField_26() const { return ___U3CTriggerStayU3Ek__BackingField_26; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerStayU3Ek__BackingField_26() { return &___U3CTriggerStayU3Ek__BackingField_26; }
	inline void set_U3CTriggerStayU3Ek__BackingField_26(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerStayU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerStayU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CTriggerEnter2DU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerEnter2DU3Ek__BackingField_27)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerEnter2DU3Ek__BackingField_27() const { return ___U3CTriggerEnter2DU3Ek__BackingField_27; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerEnter2DU3Ek__BackingField_27() { return &___U3CTriggerEnter2DU3Ek__BackingField_27; }
	inline void set_U3CTriggerEnter2DU3Ek__BackingField_27(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerEnter2DU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerEnter2DU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CTriggerExit2DU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerExit2DU3Ek__BackingField_28)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerExit2DU3Ek__BackingField_28() const { return ___U3CTriggerExit2DU3Ek__BackingField_28; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerExit2DU3Ek__BackingField_28() { return &___U3CTriggerExit2DU3Ek__BackingField_28; }
	inline void set_U3CTriggerExit2DU3Ek__BackingField_28(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerExit2DU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerExit2DU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CTriggerStay2DU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CTriggerStay2DU3Ek__BackingField_29)); }
	inline FsmEvent_t3736299882 * get_U3CTriggerStay2DU3Ek__BackingField_29() const { return ___U3CTriggerStay2DU3Ek__BackingField_29; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CTriggerStay2DU3Ek__BackingField_29() { return &___U3CTriggerStay2DU3Ek__BackingField_29; }
	inline void set_U3CTriggerStay2DU3Ek__BackingField_29(FsmEvent_t3736299882 * value)
	{
		___U3CTriggerStay2DU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerStay2DU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CApplicationFocusU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CApplicationFocusU3Ek__BackingField_30)); }
	inline FsmEvent_t3736299882 * get_U3CApplicationFocusU3Ek__BackingField_30() const { return ___U3CApplicationFocusU3Ek__BackingField_30; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CApplicationFocusU3Ek__BackingField_30() { return &___U3CApplicationFocusU3Ek__BackingField_30; }
	inline void set_U3CApplicationFocusU3Ek__BackingField_30(FsmEvent_t3736299882 * value)
	{
		___U3CApplicationFocusU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationFocusU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CApplicationPauseU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CApplicationPauseU3Ek__BackingField_31)); }
	inline FsmEvent_t3736299882 * get_U3CApplicationPauseU3Ek__BackingField_31() const { return ___U3CApplicationPauseU3Ek__BackingField_31; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CApplicationPauseU3Ek__BackingField_31() { return &___U3CApplicationPauseU3Ek__BackingField_31; }
	inline void set_U3CApplicationPauseU3Ek__BackingField_31(FsmEvent_t3736299882 * value)
	{
		___U3CApplicationPauseU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationPauseU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CApplicationQuitU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CApplicationQuitU3Ek__BackingField_32)); }
	inline FsmEvent_t3736299882 * get_U3CApplicationQuitU3Ek__BackingField_32() const { return ___U3CApplicationQuitU3Ek__BackingField_32; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CApplicationQuitU3Ek__BackingField_32() { return &___U3CApplicationQuitU3Ek__BackingField_32; }
	inline void set_U3CApplicationQuitU3Ek__BackingField_32(FsmEvent_t3736299882 * value)
	{
		___U3CApplicationQuitU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationQuitU3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_U3CParticleCollisionU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CParticleCollisionU3Ek__BackingField_33)); }
	inline FsmEvent_t3736299882 * get_U3CParticleCollisionU3Ek__BackingField_33() const { return ___U3CParticleCollisionU3Ek__BackingField_33; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CParticleCollisionU3Ek__BackingField_33() { return &___U3CParticleCollisionU3Ek__BackingField_33; }
	inline void set_U3CParticleCollisionU3Ek__BackingField_33(FsmEvent_t3736299882 * value)
	{
		___U3CParticleCollisionU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParticleCollisionU3Ek__BackingField_33), value);
	}

	inline static int32_t get_offset_of_U3CJointBreakU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CJointBreakU3Ek__BackingField_34)); }
	inline FsmEvent_t3736299882 * get_U3CJointBreakU3Ek__BackingField_34() const { return ___U3CJointBreakU3Ek__BackingField_34; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CJointBreakU3Ek__BackingField_34() { return &___U3CJointBreakU3Ek__BackingField_34; }
	inline void set_U3CJointBreakU3Ek__BackingField_34(FsmEvent_t3736299882 * value)
	{
		___U3CJointBreakU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJointBreakU3Ek__BackingField_34), value);
	}

	inline static int32_t get_offset_of_U3CJointBreak2DU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CJointBreak2DU3Ek__BackingField_35)); }
	inline FsmEvent_t3736299882 * get_U3CJointBreak2DU3Ek__BackingField_35() const { return ___U3CJointBreak2DU3Ek__BackingField_35; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CJointBreak2DU3Ek__BackingField_35() { return &___U3CJointBreak2DU3Ek__BackingField_35; }
	inline void set_U3CJointBreak2DU3Ek__BackingField_35(FsmEvent_t3736299882 * value)
	{
		___U3CJointBreak2DU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJointBreak2DU3Ek__BackingField_35), value);
	}

	inline static int32_t get_offset_of_U3CDisableU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CDisableU3Ek__BackingField_36)); }
	inline FsmEvent_t3736299882 * get_U3CDisableU3Ek__BackingField_36() const { return ___U3CDisableU3Ek__BackingField_36; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CDisableU3Ek__BackingField_36() { return &___U3CDisableU3Ek__BackingField_36; }
	inline void set_U3CDisableU3Ek__BackingField_36(FsmEvent_t3736299882 * value)
	{
		___U3CDisableU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisableU3Ek__BackingField_36), value);
	}

	inline static int32_t get_offset_of_U3CPlayerConnectedU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CPlayerConnectedU3Ek__BackingField_37)); }
	inline FsmEvent_t3736299882 * get_U3CPlayerConnectedU3Ek__BackingField_37() const { return ___U3CPlayerConnectedU3Ek__BackingField_37; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CPlayerConnectedU3Ek__BackingField_37() { return &___U3CPlayerConnectedU3Ek__BackingField_37; }
	inline void set_U3CPlayerConnectedU3Ek__BackingField_37(FsmEvent_t3736299882 * value)
	{
		___U3CPlayerConnectedU3Ek__BackingField_37 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerConnectedU3Ek__BackingField_37), value);
	}

	inline static int32_t get_offset_of_U3CServerInitializedU3Ek__BackingField_38() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CServerInitializedU3Ek__BackingField_38)); }
	inline FsmEvent_t3736299882 * get_U3CServerInitializedU3Ek__BackingField_38() const { return ___U3CServerInitializedU3Ek__BackingField_38; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CServerInitializedU3Ek__BackingField_38() { return &___U3CServerInitializedU3Ek__BackingField_38; }
	inline void set_U3CServerInitializedU3Ek__BackingField_38(FsmEvent_t3736299882 * value)
	{
		___U3CServerInitializedU3Ek__BackingField_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServerInitializedU3Ek__BackingField_38), value);
	}

	inline static int32_t get_offset_of_U3CConnectedToServerU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CConnectedToServerU3Ek__BackingField_39)); }
	inline FsmEvent_t3736299882 * get_U3CConnectedToServerU3Ek__BackingField_39() const { return ___U3CConnectedToServerU3Ek__BackingField_39; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CConnectedToServerU3Ek__BackingField_39() { return &___U3CConnectedToServerU3Ek__BackingField_39; }
	inline void set_U3CConnectedToServerU3Ek__BackingField_39(FsmEvent_t3736299882 * value)
	{
		___U3CConnectedToServerU3Ek__BackingField_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConnectedToServerU3Ek__BackingField_39), value);
	}

	inline static int32_t get_offset_of_U3CPlayerDisconnectedU3Ek__BackingField_40() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CPlayerDisconnectedU3Ek__BackingField_40)); }
	inline FsmEvent_t3736299882 * get_U3CPlayerDisconnectedU3Ek__BackingField_40() const { return ___U3CPlayerDisconnectedU3Ek__BackingField_40; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CPlayerDisconnectedU3Ek__BackingField_40() { return &___U3CPlayerDisconnectedU3Ek__BackingField_40; }
	inline void set_U3CPlayerDisconnectedU3Ek__BackingField_40(FsmEvent_t3736299882 * value)
	{
		___U3CPlayerDisconnectedU3Ek__BackingField_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerDisconnectedU3Ek__BackingField_40), value);
	}

	inline static int32_t get_offset_of_U3CDisconnectedFromServerU3Ek__BackingField_41() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CDisconnectedFromServerU3Ek__BackingField_41)); }
	inline FsmEvent_t3736299882 * get_U3CDisconnectedFromServerU3Ek__BackingField_41() const { return ___U3CDisconnectedFromServerU3Ek__BackingField_41; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CDisconnectedFromServerU3Ek__BackingField_41() { return &___U3CDisconnectedFromServerU3Ek__BackingField_41; }
	inline void set_U3CDisconnectedFromServerU3Ek__BackingField_41(FsmEvent_t3736299882 * value)
	{
		___U3CDisconnectedFromServerU3Ek__BackingField_41 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisconnectedFromServerU3Ek__BackingField_41), value);
	}

	inline static int32_t get_offset_of_U3CFailedToConnectU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CFailedToConnectU3Ek__BackingField_42)); }
	inline FsmEvent_t3736299882 * get_U3CFailedToConnectU3Ek__BackingField_42() const { return ___U3CFailedToConnectU3Ek__BackingField_42; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CFailedToConnectU3Ek__BackingField_42() { return &___U3CFailedToConnectU3Ek__BackingField_42; }
	inline void set_U3CFailedToConnectU3Ek__BackingField_42(FsmEvent_t3736299882 * value)
	{
		___U3CFailedToConnectU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFailedToConnectU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_43() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CFailedToConnectToMasterServerU3Ek__BackingField_43)); }
	inline FsmEvent_t3736299882 * get_U3CFailedToConnectToMasterServerU3Ek__BackingField_43() const { return ___U3CFailedToConnectToMasterServerU3Ek__BackingField_43; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_43() { return &___U3CFailedToConnectToMasterServerU3Ek__BackingField_43; }
	inline void set_U3CFailedToConnectToMasterServerU3Ek__BackingField_43(FsmEvent_t3736299882 * value)
	{
		___U3CFailedToConnectToMasterServerU3Ek__BackingField_43 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFailedToConnectToMasterServerU3Ek__BackingField_43), value);
	}

	inline static int32_t get_offset_of_U3CMasterServerEventU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CMasterServerEventU3Ek__BackingField_44)); }
	inline FsmEvent_t3736299882 * get_U3CMasterServerEventU3Ek__BackingField_44() const { return ___U3CMasterServerEventU3Ek__BackingField_44; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CMasterServerEventU3Ek__BackingField_44() { return &___U3CMasterServerEventU3Ek__BackingField_44; }
	inline void set_U3CMasterServerEventU3Ek__BackingField_44(FsmEvent_t3736299882 * value)
	{
		___U3CMasterServerEventU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMasterServerEventU3Ek__BackingField_44), value);
	}

	inline static int32_t get_offset_of_U3CNetworkInstantiateU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CNetworkInstantiateU3Ek__BackingField_45)); }
	inline FsmEvent_t3736299882 * get_U3CNetworkInstantiateU3Ek__BackingField_45() const { return ___U3CNetworkInstantiateU3Ek__BackingField_45; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CNetworkInstantiateU3Ek__BackingField_45() { return &___U3CNetworkInstantiateU3Ek__BackingField_45; }
	inline void set_U3CNetworkInstantiateU3Ek__BackingField_45(FsmEvent_t3736299882 * value)
	{
		___U3CNetworkInstantiateU3Ek__BackingField_45 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNetworkInstantiateU3Ek__BackingField_45), value);
	}

	inline static int32_t get_offset_of_U3CUiBeginDragU3Ek__BackingField_46() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiBeginDragU3Ek__BackingField_46)); }
	inline FsmEvent_t3736299882 * get_U3CUiBeginDragU3Ek__BackingField_46() const { return ___U3CUiBeginDragU3Ek__BackingField_46; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiBeginDragU3Ek__BackingField_46() { return &___U3CUiBeginDragU3Ek__BackingField_46; }
	inline void set_U3CUiBeginDragU3Ek__BackingField_46(FsmEvent_t3736299882 * value)
	{
		___U3CUiBeginDragU3Ek__BackingField_46 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiBeginDragU3Ek__BackingField_46), value);
	}

	inline static int32_t get_offset_of_U3CUiDragU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiDragU3Ek__BackingField_47)); }
	inline FsmEvent_t3736299882 * get_U3CUiDragU3Ek__BackingField_47() const { return ___U3CUiDragU3Ek__BackingField_47; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiDragU3Ek__BackingField_47() { return &___U3CUiDragU3Ek__BackingField_47; }
	inline void set_U3CUiDragU3Ek__BackingField_47(FsmEvent_t3736299882 * value)
	{
		___U3CUiDragU3Ek__BackingField_47 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiDragU3Ek__BackingField_47), value);
	}

	inline static int32_t get_offset_of_U3CUiEndDragU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiEndDragU3Ek__BackingField_48)); }
	inline FsmEvent_t3736299882 * get_U3CUiEndDragU3Ek__BackingField_48() const { return ___U3CUiEndDragU3Ek__BackingField_48; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiEndDragU3Ek__BackingField_48() { return &___U3CUiEndDragU3Ek__BackingField_48; }
	inline void set_U3CUiEndDragU3Ek__BackingField_48(FsmEvent_t3736299882 * value)
	{
		___U3CUiEndDragU3Ek__BackingField_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiEndDragU3Ek__BackingField_48), value);
	}

	inline static int32_t get_offset_of_U3CUiClickU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiClickU3Ek__BackingField_49)); }
	inline FsmEvent_t3736299882 * get_U3CUiClickU3Ek__BackingField_49() const { return ___U3CUiClickU3Ek__BackingField_49; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiClickU3Ek__BackingField_49() { return &___U3CUiClickU3Ek__BackingField_49; }
	inline void set_U3CUiClickU3Ek__BackingField_49(FsmEvent_t3736299882 * value)
	{
		___U3CUiClickU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiClickU3Ek__BackingField_49), value);
	}

	inline static int32_t get_offset_of_U3CUiDropU3Ek__BackingField_50() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiDropU3Ek__BackingField_50)); }
	inline FsmEvent_t3736299882 * get_U3CUiDropU3Ek__BackingField_50() const { return ___U3CUiDropU3Ek__BackingField_50; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiDropU3Ek__BackingField_50() { return &___U3CUiDropU3Ek__BackingField_50; }
	inline void set_U3CUiDropU3Ek__BackingField_50(FsmEvent_t3736299882 * value)
	{
		___U3CUiDropU3Ek__BackingField_50 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiDropU3Ek__BackingField_50), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerClickU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerClickU3Ek__BackingField_51)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerClickU3Ek__BackingField_51() const { return ___U3CUiPointerClickU3Ek__BackingField_51; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerClickU3Ek__BackingField_51() { return &___U3CUiPointerClickU3Ek__BackingField_51; }
	inline void set_U3CUiPointerClickU3Ek__BackingField_51(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerClickU3Ek__BackingField_51 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerClickU3Ek__BackingField_51), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerDownU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerDownU3Ek__BackingField_52)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerDownU3Ek__BackingField_52() const { return ___U3CUiPointerDownU3Ek__BackingField_52; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerDownU3Ek__BackingField_52() { return &___U3CUiPointerDownU3Ek__BackingField_52; }
	inline void set_U3CUiPointerDownU3Ek__BackingField_52(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerDownU3Ek__BackingField_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerDownU3Ek__BackingField_52), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerEnterU3Ek__BackingField_53() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerEnterU3Ek__BackingField_53)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerEnterU3Ek__BackingField_53() const { return ___U3CUiPointerEnterU3Ek__BackingField_53; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerEnterU3Ek__BackingField_53() { return &___U3CUiPointerEnterU3Ek__BackingField_53; }
	inline void set_U3CUiPointerEnterU3Ek__BackingField_53(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerEnterU3Ek__BackingField_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerEnterU3Ek__BackingField_53), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerExitU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerExitU3Ek__BackingField_54)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerExitU3Ek__BackingField_54() const { return ___U3CUiPointerExitU3Ek__BackingField_54; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerExitU3Ek__BackingField_54() { return &___U3CUiPointerExitU3Ek__BackingField_54; }
	inline void set_U3CUiPointerExitU3Ek__BackingField_54(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerExitU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerExitU3Ek__BackingField_54), value);
	}

	inline static int32_t get_offset_of_U3CUiPointerUpU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiPointerUpU3Ek__BackingField_55)); }
	inline FsmEvent_t3736299882 * get_U3CUiPointerUpU3Ek__BackingField_55() const { return ___U3CUiPointerUpU3Ek__BackingField_55; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiPointerUpU3Ek__BackingField_55() { return &___U3CUiPointerUpU3Ek__BackingField_55; }
	inline void set_U3CUiPointerUpU3Ek__BackingField_55(FsmEvent_t3736299882 * value)
	{
		___U3CUiPointerUpU3Ek__BackingField_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiPointerUpU3Ek__BackingField_55), value);
	}

	inline static int32_t get_offset_of_U3CUiBoolValueChangedU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiBoolValueChangedU3Ek__BackingField_56)); }
	inline FsmEvent_t3736299882 * get_U3CUiBoolValueChangedU3Ek__BackingField_56() const { return ___U3CUiBoolValueChangedU3Ek__BackingField_56; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiBoolValueChangedU3Ek__BackingField_56() { return &___U3CUiBoolValueChangedU3Ek__BackingField_56; }
	inline void set_U3CUiBoolValueChangedU3Ek__BackingField_56(FsmEvent_t3736299882 * value)
	{
		___U3CUiBoolValueChangedU3Ek__BackingField_56 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiBoolValueChangedU3Ek__BackingField_56), value);
	}

	inline static int32_t get_offset_of_U3CUiFloatValueChangedU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiFloatValueChangedU3Ek__BackingField_57)); }
	inline FsmEvent_t3736299882 * get_U3CUiFloatValueChangedU3Ek__BackingField_57() const { return ___U3CUiFloatValueChangedU3Ek__BackingField_57; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiFloatValueChangedU3Ek__BackingField_57() { return &___U3CUiFloatValueChangedU3Ek__BackingField_57; }
	inline void set_U3CUiFloatValueChangedU3Ek__BackingField_57(FsmEvent_t3736299882 * value)
	{
		___U3CUiFloatValueChangedU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiFloatValueChangedU3Ek__BackingField_57), value);
	}

	inline static int32_t get_offset_of_U3CUiIntValueChangedU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiIntValueChangedU3Ek__BackingField_58)); }
	inline FsmEvent_t3736299882 * get_U3CUiIntValueChangedU3Ek__BackingField_58() const { return ___U3CUiIntValueChangedU3Ek__BackingField_58; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiIntValueChangedU3Ek__BackingField_58() { return &___U3CUiIntValueChangedU3Ek__BackingField_58; }
	inline void set_U3CUiIntValueChangedU3Ek__BackingField_58(FsmEvent_t3736299882 * value)
	{
		___U3CUiIntValueChangedU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiIntValueChangedU3Ek__BackingField_58), value);
	}

	inline static int32_t get_offset_of_U3CUiVector2ValueChangedU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiVector2ValueChangedU3Ek__BackingField_59)); }
	inline FsmEvent_t3736299882 * get_U3CUiVector2ValueChangedU3Ek__BackingField_59() const { return ___U3CUiVector2ValueChangedU3Ek__BackingField_59; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiVector2ValueChangedU3Ek__BackingField_59() { return &___U3CUiVector2ValueChangedU3Ek__BackingField_59; }
	inline void set_U3CUiVector2ValueChangedU3Ek__BackingField_59(FsmEvent_t3736299882 * value)
	{
		___U3CUiVector2ValueChangedU3Ek__BackingField_59 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiVector2ValueChangedU3Ek__BackingField_59), value);
	}

	inline static int32_t get_offset_of_U3CUiEndEditU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(FsmEvent_t3736299882_StaticFields, ___U3CUiEndEditU3Ek__BackingField_60)); }
	inline FsmEvent_t3736299882 * get_U3CUiEndEditU3Ek__BackingField_60() const { return ___U3CUiEndEditU3Ek__BackingField_60; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CUiEndEditU3Ek__BackingField_60() { return &___U3CUiEndEditU3Ek__BackingField_60; }
	inline void set_U3CUiEndEditU3Ek__BackingField_60(FsmEvent_t3736299882 * value)
	{
		___U3CUiEndEditU3Ek__BackingField_60 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUiEndEditU3Ek__BackingField_60), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEVENT_T3736299882_H
#ifndef FSMLOG_T3265252880_H
#define FSMLOG_T3265252880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmLog
struct  FsmLog_t3265252880  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmLog::<Fsm>k__BackingField
	Fsm_t4127147824 * ___U3CFsmU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry> HutongGames.PlayMaker.FsmLog::entries
	List_1_t517607234 * ___entries_8;

public:
	inline static int32_t get_offset_of_U3CFsmU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880, ___U3CFsmU3Ek__BackingField_7)); }
	inline Fsm_t4127147824 * get_U3CFsmU3Ek__BackingField_7() const { return ___U3CFsmU3Ek__BackingField_7; }
	inline Fsm_t4127147824 ** get_address_of_U3CFsmU3Ek__BackingField_7() { return &___U3CFsmU3Ek__BackingField_7; }
	inline void set_U3CFsmU3Ek__BackingField_7(Fsm_t4127147824 * value)
	{
		___U3CFsmU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFsmU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_entries_8() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880, ___entries_8)); }
	inline List_1_t517607234 * get_entries_8() const { return ___entries_8; }
	inline List_1_t517607234 ** get_address_of_entries_8() { return &___entries_8; }
	inline void set_entries_8(List_1_t517607234 * value)
	{
		___entries_8 = value;
		Il2CppCodeGenWriteBarrier((&___entries_8), value);
	}
};

struct FsmLog_t3265252880_StaticFields
{
public:
	// System.Int32 HutongGames.PlayMaker.FsmLog::MaxSize
	int32_t ___MaxSize_0;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLog> HutongGames.PlayMaker.FsmLog::Logs
	List_1_t442360326 * ___Logs_1;
	// HutongGames.PlayMaker.FsmLogEntry[] HutongGames.PlayMaker.FsmLog::logEntryPool
	FsmLogEntryU5BU5D_t3257799173* ___logEntryPool_2;
	// System.Int32 HutongGames.PlayMaker.FsmLog::nextLogEntryPoolIndex
	int32_t ___nextLogEntryPoolIndex_3;
	// System.Boolean HutongGames.PlayMaker.FsmLog::<LoggingEnabled>k__BackingField
	bool ___U3CLoggingEnabledU3Ek__BackingField_4;
	// System.Boolean HutongGames.PlayMaker.FsmLog::<MirrorDebugLog>k__BackingField
	bool ___U3CMirrorDebugLogU3Ek__BackingField_5;
	// System.Boolean HutongGames.PlayMaker.FsmLog::<EnableDebugFlow>k__BackingField
	bool ___U3CEnableDebugFlowU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_MaxSize_0() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___MaxSize_0)); }
	inline int32_t get_MaxSize_0() const { return ___MaxSize_0; }
	inline int32_t* get_address_of_MaxSize_0() { return &___MaxSize_0; }
	inline void set_MaxSize_0(int32_t value)
	{
		___MaxSize_0 = value;
	}

	inline static int32_t get_offset_of_Logs_1() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___Logs_1)); }
	inline List_1_t442360326 * get_Logs_1() const { return ___Logs_1; }
	inline List_1_t442360326 ** get_address_of_Logs_1() { return &___Logs_1; }
	inline void set_Logs_1(List_1_t442360326 * value)
	{
		___Logs_1 = value;
		Il2CppCodeGenWriteBarrier((&___Logs_1), value);
	}

	inline static int32_t get_offset_of_logEntryPool_2() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___logEntryPool_2)); }
	inline FsmLogEntryU5BU5D_t3257799173* get_logEntryPool_2() const { return ___logEntryPool_2; }
	inline FsmLogEntryU5BU5D_t3257799173** get_address_of_logEntryPool_2() { return &___logEntryPool_2; }
	inline void set_logEntryPool_2(FsmLogEntryU5BU5D_t3257799173* value)
	{
		___logEntryPool_2 = value;
		Il2CppCodeGenWriteBarrier((&___logEntryPool_2), value);
	}

	inline static int32_t get_offset_of_nextLogEntryPoolIndex_3() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___nextLogEntryPoolIndex_3)); }
	inline int32_t get_nextLogEntryPoolIndex_3() const { return ___nextLogEntryPoolIndex_3; }
	inline int32_t* get_address_of_nextLogEntryPoolIndex_3() { return &___nextLogEntryPoolIndex_3; }
	inline void set_nextLogEntryPoolIndex_3(int32_t value)
	{
		___nextLogEntryPoolIndex_3 = value;
	}

	inline static int32_t get_offset_of_U3CLoggingEnabledU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___U3CLoggingEnabledU3Ek__BackingField_4)); }
	inline bool get_U3CLoggingEnabledU3Ek__BackingField_4() const { return ___U3CLoggingEnabledU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CLoggingEnabledU3Ek__BackingField_4() { return &___U3CLoggingEnabledU3Ek__BackingField_4; }
	inline void set_U3CLoggingEnabledU3Ek__BackingField_4(bool value)
	{
		___U3CLoggingEnabledU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMirrorDebugLogU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___U3CMirrorDebugLogU3Ek__BackingField_5)); }
	inline bool get_U3CMirrorDebugLogU3Ek__BackingField_5() const { return ___U3CMirrorDebugLogU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CMirrorDebugLogU3Ek__BackingField_5() { return &___U3CMirrorDebugLogU3Ek__BackingField_5; }
	inline void set_U3CMirrorDebugLogU3Ek__BackingField_5(bool value)
	{
		___U3CMirrorDebugLogU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CEnableDebugFlowU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FsmLog_t3265252880_StaticFields, ___U3CEnableDebugFlowU3Ek__BackingField_6)); }
	inline bool get_U3CEnableDebugFlowU3Ek__BackingField_6() const { return ___U3CEnableDebugFlowU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CEnableDebugFlowU3Ek__BackingField_6() { return &___U3CEnableDebugFlowU3Ek__BackingField_6; }
	inline void set_U3CEnableDebugFlowU3Ek__BackingField_6(bool value)
	{
		___U3CEnableDebugFlowU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMLOG_T3265252880_H
#ifndef FSMUTILITY_T1202208704_H
#define FSMUTILITY_T1202208704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmUtility
struct  FsmUtility_t1202208704  : public RuntimeObject
{
public:

public:
};

struct FsmUtility_t1202208704_StaticFields
{
public:
	// System.Text.UTF8Encoding HutongGames.PlayMaker.FsmUtility::encoding
	UTF8Encoding_t3956466879 * ___encoding_0;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(FsmUtility_t1202208704_StaticFields, ___encoding_0)); }
	inline UTF8Encoding_t3956466879 * get_encoding_0() const { return ___encoding_0; }
	inline UTF8Encoding_t3956466879 ** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(UTF8Encoding_t3956466879 * value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMUTILITY_T1202208704_H
#ifndef BITCONVERTER_T2995050545_H
#define BITCONVERTER_T2995050545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmUtility/BitConverter
struct  BitConverter_t2995050545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTER_T2995050545_H
#ifndef FSMVARIABLES_T3349589544_H
#define FSMVARIABLES_T3349589544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVariables
struct  FsmVariables_t3349589544  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::floatVariables
	FsmFloatU5BU5D_t3637897416* ___floatVariables_1;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::intVariables
	FsmIntU5BU5D_t2904461592* ___intVariables_2;
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::boolVariables
	FsmBoolU5BU5D_t1155011270* ___boolVariables_3;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::stringVariables
	FsmStringU5BU5D_t252501805* ___stringVariables_4;
	// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::vector2Variables
	FsmVector2U5BU5D_t1097319912* ___vector2Variables_5;
	// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::vector3Variables
	FsmVector3U5BU5D_t402703848* ___vector3Variables_6;
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::colorVariables
	FsmColorU5BU5D_t1111370293* ___colorVariables_7;
	// HutongGames.PlayMaker.FsmRect[] HutongGames.PlayMaker.FsmVariables::rectVariables
	FsmRectU5BU5D_t3969342952* ___rectVariables_8;
	// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::quaternionVariables
	FsmQuaternionU5BU5D_t495607758* ___quaternionVariables_9;
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.FsmVariables::gameObjectVariables
	FsmGameObjectU5BU5D_t719957643* ___gameObjectVariables_10;
	// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.FsmVariables::objectVariables
	FsmObjectU5BU5D_t635502296* ___objectVariables_11;
	// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.FsmVariables::materialVariables
	FsmMaterialU5BU5D_t3870809373* ___materialVariables_12;
	// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.FsmVariables::textureVariables
	FsmTextureU5BU5D_t2313143784* ___textureVariables_13;
	// HutongGames.PlayMaker.FsmArray[] HutongGames.PlayMaker.FsmVariables::arrayVariables
	FsmArrayU5BU5D_t2002857066* ___arrayVariables_14;
	// HutongGames.PlayMaker.FsmEnum[] HutongGames.PlayMaker.FsmVariables::enumVariables
	FsmEnumU5BU5D_t1010932050* ___enumVariables_15;
	// System.String[] HutongGames.PlayMaker.FsmVariables::categories
	StringU5BU5D_t1281789340* ___categories_16;
	// System.Int32[] HutongGames.PlayMaker.FsmVariables::variableCategoryIDs
	Int32U5BU5D_t385246372* ___variableCategoryIDs_17;

public:
	inline static int32_t get_offset_of_floatVariables_1() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___floatVariables_1)); }
	inline FsmFloatU5BU5D_t3637897416* get_floatVariables_1() const { return ___floatVariables_1; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_floatVariables_1() { return &___floatVariables_1; }
	inline void set_floatVariables_1(FsmFloatU5BU5D_t3637897416* value)
	{
		___floatVariables_1 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariables_1), value);
	}

	inline static int32_t get_offset_of_intVariables_2() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___intVariables_2)); }
	inline FsmIntU5BU5D_t2904461592* get_intVariables_2() const { return ___intVariables_2; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_intVariables_2() { return &___intVariables_2; }
	inline void set_intVariables_2(FsmIntU5BU5D_t2904461592* value)
	{
		___intVariables_2 = value;
		Il2CppCodeGenWriteBarrier((&___intVariables_2), value);
	}

	inline static int32_t get_offset_of_boolVariables_3() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___boolVariables_3)); }
	inline FsmBoolU5BU5D_t1155011270* get_boolVariables_3() const { return ___boolVariables_3; }
	inline FsmBoolU5BU5D_t1155011270** get_address_of_boolVariables_3() { return &___boolVariables_3; }
	inline void set_boolVariables_3(FsmBoolU5BU5D_t1155011270* value)
	{
		___boolVariables_3 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariables_3), value);
	}

	inline static int32_t get_offset_of_stringVariables_4() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___stringVariables_4)); }
	inline FsmStringU5BU5D_t252501805* get_stringVariables_4() const { return ___stringVariables_4; }
	inline FsmStringU5BU5D_t252501805** get_address_of_stringVariables_4() { return &___stringVariables_4; }
	inline void set_stringVariables_4(FsmStringU5BU5D_t252501805* value)
	{
		___stringVariables_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariables_4), value);
	}

	inline static int32_t get_offset_of_vector2Variables_5() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___vector2Variables_5)); }
	inline FsmVector2U5BU5D_t1097319912* get_vector2Variables_5() const { return ___vector2Variables_5; }
	inline FsmVector2U5BU5D_t1097319912** get_address_of_vector2Variables_5() { return &___vector2Variables_5; }
	inline void set_vector2Variables_5(FsmVector2U5BU5D_t1097319912* value)
	{
		___vector2Variables_5 = value;
		Il2CppCodeGenWriteBarrier((&___vector2Variables_5), value);
	}

	inline static int32_t get_offset_of_vector3Variables_6() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___vector3Variables_6)); }
	inline FsmVector3U5BU5D_t402703848* get_vector3Variables_6() const { return ___vector3Variables_6; }
	inline FsmVector3U5BU5D_t402703848** get_address_of_vector3Variables_6() { return &___vector3Variables_6; }
	inline void set_vector3Variables_6(FsmVector3U5BU5D_t402703848* value)
	{
		___vector3Variables_6 = value;
		Il2CppCodeGenWriteBarrier((&___vector3Variables_6), value);
	}

	inline static int32_t get_offset_of_colorVariables_7() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___colorVariables_7)); }
	inline FsmColorU5BU5D_t1111370293* get_colorVariables_7() const { return ___colorVariables_7; }
	inline FsmColorU5BU5D_t1111370293** get_address_of_colorVariables_7() { return &___colorVariables_7; }
	inline void set_colorVariables_7(FsmColorU5BU5D_t1111370293* value)
	{
		___colorVariables_7 = value;
		Il2CppCodeGenWriteBarrier((&___colorVariables_7), value);
	}

	inline static int32_t get_offset_of_rectVariables_8() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___rectVariables_8)); }
	inline FsmRectU5BU5D_t3969342952* get_rectVariables_8() const { return ___rectVariables_8; }
	inline FsmRectU5BU5D_t3969342952** get_address_of_rectVariables_8() { return &___rectVariables_8; }
	inline void set_rectVariables_8(FsmRectU5BU5D_t3969342952* value)
	{
		___rectVariables_8 = value;
		Il2CppCodeGenWriteBarrier((&___rectVariables_8), value);
	}

	inline static int32_t get_offset_of_quaternionVariables_9() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___quaternionVariables_9)); }
	inline FsmQuaternionU5BU5D_t495607758* get_quaternionVariables_9() const { return ___quaternionVariables_9; }
	inline FsmQuaternionU5BU5D_t495607758** get_address_of_quaternionVariables_9() { return &___quaternionVariables_9; }
	inline void set_quaternionVariables_9(FsmQuaternionU5BU5D_t495607758* value)
	{
		___quaternionVariables_9 = value;
		Il2CppCodeGenWriteBarrier((&___quaternionVariables_9), value);
	}

	inline static int32_t get_offset_of_gameObjectVariables_10() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___gameObjectVariables_10)); }
	inline FsmGameObjectU5BU5D_t719957643* get_gameObjectVariables_10() const { return ___gameObjectVariables_10; }
	inline FsmGameObjectU5BU5D_t719957643** get_address_of_gameObjectVariables_10() { return &___gameObjectVariables_10; }
	inline void set_gameObjectVariables_10(FsmGameObjectU5BU5D_t719957643* value)
	{
		___gameObjectVariables_10 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectVariables_10), value);
	}

	inline static int32_t get_offset_of_objectVariables_11() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___objectVariables_11)); }
	inline FsmObjectU5BU5D_t635502296* get_objectVariables_11() const { return ___objectVariables_11; }
	inline FsmObjectU5BU5D_t635502296** get_address_of_objectVariables_11() { return &___objectVariables_11; }
	inline void set_objectVariables_11(FsmObjectU5BU5D_t635502296* value)
	{
		___objectVariables_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectVariables_11), value);
	}

	inline static int32_t get_offset_of_materialVariables_12() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___materialVariables_12)); }
	inline FsmMaterialU5BU5D_t3870809373* get_materialVariables_12() const { return ___materialVariables_12; }
	inline FsmMaterialU5BU5D_t3870809373** get_address_of_materialVariables_12() { return &___materialVariables_12; }
	inline void set_materialVariables_12(FsmMaterialU5BU5D_t3870809373* value)
	{
		___materialVariables_12 = value;
		Il2CppCodeGenWriteBarrier((&___materialVariables_12), value);
	}

	inline static int32_t get_offset_of_textureVariables_13() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___textureVariables_13)); }
	inline FsmTextureU5BU5D_t2313143784* get_textureVariables_13() const { return ___textureVariables_13; }
	inline FsmTextureU5BU5D_t2313143784** get_address_of_textureVariables_13() { return &___textureVariables_13; }
	inline void set_textureVariables_13(FsmTextureU5BU5D_t2313143784* value)
	{
		___textureVariables_13 = value;
		Il2CppCodeGenWriteBarrier((&___textureVariables_13), value);
	}

	inline static int32_t get_offset_of_arrayVariables_14() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___arrayVariables_14)); }
	inline FsmArrayU5BU5D_t2002857066* get_arrayVariables_14() const { return ___arrayVariables_14; }
	inline FsmArrayU5BU5D_t2002857066** get_address_of_arrayVariables_14() { return &___arrayVariables_14; }
	inline void set_arrayVariables_14(FsmArrayU5BU5D_t2002857066* value)
	{
		___arrayVariables_14 = value;
		Il2CppCodeGenWriteBarrier((&___arrayVariables_14), value);
	}

	inline static int32_t get_offset_of_enumVariables_15() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___enumVariables_15)); }
	inline FsmEnumU5BU5D_t1010932050* get_enumVariables_15() const { return ___enumVariables_15; }
	inline FsmEnumU5BU5D_t1010932050** get_address_of_enumVariables_15() { return &___enumVariables_15; }
	inline void set_enumVariables_15(FsmEnumU5BU5D_t1010932050* value)
	{
		___enumVariables_15 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariables_15), value);
	}

	inline static int32_t get_offset_of_categories_16() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___categories_16)); }
	inline StringU5BU5D_t1281789340* get_categories_16() const { return ___categories_16; }
	inline StringU5BU5D_t1281789340** get_address_of_categories_16() { return &___categories_16; }
	inline void set_categories_16(StringU5BU5D_t1281789340* value)
	{
		___categories_16 = value;
		Il2CppCodeGenWriteBarrier((&___categories_16), value);
	}

	inline static int32_t get_offset_of_variableCategoryIDs_17() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544, ___variableCategoryIDs_17)); }
	inline Int32U5BU5D_t385246372* get_variableCategoryIDs_17() const { return ___variableCategoryIDs_17; }
	inline Int32U5BU5D_t385246372** get_address_of_variableCategoryIDs_17() { return &___variableCategoryIDs_17; }
	inline void set_variableCategoryIDs_17(Int32U5BU5D_t385246372* value)
	{
		___variableCategoryIDs_17 = value;
		Il2CppCodeGenWriteBarrier((&___variableCategoryIDs_17), value);
	}
};

struct FsmVariables_t3349589544_StaticFields
{
public:
	// System.Boolean HutongGames.PlayMaker.FsmVariables::<GlobalVariablesSynced>k__BackingField
	bool ___U3CGlobalVariablesSyncedU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FsmVariables_t3349589544_StaticFields, ___U3CGlobalVariablesSyncedU3Ek__BackingField_0)); }
	inline bool get_U3CGlobalVariablesSyncedU3Ek__BackingField_0() const { return ___U3CGlobalVariablesSyncedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CGlobalVariablesSyncedU3Ek__BackingField_0() { return &___U3CGlobalVariablesSyncedU3Ek__BackingField_0; }
	inline void set_U3CGlobalVariablesSyncedU3Ek__BackingField_0(bool value)
	{
		___U3CGlobalVariablesSyncedU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMVARIABLES_T3349589544_H
#ifndef REFLECTIONUTILS_T2510826940_H
#define REFLECTIONUTILS_T2510826940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ReflectionUtils
struct  ReflectionUtils_t2510826940  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t2510826940_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ReflectionUtils::assemblyNames
	List_1_t3319525431 * ___assemblyNames_0;
	// System.Reflection.Assembly[] HutongGames.PlayMaker.ReflectionUtils::loadedAssemblies
	AssemblyU5BU5D_t2792222854* ___loadedAssemblies_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> HutongGames.PlayMaker.ReflectionUtils::typeLookup
	Dictionary_2_t2269201059 * ___typeLookup_2;

public:
	inline static int32_t get_offset_of_assemblyNames_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t2510826940_StaticFields, ___assemblyNames_0)); }
	inline List_1_t3319525431 * get_assemblyNames_0() const { return ___assemblyNames_0; }
	inline List_1_t3319525431 ** get_address_of_assemblyNames_0() { return &___assemblyNames_0; }
	inline void set_assemblyNames_0(List_1_t3319525431 * value)
	{
		___assemblyNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyNames_0), value);
	}

	inline static int32_t get_offset_of_loadedAssemblies_1() { return static_cast<int32_t>(offsetof(ReflectionUtils_t2510826940_StaticFields, ___loadedAssemblies_1)); }
	inline AssemblyU5BU5D_t2792222854* get_loadedAssemblies_1() const { return ___loadedAssemblies_1; }
	inline AssemblyU5BU5D_t2792222854** get_address_of_loadedAssemblies_1() { return &___loadedAssemblies_1; }
	inline void set_loadedAssemblies_1(AssemblyU5BU5D_t2792222854* value)
	{
		___loadedAssemblies_1 = value;
		Il2CppCodeGenWriteBarrier((&___loadedAssemblies_1), value);
	}

	inline static int32_t get_offset_of_typeLookup_2() { return static_cast<int32_t>(offsetof(ReflectionUtils_t2510826940_StaticFields, ___typeLookup_2)); }
	inline Dictionary_2_t2269201059 * get_typeLookup_2() const { return ___typeLookup_2; }
	inline Dictionary_2_t2269201059 ** get_address_of_typeLookup_2() { return &___typeLookup_2; }
	inline void set_typeLookup_2(Dictionary_2_t2269201059 * value)
	{
		___typeLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeLookup_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T2510826940_H
#ifndef COLORUTILS_T43876202_H
#define COLORUTILS_T43876202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.ColorUtils
struct  ColorUtils_t43876202  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORUTILS_T43876202_H
#ifndef STRINGUTILS_T862297538_H
#define STRINGUTILS_T862297538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.StringUtils
struct  StringUtils_t862297538  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T862297538_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef EXECUTEEVENTS_T3484638744_H
#define EXECUTEEVENTS_T3484638744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t3484638744  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t3484638744_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t3995630009 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t2867327688 * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t64614563 * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_t3256600500 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t3111972472 * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_t3587542510 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t1977848392 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t972960537 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t3277009892 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t2311673543 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t2886331738 * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_t2950825503 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t955952873 * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t3373214253 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_t3912835512 * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t1475332338 * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_t2658898854 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_t231414508 * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t777473367 * ___s_InternalTransformList_18;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache0
	EventFunction_1_t3995630009 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache1
	EventFunction_1_t2867327688 * ___U3CU3Ef__mgU24cache1_20;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache2
	EventFunction_1_t64614563 * ___U3CU3Ef__mgU24cache2_21;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache3
	EventFunction_1_t3256600500 * ___U3CU3Ef__mgU24cache3_22;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache4
	EventFunction_1_t3111972472 * ___U3CU3Ef__mgU24cache4_23;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache5
	EventFunction_1_t3587542510 * ___U3CU3Ef__mgU24cache5_24;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache6
	EventFunction_1_t1977848392 * ___U3CU3Ef__mgU24cache6_25;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache7
	EventFunction_1_t972960537 * ___U3CU3Ef__mgU24cache7_26;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache8
	EventFunction_1_t3277009892 * ___U3CU3Ef__mgU24cache8_27;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache9
	EventFunction_1_t2311673543 * ___U3CU3Ef__mgU24cache9_28;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheA
	EventFunction_1_t2886331738 * ___U3CU3Ef__mgU24cacheA_29;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheB
	EventFunction_1_t2950825503 * ___U3CU3Ef__mgU24cacheB_30;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheC
	EventFunction_1_t955952873 * ___U3CU3Ef__mgU24cacheC_31;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheD
	EventFunction_1_t3373214253 * ___U3CU3Ef__mgU24cacheD_32;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheE
	EventFunction_1_t3912835512 * ___U3CU3Ef__mgU24cacheE_33;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cacheF
	EventFunction_1_t1475332338 * ___U3CU3Ef__mgU24cacheF_34;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mg$cache10
	EventFunction_1_t2658898854 * ___U3CU3Ef__mgU24cache10_35;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t3995630009 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t3995630009 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t3995630009 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerEnterHandler_0), value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t2867327688 * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t2867327688 ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t2867327688 * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerExitHandler_1), value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t64614563 * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t64614563 ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t64614563 * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerDownHandler_2), value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_t3256600500 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_t3256600500 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_t3256600500 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerUpHandler_3), value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t3111972472 * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t3111972472 ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t3111972472 * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerClickHandler_4), value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_t3587542510 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_t3587542510 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_t3587542510 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_InitializePotentialDragHandler_5), value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t1977848392 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t1977848392 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t1977848392 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_BeginDragHandler_6), value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t972960537 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t972960537 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t972960537 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_DragHandler_7), value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t3277009892 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t3277009892 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t3277009892 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EndDragHandler_8), value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t2311673543 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t2311673543 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t2311673543 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_DropHandler_9), value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t2886331738 * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t2886331738 ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t2886331738 * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ScrollHandler_10), value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_t2950825503 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_t2950825503 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_t2950825503 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateSelectedHandler_11), value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t955952873 * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t955952873 ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t955952873 * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_SelectHandler_12), value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t3373214253 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t3373214253 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t3373214253 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_DeselectHandler_13), value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_t3912835512 * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_t3912835512 ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_t3912835512 * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_MoveHandler_14), value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t1475332338 * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t1475332338 ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t1475332338 * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubmitHandler_15), value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_t2658898854 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_t2658898854 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_t2658898854 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelHandler_16), value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_t231414508 * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_t231414508 ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_t231414508 * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_HandlerListPool_17), value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t777473367 * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t777473367 ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t777473367 * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalTransformList_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline EventFunction_1_t3995630009 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline EventFunction_1_t3995630009 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(EventFunction_1_t3995630009 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline EventFunction_1_t2867327688 * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline EventFunction_1_t2867327688 ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(EventFunction_1_t2867327688 * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_21() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache2_21)); }
	inline EventFunction_1_t64614563 * get_U3CU3Ef__mgU24cache2_21() const { return ___U3CU3Ef__mgU24cache2_21; }
	inline EventFunction_1_t64614563 ** get_address_of_U3CU3Ef__mgU24cache2_21() { return &___U3CU3Ef__mgU24cache2_21; }
	inline void set_U3CU3Ef__mgU24cache2_21(EventFunction_1_t64614563 * value)
	{
		___U3CU3Ef__mgU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_22() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache3_22)); }
	inline EventFunction_1_t3256600500 * get_U3CU3Ef__mgU24cache3_22() const { return ___U3CU3Ef__mgU24cache3_22; }
	inline EventFunction_1_t3256600500 ** get_address_of_U3CU3Ef__mgU24cache3_22() { return &___U3CU3Ef__mgU24cache3_22; }
	inline void set_U3CU3Ef__mgU24cache3_22(EventFunction_1_t3256600500 * value)
	{
		___U3CU3Ef__mgU24cache3_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_23() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache4_23)); }
	inline EventFunction_1_t3111972472 * get_U3CU3Ef__mgU24cache4_23() const { return ___U3CU3Ef__mgU24cache4_23; }
	inline EventFunction_1_t3111972472 ** get_address_of_U3CU3Ef__mgU24cache4_23() { return &___U3CU3Ef__mgU24cache4_23; }
	inline void set_U3CU3Ef__mgU24cache4_23(EventFunction_1_t3111972472 * value)
	{
		___U3CU3Ef__mgU24cache4_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_24() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache5_24)); }
	inline EventFunction_1_t3587542510 * get_U3CU3Ef__mgU24cache5_24() const { return ___U3CU3Ef__mgU24cache5_24; }
	inline EventFunction_1_t3587542510 ** get_address_of_U3CU3Ef__mgU24cache5_24() { return &___U3CU3Ef__mgU24cache5_24; }
	inline void set_U3CU3Ef__mgU24cache5_24(EventFunction_1_t3587542510 * value)
	{
		___U3CU3Ef__mgU24cache5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_25() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache6_25)); }
	inline EventFunction_1_t1977848392 * get_U3CU3Ef__mgU24cache6_25() const { return ___U3CU3Ef__mgU24cache6_25; }
	inline EventFunction_1_t1977848392 ** get_address_of_U3CU3Ef__mgU24cache6_25() { return &___U3CU3Ef__mgU24cache6_25; }
	inline void set_U3CU3Ef__mgU24cache6_25(EventFunction_1_t1977848392 * value)
	{
		___U3CU3Ef__mgU24cache6_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_26() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache7_26)); }
	inline EventFunction_1_t972960537 * get_U3CU3Ef__mgU24cache7_26() const { return ___U3CU3Ef__mgU24cache7_26; }
	inline EventFunction_1_t972960537 ** get_address_of_U3CU3Ef__mgU24cache7_26() { return &___U3CU3Ef__mgU24cache7_26; }
	inline void set_U3CU3Ef__mgU24cache7_26(EventFunction_1_t972960537 * value)
	{
		___U3CU3Ef__mgU24cache7_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_27() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache8_27)); }
	inline EventFunction_1_t3277009892 * get_U3CU3Ef__mgU24cache8_27() const { return ___U3CU3Ef__mgU24cache8_27; }
	inline EventFunction_1_t3277009892 ** get_address_of_U3CU3Ef__mgU24cache8_27() { return &___U3CU3Ef__mgU24cache8_27; }
	inline void set_U3CU3Ef__mgU24cache8_27(EventFunction_1_t3277009892 * value)
	{
		___U3CU3Ef__mgU24cache8_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_28() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache9_28)); }
	inline EventFunction_1_t2311673543 * get_U3CU3Ef__mgU24cache9_28() const { return ___U3CU3Ef__mgU24cache9_28; }
	inline EventFunction_1_t2311673543 ** get_address_of_U3CU3Ef__mgU24cache9_28() { return &___U3CU3Ef__mgU24cache9_28; }
	inline void set_U3CU3Ef__mgU24cache9_28(EventFunction_1_t2311673543 * value)
	{
		___U3CU3Ef__mgU24cache9_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_29() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheA_29)); }
	inline EventFunction_1_t2886331738 * get_U3CU3Ef__mgU24cacheA_29() const { return ___U3CU3Ef__mgU24cacheA_29; }
	inline EventFunction_1_t2886331738 ** get_address_of_U3CU3Ef__mgU24cacheA_29() { return &___U3CU3Ef__mgU24cacheA_29; }
	inline void set_U3CU3Ef__mgU24cacheA_29(EventFunction_1_t2886331738 * value)
	{
		___U3CU3Ef__mgU24cacheA_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_30() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheB_30)); }
	inline EventFunction_1_t2950825503 * get_U3CU3Ef__mgU24cacheB_30() const { return ___U3CU3Ef__mgU24cacheB_30; }
	inline EventFunction_1_t2950825503 ** get_address_of_U3CU3Ef__mgU24cacheB_30() { return &___U3CU3Ef__mgU24cacheB_30; }
	inline void set_U3CU3Ef__mgU24cacheB_30(EventFunction_1_t2950825503 * value)
	{
		___U3CU3Ef__mgU24cacheB_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_31() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheC_31)); }
	inline EventFunction_1_t955952873 * get_U3CU3Ef__mgU24cacheC_31() const { return ___U3CU3Ef__mgU24cacheC_31; }
	inline EventFunction_1_t955952873 ** get_address_of_U3CU3Ef__mgU24cacheC_31() { return &___U3CU3Ef__mgU24cacheC_31; }
	inline void set_U3CU3Ef__mgU24cacheC_31(EventFunction_1_t955952873 * value)
	{
		___U3CU3Ef__mgU24cacheC_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_32() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheD_32)); }
	inline EventFunction_1_t3373214253 * get_U3CU3Ef__mgU24cacheD_32() const { return ___U3CU3Ef__mgU24cacheD_32; }
	inline EventFunction_1_t3373214253 ** get_address_of_U3CU3Ef__mgU24cacheD_32() { return &___U3CU3Ef__mgU24cacheD_32; }
	inline void set_U3CU3Ef__mgU24cacheD_32(EventFunction_1_t3373214253 * value)
	{
		___U3CU3Ef__mgU24cacheD_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_33() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheE_33)); }
	inline EventFunction_1_t3912835512 * get_U3CU3Ef__mgU24cacheE_33() const { return ___U3CU3Ef__mgU24cacheE_33; }
	inline EventFunction_1_t3912835512 ** get_address_of_U3CU3Ef__mgU24cacheE_33() { return &___U3CU3Ef__mgU24cacheE_33; }
	inline void set_U3CU3Ef__mgU24cacheE_33(EventFunction_1_t3912835512 * value)
	{
		___U3CU3Ef__mgU24cacheE_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_34() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cacheF_34)); }
	inline EventFunction_1_t1475332338 * get_U3CU3Ef__mgU24cacheF_34() const { return ___U3CU3Ef__mgU24cacheF_34; }
	inline EventFunction_1_t1475332338 ** get_address_of_U3CU3Ef__mgU24cacheF_34() { return &___U3CU3Ef__mgU24cacheF_34; }
	inline void set_U3CU3Ef__mgU24cacheF_34(EventFunction_1_t1475332338 * value)
	{
		___U3CU3Ef__mgU24cacheF_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_35() { return static_cast<int32_t>(offsetof(ExecuteEvents_t3484638744_StaticFields, ___U3CU3Ef__mgU24cache10_35)); }
	inline EventFunction_1_t2658898854 * get_U3CU3Ef__mgU24cache10_35() const { return ___U3CU3Ef__mgU24cache10_35; }
	inline EventFunction_1_t2658898854 ** get_address_of_U3CU3Ef__mgU24cache10_35() { return &___U3CU3Ef__mgU24cache10_35; }
	inline void set_U3CU3Ef__mgU24cache10_35(EventFunction_1_t2658898854 * value)
	{
		___U3CU3Ef__mgU24cache10_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEEVENTS_T3484638744_H
#ifndef MOUSESTATE_T384203932_H
#define MOUSESTATE_T384203932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseState
struct  MouseState_t384203932  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState> UnityEngine.EventSystems.PointerInputModule/MouseState::m_TrackedButtons
	List_1_t2329214678 * ___m_TrackedButtons_0;

public:
	inline static int32_t get_offset_of_m_TrackedButtons_0() { return static_cast<int32_t>(offsetof(MouseState_t384203932, ___m_TrackedButtons_0)); }
	inline List_1_t2329214678 * get_m_TrackedButtons_0() const { return ___m_TrackedButtons_0; }
	inline List_1_t2329214678 ** get_address_of_m_TrackedButtons_0() { return &___m_TrackedButtons_0; }
	inline void set_m_TrackedButtons_0(List_1_t2329214678 * value)
	{
		___m_TrackedButtons_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedButtons_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSESTATE_T384203932_H
#ifndef RAYCASTERMANAGER_T2536340562_H
#define RAYCASTERMANAGER_T2536340562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t2536340562  : public RuntimeObject
{
public:

public:
};

struct RaycasterManager_t2536340562_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t1327982029 * ___s_Raycasters_0;

public:
	inline static int32_t get_offset_of_s_Raycasters_0() { return static_cast<int32_t>(offsetof(RaycasterManager_t2536340562_StaticFields, ___s_Raycasters_0)); }
	inline List_1_t1327982029 * get_s_Raycasters_0() const { return ___s_Raycasters_0; }
	inline List_1_t1327982029 ** get_address_of_s_Raycasters_0() { return &___s_Raycasters_0; }
	inline void set_s_Raycasters_0(List_1_t1327982029 * value)
	{
		___s_Raycasters_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Raycasters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTERMANAGER_T2536340562_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef ANIMATIONTRIGGERS_T2532145056_H
#define ANIMATIONTRIGGERS_T2532145056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AnimationTriggers
struct  AnimationTriggers_t2532145056  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.AnimationTriggers::m_NormalTrigger
	String_t* ___m_NormalTrigger_4;
	// System.String UnityEngine.UI.AnimationTriggers::m_HighlightedTrigger
	String_t* ___m_HighlightedTrigger_5;
	// System.String UnityEngine.UI.AnimationTriggers::m_PressedTrigger
	String_t* ___m_PressedTrigger_6;
	// System.String UnityEngine.UI.AnimationTriggers::m_DisabledTrigger
	String_t* ___m_DisabledTrigger_7;

public:
	inline static int32_t get_offset_of_m_NormalTrigger_4() { return static_cast<int32_t>(offsetof(AnimationTriggers_t2532145056, ___m_NormalTrigger_4)); }
	inline String_t* get_m_NormalTrigger_4() const { return ___m_NormalTrigger_4; }
	inline String_t** get_address_of_m_NormalTrigger_4() { return &___m_NormalTrigger_4; }
	inline void set_m_NormalTrigger_4(String_t* value)
	{
		___m_NormalTrigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_NormalTrigger_4), value);
	}

	inline static int32_t get_offset_of_m_HighlightedTrigger_5() { return static_cast<int32_t>(offsetof(AnimationTriggers_t2532145056, ___m_HighlightedTrigger_5)); }
	inline String_t* get_m_HighlightedTrigger_5() const { return ___m_HighlightedTrigger_5; }
	inline String_t** get_address_of_m_HighlightedTrigger_5() { return &___m_HighlightedTrigger_5; }
	inline void set_m_HighlightedTrigger_5(String_t* value)
	{
		___m_HighlightedTrigger_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedTrigger_5), value);
	}

	inline static int32_t get_offset_of_m_PressedTrigger_6() { return static_cast<int32_t>(offsetof(AnimationTriggers_t2532145056, ___m_PressedTrigger_6)); }
	inline String_t* get_m_PressedTrigger_6() const { return ___m_PressedTrigger_6; }
	inline String_t** get_address_of_m_PressedTrigger_6() { return &___m_PressedTrigger_6; }
	inline void set_m_PressedTrigger_6(String_t* value)
	{
		___m_PressedTrigger_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedTrigger_6), value);
	}

	inline static int32_t get_offset_of_m_DisabledTrigger_7() { return static_cast<int32_t>(offsetof(AnimationTriggers_t2532145056, ___m_DisabledTrigger_7)); }
	inline String_t* get_m_DisabledTrigger_7() const { return ___m_DisabledTrigger_7; }
	inline String_t** get_address_of_m_DisabledTrigger_7() { return &___m_DisabledTrigger_7; }
	inline void set_m_DisabledTrigger_7(String_t* value)
	{
		___m_DisabledTrigger_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledTrigger_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRIGGERS_T2532145056_H
#ifndef U3CONFINISHSUBMITU3EC__ITERATOR0_T3413438900_H
#define U3CONFINISHSUBMITU3EC__ITERATOR0_T3413438900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0
struct  U3COnFinishSubmitU3Ec__Iterator0_t3413438900  : public RuntimeObject
{
public:
	// System.Single UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0::<fadeTime>__0
	float ___U3CfadeTimeU3E__0_0;
	// System.Single UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// UnityEngine.UI.Button UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0::$this
	Button_t4055032469 * ___U24this_2;
	// System.Object UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CfadeTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ec__Iterator0_t3413438900, ___U3CfadeTimeU3E__0_0)); }
	inline float get_U3CfadeTimeU3E__0_0() const { return ___U3CfadeTimeU3E__0_0; }
	inline float* get_address_of_U3CfadeTimeU3E__0_0() { return &___U3CfadeTimeU3E__0_0; }
	inline void set_U3CfadeTimeU3E__0_0(float value)
	{
		___U3CfadeTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ec__Iterator0_t3413438900, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ec__Iterator0_t3413438900, ___U24this_2)); }
	inline Button_t4055032469 * get_U24this_2() const { return ___U24this_2; }
	inline Button_t4055032469 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Button_t4055032469 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ec__Iterator0_t3413438900, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ec__Iterator0_t3413438900, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3COnFinishSubmitU3Ec__Iterator0_t3413438900, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONFINISHSUBMITU3EC__ITERATOR0_T3413438900_H
#ifndef CANVASUPDATEREGISTRY_T2720824592_H
#define CANVASUPDATEREGISTRY_T2720824592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasUpdateRegistry
struct  CanvasUpdateRegistry_t2720824592  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.UI.CanvasUpdateRegistry::m_PerformingLayoutUpdate
	bool ___m_PerformingLayoutUpdate_1;
	// System.Boolean UnityEngine.UI.CanvasUpdateRegistry::m_PerformingGraphicUpdate
	bool ___m_PerformingGraphicUpdate_2;
	// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement> UnityEngine.UI.CanvasUpdateRegistry::m_LayoutRebuildQueue
	IndexedSet_1_t3571286806 * ___m_LayoutRebuildQueue_3;
	// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement> UnityEngine.UI.CanvasUpdateRegistry::m_GraphicRebuildQueue
	IndexedSet_1_t3571286806 * ___m_GraphicRebuildQueue_4;

public:
	inline static int32_t get_offset_of_m_PerformingLayoutUpdate_1() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592, ___m_PerformingLayoutUpdate_1)); }
	inline bool get_m_PerformingLayoutUpdate_1() const { return ___m_PerformingLayoutUpdate_1; }
	inline bool* get_address_of_m_PerformingLayoutUpdate_1() { return &___m_PerformingLayoutUpdate_1; }
	inline void set_m_PerformingLayoutUpdate_1(bool value)
	{
		___m_PerformingLayoutUpdate_1 = value;
	}

	inline static int32_t get_offset_of_m_PerformingGraphicUpdate_2() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592, ___m_PerformingGraphicUpdate_2)); }
	inline bool get_m_PerformingGraphicUpdate_2() const { return ___m_PerformingGraphicUpdate_2; }
	inline bool* get_address_of_m_PerformingGraphicUpdate_2() { return &___m_PerformingGraphicUpdate_2; }
	inline void set_m_PerformingGraphicUpdate_2(bool value)
	{
		___m_PerformingGraphicUpdate_2 = value;
	}

	inline static int32_t get_offset_of_m_LayoutRebuildQueue_3() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592, ___m_LayoutRebuildQueue_3)); }
	inline IndexedSet_1_t3571286806 * get_m_LayoutRebuildQueue_3() const { return ___m_LayoutRebuildQueue_3; }
	inline IndexedSet_1_t3571286806 ** get_address_of_m_LayoutRebuildQueue_3() { return &___m_LayoutRebuildQueue_3; }
	inline void set_m_LayoutRebuildQueue_3(IndexedSet_1_t3571286806 * value)
	{
		___m_LayoutRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_4() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592, ___m_GraphicRebuildQueue_4)); }
	inline IndexedSet_1_t3571286806 * get_m_GraphicRebuildQueue_4() const { return ___m_GraphicRebuildQueue_4; }
	inline IndexedSet_1_t3571286806 ** get_address_of_m_GraphicRebuildQueue_4() { return &___m_GraphicRebuildQueue_4; }
	inline void set_m_GraphicRebuildQueue_4(IndexedSet_1_t3571286806 * value)
	{
		___m_GraphicRebuildQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_4), value);
	}
};

struct CanvasUpdateRegistry_t2720824592_StaticFields
{
public:
	// UnityEngine.UI.CanvasUpdateRegistry UnityEngine.UI.CanvasUpdateRegistry::s_Instance
	CanvasUpdateRegistry_t2720824592 * ___s_Instance_0;
	// System.Comparison`1<UnityEngine.UI.ICanvasElement> UnityEngine.UI.CanvasUpdateRegistry::s_SortLayoutFunction
	Comparison_1_t1896830045 * ___s_SortLayoutFunction_5;
	// System.Comparison`1<UnityEngine.UI.ICanvasElement> UnityEngine.UI.CanvasUpdateRegistry::<>f__mg$cache0
	Comparison_1_t1896830045 * ___U3CU3Ef__mgU24cache0_6;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592_StaticFields, ___s_Instance_0)); }
	inline CanvasUpdateRegistry_t2720824592 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline CanvasUpdateRegistry_t2720824592 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(CanvasUpdateRegistry_t2720824592 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}

	inline static int32_t get_offset_of_s_SortLayoutFunction_5() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592_StaticFields, ___s_SortLayoutFunction_5)); }
	inline Comparison_1_t1896830045 * get_s_SortLayoutFunction_5() const { return ___s_SortLayoutFunction_5; }
	inline Comparison_1_t1896830045 ** get_address_of_s_SortLayoutFunction_5() { return &___s_SortLayoutFunction_5; }
	inline void set_s_SortLayoutFunction_5(Comparison_1_t1896830045 * value)
	{
		___s_SortLayoutFunction_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_SortLayoutFunction_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(CanvasUpdateRegistry_t2720824592_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline Comparison_1_t1896830045 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline Comparison_1_t1896830045 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(Comparison_1_t1896830045 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASUPDATEREGISTRY_T2720824592_H
#ifndef POINT_T3074390632_H
#define POINT_T3074390632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Extensions.TextureExtensions/Point
struct  Point_t3074390632 
{
public:
	// System.Int16 HutongGames.Extensions.TextureExtensions/Point::x
	int16_t ___x_0;
	// System.Int16 HutongGames.Extensions.TextureExtensions/Point::y
	int16_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Point_t3074390632, ___x_0)); }
	inline int16_t get_x_0() const { return ___x_0; }
	inline int16_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int16_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Point_t3074390632, ___y_1)); }
	inline int16_t get_y_1() const { return ___y_1; }
	inline int16_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int16_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINT_T3074390632_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef UNITYEVENT_1_T3437345828_H
#define UNITYEVENT_1_T3437345828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3437345828  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3437345828, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3437345828_H
#ifndef UNITYEVENT_1_T489719741_H
#define UNITYEVENT_1_T489719741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t489719741  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t489719741, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T489719741_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef FLOATTWEEN_T1274330004_H
#define FLOATTWEEN_T1274330004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_t1274330004 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t1856710240 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_t1274330004, ___m_Target_0)); }
	inline FloatTweenCallback_t1856710240 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t1856710240 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t1856710240 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_t1274330004, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_t1274330004, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_t1274330004, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_t1274330004, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_t1274330004_marshaled_pinvoke
{
	FloatTweenCallback_t1856710240 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_t1274330004_marshaled_com
{
	FloatTweenCallback_t1856710240 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
#endif // FLOATTWEEN_T1274330004_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef LOOPTYPE_T1488839698_H
#define LOOPTYPE_T1488839698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.AnimationEnums.LoopType
struct  LoopType_t1488839698 
{
public:
	// System.Int32 HutongGames.PlayMaker.AnimationEnums.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t1488839698, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1488839698_H
#ifndef POSITIONOPTIONS_T658883822_H
#define POSITIONOPTIONS_T658883822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.AnimationEnums.PositionOptions
struct  PositionOptions_t658883822 
{
public:
	// System.Int32 HutongGames.PlayMaker.AnimationEnums.PositionOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PositionOptions_t658883822, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONOPTIONS_T658883822_H
#ifndef ROTATIONINTERPOLATION_T1456034865_H
#define ROTATIONINTERPOLATION_T1456034865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.AnimationEnums.RotationInterpolation
struct  RotationInterpolation_t1456034865 
{
public:
	// System.Int32 HutongGames.PlayMaker.AnimationEnums.RotationInterpolation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationInterpolation_t1456034865, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONINTERPOLATION_T1456034865_H
#ifndef ROTATIONOPTIONS_T1877027720_H
#define ROTATIONOPTIONS_T1877027720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.AnimationEnums.RotationOptions
struct  RotationOptions_t1877027720 
{
public:
	// System.Int32 HutongGames.PlayMaker.AnimationEnums.RotationOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationOptions_t1877027720, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONOPTIONS_T1877027720_H
#ifndef SCALEOPTIONS_T1715998470_H
#define SCALEOPTIONS_T1715998470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.AnimationEnums.ScaleOptions
struct  ScaleOptions_t1715998470 
{
public:
	// System.Int32 HutongGames.PlayMaker.AnimationEnums.ScaleOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleOptions_t1715998470, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEOPTIONS_T1715998470_H
#ifndef TARGETVALUEOPTIONS_T2879432043_H
#define TARGETVALUEOPTIONS_T2879432043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.AnimationEnums.TargetValueOptions
struct  TargetValueOptions_t2879432043 
{
public:
	// System.Int32 HutongGames.PlayMaker.AnimationEnums.TargetValueOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetValueOptions_t2879432043, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETVALUEOPTIONS_T2879432043_H
#ifndef ARRAYVARIABLETYPESNICIFIED_T4011111753_H
#define ARRAYVARIABLETYPESNICIFIED_T4011111753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ArrayVariableTypesNicified
struct  ArrayVariableTypesNicified_t4011111753 
{
public:
	// System.Int32 HutongGames.PlayMaker.ArrayVariableTypesNicified::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ArrayVariableTypesNicified_t4011111753, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYVARIABLETYPESNICIFIED_T4011111753_H
#ifndef EDITORFLAGS_T2240759143_H
#define EDITORFLAGS_T2240759143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Fsm/EditorFlags
struct  EditorFlags_t2240759143 
{
public:
	// System.Int32 HutongGames.PlayMaker.Fsm/EditorFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EditorFlags_t2240759143, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFLAGS_T2240759143_H
#ifndef FSMLOGTYPE_T779083073_H
#define FSMLOGTYPE_T779083073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmLogType
struct  FsmLogType_t779083073 
{
public:
	// System.Int32 HutongGames.PlayMaker.FsmLogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FsmLogType_t779083073, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMLOGTYPE_T779083073_H
#ifndef FSMSTATE_T4124398234_H
#define FSMSTATE_T4124398234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmState
struct  FsmState_t4124398234  : public RuntimeObject
{
public:
	// System.Single HutongGames.PlayMaker.FsmState::<StateTime>k__BackingField
	float ___U3CStateTimeU3Ek__BackingField_0;
	// System.Single HutongGames.PlayMaker.FsmState::<RealStartTime>k__BackingField
	float ___U3CRealStartTimeU3Ek__BackingField_1;
	// System.Int32 HutongGames.PlayMaker.FsmState::<loopCount>k__BackingField
	int32_t ___U3CloopCountU3Ek__BackingField_2;
	// System.Int32 HutongGames.PlayMaker.FsmState::<maxLoopCount>k__BackingField
	int32_t ___U3CmaxLoopCountU3Ek__BackingField_3;
	// System.Boolean HutongGames.PlayMaker.FsmState::active
	bool ___active_4;
	// System.Boolean HutongGames.PlayMaker.FsmState::finished
	bool ___finished_5;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmState::activeAction
	FsmStateAction_t3801304101 * ___activeAction_6;
	// System.Int32 HutongGames.PlayMaker.FsmState::activeActionIndex
	int32_t ___activeActionIndex_7;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmState::fsm
	Fsm_t4127147824 * ___fsm_8;
	// System.String HutongGames.PlayMaker.FsmState::name
	String_t* ___name_9;
	// System.String HutongGames.PlayMaker.FsmState::description
	String_t* ___description_10;
	// System.Byte HutongGames.PlayMaker.FsmState::colorIndex
	uint8_t ___colorIndex_11;
	// UnityEngine.Rect HutongGames.PlayMaker.FsmState::position
	Rect_t2360479859  ___position_12;
	// System.Boolean HutongGames.PlayMaker.FsmState::isBreakpoint
	bool ___isBreakpoint_13;
	// System.Boolean HutongGames.PlayMaker.FsmState::isSequence
	bool ___isSequence_14;
	// System.Boolean HutongGames.PlayMaker.FsmState::hideUnused
	bool ___hideUnused_15;
	// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.FsmState::transitions
	FsmTransitionU5BU5D_t3577734832* ___transitions_16;
	// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.FsmState::actions
	FsmStateActionU5BU5D_t1918078376* ___actions_17;
	// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.FsmState::actionData
	ActionData_t1922786972 * ___actionData_18;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::activeActions
	List_1_t978411547 * ___activeActions_19;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::_finishedActions
	List_1_t978411547 * ____finishedActions_20;

public:
	inline static int32_t get_offset_of_U3CStateTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CStateTimeU3Ek__BackingField_0)); }
	inline float get_U3CStateTimeU3Ek__BackingField_0() const { return ___U3CStateTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CStateTimeU3Ek__BackingField_0() { return &___U3CStateTimeU3Ek__BackingField_0; }
	inline void set_U3CStateTimeU3Ek__BackingField_0(float value)
	{
		___U3CStateTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRealStartTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CRealStartTimeU3Ek__BackingField_1)); }
	inline float get_U3CRealStartTimeU3Ek__BackingField_1() const { return ___U3CRealStartTimeU3Ek__BackingField_1; }
	inline float* get_address_of_U3CRealStartTimeU3Ek__BackingField_1() { return &___U3CRealStartTimeU3Ek__BackingField_1; }
	inline void set_U3CRealStartTimeU3Ek__BackingField_1(float value)
	{
		___U3CRealStartTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CloopCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CloopCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CloopCountU3Ek__BackingField_2() const { return ___U3CloopCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CloopCountU3Ek__BackingField_2() { return &___U3CloopCountU3Ek__BackingField_2; }
	inline void set_U3CloopCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CloopCountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmaxLoopCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___U3CmaxLoopCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CmaxLoopCountU3Ek__BackingField_3() const { return ___U3CmaxLoopCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmaxLoopCountU3Ek__BackingField_3() { return &___U3CmaxLoopCountU3Ek__BackingField_3; }
	inline void set_U3CmaxLoopCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CmaxLoopCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_finished_5() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___finished_5)); }
	inline bool get_finished_5() const { return ___finished_5; }
	inline bool* get_address_of_finished_5() { return &___finished_5; }
	inline void set_finished_5(bool value)
	{
		___finished_5 = value;
	}

	inline static int32_t get_offset_of_activeAction_6() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___activeAction_6)); }
	inline FsmStateAction_t3801304101 * get_activeAction_6() const { return ___activeAction_6; }
	inline FsmStateAction_t3801304101 ** get_address_of_activeAction_6() { return &___activeAction_6; }
	inline void set_activeAction_6(FsmStateAction_t3801304101 * value)
	{
		___activeAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___activeAction_6), value);
	}

	inline static int32_t get_offset_of_activeActionIndex_7() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___activeActionIndex_7)); }
	inline int32_t get_activeActionIndex_7() const { return ___activeActionIndex_7; }
	inline int32_t* get_address_of_activeActionIndex_7() { return &___activeActionIndex_7; }
	inline void set_activeActionIndex_7(int32_t value)
	{
		___activeActionIndex_7 = value;
	}

	inline static int32_t get_offset_of_fsm_8() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___fsm_8)); }
	inline Fsm_t4127147824 * get_fsm_8() const { return ___fsm_8; }
	inline Fsm_t4127147824 ** get_address_of_fsm_8() { return &___fsm_8; }
	inline void set_fsm_8(Fsm_t4127147824 * value)
	{
		___fsm_8 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_8), value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_colorIndex_11() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___colorIndex_11)); }
	inline uint8_t get_colorIndex_11() const { return ___colorIndex_11; }
	inline uint8_t* get_address_of_colorIndex_11() { return &___colorIndex_11; }
	inline void set_colorIndex_11(uint8_t value)
	{
		___colorIndex_11 = value;
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___position_12)); }
	inline Rect_t2360479859  get_position_12() const { return ___position_12; }
	inline Rect_t2360479859 * get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(Rect_t2360479859  value)
	{
		___position_12 = value;
	}

	inline static int32_t get_offset_of_isBreakpoint_13() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___isBreakpoint_13)); }
	inline bool get_isBreakpoint_13() const { return ___isBreakpoint_13; }
	inline bool* get_address_of_isBreakpoint_13() { return &___isBreakpoint_13; }
	inline void set_isBreakpoint_13(bool value)
	{
		___isBreakpoint_13 = value;
	}

	inline static int32_t get_offset_of_isSequence_14() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___isSequence_14)); }
	inline bool get_isSequence_14() const { return ___isSequence_14; }
	inline bool* get_address_of_isSequence_14() { return &___isSequence_14; }
	inline void set_isSequence_14(bool value)
	{
		___isSequence_14 = value;
	}

	inline static int32_t get_offset_of_hideUnused_15() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___hideUnused_15)); }
	inline bool get_hideUnused_15() const { return ___hideUnused_15; }
	inline bool* get_address_of_hideUnused_15() { return &___hideUnused_15; }
	inline void set_hideUnused_15(bool value)
	{
		___hideUnused_15 = value;
	}

	inline static int32_t get_offset_of_transitions_16() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___transitions_16)); }
	inline FsmTransitionU5BU5D_t3577734832* get_transitions_16() const { return ___transitions_16; }
	inline FsmTransitionU5BU5D_t3577734832** get_address_of_transitions_16() { return &___transitions_16; }
	inline void set_transitions_16(FsmTransitionU5BU5D_t3577734832* value)
	{
		___transitions_16 = value;
		Il2CppCodeGenWriteBarrier((&___transitions_16), value);
	}

	inline static int32_t get_offset_of_actions_17() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___actions_17)); }
	inline FsmStateActionU5BU5D_t1918078376* get_actions_17() const { return ___actions_17; }
	inline FsmStateActionU5BU5D_t1918078376** get_address_of_actions_17() { return &___actions_17; }
	inline void set_actions_17(FsmStateActionU5BU5D_t1918078376* value)
	{
		___actions_17 = value;
		Il2CppCodeGenWriteBarrier((&___actions_17), value);
	}

	inline static int32_t get_offset_of_actionData_18() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___actionData_18)); }
	inline ActionData_t1922786972 * get_actionData_18() const { return ___actionData_18; }
	inline ActionData_t1922786972 ** get_address_of_actionData_18() { return &___actionData_18; }
	inline void set_actionData_18(ActionData_t1922786972 * value)
	{
		___actionData_18 = value;
		Il2CppCodeGenWriteBarrier((&___actionData_18), value);
	}

	inline static int32_t get_offset_of_activeActions_19() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ___activeActions_19)); }
	inline List_1_t978411547 * get_activeActions_19() const { return ___activeActions_19; }
	inline List_1_t978411547 ** get_address_of_activeActions_19() { return &___activeActions_19; }
	inline void set_activeActions_19(List_1_t978411547 * value)
	{
		___activeActions_19 = value;
		Il2CppCodeGenWriteBarrier((&___activeActions_19), value);
	}

	inline static int32_t get_offset_of__finishedActions_20() { return static_cast<int32_t>(offsetof(FsmState_t4124398234, ____finishedActions_20)); }
	inline List_1_t978411547 * get__finishedActions_20() const { return ____finishedActions_20; }
	inline List_1_t978411547 ** get_address_of__finishedActions_20() { return &____finishedActions_20; }
	inline void set__finishedActions_20(List_1_t978411547 * value)
	{
		____finishedActions_20 = value;
		Il2CppCodeGenWriteBarrier((&____finishedActions_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATE_T4124398234_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef CUSTOMLINKCONSTRAINT_T2821529372_H
#define CUSTOMLINKCONSTRAINT_T2821529372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint
struct  CustomLinkConstraint_t2821529372 
{
public:
	// System.Byte HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomLinkConstraint_t2821529372, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLINKCONSTRAINT_T2821529372_H
#ifndef CUSTOMLINKSTYLE_T99432972_H
#define CUSTOMLINKSTYLE_T99432972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTransition/CustomLinkStyle
struct  CustomLinkStyle_t99432972 
{
public:
	// System.Byte HutongGames.PlayMaker.FsmTransition/CustomLinkStyle::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CustomLinkStyle_t99432972, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMLINKSTYLE_T99432972_H
#ifndef UIEVENTS_T57509442_H
#define UIEVENTS_T57509442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UiEvents
struct  UiEvents_t57509442 
{
public:
	// System.Int32 HutongGames.PlayMaker.UiEvents::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UiEvents_t57509442, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIEVENTS_T57509442_H
#ifndef VARIABLETYPE_T1723760892_H
#define VARIABLETYPE_T1723760892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableType
struct  VariableType_t1723760892 
{
public:
	// System.Int32 HutongGames.PlayMaker.VariableType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VariableType_t1723760892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPE_T1723760892_H
#ifndef VARIABLETYPENICIFIED_T4087325594_H
#define VARIABLETYPENICIFIED_T4087325594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableTypeNicified
struct  VariableTypeNicified_t4087325594 
{
public:
	// System.Int32 HutongGames.PlayMaker.VariableTypeNicified::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VariableTypeNicified_t4087325594, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPENICIFIED_T4087325594_H
#ifndef EVENTHANDLE_T600343995_H
#define EVENTHANDLE_T600343995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t600343995 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventHandle_t600343995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_T600343995_H
#ifndef TRIGGEREVENT_T3867320123_H
#define TRIGGEREVENT_T3867320123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct  TriggerEvent_t3867320123  : public UnityEvent_1_t489719741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T3867320123_H
#ifndef EVENTTRIGGERTYPE_T55832929_H
#define EVENTTRIGGERTYPE_T55832929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_t55832929 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventTriggerType_t55832929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGERTYPE_T55832929_H
#ifndef MOVEDIRECTION_T1216237838_H
#define MOVEDIRECTION_T1216237838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t1216237838 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MoveDirection_t1216237838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T1216237838_H
#ifndef FRAMEPRESSSTATE_T3039385657_H
#define FRAMEPRESSSTATE_T3039385657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/FramePressState
struct  FramePressState_t3039385657 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/FramePressState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FramePressState_t3039385657, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEPRESSSTATE_T3039385657_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef INPUTMODE_T3382566315_H
#define INPUTMODE_T3382566315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.StandaloneInputModule/InputMode
struct  InputMode_t3382566315 
{
public:
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule/InputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputMode_t3382566315, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMODE_T3382566315_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef BUTTONCLICKEDEVENT_T48803504_H
#define BUTTONCLICKEDEVENT_T48803504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button/ButtonClickedEvent
struct  ButtonClickedEvent_t48803504  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCLICKEDEVENT_T48803504_H
#ifndef CANVASUPDATE_T2572322932_H
#define CANVASUPDATE_T2572322932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasUpdate
struct  CanvasUpdate_t2572322932 
{
public:
	// System.Int32 UnityEngine.UI.CanvasUpdate::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CanvasUpdate_t2572322932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASUPDATE_T2572322932_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef COLORTWEENCALLBACK_T1121741130_H
#define COLORTWEENCALLBACK_T1121741130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct  ColorTweenCallback_t1121741130  : public UnityEvent_1_t3437345828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENCALLBACK_T1121741130_H
#ifndef COLORTWEENMODE_T1000778859_H
#define COLORTWEENMODE_T1000778859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
struct  ColorTweenMode_t1000778859 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorTweenMode_t1000778859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_T1000778859_H
#ifndef FLOATTWEENCALLBACK_T1856710240_H
#define FLOATTWEENCALLBACK_T1856710240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct  FloatTweenCallback_t1856710240  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATTWEENCALLBACK_T1856710240_H
#ifndef DEFAULTCONTROLS_T4098465386_H
#define DEFAULTCONTROLS_T4098465386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.DefaultControls
struct  DefaultControls_t4098465386  : public RuntimeObject
{
public:

public:
};

struct DefaultControls_t4098465386_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.DefaultControls::s_ThickElementSize
	Vector2_t2156229523  ___s_ThickElementSize_3;
	// UnityEngine.Vector2 UnityEngine.UI.DefaultControls::s_ThinElementSize
	Vector2_t2156229523  ___s_ThinElementSize_4;
	// UnityEngine.Vector2 UnityEngine.UI.DefaultControls::s_ImageElementSize
	Vector2_t2156229523  ___s_ImageElementSize_5;
	// UnityEngine.Color UnityEngine.UI.DefaultControls::s_DefaultSelectableColor
	Color_t2555686324  ___s_DefaultSelectableColor_6;
	// UnityEngine.Color UnityEngine.UI.DefaultControls::s_PanelColor
	Color_t2555686324  ___s_PanelColor_7;
	// UnityEngine.Color UnityEngine.UI.DefaultControls::s_TextColor
	Color_t2555686324  ___s_TextColor_8;

public:
	inline static int32_t get_offset_of_s_ThickElementSize_3() { return static_cast<int32_t>(offsetof(DefaultControls_t4098465386_StaticFields, ___s_ThickElementSize_3)); }
	inline Vector2_t2156229523  get_s_ThickElementSize_3() const { return ___s_ThickElementSize_3; }
	inline Vector2_t2156229523 * get_address_of_s_ThickElementSize_3() { return &___s_ThickElementSize_3; }
	inline void set_s_ThickElementSize_3(Vector2_t2156229523  value)
	{
		___s_ThickElementSize_3 = value;
	}

	inline static int32_t get_offset_of_s_ThinElementSize_4() { return static_cast<int32_t>(offsetof(DefaultControls_t4098465386_StaticFields, ___s_ThinElementSize_4)); }
	inline Vector2_t2156229523  get_s_ThinElementSize_4() const { return ___s_ThinElementSize_4; }
	inline Vector2_t2156229523 * get_address_of_s_ThinElementSize_4() { return &___s_ThinElementSize_4; }
	inline void set_s_ThinElementSize_4(Vector2_t2156229523  value)
	{
		___s_ThinElementSize_4 = value;
	}

	inline static int32_t get_offset_of_s_ImageElementSize_5() { return static_cast<int32_t>(offsetof(DefaultControls_t4098465386_StaticFields, ___s_ImageElementSize_5)); }
	inline Vector2_t2156229523  get_s_ImageElementSize_5() const { return ___s_ImageElementSize_5; }
	inline Vector2_t2156229523 * get_address_of_s_ImageElementSize_5() { return &___s_ImageElementSize_5; }
	inline void set_s_ImageElementSize_5(Vector2_t2156229523  value)
	{
		___s_ImageElementSize_5 = value;
	}

	inline static int32_t get_offset_of_s_DefaultSelectableColor_6() { return static_cast<int32_t>(offsetof(DefaultControls_t4098465386_StaticFields, ___s_DefaultSelectableColor_6)); }
	inline Color_t2555686324  get_s_DefaultSelectableColor_6() const { return ___s_DefaultSelectableColor_6; }
	inline Color_t2555686324 * get_address_of_s_DefaultSelectableColor_6() { return &___s_DefaultSelectableColor_6; }
	inline void set_s_DefaultSelectableColor_6(Color_t2555686324  value)
	{
		___s_DefaultSelectableColor_6 = value;
	}

	inline static int32_t get_offset_of_s_PanelColor_7() { return static_cast<int32_t>(offsetof(DefaultControls_t4098465386_StaticFields, ___s_PanelColor_7)); }
	inline Color_t2555686324  get_s_PanelColor_7() const { return ___s_PanelColor_7; }
	inline Color_t2555686324 * get_address_of_s_PanelColor_7() { return &___s_PanelColor_7; }
	inline void set_s_PanelColor_7(Color_t2555686324  value)
	{
		___s_PanelColor_7 = value;
	}

	inline static int32_t get_offset_of_s_TextColor_8() { return static_cast<int32_t>(offsetof(DefaultControls_t4098465386_StaticFields, ___s_TextColor_8)); }
	inline Color_t2555686324  get_s_TextColor_8() const { return ___s_TextColor_8; }
	inline Color_t2555686324 * get_address_of_s_TextColor_8() { return &___s_TextColor_8; }
	inline void set_s_TextColor_8(Color_t2555686324  value)
	{
		___s_TextColor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTROLS_T4098465386_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef MISSINGACTION_T3641918577_H
#define MISSINGACTION_T3641918577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MissingAction
struct  MissingAction_t3641918577  : public FsmStateAction_t3801304101
{
public:
	// System.String HutongGames.PlayMaker.Actions.MissingAction::actionName
	String_t* ___actionName_14;

public:
	inline static int32_t get_offset_of_actionName_14() { return static_cast<int32_t>(offsetof(MissingAction_t3641918577, ___actionName_14)); }
	inline String_t* get_actionName_14() const { return ___actionName_14; }
	inline String_t** get_address_of_actionName_14() { return &___actionName_14; }
	inline void set_actionName_14(String_t* value)
	{
		___actionName_14 = value;
		Il2CppCodeGenWriteBarrier((&___actionName_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGACTION_T3641918577_H
#ifndef FSM_T4127147824_H
#define FSM_T4127147824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Fsm
struct  Fsm_t4127147824  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo HutongGames.PlayMaker.Fsm::updateHelperSetDirty
	MethodInfo_t * ___updateHelperSetDirty_2;
	// System.Int32 HutongGames.PlayMaker.Fsm::dataVersion
	int32_t ___dataVersion_7;
	// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Fsm::owner
	MonoBehaviour_t3962482529 * ___owner_8;
	// FsmTemplate HutongGames.PlayMaker.Fsm::usedInTemplate
	FsmTemplate_t1663266674 * ___usedInTemplate_9;
	// System.String HutongGames.PlayMaker.Fsm::name
	String_t* ___name_10;
	// System.String HutongGames.PlayMaker.Fsm::startState
	String_t* ___startState_11;
	// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::states
	FsmStateU5BU5D_t3458670015* ___states_12;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::events
	FsmEventU5BU5D_t2511618479* ___events_13;
	// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::globalTransitions
	FsmTransitionU5BU5D_t3577734832* ___globalTransitions_14;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::variables
	FsmVariables_t3349589544 * ___variables_15;
	// System.String HutongGames.PlayMaker.Fsm::description
	String_t* ___description_16;
	// System.String HutongGames.PlayMaker.Fsm::docUrl
	String_t* ___docUrl_17;
	// System.Boolean HutongGames.PlayMaker.Fsm::showStateLabel
	bool ___showStateLabel_18;
	// System.Int32 HutongGames.PlayMaker.Fsm::maxLoopCount
	int32_t ___maxLoopCount_19;
	// System.String HutongGames.PlayMaker.Fsm::watermark
	String_t* ___watermark_20;
	// System.String HutongGames.PlayMaker.Fsm::password
	String_t* ___password_21;
	// System.Boolean HutongGames.PlayMaker.Fsm::locked
	bool ___locked_22;
	// System.Boolean HutongGames.PlayMaker.Fsm::manualUpdate
	bool ___manualUpdate_23;
	// System.Boolean HutongGames.PlayMaker.Fsm::keepDelayedEventsOnStateExit
	bool ___keepDelayedEventsOnStateExit_24;
	// System.Boolean HutongGames.PlayMaker.Fsm::preprocessed
	bool ___preprocessed_25;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::host
	Fsm_t4127147824 * ___host_26;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::rootFsm
	Fsm_t4127147824 * ___rootFsm_27;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::subFsmList
	List_1_t1304255270 * ___subFsmList_28;
	// System.Boolean HutongGames.PlayMaker.Fsm::setDirty
	bool ___setDirty_29;
	// System.Boolean HutongGames.PlayMaker.Fsm::activeStateEntered
	bool ___activeStateEntered_30;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.Fsm::ExposedEvents
	List_1_t913407328 * ___ExposedEvents_31;
	// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.Fsm::myLog
	FsmLog_t3265252880 * ___myLog_32;
	// System.Boolean HutongGames.PlayMaker.Fsm::RestartOnEnable
	bool ___RestartOnEnable_33;
	// System.Boolean HutongGames.PlayMaker.Fsm::<Started>k__BackingField
	bool ___U3CStartedU3Ek__BackingField_34;
	// System.Boolean HutongGames.PlayMaker.Fsm::EnableDebugFlow
	bool ___EnableDebugFlow_35;
	// System.Boolean HutongGames.PlayMaker.Fsm::EnableBreakpoints
	bool ___EnableBreakpoints_36;
	// System.Boolean HutongGames.PlayMaker.Fsm::StepFrame
	bool ___StepFrame_37;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::delayedEvents
	List_1_t1164733674 * ___delayedEvents_38;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::updateEvents
	List_1_t1164733674 * ___updateEvents_39;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::removeEvents
	List_1_t1164733674 * ___removeEvents_40;
	// HutongGames.PlayMaker.Fsm/EditorFlags HutongGames.PlayMaker.Fsm::editorFlags
	int32_t ___editorFlags_41;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::<EventTarget>k__BackingField
	FsmEventTarget_t919699796 * ___U3CEventTargetU3Ek__BackingField_42;
	// System.Boolean HutongGames.PlayMaker.Fsm::initialized
	bool ___initialized_43;
	// System.Boolean HutongGames.PlayMaker.Fsm::<Finished>k__BackingField
	bool ___U3CFinishedU3Ek__BackingField_44;
	// System.String HutongGames.PlayMaker.Fsm::activeStateName
	String_t* ___activeStateName_45;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::activeState
	FsmState_t4124398234 * ___activeState_46;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::switchToState
	FsmState_t4124398234 * ___switchToState_47;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::previousActiveState
	FsmState_t4124398234 * ___previousActiveState_48;
	// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.Fsm::<LastTransition>k__BackingField
	FsmTransition_t1340699069 * ___U3CLastTransitionU3Ek__BackingField_49;
	// System.Action`1<HutongGames.PlayMaker.FsmState> HutongGames.PlayMaker.Fsm::StateChanged
	Action_1_t1898533 * ___StateChanged_50;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsModifiedPrefabInstance>k__BackingField
	bool ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::editState
	FsmState_t4124398234 * ___editState_53;
	// System.Boolean HutongGames.PlayMaker.Fsm::<SwitchedState>k__BackingField
	bool ___U3CSwitchedStateU3Ek__BackingField_64;
	// System.Boolean HutongGames.PlayMaker.Fsm::mouseEvents
	bool ___mouseEvents_65;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleLevelLoaded
	bool ___handleLevelLoaded_66;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerEnter2D
	bool ___handleTriggerEnter2D_67;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerExit2D
	bool ___handleTriggerExit2D_68;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerStay2D
	bool ___handleTriggerStay2D_69;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionEnter2D
	bool ___handleCollisionEnter2D_70;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionExit2D
	bool ___handleCollisionExit2D_71;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionStay2D
	bool ___handleCollisionStay2D_72;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerEnter
	bool ___handleTriggerEnter_73;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerExit
	bool ___handleTriggerExit_74;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleTriggerStay
	bool ___handleTriggerStay_75;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionEnter
	bool ___handleCollisionEnter_76;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionExit
	bool ___handleCollisionExit_77;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleCollisionStay
	bool ___handleCollisionStay_78;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleParticleCollision
	bool ___handleParticleCollision_79;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleControllerColliderHit
	bool ___handleControllerColliderHit_80;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleJointBreak
	bool ___handleJointBreak_81;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleJointBreak2D
	bool ___handleJointBreak2D_82;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleOnGUI
	bool ___handleOnGUI_83;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleFixedUpdate
	bool ___handleFixedUpdate_84;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleLateUpdate
	bool ___handleLateUpdate_85;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleApplicationEvents
	bool ___handleApplicationEvents_86;
	// HutongGames.PlayMaker.UiEvents HutongGames.PlayMaker.Fsm::handleUiEvents
	int32_t ___handleUiEvents_87;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleLegacyNetworking
	bool ___handleLegacyNetworking_88;
	// UnityEngine.Collision HutongGames.PlayMaker.Fsm::<CollisionInfo>k__BackingField
	Collision_t4262080450 * ___U3CCollisionInfoU3Ek__BackingField_89;
	// UnityEngine.Collider HutongGames.PlayMaker.Fsm::<TriggerCollider>k__BackingField
	Collider_t1773347010 * ___U3CTriggerColliderU3Ek__BackingField_90;
	// UnityEngine.Collision2D HutongGames.PlayMaker.Fsm::<Collision2DInfo>k__BackingField
	Collision2D_t2842956331 * ___U3CCollision2DInfoU3Ek__BackingField_91;
	// UnityEngine.Collider2D HutongGames.PlayMaker.Fsm::<TriggerCollider2D>k__BackingField
	Collider2D_t2806799626 * ___U3CTriggerCollider2DU3Ek__BackingField_92;
	// System.Single HutongGames.PlayMaker.Fsm::<JointBreakForce>k__BackingField
	float ___U3CJointBreakForceU3Ek__BackingField_93;
	// UnityEngine.Joint2D HutongGames.PlayMaker.Fsm::<BrokenJoint2D>k__BackingField
	Joint2D_t4180440564 * ___U3CBrokenJoint2DU3Ek__BackingField_94;
	// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::<ParticleCollisionGO>k__BackingField
	GameObject_t1113636619 * ___U3CParticleCollisionGOU3Ek__BackingField_95;
	// System.String HutongGames.PlayMaker.Fsm::<TriggerName>k__BackingField
	String_t* ___U3CTriggerNameU3Ek__BackingField_96;
	// System.String HutongGames.PlayMaker.Fsm::<CollisionName>k__BackingField
	String_t* ___U3CCollisionNameU3Ek__BackingField_97;
	// System.String HutongGames.PlayMaker.Fsm::<Trigger2dName>k__BackingField
	String_t* ___U3CTrigger2dNameU3Ek__BackingField_98;
	// System.String HutongGames.PlayMaker.Fsm::<Collision2dName>k__BackingField
	String_t* ___U3CCollision2dNameU3Ek__BackingField_99;
	// UnityEngine.ControllerColliderHit HutongGames.PlayMaker.Fsm::<ControllerCollider>k__BackingField
	ControllerColliderHit_t240592346 * ___U3CControllerColliderU3Ek__BackingField_100;
	// UnityEngine.RaycastHit HutongGames.PlayMaker.Fsm::<RaycastHitInfo>k__BackingField
	RaycastHit_t1056001966  ___U3CRaycastHitInfoU3Ek__BackingField_101;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleAnimatorMove
	bool ___handleAnimatorMove_103;
	// System.Boolean HutongGames.PlayMaker.Fsm::handleAnimatorIK
	bool ___handleAnimatorIK_104;

public:
	inline static int32_t get_offset_of_updateHelperSetDirty_2() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___updateHelperSetDirty_2)); }
	inline MethodInfo_t * get_updateHelperSetDirty_2() const { return ___updateHelperSetDirty_2; }
	inline MethodInfo_t ** get_address_of_updateHelperSetDirty_2() { return &___updateHelperSetDirty_2; }
	inline void set_updateHelperSetDirty_2(MethodInfo_t * value)
	{
		___updateHelperSetDirty_2 = value;
		Il2CppCodeGenWriteBarrier((&___updateHelperSetDirty_2), value);
	}

	inline static int32_t get_offset_of_dataVersion_7() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___dataVersion_7)); }
	inline int32_t get_dataVersion_7() const { return ___dataVersion_7; }
	inline int32_t* get_address_of_dataVersion_7() { return &___dataVersion_7; }
	inline void set_dataVersion_7(int32_t value)
	{
		___dataVersion_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___owner_8)); }
	inline MonoBehaviour_t3962482529 * get_owner_8() const { return ___owner_8; }
	inline MonoBehaviour_t3962482529 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(MonoBehaviour_t3962482529 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_usedInTemplate_9() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___usedInTemplate_9)); }
	inline FsmTemplate_t1663266674 * get_usedInTemplate_9() const { return ___usedInTemplate_9; }
	inline FsmTemplate_t1663266674 ** get_address_of_usedInTemplate_9() { return &___usedInTemplate_9; }
	inline void set_usedInTemplate_9(FsmTemplate_t1663266674 * value)
	{
		___usedInTemplate_9 = value;
		Il2CppCodeGenWriteBarrier((&___usedInTemplate_9), value);
	}

	inline static int32_t get_offset_of_name_10() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___name_10)); }
	inline String_t* get_name_10() const { return ___name_10; }
	inline String_t** get_address_of_name_10() { return &___name_10; }
	inline void set_name_10(String_t* value)
	{
		___name_10 = value;
		Il2CppCodeGenWriteBarrier((&___name_10), value);
	}

	inline static int32_t get_offset_of_startState_11() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___startState_11)); }
	inline String_t* get_startState_11() const { return ___startState_11; }
	inline String_t** get_address_of_startState_11() { return &___startState_11; }
	inline void set_startState_11(String_t* value)
	{
		___startState_11 = value;
		Il2CppCodeGenWriteBarrier((&___startState_11), value);
	}

	inline static int32_t get_offset_of_states_12() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___states_12)); }
	inline FsmStateU5BU5D_t3458670015* get_states_12() const { return ___states_12; }
	inline FsmStateU5BU5D_t3458670015** get_address_of_states_12() { return &___states_12; }
	inline void set_states_12(FsmStateU5BU5D_t3458670015* value)
	{
		___states_12 = value;
		Il2CppCodeGenWriteBarrier((&___states_12), value);
	}

	inline static int32_t get_offset_of_events_13() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___events_13)); }
	inline FsmEventU5BU5D_t2511618479* get_events_13() const { return ___events_13; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_events_13() { return &___events_13; }
	inline void set_events_13(FsmEventU5BU5D_t2511618479* value)
	{
		___events_13 = value;
		Il2CppCodeGenWriteBarrier((&___events_13), value);
	}

	inline static int32_t get_offset_of_globalTransitions_14() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___globalTransitions_14)); }
	inline FsmTransitionU5BU5D_t3577734832* get_globalTransitions_14() const { return ___globalTransitions_14; }
	inline FsmTransitionU5BU5D_t3577734832** get_address_of_globalTransitions_14() { return &___globalTransitions_14; }
	inline void set_globalTransitions_14(FsmTransitionU5BU5D_t3577734832* value)
	{
		___globalTransitions_14 = value;
		Il2CppCodeGenWriteBarrier((&___globalTransitions_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___variables_15)); }
	inline FsmVariables_t3349589544 * get_variables_15() const { return ___variables_15; }
	inline FsmVariables_t3349589544 ** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmVariables_t3349589544 * value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}

	inline static int32_t get_offset_of_description_16() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___description_16)); }
	inline String_t* get_description_16() const { return ___description_16; }
	inline String_t** get_address_of_description_16() { return &___description_16; }
	inline void set_description_16(String_t* value)
	{
		___description_16 = value;
		Il2CppCodeGenWriteBarrier((&___description_16), value);
	}

	inline static int32_t get_offset_of_docUrl_17() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___docUrl_17)); }
	inline String_t* get_docUrl_17() const { return ___docUrl_17; }
	inline String_t** get_address_of_docUrl_17() { return &___docUrl_17; }
	inline void set_docUrl_17(String_t* value)
	{
		___docUrl_17 = value;
		Il2CppCodeGenWriteBarrier((&___docUrl_17), value);
	}

	inline static int32_t get_offset_of_showStateLabel_18() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___showStateLabel_18)); }
	inline bool get_showStateLabel_18() const { return ___showStateLabel_18; }
	inline bool* get_address_of_showStateLabel_18() { return &___showStateLabel_18; }
	inline void set_showStateLabel_18(bool value)
	{
		___showStateLabel_18 = value;
	}

	inline static int32_t get_offset_of_maxLoopCount_19() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___maxLoopCount_19)); }
	inline int32_t get_maxLoopCount_19() const { return ___maxLoopCount_19; }
	inline int32_t* get_address_of_maxLoopCount_19() { return &___maxLoopCount_19; }
	inline void set_maxLoopCount_19(int32_t value)
	{
		___maxLoopCount_19 = value;
	}

	inline static int32_t get_offset_of_watermark_20() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___watermark_20)); }
	inline String_t* get_watermark_20() const { return ___watermark_20; }
	inline String_t** get_address_of_watermark_20() { return &___watermark_20; }
	inline void set_watermark_20(String_t* value)
	{
		___watermark_20 = value;
		Il2CppCodeGenWriteBarrier((&___watermark_20), value);
	}

	inline static int32_t get_offset_of_password_21() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___password_21)); }
	inline String_t* get_password_21() const { return ___password_21; }
	inline String_t** get_address_of_password_21() { return &___password_21; }
	inline void set_password_21(String_t* value)
	{
		___password_21 = value;
		Il2CppCodeGenWriteBarrier((&___password_21), value);
	}

	inline static int32_t get_offset_of_locked_22() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___locked_22)); }
	inline bool get_locked_22() const { return ___locked_22; }
	inline bool* get_address_of_locked_22() { return &___locked_22; }
	inline void set_locked_22(bool value)
	{
		___locked_22 = value;
	}

	inline static int32_t get_offset_of_manualUpdate_23() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___manualUpdate_23)); }
	inline bool get_manualUpdate_23() const { return ___manualUpdate_23; }
	inline bool* get_address_of_manualUpdate_23() { return &___manualUpdate_23; }
	inline void set_manualUpdate_23(bool value)
	{
		___manualUpdate_23 = value;
	}

	inline static int32_t get_offset_of_keepDelayedEventsOnStateExit_24() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___keepDelayedEventsOnStateExit_24)); }
	inline bool get_keepDelayedEventsOnStateExit_24() const { return ___keepDelayedEventsOnStateExit_24; }
	inline bool* get_address_of_keepDelayedEventsOnStateExit_24() { return &___keepDelayedEventsOnStateExit_24; }
	inline void set_keepDelayedEventsOnStateExit_24(bool value)
	{
		___keepDelayedEventsOnStateExit_24 = value;
	}

	inline static int32_t get_offset_of_preprocessed_25() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___preprocessed_25)); }
	inline bool get_preprocessed_25() const { return ___preprocessed_25; }
	inline bool* get_address_of_preprocessed_25() { return &___preprocessed_25; }
	inline void set_preprocessed_25(bool value)
	{
		___preprocessed_25 = value;
	}

	inline static int32_t get_offset_of_host_26() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___host_26)); }
	inline Fsm_t4127147824 * get_host_26() const { return ___host_26; }
	inline Fsm_t4127147824 ** get_address_of_host_26() { return &___host_26; }
	inline void set_host_26(Fsm_t4127147824 * value)
	{
		___host_26 = value;
		Il2CppCodeGenWriteBarrier((&___host_26), value);
	}

	inline static int32_t get_offset_of_rootFsm_27() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___rootFsm_27)); }
	inline Fsm_t4127147824 * get_rootFsm_27() const { return ___rootFsm_27; }
	inline Fsm_t4127147824 ** get_address_of_rootFsm_27() { return &___rootFsm_27; }
	inline void set_rootFsm_27(Fsm_t4127147824 * value)
	{
		___rootFsm_27 = value;
		Il2CppCodeGenWriteBarrier((&___rootFsm_27), value);
	}

	inline static int32_t get_offset_of_subFsmList_28() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___subFsmList_28)); }
	inline List_1_t1304255270 * get_subFsmList_28() const { return ___subFsmList_28; }
	inline List_1_t1304255270 ** get_address_of_subFsmList_28() { return &___subFsmList_28; }
	inline void set_subFsmList_28(List_1_t1304255270 * value)
	{
		___subFsmList_28 = value;
		Il2CppCodeGenWriteBarrier((&___subFsmList_28), value);
	}

	inline static int32_t get_offset_of_setDirty_29() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___setDirty_29)); }
	inline bool get_setDirty_29() const { return ___setDirty_29; }
	inline bool* get_address_of_setDirty_29() { return &___setDirty_29; }
	inline void set_setDirty_29(bool value)
	{
		___setDirty_29 = value;
	}

	inline static int32_t get_offset_of_activeStateEntered_30() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___activeStateEntered_30)); }
	inline bool get_activeStateEntered_30() const { return ___activeStateEntered_30; }
	inline bool* get_address_of_activeStateEntered_30() { return &___activeStateEntered_30; }
	inline void set_activeStateEntered_30(bool value)
	{
		___activeStateEntered_30 = value;
	}

	inline static int32_t get_offset_of_ExposedEvents_31() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___ExposedEvents_31)); }
	inline List_1_t913407328 * get_ExposedEvents_31() const { return ___ExposedEvents_31; }
	inline List_1_t913407328 ** get_address_of_ExposedEvents_31() { return &___ExposedEvents_31; }
	inline void set_ExposedEvents_31(List_1_t913407328 * value)
	{
		___ExposedEvents_31 = value;
		Il2CppCodeGenWriteBarrier((&___ExposedEvents_31), value);
	}

	inline static int32_t get_offset_of_myLog_32() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___myLog_32)); }
	inline FsmLog_t3265252880 * get_myLog_32() const { return ___myLog_32; }
	inline FsmLog_t3265252880 ** get_address_of_myLog_32() { return &___myLog_32; }
	inline void set_myLog_32(FsmLog_t3265252880 * value)
	{
		___myLog_32 = value;
		Il2CppCodeGenWriteBarrier((&___myLog_32), value);
	}

	inline static int32_t get_offset_of_RestartOnEnable_33() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___RestartOnEnable_33)); }
	inline bool get_RestartOnEnable_33() const { return ___RestartOnEnable_33; }
	inline bool* get_address_of_RestartOnEnable_33() { return &___RestartOnEnable_33; }
	inline void set_RestartOnEnable_33(bool value)
	{
		___RestartOnEnable_33 = value;
	}

	inline static int32_t get_offset_of_U3CStartedU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CStartedU3Ek__BackingField_34)); }
	inline bool get_U3CStartedU3Ek__BackingField_34() const { return ___U3CStartedU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CStartedU3Ek__BackingField_34() { return &___U3CStartedU3Ek__BackingField_34; }
	inline void set_U3CStartedU3Ek__BackingField_34(bool value)
	{
		___U3CStartedU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_EnableDebugFlow_35() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___EnableDebugFlow_35)); }
	inline bool get_EnableDebugFlow_35() const { return ___EnableDebugFlow_35; }
	inline bool* get_address_of_EnableDebugFlow_35() { return &___EnableDebugFlow_35; }
	inline void set_EnableDebugFlow_35(bool value)
	{
		___EnableDebugFlow_35 = value;
	}

	inline static int32_t get_offset_of_EnableBreakpoints_36() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___EnableBreakpoints_36)); }
	inline bool get_EnableBreakpoints_36() const { return ___EnableBreakpoints_36; }
	inline bool* get_address_of_EnableBreakpoints_36() { return &___EnableBreakpoints_36; }
	inline void set_EnableBreakpoints_36(bool value)
	{
		___EnableBreakpoints_36 = value;
	}

	inline static int32_t get_offset_of_StepFrame_37() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___StepFrame_37)); }
	inline bool get_StepFrame_37() const { return ___StepFrame_37; }
	inline bool* get_address_of_StepFrame_37() { return &___StepFrame_37; }
	inline void set_StepFrame_37(bool value)
	{
		___StepFrame_37 = value;
	}

	inline static int32_t get_offset_of_delayedEvents_38() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___delayedEvents_38)); }
	inline List_1_t1164733674 * get_delayedEvents_38() const { return ___delayedEvents_38; }
	inline List_1_t1164733674 ** get_address_of_delayedEvents_38() { return &___delayedEvents_38; }
	inline void set_delayedEvents_38(List_1_t1164733674 * value)
	{
		___delayedEvents_38 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvents_38), value);
	}

	inline static int32_t get_offset_of_updateEvents_39() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___updateEvents_39)); }
	inline List_1_t1164733674 * get_updateEvents_39() const { return ___updateEvents_39; }
	inline List_1_t1164733674 ** get_address_of_updateEvents_39() { return &___updateEvents_39; }
	inline void set_updateEvents_39(List_1_t1164733674 * value)
	{
		___updateEvents_39 = value;
		Il2CppCodeGenWriteBarrier((&___updateEvents_39), value);
	}

	inline static int32_t get_offset_of_removeEvents_40() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___removeEvents_40)); }
	inline List_1_t1164733674 * get_removeEvents_40() const { return ___removeEvents_40; }
	inline List_1_t1164733674 ** get_address_of_removeEvents_40() { return &___removeEvents_40; }
	inline void set_removeEvents_40(List_1_t1164733674 * value)
	{
		___removeEvents_40 = value;
		Il2CppCodeGenWriteBarrier((&___removeEvents_40), value);
	}

	inline static int32_t get_offset_of_editorFlags_41() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___editorFlags_41)); }
	inline int32_t get_editorFlags_41() const { return ___editorFlags_41; }
	inline int32_t* get_address_of_editorFlags_41() { return &___editorFlags_41; }
	inline void set_editorFlags_41(int32_t value)
	{
		___editorFlags_41 = value;
	}

	inline static int32_t get_offset_of_U3CEventTargetU3Ek__BackingField_42() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CEventTargetU3Ek__BackingField_42)); }
	inline FsmEventTarget_t919699796 * get_U3CEventTargetU3Ek__BackingField_42() const { return ___U3CEventTargetU3Ek__BackingField_42; }
	inline FsmEventTarget_t919699796 ** get_address_of_U3CEventTargetU3Ek__BackingField_42() { return &___U3CEventTargetU3Ek__BackingField_42; }
	inline void set_U3CEventTargetU3Ek__BackingField_42(FsmEventTarget_t919699796 * value)
	{
		___U3CEventTargetU3Ek__BackingField_42 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventTargetU3Ek__BackingField_42), value);
	}

	inline static int32_t get_offset_of_initialized_43() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___initialized_43)); }
	inline bool get_initialized_43() const { return ___initialized_43; }
	inline bool* get_address_of_initialized_43() { return &___initialized_43; }
	inline void set_initialized_43(bool value)
	{
		___initialized_43 = value;
	}

	inline static int32_t get_offset_of_U3CFinishedU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CFinishedU3Ek__BackingField_44)); }
	inline bool get_U3CFinishedU3Ek__BackingField_44() const { return ___U3CFinishedU3Ek__BackingField_44; }
	inline bool* get_address_of_U3CFinishedU3Ek__BackingField_44() { return &___U3CFinishedU3Ek__BackingField_44; }
	inline void set_U3CFinishedU3Ek__BackingField_44(bool value)
	{
		___U3CFinishedU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_activeStateName_45() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___activeStateName_45)); }
	inline String_t* get_activeStateName_45() const { return ___activeStateName_45; }
	inline String_t** get_address_of_activeStateName_45() { return &___activeStateName_45; }
	inline void set_activeStateName_45(String_t* value)
	{
		___activeStateName_45 = value;
		Il2CppCodeGenWriteBarrier((&___activeStateName_45), value);
	}

	inline static int32_t get_offset_of_activeState_46() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___activeState_46)); }
	inline FsmState_t4124398234 * get_activeState_46() const { return ___activeState_46; }
	inline FsmState_t4124398234 ** get_address_of_activeState_46() { return &___activeState_46; }
	inline void set_activeState_46(FsmState_t4124398234 * value)
	{
		___activeState_46 = value;
		Il2CppCodeGenWriteBarrier((&___activeState_46), value);
	}

	inline static int32_t get_offset_of_switchToState_47() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___switchToState_47)); }
	inline FsmState_t4124398234 * get_switchToState_47() const { return ___switchToState_47; }
	inline FsmState_t4124398234 ** get_address_of_switchToState_47() { return &___switchToState_47; }
	inline void set_switchToState_47(FsmState_t4124398234 * value)
	{
		___switchToState_47 = value;
		Il2CppCodeGenWriteBarrier((&___switchToState_47), value);
	}

	inline static int32_t get_offset_of_previousActiveState_48() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___previousActiveState_48)); }
	inline FsmState_t4124398234 * get_previousActiveState_48() const { return ___previousActiveState_48; }
	inline FsmState_t4124398234 ** get_address_of_previousActiveState_48() { return &___previousActiveState_48; }
	inline void set_previousActiveState_48(FsmState_t4124398234 * value)
	{
		___previousActiveState_48 = value;
		Il2CppCodeGenWriteBarrier((&___previousActiveState_48), value);
	}

	inline static int32_t get_offset_of_U3CLastTransitionU3Ek__BackingField_49() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CLastTransitionU3Ek__BackingField_49)); }
	inline FsmTransition_t1340699069 * get_U3CLastTransitionU3Ek__BackingField_49() const { return ___U3CLastTransitionU3Ek__BackingField_49; }
	inline FsmTransition_t1340699069 ** get_address_of_U3CLastTransitionU3Ek__BackingField_49() { return &___U3CLastTransitionU3Ek__BackingField_49; }
	inline void set_U3CLastTransitionU3Ek__BackingField_49(FsmTransition_t1340699069 * value)
	{
		___U3CLastTransitionU3Ek__BackingField_49 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastTransitionU3Ek__BackingField_49), value);
	}

	inline static int32_t get_offset_of_StateChanged_50() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___StateChanged_50)); }
	inline Action_1_t1898533 * get_StateChanged_50() const { return ___StateChanged_50; }
	inline Action_1_t1898533 ** get_address_of_StateChanged_50() { return &___StateChanged_50; }
	inline void set_StateChanged_50(Action_1_t1898533 * value)
	{
		___StateChanged_50 = value;
		Il2CppCodeGenWriteBarrier((&___StateChanged_50), value);
	}

	inline static int32_t get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51)); }
	inline bool get_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51() const { return ___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51; }
	inline bool* get_address_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51() { return &___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51; }
	inline void set_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51(bool value)
	{
		___U3CIsModifiedPrefabInstanceU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_editState_53() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___editState_53)); }
	inline FsmState_t4124398234 * get_editState_53() const { return ___editState_53; }
	inline FsmState_t4124398234 ** get_address_of_editState_53() { return &___editState_53; }
	inline void set_editState_53(FsmState_t4124398234 * value)
	{
		___editState_53 = value;
		Il2CppCodeGenWriteBarrier((&___editState_53), value);
	}

	inline static int32_t get_offset_of_U3CSwitchedStateU3Ek__BackingField_64() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CSwitchedStateU3Ek__BackingField_64)); }
	inline bool get_U3CSwitchedStateU3Ek__BackingField_64() const { return ___U3CSwitchedStateU3Ek__BackingField_64; }
	inline bool* get_address_of_U3CSwitchedStateU3Ek__BackingField_64() { return &___U3CSwitchedStateU3Ek__BackingField_64; }
	inline void set_U3CSwitchedStateU3Ek__BackingField_64(bool value)
	{
		___U3CSwitchedStateU3Ek__BackingField_64 = value;
	}

	inline static int32_t get_offset_of_mouseEvents_65() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___mouseEvents_65)); }
	inline bool get_mouseEvents_65() const { return ___mouseEvents_65; }
	inline bool* get_address_of_mouseEvents_65() { return &___mouseEvents_65; }
	inline void set_mouseEvents_65(bool value)
	{
		___mouseEvents_65 = value;
	}

	inline static int32_t get_offset_of_handleLevelLoaded_66() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleLevelLoaded_66)); }
	inline bool get_handleLevelLoaded_66() const { return ___handleLevelLoaded_66; }
	inline bool* get_address_of_handleLevelLoaded_66() { return &___handleLevelLoaded_66; }
	inline void set_handleLevelLoaded_66(bool value)
	{
		___handleLevelLoaded_66 = value;
	}

	inline static int32_t get_offset_of_handleTriggerEnter2D_67() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerEnter2D_67)); }
	inline bool get_handleTriggerEnter2D_67() const { return ___handleTriggerEnter2D_67; }
	inline bool* get_address_of_handleTriggerEnter2D_67() { return &___handleTriggerEnter2D_67; }
	inline void set_handleTriggerEnter2D_67(bool value)
	{
		___handleTriggerEnter2D_67 = value;
	}

	inline static int32_t get_offset_of_handleTriggerExit2D_68() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerExit2D_68)); }
	inline bool get_handleTriggerExit2D_68() const { return ___handleTriggerExit2D_68; }
	inline bool* get_address_of_handleTriggerExit2D_68() { return &___handleTriggerExit2D_68; }
	inline void set_handleTriggerExit2D_68(bool value)
	{
		___handleTriggerExit2D_68 = value;
	}

	inline static int32_t get_offset_of_handleTriggerStay2D_69() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerStay2D_69)); }
	inline bool get_handleTriggerStay2D_69() const { return ___handleTriggerStay2D_69; }
	inline bool* get_address_of_handleTriggerStay2D_69() { return &___handleTriggerStay2D_69; }
	inline void set_handleTriggerStay2D_69(bool value)
	{
		___handleTriggerStay2D_69 = value;
	}

	inline static int32_t get_offset_of_handleCollisionEnter2D_70() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionEnter2D_70)); }
	inline bool get_handleCollisionEnter2D_70() const { return ___handleCollisionEnter2D_70; }
	inline bool* get_address_of_handleCollisionEnter2D_70() { return &___handleCollisionEnter2D_70; }
	inline void set_handleCollisionEnter2D_70(bool value)
	{
		___handleCollisionEnter2D_70 = value;
	}

	inline static int32_t get_offset_of_handleCollisionExit2D_71() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionExit2D_71)); }
	inline bool get_handleCollisionExit2D_71() const { return ___handleCollisionExit2D_71; }
	inline bool* get_address_of_handleCollisionExit2D_71() { return &___handleCollisionExit2D_71; }
	inline void set_handleCollisionExit2D_71(bool value)
	{
		___handleCollisionExit2D_71 = value;
	}

	inline static int32_t get_offset_of_handleCollisionStay2D_72() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionStay2D_72)); }
	inline bool get_handleCollisionStay2D_72() const { return ___handleCollisionStay2D_72; }
	inline bool* get_address_of_handleCollisionStay2D_72() { return &___handleCollisionStay2D_72; }
	inline void set_handleCollisionStay2D_72(bool value)
	{
		___handleCollisionStay2D_72 = value;
	}

	inline static int32_t get_offset_of_handleTriggerEnter_73() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerEnter_73)); }
	inline bool get_handleTriggerEnter_73() const { return ___handleTriggerEnter_73; }
	inline bool* get_address_of_handleTriggerEnter_73() { return &___handleTriggerEnter_73; }
	inline void set_handleTriggerEnter_73(bool value)
	{
		___handleTriggerEnter_73 = value;
	}

	inline static int32_t get_offset_of_handleTriggerExit_74() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerExit_74)); }
	inline bool get_handleTriggerExit_74() const { return ___handleTriggerExit_74; }
	inline bool* get_address_of_handleTriggerExit_74() { return &___handleTriggerExit_74; }
	inline void set_handleTriggerExit_74(bool value)
	{
		___handleTriggerExit_74 = value;
	}

	inline static int32_t get_offset_of_handleTriggerStay_75() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleTriggerStay_75)); }
	inline bool get_handleTriggerStay_75() const { return ___handleTriggerStay_75; }
	inline bool* get_address_of_handleTriggerStay_75() { return &___handleTriggerStay_75; }
	inline void set_handleTriggerStay_75(bool value)
	{
		___handleTriggerStay_75 = value;
	}

	inline static int32_t get_offset_of_handleCollisionEnter_76() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionEnter_76)); }
	inline bool get_handleCollisionEnter_76() const { return ___handleCollisionEnter_76; }
	inline bool* get_address_of_handleCollisionEnter_76() { return &___handleCollisionEnter_76; }
	inline void set_handleCollisionEnter_76(bool value)
	{
		___handleCollisionEnter_76 = value;
	}

	inline static int32_t get_offset_of_handleCollisionExit_77() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionExit_77)); }
	inline bool get_handleCollisionExit_77() const { return ___handleCollisionExit_77; }
	inline bool* get_address_of_handleCollisionExit_77() { return &___handleCollisionExit_77; }
	inline void set_handleCollisionExit_77(bool value)
	{
		___handleCollisionExit_77 = value;
	}

	inline static int32_t get_offset_of_handleCollisionStay_78() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleCollisionStay_78)); }
	inline bool get_handleCollisionStay_78() const { return ___handleCollisionStay_78; }
	inline bool* get_address_of_handleCollisionStay_78() { return &___handleCollisionStay_78; }
	inline void set_handleCollisionStay_78(bool value)
	{
		___handleCollisionStay_78 = value;
	}

	inline static int32_t get_offset_of_handleParticleCollision_79() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleParticleCollision_79)); }
	inline bool get_handleParticleCollision_79() const { return ___handleParticleCollision_79; }
	inline bool* get_address_of_handleParticleCollision_79() { return &___handleParticleCollision_79; }
	inline void set_handleParticleCollision_79(bool value)
	{
		___handleParticleCollision_79 = value;
	}

	inline static int32_t get_offset_of_handleControllerColliderHit_80() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleControllerColliderHit_80)); }
	inline bool get_handleControllerColliderHit_80() const { return ___handleControllerColliderHit_80; }
	inline bool* get_address_of_handleControllerColliderHit_80() { return &___handleControllerColliderHit_80; }
	inline void set_handleControllerColliderHit_80(bool value)
	{
		___handleControllerColliderHit_80 = value;
	}

	inline static int32_t get_offset_of_handleJointBreak_81() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleJointBreak_81)); }
	inline bool get_handleJointBreak_81() const { return ___handleJointBreak_81; }
	inline bool* get_address_of_handleJointBreak_81() { return &___handleJointBreak_81; }
	inline void set_handleJointBreak_81(bool value)
	{
		___handleJointBreak_81 = value;
	}

	inline static int32_t get_offset_of_handleJointBreak2D_82() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleJointBreak2D_82)); }
	inline bool get_handleJointBreak2D_82() const { return ___handleJointBreak2D_82; }
	inline bool* get_address_of_handleJointBreak2D_82() { return &___handleJointBreak2D_82; }
	inline void set_handleJointBreak2D_82(bool value)
	{
		___handleJointBreak2D_82 = value;
	}

	inline static int32_t get_offset_of_handleOnGUI_83() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleOnGUI_83)); }
	inline bool get_handleOnGUI_83() const { return ___handleOnGUI_83; }
	inline bool* get_address_of_handleOnGUI_83() { return &___handleOnGUI_83; }
	inline void set_handleOnGUI_83(bool value)
	{
		___handleOnGUI_83 = value;
	}

	inline static int32_t get_offset_of_handleFixedUpdate_84() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleFixedUpdate_84)); }
	inline bool get_handleFixedUpdate_84() const { return ___handleFixedUpdate_84; }
	inline bool* get_address_of_handleFixedUpdate_84() { return &___handleFixedUpdate_84; }
	inline void set_handleFixedUpdate_84(bool value)
	{
		___handleFixedUpdate_84 = value;
	}

	inline static int32_t get_offset_of_handleLateUpdate_85() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleLateUpdate_85)); }
	inline bool get_handleLateUpdate_85() const { return ___handleLateUpdate_85; }
	inline bool* get_address_of_handleLateUpdate_85() { return &___handleLateUpdate_85; }
	inline void set_handleLateUpdate_85(bool value)
	{
		___handleLateUpdate_85 = value;
	}

	inline static int32_t get_offset_of_handleApplicationEvents_86() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleApplicationEvents_86)); }
	inline bool get_handleApplicationEvents_86() const { return ___handleApplicationEvents_86; }
	inline bool* get_address_of_handleApplicationEvents_86() { return &___handleApplicationEvents_86; }
	inline void set_handleApplicationEvents_86(bool value)
	{
		___handleApplicationEvents_86 = value;
	}

	inline static int32_t get_offset_of_handleUiEvents_87() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleUiEvents_87)); }
	inline int32_t get_handleUiEvents_87() const { return ___handleUiEvents_87; }
	inline int32_t* get_address_of_handleUiEvents_87() { return &___handleUiEvents_87; }
	inline void set_handleUiEvents_87(int32_t value)
	{
		___handleUiEvents_87 = value;
	}

	inline static int32_t get_offset_of_handleLegacyNetworking_88() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleLegacyNetworking_88)); }
	inline bool get_handleLegacyNetworking_88() const { return ___handleLegacyNetworking_88; }
	inline bool* get_address_of_handleLegacyNetworking_88() { return &___handleLegacyNetworking_88; }
	inline void set_handleLegacyNetworking_88(bool value)
	{
		___handleLegacyNetworking_88 = value;
	}

	inline static int32_t get_offset_of_U3CCollisionInfoU3Ek__BackingField_89() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollisionInfoU3Ek__BackingField_89)); }
	inline Collision_t4262080450 * get_U3CCollisionInfoU3Ek__BackingField_89() const { return ___U3CCollisionInfoU3Ek__BackingField_89; }
	inline Collision_t4262080450 ** get_address_of_U3CCollisionInfoU3Ek__BackingField_89() { return &___U3CCollisionInfoU3Ek__BackingField_89; }
	inline void set_U3CCollisionInfoU3Ek__BackingField_89(Collision_t4262080450 * value)
	{
		___U3CCollisionInfoU3Ek__BackingField_89 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionInfoU3Ek__BackingField_89), value);
	}

	inline static int32_t get_offset_of_U3CTriggerColliderU3Ek__BackingField_90() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTriggerColliderU3Ek__BackingField_90)); }
	inline Collider_t1773347010 * get_U3CTriggerColliderU3Ek__BackingField_90() const { return ___U3CTriggerColliderU3Ek__BackingField_90; }
	inline Collider_t1773347010 ** get_address_of_U3CTriggerColliderU3Ek__BackingField_90() { return &___U3CTriggerColliderU3Ek__BackingField_90; }
	inline void set_U3CTriggerColliderU3Ek__BackingField_90(Collider_t1773347010 * value)
	{
		___U3CTriggerColliderU3Ek__BackingField_90 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerColliderU3Ek__BackingField_90), value);
	}

	inline static int32_t get_offset_of_U3CCollision2DInfoU3Ek__BackingField_91() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollision2DInfoU3Ek__BackingField_91)); }
	inline Collision2D_t2842956331 * get_U3CCollision2DInfoU3Ek__BackingField_91() const { return ___U3CCollision2DInfoU3Ek__BackingField_91; }
	inline Collision2D_t2842956331 ** get_address_of_U3CCollision2DInfoU3Ek__BackingField_91() { return &___U3CCollision2DInfoU3Ek__BackingField_91; }
	inline void set_U3CCollision2DInfoU3Ek__BackingField_91(Collision2D_t2842956331 * value)
	{
		___U3CCollision2DInfoU3Ek__BackingField_91 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollision2DInfoU3Ek__BackingField_91), value);
	}

	inline static int32_t get_offset_of_U3CTriggerCollider2DU3Ek__BackingField_92() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTriggerCollider2DU3Ek__BackingField_92)); }
	inline Collider2D_t2806799626 * get_U3CTriggerCollider2DU3Ek__BackingField_92() const { return ___U3CTriggerCollider2DU3Ek__BackingField_92; }
	inline Collider2D_t2806799626 ** get_address_of_U3CTriggerCollider2DU3Ek__BackingField_92() { return &___U3CTriggerCollider2DU3Ek__BackingField_92; }
	inline void set_U3CTriggerCollider2DU3Ek__BackingField_92(Collider2D_t2806799626 * value)
	{
		___U3CTriggerCollider2DU3Ek__BackingField_92 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerCollider2DU3Ek__BackingField_92), value);
	}

	inline static int32_t get_offset_of_U3CJointBreakForceU3Ek__BackingField_93() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CJointBreakForceU3Ek__BackingField_93)); }
	inline float get_U3CJointBreakForceU3Ek__BackingField_93() const { return ___U3CJointBreakForceU3Ek__BackingField_93; }
	inline float* get_address_of_U3CJointBreakForceU3Ek__BackingField_93() { return &___U3CJointBreakForceU3Ek__BackingField_93; }
	inline void set_U3CJointBreakForceU3Ek__BackingField_93(float value)
	{
		___U3CJointBreakForceU3Ek__BackingField_93 = value;
	}

	inline static int32_t get_offset_of_U3CBrokenJoint2DU3Ek__BackingField_94() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CBrokenJoint2DU3Ek__BackingField_94)); }
	inline Joint2D_t4180440564 * get_U3CBrokenJoint2DU3Ek__BackingField_94() const { return ___U3CBrokenJoint2DU3Ek__BackingField_94; }
	inline Joint2D_t4180440564 ** get_address_of_U3CBrokenJoint2DU3Ek__BackingField_94() { return &___U3CBrokenJoint2DU3Ek__BackingField_94; }
	inline void set_U3CBrokenJoint2DU3Ek__BackingField_94(Joint2D_t4180440564 * value)
	{
		___U3CBrokenJoint2DU3Ek__BackingField_94 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBrokenJoint2DU3Ek__BackingField_94), value);
	}

	inline static int32_t get_offset_of_U3CParticleCollisionGOU3Ek__BackingField_95() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CParticleCollisionGOU3Ek__BackingField_95)); }
	inline GameObject_t1113636619 * get_U3CParticleCollisionGOU3Ek__BackingField_95() const { return ___U3CParticleCollisionGOU3Ek__BackingField_95; }
	inline GameObject_t1113636619 ** get_address_of_U3CParticleCollisionGOU3Ek__BackingField_95() { return &___U3CParticleCollisionGOU3Ek__BackingField_95; }
	inline void set_U3CParticleCollisionGOU3Ek__BackingField_95(GameObject_t1113636619 * value)
	{
		___U3CParticleCollisionGOU3Ek__BackingField_95 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParticleCollisionGOU3Ek__BackingField_95), value);
	}

	inline static int32_t get_offset_of_U3CTriggerNameU3Ek__BackingField_96() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTriggerNameU3Ek__BackingField_96)); }
	inline String_t* get_U3CTriggerNameU3Ek__BackingField_96() const { return ___U3CTriggerNameU3Ek__BackingField_96; }
	inline String_t** get_address_of_U3CTriggerNameU3Ek__BackingField_96() { return &___U3CTriggerNameU3Ek__BackingField_96; }
	inline void set_U3CTriggerNameU3Ek__BackingField_96(String_t* value)
	{
		___U3CTriggerNameU3Ek__BackingField_96 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTriggerNameU3Ek__BackingField_96), value);
	}

	inline static int32_t get_offset_of_U3CCollisionNameU3Ek__BackingField_97() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollisionNameU3Ek__BackingField_97)); }
	inline String_t* get_U3CCollisionNameU3Ek__BackingField_97() const { return ___U3CCollisionNameU3Ek__BackingField_97; }
	inline String_t** get_address_of_U3CCollisionNameU3Ek__BackingField_97() { return &___U3CCollisionNameU3Ek__BackingField_97; }
	inline void set_U3CCollisionNameU3Ek__BackingField_97(String_t* value)
	{
		___U3CCollisionNameU3Ek__BackingField_97 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollisionNameU3Ek__BackingField_97), value);
	}

	inline static int32_t get_offset_of_U3CTrigger2dNameU3Ek__BackingField_98() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CTrigger2dNameU3Ek__BackingField_98)); }
	inline String_t* get_U3CTrigger2dNameU3Ek__BackingField_98() const { return ___U3CTrigger2dNameU3Ek__BackingField_98; }
	inline String_t** get_address_of_U3CTrigger2dNameU3Ek__BackingField_98() { return &___U3CTrigger2dNameU3Ek__BackingField_98; }
	inline void set_U3CTrigger2dNameU3Ek__BackingField_98(String_t* value)
	{
		___U3CTrigger2dNameU3Ek__BackingField_98 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrigger2dNameU3Ek__BackingField_98), value);
	}

	inline static int32_t get_offset_of_U3CCollision2dNameU3Ek__BackingField_99() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CCollision2dNameU3Ek__BackingField_99)); }
	inline String_t* get_U3CCollision2dNameU3Ek__BackingField_99() const { return ___U3CCollision2dNameU3Ek__BackingField_99; }
	inline String_t** get_address_of_U3CCollision2dNameU3Ek__BackingField_99() { return &___U3CCollision2dNameU3Ek__BackingField_99; }
	inline void set_U3CCollision2dNameU3Ek__BackingField_99(String_t* value)
	{
		___U3CCollision2dNameU3Ek__BackingField_99 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollision2dNameU3Ek__BackingField_99), value);
	}

	inline static int32_t get_offset_of_U3CControllerColliderU3Ek__BackingField_100() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CControllerColliderU3Ek__BackingField_100)); }
	inline ControllerColliderHit_t240592346 * get_U3CControllerColliderU3Ek__BackingField_100() const { return ___U3CControllerColliderU3Ek__BackingField_100; }
	inline ControllerColliderHit_t240592346 ** get_address_of_U3CControllerColliderU3Ek__BackingField_100() { return &___U3CControllerColliderU3Ek__BackingField_100; }
	inline void set_U3CControllerColliderU3Ek__BackingField_100(ControllerColliderHit_t240592346 * value)
	{
		___U3CControllerColliderU3Ek__BackingField_100 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerColliderU3Ek__BackingField_100), value);
	}

	inline static int32_t get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_101() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___U3CRaycastHitInfoU3Ek__BackingField_101)); }
	inline RaycastHit_t1056001966  get_U3CRaycastHitInfoU3Ek__BackingField_101() const { return ___U3CRaycastHitInfoU3Ek__BackingField_101; }
	inline RaycastHit_t1056001966 * get_address_of_U3CRaycastHitInfoU3Ek__BackingField_101() { return &___U3CRaycastHitInfoU3Ek__BackingField_101; }
	inline void set_U3CRaycastHitInfoU3Ek__BackingField_101(RaycastHit_t1056001966  value)
	{
		___U3CRaycastHitInfoU3Ek__BackingField_101 = value;
	}

	inline static int32_t get_offset_of_handleAnimatorMove_103() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleAnimatorMove_103)); }
	inline bool get_handleAnimatorMove_103() const { return ___handleAnimatorMove_103; }
	inline bool* get_address_of_handleAnimatorMove_103() { return &___handleAnimatorMove_103; }
	inline void set_handleAnimatorMove_103(bool value)
	{
		___handleAnimatorMove_103 = value;
	}

	inline static int32_t get_offset_of_handleAnimatorIK_104() { return static_cast<int32_t>(offsetof(Fsm_t4127147824, ___handleAnimatorIK_104)); }
	inline bool get_handleAnimatorIK_104() const { return ___handleAnimatorIK_104; }
	inline bool* get_address_of_handleAnimatorIK_104() { return &___handleAnimatorIK_104; }
	inline void set_handleAnimatorIK_104(bool value)
	{
		___handleAnimatorIK_104 = value;
	}
};

struct Fsm_t4127147824_StaticFields
{
public:
	// HutongGames.PlayMaker.FsmEventData HutongGames.PlayMaker.Fsm::EventData
	FsmEventData_t1692568573 * ___EventData_4;
	// UnityEngine.Color HutongGames.PlayMaker.Fsm::debugLookAtColor
	Color_t2555686324  ___debugLookAtColor_5;
	// UnityEngine.Color HutongGames.PlayMaker.Fsm::debugRaycastColor
	Color_t2555686324  ___debugRaycastColor_6;
	// UnityEngine.Color[] HutongGames.PlayMaker.Fsm::StateColors
	ColorU5BU5D_t941916413* ___StateColors_52;
	// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::<LastClickedObject>k__BackingField
	GameObject_t1113636619 * ___U3CLastClickedObjectU3Ek__BackingField_54;
	// System.Boolean HutongGames.PlayMaker.Fsm::<BreakpointsEnabled>k__BackingField
	bool ___U3CBreakpointsEnabledU3Ek__BackingField_55;
	// System.Boolean HutongGames.PlayMaker.Fsm::<HitBreakpoint>k__BackingField
	bool ___U3CHitBreakpointU3Ek__BackingField_56;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::<BreakAtFsm>k__BackingField
	Fsm_t4127147824 * ___U3CBreakAtFsmU3Ek__BackingField_57;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::<BreakAtState>k__BackingField
	FsmState_t4124398234 * ___U3CBreakAtStateU3Ek__BackingField_58;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsBreak>k__BackingField
	bool ___U3CIsBreakU3Ek__BackingField_59;
	// System.Boolean HutongGames.PlayMaker.Fsm::<IsErrorBreak>k__BackingField
	bool ___U3CIsErrorBreakU3Ek__BackingField_60;
	// System.String HutongGames.PlayMaker.Fsm::<LastError>k__BackingField
	String_t* ___U3CLastErrorU3Ek__BackingField_61;
	// System.Boolean HutongGames.PlayMaker.Fsm::<StepToStateChange>k__BackingField
	bool ___U3CStepToStateChangeU3Ek__BackingField_62;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::<StepFsm>k__BackingField
	Fsm_t4127147824 * ___U3CStepFsmU3Ek__BackingField_63;
	// System.Collections.Generic.Dictionary`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D> HutongGames.PlayMaker.Fsm::lastRaycastHit2DInfoLUT
	Dictionary_2_t4026181381 * ___lastRaycastHit2DInfoLUT_102;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::targetSelf
	FsmEventTarget_t919699796 * ___targetSelf_105;

public:
	inline static int32_t get_offset_of_EventData_4() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___EventData_4)); }
	inline FsmEventData_t1692568573 * get_EventData_4() const { return ___EventData_4; }
	inline FsmEventData_t1692568573 ** get_address_of_EventData_4() { return &___EventData_4; }
	inline void set_EventData_4(FsmEventData_t1692568573 * value)
	{
		___EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___EventData_4), value);
	}

	inline static int32_t get_offset_of_debugLookAtColor_5() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___debugLookAtColor_5)); }
	inline Color_t2555686324  get_debugLookAtColor_5() const { return ___debugLookAtColor_5; }
	inline Color_t2555686324 * get_address_of_debugLookAtColor_5() { return &___debugLookAtColor_5; }
	inline void set_debugLookAtColor_5(Color_t2555686324  value)
	{
		___debugLookAtColor_5 = value;
	}

	inline static int32_t get_offset_of_debugRaycastColor_6() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___debugRaycastColor_6)); }
	inline Color_t2555686324  get_debugRaycastColor_6() const { return ___debugRaycastColor_6; }
	inline Color_t2555686324 * get_address_of_debugRaycastColor_6() { return &___debugRaycastColor_6; }
	inline void set_debugRaycastColor_6(Color_t2555686324  value)
	{
		___debugRaycastColor_6 = value;
	}

	inline static int32_t get_offset_of_StateColors_52() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___StateColors_52)); }
	inline ColorU5BU5D_t941916413* get_StateColors_52() const { return ___StateColors_52; }
	inline ColorU5BU5D_t941916413** get_address_of_StateColors_52() { return &___StateColors_52; }
	inline void set_StateColors_52(ColorU5BU5D_t941916413* value)
	{
		___StateColors_52 = value;
		Il2CppCodeGenWriteBarrier((&___StateColors_52), value);
	}

	inline static int32_t get_offset_of_U3CLastClickedObjectU3Ek__BackingField_54() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CLastClickedObjectU3Ek__BackingField_54)); }
	inline GameObject_t1113636619 * get_U3CLastClickedObjectU3Ek__BackingField_54() const { return ___U3CLastClickedObjectU3Ek__BackingField_54; }
	inline GameObject_t1113636619 ** get_address_of_U3CLastClickedObjectU3Ek__BackingField_54() { return &___U3CLastClickedObjectU3Ek__BackingField_54; }
	inline void set_U3CLastClickedObjectU3Ek__BackingField_54(GameObject_t1113636619 * value)
	{
		___U3CLastClickedObjectU3Ek__BackingField_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastClickedObjectU3Ek__BackingField_54), value);
	}

	inline static int32_t get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_55() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CBreakpointsEnabledU3Ek__BackingField_55)); }
	inline bool get_U3CBreakpointsEnabledU3Ek__BackingField_55() const { return ___U3CBreakpointsEnabledU3Ek__BackingField_55; }
	inline bool* get_address_of_U3CBreakpointsEnabledU3Ek__BackingField_55() { return &___U3CBreakpointsEnabledU3Ek__BackingField_55; }
	inline void set_U3CBreakpointsEnabledU3Ek__BackingField_55(bool value)
	{
		___U3CBreakpointsEnabledU3Ek__BackingField_55 = value;
	}

	inline static int32_t get_offset_of_U3CHitBreakpointU3Ek__BackingField_56() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CHitBreakpointU3Ek__BackingField_56)); }
	inline bool get_U3CHitBreakpointU3Ek__BackingField_56() const { return ___U3CHitBreakpointU3Ek__BackingField_56; }
	inline bool* get_address_of_U3CHitBreakpointU3Ek__BackingField_56() { return &___U3CHitBreakpointU3Ek__BackingField_56; }
	inline void set_U3CHitBreakpointU3Ek__BackingField_56(bool value)
	{
		___U3CHitBreakpointU3Ek__BackingField_56 = value;
	}

	inline static int32_t get_offset_of_U3CBreakAtFsmU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CBreakAtFsmU3Ek__BackingField_57)); }
	inline Fsm_t4127147824 * get_U3CBreakAtFsmU3Ek__BackingField_57() const { return ___U3CBreakAtFsmU3Ek__BackingField_57; }
	inline Fsm_t4127147824 ** get_address_of_U3CBreakAtFsmU3Ek__BackingField_57() { return &___U3CBreakAtFsmU3Ek__BackingField_57; }
	inline void set_U3CBreakAtFsmU3Ek__BackingField_57(Fsm_t4127147824 * value)
	{
		___U3CBreakAtFsmU3Ek__BackingField_57 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBreakAtFsmU3Ek__BackingField_57), value);
	}

	inline static int32_t get_offset_of_U3CBreakAtStateU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CBreakAtStateU3Ek__BackingField_58)); }
	inline FsmState_t4124398234 * get_U3CBreakAtStateU3Ek__BackingField_58() const { return ___U3CBreakAtStateU3Ek__BackingField_58; }
	inline FsmState_t4124398234 ** get_address_of_U3CBreakAtStateU3Ek__BackingField_58() { return &___U3CBreakAtStateU3Ek__BackingField_58; }
	inline void set_U3CBreakAtStateU3Ek__BackingField_58(FsmState_t4124398234 * value)
	{
		___U3CBreakAtStateU3Ek__BackingField_58 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBreakAtStateU3Ek__BackingField_58), value);
	}

	inline static int32_t get_offset_of_U3CIsBreakU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CIsBreakU3Ek__BackingField_59)); }
	inline bool get_U3CIsBreakU3Ek__BackingField_59() const { return ___U3CIsBreakU3Ek__BackingField_59; }
	inline bool* get_address_of_U3CIsBreakU3Ek__BackingField_59() { return &___U3CIsBreakU3Ek__BackingField_59; }
	inline void set_U3CIsBreakU3Ek__BackingField_59(bool value)
	{
		___U3CIsBreakU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of_U3CIsErrorBreakU3Ek__BackingField_60() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CIsErrorBreakU3Ek__BackingField_60)); }
	inline bool get_U3CIsErrorBreakU3Ek__BackingField_60() const { return ___U3CIsErrorBreakU3Ek__BackingField_60; }
	inline bool* get_address_of_U3CIsErrorBreakU3Ek__BackingField_60() { return &___U3CIsErrorBreakU3Ek__BackingField_60; }
	inline void set_U3CIsErrorBreakU3Ek__BackingField_60(bool value)
	{
		___U3CIsErrorBreakU3Ek__BackingField_60 = value;
	}

	inline static int32_t get_offset_of_U3CLastErrorU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CLastErrorU3Ek__BackingField_61)); }
	inline String_t* get_U3CLastErrorU3Ek__BackingField_61() const { return ___U3CLastErrorU3Ek__BackingField_61; }
	inline String_t** get_address_of_U3CLastErrorU3Ek__BackingField_61() { return &___U3CLastErrorU3Ek__BackingField_61; }
	inline void set_U3CLastErrorU3Ek__BackingField_61(String_t* value)
	{
		___U3CLastErrorU3Ek__BackingField_61 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastErrorU3Ek__BackingField_61), value);
	}

	inline static int32_t get_offset_of_U3CStepToStateChangeU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CStepToStateChangeU3Ek__BackingField_62)); }
	inline bool get_U3CStepToStateChangeU3Ek__BackingField_62() const { return ___U3CStepToStateChangeU3Ek__BackingField_62; }
	inline bool* get_address_of_U3CStepToStateChangeU3Ek__BackingField_62() { return &___U3CStepToStateChangeU3Ek__BackingField_62; }
	inline void set_U3CStepToStateChangeU3Ek__BackingField_62(bool value)
	{
		___U3CStepToStateChangeU3Ek__BackingField_62 = value;
	}

	inline static int32_t get_offset_of_U3CStepFsmU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___U3CStepFsmU3Ek__BackingField_63)); }
	inline Fsm_t4127147824 * get_U3CStepFsmU3Ek__BackingField_63() const { return ___U3CStepFsmU3Ek__BackingField_63; }
	inline Fsm_t4127147824 ** get_address_of_U3CStepFsmU3Ek__BackingField_63() { return &___U3CStepFsmU3Ek__BackingField_63; }
	inline void set_U3CStepFsmU3Ek__BackingField_63(Fsm_t4127147824 * value)
	{
		___U3CStepFsmU3Ek__BackingField_63 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStepFsmU3Ek__BackingField_63), value);
	}

	inline static int32_t get_offset_of_lastRaycastHit2DInfoLUT_102() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___lastRaycastHit2DInfoLUT_102)); }
	inline Dictionary_2_t4026181381 * get_lastRaycastHit2DInfoLUT_102() const { return ___lastRaycastHit2DInfoLUT_102; }
	inline Dictionary_2_t4026181381 ** get_address_of_lastRaycastHit2DInfoLUT_102() { return &___lastRaycastHit2DInfoLUT_102; }
	inline void set_lastRaycastHit2DInfoLUT_102(Dictionary_2_t4026181381 * value)
	{
		___lastRaycastHit2DInfoLUT_102 = value;
		Il2CppCodeGenWriteBarrier((&___lastRaycastHit2DInfoLUT_102), value);
	}

	inline static int32_t get_offset_of_targetSelf_105() { return static_cast<int32_t>(offsetof(Fsm_t4127147824_StaticFields, ___targetSelf_105)); }
	inline FsmEventTarget_t919699796 * get_targetSelf_105() const { return ___targetSelf_105; }
	inline FsmEventTarget_t919699796 ** get_address_of_targetSelf_105() { return &___targetSelf_105; }
	inline void set_targetSelf_105(FsmEventTarget_t919699796 * value)
	{
		___targetSelf_105 = value;
		Il2CppCodeGenWriteBarrier((&___targetSelf_105), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSM_T4127147824_H
#ifndef FSMLOGENTRY_T3340499788_H
#define FSMLOGENTRY_T3340499788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmLogEntry
struct  FsmLogEntry_t3340499788  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.FsmLogEntry::<Log>k__BackingField
	FsmLog_t3265252880 * ___U3CLogU3Ek__BackingField_0;
	// HutongGames.PlayMaker.FsmLogType HutongGames.PlayMaker.FsmLogEntry::<LogType>k__BackingField
	int32_t ___U3CLogTypeU3Ek__BackingField_1;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::<State>k__BackingField
	FsmState_t4124398234 * ___U3CStateU3Ek__BackingField_2;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::<SentByState>k__BackingField
	FsmState_t4124398234 * ___U3CSentByStateU3Ek__BackingField_3;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmLogEntry::<Action>k__BackingField
	FsmStateAction_t3801304101 * ___U3CActionU3Ek__BackingField_4;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmLogEntry::<Event>k__BackingField
	FsmEvent_t3736299882 * ___U3CEventU3Ek__BackingField_5;
	// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.FsmLogEntry::<Transition>k__BackingField
	FsmTransition_t1340699069 * ___U3CTransitionU3Ek__BackingField_6;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmLogEntry::<EventTarget>k__BackingField
	FsmEventTarget_t919699796 * ___U3CEventTargetU3Ek__BackingField_7;
	// System.Single HutongGames.PlayMaker.FsmLogEntry::<Time>k__BackingField
	float ___U3CTimeU3Ek__BackingField_8;
	// System.Single HutongGames.PlayMaker.FsmLogEntry::<StateTime>k__BackingField
	float ___U3CStateTimeU3Ek__BackingField_9;
	// System.Int32 HutongGames.PlayMaker.FsmLogEntry::<FrameCount>k__BackingField
	int32_t ___U3CFrameCountU3Ek__BackingField_10;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::<FsmVariablesCopy>k__BackingField
	FsmVariables_t3349589544 * ___U3CFsmVariablesCopyU3Ek__BackingField_11;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::<GlobalVariablesCopy>k__BackingField
	FsmVariables_t3349589544 * ___U3CGlobalVariablesCopyU3Ek__BackingField_12;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmLogEntry::<GameObject>k__BackingField
	GameObject_t1113636619 * ___U3CGameObjectU3Ek__BackingField_13;
	// System.String HutongGames.PlayMaker.FsmLogEntry::<GameObjectName>k__BackingField
	String_t* ___U3CGameObjectNameU3Ek__BackingField_14;
	// UnityEngine.Texture HutongGames.PlayMaker.FsmLogEntry::<GameObjectIcon>k__BackingField
	Texture_t3661962703 * ___U3CGameObjectIconU3Ek__BackingField_15;
	// System.String HutongGames.PlayMaker.FsmLogEntry::text
	String_t* ___text_16;
	// System.String HutongGames.PlayMaker.FsmLogEntry::<Text2>k__BackingField
	String_t* ___U3CText2U3Ek__BackingField_17;
	// System.String HutongGames.PlayMaker.FsmLogEntry::textWithTimecode
	String_t* ___textWithTimecode_18;

public:
	inline static int32_t get_offset_of_U3CLogU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CLogU3Ek__BackingField_0)); }
	inline FsmLog_t3265252880 * get_U3CLogU3Ek__BackingField_0() const { return ___U3CLogU3Ek__BackingField_0; }
	inline FsmLog_t3265252880 ** get_address_of_U3CLogU3Ek__BackingField_0() { return &___U3CLogU3Ek__BackingField_0; }
	inline void set_U3CLogU3Ek__BackingField_0(FsmLog_t3265252880 * value)
	{
		___U3CLogU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLogU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CLogTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CLogTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogTypeU3Ek__BackingField_1() const { return ___U3CLogTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogTypeU3Ek__BackingField_1() { return &___U3CLogTypeU3Ek__BackingField_1; }
	inline void set_U3CLogTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CStateU3Ek__BackingField_2)); }
	inline FsmState_t4124398234 * get_U3CStateU3Ek__BackingField_2() const { return ___U3CStateU3Ek__BackingField_2; }
	inline FsmState_t4124398234 ** get_address_of_U3CStateU3Ek__BackingField_2() { return &___U3CStateU3Ek__BackingField_2; }
	inline void set_U3CStateU3Ek__BackingField_2(FsmState_t4124398234 * value)
	{
		___U3CStateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CSentByStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CSentByStateU3Ek__BackingField_3)); }
	inline FsmState_t4124398234 * get_U3CSentByStateU3Ek__BackingField_3() const { return ___U3CSentByStateU3Ek__BackingField_3; }
	inline FsmState_t4124398234 ** get_address_of_U3CSentByStateU3Ek__BackingField_3() { return &___U3CSentByStateU3Ek__BackingField_3; }
	inline void set_U3CSentByStateU3Ek__BackingField_3(FsmState_t4124398234 * value)
	{
		___U3CSentByStateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSentByStateU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CActionU3Ek__BackingField_4)); }
	inline FsmStateAction_t3801304101 * get_U3CActionU3Ek__BackingField_4() const { return ___U3CActionU3Ek__BackingField_4; }
	inline FsmStateAction_t3801304101 ** get_address_of_U3CActionU3Ek__BackingField_4() { return &___U3CActionU3Ek__BackingField_4; }
	inline void set_U3CActionU3Ek__BackingField_4(FsmStateAction_t3801304101 * value)
	{
		___U3CActionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CEventU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CEventU3Ek__BackingField_5)); }
	inline FsmEvent_t3736299882 * get_U3CEventU3Ek__BackingField_5() const { return ___U3CEventU3Ek__BackingField_5; }
	inline FsmEvent_t3736299882 ** get_address_of_U3CEventU3Ek__BackingField_5() { return &___U3CEventU3Ek__BackingField_5; }
	inline void set_U3CEventU3Ek__BackingField_5(FsmEvent_t3736299882 * value)
	{
		___U3CEventU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTransitionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CTransitionU3Ek__BackingField_6)); }
	inline FsmTransition_t1340699069 * get_U3CTransitionU3Ek__BackingField_6() const { return ___U3CTransitionU3Ek__BackingField_6; }
	inline FsmTransition_t1340699069 ** get_address_of_U3CTransitionU3Ek__BackingField_6() { return &___U3CTransitionU3Ek__BackingField_6; }
	inline void set_U3CTransitionU3Ek__BackingField_6(FsmTransition_t1340699069 * value)
	{
		___U3CTransitionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTransitionU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CEventTargetU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CEventTargetU3Ek__BackingField_7)); }
	inline FsmEventTarget_t919699796 * get_U3CEventTargetU3Ek__BackingField_7() const { return ___U3CEventTargetU3Ek__BackingField_7; }
	inline FsmEventTarget_t919699796 ** get_address_of_U3CEventTargetU3Ek__BackingField_7() { return &___U3CEventTargetU3Ek__BackingField_7; }
	inline void set_U3CEventTargetU3Ek__BackingField_7(FsmEventTarget_t919699796 * value)
	{
		___U3CEventTargetU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventTargetU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CTimeU3Ek__BackingField_8)); }
	inline float get_U3CTimeU3Ek__BackingField_8() const { return ___U3CTimeU3Ek__BackingField_8; }
	inline float* get_address_of_U3CTimeU3Ek__BackingField_8() { return &___U3CTimeU3Ek__BackingField_8; }
	inline void set_U3CTimeU3Ek__BackingField_8(float value)
	{
		___U3CTimeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CStateTimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CStateTimeU3Ek__BackingField_9)); }
	inline float get_U3CStateTimeU3Ek__BackingField_9() const { return ___U3CStateTimeU3Ek__BackingField_9; }
	inline float* get_address_of_U3CStateTimeU3Ek__BackingField_9() { return &___U3CStateTimeU3Ek__BackingField_9; }
	inline void set_U3CStateTimeU3Ek__BackingField_9(float value)
	{
		___U3CStateTimeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CFrameCountU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CFrameCountU3Ek__BackingField_10)); }
	inline int32_t get_U3CFrameCountU3Ek__BackingField_10() const { return ___U3CFrameCountU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CFrameCountU3Ek__BackingField_10() { return &___U3CFrameCountU3Ek__BackingField_10; }
	inline void set_U3CFrameCountU3Ek__BackingField_10(int32_t value)
	{
		___U3CFrameCountU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CFsmVariablesCopyU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CFsmVariablesCopyU3Ek__BackingField_11)); }
	inline FsmVariables_t3349589544 * get_U3CFsmVariablesCopyU3Ek__BackingField_11() const { return ___U3CFsmVariablesCopyU3Ek__BackingField_11; }
	inline FsmVariables_t3349589544 ** get_address_of_U3CFsmVariablesCopyU3Ek__BackingField_11() { return &___U3CFsmVariablesCopyU3Ek__BackingField_11; }
	inline void set_U3CFsmVariablesCopyU3Ek__BackingField_11(FsmVariables_t3349589544 * value)
	{
		___U3CFsmVariablesCopyU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFsmVariablesCopyU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CGlobalVariablesCopyU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CGlobalVariablesCopyU3Ek__BackingField_12)); }
	inline FsmVariables_t3349589544 * get_U3CGlobalVariablesCopyU3Ek__BackingField_12() const { return ___U3CGlobalVariablesCopyU3Ek__BackingField_12; }
	inline FsmVariables_t3349589544 ** get_address_of_U3CGlobalVariablesCopyU3Ek__BackingField_12() { return &___U3CGlobalVariablesCopyU3Ek__BackingField_12; }
	inline void set_U3CGlobalVariablesCopyU3Ek__BackingField_12(FsmVariables_t3349589544 * value)
	{
		___U3CGlobalVariablesCopyU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGlobalVariablesCopyU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CGameObjectU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CGameObjectU3Ek__BackingField_13)); }
	inline GameObject_t1113636619 * get_U3CGameObjectU3Ek__BackingField_13() const { return ___U3CGameObjectU3Ek__BackingField_13; }
	inline GameObject_t1113636619 ** get_address_of_U3CGameObjectU3Ek__BackingField_13() { return &___U3CGameObjectU3Ek__BackingField_13; }
	inline void set_U3CGameObjectU3Ek__BackingField_13(GameObject_t1113636619 * value)
	{
		___U3CGameObjectU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CGameObjectNameU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CGameObjectNameU3Ek__BackingField_14)); }
	inline String_t* get_U3CGameObjectNameU3Ek__BackingField_14() const { return ___U3CGameObjectNameU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CGameObjectNameU3Ek__BackingField_14() { return &___U3CGameObjectNameU3Ek__BackingField_14; }
	inline void set_U3CGameObjectNameU3Ek__BackingField_14(String_t* value)
	{
		___U3CGameObjectNameU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectNameU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CGameObjectIconU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CGameObjectIconU3Ek__BackingField_15)); }
	inline Texture_t3661962703 * get_U3CGameObjectIconU3Ek__BackingField_15() const { return ___U3CGameObjectIconU3Ek__BackingField_15; }
	inline Texture_t3661962703 ** get_address_of_U3CGameObjectIconU3Ek__BackingField_15() { return &___U3CGameObjectIconU3Ek__BackingField_15; }
	inline void set_U3CGameObjectIconU3Ek__BackingField_15(Texture_t3661962703 * value)
	{
		___U3CGameObjectIconU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectIconU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___text_16)); }
	inline String_t* get_text_16() const { return ___text_16; }
	inline String_t** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(String_t* value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}

	inline static int32_t get_offset_of_U3CText2U3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___U3CText2U3Ek__BackingField_17)); }
	inline String_t* get_U3CText2U3Ek__BackingField_17() const { return ___U3CText2U3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CText2U3Ek__BackingField_17() { return &___U3CText2U3Ek__BackingField_17; }
	inline void set_U3CText2U3Ek__BackingField_17(String_t* value)
	{
		___U3CText2U3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CText2U3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_textWithTimecode_18() { return static_cast<int32_t>(offsetof(FsmLogEntry_t3340499788, ___textWithTimecode_18)); }
	inline String_t* get_textWithTimecode_18() const { return ___textWithTimecode_18; }
	inline String_t** get_address_of_textWithTimecode_18() { return &___textWithTimecode_18; }
	inline void set_textWithTimecode_18(String_t* value)
	{
		___textWithTimecode_18 = value;
		Il2CppCodeGenWriteBarrier((&___textWithTimecode_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMLOGENTRY_T3340499788_H
#ifndef FSMTRANSITION_T1340699069_H
#define FSMTRANSITION_T1340699069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTransition
struct  FsmTransition_t1340699069  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmTransition::fsmEvent
	FsmEvent_t3736299882 * ___fsmEvent_0;
	// System.String HutongGames.PlayMaker.FsmTransition::toState
	String_t* ___toState_1;
	// HutongGames.PlayMaker.FsmTransition/CustomLinkStyle HutongGames.PlayMaker.FsmTransition::linkStyle
	uint8_t ___linkStyle_2;
	// HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint HutongGames.PlayMaker.FsmTransition::linkConstraint
	uint8_t ___linkConstraint_3;
	// System.Byte HutongGames.PlayMaker.FsmTransition::colorIndex
	uint8_t ___colorIndex_4;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmTransition::toFsmState
	FsmState_t4124398234 * ___toFsmState_5;

public:
	inline static int32_t get_offset_of_fsmEvent_0() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___fsmEvent_0)); }
	inline FsmEvent_t3736299882 * get_fsmEvent_0() const { return ___fsmEvent_0; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmEvent_0() { return &___fsmEvent_0; }
	inline void set_fsmEvent_0(FsmEvent_t3736299882 * value)
	{
		___fsmEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsmEvent_0), value);
	}

	inline static int32_t get_offset_of_toState_1() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___toState_1)); }
	inline String_t* get_toState_1() const { return ___toState_1; }
	inline String_t** get_address_of_toState_1() { return &___toState_1; }
	inline void set_toState_1(String_t* value)
	{
		___toState_1 = value;
		Il2CppCodeGenWriteBarrier((&___toState_1), value);
	}

	inline static int32_t get_offset_of_linkStyle_2() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___linkStyle_2)); }
	inline uint8_t get_linkStyle_2() const { return ___linkStyle_2; }
	inline uint8_t* get_address_of_linkStyle_2() { return &___linkStyle_2; }
	inline void set_linkStyle_2(uint8_t value)
	{
		___linkStyle_2 = value;
	}

	inline static int32_t get_offset_of_linkConstraint_3() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___linkConstraint_3)); }
	inline uint8_t get_linkConstraint_3() const { return ___linkConstraint_3; }
	inline uint8_t* get_address_of_linkConstraint_3() { return &___linkConstraint_3; }
	inline void set_linkConstraint_3(uint8_t value)
	{
		___linkConstraint_3 = value;
	}

	inline static int32_t get_offset_of_colorIndex_4() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___colorIndex_4)); }
	inline uint8_t get_colorIndex_4() const { return ___colorIndex_4; }
	inline uint8_t* get_address_of_colorIndex_4() { return &___colorIndex_4; }
	inline void set_colorIndex_4(uint8_t value)
	{
		___colorIndex_4 = value;
	}

	inline static int32_t get_offset_of_toFsmState_5() { return static_cast<int32_t>(offsetof(FsmTransition_t1340699069, ___toFsmState_5)); }
	inline FsmState_t4124398234 * get_toFsmState_5() const { return ___toFsmState_5; }
	inline FsmState_t4124398234 ** get_address_of_toFsmState_5() { return &___toFsmState_5; }
	inline void set_toFsmState_5(FsmState_t4124398234 * value)
	{
		___toFsmState_5 = value;
		Il2CppCodeGenWriteBarrier((&___toFsmState_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMTRANSITION_T1340699069_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AXISEVENTDATA_T2331243652_H
#define AXISEVENTDATA_T2331243652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t2331243652  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_t2156229523  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t2331243652, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_t2156229523  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_t2156229523 * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_t2156229523  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t2331243652, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENTDATA_T2331243652_H
#ifndef ENTRY_T3344766165_H
#define ENTRY_T3344766165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger/Entry
struct  Entry_t3344766165  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger/Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger/TriggerEvent UnityEngine.EventSystems.EventTrigger/Entry::callback
	TriggerEvent_t3867320123 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t3344766165, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t3344766165, ___callback_1)); }
	inline TriggerEvent_t3867320123 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_t3867320123 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_t3867320123 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3344766165_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef BUTTONSTATE_T857139936_H
#define BUTTONSTATE_T857139936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct  ButtonState_t857139936  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerInputModule/ButtonState::m_Button
	int32_t ___m_Button_0;
	// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData UnityEngine.EventSystems.PointerInputModule/ButtonState::m_EventData
	MouseButtonEventData_t3190347560 * ___m_EventData_1;

public:
	inline static int32_t get_offset_of_m_Button_0() { return static_cast<int32_t>(offsetof(ButtonState_t857139936, ___m_Button_0)); }
	inline int32_t get_m_Button_0() const { return ___m_Button_0; }
	inline int32_t* get_address_of_m_Button_0() { return &___m_Button_0; }
	inline void set_m_Button_0(int32_t value)
	{
		___m_Button_0 = value;
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(ButtonState_t857139936, ___m_EventData_1)); }
	inline MouseButtonEventData_t3190347560 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline MouseButtonEventData_t3190347560 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(MouseButtonEventData_t3190347560 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATE_T857139936_H
#ifndef MOUSEBUTTONEVENTDATA_T3190347560_H
#define MOUSEBUTTONEVENTDATA_T3190347560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct  MouseButtonEventData_t3190347560  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData/FramePressState UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonState
	int32_t ___buttonState_0;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::buttonData
	PointerEventData_t3807901092 * ___buttonData_1;

public:
	inline static int32_t get_offset_of_buttonState_0() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3190347560, ___buttonState_0)); }
	inline int32_t get_buttonState_0() const { return ___buttonState_0; }
	inline int32_t* get_address_of_buttonState_0() { return &___buttonState_0; }
	inline void set_buttonState_0(int32_t value)
	{
		___buttonState_0 = value;
	}

	inline static int32_t get_offset_of_buttonData_1() { return static_cast<int32_t>(offsetof(MouseButtonEventData_t3190347560, ___buttonData_1)); }
	inline PointerEventData_t3807901092 * get_buttonData_1() const { return ___buttonData_1; }
	inline PointerEventData_t3807901092 ** get_address_of_buttonData_1() { return &___buttonData_1; }
	inline void set_buttonData_1(PointerEventData_t3807901092 * value)
	{
		___buttonData_1 = value;
		Il2CppCodeGenWriteBarrier((&___buttonData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTONEVENTDATA_T3190347560_H
#ifndef COLORTWEEN_T809614380_H
#define COLORTWEEN_T809614380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t809614380 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_t1121741130 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t2555686324  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t2555686324  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_Target_0)); }
	inline ColorTweenCallback_t1121741130 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_t1121741130 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_t1121741130 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_StartColor_1)); }
	inline Color_t2555686324  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t2555686324 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t2555686324  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_TargetColor_2)); }
	inline Color_t2555686324  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t2555686324 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t2555686324  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t809614380, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t809614380_marshaled_pinvoke
{
	ColorTweenCallback_t1121741130 * ___m_Target_0;
	Color_t2555686324  ___m_StartColor_1;
	Color_t2555686324  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t809614380_marshaled_com
{
	ColorTweenCallback_t1121741130 * ___m_Target_0;
	Color_t2555686324  ___m_StartColor_1;
	Color_t2555686324  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T809614380_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef EVENTTRIGGER_T1076084509_H
#define EVENTTRIGGER_T1076084509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_t1076084509  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t521873611 * ___m_Delegates_4;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry> UnityEngine.EventSystems.EventTrigger::delegates
	List_1_t521873611 * ___delegates_5;

public:
	inline static int32_t get_offset_of_m_Delegates_4() { return static_cast<int32_t>(offsetof(EventTrigger_t1076084509, ___m_Delegates_4)); }
	inline List_1_t521873611 * get_m_Delegates_4() const { return ___m_Delegates_4; }
	inline List_1_t521873611 ** get_address_of_m_Delegates_4() { return &___m_Delegates_4; }
	inline void set_m_Delegates_4(List_1_t521873611 * value)
	{
		___m_Delegates_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegates_4), value);
	}

	inline static int32_t get_offset_of_delegates_5() { return static_cast<int32_t>(offsetof(EventTrigger_t1076084509, ___delegates_5)); }
	inline List_1_t521873611 * get_delegates_5() const { return ___delegates_5; }
	inline List_1_t521873611 ** get_address_of_delegates_5() { return &___delegates_5; }
	inline void set_delegates_5(List_1_t521873611 * value)
	{
		___delegates_5 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T1076084509_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef BASEINPUT_T3630163547_H
#define BASEINPUT_T3630163547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInput
struct  BaseInput_t3630163547  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_T3630163547_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_4)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_4), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_5)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_5), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_6)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_6), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_7)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_7), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_8)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_8), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_9)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef BASERAYCASTER_T4150874583_H
#define BASERAYCASTER_T4150874583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t4150874583  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T4150874583_H
#ifndef EVENTSYSTEM_T1003666588_H
#define EVENTSYSTEM_T1003666588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t1003666588  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t3491343620 * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t2019268878 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1113636619 * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1113636619 * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t3903027533 * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SystemInputModules_4)); }
	inline List_1_t3491343620 * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t3491343620 ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t3491343620 * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_4), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t2019268878 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t2019268878 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t2019268878 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_5), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_FirstSelected_7)); }
	inline GameObject_t1113636619 * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_t1113636619 ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_t1113636619 * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_7), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentSelected_10)); }
	inline GameObject_t1113636619 * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_t1113636619 * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_10), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DummyData_13)); }
	inline BaseEventData_t3903027533 * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t3903027533 ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t3903027533 * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_13), value);
	}
};

struct EventSystem_t1003666588_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2475741330 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t3135238028 * ___s_RaycastComparer_14;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_15;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___m_EventSystems_6)); }
	inline List_1_t2475741330 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_t2475741330 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_t2475741330 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_6), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t3135238028 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t3135238028 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t3135238028 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T1003666588_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef PHYSICSRAYCASTER_T437419520_H
#define PHYSICSRAYCASTER_T437419520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PhysicsRaycaster
struct  PhysicsRaycaster_t437419520  : public BaseRaycaster_t4150874583
{
public:
	// UnityEngine.Camera UnityEngine.EventSystems.PhysicsRaycaster::m_EventCamera
	Camera_t4157153871 * ___m_EventCamera_5;
	// UnityEngine.LayerMask UnityEngine.EventSystems.PhysicsRaycaster::m_EventMask
	LayerMask_t3493934918  ___m_EventMask_6;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_MaxRayIntersections
	int32_t ___m_MaxRayIntersections_7;
	// System.Int32 UnityEngine.EventSystems.PhysicsRaycaster::m_LastMaxRayIntersections
	int32_t ___m_LastMaxRayIntersections_8;
	// UnityEngine.RaycastHit[] UnityEngine.EventSystems.PhysicsRaycaster::m_Hits
	RaycastHitU5BU5D_t1690781147* ___m_Hits_9;

public:
	inline static int32_t get_offset_of_m_EventCamera_5() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_EventCamera_5)); }
	inline Camera_t4157153871 * get_m_EventCamera_5() const { return ___m_EventCamera_5; }
	inline Camera_t4157153871 ** get_address_of_m_EventCamera_5() { return &___m_EventCamera_5; }
	inline void set_m_EventCamera_5(Camera_t4157153871 * value)
	{
		___m_EventCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventCamera_5), value);
	}

	inline static int32_t get_offset_of_m_EventMask_6() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_EventMask_6)); }
	inline LayerMask_t3493934918  get_m_EventMask_6() const { return ___m_EventMask_6; }
	inline LayerMask_t3493934918 * get_address_of_m_EventMask_6() { return &___m_EventMask_6; }
	inline void set_m_EventMask_6(LayerMask_t3493934918  value)
	{
		___m_EventMask_6 = value;
	}

	inline static int32_t get_offset_of_m_MaxRayIntersections_7() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_MaxRayIntersections_7)); }
	inline int32_t get_m_MaxRayIntersections_7() const { return ___m_MaxRayIntersections_7; }
	inline int32_t* get_address_of_m_MaxRayIntersections_7() { return &___m_MaxRayIntersections_7; }
	inline void set_m_MaxRayIntersections_7(int32_t value)
	{
		___m_MaxRayIntersections_7 = value;
	}

	inline static int32_t get_offset_of_m_LastMaxRayIntersections_8() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_LastMaxRayIntersections_8)); }
	inline int32_t get_m_LastMaxRayIntersections_8() const { return ___m_LastMaxRayIntersections_8; }
	inline int32_t* get_address_of_m_LastMaxRayIntersections_8() { return &___m_LastMaxRayIntersections_8; }
	inline void set_m_LastMaxRayIntersections_8(int32_t value)
	{
		___m_LastMaxRayIntersections_8 = value;
	}

	inline static int32_t get_offset_of_m_Hits_9() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520, ___m_Hits_9)); }
	inline RaycastHitU5BU5D_t1690781147* get_m_Hits_9() const { return ___m_Hits_9; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_m_Hits_9() { return &___m_Hits_9; }
	inline void set_m_Hits_9(RaycastHitU5BU5D_t1690781147* value)
	{
		___m_Hits_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hits_9), value);
	}
};

struct PhysicsRaycaster_t437419520_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.RaycastHit> UnityEngine.EventSystems.PhysicsRaycaster::<>f__am$cache0
	Comparison_1_t830933145 * ___U3CU3Ef__amU24cache0_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(PhysicsRaycaster_t437419520_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Comparison_1_t830933145 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Comparison_1_t830933145 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Comparison_1_t830933145 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSRAYCASTER_T437419520_H
#ifndef POINTERINPUTMODULE_T3453173740_H
#define POINTERINPUTMODULE_T3453173740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t3453173740  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t2696614423 * ___m_PointerData_14;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_15;

public:
	inline static int32_t get_offset_of_m_PointerData_14() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_PointerData_14)); }
	inline Dictionary_2_t2696614423 * get_m_PointerData_14() const { return ___m_PointerData_14; }
	inline Dictionary_2_t2696614423 ** get_address_of_m_PointerData_14() { return &___m_PointerData_14; }
	inline void set_m_PointerData_14(Dictionary_2_t2696614423 * value)
	{
		___m_PointerData_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_14), value);
	}

	inline static int32_t get_offset_of_m_MouseState_15() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_MouseState_15)); }
	inline MouseState_t384203932 * get_m_MouseState_15() const { return ___m_MouseState_15; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_15() { return &___m_MouseState_15; }
	inline void set_m_MouseState_15(MouseState_t384203932 * value)
	{
		___m_MouseState_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T3453173740_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_18;

public:
	inline static int32_t get_offset_of_m_OnClick_18() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_18)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_18() const { return ___m_OnClick_18; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_18() { return &___m_OnClick_18; }
	inline void set_m_OnClick_18(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef PHYSICS2DRAYCASTER_T3382992964_H
#define PHYSICS2DRAYCASTER_T3382992964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.Physics2DRaycaster
struct  Physics2DRaycaster_t3382992964  : public PhysicsRaycaster_t437419520
{
public:
	// UnityEngine.RaycastHit2D[] UnityEngine.EventSystems.Physics2DRaycaster::m_Hits
	RaycastHit2DU5BU5D_t4286651560* ___m_Hits_11;

public:
	inline static int32_t get_offset_of_m_Hits_11() { return static_cast<int32_t>(offsetof(Physics2DRaycaster_t3382992964, ___m_Hits_11)); }
	inline RaycastHit2DU5BU5D_t4286651560* get_m_Hits_11() const { return ___m_Hits_11; }
	inline RaycastHit2DU5BU5D_t4286651560** get_address_of_m_Hits_11() { return &___m_Hits_11; }
	inline void set_m_Hits_11(RaycastHit2DU5BU5D_t4286651560* value)
	{
		___m_Hits_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hits_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2DRAYCASTER_T3382992964_H
#ifndef STANDALONEINPUTMODULE_T2760469101_H
#define STANDALONEINPUTMODULE_T2760469101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.StandaloneInputModule
struct  StandaloneInputModule_t2760469101  : public PointerInputModule_t3453173740
{
public:
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMoveVector
	Vector2_t2156229523  ___m_LastMoveVector_17;
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_19;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_20;
	// UnityEngine.GameObject UnityEngine.EventSystems.StandaloneInputModule::m_CurrentFocusedGameObject
	GameObject_t1113636619 * ___m_CurrentFocusedGameObject_21;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.StandaloneInputModule::m_InputPointerEvent
	PointerEventData_t3807901092 * ___m_InputPointerEvent_22;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_23;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_24;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_25;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_26;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_27;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_28;
	// System.Boolean UnityEngine.EventSystems.StandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_29;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_16() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_PrevActionTime_16)); }
	inline float get_m_PrevActionTime_16() const { return ___m_PrevActionTime_16; }
	inline float* get_address_of_m_PrevActionTime_16() { return &___m_PrevActionTime_16; }
	inline void set_m_PrevActionTime_16(float value)
	{
		___m_PrevActionTime_16 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_17() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMoveVector_17)); }
	inline Vector2_t2156229523  get_m_LastMoveVector_17() const { return ___m_LastMoveVector_17; }
	inline Vector2_t2156229523 * get_address_of_m_LastMoveVector_17() { return &___m_LastMoveVector_17; }
	inline void set_m_LastMoveVector_17(Vector2_t2156229523  value)
	{
		___m_LastMoveVector_17 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_18() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ConsecutiveMoveCount_18)); }
	inline int32_t get_m_ConsecutiveMoveCount_18() const { return ___m_ConsecutiveMoveCount_18; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_18() { return &___m_ConsecutiveMoveCount_18; }
	inline void set_m_ConsecutiveMoveCount_18(int32_t value)
	{
		___m_ConsecutiveMoveCount_18 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_19() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMousePosition_19)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_19() const { return ___m_LastMousePosition_19; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_19() { return &___m_LastMousePosition_19; }
	inline void set_m_LastMousePosition_19(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_19 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_20() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_MousePosition_20)); }
	inline Vector2_t2156229523  get_m_MousePosition_20() const { return ___m_MousePosition_20; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_20() { return &___m_MousePosition_20; }
	inline void set_m_MousePosition_20(Vector2_t2156229523  value)
	{
		___m_MousePosition_20 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFocusedGameObject_21() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CurrentFocusedGameObject_21)); }
	inline GameObject_t1113636619 * get_m_CurrentFocusedGameObject_21() const { return ___m_CurrentFocusedGameObject_21; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentFocusedGameObject_21() { return &___m_CurrentFocusedGameObject_21; }
	inline void set_m_CurrentFocusedGameObject_21(GameObject_t1113636619 * value)
	{
		___m_CurrentFocusedGameObject_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentFocusedGameObject_21), value);
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_22() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_InputPointerEvent_22)); }
	inline PointerEventData_t3807901092 * get_m_InputPointerEvent_22() const { return ___m_InputPointerEvent_22; }
	inline PointerEventData_t3807901092 ** get_address_of_m_InputPointerEvent_22() { return &___m_InputPointerEvent_22; }
	inline void set_m_InputPointerEvent_22(PointerEventData_t3807901092 * value)
	{
		___m_InputPointerEvent_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputPointerEvent_22), value);
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_23() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_HorizontalAxis_23)); }
	inline String_t* get_m_HorizontalAxis_23() const { return ___m_HorizontalAxis_23; }
	inline String_t** get_address_of_m_HorizontalAxis_23() { return &___m_HorizontalAxis_23; }
	inline void set_m_HorizontalAxis_23(String_t* value)
	{
		___m_HorizontalAxis_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_23), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_24() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_VerticalAxis_24)); }
	inline String_t* get_m_VerticalAxis_24() const { return ___m_VerticalAxis_24; }
	inline String_t** get_address_of_m_VerticalAxis_24() { return &___m_VerticalAxis_24; }
	inline void set_m_VerticalAxis_24(String_t* value)
	{
		___m_VerticalAxis_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_24), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_25() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_SubmitButton_25)); }
	inline String_t* get_m_SubmitButton_25() const { return ___m_SubmitButton_25; }
	inline String_t** get_address_of_m_SubmitButton_25() { return &___m_SubmitButton_25; }
	inline void set_m_SubmitButton_25(String_t* value)
	{
		___m_SubmitButton_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_25), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_26() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CancelButton_26)); }
	inline String_t* get_m_CancelButton_26() const { return ___m_CancelButton_26; }
	inline String_t** get_address_of_m_CancelButton_26() { return &___m_CancelButton_26; }
	inline void set_m_CancelButton_26(String_t* value)
	{
		___m_CancelButton_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_26), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_27() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_InputActionsPerSecond_27)); }
	inline float get_m_InputActionsPerSecond_27() const { return ___m_InputActionsPerSecond_27; }
	inline float* get_address_of_m_InputActionsPerSecond_27() { return &___m_InputActionsPerSecond_27; }
	inline void set_m_InputActionsPerSecond_27(float value)
	{
		___m_InputActionsPerSecond_27 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_28() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_RepeatDelay_28)); }
	inline float get_m_RepeatDelay_28() const { return ___m_RepeatDelay_28; }
	inline float* get_address_of_m_RepeatDelay_28() { return &___m_RepeatDelay_28; }
	inline void set_m_RepeatDelay_28(float value)
	{
		___m_RepeatDelay_28 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_29() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ForceModuleActive_29)); }
	inline bool get_m_ForceModuleActive_29() const { return ___m_ForceModuleActive_29; }
	inline bool* get_address_of_m_ForceModuleActive_29() { return &___m_ForceModuleActive_29; }
	inline void set_m_ForceModuleActive_29(bool value)
	{
		___m_ForceModuleActive_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUTMODULE_T2760469101_H
#ifndef TOUCHINPUTMODULE_T4248229598_H
#define TOUCHINPUTMODULE_T4248229598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.TouchInputModule
struct  TouchInputModule_t4248229598  : public PointerInputModule_t3453173740
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_17;
	// UnityEngine.EventSystems.PointerEventData UnityEngine.EventSystems.TouchInputModule::m_InputPointerEvent
	PointerEventData_t3807901092 * ___m_InputPointerEvent_18;
	// System.Boolean UnityEngine.EventSystems.TouchInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_19;

public:
	inline static int32_t get_offset_of_m_LastMousePosition_16() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_LastMousePosition_16)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_16() const { return ___m_LastMousePosition_16; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_16() { return &___m_LastMousePosition_16; }
	inline void set_m_LastMousePosition_16(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_16 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_17() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_MousePosition_17)); }
	inline Vector2_t2156229523  get_m_MousePosition_17() const { return ___m_MousePosition_17; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_17() { return &___m_MousePosition_17; }
	inline void set_m_MousePosition_17(Vector2_t2156229523  value)
	{
		___m_MousePosition_17 = value;
	}

	inline static int32_t get_offset_of_m_InputPointerEvent_18() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_InputPointerEvent_18)); }
	inline PointerEventData_t3807901092 * get_m_InputPointerEvent_18() const { return ___m_InputPointerEvent_18; }
	inline PointerEventData_t3807901092 ** get_address_of_m_InputPointerEvent_18() { return &___m_InputPointerEvent_18; }
	inline void set_m_InputPointerEvent_18(PointerEventData_t3807901092 * value)
	{
		___m_InputPointerEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputPointerEvent_18), value);
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_19() { return static_cast<int32_t>(offsetof(TouchInputModule_t4248229598, ___m_ForceModuleActive_19)); }
	inline bool get_m_ForceModuleActive_19() const { return ___m_ForceModuleActive_19; }
	inline bool* get_address_of_m_ForceModuleActive_19() { return &___m_ForceModuleActive_19; }
	inline void set_m_ForceModuleActive_19(bool value)
	{
		___m_ForceModuleActive_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINPUTMODULE_T4248229598_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (Fsm_t4127147824), -1, sizeof(Fsm_t4127147824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2200[106] = 
{
	0,
	0,
	Fsm_t4127147824::get_offset_of_updateHelperSetDirty_2(),
	0,
	Fsm_t4127147824_StaticFields::get_offset_of_EventData_4(),
	Fsm_t4127147824_StaticFields::get_offset_of_debugLookAtColor_5(),
	Fsm_t4127147824_StaticFields::get_offset_of_debugRaycastColor_6(),
	Fsm_t4127147824::get_offset_of_dataVersion_7(),
	Fsm_t4127147824::get_offset_of_owner_8(),
	Fsm_t4127147824::get_offset_of_usedInTemplate_9(),
	Fsm_t4127147824::get_offset_of_name_10(),
	Fsm_t4127147824::get_offset_of_startState_11(),
	Fsm_t4127147824::get_offset_of_states_12(),
	Fsm_t4127147824::get_offset_of_events_13(),
	Fsm_t4127147824::get_offset_of_globalTransitions_14(),
	Fsm_t4127147824::get_offset_of_variables_15(),
	Fsm_t4127147824::get_offset_of_description_16(),
	Fsm_t4127147824::get_offset_of_docUrl_17(),
	Fsm_t4127147824::get_offset_of_showStateLabel_18(),
	Fsm_t4127147824::get_offset_of_maxLoopCount_19(),
	Fsm_t4127147824::get_offset_of_watermark_20(),
	Fsm_t4127147824::get_offset_of_password_21(),
	Fsm_t4127147824::get_offset_of_locked_22(),
	Fsm_t4127147824::get_offset_of_manualUpdate_23(),
	Fsm_t4127147824::get_offset_of_keepDelayedEventsOnStateExit_24(),
	Fsm_t4127147824::get_offset_of_preprocessed_25(),
	Fsm_t4127147824::get_offset_of_host_26(),
	Fsm_t4127147824::get_offset_of_rootFsm_27(),
	Fsm_t4127147824::get_offset_of_subFsmList_28(),
	Fsm_t4127147824::get_offset_of_setDirty_29(),
	Fsm_t4127147824::get_offset_of_activeStateEntered_30(),
	Fsm_t4127147824::get_offset_of_ExposedEvents_31(),
	Fsm_t4127147824::get_offset_of_myLog_32(),
	Fsm_t4127147824::get_offset_of_RestartOnEnable_33(),
	Fsm_t4127147824::get_offset_of_U3CStartedU3Ek__BackingField_34(),
	Fsm_t4127147824::get_offset_of_EnableDebugFlow_35(),
	Fsm_t4127147824::get_offset_of_EnableBreakpoints_36(),
	Fsm_t4127147824::get_offset_of_StepFrame_37(),
	Fsm_t4127147824::get_offset_of_delayedEvents_38(),
	Fsm_t4127147824::get_offset_of_updateEvents_39(),
	Fsm_t4127147824::get_offset_of_removeEvents_40(),
	Fsm_t4127147824::get_offset_of_editorFlags_41(),
	Fsm_t4127147824::get_offset_of_U3CEventTargetU3Ek__BackingField_42(),
	Fsm_t4127147824::get_offset_of_initialized_43(),
	Fsm_t4127147824::get_offset_of_U3CFinishedU3Ek__BackingField_44(),
	Fsm_t4127147824::get_offset_of_activeStateName_45(),
	Fsm_t4127147824::get_offset_of_activeState_46(),
	Fsm_t4127147824::get_offset_of_switchToState_47(),
	Fsm_t4127147824::get_offset_of_previousActiveState_48(),
	Fsm_t4127147824::get_offset_of_U3CLastTransitionU3Ek__BackingField_49(),
	Fsm_t4127147824::get_offset_of_StateChanged_50(),
	Fsm_t4127147824::get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_51(),
	Fsm_t4127147824_StaticFields::get_offset_of_StateColors_52(),
	Fsm_t4127147824::get_offset_of_editState_53(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CLastClickedObjectU3Ek__BackingField_54(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_55(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CHitBreakpointU3Ek__BackingField_56(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CBreakAtFsmU3Ek__BackingField_57(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CBreakAtStateU3Ek__BackingField_58(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CIsBreakU3Ek__BackingField_59(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CIsErrorBreakU3Ek__BackingField_60(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CLastErrorU3Ek__BackingField_61(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CStepToStateChangeU3Ek__BackingField_62(),
	Fsm_t4127147824_StaticFields::get_offset_of_U3CStepFsmU3Ek__BackingField_63(),
	Fsm_t4127147824::get_offset_of_U3CSwitchedStateU3Ek__BackingField_64(),
	Fsm_t4127147824::get_offset_of_mouseEvents_65(),
	Fsm_t4127147824::get_offset_of_handleLevelLoaded_66(),
	Fsm_t4127147824::get_offset_of_handleTriggerEnter2D_67(),
	Fsm_t4127147824::get_offset_of_handleTriggerExit2D_68(),
	Fsm_t4127147824::get_offset_of_handleTriggerStay2D_69(),
	Fsm_t4127147824::get_offset_of_handleCollisionEnter2D_70(),
	Fsm_t4127147824::get_offset_of_handleCollisionExit2D_71(),
	Fsm_t4127147824::get_offset_of_handleCollisionStay2D_72(),
	Fsm_t4127147824::get_offset_of_handleTriggerEnter_73(),
	Fsm_t4127147824::get_offset_of_handleTriggerExit_74(),
	Fsm_t4127147824::get_offset_of_handleTriggerStay_75(),
	Fsm_t4127147824::get_offset_of_handleCollisionEnter_76(),
	Fsm_t4127147824::get_offset_of_handleCollisionExit_77(),
	Fsm_t4127147824::get_offset_of_handleCollisionStay_78(),
	Fsm_t4127147824::get_offset_of_handleParticleCollision_79(),
	Fsm_t4127147824::get_offset_of_handleControllerColliderHit_80(),
	Fsm_t4127147824::get_offset_of_handleJointBreak_81(),
	Fsm_t4127147824::get_offset_of_handleJointBreak2D_82(),
	Fsm_t4127147824::get_offset_of_handleOnGUI_83(),
	Fsm_t4127147824::get_offset_of_handleFixedUpdate_84(),
	Fsm_t4127147824::get_offset_of_handleLateUpdate_85(),
	Fsm_t4127147824::get_offset_of_handleApplicationEvents_86(),
	Fsm_t4127147824::get_offset_of_handleUiEvents_87(),
	Fsm_t4127147824::get_offset_of_handleLegacyNetworking_88(),
	Fsm_t4127147824::get_offset_of_U3CCollisionInfoU3Ek__BackingField_89(),
	Fsm_t4127147824::get_offset_of_U3CTriggerColliderU3Ek__BackingField_90(),
	Fsm_t4127147824::get_offset_of_U3CCollision2DInfoU3Ek__BackingField_91(),
	Fsm_t4127147824::get_offset_of_U3CTriggerCollider2DU3Ek__BackingField_92(),
	Fsm_t4127147824::get_offset_of_U3CJointBreakForceU3Ek__BackingField_93(),
	Fsm_t4127147824::get_offset_of_U3CBrokenJoint2DU3Ek__BackingField_94(),
	Fsm_t4127147824::get_offset_of_U3CParticleCollisionGOU3Ek__BackingField_95(),
	Fsm_t4127147824::get_offset_of_U3CTriggerNameU3Ek__BackingField_96(),
	Fsm_t4127147824::get_offset_of_U3CCollisionNameU3Ek__BackingField_97(),
	Fsm_t4127147824::get_offset_of_U3CTrigger2dNameU3Ek__BackingField_98(),
	Fsm_t4127147824::get_offset_of_U3CCollision2dNameU3Ek__BackingField_99(),
	Fsm_t4127147824::get_offset_of_U3CControllerColliderU3Ek__BackingField_100(),
	Fsm_t4127147824::get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_101(),
	Fsm_t4127147824_StaticFields::get_offset_of_lastRaycastHit2DInfoLUT_102(),
	Fsm_t4127147824::get_offset_of_handleAnimatorMove_103(),
	Fsm_t4127147824::get_offset_of_handleAnimatorIK_104(),
	Fsm_t4127147824_StaticFields::get_offset_of_targetSelf_105(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (EditorFlags_t2240759143)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2201[6] = 
{
	EditorFlags_t2240759143::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (UiEvents_t57509442)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2202[19] = 
{
	UiEvents_t57509442::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (FsmEvent_t3736299882), -1, sizeof(FsmEvent_t3736299882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2203[61] = 
{
	FsmEvent_t3736299882_StaticFields::get_offset_of__eventLookup_0(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_syncObj_1(),
	FsmEvent_t3736299882::get_offset_of_name_2(),
	FsmEvent_t3736299882::get_offset_of_isSystemEvent_3(),
	FsmEvent_t3736299882::get_offset_of_isGlobal_4(),
	FsmEvent_t3736299882::get_offset_of_U3CPathU3Ek__BackingField_5(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CBecameInvisibleU3Ek__BackingField_6(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CBecameVisibleU3Ek__BackingField_7(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CCollisionEnterU3Ek__BackingField_8(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CCollisionExitU3Ek__BackingField_9(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CCollisionStayU3Ek__BackingField_10(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CCollisionEnter2DU3Ek__BackingField_11(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CCollisionExit2DU3Ek__BackingField_12(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CCollisionStay2DU3Ek__BackingField_13(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CControllerColliderHitU3Ek__BackingField_14(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CFinishedU3Ek__BackingField_15(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CLevelLoadedU3Ek__BackingField_16(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseDownU3Ek__BackingField_17(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseDragU3Ek__BackingField_18(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseEnterU3Ek__BackingField_19(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseExitU3Ek__BackingField_20(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseOverU3Ek__BackingField_21(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseUpU3Ek__BackingField_22(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMouseUpAsButtonU3Ek__BackingField_23(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CTriggerEnterU3Ek__BackingField_24(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CTriggerExitU3Ek__BackingField_25(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CTriggerStayU3Ek__BackingField_26(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CTriggerEnter2DU3Ek__BackingField_27(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CTriggerExit2DU3Ek__BackingField_28(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CTriggerStay2DU3Ek__BackingField_29(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CApplicationFocusU3Ek__BackingField_30(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CApplicationPauseU3Ek__BackingField_31(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CApplicationQuitU3Ek__BackingField_32(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CParticleCollisionU3Ek__BackingField_33(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CJointBreakU3Ek__BackingField_34(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CJointBreak2DU3Ek__BackingField_35(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CDisableU3Ek__BackingField_36(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CPlayerConnectedU3Ek__BackingField_37(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CServerInitializedU3Ek__BackingField_38(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CConnectedToServerU3Ek__BackingField_39(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CPlayerDisconnectedU3Ek__BackingField_40(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CDisconnectedFromServerU3Ek__BackingField_41(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CFailedToConnectU3Ek__BackingField_42(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_43(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CMasterServerEventU3Ek__BackingField_44(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CNetworkInstantiateU3Ek__BackingField_45(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiBeginDragU3Ek__BackingField_46(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiDragU3Ek__BackingField_47(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiEndDragU3Ek__BackingField_48(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiClickU3Ek__BackingField_49(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiDropU3Ek__BackingField_50(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiPointerClickU3Ek__BackingField_51(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiPointerDownU3Ek__BackingField_52(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiPointerEnterU3Ek__BackingField_53(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiPointerExitU3Ek__BackingField_54(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiPointerUpU3Ek__BackingField_55(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiBoolValueChangedU3Ek__BackingField_56(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiFloatValueChangedU3Ek__BackingField_57(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiIntValueChangedU3Ek__BackingField_58(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiVector2ValueChangedU3Ek__BackingField_59(),
	FsmEvent_t3736299882_StaticFields::get_offset_of_U3CUiEndEditU3Ek__BackingField_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (FsmLog_t3265252880), -1, sizeof(FsmLog_t3265252880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2204[9] = 
{
	FsmLog_t3265252880_StaticFields::get_offset_of_MaxSize_0(),
	FsmLog_t3265252880_StaticFields::get_offset_of_Logs_1(),
	FsmLog_t3265252880_StaticFields::get_offset_of_logEntryPool_2(),
	FsmLog_t3265252880_StaticFields::get_offset_of_nextLogEntryPoolIndex_3(),
	FsmLog_t3265252880_StaticFields::get_offset_of_U3CLoggingEnabledU3Ek__BackingField_4(),
	FsmLog_t3265252880_StaticFields::get_offset_of_U3CMirrorDebugLogU3Ek__BackingField_5(),
	FsmLog_t3265252880_StaticFields::get_offset_of_U3CEnableDebugFlowU3Ek__BackingField_6(),
	FsmLog_t3265252880::get_offset_of_U3CFsmU3Ek__BackingField_7(),
	FsmLog_t3265252880::get_offset_of_entries_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (FsmLogEntry_t3340499788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[19] = 
{
	FsmLogEntry_t3340499788::get_offset_of_U3CLogU3Ek__BackingField_0(),
	FsmLogEntry_t3340499788::get_offset_of_U3CLogTypeU3Ek__BackingField_1(),
	FsmLogEntry_t3340499788::get_offset_of_U3CStateU3Ek__BackingField_2(),
	FsmLogEntry_t3340499788::get_offset_of_U3CSentByStateU3Ek__BackingField_3(),
	FsmLogEntry_t3340499788::get_offset_of_U3CActionU3Ek__BackingField_4(),
	FsmLogEntry_t3340499788::get_offset_of_U3CEventU3Ek__BackingField_5(),
	FsmLogEntry_t3340499788::get_offset_of_U3CTransitionU3Ek__BackingField_6(),
	FsmLogEntry_t3340499788::get_offset_of_U3CEventTargetU3Ek__BackingField_7(),
	FsmLogEntry_t3340499788::get_offset_of_U3CTimeU3Ek__BackingField_8(),
	FsmLogEntry_t3340499788::get_offset_of_U3CStateTimeU3Ek__BackingField_9(),
	FsmLogEntry_t3340499788::get_offset_of_U3CFrameCountU3Ek__BackingField_10(),
	FsmLogEntry_t3340499788::get_offset_of_U3CFsmVariablesCopyU3Ek__BackingField_11(),
	FsmLogEntry_t3340499788::get_offset_of_U3CGlobalVariablesCopyU3Ek__BackingField_12(),
	FsmLogEntry_t3340499788::get_offset_of_U3CGameObjectU3Ek__BackingField_13(),
	FsmLogEntry_t3340499788::get_offset_of_U3CGameObjectNameU3Ek__BackingField_14(),
	FsmLogEntry_t3340499788::get_offset_of_U3CGameObjectIconU3Ek__BackingField_15(),
	FsmLogEntry_t3340499788::get_offset_of_text_16(),
	FsmLogEntry_t3340499788::get_offset_of_U3CText2U3Ek__BackingField_17(),
	FsmLogEntry_t3340499788::get_offset_of_textWithTimecode_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (FsmLogType_t779083073)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2206[12] = 
{
	FsmLogType_t779083073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (FsmState_t4124398234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[21] = 
{
	FsmState_t4124398234::get_offset_of_U3CStateTimeU3Ek__BackingField_0(),
	FsmState_t4124398234::get_offset_of_U3CRealStartTimeU3Ek__BackingField_1(),
	FsmState_t4124398234::get_offset_of_U3CloopCountU3Ek__BackingField_2(),
	FsmState_t4124398234::get_offset_of_U3CmaxLoopCountU3Ek__BackingField_3(),
	FsmState_t4124398234::get_offset_of_active_4(),
	FsmState_t4124398234::get_offset_of_finished_5(),
	FsmState_t4124398234::get_offset_of_activeAction_6(),
	FsmState_t4124398234::get_offset_of_activeActionIndex_7(),
	FsmState_t4124398234::get_offset_of_fsm_8(),
	FsmState_t4124398234::get_offset_of_name_9(),
	FsmState_t4124398234::get_offset_of_description_10(),
	FsmState_t4124398234::get_offset_of_colorIndex_11(),
	FsmState_t4124398234::get_offset_of_position_12(),
	FsmState_t4124398234::get_offset_of_isBreakpoint_13(),
	FsmState_t4124398234::get_offset_of_isSequence_14(),
	FsmState_t4124398234::get_offset_of_hideUnused_15(),
	FsmState_t4124398234::get_offset_of_transitions_16(),
	FsmState_t4124398234::get_offset_of_actions_17(),
	FsmState_t4124398234::get_offset_of_actionData_18(),
	FsmState_t4124398234::get_offset_of_activeActions_19(),
	FsmState_t4124398234::get_offset_of__finishedActions_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (FsmStateAction_t3801304101), -1, sizeof(FsmStateAction_t3801304101_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2208[14] = 
{
	FsmStateAction_t3801304101_StaticFields::get_offset_of_ActiveHighlightColor_0(),
	FsmStateAction_t3801304101_StaticFields::get_offset_of_Repaint_1(),
	FsmStateAction_t3801304101::get_offset_of_name_2(),
	FsmStateAction_t3801304101::get_offset_of_enabled_3(),
	FsmStateAction_t3801304101::get_offset_of_isOpen_4(),
	FsmStateAction_t3801304101::get_offset_of_active_5(),
	FsmStateAction_t3801304101::get_offset_of_finished_6(),
	FsmStateAction_t3801304101::get_offset_of_autoName_7(),
	FsmStateAction_t3801304101::get_offset_of_owner_8(),
	FsmStateAction_t3801304101::get_offset_of_fsmState_9(),
	FsmStateAction_t3801304101::get_offset_of_fsm_10(),
	FsmStateAction_t3801304101::get_offset_of_fsmComponent_11(),
	FsmStateAction_t3801304101::get_offset_of_U3CDisplayNameU3Ek__BackingField_12(),
	FsmStateAction_t3801304101::get_offset_of_U3CEnteredU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (FsmTransition_t1340699069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[6] = 
{
	FsmTransition_t1340699069::get_offset_of_fsmEvent_0(),
	FsmTransition_t1340699069::get_offset_of_toState_1(),
	FsmTransition_t1340699069::get_offset_of_linkStyle_2(),
	FsmTransition_t1340699069::get_offset_of_linkConstraint_3(),
	FsmTransition_t1340699069::get_offset_of_colorIndex_4(),
	FsmTransition_t1340699069::get_offset_of_toFsmState_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (CustomLinkStyle_t99432972)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	CustomLinkStyle_t99432972::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (CustomLinkConstraint_t2821529372)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2211[4] = 
{
	CustomLinkConstraint_t2821529372::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (FsmUtility_t1202208704), -1, sizeof(FsmUtility_t1202208704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	FsmUtility_t1202208704_StaticFields::get_offset_of_encoding_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (BitConverter_t2995050545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (FsmVariables_t3349589544), -1, sizeof(FsmVariables_t3349589544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[18] = 
{
	FsmVariables_t3349589544_StaticFields::get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_0(),
	FsmVariables_t3349589544::get_offset_of_floatVariables_1(),
	FsmVariables_t3349589544::get_offset_of_intVariables_2(),
	FsmVariables_t3349589544::get_offset_of_boolVariables_3(),
	FsmVariables_t3349589544::get_offset_of_stringVariables_4(),
	FsmVariables_t3349589544::get_offset_of_vector2Variables_5(),
	FsmVariables_t3349589544::get_offset_of_vector3Variables_6(),
	FsmVariables_t3349589544::get_offset_of_colorVariables_7(),
	FsmVariables_t3349589544::get_offset_of_rectVariables_8(),
	FsmVariables_t3349589544::get_offset_of_quaternionVariables_9(),
	FsmVariables_t3349589544::get_offset_of_gameObjectVariables_10(),
	FsmVariables_t3349589544::get_offset_of_objectVariables_11(),
	FsmVariables_t3349589544::get_offset_of_materialVariables_12(),
	FsmVariables_t3349589544::get_offset_of_textureVariables_13(),
	FsmVariables_t3349589544::get_offset_of_arrayVariables_14(),
	FsmVariables_t3349589544::get_offset_of_enumVariables_15(),
	FsmVariables_t3349589544::get_offset_of_categories_16(),
	FsmVariables_t3349589544::get_offset_of_variableCategoryIDs_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (VariableType_t1723760892)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2215[17] = 
{
	VariableType_t1723760892::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (VariableTypeNicified_t4087325594)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[16] = 
{
	VariableTypeNicified_t4087325594::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (ArrayVariableTypesNicified_t4011111753)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2217[15] = 
{
	ArrayVariableTypesNicified_t4011111753::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (ReflectionUtils_t2510826940), -1, sizeof(ReflectionUtils_t2510826940_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	ReflectionUtils_t2510826940_StaticFields::get_offset_of_assemblyNames_0(),
	ReflectionUtils_t2510826940_StaticFields::get_offset_of_loadedAssemblies_1(),
	ReflectionUtils_t2510826940_StaticFields::get_offset_of_typeLookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (MissingAction_t3641918577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	MissingAction_t3641918577::get_offset_of_actionName_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (LoopType_t1488839698)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2221[4] = 
{
	LoopType_t1488839698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (RotationInterpolation_t1456034865)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2222[3] = 
{
	RotationInterpolation_t1456034865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (RotationOptions_t1877027720)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2223[7] = 
{
	RotationOptions_t1877027720::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (ScaleOptions_t1715998470)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2224[6] = 
{
	ScaleOptions_t1715998470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (PositionOptions_t658883822)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[7] = 
{
	PositionOptions_t658883822::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (TargetValueOptions_t2879432043)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[4] = 
{
	TargetValueOptions_t2879432043::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (ColorUtils_t43876202), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (StringUtils_t862297538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (RectExtensions_t2063872102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (TextureExtensions_t1673782326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (Point_t3074390632)+ sizeof (RuntimeObject), sizeof(Point_t3074390632 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[2] = 
{
	Point_t3074390632::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Point_t3074390632::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (U3CModuleU3E_t692745553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (EventHandle_t600343995)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[3] = 
{
	EventHandle_t600343995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (EventSystem_t1003666588), -1, sizeof(EventSystem_t1003666588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2254[12] = 
{
	EventSystem_t1003666588::get_offset_of_m_SystemInputModules_4(),
	EventSystem_t1003666588::get_offset_of_m_CurrentInputModule_5(),
	EventSystem_t1003666588_StaticFields::get_offset_of_m_EventSystems_6(),
	EventSystem_t1003666588::get_offset_of_m_FirstSelected_7(),
	EventSystem_t1003666588::get_offset_of_m_sendNavigationEvents_8(),
	EventSystem_t1003666588::get_offset_of_m_DragThreshold_9(),
	EventSystem_t1003666588::get_offset_of_m_CurrentSelected_10(),
	EventSystem_t1003666588::get_offset_of_m_HasFocus_11(),
	EventSystem_t1003666588::get_offset_of_m_SelectionGuard_12(),
	EventSystem_t1003666588::get_offset_of_m_DummyData_13(),
	EventSystem_t1003666588_StaticFields::get_offset_of_s_RaycastComparer_14(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (EventTrigger_t1076084509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[2] = 
{
	EventTrigger_t1076084509::get_offset_of_m_Delegates_4(),
	EventTrigger_t1076084509::get_offset_of_delegates_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (TriggerEvent_t3867320123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (Entry_t3344766165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[2] = 
{
	Entry_t3344766165::get_offset_of_eventID_0(),
	Entry_t3344766165::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (EventTriggerType_t55832929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2258[18] = 
{
	EventTriggerType_t55832929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (ExecuteEvents_t3484638744), -1, sizeof(ExecuteEvents_t3484638744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2259[36] = 
{
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (MoveDirection_t1216237838)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[6] = 
{
	MoveDirection_t1216237838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (RaycasterManager_t2536340562), -1, sizeof(RaycasterManager_t2536340562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2262[1] = 
{
	RaycasterManager_t2536340562_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (RaycastResult_t3360306849)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[10] = 
{
	RaycastResult_t3360306849::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t3360306849::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (UIBehaviour_t3495933518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (AxisEventData_t2331243652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[2] = 
{
	AxisEventData_t2331243652::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t2331243652::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (AbstractEventData_t4171500731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2266[1] = 
{
	AbstractEventData_t4171500731::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (BaseEventData_t3903027533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	BaseEventData_t3903027533::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (PointerEventData_t3807901092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[21] = 
{
	PointerEventData_t3807901092::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t3807901092::get_offset_of_m_PointerPress_3(),
	PointerEventData_t3807901092::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t3807901092::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t3807901092::get_offset_of_hovered_9(),
	PointerEventData_t3807901092::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t3807901092::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t3807901092::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t3807901092::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t3807901092::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t3807901092::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t3807901092::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t3807901092::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t3807901092::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t3807901092::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t3807901092::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t3807901092::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (InputButton_t3704011348)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[4] = 
{
	InputButton_t3704011348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (FramePressState_t3039385657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2270[5] = 
{
	FramePressState_t3039385657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (BaseInput_t3630163547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (BaseInputModule_t2019268878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[6] = 
{
	BaseInputModule_t2019268878::get_offset_of_m_RaycastResultCache_4(),
	BaseInputModule_t2019268878::get_offset_of_m_AxisEventData_5(),
	BaseInputModule_t2019268878::get_offset_of_m_EventSystem_6(),
	BaseInputModule_t2019268878::get_offset_of_m_BaseEventData_7(),
	BaseInputModule_t2019268878::get_offset_of_m_InputOverride_8(),
	BaseInputModule_t2019268878::get_offset_of_m_DefaultInput_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (PointerInputModule_t3453173740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3453173740::get_offset_of_m_PointerData_14(),
	PointerInputModule_t3453173740::get_offset_of_m_MouseState_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ButtonState_t857139936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[2] = 
{
	ButtonState_t857139936::get_offset_of_m_Button_0(),
	ButtonState_t857139936::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (MouseState_t384203932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[1] = 
{
	MouseState_t384203932::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (MouseButtonEventData_t3190347560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[2] = 
{
	MouseButtonEventData_t3190347560::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3190347560::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (StandaloneInputModule_t2760469101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[14] = 
{
	StandaloneInputModule_t2760469101::get_offset_of_m_PrevActionTime_16(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMoveVector_17(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ConsecutiveMoveCount_18(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMousePosition_19(),
	StandaloneInputModule_t2760469101::get_offset_of_m_MousePosition_20(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CurrentFocusedGameObject_21(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputPointerEvent_22(),
	StandaloneInputModule_t2760469101::get_offset_of_m_HorizontalAxis_23(),
	StandaloneInputModule_t2760469101::get_offset_of_m_VerticalAxis_24(),
	StandaloneInputModule_t2760469101::get_offset_of_m_SubmitButton_25(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CancelButton_26(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputActionsPerSecond_27(),
	StandaloneInputModule_t2760469101::get_offset_of_m_RepeatDelay_28(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ForceModuleActive_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (InputMode_t3382566315)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2278[3] = 
{
	InputMode_t3382566315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (TouchInputModule_t4248229598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[4] = 
{
	TouchInputModule_t4248229598::get_offset_of_m_LastMousePosition_16(),
	TouchInputModule_t4248229598::get_offset_of_m_MousePosition_17(),
	TouchInputModule_t4248229598::get_offset_of_m_InputPointerEvent_18(),
	TouchInputModule_t4248229598::get_offset_of_m_ForceModuleActive_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (BaseRaycaster_t4150874583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (Physics2DRaycaster_t3382992964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[1] = 
{
	Physics2DRaycaster_t3382992964::get_offset_of_m_Hits_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (PhysicsRaycaster_t437419520), -1, sizeof(PhysicsRaycaster_t437419520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2282[7] = 
{
	0,
	PhysicsRaycaster_t437419520::get_offset_of_m_EventCamera_5(),
	PhysicsRaycaster_t437419520::get_offset_of_m_EventMask_6(),
	PhysicsRaycaster_t437419520::get_offset_of_m_MaxRayIntersections_7(),
	PhysicsRaycaster_t437419520::get_offset_of_m_LastMaxRayIntersections_8(),
	PhysicsRaycaster_t437419520::get_offset_of_m_Hits_9(),
	PhysicsRaycaster_t437419520_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (ColorTween_t809614380)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[6] = 
{
	ColorTween_t809614380::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t809614380::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (ColorTweenMode_t1000778859)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2285[4] = 
{
	ColorTweenMode_t1000778859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (ColorTweenCallback_t1121741130), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (FloatTween_t1274330004)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[5] = 
{
	FloatTween_t1274330004::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1274330004::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1274330004::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1274330004::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1274330004::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (FloatTweenCallback_t1856710240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (AnimationTriggers_t2532145056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2291[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t2532145056::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t2532145056::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t2532145056::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t2532145056::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (Button_t4055032469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[1] = 
{
	Button_t4055032469::get_offset_of_m_OnClick_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (ButtonClickedEvent_t48803504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U3COnFinishSubmitU3Ec__Iterator0_t3413438900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[6] = 
{
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CelapsedTimeU3E__0_1(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24this_2(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24disposing_4(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (CanvasUpdate_t2572322932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2295[7] = 
{
	CanvasUpdate_t2572322932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (CanvasUpdateRegistry_t2720824592), -1, sizeof(CanvasUpdateRegistry_t2720824592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2297[7] = 
{
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (ColorBlock_t2139031574)+ sizeof (RuntimeObject), sizeof(ColorBlock_t2139031574 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2298[6] = 
{
	ColorBlock_t2139031574::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorBlock_t2139031574::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorBlock_t2139031574::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorBlock_t2139031574::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorBlock_t2139031574::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorBlock_t2139031574::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (DefaultControls_t4098465386), -1, sizeof(DefaultControls_t4098465386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2299[9] = 
{
	0,
	0,
	0,
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_TextColor_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
