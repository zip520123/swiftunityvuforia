﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// FsmTemplate
struct FsmTemplate_t1663266674;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t1692568573;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t919699796;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t2057874452;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t4259038327;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t3649179877;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t3801304101;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t1738787045;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2413660594;
// HutongGames.PlayMaker.FsmVarOverride[]
struct FsmVarOverrideU5BU5D_t4166400507;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t2808389578;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// PlayMakerProxyBase/Collision2DEvent
struct Collision2DEvent_t1483235594;
// PlayMakerProxyBase/CollisionEvent
struct CollisionEvent_t2644514664;
// PlayMakerProxyBase/ControllerCollisionEvent
struct ControllerCollisionEvent_t2755027817;
// PlayMakerProxyBase/ParticleCollisionEvent
struct ParticleCollisionEvent_t3984792766;
// PlayMakerProxyBase/Trigger2DEvent
struct Trigger2DEvent_t2411364400;
// PlayMakerProxyBase/TriggerEvent
struct TriggerEvent_t1933226311;
// System.Action
struct Action_t1264377477;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2269201059;
// System.Collections.Generic.Dictionary`2<System.Type,System.Int32>
struct Dictionary_2_t1100325521;
// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>
struct Dictionary_2_t3290498044;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.ActionReport>
struct List_1_t2186145299;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve>
struct List_1_t609818682;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray>
struct List_1_t3228936961;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool>
struct List_1_t1635882709;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor>
struct List_1_t3210974930;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>
struct List_1_t38871609;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget>
struct List_1_t2391774538;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat>
struct List_1_t60361595;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>
struct List_1_t759006388;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt>
struct List_1_t2346347883;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject>
struct List_1_t4078944939;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault>
struct List_1_t767717880;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty>
struct List_1_t2922450248;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion>
struct List_1_t1436145773;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect>
struct List_1_t826287323;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString>
struct List_1_t3257989946;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>
struct List_1_t1994274845;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar>
struct List_1_t3885735336;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2>
struct List_1_t142204123;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3>
struct List_1_t2098519259;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>
struct List_1_t3194788026;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption>
struct List_1_t189033643;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t305932461;
// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t3085084973;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1569362707;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Reflection.FieldInfo>
struct List_1_t1358810031;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t2103082695;
// System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>
struct Stack_1_t675569983;
// System.Comparison`1<PlayMakerFSM>
struct Comparison_1_t1387941410;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Enum
struct Enum_t4135868527;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1302094432;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.Collision2D
struct Collision2D_t2842956331;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ACTIONDATA_T1922786972_H
#define ACTIONDATA_T1922786972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionData
struct  ActionData_t1922786972  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::actionNames
	List_1_t3319525431 * ___actionNames_7;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::customNames
	List_1_t3319525431 * ___customNames_8;
	// System.Collections.Generic.List`1<System.Boolean> HutongGames.PlayMaker.ActionData::actionEnabled
	List_1_t1569362707 * ___actionEnabled_9;
	// System.Collections.Generic.List`1<System.Boolean> HutongGames.PlayMaker.ActionData::actionIsOpen
	List_1_t1569362707 * ___actionIsOpen_10;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::actionStartIndex
	List_1_t128053199 * ___actionStartIndex_11;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::actionHashCodes
	List_1_t128053199 * ___actionHashCodes_12;
	// System.Collections.Generic.List`1<UnityEngine.Object> HutongGames.PlayMaker.ActionData::unityObjectParams
	List_1_t2103082695 * ___unityObjectParams_13;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject> HutongGames.PlayMaker.ActionData::fsmGameObjectParams
	List_1_t759006388 * ___fsmGameObjectParams_14;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault> HutongGames.PlayMaker.ActionData::fsmOwnerDefaultParams
	List_1_t767717880 * ___fsmOwnerDefaultParams_15;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve> HutongGames.PlayMaker.ActionData::animationCurveParams
	List_1_t609818682 * ___animationCurveParams_16;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall> HutongGames.PlayMaker.ActionData::functionCallParams
	List_1_t3194788026 * ___functionCallParams_17;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl> HutongGames.PlayMaker.ActionData::fsmTemplateControlParams
	List_1_t1994274845 * ___fsmTemplateControlParams_18;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget> HutongGames.PlayMaker.ActionData::fsmEventTargetParams
	List_1_t2391774538 * ___fsmEventTargetParams_19;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty> HutongGames.PlayMaker.ActionData::fsmPropertyParams
	List_1_t2922450248 * ___fsmPropertyParams_20;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption> HutongGames.PlayMaker.ActionData::layoutOptionParams
	List_1_t189033643 * ___layoutOptionParams_21;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString> HutongGames.PlayMaker.ActionData::fsmStringParams
	List_1_t3257989946 * ___fsmStringParams_22;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject> HutongGames.PlayMaker.ActionData::fsmObjectParams
	List_1_t4078944939 * ___fsmObjectParams_23;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar> HutongGames.PlayMaker.ActionData::fsmVarParams
	List_1_t3885735336 * ___fsmVarParams_24;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray> HutongGames.PlayMaker.ActionData::fsmArrayParams
	List_1_t3228936961 * ___fsmArrayParams_25;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum> HutongGames.PlayMaker.ActionData::fsmEnumParams
	List_1_t38871609 * ___fsmEnumParams_26;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat> HutongGames.PlayMaker.ActionData::fsmFloatParams
	List_1_t60361595 * ___fsmFloatParams_27;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt> HutongGames.PlayMaker.ActionData::fsmIntParams
	List_1_t2346347883 * ___fsmIntParams_28;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool> HutongGames.PlayMaker.ActionData::fsmBoolParams
	List_1_t1635882709 * ___fsmBoolParams_29;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2> HutongGames.PlayMaker.ActionData::fsmVector2Params
	List_1_t142204123 * ___fsmVector2Params_30;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3> HutongGames.PlayMaker.ActionData::fsmVector3Params
	List_1_t2098519259 * ___fsmVector3Params_31;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor> HutongGames.PlayMaker.ActionData::fsmColorParams
	List_1_t3210974930 * ___fsmColorParams_32;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect> HutongGames.PlayMaker.ActionData::fsmRectParams
	List_1_t826287323 * ___fsmRectParams_33;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion> HutongGames.PlayMaker.ActionData::fsmQuaternionParams
	List_1_t1436145773 * ___fsmQuaternionParams_34;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::stringParams
	List_1_t3319525431 * ___stringParams_35;
	// System.Collections.Generic.List`1<System.Byte> HutongGames.PlayMaker.ActionData::byteData
	List_1_t2606371118 * ___byteData_36;
	// System.Byte[] HutongGames.PlayMaker.ActionData::byteDataAsArray
	ByteU5BU5D_t4116647657* ___byteDataAsArray_37;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::arrayParamSizes
	List_1_t128053199 * ___arrayParamSizes_38;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::arrayParamTypes
	List_1_t3319525431 * ___arrayParamTypes_39;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::customTypeSizes
	List_1_t128053199 * ___customTypeSizes_40;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::customTypeNames
	List_1_t3319525431 * ___customTypeNames_41;
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType> HutongGames.PlayMaker.ActionData::paramDataType
	List_1_t305932461 * ___paramDataType_42;
	// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::paramName
	List_1_t3319525431 * ___paramName_43;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::paramDataPos
	List_1_t128053199 * ___paramDataPos_44;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::paramByteDataSize
	List_1_t128053199 * ___paramByteDataSize_45;
	// System.Int32 HutongGames.PlayMaker.ActionData::nextParamIndex
	int32_t ___nextParamIndex_46;

public:
	inline static int32_t get_offset_of_actionNames_7() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___actionNames_7)); }
	inline List_1_t3319525431 * get_actionNames_7() const { return ___actionNames_7; }
	inline List_1_t3319525431 ** get_address_of_actionNames_7() { return &___actionNames_7; }
	inline void set_actionNames_7(List_1_t3319525431 * value)
	{
		___actionNames_7 = value;
		Il2CppCodeGenWriteBarrier((&___actionNames_7), value);
	}

	inline static int32_t get_offset_of_customNames_8() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___customNames_8)); }
	inline List_1_t3319525431 * get_customNames_8() const { return ___customNames_8; }
	inline List_1_t3319525431 ** get_address_of_customNames_8() { return &___customNames_8; }
	inline void set_customNames_8(List_1_t3319525431 * value)
	{
		___customNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___customNames_8), value);
	}

	inline static int32_t get_offset_of_actionEnabled_9() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___actionEnabled_9)); }
	inline List_1_t1569362707 * get_actionEnabled_9() const { return ___actionEnabled_9; }
	inline List_1_t1569362707 ** get_address_of_actionEnabled_9() { return &___actionEnabled_9; }
	inline void set_actionEnabled_9(List_1_t1569362707 * value)
	{
		___actionEnabled_9 = value;
		Il2CppCodeGenWriteBarrier((&___actionEnabled_9), value);
	}

	inline static int32_t get_offset_of_actionIsOpen_10() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___actionIsOpen_10)); }
	inline List_1_t1569362707 * get_actionIsOpen_10() const { return ___actionIsOpen_10; }
	inline List_1_t1569362707 ** get_address_of_actionIsOpen_10() { return &___actionIsOpen_10; }
	inline void set_actionIsOpen_10(List_1_t1569362707 * value)
	{
		___actionIsOpen_10 = value;
		Il2CppCodeGenWriteBarrier((&___actionIsOpen_10), value);
	}

	inline static int32_t get_offset_of_actionStartIndex_11() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___actionStartIndex_11)); }
	inline List_1_t128053199 * get_actionStartIndex_11() const { return ___actionStartIndex_11; }
	inline List_1_t128053199 ** get_address_of_actionStartIndex_11() { return &___actionStartIndex_11; }
	inline void set_actionStartIndex_11(List_1_t128053199 * value)
	{
		___actionStartIndex_11 = value;
		Il2CppCodeGenWriteBarrier((&___actionStartIndex_11), value);
	}

	inline static int32_t get_offset_of_actionHashCodes_12() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___actionHashCodes_12)); }
	inline List_1_t128053199 * get_actionHashCodes_12() const { return ___actionHashCodes_12; }
	inline List_1_t128053199 ** get_address_of_actionHashCodes_12() { return &___actionHashCodes_12; }
	inline void set_actionHashCodes_12(List_1_t128053199 * value)
	{
		___actionHashCodes_12 = value;
		Il2CppCodeGenWriteBarrier((&___actionHashCodes_12), value);
	}

	inline static int32_t get_offset_of_unityObjectParams_13() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___unityObjectParams_13)); }
	inline List_1_t2103082695 * get_unityObjectParams_13() const { return ___unityObjectParams_13; }
	inline List_1_t2103082695 ** get_address_of_unityObjectParams_13() { return &___unityObjectParams_13; }
	inline void set_unityObjectParams_13(List_1_t2103082695 * value)
	{
		___unityObjectParams_13 = value;
		Il2CppCodeGenWriteBarrier((&___unityObjectParams_13), value);
	}

	inline static int32_t get_offset_of_fsmGameObjectParams_14() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmGameObjectParams_14)); }
	inline List_1_t759006388 * get_fsmGameObjectParams_14() const { return ___fsmGameObjectParams_14; }
	inline List_1_t759006388 ** get_address_of_fsmGameObjectParams_14() { return &___fsmGameObjectParams_14; }
	inline void set_fsmGameObjectParams_14(List_1_t759006388 * value)
	{
		___fsmGameObjectParams_14 = value;
		Il2CppCodeGenWriteBarrier((&___fsmGameObjectParams_14), value);
	}

	inline static int32_t get_offset_of_fsmOwnerDefaultParams_15() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmOwnerDefaultParams_15)); }
	inline List_1_t767717880 * get_fsmOwnerDefaultParams_15() const { return ___fsmOwnerDefaultParams_15; }
	inline List_1_t767717880 ** get_address_of_fsmOwnerDefaultParams_15() { return &___fsmOwnerDefaultParams_15; }
	inline void set_fsmOwnerDefaultParams_15(List_1_t767717880 * value)
	{
		___fsmOwnerDefaultParams_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmOwnerDefaultParams_15), value);
	}

	inline static int32_t get_offset_of_animationCurveParams_16() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___animationCurveParams_16)); }
	inline List_1_t609818682 * get_animationCurveParams_16() const { return ___animationCurveParams_16; }
	inline List_1_t609818682 ** get_address_of_animationCurveParams_16() { return &___animationCurveParams_16; }
	inline void set_animationCurveParams_16(List_1_t609818682 * value)
	{
		___animationCurveParams_16 = value;
		Il2CppCodeGenWriteBarrier((&___animationCurveParams_16), value);
	}

	inline static int32_t get_offset_of_functionCallParams_17() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___functionCallParams_17)); }
	inline List_1_t3194788026 * get_functionCallParams_17() const { return ___functionCallParams_17; }
	inline List_1_t3194788026 ** get_address_of_functionCallParams_17() { return &___functionCallParams_17; }
	inline void set_functionCallParams_17(List_1_t3194788026 * value)
	{
		___functionCallParams_17 = value;
		Il2CppCodeGenWriteBarrier((&___functionCallParams_17), value);
	}

	inline static int32_t get_offset_of_fsmTemplateControlParams_18() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmTemplateControlParams_18)); }
	inline List_1_t1994274845 * get_fsmTemplateControlParams_18() const { return ___fsmTemplateControlParams_18; }
	inline List_1_t1994274845 ** get_address_of_fsmTemplateControlParams_18() { return &___fsmTemplateControlParams_18; }
	inline void set_fsmTemplateControlParams_18(List_1_t1994274845 * value)
	{
		___fsmTemplateControlParams_18 = value;
		Il2CppCodeGenWriteBarrier((&___fsmTemplateControlParams_18), value);
	}

	inline static int32_t get_offset_of_fsmEventTargetParams_19() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmEventTargetParams_19)); }
	inline List_1_t2391774538 * get_fsmEventTargetParams_19() const { return ___fsmEventTargetParams_19; }
	inline List_1_t2391774538 ** get_address_of_fsmEventTargetParams_19() { return &___fsmEventTargetParams_19; }
	inline void set_fsmEventTargetParams_19(List_1_t2391774538 * value)
	{
		___fsmEventTargetParams_19 = value;
		Il2CppCodeGenWriteBarrier((&___fsmEventTargetParams_19), value);
	}

	inline static int32_t get_offset_of_fsmPropertyParams_20() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmPropertyParams_20)); }
	inline List_1_t2922450248 * get_fsmPropertyParams_20() const { return ___fsmPropertyParams_20; }
	inline List_1_t2922450248 ** get_address_of_fsmPropertyParams_20() { return &___fsmPropertyParams_20; }
	inline void set_fsmPropertyParams_20(List_1_t2922450248 * value)
	{
		___fsmPropertyParams_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmPropertyParams_20), value);
	}

	inline static int32_t get_offset_of_layoutOptionParams_21() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___layoutOptionParams_21)); }
	inline List_1_t189033643 * get_layoutOptionParams_21() const { return ___layoutOptionParams_21; }
	inline List_1_t189033643 ** get_address_of_layoutOptionParams_21() { return &___layoutOptionParams_21; }
	inline void set_layoutOptionParams_21(List_1_t189033643 * value)
	{
		___layoutOptionParams_21 = value;
		Il2CppCodeGenWriteBarrier((&___layoutOptionParams_21), value);
	}

	inline static int32_t get_offset_of_fsmStringParams_22() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmStringParams_22)); }
	inline List_1_t3257989946 * get_fsmStringParams_22() const { return ___fsmStringParams_22; }
	inline List_1_t3257989946 ** get_address_of_fsmStringParams_22() { return &___fsmStringParams_22; }
	inline void set_fsmStringParams_22(List_1_t3257989946 * value)
	{
		___fsmStringParams_22 = value;
		Il2CppCodeGenWriteBarrier((&___fsmStringParams_22), value);
	}

	inline static int32_t get_offset_of_fsmObjectParams_23() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmObjectParams_23)); }
	inline List_1_t4078944939 * get_fsmObjectParams_23() const { return ___fsmObjectParams_23; }
	inline List_1_t4078944939 ** get_address_of_fsmObjectParams_23() { return &___fsmObjectParams_23; }
	inline void set_fsmObjectParams_23(List_1_t4078944939 * value)
	{
		___fsmObjectParams_23 = value;
		Il2CppCodeGenWriteBarrier((&___fsmObjectParams_23), value);
	}

	inline static int32_t get_offset_of_fsmVarParams_24() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmVarParams_24)); }
	inline List_1_t3885735336 * get_fsmVarParams_24() const { return ___fsmVarParams_24; }
	inline List_1_t3885735336 ** get_address_of_fsmVarParams_24() { return &___fsmVarParams_24; }
	inline void set_fsmVarParams_24(List_1_t3885735336 * value)
	{
		___fsmVarParams_24 = value;
		Il2CppCodeGenWriteBarrier((&___fsmVarParams_24), value);
	}

	inline static int32_t get_offset_of_fsmArrayParams_25() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmArrayParams_25)); }
	inline List_1_t3228936961 * get_fsmArrayParams_25() const { return ___fsmArrayParams_25; }
	inline List_1_t3228936961 ** get_address_of_fsmArrayParams_25() { return &___fsmArrayParams_25; }
	inline void set_fsmArrayParams_25(List_1_t3228936961 * value)
	{
		___fsmArrayParams_25 = value;
		Il2CppCodeGenWriteBarrier((&___fsmArrayParams_25), value);
	}

	inline static int32_t get_offset_of_fsmEnumParams_26() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmEnumParams_26)); }
	inline List_1_t38871609 * get_fsmEnumParams_26() const { return ___fsmEnumParams_26; }
	inline List_1_t38871609 ** get_address_of_fsmEnumParams_26() { return &___fsmEnumParams_26; }
	inline void set_fsmEnumParams_26(List_1_t38871609 * value)
	{
		___fsmEnumParams_26 = value;
		Il2CppCodeGenWriteBarrier((&___fsmEnumParams_26), value);
	}

	inline static int32_t get_offset_of_fsmFloatParams_27() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmFloatParams_27)); }
	inline List_1_t60361595 * get_fsmFloatParams_27() const { return ___fsmFloatParams_27; }
	inline List_1_t60361595 ** get_address_of_fsmFloatParams_27() { return &___fsmFloatParams_27; }
	inline void set_fsmFloatParams_27(List_1_t60361595 * value)
	{
		___fsmFloatParams_27 = value;
		Il2CppCodeGenWriteBarrier((&___fsmFloatParams_27), value);
	}

	inline static int32_t get_offset_of_fsmIntParams_28() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmIntParams_28)); }
	inline List_1_t2346347883 * get_fsmIntParams_28() const { return ___fsmIntParams_28; }
	inline List_1_t2346347883 ** get_address_of_fsmIntParams_28() { return &___fsmIntParams_28; }
	inline void set_fsmIntParams_28(List_1_t2346347883 * value)
	{
		___fsmIntParams_28 = value;
		Il2CppCodeGenWriteBarrier((&___fsmIntParams_28), value);
	}

	inline static int32_t get_offset_of_fsmBoolParams_29() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmBoolParams_29)); }
	inline List_1_t1635882709 * get_fsmBoolParams_29() const { return ___fsmBoolParams_29; }
	inline List_1_t1635882709 ** get_address_of_fsmBoolParams_29() { return &___fsmBoolParams_29; }
	inline void set_fsmBoolParams_29(List_1_t1635882709 * value)
	{
		___fsmBoolParams_29 = value;
		Il2CppCodeGenWriteBarrier((&___fsmBoolParams_29), value);
	}

	inline static int32_t get_offset_of_fsmVector2Params_30() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmVector2Params_30)); }
	inline List_1_t142204123 * get_fsmVector2Params_30() const { return ___fsmVector2Params_30; }
	inline List_1_t142204123 ** get_address_of_fsmVector2Params_30() { return &___fsmVector2Params_30; }
	inline void set_fsmVector2Params_30(List_1_t142204123 * value)
	{
		___fsmVector2Params_30 = value;
		Il2CppCodeGenWriteBarrier((&___fsmVector2Params_30), value);
	}

	inline static int32_t get_offset_of_fsmVector3Params_31() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmVector3Params_31)); }
	inline List_1_t2098519259 * get_fsmVector3Params_31() const { return ___fsmVector3Params_31; }
	inline List_1_t2098519259 ** get_address_of_fsmVector3Params_31() { return &___fsmVector3Params_31; }
	inline void set_fsmVector3Params_31(List_1_t2098519259 * value)
	{
		___fsmVector3Params_31 = value;
		Il2CppCodeGenWriteBarrier((&___fsmVector3Params_31), value);
	}

	inline static int32_t get_offset_of_fsmColorParams_32() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmColorParams_32)); }
	inline List_1_t3210974930 * get_fsmColorParams_32() const { return ___fsmColorParams_32; }
	inline List_1_t3210974930 ** get_address_of_fsmColorParams_32() { return &___fsmColorParams_32; }
	inline void set_fsmColorParams_32(List_1_t3210974930 * value)
	{
		___fsmColorParams_32 = value;
		Il2CppCodeGenWriteBarrier((&___fsmColorParams_32), value);
	}

	inline static int32_t get_offset_of_fsmRectParams_33() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmRectParams_33)); }
	inline List_1_t826287323 * get_fsmRectParams_33() const { return ___fsmRectParams_33; }
	inline List_1_t826287323 ** get_address_of_fsmRectParams_33() { return &___fsmRectParams_33; }
	inline void set_fsmRectParams_33(List_1_t826287323 * value)
	{
		___fsmRectParams_33 = value;
		Il2CppCodeGenWriteBarrier((&___fsmRectParams_33), value);
	}

	inline static int32_t get_offset_of_fsmQuaternionParams_34() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___fsmQuaternionParams_34)); }
	inline List_1_t1436145773 * get_fsmQuaternionParams_34() const { return ___fsmQuaternionParams_34; }
	inline List_1_t1436145773 ** get_address_of_fsmQuaternionParams_34() { return &___fsmQuaternionParams_34; }
	inline void set_fsmQuaternionParams_34(List_1_t1436145773 * value)
	{
		___fsmQuaternionParams_34 = value;
		Il2CppCodeGenWriteBarrier((&___fsmQuaternionParams_34), value);
	}

	inline static int32_t get_offset_of_stringParams_35() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___stringParams_35)); }
	inline List_1_t3319525431 * get_stringParams_35() const { return ___stringParams_35; }
	inline List_1_t3319525431 ** get_address_of_stringParams_35() { return &___stringParams_35; }
	inline void set_stringParams_35(List_1_t3319525431 * value)
	{
		___stringParams_35 = value;
		Il2CppCodeGenWriteBarrier((&___stringParams_35), value);
	}

	inline static int32_t get_offset_of_byteData_36() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___byteData_36)); }
	inline List_1_t2606371118 * get_byteData_36() const { return ___byteData_36; }
	inline List_1_t2606371118 ** get_address_of_byteData_36() { return &___byteData_36; }
	inline void set_byteData_36(List_1_t2606371118 * value)
	{
		___byteData_36 = value;
		Il2CppCodeGenWriteBarrier((&___byteData_36), value);
	}

	inline static int32_t get_offset_of_byteDataAsArray_37() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___byteDataAsArray_37)); }
	inline ByteU5BU5D_t4116647657* get_byteDataAsArray_37() const { return ___byteDataAsArray_37; }
	inline ByteU5BU5D_t4116647657** get_address_of_byteDataAsArray_37() { return &___byteDataAsArray_37; }
	inline void set_byteDataAsArray_37(ByteU5BU5D_t4116647657* value)
	{
		___byteDataAsArray_37 = value;
		Il2CppCodeGenWriteBarrier((&___byteDataAsArray_37), value);
	}

	inline static int32_t get_offset_of_arrayParamSizes_38() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___arrayParamSizes_38)); }
	inline List_1_t128053199 * get_arrayParamSizes_38() const { return ___arrayParamSizes_38; }
	inline List_1_t128053199 ** get_address_of_arrayParamSizes_38() { return &___arrayParamSizes_38; }
	inline void set_arrayParamSizes_38(List_1_t128053199 * value)
	{
		___arrayParamSizes_38 = value;
		Il2CppCodeGenWriteBarrier((&___arrayParamSizes_38), value);
	}

	inline static int32_t get_offset_of_arrayParamTypes_39() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___arrayParamTypes_39)); }
	inline List_1_t3319525431 * get_arrayParamTypes_39() const { return ___arrayParamTypes_39; }
	inline List_1_t3319525431 ** get_address_of_arrayParamTypes_39() { return &___arrayParamTypes_39; }
	inline void set_arrayParamTypes_39(List_1_t3319525431 * value)
	{
		___arrayParamTypes_39 = value;
		Il2CppCodeGenWriteBarrier((&___arrayParamTypes_39), value);
	}

	inline static int32_t get_offset_of_customTypeSizes_40() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___customTypeSizes_40)); }
	inline List_1_t128053199 * get_customTypeSizes_40() const { return ___customTypeSizes_40; }
	inline List_1_t128053199 ** get_address_of_customTypeSizes_40() { return &___customTypeSizes_40; }
	inline void set_customTypeSizes_40(List_1_t128053199 * value)
	{
		___customTypeSizes_40 = value;
		Il2CppCodeGenWriteBarrier((&___customTypeSizes_40), value);
	}

	inline static int32_t get_offset_of_customTypeNames_41() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___customTypeNames_41)); }
	inline List_1_t3319525431 * get_customTypeNames_41() const { return ___customTypeNames_41; }
	inline List_1_t3319525431 ** get_address_of_customTypeNames_41() { return &___customTypeNames_41; }
	inline void set_customTypeNames_41(List_1_t3319525431 * value)
	{
		___customTypeNames_41 = value;
		Il2CppCodeGenWriteBarrier((&___customTypeNames_41), value);
	}

	inline static int32_t get_offset_of_paramDataType_42() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___paramDataType_42)); }
	inline List_1_t305932461 * get_paramDataType_42() const { return ___paramDataType_42; }
	inline List_1_t305932461 ** get_address_of_paramDataType_42() { return &___paramDataType_42; }
	inline void set_paramDataType_42(List_1_t305932461 * value)
	{
		___paramDataType_42 = value;
		Il2CppCodeGenWriteBarrier((&___paramDataType_42), value);
	}

	inline static int32_t get_offset_of_paramName_43() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___paramName_43)); }
	inline List_1_t3319525431 * get_paramName_43() const { return ___paramName_43; }
	inline List_1_t3319525431 ** get_address_of_paramName_43() { return &___paramName_43; }
	inline void set_paramName_43(List_1_t3319525431 * value)
	{
		___paramName_43 = value;
		Il2CppCodeGenWriteBarrier((&___paramName_43), value);
	}

	inline static int32_t get_offset_of_paramDataPos_44() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___paramDataPos_44)); }
	inline List_1_t128053199 * get_paramDataPos_44() const { return ___paramDataPos_44; }
	inline List_1_t128053199 ** get_address_of_paramDataPos_44() { return &___paramDataPos_44; }
	inline void set_paramDataPos_44(List_1_t128053199 * value)
	{
		___paramDataPos_44 = value;
		Il2CppCodeGenWriteBarrier((&___paramDataPos_44), value);
	}

	inline static int32_t get_offset_of_paramByteDataSize_45() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___paramByteDataSize_45)); }
	inline List_1_t128053199 * get_paramByteDataSize_45() const { return ___paramByteDataSize_45; }
	inline List_1_t128053199 ** get_address_of_paramByteDataSize_45() { return &___paramByteDataSize_45; }
	inline void set_paramByteDataSize_45(List_1_t128053199 * value)
	{
		___paramByteDataSize_45 = value;
		Il2CppCodeGenWriteBarrier((&___paramByteDataSize_45), value);
	}

	inline static int32_t get_offset_of_nextParamIndex_46() { return static_cast<int32_t>(offsetof(ActionData_t1922786972, ___nextParamIndex_46)); }
	inline int32_t get_nextParamIndex_46() const { return ___nextParamIndex_46; }
	inline int32_t* get_address_of_nextParamIndex_46() { return &___nextParamIndex_46; }
	inline void set_nextParamIndex_46(int32_t value)
	{
		___nextParamIndex_46 = value;
	}
};

struct ActionData_t1922786972_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> HutongGames.PlayMaker.ActionData::ActionTypeLookup
	Dictionary_2_t2269201059 * ___ActionTypeLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]> HutongGames.PlayMaker.ActionData::ActionFieldsLookup
	Dictionary_2_t3290498044 * ___ActionFieldsLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Int32> HutongGames.PlayMaker.ActionData::ActionHashCodeLookup
	Dictionary_2_t1100325521 * ___ActionHashCodeLookup_3;
	// System.Boolean HutongGames.PlayMaker.ActionData::resaveActionData
	bool ___resaveActionData_4;
	// System.Collections.Generic.List`1<System.Int32> HutongGames.PlayMaker.ActionData::UsedIndices
	List_1_t128053199 * ___UsedIndices_5;
	// System.Collections.Generic.List`1<System.Reflection.FieldInfo> HutongGames.PlayMaker.ActionData::InitFields
	List_1_t1358810031 * ___InitFields_6;

public:
	inline static int32_t get_offset_of_ActionTypeLookup_1() { return static_cast<int32_t>(offsetof(ActionData_t1922786972_StaticFields, ___ActionTypeLookup_1)); }
	inline Dictionary_2_t2269201059 * get_ActionTypeLookup_1() const { return ___ActionTypeLookup_1; }
	inline Dictionary_2_t2269201059 ** get_address_of_ActionTypeLookup_1() { return &___ActionTypeLookup_1; }
	inline void set_ActionTypeLookup_1(Dictionary_2_t2269201059 * value)
	{
		___ActionTypeLookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___ActionTypeLookup_1), value);
	}

	inline static int32_t get_offset_of_ActionFieldsLookup_2() { return static_cast<int32_t>(offsetof(ActionData_t1922786972_StaticFields, ___ActionFieldsLookup_2)); }
	inline Dictionary_2_t3290498044 * get_ActionFieldsLookup_2() const { return ___ActionFieldsLookup_2; }
	inline Dictionary_2_t3290498044 ** get_address_of_ActionFieldsLookup_2() { return &___ActionFieldsLookup_2; }
	inline void set_ActionFieldsLookup_2(Dictionary_2_t3290498044 * value)
	{
		___ActionFieldsLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___ActionFieldsLookup_2), value);
	}

	inline static int32_t get_offset_of_ActionHashCodeLookup_3() { return static_cast<int32_t>(offsetof(ActionData_t1922786972_StaticFields, ___ActionHashCodeLookup_3)); }
	inline Dictionary_2_t1100325521 * get_ActionHashCodeLookup_3() const { return ___ActionHashCodeLookup_3; }
	inline Dictionary_2_t1100325521 ** get_address_of_ActionHashCodeLookup_3() { return &___ActionHashCodeLookup_3; }
	inline void set_ActionHashCodeLookup_3(Dictionary_2_t1100325521 * value)
	{
		___ActionHashCodeLookup_3 = value;
		Il2CppCodeGenWriteBarrier((&___ActionHashCodeLookup_3), value);
	}

	inline static int32_t get_offset_of_resaveActionData_4() { return static_cast<int32_t>(offsetof(ActionData_t1922786972_StaticFields, ___resaveActionData_4)); }
	inline bool get_resaveActionData_4() const { return ___resaveActionData_4; }
	inline bool* get_address_of_resaveActionData_4() { return &___resaveActionData_4; }
	inline void set_resaveActionData_4(bool value)
	{
		___resaveActionData_4 = value;
	}

	inline static int32_t get_offset_of_UsedIndices_5() { return static_cast<int32_t>(offsetof(ActionData_t1922786972_StaticFields, ___UsedIndices_5)); }
	inline List_1_t128053199 * get_UsedIndices_5() const { return ___UsedIndices_5; }
	inline List_1_t128053199 ** get_address_of_UsedIndices_5() { return &___UsedIndices_5; }
	inline void set_UsedIndices_5(List_1_t128053199 * value)
	{
		___UsedIndices_5 = value;
		Il2CppCodeGenWriteBarrier((&___UsedIndices_5), value);
	}

	inline static int32_t get_offset_of_InitFields_6() { return static_cast<int32_t>(offsetof(ActionData_t1922786972_StaticFields, ___InitFields_6)); }
	inline List_1_t1358810031 * get_InitFields_6() const { return ___InitFields_6; }
	inline List_1_t1358810031 ** get_address_of_InitFields_6() { return &___InitFields_6; }
	inline void set_InitFields_6(List_1_t1358810031 * value)
	{
		___InitFields_6 = value;
		Il2CppCodeGenWriteBarrier((&___InitFields_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONDATA_T1922786972_H
#ifndef CONTEXT_T1719175498_H
#define CONTEXT_T1719175498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionData/Context
struct  Context_t1719175498  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.ActionData/Context::currentFsm
	Fsm_t4127147824 * ___currentFsm_0;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.ActionData/Context::currentState
	FsmState_t4124398234 * ___currentState_1;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData/Context::currentAction
	FsmStateAction_t3801304101 * ___currentAction_2;
	// System.Int32 HutongGames.PlayMaker.ActionData/Context::currentActionIndex
	int32_t ___currentActionIndex_3;
	// System.String HutongGames.PlayMaker.ActionData/Context::currentParameter
	String_t* ___currentParameter_4;

public:
	inline static int32_t get_offset_of_currentFsm_0() { return static_cast<int32_t>(offsetof(Context_t1719175498, ___currentFsm_0)); }
	inline Fsm_t4127147824 * get_currentFsm_0() const { return ___currentFsm_0; }
	inline Fsm_t4127147824 ** get_address_of_currentFsm_0() { return &___currentFsm_0; }
	inline void set_currentFsm_0(Fsm_t4127147824 * value)
	{
		___currentFsm_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentFsm_0), value);
	}

	inline static int32_t get_offset_of_currentState_1() { return static_cast<int32_t>(offsetof(Context_t1719175498, ___currentState_1)); }
	inline FsmState_t4124398234 * get_currentState_1() const { return ___currentState_1; }
	inline FsmState_t4124398234 ** get_address_of_currentState_1() { return &___currentState_1; }
	inline void set_currentState_1(FsmState_t4124398234 * value)
	{
		___currentState_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentState_1), value);
	}

	inline static int32_t get_offset_of_currentAction_2() { return static_cast<int32_t>(offsetof(Context_t1719175498, ___currentAction_2)); }
	inline FsmStateAction_t3801304101 * get_currentAction_2() const { return ___currentAction_2; }
	inline FsmStateAction_t3801304101 ** get_address_of_currentAction_2() { return &___currentAction_2; }
	inline void set_currentAction_2(FsmStateAction_t3801304101 * value)
	{
		___currentAction_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentAction_2), value);
	}

	inline static int32_t get_offset_of_currentActionIndex_3() { return static_cast<int32_t>(offsetof(Context_t1719175498, ___currentActionIndex_3)); }
	inline int32_t get_currentActionIndex_3() const { return ___currentActionIndex_3; }
	inline int32_t* get_address_of_currentActionIndex_3() { return &___currentActionIndex_3; }
	inline void set_currentActionIndex_3(int32_t value)
	{
		___currentActionIndex_3 = value;
	}

	inline static int32_t get_offset_of_currentParameter_4() { return static_cast<int32_t>(offsetof(Context_t1719175498, ___currentParameter_4)); }
	inline String_t* get_currentParameter_4() const { return ___currentParameter_4; }
	inline String_t** get_address_of_currentParameter_4() { return &___currentParameter_4; }
	inline void set_currentParameter_4(String_t* value)
	{
		___currentParameter_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentParameter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T1719175498_H
#ifndef ACTIONREPORT_T714070557_H
#define ACTIONREPORT_T714070557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionReport
struct  ActionReport_t714070557  : public RuntimeObject
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.ActionReport::fsm
	PlayMakerFSM_t1613010231 * ___fsm_3;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.ActionReport::state
	FsmState_t4124398234 * ___state_4;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionReport::action
	FsmStateAction_t3801304101 * ___action_5;
	// System.Int32 HutongGames.PlayMaker.ActionReport::actionIndex
	int32_t ___actionIndex_6;
	// System.String HutongGames.PlayMaker.ActionReport::logText
	String_t* ___logText_7;
	// System.Boolean HutongGames.PlayMaker.ActionReport::isError
	bool ___isError_8;
	// System.String HutongGames.PlayMaker.ActionReport::parameter
	String_t* ___parameter_9;

public:
	inline static int32_t get_offset_of_fsm_3() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___fsm_3)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_3() const { return ___fsm_3; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_3() { return &___fsm_3; }
	inline void set_fsm_3(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_3 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_3), value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___state_4)); }
	inline FsmState_t4124398234 * get_state_4() const { return ___state_4; }
	inline FsmState_t4124398234 ** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(FsmState_t4124398234 * value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_action_5() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___action_5)); }
	inline FsmStateAction_t3801304101 * get_action_5() const { return ___action_5; }
	inline FsmStateAction_t3801304101 ** get_address_of_action_5() { return &___action_5; }
	inline void set_action_5(FsmStateAction_t3801304101 * value)
	{
		___action_5 = value;
		Il2CppCodeGenWriteBarrier((&___action_5), value);
	}

	inline static int32_t get_offset_of_actionIndex_6() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___actionIndex_6)); }
	inline int32_t get_actionIndex_6() const { return ___actionIndex_6; }
	inline int32_t* get_address_of_actionIndex_6() { return &___actionIndex_6; }
	inline void set_actionIndex_6(int32_t value)
	{
		___actionIndex_6 = value;
	}

	inline static int32_t get_offset_of_logText_7() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___logText_7)); }
	inline String_t* get_logText_7() const { return ___logText_7; }
	inline String_t** get_address_of_logText_7() { return &___logText_7; }
	inline void set_logText_7(String_t* value)
	{
		___logText_7 = value;
		Il2CppCodeGenWriteBarrier((&___logText_7), value);
	}

	inline static int32_t get_offset_of_isError_8() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___isError_8)); }
	inline bool get_isError_8() const { return ___isError_8; }
	inline bool* get_address_of_isError_8() { return &___isError_8; }
	inline void set_isError_8(bool value)
	{
		___isError_8 = value;
	}

	inline static int32_t get_offset_of_parameter_9() { return static_cast<int32_t>(offsetof(ActionReport_t714070557, ___parameter_9)); }
	inline String_t* get_parameter_9() const { return ___parameter_9; }
	inline String_t** get_address_of_parameter_9() { return &___parameter_9; }
	inline void set_parameter_9(String_t* value)
	{
		___parameter_9 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_9), value);
	}
};

struct ActionReport_t714070557_StaticFields
{
public:
	// System.Collections.Generic.List`1<HutongGames.PlayMaker.ActionReport> HutongGames.PlayMaker.ActionReport::ActionReportList
	List_1_t2186145299 * ___ActionReportList_0;
	// System.Int32 HutongGames.PlayMaker.ActionReport::InfoCount
	int32_t ___InfoCount_1;
	// System.Int32 HutongGames.PlayMaker.ActionReport::ErrorCount
	int32_t ___ErrorCount_2;

public:
	inline static int32_t get_offset_of_ActionReportList_0() { return static_cast<int32_t>(offsetof(ActionReport_t714070557_StaticFields, ___ActionReportList_0)); }
	inline List_1_t2186145299 * get_ActionReportList_0() const { return ___ActionReportList_0; }
	inline List_1_t2186145299 ** get_address_of_ActionReportList_0() { return &___ActionReportList_0; }
	inline void set_ActionReportList_0(List_1_t2186145299 * value)
	{
		___ActionReportList_0 = value;
		Il2CppCodeGenWriteBarrier((&___ActionReportList_0), value);
	}

	inline static int32_t get_offset_of_InfoCount_1() { return static_cast<int32_t>(offsetof(ActionReport_t714070557_StaticFields, ___InfoCount_1)); }
	inline int32_t get_InfoCount_1() const { return ___InfoCount_1; }
	inline int32_t* get_address_of_InfoCount_1() { return &___InfoCount_1; }
	inline void set_InfoCount_1(int32_t value)
	{
		___InfoCount_1 = value;
	}

	inline static int32_t get_offset_of_ErrorCount_2() { return static_cast<int32_t>(offsetof(ActionReport_t714070557_StaticFields, ___ErrorCount_2)); }
	inline int32_t get_ErrorCount_2() const { return ___ErrorCount_2; }
	inline int32_t* get_address_of_ErrorCount_2() { return &___ErrorCount_2; }
	inline void set_ErrorCount_2(int32_t value)
	{
		___ErrorCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONREPORT_T714070557_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T389513717_H
#define U3CU3EC__DISPLAYCLASS18_0_T389513717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionReport/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t389513717  : public RuntimeObject
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.ActionReport/<>c__DisplayClass18_0::fsm
	PlayMakerFSM_t1613010231 * ___fsm_0;

public:
	inline static int32_t get_offset_of_fsm_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t389513717, ___fsm_0)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_0() const { return ___fsm_0; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_0() { return &___fsm_0; }
	inline void set_fsm_0(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T389513717_H
#ifndef DEBUGUTILS_T1031043539_H
#define DEBUGUTILS_T1031043539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.DebugUtils
struct  DebugUtils_t1031043539  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGUTILS_T1031043539_H
#ifndef DELAYEDEVENT_T3987626228_H
#define DELAYEDEVENT_T3987626228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.DelayedEvent
struct  DelayedEvent_t3987626228  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.DelayedEvent::fsm
	Fsm_t4127147824 * ___fsm_0;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.DelayedEvent::fsmEvent
	FsmEvent_t3736299882 * ___fsmEvent_1;
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.DelayedEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_2;
	// HutongGames.PlayMaker.FsmEventData HutongGames.PlayMaker.DelayedEvent::eventData
	FsmEventData_t1692568573 * ___eventData_3;
	// System.Single HutongGames.PlayMaker.DelayedEvent::timer
	float ___timer_4;
	// System.Single HutongGames.PlayMaker.DelayedEvent::delay
	float ___delay_5;
	// System.Boolean HutongGames.PlayMaker.DelayedEvent::eventFired
	bool ___eventFired_6;

public:
	inline static int32_t get_offset_of_fsm_0() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___fsm_0)); }
	inline Fsm_t4127147824 * get_fsm_0() const { return ___fsm_0; }
	inline Fsm_t4127147824 ** get_address_of_fsm_0() { return &___fsm_0; }
	inline void set_fsm_0(Fsm_t4127147824 * value)
	{
		___fsm_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_0), value);
	}

	inline static int32_t get_offset_of_fsmEvent_1() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___fsmEvent_1)); }
	inline FsmEvent_t3736299882 * get_fsmEvent_1() const { return ___fsmEvent_1; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmEvent_1() { return &___fsmEvent_1; }
	inline void set_fsmEvent_1(FsmEvent_t3736299882 * value)
	{
		___fsmEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___fsmEvent_1), value);
	}

	inline static int32_t get_offset_of_eventTarget_2() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___eventTarget_2)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_2() const { return ___eventTarget_2; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_2() { return &___eventTarget_2; }
	inline void set_eventTarget_2(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_2), value);
	}

	inline static int32_t get_offset_of_eventData_3() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___eventData_3)); }
	inline FsmEventData_t1692568573 * get_eventData_3() const { return ___eventData_3; }
	inline FsmEventData_t1692568573 ** get_address_of_eventData_3() { return &___eventData_3; }
	inline void set_eventData_3(FsmEventData_t1692568573 * value)
	{
		___eventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_3), value);
	}

	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}

	inline static int32_t get_offset_of_delay_5() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___delay_5)); }
	inline float get_delay_5() const { return ___delay_5; }
	inline float* get_address_of_delay_5() { return &___delay_5; }
	inline void set_delay_5(float value)
	{
		___delay_5 = value;
	}

	inline static int32_t get_offset_of_eventFired_6() { return static_cast<int32_t>(offsetof(DelayedEvent_t3987626228, ___eventFired_6)); }
	inline bool get_eventFired_6() const { return ___eventFired_6; }
	inline bool* get_address_of_eventFired_6() { return &___eventFired_6; }
	inline void set_eventFired_6(bool value)
	{
		___eventFired_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELAYEDEVENT_T3987626228_H
#ifndef FSMANIMATIONCURVE_T3432711236_H
#define FSMANIMATIONCURVE_T3432711236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmAnimationCurve
struct  FsmAnimationCurve_t3432711236  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve HutongGames.PlayMaker.FsmAnimationCurve::curve
	AnimationCurve_t3046754366 * ___curve_0;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(FsmAnimationCurve_t3432711236, ___curve_0)); }
	inline AnimationCurve_t3046754366 * get_curve_0() const { return ___curve_0; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(AnimationCurve_t3046754366 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMANIMATIONCURVE_T3432711236_H
#ifndef FSMDEBUGUTILITY_T2030146397_H
#define FSMDEBUGUTILITY_T2030146397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmDebugUtility
struct  FsmDebugUtility_t2030146397  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMDEBUGUTILITY_T2030146397_H
#ifndef FSMEXECUTIONSTACK_T2219150547_H
#define FSMEXECUTIONSTACK_T2219150547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmExecutionStack
struct  FsmExecutionStack_t2219150547  : public RuntimeObject
{
public:

public:
};

struct FsmExecutionStack_t2219150547_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.FsmExecutionStack::fsmExecutionStack
	Stack_1_t675569983 * ___fsmExecutionStack_0;
	// System.Int32 HutongGames.PlayMaker.FsmExecutionStack::<MaxStackCount>k__BackingField
	int32_t ___U3CMaxStackCountU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_fsmExecutionStack_0() { return static_cast<int32_t>(offsetof(FsmExecutionStack_t2219150547_StaticFields, ___fsmExecutionStack_0)); }
	inline Stack_1_t675569983 * get_fsmExecutionStack_0() const { return ___fsmExecutionStack_0; }
	inline Stack_1_t675569983 ** get_address_of_fsmExecutionStack_0() { return &___fsmExecutionStack_0; }
	inline void set_fsmExecutionStack_0(Stack_1_t675569983 * value)
	{
		___fsmExecutionStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsmExecutionStack_0), value);
	}

	inline static int32_t get_offset_of_U3CMaxStackCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FsmExecutionStack_t2219150547_StaticFields, ___U3CMaxStackCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CMaxStackCountU3Ek__BackingField_1() const { return ___U3CMaxStackCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CMaxStackCountU3Ek__BackingField_1() { return &___U3CMaxStackCountU3Ek__BackingField_1; }
	inline void set_U3CMaxStackCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CMaxStackCountU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEXECUTIONSTACK_T2219150547_H
#ifndef FSMPROPERTY_T1450375506_H
#define FSMPROPERTY_T1450375506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmProperty
struct  FsmProperty_t1450375506  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmProperty::TargetObject
	FsmObject_t2606870197 * ___TargetObject_0;
	// System.String HutongGames.PlayMaker.FsmProperty::TargetTypeName
	String_t* ___TargetTypeName_1;
	// System.Type HutongGames.PlayMaker.FsmProperty::TargetType
	Type_t * ___TargetType_2;
	// System.String HutongGames.PlayMaker.FsmProperty::PropertyName
	String_t* ___PropertyName_3;
	// System.Type HutongGames.PlayMaker.FsmProperty::PropertyType
	Type_t * ___PropertyType_4;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmProperty::BoolParameter
	FsmBool_t163807967 * ___BoolParameter_5;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmProperty::FloatParameter
	FsmFloat_t2883254149 * ___FloatParameter_6;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmProperty::IntParameter
	FsmInt_t874273141 * ___IntParameter_7;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmProperty::GameObjectParameter
	FsmGameObject_t3581898942 * ___GameObjectParameter_8;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmProperty::StringParameter
	FsmString_t1785915204 * ___StringParameter_9;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmProperty::Vector2Parameter
	FsmVector2_t2965096677 * ___Vector2Parameter_10;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmProperty::Vector3Parameter
	FsmVector3_t626444517 * ___Vector3Parameter_11;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmProperty::RectParamater
	FsmRect_t3649179877 * ___RectParamater_12;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmProperty::QuaternionParameter
	FsmQuaternion_t4259038327 * ___QuaternionParameter_13;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmProperty::ObjectParameter
	FsmObject_t2606870197 * ___ObjectParameter_14;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.FsmProperty::MaterialParameter
	FsmMaterial_t2057874452 * ___MaterialParameter_15;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.FsmProperty::TextureParameter
	FsmTexture_t1738787045 * ___TextureParameter_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmProperty::ColorParameter
	FsmColor_t1738900188 * ___ColorParameter_17;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.FsmProperty::EnumParameter
	FsmEnum_t2861764163 * ___EnumParameter_18;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.FsmProperty::ArrayParameter
	FsmArray_t1756862219 * ___ArrayParameter_19;
	// System.Boolean HutongGames.PlayMaker.FsmProperty::setProperty
	bool ___setProperty_20;
	// System.Boolean HutongGames.PlayMaker.FsmProperty::initialized
	bool ___initialized_21;
	// UnityEngine.Object HutongGames.PlayMaker.FsmProperty::targetObjectCached
	Object_t631007953 * ___targetObjectCached_22;
	// System.Reflection.MemberInfo[] HutongGames.PlayMaker.FsmProperty::memberInfo
	MemberInfoU5BU5D_t1302094432* ___memberInfo_23;

public:
	inline static int32_t get_offset_of_TargetObject_0() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___TargetObject_0)); }
	inline FsmObject_t2606870197 * get_TargetObject_0() const { return ___TargetObject_0; }
	inline FsmObject_t2606870197 ** get_address_of_TargetObject_0() { return &___TargetObject_0; }
	inline void set_TargetObject_0(FsmObject_t2606870197 * value)
	{
		___TargetObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetObject_0), value);
	}

	inline static int32_t get_offset_of_TargetTypeName_1() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___TargetTypeName_1)); }
	inline String_t* get_TargetTypeName_1() const { return ___TargetTypeName_1; }
	inline String_t** get_address_of_TargetTypeName_1() { return &___TargetTypeName_1; }
	inline void set_TargetTypeName_1(String_t* value)
	{
		___TargetTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTypeName_1), value);
	}

	inline static int32_t get_offset_of_TargetType_2() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___TargetType_2)); }
	inline Type_t * get_TargetType_2() const { return ___TargetType_2; }
	inline Type_t ** get_address_of_TargetType_2() { return &___TargetType_2; }
	inline void set_TargetType_2(Type_t * value)
	{
		___TargetType_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetType_2), value);
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_PropertyType_4() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___PropertyType_4)); }
	inline Type_t * get_PropertyType_4() const { return ___PropertyType_4; }
	inline Type_t ** get_address_of_PropertyType_4() { return &___PropertyType_4; }
	inline void set_PropertyType_4(Type_t * value)
	{
		___PropertyType_4 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyType_4), value);
	}

	inline static int32_t get_offset_of_BoolParameter_5() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___BoolParameter_5)); }
	inline FsmBool_t163807967 * get_BoolParameter_5() const { return ___BoolParameter_5; }
	inline FsmBool_t163807967 ** get_address_of_BoolParameter_5() { return &___BoolParameter_5; }
	inline void set_BoolParameter_5(FsmBool_t163807967 * value)
	{
		___BoolParameter_5 = value;
		Il2CppCodeGenWriteBarrier((&___BoolParameter_5), value);
	}

	inline static int32_t get_offset_of_FloatParameter_6() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___FloatParameter_6)); }
	inline FsmFloat_t2883254149 * get_FloatParameter_6() const { return ___FloatParameter_6; }
	inline FsmFloat_t2883254149 ** get_address_of_FloatParameter_6() { return &___FloatParameter_6; }
	inline void set_FloatParameter_6(FsmFloat_t2883254149 * value)
	{
		___FloatParameter_6 = value;
		Il2CppCodeGenWriteBarrier((&___FloatParameter_6), value);
	}

	inline static int32_t get_offset_of_IntParameter_7() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___IntParameter_7)); }
	inline FsmInt_t874273141 * get_IntParameter_7() const { return ___IntParameter_7; }
	inline FsmInt_t874273141 ** get_address_of_IntParameter_7() { return &___IntParameter_7; }
	inline void set_IntParameter_7(FsmInt_t874273141 * value)
	{
		___IntParameter_7 = value;
		Il2CppCodeGenWriteBarrier((&___IntParameter_7), value);
	}

	inline static int32_t get_offset_of_GameObjectParameter_8() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___GameObjectParameter_8)); }
	inline FsmGameObject_t3581898942 * get_GameObjectParameter_8() const { return ___GameObjectParameter_8; }
	inline FsmGameObject_t3581898942 ** get_address_of_GameObjectParameter_8() { return &___GameObjectParameter_8; }
	inline void set_GameObjectParameter_8(FsmGameObject_t3581898942 * value)
	{
		___GameObjectParameter_8 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectParameter_8), value);
	}

	inline static int32_t get_offset_of_StringParameter_9() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___StringParameter_9)); }
	inline FsmString_t1785915204 * get_StringParameter_9() const { return ___StringParameter_9; }
	inline FsmString_t1785915204 ** get_address_of_StringParameter_9() { return &___StringParameter_9; }
	inline void set_StringParameter_9(FsmString_t1785915204 * value)
	{
		___StringParameter_9 = value;
		Il2CppCodeGenWriteBarrier((&___StringParameter_9), value);
	}

	inline static int32_t get_offset_of_Vector2Parameter_10() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___Vector2Parameter_10)); }
	inline FsmVector2_t2965096677 * get_Vector2Parameter_10() const { return ___Vector2Parameter_10; }
	inline FsmVector2_t2965096677 ** get_address_of_Vector2Parameter_10() { return &___Vector2Parameter_10; }
	inline void set_Vector2Parameter_10(FsmVector2_t2965096677 * value)
	{
		___Vector2Parameter_10 = value;
		Il2CppCodeGenWriteBarrier((&___Vector2Parameter_10), value);
	}

	inline static int32_t get_offset_of_Vector3Parameter_11() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___Vector3Parameter_11)); }
	inline FsmVector3_t626444517 * get_Vector3Parameter_11() const { return ___Vector3Parameter_11; }
	inline FsmVector3_t626444517 ** get_address_of_Vector3Parameter_11() { return &___Vector3Parameter_11; }
	inline void set_Vector3Parameter_11(FsmVector3_t626444517 * value)
	{
		___Vector3Parameter_11 = value;
		Il2CppCodeGenWriteBarrier((&___Vector3Parameter_11), value);
	}

	inline static int32_t get_offset_of_RectParamater_12() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___RectParamater_12)); }
	inline FsmRect_t3649179877 * get_RectParamater_12() const { return ___RectParamater_12; }
	inline FsmRect_t3649179877 ** get_address_of_RectParamater_12() { return &___RectParamater_12; }
	inline void set_RectParamater_12(FsmRect_t3649179877 * value)
	{
		___RectParamater_12 = value;
		Il2CppCodeGenWriteBarrier((&___RectParamater_12), value);
	}

	inline static int32_t get_offset_of_QuaternionParameter_13() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___QuaternionParameter_13)); }
	inline FsmQuaternion_t4259038327 * get_QuaternionParameter_13() const { return ___QuaternionParameter_13; }
	inline FsmQuaternion_t4259038327 ** get_address_of_QuaternionParameter_13() { return &___QuaternionParameter_13; }
	inline void set_QuaternionParameter_13(FsmQuaternion_t4259038327 * value)
	{
		___QuaternionParameter_13 = value;
		Il2CppCodeGenWriteBarrier((&___QuaternionParameter_13), value);
	}

	inline static int32_t get_offset_of_ObjectParameter_14() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___ObjectParameter_14)); }
	inline FsmObject_t2606870197 * get_ObjectParameter_14() const { return ___ObjectParameter_14; }
	inline FsmObject_t2606870197 ** get_address_of_ObjectParameter_14() { return &___ObjectParameter_14; }
	inline void set_ObjectParameter_14(FsmObject_t2606870197 * value)
	{
		___ObjectParameter_14 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectParameter_14), value);
	}

	inline static int32_t get_offset_of_MaterialParameter_15() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___MaterialParameter_15)); }
	inline FsmMaterial_t2057874452 * get_MaterialParameter_15() const { return ___MaterialParameter_15; }
	inline FsmMaterial_t2057874452 ** get_address_of_MaterialParameter_15() { return &___MaterialParameter_15; }
	inline void set_MaterialParameter_15(FsmMaterial_t2057874452 * value)
	{
		___MaterialParameter_15 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialParameter_15), value);
	}

	inline static int32_t get_offset_of_TextureParameter_16() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___TextureParameter_16)); }
	inline FsmTexture_t1738787045 * get_TextureParameter_16() const { return ___TextureParameter_16; }
	inline FsmTexture_t1738787045 ** get_address_of_TextureParameter_16() { return &___TextureParameter_16; }
	inline void set_TextureParameter_16(FsmTexture_t1738787045 * value)
	{
		___TextureParameter_16 = value;
		Il2CppCodeGenWriteBarrier((&___TextureParameter_16), value);
	}

	inline static int32_t get_offset_of_ColorParameter_17() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___ColorParameter_17)); }
	inline FsmColor_t1738900188 * get_ColorParameter_17() const { return ___ColorParameter_17; }
	inline FsmColor_t1738900188 ** get_address_of_ColorParameter_17() { return &___ColorParameter_17; }
	inline void set_ColorParameter_17(FsmColor_t1738900188 * value)
	{
		___ColorParameter_17 = value;
		Il2CppCodeGenWriteBarrier((&___ColorParameter_17), value);
	}

	inline static int32_t get_offset_of_EnumParameter_18() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___EnumParameter_18)); }
	inline FsmEnum_t2861764163 * get_EnumParameter_18() const { return ___EnumParameter_18; }
	inline FsmEnum_t2861764163 ** get_address_of_EnumParameter_18() { return &___EnumParameter_18; }
	inline void set_EnumParameter_18(FsmEnum_t2861764163 * value)
	{
		___EnumParameter_18 = value;
		Il2CppCodeGenWriteBarrier((&___EnumParameter_18), value);
	}

	inline static int32_t get_offset_of_ArrayParameter_19() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___ArrayParameter_19)); }
	inline FsmArray_t1756862219 * get_ArrayParameter_19() const { return ___ArrayParameter_19; }
	inline FsmArray_t1756862219 ** get_address_of_ArrayParameter_19() { return &___ArrayParameter_19; }
	inline void set_ArrayParameter_19(FsmArray_t1756862219 * value)
	{
		___ArrayParameter_19 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayParameter_19), value);
	}

	inline static int32_t get_offset_of_setProperty_20() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___setProperty_20)); }
	inline bool get_setProperty_20() const { return ___setProperty_20; }
	inline bool* get_address_of_setProperty_20() { return &___setProperty_20; }
	inline void set_setProperty_20(bool value)
	{
		___setProperty_20 = value;
	}

	inline static int32_t get_offset_of_initialized_21() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___initialized_21)); }
	inline bool get_initialized_21() const { return ___initialized_21; }
	inline bool* get_address_of_initialized_21() { return &___initialized_21; }
	inline void set_initialized_21(bool value)
	{
		___initialized_21 = value;
	}

	inline static int32_t get_offset_of_targetObjectCached_22() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___targetObjectCached_22)); }
	inline Object_t631007953 * get_targetObjectCached_22() const { return ___targetObjectCached_22; }
	inline Object_t631007953 ** get_address_of_targetObjectCached_22() { return &___targetObjectCached_22; }
	inline void set_targetObjectCached_22(Object_t631007953 * value)
	{
		___targetObjectCached_22 = value;
		Il2CppCodeGenWriteBarrier((&___targetObjectCached_22), value);
	}

	inline static int32_t get_offset_of_memberInfo_23() { return static_cast<int32_t>(offsetof(FsmProperty_t1450375506, ___memberInfo_23)); }
	inline MemberInfoU5BU5D_t1302094432* get_memberInfo_23() const { return ___memberInfo_23; }
	inline MemberInfoU5BU5D_t1302094432** get_address_of_memberInfo_23() { return &___memberInfo_23; }
	inline void set_memberInfo_23(MemberInfoU5BU5D_t1302094432* value)
	{
		___memberInfo_23 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMPROPERTY_T1450375506_H
#ifndef FSMTEMPLATECONTROL_T522200103_H
#define FSMTEMPLATECONTROL_T522200103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTemplateControl
struct  FsmTemplateControl_t522200103  : public RuntimeObject
{
public:
	// FsmTemplate HutongGames.PlayMaker.FsmTemplateControl::fsmTemplate
	FsmTemplate_t1663266674 * ___fsmTemplate_0;
	// HutongGames.PlayMaker.FsmVarOverride[] HutongGames.PlayMaker.FsmTemplateControl::fsmVarOverrides
	FsmVarOverrideU5BU5D_t4166400507* ___fsmVarOverrides_1;
	// System.Int32 HutongGames.PlayMaker.FsmTemplateControl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_2;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmTemplateControl::runFsm
	Fsm_t4127147824 * ___runFsm_3;

public:
	inline static int32_t get_offset_of_fsmTemplate_0() { return static_cast<int32_t>(offsetof(FsmTemplateControl_t522200103, ___fsmTemplate_0)); }
	inline FsmTemplate_t1663266674 * get_fsmTemplate_0() const { return ___fsmTemplate_0; }
	inline FsmTemplate_t1663266674 ** get_address_of_fsmTemplate_0() { return &___fsmTemplate_0; }
	inline void set_fsmTemplate_0(FsmTemplate_t1663266674 * value)
	{
		___fsmTemplate_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsmTemplate_0), value);
	}

	inline static int32_t get_offset_of_fsmVarOverrides_1() { return static_cast<int32_t>(offsetof(FsmTemplateControl_t522200103, ___fsmVarOverrides_1)); }
	inline FsmVarOverrideU5BU5D_t4166400507* get_fsmVarOverrides_1() const { return ___fsmVarOverrides_1; }
	inline FsmVarOverrideU5BU5D_t4166400507** get_address_of_fsmVarOverrides_1() { return &___fsmVarOverrides_1; }
	inline void set_fsmVarOverrides_1(FsmVarOverrideU5BU5D_t4166400507* value)
	{
		___fsmVarOverrides_1 = value;
		Il2CppCodeGenWriteBarrier((&___fsmVarOverrides_1), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FsmTemplateControl_t522200103, ___U3CIDU3Ek__BackingField_2)); }
	inline int32_t get_U3CIDU3Ek__BackingField_2() const { return ___U3CIDU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_2() { return &___U3CIDU3Ek__BackingField_2; }
	inline void set_U3CIDU3Ek__BackingField_2(int32_t value)
	{
		___U3CIDU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_runFsm_3() { return static_cast<int32_t>(offsetof(FsmTemplateControl_t522200103, ___runFsm_3)); }
	inline Fsm_t4127147824 * get_runFsm_3() const { return ___runFsm_3; }
	inline Fsm_t4127147824 ** get_address_of_runFsm_3() { return &___runFsm_3; }
	inline void set_runFsm_3(Fsm_t4127147824 * value)
	{
		___runFsm_3 = value;
		Il2CppCodeGenWriteBarrier((&___runFsm_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMTEMPLATECONTROL_T522200103_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T1615683978_H
#define U3CU3EC__DISPLAYCLASS16_0_T1615683978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t1615683978  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass16_0::namedVariable
	NamedVariable_t2808389578 * ___namedVariable_0;

public:
	inline static int32_t get_offset_of_namedVariable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t1615683978, ___namedVariable_0)); }
	inline NamedVariable_t2808389578 * get_namedVariable_0() const { return ___namedVariable_0; }
	inline NamedVariable_t2808389578 ** get_address_of_namedVariable_0() { return &___namedVariable_0; }
	inline void set_namedVariable_0(NamedVariable_t2808389578 * value)
	{
		___namedVariable_0 = value;
		Il2CppCodeGenWriteBarrier((&___namedVariable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T1615683978_H
#ifndef FSMTIME_T2866059696_H
#define FSMTIME_T2866059696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTime
struct  FsmTime_t2866059696  : public RuntimeObject
{
public:

public:
};

struct FsmTime_t2866059696_StaticFields
{
public:
	// System.Boolean HutongGames.PlayMaker.FsmTime::firstUpdateHasHappened
	bool ___firstUpdateHasHappened_0;
	// System.Single HutongGames.PlayMaker.FsmTime::totalEditorPlayerPausedTime
	float ___totalEditorPlayerPausedTime_1;
	// System.Single HutongGames.PlayMaker.FsmTime::realtimeLastUpdate
	float ___realtimeLastUpdate_2;
	// System.Int32 HutongGames.PlayMaker.FsmTime::frameCountLastUpdate
	int32_t ___frameCountLastUpdate_3;

public:
	inline static int32_t get_offset_of_firstUpdateHasHappened_0() { return static_cast<int32_t>(offsetof(FsmTime_t2866059696_StaticFields, ___firstUpdateHasHappened_0)); }
	inline bool get_firstUpdateHasHappened_0() const { return ___firstUpdateHasHappened_0; }
	inline bool* get_address_of_firstUpdateHasHappened_0() { return &___firstUpdateHasHappened_0; }
	inline void set_firstUpdateHasHappened_0(bool value)
	{
		___firstUpdateHasHappened_0 = value;
	}

	inline static int32_t get_offset_of_totalEditorPlayerPausedTime_1() { return static_cast<int32_t>(offsetof(FsmTime_t2866059696_StaticFields, ___totalEditorPlayerPausedTime_1)); }
	inline float get_totalEditorPlayerPausedTime_1() const { return ___totalEditorPlayerPausedTime_1; }
	inline float* get_address_of_totalEditorPlayerPausedTime_1() { return &___totalEditorPlayerPausedTime_1; }
	inline void set_totalEditorPlayerPausedTime_1(float value)
	{
		___totalEditorPlayerPausedTime_1 = value;
	}

	inline static int32_t get_offset_of_realtimeLastUpdate_2() { return static_cast<int32_t>(offsetof(FsmTime_t2866059696_StaticFields, ___realtimeLastUpdate_2)); }
	inline float get_realtimeLastUpdate_2() const { return ___realtimeLastUpdate_2; }
	inline float* get_address_of_realtimeLastUpdate_2() { return &___realtimeLastUpdate_2; }
	inline void set_realtimeLastUpdate_2(float value)
	{
		___realtimeLastUpdate_2 = value;
	}

	inline static int32_t get_offset_of_frameCountLastUpdate_3() { return static_cast<int32_t>(offsetof(FsmTime_t2866059696_StaticFields, ___frameCountLastUpdate_3)); }
	inline int32_t get_frameCountLastUpdate_3() const { return ___frameCountLastUpdate_3; }
	inline int32_t* get_address_of_frameCountLastUpdate_3() { return &___frameCountLastUpdate_3; }
	inline void set_frameCountLastUpdate_3(int32_t value)
	{
		___frameCountLastUpdate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMTIME_T2866059696_H
#ifndef FSMVAROVERRIDE_T858756622_H
#define FSMVAROVERRIDE_T858756622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVarOverride
struct  FsmVarOverride_t858756622  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVarOverride::variable
	NamedVariable_t2808389578 * ___variable_0;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.FsmVarOverride::fsmVar
	FsmVar_t2413660594 * ___fsmVar_1;
	// System.Boolean HutongGames.PlayMaker.FsmVarOverride::isEdited
	bool ___isEdited_2;

public:
	inline static int32_t get_offset_of_variable_0() { return static_cast<int32_t>(offsetof(FsmVarOverride_t858756622, ___variable_0)); }
	inline NamedVariable_t2808389578 * get_variable_0() const { return ___variable_0; }
	inline NamedVariable_t2808389578 ** get_address_of_variable_0() { return &___variable_0; }
	inline void set_variable_0(NamedVariable_t2808389578 * value)
	{
		___variable_0 = value;
		Il2CppCodeGenWriteBarrier((&___variable_0), value);
	}

	inline static int32_t get_offset_of_fsmVar_1() { return static_cast<int32_t>(offsetof(FsmVarOverride_t858756622, ___fsmVar_1)); }
	inline FsmVar_t2413660594 * get_fsmVar_1() const { return ___fsmVar_1; }
	inline FsmVar_t2413660594 ** get_address_of_fsmVar_1() { return &___fsmVar_1; }
	inline void set_fsmVar_1(FsmVar_t2413660594 * value)
	{
		___fsmVar_1 = value;
		Il2CppCodeGenWriteBarrier((&___fsmVar_1), value);
	}

	inline static int32_t get_offset_of_isEdited_2() { return static_cast<int32_t>(offsetof(FsmVarOverride_t858756622, ___isEdited_2)); }
	inline bool get_isEdited_2() const { return ___isEdited_2; }
	inline bool* get_address_of_isEdited_2() { return &___isEdited_2; }
	inline void set_isEdited_2(bool value)
	{
		___isEdited_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMVAROVERRIDE_T858756622_H
#ifndef FUNCTIONCALL_T1722713284_H
#define FUNCTIONCALL_T1722713284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FunctionCall
struct  FunctionCall_t1722713284  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FunctionCall::FunctionName
	String_t* ___FunctionName_0;
	// System.String HutongGames.PlayMaker.FunctionCall::parameterType
	String_t* ___parameterType_1;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FunctionCall::BoolParameter
	FsmBool_t163807967 * ___BoolParameter_2;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FunctionCall::FloatParameter
	FsmFloat_t2883254149 * ___FloatParameter_3;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FunctionCall::IntParameter
	FsmInt_t874273141 * ___IntParameter_4;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FunctionCall::GameObjectParameter
	FsmGameObject_t3581898942 * ___GameObjectParameter_5;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FunctionCall::ObjectParameter
	FsmObject_t2606870197 * ___ObjectParameter_6;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FunctionCall::StringParameter
	FsmString_t1785915204 * ___StringParameter_7;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FunctionCall::Vector2Parameter
	FsmVector2_t2965096677 * ___Vector2Parameter_8;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FunctionCall::Vector3Parameter
	FsmVector3_t626444517 * ___Vector3Parameter_9;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FunctionCall::RectParamater
	FsmRect_t3649179877 * ___RectParamater_10;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FunctionCall::QuaternionParameter
	FsmQuaternion_t4259038327 * ___QuaternionParameter_11;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.FunctionCall::MaterialParameter
	FsmMaterial_t2057874452 * ___MaterialParameter_12;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.FunctionCall::TextureParameter
	FsmTexture_t1738787045 * ___TextureParameter_13;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FunctionCall::ColorParameter
	FsmColor_t1738900188 * ___ColorParameter_14;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.FunctionCall::EnumParameter
	FsmEnum_t2861764163 * ___EnumParameter_15;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.FunctionCall::ArrayParameter
	FsmArray_t1756862219 * ___ArrayParameter_16;

public:
	inline static int32_t get_offset_of_FunctionName_0() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___FunctionName_0)); }
	inline String_t* get_FunctionName_0() const { return ___FunctionName_0; }
	inline String_t** get_address_of_FunctionName_0() { return &___FunctionName_0; }
	inline void set_FunctionName_0(String_t* value)
	{
		___FunctionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___FunctionName_0), value);
	}

	inline static int32_t get_offset_of_parameterType_1() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___parameterType_1)); }
	inline String_t* get_parameterType_1() const { return ___parameterType_1; }
	inline String_t** get_address_of_parameterType_1() { return &___parameterType_1; }
	inline void set_parameterType_1(String_t* value)
	{
		___parameterType_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameterType_1), value);
	}

	inline static int32_t get_offset_of_BoolParameter_2() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___BoolParameter_2)); }
	inline FsmBool_t163807967 * get_BoolParameter_2() const { return ___BoolParameter_2; }
	inline FsmBool_t163807967 ** get_address_of_BoolParameter_2() { return &___BoolParameter_2; }
	inline void set_BoolParameter_2(FsmBool_t163807967 * value)
	{
		___BoolParameter_2 = value;
		Il2CppCodeGenWriteBarrier((&___BoolParameter_2), value);
	}

	inline static int32_t get_offset_of_FloatParameter_3() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___FloatParameter_3)); }
	inline FsmFloat_t2883254149 * get_FloatParameter_3() const { return ___FloatParameter_3; }
	inline FsmFloat_t2883254149 ** get_address_of_FloatParameter_3() { return &___FloatParameter_3; }
	inline void set_FloatParameter_3(FsmFloat_t2883254149 * value)
	{
		___FloatParameter_3 = value;
		Il2CppCodeGenWriteBarrier((&___FloatParameter_3), value);
	}

	inline static int32_t get_offset_of_IntParameter_4() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___IntParameter_4)); }
	inline FsmInt_t874273141 * get_IntParameter_4() const { return ___IntParameter_4; }
	inline FsmInt_t874273141 ** get_address_of_IntParameter_4() { return &___IntParameter_4; }
	inline void set_IntParameter_4(FsmInt_t874273141 * value)
	{
		___IntParameter_4 = value;
		Il2CppCodeGenWriteBarrier((&___IntParameter_4), value);
	}

	inline static int32_t get_offset_of_GameObjectParameter_5() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___GameObjectParameter_5)); }
	inline FsmGameObject_t3581898942 * get_GameObjectParameter_5() const { return ___GameObjectParameter_5; }
	inline FsmGameObject_t3581898942 ** get_address_of_GameObjectParameter_5() { return &___GameObjectParameter_5; }
	inline void set_GameObjectParameter_5(FsmGameObject_t3581898942 * value)
	{
		___GameObjectParameter_5 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectParameter_5), value);
	}

	inline static int32_t get_offset_of_ObjectParameter_6() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___ObjectParameter_6)); }
	inline FsmObject_t2606870197 * get_ObjectParameter_6() const { return ___ObjectParameter_6; }
	inline FsmObject_t2606870197 ** get_address_of_ObjectParameter_6() { return &___ObjectParameter_6; }
	inline void set_ObjectParameter_6(FsmObject_t2606870197 * value)
	{
		___ObjectParameter_6 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectParameter_6), value);
	}

	inline static int32_t get_offset_of_StringParameter_7() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___StringParameter_7)); }
	inline FsmString_t1785915204 * get_StringParameter_7() const { return ___StringParameter_7; }
	inline FsmString_t1785915204 ** get_address_of_StringParameter_7() { return &___StringParameter_7; }
	inline void set_StringParameter_7(FsmString_t1785915204 * value)
	{
		___StringParameter_7 = value;
		Il2CppCodeGenWriteBarrier((&___StringParameter_7), value);
	}

	inline static int32_t get_offset_of_Vector2Parameter_8() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___Vector2Parameter_8)); }
	inline FsmVector2_t2965096677 * get_Vector2Parameter_8() const { return ___Vector2Parameter_8; }
	inline FsmVector2_t2965096677 ** get_address_of_Vector2Parameter_8() { return &___Vector2Parameter_8; }
	inline void set_Vector2Parameter_8(FsmVector2_t2965096677 * value)
	{
		___Vector2Parameter_8 = value;
		Il2CppCodeGenWriteBarrier((&___Vector2Parameter_8), value);
	}

	inline static int32_t get_offset_of_Vector3Parameter_9() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___Vector3Parameter_9)); }
	inline FsmVector3_t626444517 * get_Vector3Parameter_9() const { return ___Vector3Parameter_9; }
	inline FsmVector3_t626444517 ** get_address_of_Vector3Parameter_9() { return &___Vector3Parameter_9; }
	inline void set_Vector3Parameter_9(FsmVector3_t626444517 * value)
	{
		___Vector3Parameter_9 = value;
		Il2CppCodeGenWriteBarrier((&___Vector3Parameter_9), value);
	}

	inline static int32_t get_offset_of_RectParamater_10() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___RectParamater_10)); }
	inline FsmRect_t3649179877 * get_RectParamater_10() const { return ___RectParamater_10; }
	inline FsmRect_t3649179877 ** get_address_of_RectParamater_10() { return &___RectParamater_10; }
	inline void set_RectParamater_10(FsmRect_t3649179877 * value)
	{
		___RectParamater_10 = value;
		Il2CppCodeGenWriteBarrier((&___RectParamater_10), value);
	}

	inline static int32_t get_offset_of_QuaternionParameter_11() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___QuaternionParameter_11)); }
	inline FsmQuaternion_t4259038327 * get_QuaternionParameter_11() const { return ___QuaternionParameter_11; }
	inline FsmQuaternion_t4259038327 ** get_address_of_QuaternionParameter_11() { return &___QuaternionParameter_11; }
	inline void set_QuaternionParameter_11(FsmQuaternion_t4259038327 * value)
	{
		___QuaternionParameter_11 = value;
		Il2CppCodeGenWriteBarrier((&___QuaternionParameter_11), value);
	}

	inline static int32_t get_offset_of_MaterialParameter_12() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___MaterialParameter_12)); }
	inline FsmMaterial_t2057874452 * get_MaterialParameter_12() const { return ___MaterialParameter_12; }
	inline FsmMaterial_t2057874452 ** get_address_of_MaterialParameter_12() { return &___MaterialParameter_12; }
	inline void set_MaterialParameter_12(FsmMaterial_t2057874452 * value)
	{
		___MaterialParameter_12 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialParameter_12), value);
	}

	inline static int32_t get_offset_of_TextureParameter_13() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___TextureParameter_13)); }
	inline FsmTexture_t1738787045 * get_TextureParameter_13() const { return ___TextureParameter_13; }
	inline FsmTexture_t1738787045 ** get_address_of_TextureParameter_13() { return &___TextureParameter_13; }
	inline void set_TextureParameter_13(FsmTexture_t1738787045 * value)
	{
		___TextureParameter_13 = value;
		Il2CppCodeGenWriteBarrier((&___TextureParameter_13), value);
	}

	inline static int32_t get_offset_of_ColorParameter_14() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___ColorParameter_14)); }
	inline FsmColor_t1738900188 * get_ColorParameter_14() const { return ___ColorParameter_14; }
	inline FsmColor_t1738900188 ** get_address_of_ColorParameter_14() { return &___ColorParameter_14; }
	inline void set_ColorParameter_14(FsmColor_t1738900188 * value)
	{
		___ColorParameter_14 = value;
		Il2CppCodeGenWriteBarrier((&___ColorParameter_14), value);
	}

	inline static int32_t get_offset_of_EnumParameter_15() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___EnumParameter_15)); }
	inline FsmEnum_t2861764163 * get_EnumParameter_15() const { return ___EnumParameter_15; }
	inline FsmEnum_t2861764163 ** get_address_of_EnumParameter_15() { return &___EnumParameter_15; }
	inline void set_EnumParameter_15(FsmEnum_t2861764163 * value)
	{
		___EnumParameter_15 = value;
		Il2CppCodeGenWriteBarrier((&___EnumParameter_15), value);
	}

	inline static int32_t get_offset_of_ArrayParameter_16() { return static_cast<int32_t>(offsetof(FunctionCall_t1722713284, ___ArrayParameter_16)); }
	inline FsmArray_t1756862219 * get_ArrayParameter_16() const { return ___ArrayParameter_16; }
	inline FsmArray_t1756862219 ** get_address_of_ArrayParameter_16() { return &___ArrayParameter_16; }
	inline void set_ArrayParameter_16(FsmArray_t1756862219 * value)
	{
		___ArrayParameter_16 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayParameter_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONCALL_T1722713284_H
#ifndef NAMEDVARIABLE_T2808389578_H
#define NAMEDVARIABLE_T2808389578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.NamedVariable
struct  NamedVariable_t2808389578  : public RuntimeObject
{
public:
	// System.Boolean HutongGames.PlayMaker.NamedVariable::useVariable
	bool ___useVariable_0;
	// System.String HutongGames.PlayMaker.NamedVariable::name
	String_t* ___name_1;
	// System.String HutongGames.PlayMaker.NamedVariable::tooltip
	String_t* ___tooltip_2;
	// System.Boolean HutongGames.PlayMaker.NamedVariable::showInInspector
	bool ___showInInspector_3;
	// System.Boolean HutongGames.PlayMaker.NamedVariable::networkSync
	bool ___networkSync_4;
	// System.Object HutongGames.PlayMaker.NamedVariable::obj
	RuntimeObject * ___obj_5;

public:
	inline static int32_t get_offset_of_useVariable_0() { return static_cast<int32_t>(offsetof(NamedVariable_t2808389578, ___useVariable_0)); }
	inline bool get_useVariable_0() const { return ___useVariable_0; }
	inline bool* get_address_of_useVariable_0() { return &___useVariable_0; }
	inline void set_useVariable_0(bool value)
	{
		___useVariable_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(NamedVariable_t2808389578, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_tooltip_2() { return static_cast<int32_t>(offsetof(NamedVariable_t2808389578, ___tooltip_2)); }
	inline String_t* get_tooltip_2() const { return ___tooltip_2; }
	inline String_t** get_address_of_tooltip_2() { return &___tooltip_2; }
	inline void set_tooltip_2(String_t* value)
	{
		___tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_2), value);
	}

	inline static int32_t get_offset_of_showInInspector_3() { return static_cast<int32_t>(offsetof(NamedVariable_t2808389578, ___showInInspector_3)); }
	inline bool get_showInInspector_3() const { return ___showInInspector_3; }
	inline bool* get_address_of_showInInspector_3() { return &___showInInspector_3; }
	inline void set_showInInspector_3(bool value)
	{
		___showInInspector_3 = value;
	}

	inline static int32_t get_offset_of_networkSync_4() { return static_cast<int32_t>(offsetof(NamedVariable_t2808389578, ___networkSync_4)); }
	inline bool get_networkSync_4() const { return ___networkSync_4; }
	inline bool* get_address_of_networkSync_4() { return &___networkSync_4; }
	inline void set_networkSync_4(bool value)
	{
		___networkSync_4 = value;
	}

	inline static int32_t get_offset_of_obj_5() { return static_cast<int32_t>(offsetof(NamedVariable_t2808389578, ___obj_5)); }
	inline RuntimeObject * get_obj_5() const { return ___obj_5; }
	inline RuntimeObject ** get_address_of_obj_5() { return &___obj_5; }
	inline void set_obj_5(RuntimeObject * value)
	{
		___obj_5 = value;
		Il2CppCodeGenWriteBarrier((&___obj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDVARIABLE_T2808389578_H
#ifndef U3CU3EC_T2300308631_H
#define U3CU3EC_T2300308631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGUI/<>c
struct  U3CU3Ec_t2300308631  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2300308631_StaticFields
{
public:
	// PlayMakerGUI/<>c PlayMakerGUI/<>c::<>9
	U3CU3Ec_t2300308631 * ___U3CU3E9_0;
	// System.Comparison`1<PlayMakerFSM> PlayMakerGUI/<>c::<>9__65_0
	Comparison_1_t1387941410 * ___U3CU3E9__65_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2300308631_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2300308631 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2300308631 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2300308631 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__65_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2300308631_StaticFields, ___U3CU3E9__65_0_1)); }
	inline Comparison_1_t1387941410 * get_U3CU3E9__65_0_1() const { return ___U3CU3E9__65_0_1; }
	inline Comparison_1_t1387941410 ** get_address_of_U3CU3E9__65_0_1() { return &___U3CU3E9__65_0_1; }
	inline void set_U3CU3E9__65_0_1(Comparison_1_t1387941410 * value)
	{
		___U3CU3E9__65_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__65_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2300308631_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ACTIONCATEGORYATTRIBUTE_T1469261392_H
#define ACTIONCATEGORYATTRIBUTE_T1469261392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionCategoryAttribute
struct  ActionCategoryAttribute_t1469261392  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.ActionCategoryAttribute::category
	String_t* ___category_0;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(ActionCategoryAttribute_t1469261392, ___category_0)); }
	inline String_t* get_category_0() const { return ___category_0; }
	inline String_t** get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(String_t* value)
	{
		___category_0 = value;
		Il2CppCodeGenWriteBarrier((&___category_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCATEGORYATTRIBUTE_T1469261392_H
#ifndef ACTIONSECTION_T3425516445_H
#define ACTIONSECTION_T3425516445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionSection
struct  ActionSection_t3425516445  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.ActionSection::section
	String_t* ___section_0;

public:
	inline static int32_t get_offset_of_section_0() { return static_cast<int32_t>(offsetof(ActionSection_t3425516445, ___section_0)); }
	inline String_t* get_section_0() const { return ___section_0; }
	inline String_t** get_address_of_section_0() { return &___section_0; }
	inline void set_section_0(String_t* value)
	{
		___section_0 = value;
		Il2CppCodeGenWriteBarrier((&___section_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONSECTION_T3425516445_H
#ifndef ACTIONTARGET_T3397043202_H
#define ACTIONTARGET_T3397043202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionTarget
struct  ActionTarget_t3397043202  : public Attribute_t861562559
{
public:
	// System.Type HutongGames.PlayMaker.ActionTarget::objectType
	Type_t * ___objectType_0;
	// System.String HutongGames.PlayMaker.ActionTarget::fieldName
	String_t* ___fieldName_1;
	// System.Boolean HutongGames.PlayMaker.ActionTarget::allowPrefabs
	bool ___allowPrefabs_2;

public:
	inline static int32_t get_offset_of_objectType_0() { return static_cast<int32_t>(offsetof(ActionTarget_t3397043202, ___objectType_0)); }
	inline Type_t * get_objectType_0() const { return ___objectType_0; }
	inline Type_t ** get_address_of_objectType_0() { return &___objectType_0; }
	inline void set_objectType_0(Type_t * value)
	{
		___objectType_0 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_0), value);
	}

	inline static int32_t get_offset_of_fieldName_1() { return static_cast<int32_t>(offsetof(ActionTarget_t3397043202, ___fieldName_1)); }
	inline String_t* get_fieldName_1() const { return ___fieldName_1; }
	inline String_t** get_address_of_fieldName_1() { return &___fieldName_1; }
	inline void set_fieldName_1(String_t* value)
	{
		___fieldName_1 = value;
		Il2CppCodeGenWriteBarrier((&___fieldName_1), value);
	}

	inline static int32_t get_offset_of_allowPrefabs_2() { return static_cast<int32_t>(offsetof(ActionTarget_t3397043202, ___allowPrefabs_2)); }
	inline bool get_allowPrefabs_2() const { return ___allowPrefabs_2; }
	inline bool* get_address_of_allowPrefabs_2() { return &___allowPrefabs_2; }
	inline void set_allowPrefabs_2(bool value)
	{
		___allowPrefabs_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONTARGET_T3397043202_H
#ifndef CHECKFORCOMPONENTATTRIBUTE_T1147964680_H
#define CHECKFORCOMPONENTATTRIBUTE_T1147964680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.CheckForComponentAttribute
struct  CheckForComponentAttribute_t1147964680  : public Attribute_t861562559
{
public:
	// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::type0
	Type_t * ___type0_0;
	// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::type1
	Type_t * ___type1_1;
	// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::type2
	Type_t * ___type2_2;

public:
	inline static int32_t get_offset_of_type0_0() { return static_cast<int32_t>(offsetof(CheckForComponentAttribute_t1147964680, ___type0_0)); }
	inline Type_t * get_type0_0() const { return ___type0_0; }
	inline Type_t ** get_address_of_type0_0() { return &___type0_0; }
	inline void set_type0_0(Type_t * value)
	{
		___type0_0 = value;
		Il2CppCodeGenWriteBarrier((&___type0_0), value);
	}

	inline static int32_t get_offset_of_type1_1() { return static_cast<int32_t>(offsetof(CheckForComponentAttribute_t1147964680, ___type1_1)); }
	inline Type_t * get_type1_1() const { return ___type1_1; }
	inline Type_t ** get_address_of_type1_1() { return &___type1_1; }
	inline void set_type1_1(Type_t * value)
	{
		___type1_1 = value;
		Il2CppCodeGenWriteBarrier((&___type1_1), value);
	}

	inline static int32_t get_offset_of_type2_2() { return static_cast<int32_t>(offsetof(CheckForComponentAttribute_t1147964680, ___type2_2)); }
	inline Type_t * get_type2_2() const { return ___type2_2; }
	inline Type_t ** get_address_of_type2_2() { return &___type2_2; }
	inline void set_type2_2(Type_t * value)
	{
		___type2_2 = value;
		Il2CppCodeGenWriteBarrier((&___type2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKFORCOMPONENTATTRIBUTE_T1147964680_H
#ifndef COMPOUNDARRAYATTRIBUTE_T3735867164_H
#define COMPOUNDARRAYATTRIBUTE_T3735867164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.CompoundArrayAttribute
struct  CompoundArrayAttribute_t3735867164  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.CompoundArrayAttribute::name
	String_t* ___name_0;
	// System.String HutongGames.PlayMaker.CompoundArrayAttribute::firstArrayName
	String_t* ___firstArrayName_1;
	// System.String HutongGames.PlayMaker.CompoundArrayAttribute::secondArrayName
	String_t* ___secondArrayName_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CompoundArrayAttribute_t3735867164, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_firstArrayName_1() { return static_cast<int32_t>(offsetof(CompoundArrayAttribute_t3735867164, ___firstArrayName_1)); }
	inline String_t* get_firstArrayName_1() const { return ___firstArrayName_1; }
	inline String_t** get_address_of_firstArrayName_1() { return &___firstArrayName_1; }
	inline void set_firstArrayName_1(String_t* value)
	{
		___firstArrayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstArrayName_1), value);
	}

	inline static int32_t get_offset_of_secondArrayName_2() { return static_cast<int32_t>(offsetof(CompoundArrayAttribute_t3735867164, ___secondArrayName_2)); }
	inline String_t* get_secondArrayName_2() const { return ___secondArrayName_2; }
	inline String_t** get_address_of_secondArrayName_2() { return &___secondArrayName_2; }
	inline void set_secondArrayName_2(String_t* value)
	{
		___secondArrayName_2 = value;
		Il2CppCodeGenWriteBarrier((&___secondArrayName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDARRAYATTRIBUTE_T3735867164_H
#ifndef DISPLAYORDERATTRIBUTE_T2986454654_H
#define DISPLAYORDERATTRIBUTE_T2986454654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.DisplayOrderAttribute
struct  DisplayOrderAttribute_t2986454654  : public Attribute_t861562559
{
public:
	// System.Int32 HutongGames.PlayMaker.DisplayOrderAttribute::index
	int32_t ___index_0;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(DisplayOrderAttribute_t2986454654, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYORDERATTRIBUTE_T2986454654_H
#ifndef FSMBOOL_T163807967_H
#define FSMBOOL_T163807967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmBool
struct  FsmBool_t163807967  : public NamedVariable_t2808389578
{
public:
	// System.Boolean HutongGames.PlayMaker.FsmBool::value
	bool ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmBool_t163807967, ___value_6)); }
	inline bool get_value_6() const { return ___value_6; }
	inline bool* get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(bool value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMBOOL_T163807967_H
#ifndef FSMENUM_T2861764163_H
#define FSMENUM_T2861764163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEnum
struct  FsmEnum_t2861764163  : public NamedVariable_t2808389578
{
public:
	// System.String HutongGames.PlayMaker.FsmEnum::enumName
	String_t* ___enumName_6;
	// System.Int32 HutongGames.PlayMaker.FsmEnum::intValue
	int32_t ___intValue_7;
	// System.Enum HutongGames.PlayMaker.FsmEnum::value
	Enum_t4135868527 * ___value_8;
	// System.Int32 HutongGames.PlayMaker.FsmEnum::parsedIntValue
	int32_t ___parsedIntValue_9;
	// System.Type HutongGames.PlayMaker.FsmEnum::enumType
	Type_t * ___enumType_10;

public:
	inline static int32_t get_offset_of_enumName_6() { return static_cast<int32_t>(offsetof(FsmEnum_t2861764163, ___enumName_6)); }
	inline String_t* get_enumName_6() const { return ___enumName_6; }
	inline String_t** get_address_of_enumName_6() { return &___enumName_6; }
	inline void set_enumName_6(String_t* value)
	{
		___enumName_6 = value;
		Il2CppCodeGenWriteBarrier((&___enumName_6), value);
	}

	inline static int32_t get_offset_of_intValue_7() { return static_cast<int32_t>(offsetof(FsmEnum_t2861764163, ___intValue_7)); }
	inline int32_t get_intValue_7() const { return ___intValue_7; }
	inline int32_t* get_address_of_intValue_7() { return &___intValue_7; }
	inline void set_intValue_7(int32_t value)
	{
		___intValue_7 = value;
	}

	inline static int32_t get_offset_of_value_8() { return static_cast<int32_t>(offsetof(FsmEnum_t2861764163, ___value_8)); }
	inline Enum_t4135868527 * get_value_8() const { return ___value_8; }
	inline Enum_t4135868527 ** get_address_of_value_8() { return &___value_8; }
	inline void set_value_8(Enum_t4135868527 * value)
	{
		___value_8 = value;
		Il2CppCodeGenWriteBarrier((&___value_8), value);
	}

	inline static int32_t get_offset_of_parsedIntValue_9() { return static_cast<int32_t>(offsetof(FsmEnum_t2861764163, ___parsedIntValue_9)); }
	inline int32_t get_parsedIntValue_9() const { return ___parsedIntValue_9; }
	inline int32_t* get_address_of_parsedIntValue_9() { return &___parsedIntValue_9; }
	inline void set_parsedIntValue_9(int32_t value)
	{
		___parsedIntValue_9 = value;
	}

	inline static int32_t get_offset_of_enumType_10() { return static_cast<int32_t>(offsetof(FsmEnum_t2861764163, ___enumType_10)); }
	inline Type_t * get_enumType_10() const { return ___enumType_10; }
	inline Type_t ** get_address_of_enumType_10() { return &___enumType_10; }
	inline void set_enumType_10(Type_t * value)
	{
		___enumType_10 = value;
		Il2CppCodeGenWriteBarrier((&___enumType_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMENUM_T2861764163_H
#ifndef FSMFLOAT_T2883254149_H
#define FSMFLOAT_T2883254149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmFloat
struct  FsmFloat_t2883254149  : public NamedVariable_t2808389578
{
public:
	// System.Single HutongGames.PlayMaker.FsmFloat::value
	float ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmFloat_t2883254149, ___value_6)); }
	inline float get_value_6() const { return ___value_6; }
	inline float* get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(float value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMFLOAT_T2883254149_H
#ifndef FSMGAMEOBJECT_T3581898942_H
#define FSMGAMEOBJECT_T3581898942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmGameObject
struct  FsmGameObject_t3581898942  : public NamedVariable_t2808389578
{
public:
	// System.Action HutongGames.PlayMaker.FsmGameObject::OnChange
	Action_t1264377477 * ___OnChange_6;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmGameObject::value
	GameObject_t1113636619 * ___value_7;

public:
	inline static int32_t get_offset_of_OnChange_6() { return static_cast<int32_t>(offsetof(FsmGameObject_t3581898942, ___OnChange_6)); }
	inline Action_t1264377477 * get_OnChange_6() const { return ___OnChange_6; }
	inline Action_t1264377477 ** get_address_of_OnChange_6() { return &___OnChange_6; }
	inline void set_OnChange_6(Action_t1264377477 * value)
	{
		___OnChange_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnChange_6), value);
	}

	inline static int32_t get_offset_of_value_7() { return static_cast<int32_t>(offsetof(FsmGameObject_t3581898942, ___value_7)); }
	inline GameObject_t1113636619 * get_value_7() const { return ___value_7; }
	inline GameObject_t1113636619 ** get_address_of_value_7() { return &___value_7; }
	inline void set_value_7(GameObject_t1113636619 * value)
	{
		___value_7 = value;
		Il2CppCodeGenWriteBarrier((&___value_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMGAMEOBJECT_T3581898942_H
#ifndef FSMINT_T874273141_H
#define FSMINT_T874273141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmInt
struct  FsmInt_t874273141  : public NamedVariable_t2808389578
{
public:
	// System.Int32 HutongGames.PlayMaker.FsmInt::value
	int32_t ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmInt_t874273141, ___value_6)); }
	inline int32_t get_value_6() const { return ___value_6; }
	inline int32_t* get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(int32_t value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMINT_T874273141_H
#ifndef FSMOBJECT_T2606870197_H
#define FSMOBJECT_T2606870197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmObject
struct  FsmObject_t2606870197  : public NamedVariable_t2808389578
{
public:
	// System.String HutongGames.PlayMaker.FsmObject::typeName
	String_t* ___typeName_6;
	// UnityEngine.Object HutongGames.PlayMaker.FsmObject::value
	Object_t631007953 * ___value_7;
	// System.Type HutongGames.PlayMaker.FsmObject::objectType
	Type_t * ___objectType_8;

public:
	inline static int32_t get_offset_of_typeName_6() { return static_cast<int32_t>(offsetof(FsmObject_t2606870197, ___typeName_6)); }
	inline String_t* get_typeName_6() const { return ___typeName_6; }
	inline String_t** get_address_of_typeName_6() { return &___typeName_6; }
	inline void set_typeName_6(String_t* value)
	{
		___typeName_6 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_6), value);
	}

	inline static int32_t get_offset_of_value_7() { return static_cast<int32_t>(offsetof(FsmObject_t2606870197, ___value_7)); }
	inline Object_t631007953 * get_value_7() const { return ___value_7; }
	inline Object_t631007953 ** get_address_of_value_7() { return &___value_7; }
	inline void set_value_7(Object_t631007953 * value)
	{
		___value_7 = value;
		Il2CppCodeGenWriteBarrier((&___value_7), value);
	}

	inline static int32_t get_offset_of_objectType_8() { return static_cast<int32_t>(offsetof(FsmObject_t2606870197, ___objectType_8)); }
	inline Type_t * get_objectType_8() const { return ___objectType_8; }
	inline Type_t ** get_address_of_objectType_8() { return &___objectType_8; }
	inline void set_objectType_8(Type_t * value)
	{
		___objectType_8 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMOBJECT_T2606870197_H
#ifndef FSMSTRING_T1785915204_H
#define FSMSTRING_T1785915204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmString
struct  FsmString_t1785915204  : public NamedVariable_t2808389578
{
public:
	// System.String HutongGames.PlayMaker.FsmString::value
	String_t* ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmString_t1785915204, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTRING_T1785915204_H
#ifndef HASFLOATSLIDERATTRIBUTE_T228634986_H
#define HASFLOATSLIDERATTRIBUTE_T228634986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.HasFloatSliderAttribute
struct  HasFloatSliderAttribute_t228634986  : public Attribute_t861562559
{
public:
	// System.Single HutongGames.PlayMaker.HasFloatSliderAttribute::minValue
	float ___minValue_0;
	// System.Single HutongGames.PlayMaker.HasFloatSliderAttribute::maxValue
	float ___maxValue_1;

public:
	inline static int32_t get_offset_of_minValue_0() { return static_cast<int32_t>(offsetof(HasFloatSliderAttribute_t228634986, ___minValue_0)); }
	inline float get_minValue_0() const { return ___minValue_0; }
	inline float* get_address_of_minValue_0() { return &___minValue_0; }
	inline void set_minValue_0(float value)
	{
		___minValue_0 = value;
	}

	inline static int32_t get_offset_of_maxValue_1() { return static_cast<int32_t>(offsetof(HasFloatSliderAttribute_t228634986, ___maxValue_1)); }
	inline float get_maxValue_1() const { return ___maxValue_1; }
	inline float* get_address_of_maxValue_1() { return &___maxValue_1; }
	inline void set_maxValue_1(float value)
	{
		___maxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASFLOATSLIDERATTRIBUTE_T228634986_H
#ifndef HELPURLATTRIBUTE_T1897678690_H
#define HELPURLATTRIBUTE_T1897678690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.HelpUrlAttribute
struct  HelpUrlAttribute_t1897678690  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.HelpUrlAttribute::url
	String_t* ___url_0;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(HelpUrlAttribute_t1897678690, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPURLATTRIBUTE_T1897678690_H
#ifndef HIDEIFATTRIBUTE_T785818473_H
#define HIDEIFATTRIBUTE_T785818473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.HideIfAttribute
struct  HideIfAttribute_t785818473  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.HideIfAttribute::<Test>k__BackingField
	String_t* ___U3CTestU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTestU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HideIfAttribute_t785818473, ___U3CTestU3Ek__BackingField_0)); }
	inline String_t* get_U3CTestU3Ek__BackingField_0() const { return ___U3CTestU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTestU3Ek__BackingField_0() { return &___U3CTestU3Ek__BackingField_0; }
	inline void set_U3CTestU3Ek__BackingField_0(String_t* value)
	{
		___U3CTestU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEIFATTRIBUTE_T785818473_H
#ifndef HIDETYPEFILTER_T1596225627_H
#define HIDETYPEFILTER_T1596225627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.HideTypeFilter
struct  HideTypeFilter_t1596225627  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDETYPEFILTER_T1596225627_H
#ifndef MATCHELEMENTTYPEATTRIBUTE_T3380579236_H
#define MATCHELEMENTTYPEATTRIBUTE_T3380579236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.MatchElementTypeAttribute
struct  MatchElementTypeAttribute_t3380579236  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.MatchElementTypeAttribute::fieldName
	String_t* ___fieldName_0;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(MatchElementTypeAttribute_t3380579236, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHELEMENTTYPEATTRIBUTE_T3380579236_H
#ifndef MATCHFIELDTYPEATTRIBUTE_T2885974556_H
#define MATCHFIELDTYPEATTRIBUTE_T2885974556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.MatchFieldTypeAttribute
struct  MatchFieldTypeAttribute_t2885974556  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.MatchFieldTypeAttribute::fieldName
	String_t* ___fieldName_0;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(MatchFieldTypeAttribute_t2885974556, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHFIELDTYPEATTRIBUTE_T2885974556_H
#ifndef NOACTIONTARGETSATTRIBUTE_T3254466650_H
#define NOACTIONTARGETSATTRIBUTE_T3254466650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.NoActionTargetsAttribute
struct  NoActionTargetsAttribute_t3254466650  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOACTIONTARGETSATTRIBUTE_T3254466650_H
#ifndef NOTEATTRIBUTE_T316799825_H
#define NOTEATTRIBUTE_T316799825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.NoteAttribute
struct  NoteAttribute_t316799825  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.NoteAttribute::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(NoteAttribute_t316799825, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTEATTRIBUTE_T316799825_H
#ifndef OBJECTTYPEATTRIBUTE_T3116760320_H
#define OBJECTTYPEATTRIBUTE_T3116760320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ObjectTypeAttribute
struct  ObjectTypeAttribute_t3116760320  : public Attribute_t861562559
{
public:
	// System.Type HutongGames.PlayMaker.ObjectTypeAttribute::objectType
	Type_t * ___objectType_0;

public:
	inline static int32_t get_offset_of_objectType_0() { return static_cast<int32_t>(offsetof(ObjectTypeAttribute_t3116760320, ___objectType_0)); }
	inline Type_t * get_objectType_0() const { return ___objectType_0; }
	inline Type_t ** get_address_of_objectType_0() { return &___objectType_0; }
	inline void set_objectType_0(Type_t * value)
	{
		___objectType_0 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPEATTRIBUTE_T3116760320_H
#ifndef PREVIEWFIELDATTRIBUTE_T1055979593_H
#define PREVIEWFIELDATTRIBUTE_T1055979593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.PreviewFieldAttribute
struct  PreviewFieldAttribute_t1055979593  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.PreviewFieldAttribute::<MethodName>k__BackingField
	String_t* ___U3CMethodNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMethodNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PreviewFieldAttribute_t1055979593, ___U3CMethodNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CMethodNameU3Ek__BackingField_0() const { return ___U3CMethodNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMethodNameU3Ek__BackingField_0() { return &___U3CMethodNameU3Ek__BackingField_0; }
	inline void set_U3CMethodNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CMethodNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWFIELDATTRIBUTE_T1055979593_H
#ifndef READONLYATTRIBUTE_T1786548413_H
#define READONLYATTRIBUTE_T1786548413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ReadonlyAttribute
struct  ReadonlyAttribute_t1786548413  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYATTRIBUTE_T1786548413_H
#ifndef REQUIREDFIELDATTRIBUTE_T3920128169_H
#define REQUIREDFIELDATTRIBUTE_T3920128169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.RequiredFieldAttribute
struct  RequiredFieldAttribute_t3920128169  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDFIELDATTRIBUTE_T3920128169_H
#ifndef SETTINGSMENUITEMATTRIBUTE_T2376176270_H
#define SETTINGSMENUITEMATTRIBUTE_T2376176270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.SettingsMenuItemAttribute
struct  SettingsMenuItemAttribute_t2376176270  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.SettingsMenuItemAttribute::<MenuItem>k__BackingField
	String_t* ___U3CMenuItemU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMenuItemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SettingsMenuItemAttribute_t2376176270, ___U3CMenuItemU3Ek__BackingField_0)); }
	inline String_t* get_U3CMenuItemU3Ek__BackingField_0() const { return ___U3CMenuItemU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMenuItemU3Ek__BackingField_0() { return &___U3CMenuItemU3Ek__BackingField_0; }
	inline void set_U3CMenuItemU3Ek__BackingField_0(String_t* value)
	{
		___U3CMenuItemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuItemU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMENUITEMATTRIBUTE_T2376176270_H
#ifndef SETTINGSMENUITEMSTATEATTRIBUTE_T3930598301_H
#define SETTINGSMENUITEMSTATEATTRIBUTE_T3930598301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.SettingsMenuItemStateAttribute
struct  SettingsMenuItemStateAttribute_t3930598301  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.SettingsMenuItemStateAttribute::<MenuItem>k__BackingField
	String_t* ___U3CMenuItemU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CMenuItemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SettingsMenuItemStateAttribute_t3930598301, ___U3CMenuItemU3Ek__BackingField_0)); }
	inline String_t* get_U3CMenuItemU3Ek__BackingField_0() const { return ___U3CMenuItemU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMenuItemU3Ek__BackingField_0() { return &___U3CMenuItemU3Ek__BackingField_0; }
	inline void set_U3CMenuItemU3Ek__BackingField_0(String_t* value)
	{
		___U3CMenuItemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuItemU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMENUITEMSTATEATTRIBUTE_T3930598301_H
#ifndef TITLEATTRIBUTE_T196394849_H
#define TITLEATTRIBUTE_T196394849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TitleAttribute
struct  TitleAttribute_t196394849  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.TitleAttribute::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(TitleAttribute_t196394849, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TITLEATTRIBUTE_T196394849_H
#ifndef TOOLTIPATTRIBUTE_T2411168496_H
#define TOOLTIPATTRIBUTE_T2411168496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TooltipAttribute
struct  TooltipAttribute_t2411168496  : public Attribute_t861562559
{
public:
	// System.String HutongGames.PlayMaker.TooltipAttribute::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t2411168496, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPATTRIBUTE_T2411168496_H
#ifndef VARIABLETYPEFILTER_T3022214907_H
#define VARIABLETYPEFILTER_T3022214907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableTypeFilter
struct  VariableTypeFilter_t3022214907  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPEFILTER_T3022214907_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef ACTIONCATEGORY_T2600881856_H
#define ACTIONCATEGORY_T2600881856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ActionCategory
struct  ActionCategory_t2600881856 
{
public:
	// System.Int32 HutongGames.PlayMaker.ActionCategory::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ActionCategory_t2600881856, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONCATEGORY_T2600881856_H
#ifndef COLLISION2DTYPE_T450510585_H
#define COLLISION2DTYPE_T450510585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Collision2DType
struct  Collision2DType_t450510585 
{
public:
	// System.Int32 HutongGames.PlayMaker.Collision2DType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Collision2DType_t450510585, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISION2DTYPE_T450510585_H
#ifndef COLLISIONTYPE_T4059948830_H
#define COLLISIONTYPE_T4059948830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.CollisionType
struct  CollisionType_t4059948830 
{
public:
	// System.Int32 HutongGames.PlayMaker.CollisionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionType_t4059948830, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONTYPE_T4059948830_H
#ifndef COLORBLENDMODE_T3516528287_H
#define COLORBLENDMODE_T3516528287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ColorBlendMode
struct  ColorBlendMode_t3516528287 
{
public:
	// System.Int32 HutongGames.PlayMaker.ColorBlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorBlendMode_t3516528287, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLENDMODE_T3516528287_H
#ifndef FSMCOLOR_T1738900188_H
#define FSMCOLOR_T1738900188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmColor
struct  FsmColor_t1738900188  : public NamedVariable_t2808389578
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmColor::value
	Color_t2555686324  ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmColor_t1738900188, ___value_6)); }
	inline Color_t2555686324  get_value_6() const { return ___value_6; }
	inline Color_t2555686324 * get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(Color_t2555686324  value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMCOLOR_T1738900188_H
#ifndef FSMEVENTDATA_T1692568573_H
#define FSMEVENTDATA_T1692568573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEventData
struct  FsmEventData_t1692568573  : public RuntimeObject
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmEventData::SentByGameObject
	GameObject_t1113636619 * ___SentByGameObject_0;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmEventData::SentByFsm
	Fsm_t4127147824 * ___SentByFsm_1;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmEventData::SentByState
	FsmState_t4124398234 * ___SentByState_2;
	// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmEventData::SentByAction
	FsmStateAction_t3801304101 * ___SentByAction_3;
	// System.Boolean HutongGames.PlayMaker.FsmEventData::BoolData
	bool ___BoolData_4;
	// System.Int32 HutongGames.PlayMaker.FsmEventData::IntData
	int32_t ___IntData_5;
	// System.Single HutongGames.PlayMaker.FsmEventData::FloatData
	float ___FloatData_6;
	// UnityEngine.Vector2 HutongGames.PlayMaker.FsmEventData::Vector2Data
	Vector2_t2156229523  ___Vector2Data_7;
	// UnityEngine.Vector3 HutongGames.PlayMaker.FsmEventData::Vector3Data
	Vector3_t3722313464  ___Vector3Data_8;
	// System.String HutongGames.PlayMaker.FsmEventData::StringData
	String_t* ___StringData_9;
	// UnityEngine.Quaternion HutongGames.PlayMaker.FsmEventData::QuaternionData
	Quaternion_t2301928331  ___QuaternionData_10;
	// UnityEngine.Rect HutongGames.PlayMaker.FsmEventData::RectData
	Rect_t2360479859  ___RectData_11;
	// UnityEngine.Color HutongGames.PlayMaker.FsmEventData::ColorData
	Color_t2555686324  ___ColorData_12;
	// UnityEngine.Object HutongGames.PlayMaker.FsmEventData::ObjectData
	Object_t631007953 * ___ObjectData_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmEventData::GameObjectData
	GameObject_t1113636619 * ___GameObjectData_14;
	// UnityEngine.Material HutongGames.PlayMaker.FsmEventData::MaterialData
	Material_t340375123 * ___MaterialData_15;
	// UnityEngine.Texture HutongGames.PlayMaker.FsmEventData::TextureData
	Texture_t3661962703 * ___TextureData_16;

public:
	inline static int32_t get_offset_of_SentByGameObject_0() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByGameObject_0)); }
	inline GameObject_t1113636619 * get_SentByGameObject_0() const { return ___SentByGameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_SentByGameObject_0() { return &___SentByGameObject_0; }
	inline void set_SentByGameObject_0(GameObject_t1113636619 * value)
	{
		___SentByGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___SentByGameObject_0), value);
	}

	inline static int32_t get_offset_of_SentByFsm_1() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByFsm_1)); }
	inline Fsm_t4127147824 * get_SentByFsm_1() const { return ___SentByFsm_1; }
	inline Fsm_t4127147824 ** get_address_of_SentByFsm_1() { return &___SentByFsm_1; }
	inline void set_SentByFsm_1(Fsm_t4127147824 * value)
	{
		___SentByFsm_1 = value;
		Il2CppCodeGenWriteBarrier((&___SentByFsm_1), value);
	}

	inline static int32_t get_offset_of_SentByState_2() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByState_2)); }
	inline FsmState_t4124398234 * get_SentByState_2() const { return ___SentByState_2; }
	inline FsmState_t4124398234 ** get_address_of_SentByState_2() { return &___SentByState_2; }
	inline void set_SentByState_2(FsmState_t4124398234 * value)
	{
		___SentByState_2 = value;
		Il2CppCodeGenWriteBarrier((&___SentByState_2), value);
	}

	inline static int32_t get_offset_of_SentByAction_3() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___SentByAction_3)); }
	inline FsmStateAction_t3801304101 * get_SentByAction_3() const { return ___SentByAction_3; }
	inline FsmStateAction_t3801304101 ** get_address_of_SentByAction_3() { return &___SentByAction_3; }
	inline void set_SentByAction_3(FsmStateAction_t3801304101 * value)
	{
		___SentByAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___SentByAction_3), value);
	}

	inline static int32_t get_offset_of_BoolData_4() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___BoolData_4)); }
	inline bool get_BoolData_4() const { return ___BoolData_4; }
	inline bool* get_address_of_BoolData_4() { return &___BoolData_4; }
	inline void set_BoolData_4(bool value)
	{
		___BoolData_4 = value;
	}

	inline static int32_t get_offset_of_IntData_5() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___IntData_5)); }
	inline int32_t get_IntData_5() const { return ___IntData_5; }
	inline int32_t* get_address_of_IntData_5() { return &___IntData_5; }
	inline void set_IntData_5(int32_t value)
	{
		___IntData_5 = value;
	}

	inline static int32_t get_offset_of_FloatData_6() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___FloatData_6)); }
	inline float get_FloatData_6() const { return ___FloatData_6; }
	inline float* get_address_of_FloatData_6() { return &___FloatData_6; }
	inline void set_FloatData_6(float value)
	{
		___FloatData_6 = value;
	}

	inline static int32_t get_offset_of_Vector2Data_7() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___Vector2Data_7)); }
	inline Vector2_t2156229523  get_Vector2Data_7() const { return ___Vector2Data_7; }
	inline Vector2_t2156229523 * get_address_of_Vector2Data_7() { return &___Vector2Data_7; }
	inline void set_Vector2Data_7(Vector2_t2156229523  value)
	{
		___Vector2Data_7 = value;
	}

	inline static int32_t get_offset_of_Vector3Data_8() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___Vector3Data_8)); }
	inline Vector3_t3722313464  get_Vector3Data_8() const { return ___Vector3Data_8; }
	inline Vector3_t3722313464 * get_address_of_Vector3Data_8() { return &___Vector3Data_8; }
	inline void set_Vector3Data_8(Vector3_t3722313464  value)
	{
		___Vector3Data_8 = value;
	}

	inline static int32_t get_offset_of_StringData_9() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___StringData_9)); }
	inline String_t* get_StringData_9() const { return ___StringData_9; }
	inline String_t** get_address_of_StringData_9() { return &___StringData_9; }
	inline void set_StringData_9(String_t* value)
	{
		___StringData_9 = value;
		Il2CppCodeGenWriteBarrier((&___StringData_9), value);
	}

	inline static int32_t get_offset_of_QuaternionData_10() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___QuaternionData_10)); }
	inline Quaternion_t2301928331  get_QuaternionData_10() const { return ___QuaternionData_10; }
	inline Quaternion_t2301928331 * get_address_of_QuaternionData_10() { return &___QuaternionData_10; }
	inline void set_QuaternionData_10(Quaternion_t2301928331  value)
	{
		___QuaternionData_10 = value;
	}

	inline static int32_t get_offset_of_RectData_11() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___RectData_11)); }
	inline Rect_t2360479859  get_RectData_11() const { return ___RectData_11; }
	inline Rect_t2360479859 * get_address_of_RectData_11() { return &___RectData_11; }
	inline void set_RectData_11(Rect_t2360479859  value)
	{
		___RectData_11 = value;
	}

	inline static int32_t get_offset_of_ColorData_12() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___ColorData_12)); }
	inline Color_t2555686324  get_ColorData_12() const { return ___ColorData_12; }
	inline Color_t2555686324 * get_address_of_ColorData_12() { return &___ColorData_12; }
	inline void set_ColorData_12(Color_t2555686324  value)
	{
		___ColorData_12 = value;
	}

	inline static int32_t get_offset_of_ObjectData_13() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___ObjectData_13)); }
	inline Object_t631007953 * get_ObjectData_13() const { return ___ObjectData_13; }
	inline Object_t631007953 ** get_address_of_ObjectData_13() { return &___ObjectData_13; }
	inline void set_ObjectData_13(Object_t631007953 * value)
	{
		___ObjectData_13 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectData_13), value);
	}

	inline static int32_t get_offset_of_GameObjectData_14() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___GameObjectData_14)); }
	inline GameObject_t1113636619 * get_GameObjectData_14() const { return ___GameObjectData_14; }
	inline GameObject_t1113636619 ** get_address_of_GameObjectData_14() { return &___GameObjectData_14; }
	inline void set_GameObjectData_14(GameObject_t1113636619 * value)
	{
		___GameObjectData_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameObjectData_14), value);
	}

	inline static int32_t get_offset_of_MaterialData_15() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___MaterialData_15)); }
	inline Material_t340375123 * get_MaterialData_15() const { return ___MaterialData_15; }
	inline Material_t340375123 ** get_address_of_MaterialData_15() { return &___MaterialData_15; }
	inline void set_MaterialData_15(Material_t340375123 * value)
	{
		___MaterialData_15 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialData_15), value);
	}

	inline static int32_t get_offset_of_TextureData_16() { return static_cast<int32_t>(offsetof(FsmEventData_t1692568573, ___TextureData_16)); }
	inline Texture_t3661962703 * get_TextureData_16() const { return ___TextureData_16; }
	inline Texture_t3661962703 ** get_address_of_TextureData_16() { return &___TextureData_16; }
	inline void set_TextureData_16(Texture_t3661962703 * value)
	{
		___TextureData_16 = value;
		Il2CppCodeGenWriteBarrier((&___TextureData_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEVENTDATA_T1692568573_H
#ifndef EVENTTARGET_T3002913944_H
#define EVENTTARGET_T3002913944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEventTarget/EventTarget
struct  EventTarget_t3002913944 
{
public:
	// System.Int32 HutongGames.PlayMaker.FsmEventTarget/EventTarget::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventTarget_t3002913944, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTARGET_T3002913944_H
#ifndef FSMMATERIAL_T2057874452_H
#define FSMMATERIAL_T2057874452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmMaterial
struct  FsmMaterial_t2057874452  : public FsmObject_t2606870197
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMMATERIAL_T2057874452_H
#ifndef FSMQUATERNION_T4259038327_H
#define FSMQUATERNION_T4259038327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmQuaternion
struct  FsmQuaternion_t4259038327  : public NamedVariable_t2808389578
{
public:
	// UnityEngine.Quaternion HutongGames.PlayMaker.FsmQuaternion::value
	Quaternion_t2301928331  ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmQuaternion_t4259038327, ___value_6)); }
	inline Quaternion_t2301928331  get_value_6() const { return ___value_6; }
	inline Quaternion_t2301928331 * get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(Quaternion_t2301928331  value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMQUATERNION_T4259038327_H
#ifndef FSMRECT_T3649179877_H
#define FSMRECT_T3649179877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmRect
struct  FsmRect_t3649179877  : public NamedVariable_t2808389578
{
public:
	// UnityEngine.Rect HutongGames.PlayMaker.FsmRect::value
	Rect_t2360479859  ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmRect_t3649179877, ___value_6)); }
	inline Rect_t2360479859  get_value_6() const { return ___value_6; }
	inline Rect_t2360479859 * get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(Rect_t2360479859  value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMRECT_T3649179877_H
#ifndef FSMTEXTURE_T1738787045_H
#define FSMTEXTURE_T1738787045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTexture
struct  FsmTexture_t1738787045  : public FsmObject_t2606870197
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMTEXTURE_T1738787045_H
#ifndef FSMVECTOR2_T2965096677_H
#define FSMVECTOR2_T2965096677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVector2
struct  FsmVector2_t2965096677  : public NamedVariable_t2808389578
{
public:
	// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVector2::value
	Vector2_t2156229523  ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmVector2_t2965096677, ___value_6)); }
	inline Vector2_t2156229523  get_value_6() const { return ___value_6; }
	inline Vector2_t2156229523 * get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(Vector2_t2156229523  value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMVECTOR2_T2965096677_H
#ifndef FSMVECTOR3_T626444517_H
#define FSMVECTOR3_T626444517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVector3
struct  FsmVector3_t626444517  : public NamedVariable_t2808389578
{
public:
	// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVector3::value
	Vector3_t3722313464  ___value_6;

public:
	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(FsmVector3_t626444517, ___value_6)); }
	inline Vector3_t3722313464  get_value_6() const { return ___value_6; }
	inline Vector3_t3722313464 * get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(Vector3_t3722313464  value)
	{
		___value_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMVECTOR3_T626444517_H
#ifndef INTERPOLATIONTYPE_T358821704_H
#define INTERPOLATIONTYPE_T358821704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.InterpolationType
struct  InterpolationType_t358821704 
{
public:
	// System.Int32 HutongGames.PlayMaker.InterpolationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InterpolationType_t358821704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONTYPE_T358821704_H
#ifndef LAYOUTOPTIONTYPE_T782463089_H
#define LAYOUTOPTIONTYPE_T782463089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.LayoutOption/LayoutOptionType
struct  LayoutOptionType_t782463089 
{
public:
	// System.Int32 HutongGames.PlayMaker.LayoutOption/LayoutOptionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LayoutOptionType_t782463089, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTOPTIONTYPE_T782463089_H
#ifndef LOGLEVEL_T183558972_H
#define LOGLEVEL_T183558972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.LogLevel
struct  LogLevel_t183558972 
{
public:
	// System.Int32 HutongGames.PlayMaker.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t183558972, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T183558972_H
#ifndef MOUSEBUTTON_T2824504455_H
#define MOUSEBUTTON_T2824504455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.MouseButton
struct  MouseButton_t2824504455 
{
public:
	// System.Int32 HutongGames.PlayMaker.MouseButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseButton_t2824504455, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_T2824504455_H
#ifndef MOUSEEVENTTYPE_T1688122298_H
#define MOUSEEVENTTYPE_T1688122298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.MouseEventType
struct  MouseEventType_t1688122298 
{
public:
	// System.Int32 HutongGames.PlayMaker.MouseEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseEventType_t1688122298, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEEVENTTYPE_T1688122298_H
#ifndef NONE_T4017744907_H
#define NONE_T4017744907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.None
struct  None_t4017744907 
{
public:
	// System.Int32 HutongGames.PlayMaker.None::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(None_t4017744907, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONE_T4017744907_H
#ifndef OWNERDEFAULTOPTION_T4041051211_H
#define OWNERDEFAULTOPTION_T4041051211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.OwnerDefaultOption
struct  OwnerDefaultOption_t4041051211 
{
public:
	// System.Int32 HutongGames.PlayMaker.OwnerDefaultOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OwnerDefaultOption_t4041051211, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWNERDEFAULTOPTION_T4041051211_H
#ifndef PARAMDATATYPE_T3128825015_H
#define PARAMDATATYPE_T3128825015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ParamDataType
struct  ParamDataType_t3128825015 
{
public:
	// System.Int32 HutongGames.PlayMaker.ParamDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParamDataType_t3128825015, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMDATATYPE_T3128825015_H
#ifndef TRIGGER2DTYPE_T3885175869_H
#define TRIGGER2DTYPE_T3885175869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Trigger2DType
struct  Trigger2DType_t3885175869 
{
public:
	// System.Int32 HutongGames.PlayMaker.Trigger2DType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger2DType_t3885175869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER2DTYPE_T3885175869_H
#ifndef TRIGGERTYPE_T1545777084_H
#define TRIGGERTYPE_T1545777084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.TriggerType
struct  TriggerType_t1545777084 
{
public:
	// System.Int32 HutongGames.PlayMaker.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t1545777084, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T1545777084_H
#ifndef UIHINT_T1096202973_H
#define UIHINT_T1096202973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UIHint
struct  UIHint_t1096202973 
{
public:
	// System.Int32 HutongGames.PlayMaker.UIHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UIHint_t1096202973, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIHINT_T1096202973_H
#ifndef VARIABLETYPE_T1723760892_H
#define VARIABLETYPE_T1723760892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableType
struct  VariableType_t1723760892 
{
public:
	// System.Int32 HutongGames.PlayMaker.VariableType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VariableType_t1723760892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPE_T1723760892_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ARRAYEDITORATTRIBUTE_T4006704688_H
#define ARRAYEDITORATTRIBUTE_T4006704688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.ArrayEditorAttribute
struct  ArrayEditorAttribute_t4006704688  : public Attribute_t861562559
{
public:
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.ArrayEditorAttribute::variableType
	int32_t ___variableType_0;
	// System.Type HutongGames.PlayMaker.ArrayEditorAttribute::objectType
	Type_t * ___objectType_1;
	// System.String HutongGames.PlayMaker.ArrayEditorAttribute::elementName
	String_t* ___elementName_2;
	// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::fixedSize
	int32_t ___fixedSize_3;
	// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::maxSize
	int32_t ___maxSize_4;
	// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::minSize
	int32_t ___minSize_5;

public:
	inline static int32_t get_offset_of_variableType_0() { return static_cast<int32_t>(offsetof(ArrayEditorAttribute_t4006704688, ___variableType_0)); }
	inline int32_t get_variableType_0() const { return ___variableType_0; }
	inline int32_t* get_address_of_variableType_0() { return &___variableType_0; }
	inline void set_variableType_0(int32_t value)
	{
		___variableType_0 = value;
	}

	inline static int32_t get_offset_of_objectType_1() { return static_cast<int32_t>(offsetof(ArrayEditorAttribute_t4006704688, ___objectType_1)); }
	inline Type_t * get_objectType_1() const { return ___objectType_1; }
	inline Type_t ** get_address_of_objectType_1() { return &___objectType_1; }
	inline void set_objectType_1(Type_t * value)
	{
		___objectType_1 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_1), value);
	}

	inline static int32_t get_offset_of_elementName_2() { return static_cast<int32_t>(offsetof(ArrayEditorAttribute_t4006704688, ___elementName_2)); }
	inline String_t* get_elementName_2() const { return ___elementName_2; }
	inline String_t** get_address_of_elementName_2() { return &___elementName_2; }
	inline void set_elementName_2(String_t* value)
	{
		___elementName_2 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_2), value);
	}

	inline static int32_t get_offset_of_fixedSize_3() { return static_cast<int32_t>(offsetof(ArrayEditorAttribute_t4006704688, ___fixedSize_3)); }
	inline int32_t get_fixedSize_3() const { return ___fixedSize_3; }
	inline int32_t* get_address_of_fixedSize_3() { return &___fixedSize_3; }
	inline void set_fixedSize_3(int32_t value)
	{
		___fixedSize_3 = value;
	}

	inline static int32_t get_offset_of_maxSize_4() { return static_cast<int32_t>(offsetof(ArrayEditorAttribute_t4006704688, ___maxSize_4)); }
	inline int32_t get_maxSize_4() const { return ___maxSize_4; }
	inline int32_t* get_address_of_maxSize_4() { return &___maxSize_4; }
	inline void set_maxSize_4(int32_t value)
	{
		___maxSize_4 = value;
	}

	inline static int32_t get_offset_of_minSize_5() { return static_cast<int32_t>(offsetof(ArrayEditorAttribute_t4006704688, ___minSize_5)); }
	inline int32_t get_minSize_5() const { return ___minSize_5; }
	inline int32_t* get_address_of_minSize_5() { return &___minSize_5; }
	inline void set_minSize_5(int32_t value)
	{
		___minSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYEDITORATTRIBUTE_T4006704688_H
#ifndef EVENTTARGETATTRIBUTE_T1040314792_H
#define EVENTTARGETATTRIBUTE_T1040314792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.EventTargetAttribute
struct  EventTargetAttribute_t1040314792  : public Attribute_t861562559
{
public:
	// HutongGames.PlayMaker.FsmEventTarget/EventTarget HutongGames.PlayMaker.EventTargetAttribute::target
	int32_t ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(EventTargetAttribute_t1040314792, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTARGETATTRIBUTE_T1040314792_H
#ifndef FSMARRAY_T1756862219_H
#define FSMARRAY_T1756862219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmArray
struct  FsmArray_t1756862219  : public NamedVariable_t2808389578
{
public:
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::type
	int32_t ___type_6;
	// System.String HutongGames.PlayMaker.FsmArray::objectTypeName
	String_t* ___objectTypeName_7;
	// System.Type HutongGames.PlayMaker.FsmArray::objectType
	Type_t * ___objectType_8;
	// System.Single[] HutongGames.PlayMaker.FsmArray::floatValues
	SingleU5BU5D_t1444911251* ___floatValues_9;
	// System.Int32[] HutongGames.PlayMaker.FsmArray::intValues
	Int32U5BU5D_t385246372* ___intValues_10;
	// System.Boolean[] HutongGames.PlayMaker.FsmArray::boolValues
	BooleanU5BU5D_t2897418192* ___boolValues_11;
	// System.String[] HutongGames.PlayMaker.FsmArray::stringValues
	StringU5BU5D_t1281789340* ___stringValues_12;
	// UnityEngine.Vector4[] HutongGames.PlayMaker.FsmArray::vector4Values
	Vector4U5BU5D_t934056436* ___vector4Values_13;
	// UnityEngine.Object[] HutongGames.PlayMaker.FsmArray::objectReferences
	ObjectU5BU5D_t1417781964* ___objectReferences_14;
	// System.Array HutongGames.PlayMaker.FsmArray::sourceArray
	RuntimeArray * ___sourceArray_15;
	// System.Object[] HutongGames.PlayMaker.FsmArray::values
	ObjectU5BU5D_t2843939325* ___values_16;

public:
	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___type_6)); }
	inline int32_t get_type_6() const { return ___type_6; }
	inline int32_t* get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(int32_t value)
	{
		___type_6 = value;
	}

	inline static int32_t get_offset_of_objectTypeName_7() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___objectTypeName_7)); }
	inline String_t* get_objectTypeName_7() const { return ___objectTypeName_7; }
	inline String_t** get_address_of_objectTypeName_7() { return &___objectTypeName_7; }
	inline void set_objectTypeName_7(String_t* value)
	{
		___objectTypeName_7 = value;
		Il2CppCodeGenWriteBarrier((&___objectTypeName_7), value);
	}

	inline static int32_t get_offset_of_objectType_8() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___objectType_8)); }
	inline Type_t * get_objectType_8() const { return ___objectType_8; }
	inline Type_t ** get_address_of_objectType_8() { return &___objectType_8; }
	inline void set_objectType_8(Type_t * value)
	{
		___objectType_8 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_8), value);
	}

	inline static int32_t get_offset_of_floatValues_9() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___floatValues_9)); }
	inline SingleU5BU5D_t1444911251* get_floatValues_9() const { return ___floatValues_9; }
	inline SingleU5BU5D_t1444911251** get_address_of_floatValues_9() { return &___floatValues_9; }
	inline void set_floatValues_9(SingleU5BU5D_t1444911251* value)
	{
		___floatValues_9 = value;
		Il2CppCodeGenWriteBarrier((&___floatValues_9), value);
	}

	inline static int32_t get_offset_of_intValues_10() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___intValues_10)); }
	inline Int32U5BU5D_t385246372* get_intValues_10() const { return ___intValues_10; }
	inline Int32U5BU5D_t385246372** get_address_of_intValues_10() { return &___intValues_10; }
	inline void set_intValues_10(Int32U5BU5D_t385246372* value)
	{
		___intValues_10 = value;
		Il2CppCodeGenWriteBarrier((&___intValues_10), value);
	}

	inline static int32_t get_offset_of_boolValues_11() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___boolValues_11)); }
	inline BooleanU5BU5D_t2897418192* get_boolValues_11() const { return ___boolValues_11; }
	inline BooleanU5BU5D_t2897418192** get_address_of_boolValues_11() { return &___boolValues_11; }
	inline void set_boolValues_11(BooleanU5BU5D_t2897418192* value)
	{
		___boolValues_11 = value;
		Il2CppCodeGenWriteBarrier((&___boolValues_11), value);
	}

	inline static int32_t get_offset_of_stringValues_12() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___stringValues_12)); }
	inline StringU5BU5D_t1281789340* get_stringValues_12() const { return ___stringValues_12; }
	inline StringU5BU5D_t1281789340** get_address_of_stringValues_12() { return &___stringValues_12; }
	inline void set_stringValues_12(StringU5BU5D_t1281789340* value)
	{
		___stringValues_12 = value;
		Il2CppCodeGenWriteBarrier((&___stringValues_12), value);
	}

	inline static int32_t get_offset_of_vector4Values_13() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___vector4Values_13)); }
	inline Vector4U5BU5D_t934056436* get_vector4Values_13() const { return ___vector4Values_13; }
	inline Vector4U5BU5D_t934056436** get_address_of_vector4Values_13() { return &___vector4Values_13; }
	inline void set_vector4Values_13(Vector4U5BU5D_t934056436* value)
	{
		___vector4Values_13 = value;
		Il2CppCodeGenWriteBarrier((&___vector4Values_13), value);
	}

	inline static int32_t get_offset_of_objectReferences_14() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___objectReferences_14)); }
	inline ObjectU5BU5D_t1417781964* get_objectReferences_14() const { return ___objectReferences_14; }
	inline ObjectU5BU5D_t1417781964** get_address_of_objectReferences_14() { return &___objectReferences_14; }
	inline void set_objectReferences_14(ObjectU5BU5D_t1417781964* value)
	{
		___objectReferences_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectReferences_14), value);
	}

	inline static int32_t get_offset_of_sourceArray_15() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___sourceArray_15)); }
	inline RuntimeArray * get_sourceArray_15() const { return ___sourceArray_15; }
	inline RuntimeArray ** get_address_of_sourceArray_15() { return &___sourceArray_15; }
	inline void set_sourceArray_15(RuntimeArray * value)
	{
		___sourceArray_15 = value;
		Il2CppCodeGenWriteBarrier((&___sourceArray_15), value);
	}

	inline static int32_t get_offset_of_values_16() { return static_cast<int32_t>(offsetof(FsmArray_t1756862219, ___values_16)); }
	inline ObjectU5BU5D_t2843939325* get_values_16() const { return ___values_16; }
	inline ObjectU5BU5D_t2843939325** get_address_of_values_16() { return &___values_16; }
	inline void set_values_16(ObjectU5BU5D_t2843939325* value)
	{
		___values_16 = value;
		Il2CppCodeGenWriteBarrier((&___values_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMARRAY_T1756862219_H
#ifndef FSMEVENTTARGET_T919699796_H
#define FSMEVENTTARGET_T919699796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmEventTarget
struct  FsmEventTarget_t919699796  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.FsmEventTarget/EventTarget HutongGames.PlayMaker.FsmEventTarget::target
	int32_t ___target_1;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmEventTarget::excludeSelf
	FsmBool_t163807967 * ___excludeSelf_2;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.FsmEventTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_3;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmEventTarget::fsmName
	FsmString_t1785915204 * ___fsmName_4;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmEventTarget::sendToChildren
	FsmBool_t163807967 * ___sendToChildren_5;
	// PlayMakerFSM HutongGames.PlayMaker.FsmEventTarget::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_6;

public:
	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796, ___target_1)); }
	inline int32_t get_target_1() const { return ___target_1; }
	inline int32_t* get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(int32_t value)
	{
		___target_1 = value;
	}

	inline static int32_t get_offset_of_excludeSelf_2() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796, ___excludeSelf_2)); }
	inline FsmBool_t163807967 * get_excludeSelf_2() const { return ___excludeSelf_2; }
	inline FsmBool_t163807967 ** get_address_of_excludeSelf_2() { return &___excludeSelf_2; }
	inline void set_excludeSelf_2(FsmBool_t163807967 * value)
	{
		___excludeSelf_2 = value;
		Il2CppCodeGenWriteBarrier((&___excludeSelf_2), value);
	}

	inline static int32_t get_offset_of_gameObject_3() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796, ___gameObject_3)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_3() const { return ___gameObject_3; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_3() { return &___gameObject_3; }
	inline void set_gameObject_3(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_3), value);
	}

	inline static int32_t get_offset_of_fsmName_4() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796, ___fsmName_4)); }
	inline FsmString_t1785915204 * get_fsmName_4() const { return ___fsmName_4; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_4() { return &___fsmName_4; }
	inline void set_fsmName_4(FsmString_t1785915204 * value)
	{
		___fsmName_4 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_4), value);
	}

	inline static int32_t get_offset_of_sendToChildren_5() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796, ___sendToChildren_5)); }
	inline FsmBool_t163807967 * get_sendToChildren_5() const { return ___sendToChildren_5; }
	inline FsmBool_t163807967 ** get_address_of_sendToChildren_5() { return &___sendToChildren_5; }
	inline void set_sendToChildren_5(FsmBool_t163807967 * value)
	{
		___sendToChildren_5 = value;
		Il2CppCodeGenWriteBarrier((&___sendToChildren_5), value);
	}

	inline static int32_t get_offset_of_fsmComponent_6() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796, ___fsmComponent_6)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_6() const { return ___fsmComponent_6; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_6() { return &___fsmComponent_6; }
	inline void set_fsmComponent_6(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_6), value);
	}
};

struct FsmEventTarget_t919699796_StaticFields
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmEventTarget::self
	FsmEventTarget_t919699796 * ___self_0;

public:
	inline static int32_t get_offset_of_self_0() { return static_cast<int32_t>(offsetof(FsmEventTarget_t919699796_StaticFields, ___self_0)); }
	inline FsmEventTarget_t919699796 * get_self_0() const { return ___self_0; }
	inline FsmEventTarget_t919699796 ** get_address_of_self_0() { return &___self_0; }
	inline void set_self_0(FsmEventTarget_t919699796 * value)
	{
		___self_0 = value;
		Il2CppCodeGenWriteBarrier((&___self_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMEVENTTARGET_T919699796_H
#ifndef FSMOWNERDEFAULT_T3590610434_H
#define FSMOWNERDEFAULT_T3590610434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmOwnerDefault
struct  FsmOwnerDefault_t3590610434  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.OwnerDefaultOption HutongGames.PlayMaker.FsmOwnerDefault::ownerOption
	int32_t ___ownerOption_0;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmOwnerDefault::gameObject
	FsmGameObject_t3581898942 * ___gameObject_1;

public:
	inline static int32_t get_offset_of_ownerOption_0() { return static_cast<int32_t>(offsetof(FsmOwnerDefault_t3590610434, ___ownerOption_0)); }
	inline int32_t get_ownerOption_0() const { return ___ownerOption_0; }
	inline int32_t* get_address_of_ownerOption_0() { return &___ownerOption_0; }
	inline void set_ownerOption_0(int32_t value)
	{
		___ownerOption_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(FsmOwnerDefault_t3590610434, ___gameObject_1)); }
	inline FsmGameObject_t3581898942 * get_gameObject_1() const { return ___gameObject_1; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(FsmGameObject_t3581898942 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMOWNERDEFAULT_T3590610434_H
#ifndef FSMVAR_T2413660594_H
#define FSMVAR_T2413660594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVar
struct  FsmVar_t2413660594  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmVar::variableName
	String_t* ___variableName_0;
	// System.String HutongGames.PlayMaker.FsmVar::objectType
	String_t* ___objectType_1;
	// System.Boolean HutongGames.PlayMaker.FsmVar::useVariable
	bool ___useVariable_2;
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVar::namedVar
	NamedVariable_t2808389578 * ___namedVar_3;
	// System.Type HutongGames.PlayMaker.FsmVar::namedVarType
	Type_t * ___namedVarType_4;
	// System.Type HutongGames.PlayMaker.FsmVar::enumType
	Type_t * ___enumType_5;
	// System.Enum HutongGames.PlayMaker.FsmVar::enumValue
	Enum_t4135868527 * ___enumValue_6;
	// System.Type HutongGames.PlayMaker.FsmVar::_objectType
	Type_t * ____objectType_7;
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVar::type
	int32_t ___type_8;
	// System.Single HutongGames.PlayMaker.FsmVar::floatValue
	float ___floatValue_9;
	// System.Int32 HutongGames.PlayMaker.FsmVar::intValue
	int32_t ___intValue_10;
	// System.Boolean HutongGames.PlayMaker.FsmVar::boolValue
	bool ___boolValue_11;
	// System.String HutongGames.PlayMaker.FsmVar::stringValue
	String_t* ___stringValue_12;
	// UnityEngine.Vector4 HutongGames.PlayMaker.FsmVar::vector4Value
	Vector4_t3319028937  ___vector4Value_13;
	// UnityEngine.Object HutongGames.PlayMaker.FsmVar::objectReference
	Object_t631007953 * ___objectReference_14;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.FsmVar::arrayValue
	FsmArray_t1756862219 * ___arrayValue_15;
	// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVar::vector2
	Vector2_t2156229523  ___vector2_16;
	// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVar::vector3
	Vector3_t3722313464  ___vector3_17;
	// UnityEngine.Rect HutongGames.PlayMaker.FsmVar::rect
	Rect_t2360479859  ___rect_18;

public:
	inline static int32_t get_offset_of_variableName_0() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___variableName_0)); }
	inline String_t* get_variableName_0() const { return ___variableName_0; }
	inline String_t** get_address_of_variableName_0() { return &___variableName_0; }
	inline void set_variableName_0(String_t* value)
	{
		___variableName_0 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_0), value);
	}

	inline static int32_t get_offset_of_objectType_1() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___objectType_1)); }
	inline String_t* get_objectType_1() const { return ___objectType_1; }
	inline String_t** get_address_of_objectType_1() { return &___objectType_1; }
	inline void set_objectType_1(String_t* value)
	{
		___objectType_1 = value;
		Il2CppCodeGenWriteBarrier((&___objectType_1), value);
	}

	inline static int32_t get_offset_of_useVariable_2() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___useVariable_2)); }
	inline bool get_useVariable_2() const { return ___useVariable_2; }
	inline bool* get_address_of_useVariable_2() { return &___useVariable_2; }
	inline void set_useVariable_2(bool value)
	{
		___useVariable_2 = value;
	}

	inline static int32_t get_offset_of_namedVar_3() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___namedVar_3)); }
	inline NamedVariable_t2808389578 * get_namedVar_3() const { return ___namedVar_3; }
	inline NamedVariable_t2808389578 ** get_address_of_namedVar_3() { return &___namedVar_3; }
	inline void set_namedVar_3(NamedVariable_t2808389578 * value)
	{
		___namedVar_3 = value;
		Il2CppCodeGenWriteBarrier((&___namedVar_3), value);
	}

	inline static int32_t get_offset_of_namedVarType_4() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___namedVarType_4)); }
	inline Type_t * get_namedVarType_4() const { return ___namedVarType_4; }
	inline Type_t ** get_address_of_namedVarType_4() { return &___namedVarType_4; }
	inline void set_namedVarType_4(Type_t * value)
	{
		___namedVarType_4 = value;
		Il2CppCodeGenWriteBarrier((&___namedVarType_4), value);
	}

	inline static int32_t get_offset_of_enumType_5() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___enumType_5)); }
	inline Type_t * get_enumType_5() const { return ___enumType_5; }
	inline Type_t ** get_address_of_enumType_5() { return &___enumType_5; }
	inline void set_enumType_5(Type_t * value)
	{
		___enumType_5 = value;
		Il2CppCodeGenWriteBarrier((&___enumType_5), value);
	}

	inline static int32_t get_offset_of_enumValue_6() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___enumValue_6)); }
	inline Enum_t4135868527 * get_enumValue_6() const { return ___enumValue_6; }
	inline Enum_t4135868527 ** get_address_of_enumValue_6() { return &___enumValue_6; }
	inline void set_enumValue_6(Enum_t4135868527 * value)
	{
		___enumValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___enumValue_6), value);
	}

	inline static int32_t get_offset_of__objectType_7() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ____objectType_7)); }
	inline Type_t * get__objectType_7() const { return ____objectType_7; }
	inline Type_t ** get_address_of__objectType_7() { return &____objectType_7; }
	inline void set__objectType_7(Type_t * value)
	{
		____objectType_7 = value;
		Il2CppCodeGenWriteBarrier((&____objectType_7), value);
	}

	inline static int32_t get_offset_of_type_8() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___type_8)); }
	inline int32_t get_type_8() const { return ___type_8; }
	inline int32_t* get_address_of_type_8() { return &___type_8; }
	inline void set_type_8(int32_t value)
	{
		___type_8 = value;
	}

	inline static int32_t get_offset_of_floatValue_9() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___floatValue_9)); }
	inline float get_floatValue_9() const { return ___floatValue_9; }
	inline float* get_address_of_floatValue_9() { return &___floatValue_9; }
	inline void set_floatValue_9(float value)
	{
		___floatValue_9 = value;
	}

	inline static int32_t get_offset_of_intValue_10() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___intValue_10)); }
	inline int32_t get_intValue_10() const { return ___intValue_10; }
	inline int32_t* get_address_of_intValue_10() { return &___intValue_10; }
	inline void set_intValue_10(int32_t value)
	{
		___intValue_10 = value;
	}

	inline static int32_t get_offset_of_boolValue_11() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___boolValue_11)); }
	inline bool get_boolValue_11() const { return ___boolValue_11; }
	inline bool* get_address_of_boolValue_11() { return &___boolValue_11; }
	inline void set_boolValue_11(bool value)
	{
		___boolValue_11 = value;
	}

	inline static int32_t get_offset_of_stringValue_12() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___stringValue_12)); }
	inline String_t* get_stringValue_12() const { return ___stringValue_12; }
	inline String_t** get_address_of_stringValue_12() { return &___stringValue_12; }
	inline void set_stringValue_12(String_t* value)
	{
		___stringValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_12), value);
	}

	inline static int32_t get_offset_of_vector4Value_13() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___vector4Value_13)); }
	inline Vector4_t3319028937  get_vector4Value_13() const { return ___vector4Value_13; }
	inline Vector4_t3319028937 * get_address_of_vector4Value_13() { return &___vector4Value_13; }
	inline void set_vector4Value_13(Vector4_t3319028937  value)
	{
		___vector4Value_13 = value;
	}

	inline static int32_t get_offset_of_objectReference_14() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___objectReference_14)); }
	inline Object_t631007953 * get_objectReference_14() const { return ___objectReference_14; }
	inline Object_t631007953 ** get_address_of_objectReference_14() { return &___objectReference_14; }
	inline void set_objectReference_14(Object_t631007953 * value)
	{
		___objectReference_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectReference_14), value);
	}

	inline static int32_t get_offset_of_arrayValue_15() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___arrayValue_15)); }
	inline FsmArray_t1756862219 * get_arrayValue_15() const { return ___arrayValue_15; }
	inline FsmArray_t1756862219 ** get_address_of_arrayValue_15() { return &___arrayValue_15; }
	inline void set_arrayValue_15(FsmArray_t1756862219 * value)
	{
		___arrayValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___arrayValue_15), value);
	}

	inline static int32_t get_offset_of_vector2_16() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___vector2_16)); }
	inline Vector2_t2156229523  get_vector2_16() const { return ___vector2_16; }
	inline Vector2_t2156229523 * get_address_of_vector2_16() { return &___vector2_16; }
	inline void set_vector2_16(Vector2_t2156229523  value)
	{
		___vector2_16 = value;
	}

	inline static int32_t get_offset_of_vector3_17() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___vector3_17)); }
	inline Vector3_t3722313464  get_vector3_17() const { return ___vector3_17; }
	inline Vector3_t3722313464 * get_address_of_vector3_17() { return &___vector3_17; }
	inline void set_vector3_17(Vector3_t3722313464  value)
	{
		___vector3_17 = value;
	}

	inline static int32_t get_offset_of_rect_18() { return static_cast<int32_t>(offsetof(FsmVar_t2413660594, ___rect_18)); }
	inline Rect_t2360479859  get_rect_18() const { return ___rect_18; }
	inline Rect_t2360479859 * get_address_of_rect_18() { return &___rect_18; }
	inline void set_rect_18(Rect_t2360479859  value)
	{
		___rect_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMVAR_T2413660594_H
#ifndef LAYOUTOPTION_T3011926197_H
#define LAYOUTOPTION_T3011926197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.LayoutOption
struct  LayoutOption_t3011926197  : public RuntimeObject
{
public:
	// HutongGames.PlayMaker.LayoutOption/LayoutOptionType HutongGames.PlayMaker.LayoutOption::option
	int32_t ___option_0;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.LayoutOption::floatParam
	FsmFloat_t2883254149 * ___floatParam_1;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.LayoutOption::boolParam
	FsmBool_t163807967 * ___boolParam_2;

public:
	inline static int32_t get_offset_of_option_0() { return static_cast<int32_t>(offsetof(LayoutOption_t3011926197, ___option_0)); }
	inline int32_t get_option_0() const { return ___option_0; }
	inline int32_t* get_address_of_option_0() { return &___option_0; }
	inline void set_option_0(int32_t value)
	{
		___option_0 = value;
	}

	inline static int32_t get_offset_of_floatParam_1() { return static_cast<int32_t>(offsetof(LayoutOption_t3011926197, ___floatParam_1)); }
	inline FsmFloat_t2883254149 * get_floatParam_1() const { return ___floatParam_1; }
	inline FsmFloat_t2883254149 ** get_address_of_floatParam_1() { return &___floatParam_1; }
	inline void set_floatParam_1(FsmFloat_t2883254149 * value)
	{
		___floatParam_1 = value;
		Il2CppCodeGenWriteBarrier((&___floatParam_1), value);
	}

	inline static int32_t get_offset_of_boolParam_2() { return static_cast<int32_t>(offsetof(LayoutOption_t3011926197, ___boolParam_2)); }
	inline FsmBool_t163807967 * get_boolParam_2() const { return ___boolParam_2; }
	inline FsmBool_t163807967 ** get_address_of_boolParam_2() { return &___boolParam_2; }
	inline void set_boolParam_2(FsmBool_t163807967 * value)
	{
		___boolParam_2 = value;
		Il2CppCodeGenWriteBarrier((&___boolParam_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTOPTION_T3011926197_H
#ifndef UIHINTATTRIBUTE_T2334957252_H
#define UIHINTATTRIBUTE_T2334957252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.UIHintAttribute
struct  UIHintAttribute_t2334957252  : public Attribute_t861562559
{
public:
	// HutongGames.PlayMaker.UIHint HutongGames.PlayMaker.UIHintAttribute::hint
	int32_t ___hint_0;

public:
	inline static int32_t get_offset_of_hint_0() { return static_cast<int32_t>(offsetof(UIHintAttribute_t2334957252, ___hint_0)); }
	inline int32_t get_hint_0() const { return ___hint_0; }
	inline int32_t* get_address_of_hint_0() { return &___hint_0; }
	inline void set_hint_0(int32_t value)
	{
		___hint_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIHINTATTRIBUTE_T2334957252_H
#ifndef VARIABLETYPEATTRIBUTE_T3556842571_H
#define VARIABLETYPEATTRIBUTE_T3556842571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.VariableTypeAttribute
struct  VariableTypeAttribute_t3556842571  : public Attribute_t861562559
{
public:
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.VariableTypeAttribute::type
	int32_t ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(VariableTypeAttribute_t3556842571, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLETYPEATTRIBUTE_T3556842571_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef COLLISION2DEVENT_T1483235594_H
#define COLLISION2DEVENT_T1483235594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/Collision2DEvent
struct  Collision2DEvent_t1483235594  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISION2DEVENT_T1483235594_H
#ifndef COLLISIONEVENT_T2644514664_H
#define COLLISIONEVENT_T2644514664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/CollisionEvent
struct  CollisionEvent_t2644514664  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONEVENT_T2644514664_H
#ifndef CONTROLLERCOLLISIONEVENT_T2755027817_H
#define CONTROLLERCOLLISIONEVENT_T2755027817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/ControllerCollisionEvent
struct  ControllerCollisionEvent_t2755027817  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERCOLLISIONEVENT_T2755027817_H
#ifndef PARTICLECOLLISIONEVENT_T3984792766_H
#define PARTICLECOLLISIONEVENT_T3984792766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/ParticleCollisionEvent
struct  ParticleCollisionEvent_t3984792766  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISIONEVENT_T3984792766_H
#ifndef TRIGGER2DEVENT_T2411364400_H
#define TRIGGER2DEVENT_T2411364400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/Trigger2DEvent
struct  Trigger2DEvent_t2411364400  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER2DEVENT_T2411364400_H
#ifndef TRIGGEREVENT_T1933226311_H
#define TRIGGEREVENT_T1933226311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase/TriggerEvent
struct  TriggerEvent_t1933226311  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T1933226311_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PLAYMAKERONGUI_T3357975613_H
#define PLAYMAKERONGUI_T3357975613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerOnGUI
struct  PlayMakerOnGUI_t3357975613  : public MonoBehaviour_t3962482529
{
public:
	// PlayMakerFSM PlayMakerOnGUI::playMakerFSM
	PlayMakerFSM_t1613010231 * ___playMakerFSM_4;
	// System.Boolean PlayMakerOnGUI::previewInEditMode
	bool ___previewInEditMode_5;

public:
	inline static int32_t get_offset_of_playMakerFSM_4() { return static_cast<int32_t>(offsetof(PlayMakerOnGUI_t3357975613, ___playMakerFSM_4)); }
	inline PlayMakerFSM_t1613010231 * get_playMakerFSM_4() const { return ___playMakerFSM_4; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_playMakerFSM_4() { return &___playMakerFSM_4; }
	inline void set_playMakerFSM_4(PlayMakerFSM_t1613010231 * value)
	{
		___playMakerFSM_4 = value;
		Il2CppCodeGenWriteBarrier((&___playMakerFSM_4), value);
	}

	inline static int32_t get_offset_of_previewInEditMode_5() { return static_cast<int32_t>(offsetof(PlayMakerOnGUI_t3357975613, ___previewInEditMode_5)); }
	inline bool get_previewInEditMode_5() const { return ___previewInEditMode_5; }
	inline bool* get_address_of_previewInEditMode_5() { return &___previewInEditMode_5; }
	inline void set_previewInEditMode_5(bool value)
	{
		___previewInEditMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERONGUI_T3357975613_H
#ifndef PLAYMAKERPROXYBASE_T90512809_H
#define PLAYMAKERPROXYBASE_T90512809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerProxyBase
struct  PlayMakerProxyBase_t90512809  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerProxyBase::TargetFSMs
	List_1_t3085084973 * ___TargetFSMs_4;
	// PlayMakerProxyBase/TriggerEvent PlayMakerProxyBase::TriggerEventCallback
	TriggerEvent_t1933226311 * ___TriggerEventCallback_5;
	// PlayMakerProxyBase/CollisionEvent PlayMakerProxyBase::CollisionEventCallback
	CollisionEvent_t2644514664 * ___CollisionEventCallback_6;
	// PlayMakerProxyBase/ParticleCollisionEvent PlayMakerProxyBase::ParticleCollisionEventCallback
	ParticleCollisionEvent_t3984792766 * ___ParticleCollisionEventCallback_7;
	// PlayMakerProxyBase/ControllerCollisionEvent PlayMakerProxyBase::ControllerCollisionEventCallback
	ControllerCollisionEvent_t2755027817 * ___ControllerCollisionEventCallback_8;
	// PlayMakerProxyBase/Trigger2DEvent PlayMakerProxyBase::Trigger2DEventCallback
	Trigger2DEvent_t2411364400 * ___Trigger2DEventCallback_9;
	// PlayMakerProxyBase/Collision2DEvent PlayMakerProxyBase::Collision2DEventCallback
	Collision2DEvent_t1483235594 * ___Collision2DEventCallback_10;

public:
	inline static int32_t get_offset_of_TargetFSMs_4() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___TargetFSMs_4)); }
	inline List_1_t3085084973 * get_TargetFSMs_4() const { return ___TargetFSMs_4; }
	inline List_1_t3085084973 ** get_address_of_TargetFSMs_4() { return &___TargetFSMs_4; }
	inline void set_TargetFSMs_4(List_1_t3085084973 * value)
	{
		___TargetFSMs_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetFSMs_4), value);
	}

	inline static int32_t get_offset_of_TriggerEventCallback_5() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___TriggerEventCallback_5)); }
	inline TriggerEvent_t1933226311 * get_TriggerEventCallback_5() const { return ___TriggerEventCallback_5; }
	inline TriggerEvent_t1933226311 ** get_address_of_TriggerEventCallback_5() { return &___TriggerEventCallback_5; }
	inline void set_TriggerEventCallback_5(TriggerEvent_t1933226311 * value)
	{
		___TriggerEventCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___TriggerEventCallback_5), value);
	}

	inline static int32_t get_offset_of_CollisionEventCallback_6() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___CollisionEventCallback_6)); }
	inline CollisionEvent_t2644514664 * get_CollisionEventCallback_6() const { return ___CollisionEventCallback_6; }
	inline CollisionEvent_t2644514664 ** get_address_of_CollisionEventCallback_6() { return &___CollisionEventCallback_6; }
	inline void set_CollisionEventCallback_6(CollisionEvent_t2644514664 * value)
	{
		___CollisionEventCallback_6 = value;
		Il2CppCodeGenWriteBarrier((&___CollisionEventCallback_6), value);
	}

	inline static int32_t get_offset_of_ParticleCollisionEventCallback_7() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___ParticleCollisionEventCallback_7)); }
	inline ParticleCollisionEvent_t3984792766 * get_ParticleCollisionEventCallback_7() const { return ___ParticleCollisionEventCallback_7; }
	inline ParticleCollisionEvent_t3984792766 ** get_address_of_ParticleCollisionEventCallback_7() { return &___ParticleCollisionEventCallback_7; }
	inline void set_ParticleCollisionEventCallback_7(ParticleCollisionEvent_t3984792766 * value)
	{
		___ParticleCollisionEventCallback_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleCollisionEventCallback_7), value);
	}

	inline static int32_t get_offset_of_ControllerCollisionEventCallback_8() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___ControllerCollisionEventCallback_8)); }
	inline ControllerCollisionEvent_t2755027817 * get_ControllerCollisionEventCallback_8() const { return ___ControllerCollisionEventCallback_8; }
	inline ControllerCollisionEvent_t2755027817 ** get_address_of_ControllerCollisionEventCallback_8() { return &___ControllerCollisionEventCallback_8; }
	inline void set_ControllerCollisionEventCallback_8(ControllerCollisionEvent_t2755027817 * value)
	{
		___ControllerCollisionEventCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerCollisionEventCallback_8), value);
	}

	inline static int32_t get_offset_of_Trigger2DEventCallback_9() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___Trigger2DEventCallback_9)); }
	inline Trigger2DEvent_t2411364400 * get_Trigger2DEventCallback_9() const { return ___Trigger2DEventCallback_9; }
	inline Trigger2DEvent_t2411364400 ** get_address_of_Trigger2DEventCallback_9() { return &___Trigger2DEventCallback_9; }
	inline void set_Trigger2DEventCallback_9(Trigger2DEvent_t2411364400 * value)
	{
		___Trigger2DEventCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger2DEventCallback_9), value);
	}

	inline static int32_t get_offset_of_Collision2DEventCallback_10() { return static_cast<int32_t>(offsetof(PlayMakerProxyBase_t90512809, ___Collision2DEventCallback_10)); }
	inline Collision2DEvent_t1483235594 * get_Collision2DEventCallback_10() const { return ___Collision2DEventCallback_10; }
	inline Collision2DEvent_t1483235594 ** get_address_of_Collision2DEventCallback_10() { return &___Collision2DEventCallback_10; }
	inline void set_Collision2DEventCallback_10(Collision2DEvent_t1483235594 * value)
	{
		___Collision2DEventCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___Collision2DEventCallback_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERPROXYBASE_T90512809_H
#ifndef PLAYMAKERJOINTBREAK_T32499873_H
#define PLAYMAKERJOINTBREAK_T32499873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerJointBreak
struct  PlayMakerJointBreak_t32499873  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERJOINTBREAK_T32499873_H
#ifndef PLAYMAKERJOINTBREAK2D_T628918649_H
#define PLAYMAKERJOINTBREAK2D_T628918649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerJointBreak2D
struct  PlayMakerJointBreak2D_t628918649  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERJOINTBREAK2D_T628918649_H
#ifndef PLAYMAKERMOUSEEVENTS_T4026808530_H
#define PLAYMAKERMOUSEEVENTS_T4026808530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerMouseEvents
struct  PlayMakerMouseEvents_t4026808530  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERMOUSEEVENTS_T4026808530_H
#ifndef PLAYMAKERPARTICLECOLLISION_T716698883_H
#define PLAYMAKERPARTICLECOLLISION_T716698883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerParticleCollision
struct  PlayMakerParticleCollision_t716698883  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERPARTICLECOLLISION_T716698883_H
#ifndef PLAYMAKERTRIGGERENTER_T2165260292_H
#define PLAYMAKERTRIGGERENTER_T2165260292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerEnter
struct  PlayMakerTriggerEnter_t2165260292  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERENTER_T2165260292_H
#ifndef PLAYMAKERTRIGGERENTER2D_T1611673000_H
#define PLAYMAKERTRIGGERENTER2D_T1611673000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerEnter2D
struct  PlayMakerTriggerEnter2D_t1611673000  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERENTER2D_T1611673000_H
#ifndef PLAYMAKERTRIGGEREXIT_T2127564928_H
#define PLAYMAKERTRIGGEREXIT_T2127564928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerExit
struct  PlayMakerTriggerExit_t2127564928  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGEREXIT_T2127564928_H
#ifndef PLAYMAKERTRIGGEREXIT2D_T2899497211_H
#define PLAYMAKERTRIGGEREXIT2D_T2899497211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerExit2D
struct  PlayMakerTriggerExit2D_t2899497211  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGEREXIT2D_T2899497211_H
#ifndef PLAYMAKERTRIGGERSTAY_T1432841146_H
#define PLAYMAKERTRIGGERSTAY_T1432841146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerStay
struct  PlayMakerTriggerStay_t1432841146  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERSTAY_T1432841146_H
#ifndef PLAYMAKERTRIGGERSTAY2D_T2180132409_H
#define PLAYMAKERTRIGGERSTAY2D_T2180132409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerTriggerStay2D
struct  PlayMakerTriggerStay2D_t2180132409  : public PlayMakerProxyBase_t90512809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMAKERTRIGGERSTAY2D_T2180132409_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (U3CU3Ec_t2300308631), -1, sizeof(U3CU3Ec_t2300308631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2100[2] = 
{
	U3CU3Ec_t2300308631_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2300308631_StaticFields::get_offset_of_U3CU3E9__65_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (PlayMakerMouseEvents_t4026808530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (PlayMakerOnGUI_t3357975613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	PlayMakerOnGUI_t3357975613::get_offset_of_playMakerFSM_4(),
	PlayMakerOnGUI_t3357975613::get_offset_of_previewInEditMode_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (PlayMakerProxyBase_t90512809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[7] = 
{
	PlayMakerProxyBase_t90512809::get_offset_of_TargetFSMs_4(),
	PlayMakerProxyBase_t90512809::get_offset_of_TriggerEventCallback_5(),
	PlayMakerProxyBase_t90512809::get_offset_of_CollisionEventCallback_6(),
	PlayMakerProxyBase_t90512809::get_offset_of_ParticleCollisionEventCallback_7(),
	PlayMakerProxyBase_t90512809::get_offset_of_ControllerCollisionEventCallback_8(),
	PlayMakerProxyBase_t90512809::get_offset_of_Trigger2DEventCallback_9(),
	PlayMakerProxyBase_t90512809::get_offset_of_Collision2DEventCallback_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (TriggerEvent_t1933226311), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (CollisionEvent_t2644514664), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Trigger2DEvent_t2411364400), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (Collision2DEvent_t1483235594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (ParticleCollisionEvent_t3984792766), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ControllerCollisionEvent_t2755027817), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (PlayMakerTriggerEnter_t2165260292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (PlayMakerTriggerEnter2D_t1611673000), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (PlayMakerTriggerExit_t2127564928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (PlayMakerTriggerExit2D_t2899497211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (PlayMakerTriggerStay_t1432841146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (PlayMakerTriggerStay2D_t2180132409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (PlayMakerParticleCollision_t716698883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (PlayMakerJointBreak_t32499873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (PlayMakerJointBreak2D_t628918649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (CollisionType_t4059948830)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[6] = 
{
	CollisionType_t4059948830::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (TriggerType_t1545777084)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2120[4] = 
{
	TriggerType_t1545777084::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (Collision2DType_t450510585)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2121[5] = 
{
	Collision2DType_t450510585::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (Trigger2DType_t3885175869)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2122[4] = 
{
	Trigger2DType_t3885175869::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (InterpolationType_t358821704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2123[3] = 
{
	InterpolationType_t358821704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (MouseEventType_t1688122298)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2124[7] = 
{
	MouseEventType_t1688122298::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (ActionCategory_t2600881856)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2125[57] = 
{
	ActionCategory_t2600881856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (UIHint_t1096202973)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2126[39] = 
{
	UIHint_t1096202973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (MouseButton_t2824504455)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2127[5] = 
{
	MouseButton_t2824504455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (LogLevel_t183558972)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2128[4] = 
{
	LogLevel_t183558972::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (ColorBlendMode_t3516528287)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2129[4] = 
{
	ColorBlendMode_t3516528287::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (ActionReport_t714070557), -1, sizeof(ActionReport_t714070557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[10] = 
{
	ActionReport_t714070557_StaticFields::get_offset_of_ActionReportList_0(),
	ActionReport_t714070557_StaticFields::get_offset_of_InfoCount_1(),
	ActionReport_t714070557_StaticFields::get_offset_of_ErrorCount_2(),
	ActionReport_t714070557::get_offset_of_fsm_3(),
	ActionReport_t714070557::get_offset_of_state_4(),
	ActionReport_t714070557::get_offset_of_action_5(),
	ActionReport_t714070557::get_offset_of_actionIndex_6(),
	ActionReport_t714070557::get_offset_of_logText_7(),
	ActionReport_t714070557::get_offset_of_isError_8(),
	ActionReport_t714070557::get_offset_of_parameter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (U3CU3Ec__DisplayClass18_0_t389513717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[1] = 
{
	U3CU3Ec__DisplayClass18_0_t389513717::get_offset_of_fsm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (ActionTarget_t3397043202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[3] = 
{
	ActionTarget_t3397043202::get_offset_of_objectType_0(),
	ActionTarget_t3397043202::get_offset_of_fieldName_1(),
	ActionTarget_t3397043202::get_offset_of_allowPrefabs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (NoActionTargetsAttribute_t3254466650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (HideIfAttribute_t785818473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[1] = 
{
	HideIfAttribute_t785818473::get_offset_of_U3CTestU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (SettingsMenuItemAttribute_t2376176270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	SettingsMenuItemAttribute_t2376176270::get_offset_of_U3CMenuItemU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (PreviewFieldAttribute_t1055979593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	PreviewFieldAttribute_t1055979593::get_offset_of_U3CMethodNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (SettingsMenuItemStateAttribute_t3930598301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	SettingsMenuItemStateAttribute_t3930598301::get_offset_of_U3CMenuItemU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (ActionCategoryAttribute_t1469261392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[1] = 
{
	ActionCategoryAttribute_t1469261392::get_offset_of_category_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (ActionSection_t3425516445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[1] = 
{
	ActionSection_t3425516445::get_offset_of_section_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (ArrayEditorAttribute_t4006704688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[6] = 
{
	ArrayEditorAttribute_t4006704688::get_offset_of_variableType_0(),
	ArrayEditorAttribute_t4006704688::get_offset_of_objectType_1(),
	ArrayEditorAttribute_t4006704688::get_offset_of_elementName_2(),
	ArrayEditorAttribute_t4006704688::get_offset_of_fixedSize_3(),
	ArrayEditorAttribute_t4006704688::get_offset_of_maxSize_4(),
	ArrayEditorAttribute_t4006704688::get_offset_of_minSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (CheckForComponentAttribute_t1147964680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[3] = 
{
	CheckForComponentAttribute_t1147964680::get_offset_of_type0_0(),
	CheckForComponentAttribute_t1147964680::get_offset_of_type1_1(),
	CheckForComponentAttribute_t1147964680::get_offset_of_type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (CompoundArrayAttribute_t3735867164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[3] = 
{
	CompoundArrayAttribute_t3735867164::get_offset_of_name_0(),
	CompoundArrayAttribute_t3735867164::get_offset_of_firstArrayName_1(),
	CompoundArrayAttribute_t3735867164::get_offset_of_secondArrayName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (DisplayOrderAttribute_t2986454654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[1] = 
{
	DisplayOrderAttribute_t2986454654::get_offset_of_index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (EventTargetAttribute_t1040314792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[1] = 
{
	EventTargetAttribute_t1040314792::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (HasFloatSliderAttribute_t228634986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[2] = 
{
	HasFloatSliderAttribute_t228634986::get_offset_of_minValue_0(),
	HasFloatSliderAttribute_t228634986::get_offset_of_maxValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (HelpUrlAttribute_t1897678690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[1] = 
{
	HelpUrlAttribute_t1897678690::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (HideTypeFilter_t1596225627), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (MatchElementTypeAttribute_t3380579236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[1] = 
{
	MatchElementTypeAttribute_t3380579236::get_offset_of_fieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (MatchFieldTypeAttribute_t2885974556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	MatchFieldTypeAttribute_t2885974556::get_offset_of_fieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (NoteAttribute_t316799825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[1] = 
{
	NoteAttribute_t316799825::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (ObjectTypeAttribute_t3116760320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[1] = 
{
	ObjectTypeAttribute_t3116760320::get_offset_of_objectType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (ReadonlyAttribute_t1786548413), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (VariableTypeAttribute_t3556842571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[1] = 
{
	VariableTypeAttribute_t3556842571::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (VariableTypeFilter_t3022214907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (RequiredFieldAttribute_t3920128169), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (TitleAttribute_t196394849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	TitleAttribute_t196394849::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (TooltipAttribute_t2411168496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[1] = 
{
	TooltipAttribute_t2411168496::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (UIHintAttribute_t2334957252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[1] = 
{
	UIHintAttribute_t2334957252::get_offset_of_hint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (FsmTemplateControl_t522200103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[4] = 
{
	FsmTemplateControl_t522200103::get_offset_of_fsmTemplate_0(),
	FsmTemplateControl_t522200103::get_offset_of_fsmVarOverrides_1(),
	FsmTemplateControl_t522200103::get_offset_of_U3CIDU3Ek__BackingField_2(),
	FsmTemplateControl_t522200103::get_offset_of_runFsm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (U3CU3Ec__DisplayClass16_0_t1615683978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[1] = 
{
	U3CU3Ec__DisplayClass16_0_t1615683978::get_offset_of_namedVariable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (FsmEventTarget_t919699796), -1, sizeof(FsmEventTarget_t919699796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[7] = 
{
	FsmEventTarget_t919699796_StaticFields::get_offset_of_self_0(),
	FsmEventTarget_t919699796::get_offset_of_target_1(),
	FsmEventTarget_t919699796::get_offset_of_excludeSelf_2(),
	FsmEventTarget_t919699796::get_offset_of_gameObject_3(),
	FsmEventTarget_t919699796::get_offset_of_fsmName_4(),
	FsmEventTarget_t919699796::get_offset_of_sendToChildren_5(),
	FsmEventTarget_t919699796::get_offset_of_fsmComponent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (EventTarget_t3002913944)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[8] = 
{
	EventTarget_t3002913944::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (FsmVarOverride_t858756622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[3] = 
{
	FsmVarOverride_t858756622::get_offset_of_variable_0(),
	FsmVarOverride_t858756622::get_offset_of_fsmVar_1(),
	FsmVarOverride_t858756622::get_offset_of_isEdited_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (FunctionCall_t1722713284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[17] = 
{
	FunctionCall_t1722713284::get_offset_of_FunctionName_0(),
	FunctionCall_t1722713284::get_offset_of_parameterType_1(),
	FunctionCall_t1722713284::get_offset_of_BoolParameter_2(),
	FunctionCall_t1722713284::get_offset_of_FloatParameter_3(),
	FunctionCall_t1722713284::get_offset_of_IntParameter_4(),
	FunctionCall_t1722713284::get_offset_of_GameObjectParameter_5(),
	FunctionCall_t1722713284::get_offset_of_ObjectParameter_6(),
	FunctionCall_t1722713284::get_offset_of_StringParameter_7(),
	FunctionCall_t1722713284::get_offset_of_Vector2Parameter_8(),
	FunctionCall_t1722713284::get_offset_of_Vector3Parameter_9(),
	FunctionCall_t1722713284::get_offset_of_RectParamater_10(),
	FunctionCall_t1722713284::get_offset_of_QuaternionParameter_11(),
	FunctionCall_t1722713284::get_offset_of_MaterialParameter_12(),
	FunctionCall_t1722713284::get_offset_of_TextureParameter_13(),
	FunctionCall_t1722713284::get_offset_of_ColorParameter_14(),
	FunctionCall_t1722713284::get_offset_of_EnumParameter_15(),
	FunctionCall_t1722713284::get_offset_of_ArrayParameter_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (LayoutOption_t3011926197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[3] = 
{
	LayoutOption_t3011926197::get_offset_of_option_0(),
	LayoutOption_t3011926197::get_offset_of_floatParam_1(),
	LayoutOption_t3011926197::get_offset_of_boolParam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (LayoutOptionType_t782463089)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2166[9] = 
{
	LayoutOptionType_t782463089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (DebugUtils_t1031043539), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (DelayedEvent_t3987626228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[7] = 
{
	DelayedEvent_t3987626228::get_offset_of_fsm_0(),
	DelayedEvent_t3987626228::get_offset_of_fsmEvent_1(),
	DelayedEvent_t3987626228::get_offset_of_eventTarget_2(),
	DelayedEvent_t3987626228::get_offset_of_eventData_3(),
	DelayedEvent_t3987626228::get_offset_of_timer_4(),
	DelayedEvent_t3987626228::get_offset_of_delay_5(),
	DelayedEvent_t3987626228::get_offset_of_eventFired_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (FsmDebugUtility_t2030146397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (FsmEventData_t1692568573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[17] = 
{
	FsmEventData_t1692568573::get_offset_of_SentByGameObject_0(),
	FsmEventData_t1692568573::get_offset_of_SentByFsm_1(),
	FsmEventData_t1692568573::get_offset_of_SentByState_2(),
	FsmEventData_t1692568573::get_offset_of_SentByAction_3(),
	FsmEventData_t1692568573::get_offset_of_BoolData_4(),
	FsmEventData_t1692568573::get_offset_of_IntData_5(),
	FsmEventData_t1692568573::get_offset_of_FloatData_6(),
	FsmEventData_t1692568573::get_offset_of_Vector2Data_7(),
	FsmEventData_t1692568573::get_offset_of_Vector3Data_8(),
	FsmEventData_t1692568573::get_offset_of_StringData_9(),
	FsmEventData_t1692568573::get_offset_of_QuaternionData_10(),
	FsmEventData_t1692568573::get_offset_of_RectData_11(),
	FsmEventData_t1692568573::get_offset_of_ColorData_12(),
	FsmEventData_t1692568573::get_offset_of_ObjectData_13(),
	FsmEventData_t1692568573::get_offset_of_GameObjectData_14(),
	FsmEventData_t1692568573::get_offset_of_MaterialData_15(),
	FsmEventData_t1692568573::get_offset_of_TextureData_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (FsmExecutionStack_t2219150547), -1, sizeof(FsmExecutionStack_t2219150547_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[2] = 
{
	FsmExecutionStack_t2219150547_StaticFields::get_offset_of_fsmExecutionStack_0(),
	FsmExecutionStack_t2219150547_StaticFields::get_offset_of_U3CMaxStackCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (FsmProperty_t1450375506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[24] = 
{
	FsmProperty_t1450375506::get_offset_of_TargetObject_0(),
	FsmProperty_t1450375506::get_offset_of_TargetTypeName_1(),
	FsmProperty_t1450375506::get_offset_of_TargetType_2(),
	FsmProperty_t1450375506::get_offset_of_PropertyName_3(),
	FsmProperty_t1450375506::get_offset_of_PropertyType_4(),
	FsmProperty_t1450375506::get_offset_of_BoolParameter_5(),
	FsmProperty_t1450375506::get_offset_of_FloatParameter_6(),
	FsmProperty_t1450375506::get_offset_of_IntParameter_7(),
	FsmProperty_t1450375506::get_offset_of_GameObjectParameter_8(),
	FsmProperty_t1450375506::get_offset_of_StringParameter_9(),
	FsmProperty_t1450375506::get_offset_of_Vector2Parameter_10(),
	FsmProperty_t1450375506::get_offset_of_Vector3Parameter_11(),
	FsmProperty_t1450375506::get_offset_of_RectParamater_12(),
	FsmProperty_t1450375506::get_offset_of_QuaternionParameter_13(),
	FsmProperty_t1450375506::get_offset_of_ObjectParameter_14(),
	FsmProperty_t1450375506::get_offset_of_MaterialParameter_15(),
	FsmProperty_t1450375506::get_offset_of_TextureParameter_16(),
	FsmProperty_t1450375506::get_offset_of_ColorParameter_17(),
	FsmProperty_t1450375506::get_offset_of_EnumParameter_18(),
	FsmProperty_t1450375506::get_offset_of_ArrayParameter_19(),
	FsmProperty_t1450375506::get_offset_of_setProperty_20(),
	FsmProperty_t1450375506::get_offset_of_initialized_21(),
	FsmProperty_t1450375506::get_offset_of_targetObjectCached_22(),
	FsmProperty_t1450375506::get_offset_of_memberInfo_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (FsmTime_t2866059696), -1, sizeof(FsmTime_t2866059696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2173[4] = 
{
	FsmTime_t2866059696_StaticFields::get_offset_of_firstUpdateHasHappened_0(),
	FsmTime_t2866059696_StaticFields::get_offset_of_totalEditorPlayerPausedTime_1(),
	FsmTime_t2866059696_StaticFields::get_offset_of_realtimeLastUpdate_2(),
	FsmTime_t2866059696_StaticFields::get_offset_of_frameCountLastUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (FsmAnimationCurve_t3432711236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[1] = 
{
	FsmAnimationCurve_t3432711236::get_offset_of_curve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (FsmArray_t1756862219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[11] = 
{
	FsmArray_t1756862219::get_offset_of_type_6(),
	FsmArray_t1756862219::get_offset_of_objectTypeName_7(),
	FsmArray_t1756862219::get_offset_of_objectType_8(),
	FsmArray_t1756862219::get_offset_of_floatValues_9(),
	FsmArray_t1756862219::get_offset_of_intValues_10(),
	FsmArray_t1756862219::get_offset_of_boolValues_11(),
	FsmArray_t1756862219::get_offset_of_stringValues_12(),
	FsmArray_t1756862219::get_offset_of_vector4Values_13(),
	FsmArray_t1756862219::get_offset_of_objectReferences_14(),
	FsmArray_t1756862219::get_offset_of_sourceArray_15(),
	FsmArray_t1756862219::get_offset_of_values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (FsmBool_t163807967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[1] = 
{
	FsmBool_t163807967::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (FsmColor_t1738900188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[1] = 
{
	FsmColor_t1738900188::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (FsmEnum_t2861764163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[5] = 
{
	FsmEnum_t2861764163::get_offset_of_enumName_6(),
	FsmEnum_t2861764163::get_offset_of_intValue_7(),
	FsmEnum_t2861764163::get_offset_of_value_8(),
	FsmEnum_t2861764163::get_offset_of_parsedIntValue_9(),
	FsmEnum_t2861764163::get_offset_of_enumType_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (None_t4017744907)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	None_t4017744907::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (FsmFloat_t2883254149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[1] = 
{
	FsmFloat_t2883254149::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (FsmGameObject_t3581898942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[2] = 
{
	FsmGameObject_t3581898942::get_offset_of_OnChange_6(),
	FsmGameObject_t3581898942::get_offset_of_value_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (FsmInt_t874273141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[1] = 
{
	FsmInt_t874273141::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (FsmMaterial_t2057874452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (FsmObject_t2606870197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[3] = 
{
	FsmObject_t2606870197::get_offset_of_typeName_6(),
	FsmObject_t2606870197::get_offset_of_value_7(),
	FsmObject_t2606870197::get_offset_of_objectType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (FsmOwnerDefault_t3590610434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[2] = 
{
	FsmOwnerDefault_t3590610434::get_offset_of_ownerOption_0(),
	FsmOwnerDefault_t3590610434::get_offset_of_gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (OwnerDefaultOption_t4041051211)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2186[3] = 
{
	OwnerDefaultOption_t4041051211::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (FsmQuaternion_t4259038327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[1] = 
{
	FsmQuaternion_t4259038327::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (FsmRect_t3649179877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	FsmRect_t3649179877::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (FsmString_t1785915204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	FsmString_t1785915204::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (FsmTexture_t1738787045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (FsmVar_t2413660594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[19] = 
{
	FsmVar_t2413660594::get_offset_of_variableName_0(),
	FsmVar_t2413660594::get_offset_of_objectType_1(),
	FsmVar_t2413660594::get_offset_of_useVariable_2(),
	FsmVar_t2413660594::get_offset_of_namedVar_3(),
	FsmVar_t2413660594::get_offset_of_namedVarType_4(),
	FsmVar_t2413660594::get_offset_of_enumType_5(),
	FsmVar_t2413660594::get_offset_of_enumValue_6(),
	FsmVar_t2413660594::get_offset_of__objectType_7(),
	FsmVar_t2413660594::get_offset_of_type_8(),
	FsmVar_t2413660594::get_offset_of_floatValue_9(),
	FsmVar_t2413660594::get_offset_of_intValue_10(),
	FsmVar_t2413660594::get_offset_of_boolValue_11(),
	FsmVar_t2413660594::get_offset_of_stringValue_12(),
	FsmVar_t2413660594::get_offset_of_vector4Value_13(),
	FsmVar_t2413660594::get_offset_of_objectReference_14(),
	FsmVar_t2413660594::get_offset_of_arrayValue_15(),
	FsmVar_t2413660594::get_offset_of_vector2_16(),
	FsmVar_t2413660594::get_offset_of_vector3_17(),
	FsmVar_t2413660594::get_offset_of_rect_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (FsmVector2_t2965096677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[1] = 
{
	FsmVector2_t2965096677::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (FsmVector3_t626444517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[1] = 
{
	FsmVector3_t626444517::get_offset_of_value_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (NamedVariable_t2808389578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[6] = 
{
	NamedVariable_t2808389578::get_offset_of_useVariable_0(),
	NamedVariable_t2808389578::get_offset_of_name_1(),
	NamedVariable_t2808389578::get_offset_of_tooltip_2(),
	NamedVariable_t2808389578::get_offset_of_showInInspector_3(),
	NamedVariable_t2808389578::get_offset_of_networkSync_4(),
	NamedVariable_t2808389578::get_offset_of_obj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (ActionData_t1922786972), -1, sizeof(ActionData_t1922786972_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2197[48] = 
{
	0,
	ActionData_t1922786972_StaticFields::get_offset_of_ActionTypeLookup_1(),
	ActionData_t1922786972_StaticFields::get_offset_of_ActionFieldsLookup_2(),
	ActionData_t1922786972_StaticFields::get_offset_of_ActionHashCodeLookup_3(),
	ActionData_t1922786972_StaticFields::get_offset_of_resaveActionData_4(),
	ActionData_t1922786972_StaticFields::get_offset_of_UsedIndices_5(),
	ActionData_t1922786972_StaticFields::get_offset_of_InitFields_6(),
	ActionData_t1922786972::get_offset_of_actionNames_7(),
	ActionData_t1922786972::get_offset_of_customNames_8(),
	ActionData_t1922786972::get_offset_of_actionEnabled_9(),
	ActionData_t1922786972::get_offset_of_actionIsOpen_10(),
	ActionData_t1922786972::get_offset_of_actionStartIndex_11(),
	ActionData_t1922786972::get_offset_of_actionHashCodes_12(),
	ActionData_t1922786972::get_offset_of_unityObjectParams_13(),
	ActionData_t1922786972::get_offset_of_fsmGameObjectParams_14(),
	ActionData_t1922786972::get_offset_of_fsmOwnerDefaultParams_15(),
	ActionData_t1922786972::get_offset_of_animationCurveParams_16(),
	ActionData_t1922786972::get_offset_of_functionCallParams_17(),
	ActionData_t1922786972::get_offset_of_fsmTemplateControlParams_18(),
	ActionData_t1922786972::get_offset_of_fsmEventTargetParams_19(),
	ActionData_t1922786972::get_offset_of_fsmPropertyParams_20(),
	ActionData_t1922786972::get_offset_of_layoutOptionParams_21(),
	ActionData_t1922786972::get_offset_of_fsmStringParams_22(),
	ActionData_t1922786972::get_offset_of_fsmObjectParams_23(),
	ActionData_t1922786972::get_offset_of_fsmVarParams_24(),
	ActionData_t1922786972::get_offset_of_fsmArrayParams_25(),
	ActionData_t1922786972::get_offset_of_fsmEnumParams_26(),
	ActionData_t1922786972::get_offset_of_fsmFloatParams_27(),
	ActionData_t1922786972::get_offset_of_fsmIntParams_28(),
	ActionData_t1922786972::get_offset_of_fsmBoolParams_29(),
	ActionData_t1922786972::get_offset_of_fsmVector2Params_30(),
	ActionData_t1922786972::get_offset_of_fsmVector3Params_31(),
	ActionData_t1922786972::get_offset_of_fsmColorParams_32(),
	ActionData_t1922786972::get_offset_of_fsmRectParams_33(),
	ActionData_t1922786972::get_offset_of_fsmQuaternionParams_34(),
	ActionData_t1922786972::get_offset_of_stringParams_35(),
	ActionData_t1922786972::get_offset_of_byteData_36(),
	ActionData_t1922786972::get_offset_of_byteDataAsArray_37(),
	ActionData_t1922786972::get_offset_of_arrayParamSizes_38(),
	ActionData_t1922786972::get_offset_of_arrayParamTypes_39(),
	ActionData_t1922786972::get_offset_of_customTypeSizes_40(),
	ActionData_t1922786972::get_offset_of_customTypeNames_41(),
	ActionData_t1922786972::get_offset_of_paramDataType_42(),
	ActionData_t1922786972::get_offset_of_paramName_43(),
	ActionData_t1922786972::get_offset_of_paramDataPos_44(),
	ActionData_t1922786972::get_offset_of_paramByteDataSize_45(),
	ActionData_t1922786972::get_offset_of_nextParamIndex_46(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (Context_t1719175498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[5] = 
{
	Context_t1719175498::get_offset_of_currentFsm_0(),
	Context_t1719175498::get_offset_of_currentState_1(),
	Context_t1719175498::get_offset_of_currentAction_2(),
	Context_t1719175498::get_offset_of_currentActionIndex_3(),
	Context_t1719175498::get_offset_of_currentParameter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (ParamDataType_t3128825015)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2199[44] = 
{
	ParamDataType_t3128825015::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
