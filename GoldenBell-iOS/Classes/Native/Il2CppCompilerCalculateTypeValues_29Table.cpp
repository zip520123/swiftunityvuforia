﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Colorful.Glitch/InterferenceSettings
struct InterferenceSettings_t2118005662;
// Colorful.Glitch/TearingSettings
struct TearingSettings_t1139179787;
// DynamicShadowDistance
struct DynamicShadowDistance_t2173449775;
// RenderTextureToProjector
struct RenderTextureToProjector_t1529163424;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// Test
struct Test_t650638817;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Projector
struct Projector_t2748997877;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture3D
struct Texture3D_t1884131049;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CLIB_T276046374_H
#define CLIB_T276046374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.CLib
struct  CLib_t276046374  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIB_T276046374_H
#ifndef INTERFERENCESETTINGS_T2118005662_H
#define INTERFERENCESETTINGS_T2118005662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch/InterferenceSettings
struct  InterferenceSettings_t2118005662  : public RuntimeObject
{
public:
	// System.Single Colorful.Glitch/InterferenceSettings::Speed
	float ___Speed_0;
	// System.Single Colorful.Glitch/InterferenceSettings::Density
	float ___Density_1;
	// System.Single Colorful.Glitch/InterferenceSettings::MaxDisplacement
	float ___MaxDisplacement_2;

public:
	inline static int32_t get_offset_of_Speed_0() { return static_cast<int32_t>(offsetof(InterferenceSettings_t2118005662, ___Speed_0)); }
	inline float get_Speed_0() const { return ___Speed_0; }
	inline float* get_address_of_Speed_0() { return &___Speed_0; }
	inline void set_Speed_0(float value)
	{
		___Speed_0 = value;
	}

	inline static int32_t get_offset_of_Density_1() { return static_cast<int32_t>(offsetof(InterferenceSettings_t2118005662, ___Density_1)); }
	inline float get_Density_1() const { return ___Density_1; }
	inline float* get_address_of_Density_1() { return &___Density_1; }
	inline void set_Density_1(float value)
	{
		___Density_1 = value;
	}

	inline static int32_t get_offset_of_MaxDisplacement_2() { return static_cast<int32_t>(offsetof(InterferenceSettings_t2118005662, ___MaxDisplacement_2)); }
	inline float get_MaxDisplacement_2() const { return ___MaxDisplacement_2; }
	inline float* get_address_of_MaxDisplacement_2() { return &___MaxDisplacement_2; }
	inline void set_MaxDisplacement_2(float value)
	{
		___MaxDisplacement_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERFERENCESETTINGS_T2118005662_H
#ifndef TEARINGSETTINGS_T1139179787_H
#define TEARINGSETTINGS_T1139179787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch/TearingSettings
struct  TearingSettings_t1139179787  : public RuntimeObject
{
public:
	// System.Single Colorful.Glitch/TearingSettings::Speed
	float ___Speed_0;
	// System.Single Colorful.Glitch/TearingSettings::Intensity
	float ___Intensity_1;
	// System.Single Colorful.Glitch/TearingSettings::MaxDisplacement
	float ___MaxDisplacement_2;
	// System.Boolean Colorful.Glitch/TearingSettings::AllowFlipping
	bool ___AllowFlipping_3;
	// System.Boolean Colorful.Glitch/TearingSettings::YuvColorBleeding
	bool ___YuvColorBleeding_4;
	// System.Single Colorful.Glitch/TearingSettings::YuvOffset
	float ___YuvOffset_5;

public:
	inline static int32_t get_offset_of_Speed_0() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___Speed_0)); }
	inline float get_Speed_0() const { return ___Speed_0; }
	inline float* get_address_of_Speed_0() { return &___Speed_0; }
	inline void set_Speed_0(float value)
	{
		___Speed_0 = value;
	}

	inline static int32_t get_offset_of_Intensity_1() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___Intensity_1)); }
	inline float get_Intensity_1() const { return ___Intensity_1; }
	inline float* get_address_of_Intensity_1() { return &___Intensity_1; }
	inline void set_Intensity_1(float value)
	{
		___Intensity_1 = value;
	}

	inline static int32_t get_offset_of_MaxDisplacement_2() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___MaxDisplacement_2)); }
	inline float get_MaxDisplacement_2() const { return ___MaxDisplacement_2; }
	inline float* get_address_of_MaxDisplacement_2() { return &___MaxDisplacement_2; }
	inline void set_MaxDisplacement_2(float value)
	{
		___MaxDisplacement_2 = value;
	}

	inline static int32_t get_offset_of_AllowFlipping_3() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___AllowFlipping_3)); }
	inline bool get_AllowFlipping_3() const { return ___AllowFlipping_3; }
	inline bool* get_address_of_AllowFlipping_3() { return &___AllowFlipping_3; }
	inline void set_AllowFlipping_3(bool value)
	{
		___AllowFlipping_3 = value;
	}

	inline static int32_t get_offset_of_YuvColorBleeding_4() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___YuvColorBleeding_4)); }
	inline bool get_YuvColorBleeding_4() const { return ___YuvColorBleeding_4; }
	inline bool* get_address_of_YuvColorBleeding_4() { return &___YuvColorBleeding_4; }
	inline void set_YuvColorBleeding_4(bool value)
	{
		___YuvColorBleeding_4 = value;
	}

	inline static int32_t get_offset_of_YuvOffset_5() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___YuvOffset_5)); }
	inline float get_YuvOffset_5() const { return ___YuvOffset_5; }
	inline float* get_address_of_YuvOffset_5() { return &___YuvOffset_5; }
	inline void set_YuvOffset_5(float value)
	{
		___YuvOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEARINGSETTINGS_T1139179787_H
#ifndef U3CCUSTOMUPDATEU3EC__ITERATOR0_T97322721_H
#define U3CCUSTOMUPDATEU3EC__ITERATOR0_T97322721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicShadowDistance/<CustomUpdate>c__Iterator0
struct  U3CCustomUpdateU3Ec__Iterator0_t97322721  : public RuntimeObject
{
public:
	// DynamicShadowDistance DynamicShadowDistance/<CustomUpdate>c__Iterator0::$this
	DynamicShadowDistance_t2173449775 * ___U24this_0;
	// System.Object DynamicShadowDistance/<CustomUpdate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean DynamicShadowDistance/<CustomUpdate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 DynamicShadowDistance/<CustomUpdate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCustomUpdateU3Ec__Iterator0_t97322721, ___U24this_0)); }
	inline DynamicShadowDistance_t2173449775 * get_U24this_0() const { return ___U24this_0; }
	inline DynamicShadowDistance_t2173449775 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(DynamicShadowDistance_t2173449775 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCustomUpdateU3Ec__Iterator0_t97322721, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCustomUpdateU3Ec__Iterator0_t97322721, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCustomUpdateU3Ec__Iterator0_t97322721, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCUSTOMUPDATEU3EC__ITERATOR0_T97322721_H
#ifndef NATIVESHARE_T4027546635_H
#define NATIVESHARE_T4027546635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare
struct  NativeShare_t4027546635  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVESHARE_T4027546635_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CDELAYEDSHARE_IMAGEU3EC__ITERATOR2_T908651865_H
#define U3CDELAYEDSHARE_IMAGEU3EC__ITERATOR2_T908651865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Test/<DelayedShare_Image>c__Iterator2
struct  U3CDelayedShare_ImageU3Ec__Iterator2_t908651865  : public RuntimeObject
{
public:
	// System.String Test/<DelayedShare_Image>c__Iterator2::screenShotPath
	String_t* ___screenShotPath_0;
	// Test Test/<DelayedShare_Image>c__Iterator2::$this
	Test_t650638817 * ___U24this_1;
	// System.Object Test/<DelayedShare_Image>c__Iterator2::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Test/<DelayedShare_Image>c__Iterator2::$disposing
	bool ___U24disposing_3;
	// System.Int32 Test/<DelayedShare_Image>c__Iterator2::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_screenShotPath_0() { return static_cast<int32_t>(offsetof(U3CDelayedShare_ImageU3Ec__Iterator2_t908651865, ___screenShotPath_0)); }
	inline String_t* get_screenShotPath_0() const { return ___screenShotPath_0; }
	inline String_t** get_address_of_screenShotPath_0() { return &___screenShotPath_0; }
	inline void set_screenShotPath_0(String_t* value)
	{
		___screenShotPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotPath_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayedShare_ImageU3Ec__Iterator2_t908651865, ___U24this_1)); }
	inline Test_t650638817 * get_U24this_1() const { return ___U24this_1; }
	inline Test_t650638817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Test_t650638817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayedShare_ImageU3Ec__Iterator2_t908651865, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayedShare_ImageU3Ec__Iterator2_t908651865, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayedShare_ImageU3Ec__Iterator2_t908651865, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSHARE_IMAGEU3EC__ITERATOR2_T908651865_H
#ifndef U3CGETSCREENSHOTU3EC__ITERATOR1_T1577098861_H
#define U3CGETSCREENSHOTU3EC__ITERATOR1_T1577098861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Test/<GetScreenshot>c__Iterator1
struct  U3CGetScreenshotU3Ec__Iterator1_t1577098861  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Test/<GetScreenshot>c__Iterator1::<screenshot>__0
	Texture2D_t3840446185 * ___U3CscreenshotU3E__0_0;
	// Test Test/<GetScreenshot>c__Iterator1::$this
	Test_t650638817 * ___U24this_1;
	// System.Object Test/<GetScreenshot>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Test/<GetScreenshot>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 Test/<GetScreenshot>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CscreenshotU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetScreenshotU3Ec__Iterator1_t1577098861, ___U3CscreenshotU3E__0_0)); }
	inline Texture2D_t3840446185 * get_U3CscreenshotU3E__0_0() const { return ___U3CscreenshotU3E__0_0; }
	inline Texture2D_t3840446185 ** get_address_of_U3CscreenshotU3E__0_0() { return &___U3CscreenshotU3E__0_0; }
	inline void set_U3CscreenshotU3E__0_0(Texture2D_t3840446185 * value)
	{
		___U3CscreenshotU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscreenshotU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetScreenshotU3Ec__Iterator1_t1577098861, ___U24this_1)); }
	inline Test_t650638817 * get_U24this_1() const { return ___U24this_1; }
	inline Test_t650638817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Test_t650638817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetScreenshotU3Ec__Iterator1_t1577098861, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetScreenshotU3Ec__Iterator1_t1577098861, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetScreenshotU3Ec__Iterator1_t1577098861, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSCREENSHOTU3EC__ITERATOR1_T1577098861_H
#ifndef U3CDELAYEDSHAREU3EC__ITERATOR0_T2132201013_H
#define U3CDELAYEDSHAREU3EC__ITERATOR0_T2132201013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Test/<delayedShare>c__Iterator0
struct  U3CdelayedShareU3Ec__Iterator0_t2132201013  : public RuntimeObject
{
public:
	// System.String Test/<delayedShare>c__Iterator0::screenShotPath
	String_t* ___screenShotPath_0;
	// System.String Test/<delayedShare>c__Iterator0::text
	String_t* ___text_1;
	// System.Object Test/<delayedShare>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Test/<delayedShare>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Test/<delayedShare>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_screenShotPath_0() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator0_t2132201013, ___screenShotPath_0)); }
	inline String_t* get_screenShotPath_0() const { return ___screenShotPath_0; }
	inline String_t** get_address_of_screenShotPath_0() { return &___screenShotPath_0; }
	inline void set_screenShotPath_0(String_t* value)
	{
		___screenShotPath_0 = value;
		Il2CppCodeGenWriteBarrier((&___screenShotPath_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator0_t2132201013, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator0_t2132201013, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator0_t2132201013, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CdelayedShareU3Ec__Iterator0_t2132201013, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSHAREU3EC__ITERATOR0_T2132201013_H
#ifndef CONFIGSTRUCT_T1540812723_H
#define CONFIGSTRUCT_T1540812723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/ConfigStruct
struct  ConfigStruct_t1540812723 
{
public:
	// System.String NativeShare/ConfigStruct::title
	String_t* ___title_0;
	// System.String NativeShare/ConfigStruct::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(ConfigStruct_t1540812723, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(ConfigStruct_t1540812723, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NativeShare/ConfigStruct
struct ConfigStruct_t1540812723_marshaled_pinvoke
{
	char* ___title_0;
	char* ___message_1;
};
// Native definition for COM marshalling of NativeShare/ConfigStruct
struct ConfigStruct_t1540812723_marshaled_com
{
	Il2CppChar* ___title_0;
	Il2CppChar* ___message_1;
};
#endif // CONFIGSTRUCT_T1540812723_H
#ifndef SOCIALSHARINGSTRUCT_T2558757706_H
#define SOCIALSHARINGSTRUCT_T2558757706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeShare/SocialSharingStruct
struct  SocialSharingStruct_t2558757706 
{
public:
	// System.String NativeShare/SocialSharingStruct::text
	String_t* ___text_0;
	// System.String NativeShare/SocialSharingStruct::subject
	String_t* ___subject_1;
	// System.String NativeShare/SocialSharingStruct::filePaths
	String_t* ___filePaths_2;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t2558757706, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_subject_1() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t2558757706, ___subject_1)); }
	inline String_t* get_subject_1() const { return ___subject_1; }
	inline String_t** get_address_of_subject_1() { return &___subject_1; }
	inline void set_subject_1(String_t* value)
	{
		___subject_1 = value;
		Il2CppCodeGenWriteBarrier((&___subject_1), value);
	}

	inline static int32_t get_offset_of_filePaths_2() { return static_cast<int32_t>(offsetof(SocialSharingStruct_t2558757706, ___filePaths_2)); }
	inline String_t* get_filePaths_2() const { return ___filePaths_2; }
	inline String_t** get_address_of_filePaths_2() { return &___filePaths_2; }
	inline void set_filePaths_2(String_t* value)
	{
		___filePaths_2 = value;
		Il2CppCodeGenWriteBarrier((&___filePaths_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of NativeShare/SocialSharingStruct
struct SocialSharingStruct_t2558757706_marshaled_pinvoke
{
	char* ___text_0;
	char* ___subject_1;
	char* ___filePaths_2;
};
// Native definition for COM marshalling of NativeShare/SocialSharingStruct
struct SocialSharingStruct_t2558757706_marshaled_com
{
	Il2CppChar* ___text_0;
	Il2CppChar* ___subject_1;
	Il2CppChar* ___filePaths_2;
};
#endif // SOCIALSHARINGSTRUCT_T2558757706_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef BLENDINGMODE_T1651387113_H
#define BLENDINGMODE_T1651387113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Blend/BlendingMode
struct  BlendingMode_t1651387113 
{
public:
	// System.Int32 Colorful.Blend/BlendingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendingMode_t1651387113, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDINGMODE_T1651387113_H
#ifndef CHANNEL_T1767287641_H
#define CHANNEL_T1767287641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelSwapper/Channel
struct  Channel_t1767287641 
{
public:
	// System.Int32 Colorful.ChannelSwapper/Channel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Channel_t1767287641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNEL_T1767287641_H
#ifndef QUALITYPRESET_T119956193_H
#define QUALITYPRESET_T119956193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DirectionalBlur/QualityPreset
struct  QualityPreset_t119956193 
{
public:
	// System.Int32 Colorful.DirectionalBlur/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t119956193, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T119956193_H
#ifndef COLORMODE_T3494539450_H
#define COLORMODE_T3494539450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.FastVignette/ColorMode
struct  ColorMode_t3494539450 
{
public:
	// System.Int32 Colorful.FastVignette/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t3494539450, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T3494539450_H
#ifndef GLITCHINGMODE_T3305168335_H
#define GLITCHINGMODE_T3305168335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch/GlitchingMode
struct  GlitchingMode_t3305168335 
{
public:
	// System.Int32 Colorful.Glitch/GlitchingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GlitchingMode_t3305168335, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCHINGMODE_T3305168335_H
#ifndef SIZEMODE_T1428983497_H
#define SIZEMODE_T1428983497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Led/SizeMode
struct  SizeMode_t1428983497 
{
public:
	// System.Int32 Colorful.Led/SizeMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SizeMode_t1428983497, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZEMODE_T1428983497_H
#ifndef QUALITYPRESET_T618172025_H
#define QUALITYPRESET_T618172025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LensDistortionBlur/QualityPreset
struct  QualityPreset_t618172025 
{
public:
	// System.Int32 Colorful.LensDistortionBlur/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t618172025, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T618172025_H
#ifndef COLORMODE_T803725769_H
#define COLORMODE_T803725769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Levels/ColorMode
struct  ColorMode_t803725769 
{
public:
	// System.Int32 Colorful.Levels/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t803725769, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T803725769_H
#ifndef PRESET_T400928058_H
#define PRESET_T400928058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LoFiPalette/Preset
struct  Preset_t400928058 
{
public:
	// System.Int32 Colorful.LoFiPalette/Preset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Preset_t400928058, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESET_T400928058_H
#ifndef MINATTRIBUTE_T1755422200_H
#define MINATTRIBUTE_T1755422200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.MinAttribute
struct  MinAttribute_t1755422200  : public PropertyAttribute_t3677895545
{
public:
	// System.Single Colorful.MinAttribute::Min
	float ___Min_0;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t1755422200, ___Min_0)); }
	inline float get_Min_0() const { return ___Min_0; }
	inline float* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(float value)
	{
		___Min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T1755422200_H
#ifndef COLORMODE_T2454795589_H
#define COLORMODE_T2454795589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Noise/ColorMode
struct  ColorMode_t2454795589 
{
public:
	// System.Int32 Colorful.Noise/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t2454795589, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T2454795589_H
#ifndef SIZEMODE_T3498286277_H
#define SIZEMODE_T3498286277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Pixelate/SizeMode
struct  SizeMode_t3498286277 
{
public:
	// System.Int32 Colorful.Pixelate/SizeMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SizeMode_t3498286277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZEMODE_T3498286277_H
#ifndef QUALITYPRESET_T1040903510_H
#define QUALITYPRESET_T1040903510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.RadialBlur/QualityPreset
struct  QualityPreset_t1040903510 
{
public:
	// System.Int32 Colorful.RadialBlur/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t1040903510, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T1040903510_H
#ifndef COLORMODE_T2800918933_H
#define COLORMODE_T2800918933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ShadowsMidtonesHighlights/ColorMode
struct  ColorMode_t2800918933 
{
public:
	// System.Int32 Colorful.ShadowsMidtonesHighlights/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t2800918933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T2800918933_H
#ifndef ALGORITHM_T3262665680_H
#define ALGORITHM_T3262665680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Sharpen/Algorithm
struct  Algorithm_t3262665680 
{
public:
	// System.Int32 Colorful.Sharpen/Algorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Algorithm_t3262665680, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHM_T3262665680_H
#ifndef COLORMODE_T650858100_H
#define COLORMODE_T650858100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Strokes/ColorMode
struct  ColorMode_t650858100 
{
public:
	// System.Int32 Colorful.Strokes/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t650858100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T650858100_H
#ifndef INSTRAGRAMFILTER_T685827072_H
#define INSTRAGRAMFILTER_T685827072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Vintage/InstragramFilter
struct  InstragramFilter_t685827072 
{
public:
	// System.Int32 Colorful.Vintage/InstragramFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstragramFilter_t685827072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRAGRAMFILTER_T685827072_H
#ifndef BALANCEMODE_T686342893_H
#define BALANCEMODE_T686342893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.WhiteBalance/BalanceMode
struct  BalanceMode_t686342893 
{
public:
	// System.Int32 Colorful.WhiteBalance/BalanceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BalanceMode_t686342893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALANCEMODE_T686342893_H
#ifndef ALGORITHM_T3898688573_H
#define ALGORITHM_T3898688573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Wiggle/Algorithm
struct  Algorithm_t3898688573 
{
public:
	// System.Int32 Colorful.Wiggle/Algorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Algorithm_t3898688573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHM_T3898688573_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BASEEFFECT_T1187847871_H
#define BASEEFFECT_T1187847871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BaseEffect
struct  BaseEffect_t1187847871  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader Colorful.BaseEffect::Shader
	Shader_t4151988712 * ___Shader_4;
	// UnityEngine.Material Colorful.BaseEffect::m_Material
	Material_t340375123 * ___m_Material_5;

public:
	inline static int32_t get_offset_of_Shader_4() { return static_cast<int32_t>(offsetof(BaseEffect_t1187847871, ___Shader_4)); }
	inline Shader_t4151988712 * get_Shader_4() const { return ___Shader_4; }
	inline Shader_t4151988712 ** get_address_of_Shader_4() { return &___Shader_4; }
	inline void set_Shader_4(Shader_t4151988712 * value)
	{
		___Shader_4 = value;
		Il2CppCodeGenWriteBarrier((&___Shader_4), value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(BaseEffect_t1187847871, ___m_Material_5)); }
	inline Material_t340375123 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t340375123 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t340375123 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEFFECT_T1187847871_H
#ifndef HISTOGRAM_T2046042414_H
#define HISTOGRAM_T2046042414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Histogram
struct  Histogram_t2046042414  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HISTOGRAM_T2046042414_H
#ifndef LOOKUPFILTER3D_T3361124730_H
#define LOOKUPFILTER3D_T3361124730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LookupFilter3D
struct  LookupFilter3D_t3361124730  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Colorful.LookupFilter3D::LookupTexture
	Texture2D_t3840446185 * ___LookupTexture_4;
	// System.Single Colorful.LookupFilter3D::Amount
	float ___Amount_5;
	// System.Boolean Colorful.LookupFilter3D::ForceCompatibility
	bool ___ForceCompatibility_6;
	// UnityEngine.Texture3D Colorful.LookupFilter3D::m_Lut3D
	Texture3D_t1884131049 * ___m_Lut3D_7;
	// System.String Colorful.LookupFilter3D::m_BaseTextureName
	String_t* ___m_BaseTextureName_8;
	// System.Boolean Colorful.LookupFilter3D::m_Use2DLut
	bool ___m_Use2DLut_9;
	// UnityEngine.Shader Colorful.LookupFilter3D::Shader2D
	Shader_t4151988712 * ___Shader2D_10;
	// UnityEngine.Shader Colorful.LookupFilter3D::Shader3D
	Shader_t4151988712 * ___Shader3D_11;
	// UnityEngine.Material Colorful.LookupFilter3D::m_Material2D
	Material_t340375123 * ___m_Material2D_12;
	// UnityEngine.Material Colorful.LookupFilter3D::m_Material3D
	Material_t340375123 * ___m_Material3D_13;

public:
	inline static int32_t get_offset_of_LookupTexture_4() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___LookupTexture_4)); }
	inline Texture2D_t3840446185 * get_LookupTexture_4() const { return ___LookupTexture_4; }
	inline Texture2D_t3840446185 ** get_address_of_LookupTexture_4() { return &___LookupTexture_4; }
	inline void set_LookupTexture_4(Texture2D_t3840446185 * value)
	{
		___LookupTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___LookupTexture_4), value);
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}

	inline static int32_t get_offset_of_ForceCompatibility_6() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___ForceCompatibility_6)); }
	inline bool get_ForceCompatibility_6() const { return ___ForceCompatibility_6; }
	inline bool* get_address_of_ForceCompatibility_6() { return &___ForceCompatibility_6; }
	inline void set_ForceCompatibility_6(bool value)
	{
		___ForceCompatibility_6 = value;
	}

	inline static int32_t get_offset_of_m_Lut3D_7() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Lut3D_7)); }
	inline Texture3D_t1884131049 * get_m_Lut3D_7() const { return ___m_Lut3D_7; }
	inline Texture3D_t1884131049 ** get_address_of_m_Lut3D_7() { return &___m_Lut3D_7; }
	inline void set_m_Lut3D_7(Texture3D_t1884131049 * value)
	{
		___m_Lut3D_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lut3D_7), value);
	}

	inline static int32_t get_offset_of_m_BaseTextureName_8() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_BaseTextureName_8)); }
	inline String_t* get_m_BaseTextureName_8() const { return ___m_BaseTextureName_8; }
	inline String_t** get_address_of_m_BaseTextureName_8() { return &___m_BaseTextureName_8; }
	inline void set_m_BaseTextureName_8(String_t* value)
	{
		___m_BaseTextureName_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseTextureName_8), value);
	}

	inline static int32_t get_offset_of_m_Use2DLut_9() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Use2DLut_9)); }
	inline bool get_m_Use2DLut_9() const { return ___m_Use2DLut_9; }
	inline bool* get_address_of_m_Use2DLut_9() { return &___m_Use2DLut_9; }
	inline void set_m_Use2DLut_9(bool value)
	{
		___m_Use2DLut_9 = value;
	}

	inline static int32_t get_offset_of_Shader2D_10() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___Shader2D_10)); }
	inline Shader_t4151988712 * get_Shader2D_10() const { return ___Shader2D_10; }
	inline Shader_t4151988712 ** get_address_of_Shader2D_10() { return &___Shader2D_10; }
	inline void set_Shader2D_10(Shader_t4151988712 * value)
	{
		___Shader2D_10 = value;
		Il2CppCodeGenWriteBarrier((&___Shader2D_10), value);
	}

	inline static int32_t get_offset_of_Shader3D_11() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___Shader3D_11)); }
	inline Shader_t4151988712 * get_Shader3D_11() const { return ___Shader3D_11; }
	inline Shader_t4151988712 ** get_address_of_Shader3D_11() { return &___Shader3D_11; }
	inline void set_Shader3D_11(Shader_t4151988712 * value)
	{
		___Shader3D_11 = value;
		Il2CppCodeGenWriteBarrier((&___Shader3D_11), value);
	}

	inline static int32_t get_offset_of_m_Material2D_12() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Material2D_12)); }
	inline Material_t340375123 * get_m_Material2D_12() const { return ___m_Material2D_12; }
	inline Material_t340375123 ** get_address_of_m_Material2D_12() { return &___m_Material2D_12; }
	inline void set_m_Material2D_12(Material_t340375123 * value)
	{
		___m_Material2D_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material2D_12), value);
	}

	inline static int32_t get_offset_of_m_Material3D_13() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Material3D_13)); }
	inline Material_t340375123 * get_m_Material3D_13() const { return ___m_Material3D_13; }
	inline Material_t340375123 ** get_address_of_m_Material3D_13() { return &___m_Material3D_13; }
	inline void set_m_Material3D_13(Material_t340375123 * value)
	{
		___m_Material3D_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material3D_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKUPFILTER3D_T3361124730_H
#ifndef FOLLOWTARGET_T1456261924_H
#define FOLLOWTARGET_T1456261924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTarget
struct  FollowTarget_t1456261924  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FollowTarget::target
	Transform_t3600365921 * ___target_4;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(FollowTarget_t1456261924, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T1456261924_H
#ifndef NYARSETUP_T1519484188_H
#define NYARSETUP_T1519484188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NYARSetup
struct  NYARSetup_t1519484188  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera NYARSetup::BGCam
	Camera_t4157153871 * ___BGCam_5;
	// UnityEngine.Material NYARSetup::projectorMat
	Material_t340375123 * ___projectorMat_6;
	// UnityEngine.LayerMask NYARSetup::projectorIgnoreLayerMask
	LayerMask_t3493934918  ___projectorIgnoreLayerMask_7;
	// UnityEngine.LayerMask NYARSetup::bgCamLayerMask
	LayerMask_t3493934918  ___bgCamLayerMask_8;
	// UnityEngine.Texture2D NYARSetup::arCameraScreenShot
	Texture2D_t3840446185 * ___arCameraScreenShot_9;
	// RenderTextureToProjector NYARSetup::RTtPScript
	RenderTextureToProjector_t1529163424 * ___RTtPScript_10;
	// UnityEngine.UI.RawImage NYARSetup::uiFrontCameraImage
	RawImage_t3182918964 * ___uiFrontCameraImage_11;

public:
	inline static int32_t get_offset_of_BGCam_5() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___BGCam_5)); }
	inline Camera_t4157153871 * get_BGCam_5() const { return ___BGCam_5; }
	inline Camera_t4157153871 ** get_address_of_BGCam_5() { return &___BGCam_5; }
	inline void set_BGCam_5(Camera_t4157153871 * value)
	{
		___BGCam_5 = value;
		Il2CppCodeGenWriteBarrier((&___BGCam_5), value);
	}

	inline static int32_t get_offset_of_projectorMat_6() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___projectorMat_6)); }
	inline Material_t340375123 * get_projectorMat_6() const { return ___projectorMat_6; }
	inline Material_t340375123 ** get_address_of_projectorMat_6() { return &___projectorMat_6; }
	inline void set_projectorMat_6(Material_t340375123 * value)
	{
		___projectorMat_6 = value;
		Il2CppCodeGenWriteBarrier((&___projectorMat_6), value);
	}

	inline static int32_t get_offset_of_projectorIgnoreLayerMask_7() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___projectorIgnoreLayerMask_7)); }
	inline LayerMask_t3493934918  get_projectorIgnoreLayerMask_7() const { return ___projectorIgnoreLayerMask_7; }
	inline LayerMask_t3493934918 * get_address_of_projectorIgnoreLayerMask_7() { return &___projectorIgnoreLayerMask_7; }
	inline void set_projectorIgnoreLayerMask_7(LayerMask_t3493934918  value)
	{
		___projectorIgnoreLayerMask_7 = value;
	}

	inline static int32_t get_offset_of_bgCamLayerMask_8() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___bgCamLayerMask_8)); }
	inline LayerMask_t3493934918  get_bgCamLayerMask_8() const { return ___bgCamLayerMask_8; }
	inline LayerMask_t3493934918 * get_address_of_bgCamLayerMask_8() { return &___bgCamLayerMask_8; }
	inline void set_bgCamLayerMask_8(LayerMask_t3493934918  value)
	{
		___bgCamLayerMask_8 = value;
	}

	inline static int32_t get_offset_of_arCameraScreenShot_9() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___arCameraScreenShot_9)); }
	inline Texture2D_t3840446185 * get_arCameraScreenShot_9() const { return ___arCameraScreenShot_9; }
	inline Texture2D_t3840446185 ** get_address_of_arCameraScreenShot_9() { return &___arCameraScreenShot_9; }
	inline void set_arCameraScreenShot_9(Texture2D_t3840446185 * value)
	{
		___arCameraScreenShot_9 = value;
		Il2CppCodeGenWriteBarrier((&___arCameraScreenShot_9), value);
	}

	inline static int32_t get_offset_of_RTtPScript_10() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___RTtPScript_10)); }
	inline RenderTextureToProjector_t1529163424 * get_RTtPScript_10() const { return ___RTtPScript_10; }
	inline RenderTextureToProjector_t1529163424 ** get_address_of_RTtPScript_10() { return &___RTtPScript_10; }
	inline void set_RTtPScript_10(RenderTextureToProjector_t1529163424 * value)
	{
		___RTtPScript_10 = value;
		Il2CppCodeGenWriteBarrier((&___RTtPScript_10), value);
	}

	inline static int32_t get_offset_of_uiFrontCameraImage_11() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188, ___uiFrontCameraImage_11)); }
	inline RawImage_t3182918964 * get_uiFrontCameraImage_11() const { return ___uiFrontCameraImage_11; }
	inline RawImage_t3182918964 ** get_address_of_uiFrontCameraImage_11() { return &___uiFrontCameraImage_11; }
	inline void set_uiFrontCameraImage_11(RawImage_t3182918964 * value)
	{
		___uiFrontCameraImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___uiFrontCameraImage_11), value);
	}
};

struct NYARSetup_t1519484188_StaticFields
{
public:
	// NYARSetup NYARSetup::Instance
	NYARSetup_t1519484188 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(NYARSetup_t1519484188_StaticFields, ___Instance_4)); }
	inline NYARSetup_t1519484188 * get_Instance_4() const { return ___Instance_4; }
	inline NYARSetup_t1519484188 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(NYARSetup_t1519484188 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NYARSETUP_T1519484188_H
#ifndef PROJECTORCOPYCAMERADATA_T3967883643_H
#define PROJECTORCOPYCAMERADATA_T3967883643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectorCopyCameraData
struct  ProjectorCopyCameraData_t3967883643  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ProjectorCopyCameraData::targetCamera
	Camera_t4157153871 * ___targetCamera_4;

public:
	inline static int32_t get_offset_of_targetCamera_4() { return static_cast<int32_t>(offsetof(ProjectorCopyCameraData_t3967883643, ___targetCamera_4)); }
	inline Camera_t4157153871 * get_targetCamera_4() const { return ___targetCamera_4; }
	inline Camera_t4157153871 ** get_address_of_targetCamera_4() { return &___targetCamera_4; }
	inline void set_targetCamera_4(Camera_t4157153871 * value)
	{
		___targetCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetCamera_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTORCOPYCAMERADATA_T3967883643_H
#ifndef RENDERTEXTURETOPROJECTOR_T1529163424_H
#define RENDERTEXTURETOPROJECTOR_T1529163424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderTextureToProjector
struct  RenderTextureToProjector_t1529163424  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RenderTexture RenderTextureToProjector::_texture
	RenderTexture_t2108887433 * ____texture_4;
	// UnityEngine.Projector RenderTextureToProjector::_projector
	Projector_t2748997877 * ____projector_5;

public:
	inline static int32_t get_offset_of__texture_4() { return static_cast<int32_t>(offsetof(RenderTextureToProjector_t1529163424, ____texture_4)); }
	inline RenderTexture_t2108887433 * get__texture_4() const { return ____texture_4; }
	inline RenderTexture_t2108887433 ** get_address_of__texture_4() { return &____texture_4; }
	inline void set__texture_4(RenderTexture_t2108887433 * value)
	{
		____texture_4 = value;
		Il2CppCodeGenWriteBarrier((&____texture_4), value);
	}

	inline static int32_t get_offset_of__projector_5() { return static_cast<int32_t>(offsetof(RenderTextureToProjector_t1529163424, ____projector_5)); }
	inline Projector_t2748997877 * get__projector_5() const { return ____projector_5; }
	inline Projector_t2748997877 ** get_address_of__projector_5() { return &____projector_5; }
	inline void set__projector_5(Projector_t2748997877 * value)
	{
		____projector_5 = value;
		Il2CppCodeGenWriteBarrier((&____projector_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURETOPROJECTOR_T1529163424_H
#ifndef TEST_T650638817_H
#define TEST_T650638817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Test
struct  Test_t650638817  : public MonoBehaviour_t3962482529
{
public:
	// System.String Test::ScreenshotName
	String_t* ___ScreenshotName_4;

public:
	inline static int32_t get_offset_of_ScreenshotName_4() { return static_cast<int32_t>(offsetof(Test_t650638817, ___ScreenshotName_4)); }
	inline String_t* get_ScreenshotName_4() const { return ___ScreenshotName_4; }
	inline String_t** get_address_of_ScreenshotName_4() { return &___ScreenshotName_4; }
	inline void set_ScreenshotName_4(String_t* value)
	{
		___ScreenshotName_4 = value;
		Il2CppCodeGenWriteBarrier((&___ScreenshotName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEST_T650638817_H
#ifndef CAMERA_FOLLOW_DANCE_PACK_05_T545415106_H
#define CAMERA_FOLLOW_DANCE_PACK_05_T545415106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// camera_follow_dance_pack_05
struct  camera_follow_dance_pack_05_t545415106  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject camera_follow_dance_pack_05::player
	GameObject_t1113636619 * ___player_4;
	// System.Single camera_follow_dance_pack_05::cameraHeight
	float ___cameraHeight_5;
	// System.Single camera_follow_dance_pack_05::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(camera_follow_dance_pack_05_t545415106, ___player_4)); }
	inline GameObject_t1113636619 * get_player_4() const { return ___player_4; }
	inline GameObject_t1113636619 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1113636619 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(camera_follow_dance_pack_05_t545415106, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(camera_follow_dance_pack_05_t545415106, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_FOLLOW_DANCE_PACK_05_T545415106_H
#ifndef DANCE_MOCAP_01_FOLLOW_PLAYER_T692022596_H
#define DANCE_MOCAP_01_FOLLOW_PLAYER_T692022596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// dance_mocap_01_follow_player
struct  dance_mocap_01_follow_player_t692022596  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject dance_mocap_01_follow_player::Player
	GameObject_t1113636619 * ___Player_4;
	// System.Single dance_mocap_01_follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single dance_mocap_01_follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(dance_mocap_01_follow_player_t692022596, ___Player_4)); }
	inline GameObject_t1113636619 * get_Player_4() const { return ___Player_4; }
	inline GameObject_t1113636619 ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_t1113636619 * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(dance_mocap_01_follow_player_t692022596, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(dance_mocap_01_follow_player_t692022596, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DANCE_MOCAP_01_FOLLOW_PLAYER_T692022596_H
#ifndef DANCE_MOCAP_02_FOLLOW_PLAYER_T3181538630_H
#define DANCE_MOCAP_02_FOLLOW_PLAYER_T3181538630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// dance_mocap_02_follow_player
struct  dance_mocap_02_follow_player_t3181538630  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject dance_mocap_02_follow_player::Player
	GameObject_t1113636619 * ___Player_4;
	// System.Single dance_mocap_02_follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single dance_mocap_02_follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(dance_mocap_02_follow_player_t3181538630, ___Player_4)); }
	inline GameObject_t1113636619 * get_Player_4() const { return ___Player_4; }
	inline GameObject_t1113636619 ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_t1113636619 * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(dance_mocap_02_follow_player_t3181538630, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(dance_mocap_02_follow_player_t3181538630, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DANCE_MOCAP_02_FOLLOW_PLAYER_T3181538630_H
#ifndef DANCE_MOCAP_03_FOLLOW_PLAYER_T545090885_H
#define DANCE_MOCAP_03_FOLLOW_PLAYER_T545090885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// dance_mocap_03_follow_player
struct  dance_mocap_03_follow_player_t545090885  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject dance_mocap_03_follow_player::Player
	GameObject_t1113636619 * ___Player_4;
	// System.Single dance_mocap_03_follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single dance_mocap_03_follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(dance_mocap_03_follow_player_t545090885, ___Player_4)); }
	inline GameObject_t1113636619 * get_Player_4() const { return ___Player_4; }
	inline GameObject_t1113636619 ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_t1113636619 * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(dance_mocap_03_follow_player_t545090885, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(dance_mocap_03_follow_player_t545090885, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DANCE_MOCAP_03_FOLLOW_PLAYER_T545090885_H
#ifndef FOLLOW_PLAYER_T1274992039_H
#define FOLLOW_PLAYER_T1274992039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// follow_player
struct  follow_player_t1274992039  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject follow_player::player
	GameObject_t1113636619 * ___player_4;
	// System.Single follow_player::cameraHeight
	float ___cameraHeight_5;
	// System.Single follow_player::cameraDistance
	float ___cameraDistance_6;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(follow_player_t1274992039, ___player_4)); }
	inline GameObject_t1113636619 * get_player_4() const { return ___player_4; }
	inline GameObject_t1113636619 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1113636619 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_cameraHeight_5() { return static_cast<int32_t>(offsetof(follow_player_t1274992039, ___cameraHeight_5)); }
	inline float get_cameraHeight_5() const { return ___cameraHeight_5; }
	inline float* get_address_of_cameraHeight_5() { return &___cameraHeight_5; }
	inline void set_cameraHeight_5(float value)
	{
		___cameraHeight_5 = value;
	}

	inline static int32_t get_offset_of_cameraDistance_6() { return static_cast<int32_t>(offsetof(follow_player_t1274992039, ___cameraDistance_6)); }
	inline float get_cameraDistance_6() const { return ___cameraDistance_6; }
	inline float* get_address_of_cameraDistance_6() { return &___cameraDistance_6; }
	inline void set_cameraDistance_6(float value)
	{
		___cameraDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOW_PLAYER_T1274992039_H
#ifndef ANALOGTV_T1398652140_H
#define ANALOGTV_T1398652140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.AnalogTV
struct  AnalogTV_t1398652140  : public BaseEffect_t1187847871
{
public:
	// System.Boolean Colorful.AnalogTV::AutomaticPhase
	bool ___AutomaticPhase_6;
	// System.Single Colorful.AnalogTV::Phase
	float ___Phase_7;
	// System.Boolean Colorful.AnalogTV::ConvertToGrayscale
	bool ___ConvertToGrayscale_8;
	// System.Single Colorful.AnalogTV::NoiseIntensity
	float ___NoiseIntensity_9;
	// System.Single Colorful.AnalogTV::ScanlinesIntensity
	float ___ScanlinesIntensity_10;
	// System.Int32 Colorful.AnalogTV::ScanlinesCount
	int32_t ___ScanlinesCount_11;
	// System.Single Colorful.AnalogTV::ScanlinesOffset
	float ___ScanlinesOffset_12;
	// System.Boolean Colorful.AnalogTV::VerticalScanlines
	bool ___VerticalScanlines_13;
	// System.Single Colorful.AnalogTV::Distortion
	float ___Distortion_14;
	// System.Single Colorful.AnalogTV::CubicDistortion
	float ___CubicDistortion_15;
	// System.Single Colorful.AnalogTV::Scale
	float ___Scale_16;

public:
	inline static int32_t get_offset_of_AutomaticPhase_6() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___AutomaticPhase_6)); }
	inline bool get_AutomaticPhase_6() const { return ___AutomaticPhase_6; }
	inline bool* get_address_of_AutomaticPhase_6() { return &___AutomaticPhase_6; }
	inline void set_AutomaticPhase_6(bool value)
	{
		___AutomaticPhase_6 = value;
	}

	inline static int32_t get_offset_of_Phase_7() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___Phase_7)); }
	inline float get_Phase_7() const { return ___Phase_7; }
	inline float* get_address_of_Phase_7() { return &___Phase_7; }
	inline void set_Phase_7(float value)
	{
		___Phase_7 = value;
	}

	inline static int32_t get_offset_of_ConvertToGrayscale_8() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ConvertToGrayscale_8)); }
	inline bool get_ConvertToGrayscale_8() const { return ___ConvertToGrayscale_8; }
	inline bool* get_address_of_ConvertToGrayscale_8() { return &___ConvertToGrayscale_8; }
	inline void set_ConvertToGrayscale_8(bool value)
	{
		___ConvertToGrayscale_8 = value;
	}

	inline static int32_t get_offset_of_NoiseIntensity_9() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___NoiseIntensity_9)); }
	inline float get_NoiseIntensity_9() const { return ___NoiseIntensity_9; }
	inline float* get_address_of_NoiseIntensity_9() { return &___NoiseIntensity_9; }
	inline void set_NoiseIntensity_9(float value)
	{
		___NoiseIntensity_9 = value;
	}

	inline static int32_t get_offset_of_ScanlinesIntensity_10() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ScanlinesIntensity_10)); }
	inline float get_ScanlinesIntensity_10() const { return ___ScanlinesIntensity_10; }
	inline float* get_address_of_ScanlinesIntensity_10() { return &___ScanlinesIntensity_10; }
	inline void set_ScanlinesIntensity_10(float value)
	{
		___ScanlinesIntensity_10 = value;
	}

	inline static int32_t get_offset_of_ScanlinesCount_11() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ScanlinesCount_11)); }
	inline int32_t get_ScanlinesCount_11() const { return ___ScanlinesCount_11; }
	inline int32_t* get_address_of_ScanlinesCount_11() { return &___ScanlinesCount_11; }
	inline void set_ScanlinesCount_11(int32_t value)
	{
		___ScanlinesCount_11 = value;
	}

	inline static int32_t get_offset_of_ScanlinesOffset_12() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ScanlinesOffset_12)); }
	inline float get_ScanlinesOffset_12() const { return ___ScanlinesOffset_12; }
	inline float* get_address_of_ScanlinesOffset_12() { return &___ScanlinesOffset_12; }
	inline void set_ScanlinesOffset_12(float value)
	{
		___ScanlinesOffset_12 = value;
	}

	inline static int32_t get_offset_of_VerticalScanlines_13() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___VerticalScanlines_13)); }
	inline bool get_VerticalScanlines_13() const { return ___VerticalScanlines_13; }
	inline bool* get_address_of_VerticalScanlines_13() { return &___VerticalScanlines_13; }
	inline void set_VerticalScanlines_13(bool value)
	{
		___VerticalScanlines_13 = value;
	}

	inline static int32_t get_offset_of_Distortion_14() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___Distortion_14)); }
	inline float get_Distortion_14() const { return ___Distortion_14; }
	inline float* get_address_of_Distortion_14() { return &___Distortion_14; }
	inline void set_Distortion_14(float value)
	{
		___Distortion_14 = value;
	}

	inline static int32_t get_offset_of_CubicDistortion_15() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___CubicDistortion_15)); }
	inline float get_CubicDistortion_15() const { return ___CubicDistortion_15; }
	inline float* get_address_of_CubicDistortion_15() { return &___CubicDistortion_15; }
	inline void set_CubicDistortion_15(float value)
	{
		___CubicDistortion_15 = value;
	}

	inline static int32_t get_offset_of_Scale_16() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___Scale_16)); }
	inline float get_Scale_16() const { return ___Scale_16; }
	inline float* get_address_of_Scale_16() { return &___Scale_16; }
	inline void set_Scale_16(float value)
	{
		___Scale_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALOGTV_T1398652140_H
#ifndef BILATERALGAUSSIANBLUR_T938878669_H
#define BILATERALGAUSSIANBLUR_T938878669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BilateralGaussianBlur
struct  BilateralGaussianBlur_t938878669  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.BilateralGaussianBlur::Passes
	int32_t ___Passes_6;
	// System.Single Colorful.BilateralGaussianBlur::Threshold
	float ___Threshold_7;
	// System.Single Colorful.BilateralGaussianBlur::Amount
	float ___Amount_8;

public:
	inline static int32_t get_offset_of_Passes_6() { return static_cast<int32_t>(offsetof(BilateralGaussianBlur_t938878669, ___Passes_6)); }
	inline int32_t get_Passes_6() const { return ___Passes_6; }
	inline int32_t* get_address_of_Passes_6() { return &___Passes_6; }
	inline void set_Passes_6(int32_t value)
	{
		___Passes_6 = value;
	}

	inline static int32_t get_offset_of_Threshold_7() { return static_cast<int32_t>(offsetof(BilateralGaussianBlur_t938878669, ___Threshold_7)); }
	inline float get_Threshold_7() const { return ___Threshold_7; }
	inline float* get_address_of_Threshold_7() { return &___Threshold_7; }
	inline void set_Threshold_7(float value)
	{
		___Threshold_7 = value;
	}

	inline static int32_t get_offset_of_Amount_8() { return static_cast<int32_t>(offsetof(BilateralGaussianBlur_t938878669, ___Amount_8)); }
	inline float get_Amount_8() const { return ___Amount_8; }
	inline float* get_address_of_Amount_8() { return &___Amount_8; }
	inline void set_Amount_8(float value)
	{
		___Amount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILATERALGAUSSIANBLUR_T938878669_H
#ifndef BLEACHBYPASS_T2568391998_H
#define BLEACHBYPASS_T2568391998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BleachBypass
struct  BleachBypass_t2568391998  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.BleachBypass::Amount
	float ___Amount_6;

public:
	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(BleachBypass_t2568391998, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLEACHBYPASS_T2568391998_H
#ifndef BLEND_T936097170_H
#define BLEND_T936097170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Blend
struct  Blend_t936097170  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Texture Colorful.Blend::Texture
	Texture_t3661962703 * ___Texture_6;
	// System.Single Colorful.Blend::Amount
	float ___Amount_7;
	// Colorful.Blend/BlendingMode Colorful.Blend::Mode
	int32_t ___Mode_8;

public:
	inline static int32_t get_offset_of_Texture_6() { return static_cast<int32_t>(offsetof(Blend_t936097170, ___Texture_6)); }
	inline Texture_t3661962703 * get_Texture_6() const { return ___Texture_6; }
	inline Texture_t3661962703 ** get_address_of_Texture_6() { return &___Texture_6; }
	inline void set_Texture_6(Texture_t3661962703 * value)
	{
		___Texture_6 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_6), value);
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(Blend_t936097170, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}

	inline static int32_t get_offset_of_Mode_8() { return static_cast<int32_t>(offsetof(Blend_t936097170, ___Mode_8)); }
	inline int32_t get_Mode_8() const { return ___Mode_8; }
	inline int32_t* get_address_of_Mode_8() { return &___Mode_8; }
	inline void set_Mode_8(int32_t value)
	{
		___Mode_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLEND_T936097170_H
#ifndef BRIGHTNESSCONTRASTGAMMA_T158109514_H
#define BRIGHTNESSCONTRASTGAMMA_T158109514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BrightnessContrastGamma
struct  BrightnessContrastGamma_t158109514  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.BrightnessContrastGamma::Brightness
	float ___Brightness_6;
	// System.Single Colorful.BrightnessContrastGamma::Contrast
	float ___Contrast_7;
	// UnityEngine.Vector3 Colorful.BrightnessContrastGamma::ContrastCoeff
	Vector3_t3722313464  ___ContrastCoeff_8;
	// System.Single Colorful.BrightnessContrastGamma::Gamma
	float ___Gamma_9;

public:
	inline static int32_t get_offset_of_Brightness_6() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___Brightness_6)); }
	inline float get_Brightness_6() const { return ___Brightness_6; }
	inline float* get_address_of_Brightness_6() { return &___Brightness_6; }
	inline void set_Brightness_6(float value)
	{
		___Brightness_6 = value;
	}

	inline static int32_t get_offset_of_Contrast_7() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___Contrast_7)); }
	inline float get_Contrast_7() const { return ___Contrast_7; }
	inline float* get_address_of_Contrast_7() { return &___Contrast_7; }
	inline void set_Contrast_7(float value)
	{
		___Contrast_7 = value;
	}

	inline static int32_t get_offset_of_ContrastCoeff_8() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___ContrastCoeff_8)); }
	inline Vector3_t3722313464  get_ContrastCoeff_8() const { return ___ContrastCoeff_8; }
	inline Vector3_t3722313464 * get_address_of_ContrastCoeff_8() { return &___ContrastCoeff_8; }
	inline void set_ContrastCoeff_8(Vector3_t3722313464  value)
	{
		___ContrastCoeff_8 = value;
	}

	inline static int32_t get_offset_of_Gamma_9() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___Gamma_9)); }
	inline float get_Gamma_9() const { return ___Gamma_9; }
	inline float* get_address_of_Gamma_9() { return &___Gamma_9; }
	inline void set_Gamma_9(float value)
	{
		___Gamma_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESSCONTRASTGAMMA_T158109514_H
#ifndef CHANNELCLAMPER_T4227510363_H
#define CHANNELCLAMPER_T4227510363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelClamper
struct  ChannelClamper_t4227510363  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector2 Colorful.ChannelClamper::Red
	Vector2_t2156229523  ___Red_6;
	// UnityEngine.Vector2 Colorful.ChannelClamper::Green
	Vector2_t2156229523  ___Green_7;
	// UnityEngine.Vector2 Colorful.ChannelClamper::Blue
	Vector2_t2156229523  ___Blue_8;

public:
	inline static int32_t get_offset_of_Red_6() { return static_cast<int32_t>(offsetof(ChannelClamper_t4227510363, ___Red_6)); }
	inline Vector2_t2156229523  get_Red_6() const { return ___Red_6; }
	inline Vector2_t2156229523 * get_address_of_Red_6() { return &___Red_6; }
	inline void set_Red_6(Vector2_t2156229523  value)
	{
		___Red_6 = value;
	}

	inline static int32_t get_offset_of_Green_7() { return static_cast<int32_t>(offsetof(ChannelClamper_t4227510363, ___Green_7)); }
	inline Vector2_t2156229523  get_Green_7() const { return ___Green_7; }
	inline Vector2_t2156229523 * get_address_of_Green_7() { return &___Green_7; }
	inline void set_Green_7(Vector2_t2156229523  value)
	{
		___Green_7 = value;
	}

	inline static int32_t get_offset_of_Blue_8() { return static_cast<int32_t>(offsetof(ChannelClamper_t4227510363, ___Blue_8)); }
	inline Vector2_t2156229523  get_Blue_8() const { return ___Blue_8; }
	inline Vector2_t2156229523 * get_address_of_Blue_8() { return &___Blue_8; }
	inline void set_Blue_8(Vector2_t2156229523  value)
	{
		___Blue_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELCLAMPER_T4227510363_H
#ifndef CHANNELMIXER_T2404774860_H
#define CHANNELMIXER_T2404774860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelMixer
struct  ChannelMixer_t2404774860  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector3 Colorful.ChannelMixer::Red
	Vector3_t3722313464  ___Red_6;
	// UnityEngine.Vector3 Colorful.ChannelMixer::Green
	Vector3_t3722313464  ___Green_7;
	// UnityEngine.Vector3 Colorful.ChannelMixer::Blue
	Vector3_t3722313464  ___Blue_8;
	// UnityEngine.Vector3 Colorful.ChannelMixer::Constant
	Vector3_t3722313464  ___Constant_9;

public:
	inline static int32_t get_offset_of_Red_6() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Red_6)); }
	inline Vector3_t3722313464  get_Red_6() const { return ___Red_6; }
	inline Vector3_t3722313464 * get_address_of_Red_6() { return &___Red_6; }
	inline void set_Red_6(Vector3_t3722313464  value)
	{
		___Red_6 = value;
	}

	inline static int32_t get_offset_of_Green_7() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Green_7)); }
	inline Vector3_t3722313464  get_Green_7() const { return ___Green_7; }
	inline Vector3_t3722313464 * get_address_of_Green_7() { return &___Green_7; }
	inline void set_Green_7(Vector3_t3722313464  value)
	{
		___Green_7 = value;
	}

	inline static int32_t get_offset_of_Blue_8() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Blue_8)); }
	inline Vector3_t3722313464  get_Blue_8() const { return ___Blue_8; }
	inline Vector3_t3722313464 * get_address_of_Blue_8() { return &___Blue_8; }
	inline void set_Blue_8(Vector3_t3722313464  value)
	{
		___Blue_8 = value;
	}

	inline static int32_t get_offset_of_Constant_9() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Constant_9)); }
	inline Vector3_t3722313464  get_Constant_9() const { return ___Constant_9; }
	inline Vector3_t3722313464 * get_address_of_Constant_9() { return &___Constant_9; }
	inline void set_Constant_9(Vector3_t3722313464  value)
	{
		___Constant_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXER_T2404774860_H
#ifndef CHANNELSWAPPER_T1718792779_H
#define CHANNELSWAPPER_T1718792779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelSwapper
struct  ChannelSwapper_t1718792779  : public BaseEffect_t1187847871
{
public:
	// Colorful.ChannelSwapper/Channel Colorful.ChannelSwapper::RedSource
	int32_t ___RedSource_6;
	// Colorful.ChannelSwapper/Channel Colorful.ChannelSwapper::GreenSource
	int32_t ___GreenSource_7;
	// Colorful.ChannelSwapper/Channel Colorful.ChannelSwapper::BlueSource
	int32_t ___BlueSource_8;

public:
	inline static int32_t get_offset_of_RedSource_6() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779, ___RedSource_6)); }
	inline int32_t get_RedSource_6() const { return ___RedSource_6; }
	inline int32_t* get_address_of_RedSource_6() { return &___RedSource_6; }
	inline void set_RedSource_6(int32_t value)
	{
		___RedSource_6 = value;
	}

	inline static int32_t get_offset_of_GreenSource_7() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779, ___GreenSource_7)); }
	inline int32_t get_GreenSource_7() const { return ___GreenSource_7; }
	inline int32_t* get_address_of_GreenSource_7() { return &___GreenSource_7; }
	inline void set_GreenSource_7(int32_t value)
	{
		___GreenSource_7 = value;
	}

	inline static int32_t get_offset_of_BlueSource_8() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779, ___BlueSource_8)); }
	inline int32_t get_BlueSource_8() const { return ___BlueSource_8; }
	inline int32_t* get_address_of_BlueSource_8() { return &___BlueSource_8; }
	inline void set_BlueSource_8(int32_t value)
	{
		___BlueSource_8 = value;
	}
};

struct ChannelSwapper_t1718792779_StaticFields
{
public:
	// UnityEngine.Vector4[] Colorful.ChannelSwapper::m_Channels
	Vector4U5BU5D_t934056436* ___m_Channels_9;

public:
	inline static int32_t get_offset_of_m_Channels_9() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779_StaticFields, ___m_Channels_9)); }
	inline Vector4U5BU5D_t934056436* get_m_Channels_9() const { return ___m_Channels_9; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_Channels_9() { return &___m_Channels_9; }
	inline void set_m_Channels_9(Vector4U5BU5D_t934056436* value)
	{
		___m_Channels_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELSWAPPER_T1718792779_H
#ifndef CHROMATICABERRATION_T2529873745_H
#define CHROMATICABERRATION_T2529873745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChromaticAberration
struct  ChromaticAberration_t2529873745  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.ChromaticAberration::RedRefraction
	float ___RedRefraction_6;
	// System.Single Colorful.ChromaticAberration::GreenRefraction
	float ___GreenRefraction_7;
	// System.Single Colorful.ChromaticAberration::BlueRefraction
	float ___BlueRefraction_8;
	// System.Boolean Colorful.ChromaticAberration::PreserveAlpha
	bool ___PreserveAlpha_9;

public:
	inline static int32_t get_offset_of_RedRefraction_6() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___RedRefraction_6)); }
	inline float get_RedRefraction_6() const { return ___RedRefraction_6; }
	inline float* get_address_of_RedRefraction_6() { return &___RedRefraction_6; }
	inline void set_RedRefraction_6(float value)
	{
		___RedRefraction_6 = value;
	}

	inline static int32_t get_offset_of_GreenRefraction_7() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___GreenRefraction_7)); }
	inline float get_GreenRefraction_7() const { return ___GreenRefraction_7; }
	inline float* get_address_of_GreenRefraction_7() { return &___GreenRefraction_7; }
	inline void set_GreenRefraction_7(float value)
	{
		___GreenRefraction_7 = value;
	}

	inline static int32_t get_offset_of_BlueRefraction_8() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___BlueRefraction_8)); }
	inline float get_BlueRefraction_8() const { return ___BlueRefraction_8; }
	inline float* get_address_of_BlueRefraction_8() { return &___BlueRefraction_8; }
	inline void set_BlueRefraction_8(float value)
	{
		___BlueRefraction_8 = value;
	}

	inline static int32_t get_offset_of_PreserveAlpha_9() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___PreserveAlpha_9)); }
	inline bool get_PreserveAlpha_9() const { return ___PreserveAlpha_9; }
	inline bool* get_address_of_PreserveAlpha_9() { return &___PreserveAlpha_9; }
	inline void set_PreserveAlpha_9(bool value)
	{
		___PreserveAlpha_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATION_T2529873745_H
#ifndef COMICBOOK_T2243422766_H
#define COMICBOOK_T2243422766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ComicBook
struct  ComicBook_t2243422766  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.ComicBook::StripAngle
	float ___StripAngle_6;
	// System.Single Colorful.ComicBook::StripDensity
	float ___StripDensity_7;
	// System.Single Colorful.ComicBook::StripThickness
	float ___StripThickness_8;
	// UnityEngine.Vector2 Colorful.ComicBook::StripLimits
	Vector2_t2156229523  ___StripLimits_9;
	// UnityEngine.Color Colorful.ComicBook::StripInnerColor
	Color_t2555686324  ___StripInnerColor_10;
	// UnityEngine.Color Colorful.ComicBook::StripOuterColor
	Color_t2555686324  ___StripOuterColor_11;
	// UnityEngine.Color Colorful.ComicBook::FillColor
	Color_t2555686324  ___FillColor_12;
	// UnityEngine.Color Colorful.ComicBook::BackgroundColor
	Color_t2555686324  ___BackgroundColor_13;
	// System.Boolean Colorful.ComicBook::EdgeDetection
	bool ___EdgeDetection_14;
	// System.Single Colorful.ComicBook::EdgeThreshold
	float ___EdgeThreshold_15;
	// UnityEngine.Color Colorful.ComicBook::EdgeColor
	Color_t2555686324  ___EdgeColor_16;
	// System.Single Colorful.ComicBook::Amount
	float ___Amount_17;

public:
	inline static int32_t get_offset_of_StripAngle_6() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripAngle_6)); }
	inline float get_StripAngle_6() const { return ___StripAngle_6; }
	inline float* get_address_of_StripAngle_6() { return &___StripAngle_6; }
	inline void set_StripAngle_6(float value)
	{
		___StripAngle_6 = value;
	}

	inline static int32_t get_offset_of_StripDensity_7() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripDensity_7)); }
	inline float get_StripDensity_7() const { return ___StripDensity_7; }
	inline float* get_address_of_StripDensity_7() { return &___StripDensity_7; }
	inline void set_StripDensity_7(float value)
	{
		___StripDensity_7 = value;
	}

	inline static int32_t get_offset_of_StripThickness_8() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripThickness_8)); }
	inline float get_StripThickness_8() const { return ___StripThickness_8; }
	inline float* get_address_of_StripThickness_8() { return &___StripThickness_8; }
	inline void set_StripThickness_8(float value)
	{
		___StripThickness_8 = value;
	}

	inline static int32_t get_offset_of_StripLimits_9() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripLimits_9)); }
	inline Vector2_t2156229523  get_StripLimits_9() const { return ___StripLimits_9; }
	inline Vector2_t2156229523 * get_address_of_StripLimits_9() { return &___StripLimits_9; }
	inline void set_StripLimits_9(Vector2_t2156229523  value)
	{
		___StripLimits_9 = value;
	}

	inline static int32_t get_offset_of_StripInnerColor_10() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripInnerColor_10)); }
	inline Color_t2555686324  get_StripInnerColor_10() const { return ___StripInnerColor_10; }
	inline Color_t2555686324 * get_address_of_StripInnerColor_10() { return &___StripInnerColor_10; }
	inline void set_StripInnerColor_10(Color_t2555686324  value)
	{
		___StripInnerColor_10 = value;
	}

	inline static int32_t get_offset_of_StripOuterColor_11() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripOuterColor_11)); }
	inline Color_t2555686324  get_StripOuterColor_11() const { return ___StripOuterColor_11; }
	inline Color_t2555686324 * get_address_of_StripOuterColor_11() { return &___StripOuterColor_11; }
	inline void set_StripOuterColor_11(Color_t2555686324  value)
	{
		___StripOuterColor_11 = value;
	}

	inline static int32_t get_offset_of_FillColor_12() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___FillColor_12)); }
	inline Color_t2555686324  get_FillColor_12() const { return ___FillColor_12; }
	inline Color_t2555686324 * get_address_of_FillColor_12() { return &___FillColor_12; }
	inline void set_FillColor_12(Color_t2555686324  value)
	{
		___FillColor_12 = value;
	}

	inline static int32_t get_offset_of_BackgroundColor_13() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___BackgroundColor_13)); }
	inline Color_t2555686324  get_BackgroundColor_13() const { return ___BackgroundColor_13; }
	inline Color_t2555686324 * get_address_of_BackgroundColor_13() { return &___BackgroundColor_13; }
	inline void set_BackgroundColor_13(Color_t2555686324  value)
	{
		___BackgroundColor_13 = value;
	}

	inline static int32_t get_offset_of_EdgeDetection_14() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___EdgeDetection_14)); }
	inline bool get_EdgeDetection_14() const { return ___EdgeDetection_14; }
	inline bool* get_address_of_EdgeDetection_14() { return &___EdgeDetection_14; }
	inline void set_EdgeDetection_14(bool value)
	{
		___EdgeDetection_14 = value;
	}

	inline static int32_t get_offset_of_EdgeThreshold_15() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___EdgeThreshold_15)); }
	inline float get_EdgeThreshold_15() const { return ___EdgeThreshold_15; }
	inline float* get_address_of_EdgeThreshold_15() { return &___EdgeThreshold_15; }
	inline void set_EdgeThreshold_15(float value)
	{
		___EdgeThreshold_15 = value;
	}

	inline static int32_t get_offset_of_EdgeColor_16() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___EdgeColor_16)); }
	inline Color_t2555686324  get_EdgeColor_16() const { return ___EdgeColor_16; }
	inline Color_t2555686324 * get_address_of_EdgeColor_16() { return &___EdgeColor_16; }
	inline void set_EdgeColor_16(Color_t2555686324  value)
	{
		___EdgeColor_16 = value;
	}

	inline static int32_t get_offset_of_Amount_17() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___Amount_17)); }
	inline float get_Amount_17() const { return ___Amount_17; }
	inline float* get_address_of_Amount_17() { return &___Amount_17; }
	inline void set_Amount_17(float value)
	{
		___Amount_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMICBOOK_T2243422766_H
#ifndef CONTRASTGAIN_T1677431729_H
#define CONTRASTGAIN_T1677431729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ContrastGain
struct  ContrastGain_t1677431729  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.ContrastGain::Gain
	float ___Gain_6;

public:
	inline static int32_t get_offset_of_Gain_6() { return static_cast<int32_t>(offsetof(ContrastGain_t1677431729, ___Gain_6)); }
	inline float get_Gain_6() const { return ___Gain_6; }
	inline float* get_address_of_Gain_6() { return &___Gain_6; }
	inline void set_Gain_6(float value)
	{
		___Gain_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTGAIN_T1677431729_H
#ifndef CONTRASTVIGNETTE_T2834033980_H
#define CONTRASTVIGNETTE_T2834033980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ContrastVignette
struct  ContrastVignette_t2834033980  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector2 Colorful.ContrastVignette::Center
	Vector2_t2156229523  ___Center_6;
	// System.Single Colorful.ContrastVignette::Sharpness
	float ___Sharpness_7;
	// System.Single Colorful.ContrastVignette::Darkness
	float ___Darkness_8;
	// System.Single Colorful.ContrastVignette::Contrast
	float ___Contrast_9;
	// UnityEngine.Vector3 Colorful.ContrastVignette::ContrastCoeff
	Vector3_t3722313464  ___ContrastCoeff_10;
	// System.Single Colorful.ContrastVignette::EdgeBlending
	float ___EdgeBlending_11;

public:
	inline static int32_t get_offset_of_Center_6() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Center_6)); }
	inline Vector2_t2156229523  get_Center_6() const { return ___Center_6; }
	inline Vector2_t2156229523 * get_address_of_Center_6() { return &___Center_6; }
	inline void set_Center_6(Vector2_t2156229523  value)
	{
		___Center_6 = value;
	}

	inline static int32_t get_offset_of_Sharpness_7() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Sharpness_7)); }
	inline float get_Sharpness_7() const { return ___Sharpness_7; }
	inline float* get_address_of_Sharpness_7() { return &___Sharpness_7; }
	inline void set_Sharpness_7(float value)
	{
		___Sharpness_7 = value;
	}

	inline static int32_t get_offset_of_Darkness_8() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Darkness_8)); }
	inline float get_Darkness_8() const { return ___Darkness_8; }
	inline float* get_address_of_Darkness_8() { return &___Darkness_8; }
	inline void set_Darkness_8(float value)
	{
		___Darkness_8 = value;
	}

	inline static int32_t get_offset_of_Contrast_9() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Contrast_9)); }
	inline float get_Contrast_9() const { return ___Contrast_9; }
	inline float* get_address_of_Contrast_9() { return &___Contrast_9; }
	inline void set_Contrast_9(float value)
	{
		___Contrast_9 = value;
	}

	inline static int32_t get_offset_of_ContrastCoeff_10() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___ContrastCoeff_10)); }
	inline Vector3_t3722313464  get_ContrastCoeff_10() const { return ___ContrastCoeff_10; }
	inline Vector3_t3722313464 * get_address_of_ContrastCoeff_10() { return &___ContrastCoeff_10; }
	inline void set_ContrastCoeff_10(Vector3_t3722313464  value)
	{
		___ContrastCoeff_10 = value;
	}

	inline static int32_t get_offset_of_EdgeBlending_11() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___EdgeBlending_11)); }
	inline float get_EdgeBlending_11() const { return ___EdgeBlending_11; }
	inline float* get_address_of_EdgeBlending_11() { return &___EdgeBlending_11; }
	inline void set_EdgeBlending_11(float value)
	{
		___EdgeBlending_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTVIGNETTE_T2834033980_H
#ifndef CONVOLUTION3X3_T1353237119_H
#define CONVOLUTION3X3_T1353237119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Convolution3x3
struct  Convolution3x3_t1353237119  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector3 Colorful.Convolution3x3::KernelTop
	Vector3_t3722313464  ___KernelTop_6;
	// UnityEngine.Vector3 Colorful.Convolution3x3::KernelMiddle
	Vector3_t3722313464  ___KernelMiddle_7;
	// UnityEngine.Vector3 Colorful.Convolution3x3::KernelBottom
	Vector3_t3722313464  ___KernelBottom_8;
	// System.Single Colorful.Convolution3x3::Divisor
	float ___Divisor_9;
	// System.Single Colorful.Convolution3x3::Amount
	float ___Amount_10;

public:
	inline static int32_t get_offset_of_KernelTop_6() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___KernelTop_6)); }
	inline Vector3_t3722313464  get_KernelTop_6() const { return ___KernelTop_6; }
	inline Vector3_t3722313464 * get_address_of_KernelTop_6() { return &___KernelTop_6; }
	inline void set_KernelTop_6(Vector3_t3722313464  value)
	{
		___KernelTop_6 = value;
	}

	inline static int32_t get_offset_of_KernelMiddle_7() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___KernelMiddle_7)); }
	inline Vector3_t3722313464  get_KernelMiddle_7() const { return ___KernelMiddle_7; }
	inline Vector3_t3722313464 * get_address_of_KernelMiddle_7() { return &___KernelMiddle_7; }
	inline void set_KernelMiddle_7(Vector3_t3722313464  value)
	{
		___KernelMiddle_7 = value;
	}

	inline static int32_t get_offset_of_KernelBottom_8() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___KernelBottom_8)); }
	inline Vector3_t3722313464  get_KernelBottom_8() const { return ___KernelBottom_8; }
	inline Vector3_t3722313464 * get_address_of_KernelBottom_8() { return &___KernelBottom_8; }
	inline void set_KernelBottom_8(Vector3_t3722313464  value)
	{
		___KernelBottom_8 = value;
	}

	inline static int32_t get_offset_of_Divisor_9() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___Divisor_9)); }
	inline float get_Divisor_9() const { return ___Divisor_9; }
	inline float* get_address_of_Divisor_9() { return &___Divisor_9; }
	inline void set_Divisor_9(float value)
	{
		___Divisor_9 = value;
	}

	inline static int32_t get_offset_of_Amount_10() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___Amount_10)); }
	inline float get_Amount_10() const { return ___Amount_10; }
	inline float* get_address_of_Amount_10() { return &___Amount_10; }
	inline void set_Amount_10(float value)
	{
		___Amount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVOLUTION3X3_T1353237119_H
#ifndef CROSSSTITCH_T1042481064_H
#define CROSSSTITCH_T1042481064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.CrossStitch
struct  CrossStitch_t1042481064  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.CrossStitch::Size
	int32_t ___Size_6;
	// System.Single Colorful.CrossStitch::Brightness
	float ___Brightness_7;
	// System.Boolean Colorful.CrossStitch::Invert
	bool ___Invert_8;
	// System.Boolean Colorful.CrossStitch::Pixelize
	bool ___Pixelize_9;

public:
	inline static int32_t get_offset_of_Size_6() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Size_6)); }
	inline int32_t get_Size_6() const { return ___Size_6; }
	inline int32_t* get_address_of_Size_6() { return &___Size_6; }
	inline void set_Size_6(int32_t value)
	{
		___Size_6 = value;
	}

	inline static int32_t get_offset_of_Brightness_7() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Brightness_7)); }
	inline float get_Brightness_7() const { return ___Brightness_7; }
	inline float* get_address_of_Brightness_7() { return &___Brightness_7; }
	inline void set_Brightness_7(float value)
	{
		___Brightness_7 = value;
	}

	inline static int32_t get_offset_of_Invert_8() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Invert_8)); }
	inline bool get_Invert_8() const { return ___Invert_8; }
	inline bool* get_address_of_Invert_8() { return &___Invert_8; }
	inline void set_Invert_8(bool value)
	{
		___Invert_8 = value;
	}

	inline static int32_t get_offset_of_Pixelize_9() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Pixelize_9)); }
	inline bool get_Pixelize_9() const { return ___Pixelize_9; }
	inline bool* get_address_of_Pixelize_9() { return &___Pixelize_9; }
	inline void set_Pixelize_9(bool value)
	{
		___Pixelize_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSSTITCH_T1042481064_H
#ifndef DIRECTIONALBLUR_T13325309_H
#define DIRECTIONALBLUR_T13325309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DirectionalBlur
struct  DirectionalBlur_t13325309  : public BaseEffect_t1187847871
{
public:
	// Colorful.DirectionalBlur/QualityPreset Colorful.DirectionalBlur::Quality
	int32_t ___Quality_6;
	// System.Int32 Colorful.DirectionalBlur::Samples
	int32_t ___Samples_7;
	// System.Single Colorful.DirectionalBlur::Strength
	float ___Strength_8;
	// System.Single Colorful.DirectionalBlur::Angle
	float ___Angle_9;

public:
	inline static int32_t get_offset_of_Quality_6() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Quality_6)); }
	inline int32_t get_Quality_6() const { return ___Quality_6; }
	inline int32_t* get_address_of_Quality_6() { return &___Quality_6; }
	inline void set_Quality_6(int32_t value)
	{
		___Quality_6 = value;
	}

	inline static int32_t get_offset_of_Samples_7() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Samples_7)); }
	inline int32_t get_Samples_7() const { return ___Samples_7; }
	inline int32_t* get_address_of_Samples_7() { return &___Samples_7; }
	inline void set_Samples_7(int32_t value)
	{
		___Samples_7 = value;
	}

	inline static int32_t get_offset_of_Strength_8() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Strength_8)); }
	inline float get_Strength_8() const { return ___Strength_8; }
	inline float* get_address_of_Strength_8() { return &___Strength_8; }
	inline void set_Strength_8(float value)
	{
		___Strength_8 = value;
	}

	inline static int32_t get_offset_of_Angle_9() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Angle_9)); }
	inline float get_Angle_9() const { return ___Angle_9; }
	inline float* get_address_of_Angle_9() { return &___Angle_9; }
	inline void set_Angle_9(float value)
	{
		___Angle_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONALBLUR_T13325309_H
#ifndef DITHERING_T358002770_H
#define DITHERING_T358002770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Dithering
struct  Dithering_t358002770  : public BaseEffect_t1187847871
{
public:
	// System.Boolean Colorful.Dithering::ShowOriginal
	bool ___ShowOriginal_6;
	// System.Boolean Colorful.Dithering::ConvertToGrayscale
	bool ___ConvertToGrayscale_7;
	// System.Single Colorful.Dithering::RedLuminance
	float ___RedLuminance_8;
	// System.Single Colorful.Dithering::GreenLuminance
	float ___GreenLuminance_9;
	// System.Single Colorful.Dithering::BlueLuminance
	float ___BlueLuminance_10;
	// System.Single Colorful.Dithering::Amount
	float ___Amount_11;
	// UnityEngine.Texture2D Colorful.Dithering::m_DitherPattern
	Texture2D_t3840446185 * ___m_DitherPattern_12;

public:
	inline static int32_t get_offset_of_ShowOriginal_6() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___ShowOriginal_6)); }
	inline bool get_ShowOriginal_6() const { return ___ShowOriginal_6; }
	inline bool* get_address_of_ShowOriginal_6() { return &___ShowOriginal_6; }
	inline void set_ShowOriginal_6(bool value)
	{
		___ShowOriginal_6 = value;
	}

	inline static int32_t get_offset_of_ConvertToGrayscale_7() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___ConvertToGrayscale_7)); }
	inline bool get_ConvertToGrayscale_7() const { return ___ConvertToGrayscale_7; }
	inline bool* get_address_of_ConvertToGrayscale_7() { return &___ConvertToGrayscale_7; }
	inline void set_ConvertToGrayscale_7(bool value)
	{
		___ConvertToGrayscale_7 = value;
	}

	inline static int32_t get_offset_of_RedLuminance_8() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___RedLuminance_8)); }
	inline float get_RedLuminance_8() const { return ___RedLuminance_8; }
	inline float* get_address_of_RedLuminance_8() { return &___RedLuminance_8; }
	inline void set_RedLuminance_8(float value)
	{
		___RedLuminance_8 = value;
	}

	inline static int32_t get_offset_of_GreenLuminance_9() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___GreenLuminance_9)); }
	inline float get_GreenLuminance_9() const { return ___GreenLuminance_9; }
	inline float* get_address_of_GreenLuminance_9() { return &___GreenLuminance_9; }
	inline void set_GreenLuminance_9(float value)
	{
		___GreenLuminance_9 = value;
	}

	inline static int32_t get_offset_of_BlueLuminance_10() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___BlueLuminance_10)); }
	inline float get_BlueLuminance_10() const { return ___BlueLuminance_10; }
	inline float* get_address_of_BlueLuminance_10() { return &___BlueLuminance_10; }
	inline void set_BlueLuminance_10(float value)
	{
		___BlueLuminance_10 = value;
	}

	inline static int32_t get_offset_of_Amount_11() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___Amount_11)); }
	inline float get_Amount_11() const { return ___Amount_11; }
	inline float* get_address_of_Amount_11() { return &___Amount_11; }
	inline void set_Amount_11(float value)
	{
		___Amount_11 = value;
	}

	inline static int32_t get_offset_of_m_DitherPattern_12() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___m_DitherPattern_12)); }
	inline Texture2D_t3840446185 * get_m_DitherPattern_12() const { return ___m_DitherPattern_12; }
	inline Texture2D_t3840446185 ** get_address_of_m_DitherPattern_12() { return &___m_DitherPattern_12; }
	inline void set_m_DitherPattern_12(Texture2D_t3840446185 * value)
	{
		___m_DitherPattern_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_DitherPattern_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERING_T358002770_H
#ifndef DOUBLEVISION_T3179333181_H
#define DOUBLEVISION_T3179333181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DoubleVision
struct  DoubleVision_t3179333181  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector2 Colorful.DoubleVision::Displace
	Vector2_t2156229523  ___Displace_6;
	// System.Single Colorful.DoubleVision::Amount
	float ___Amount_7;

public:
	inline static int32_t get_offset_of_Displace_6() { return static_cast<int32_t>(offsetof(DoubleVision_t3179333181, ___Displace_6)); }
	inline Vector2_t2156229523  get_Displace_6() const { return ___Displace_6; }
	inline Vector2_t2156229523 * get_address_of_Displace_6() { return &___Displace_6; }
	inline void set_Displace_6(Vector2_t2156229523  value)
	{
		___Displace_6 = value;
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(DoubleVision_t3179333181, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEVISION_T3179333181_H
#ifndef DYNAMICLOOKUP_T4028714612_H
#define DYNAMICLOOKUP_T4028714612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DynamicLookup
struct  DynamicLookup_t4028714612  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Color Colorful.DynamicLookup::White
	Color_t2555686324  ___White_6;
	// UnityEngine.Color Colorful.DynamicLookup::Black
	Color_t2555686324  ___Black_7;
	// UnityEngine.Color Colorful.DynamicLookup::Red
	Color_t2555686324  ___Red_8;
	// UnityEngine.Color Colorful.DynamicLookup::Green
	Color_t2555686324  ___Green_9;
	// UnityEngine.Color Colorful.DynamicLookup::Blue
	Color_t2555686324  ___Blue_10;
	// UnityEngine.Color Colorful.DynamicLookup::Yellow
	Color_t2555686324  ___Yellow_11;
	// UnityEngine.Color Colorful.DynamicLookup::Magenta
	Color_t2555686324  ___Magenta_12;
	// UnityEngine.Color Colorful.DynamicLookup::Cyan
	Color_t2555686324  ___Cyan_13;
	// System.Single Colorful.DynamicLookup::Amount
	float ___Amount_14;

public:
	inline static int32_t get_offset_of_White_6() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___White_6)); }
	inline Color_t2555686324  get_White_6() const { return ___White_6; }
	inline Color_t2555686324 * get_address_of_White_6() { return &___White_6; }
	inline void set_White_6(Color_t2555686324  value)
	{
		___White_6 = value;
	}

	inline static int32_t get_offset_of_Black_7() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Black_7)); }
	inline Color_t2555686324  get_Black_7() const { return ___Black_7; }
	inline Color_t2555686324 * get_address_of_Black_7() { return &___Black_7; }
	inline void set_Black_7(Color_t2555686324  value)
	{
		___Black_7 = value;
	}

	inline static int32_t get_offset_of_Red_8() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Red_8)); }
	inline Color_t2555686324  get_Red_8() const { return ___Red_8; }
	inline Color_t2555686324 * get_address_of_Red_8() { return &___Red_8; }
	inline void set_Red_8(Color_t2555686324  value)
	{
		___Red_8 = value;
	}

	inline static int32_t get_offset_of_Green_9() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Green_9)); }
	inline Color_t2555686324  get_Green_9() const { return ___Green_9; }
	inline Color_t2555686324 * get_address_of_Green_9() { return &___Green_9; }
	inline void set_Green_9(Color_t2555686324  value)
	{
		___Green_9 = value;
	}

	inline static int32_t get_offset_of_Blue_10() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Blue_10)); }
	inline Color_t2555686324  get_Blue_10() const { return ___Blue_10; }
	inline Color_t2555686324 * get_address_of_Blue_10() { return &___Blue_10; }
	inline void set_Blue_10(Color_t2555686324  value)
	{
		___Blue_10 = value;
	}

	inline static int32_t get_offset_of_Yellow_11() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Yellow_11)); }
	inline Color_t2555686324  get_Yellow_11() const { return ___Yellow_11; }
	inline Color_t2555686324 * get_address_of_Yellow_11() { return &___Yellow_11; }
	inline void set_Yellow_11(Color_t2555686324  value)
	{
		___Yellow_11 = value;
	}

	inline static int32_t get_offset_of_Magenta_12() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Magenta_12)); }
	inline Color_t2555686324  get_Magenta_12() const { return ___Magenta_12; }
	inline Color_t2555686324 * get_address_of_Magenta_12() { return &___Magenta_12; }
	inline void set_Magenta_12(Color_t2555686324  value)
	{
		___Magenta_12 = value;
	}

	inline static int32_t get_offset_of_Cyan_13() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Cyan_13)); }
	inline Color_t2555686324  get_Cyan_13() const { return ___Cyan_13; }
	inline Color_t2555686324 * get_address_of_Cyan_13() { return &___Cyan_13; }
	inline void set_Cyan_13(Color_t2555686324  value)
	{
		___Cyan_13 = value;
	}

	inline static int32_t get_offset_of_Amount_14() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Amount_14)); }
	inline float get_Amount_14() const { return ___Amount_14; }
	inline float* get_address_of_Amount_14() { return &___Amount_14; }
	inline void set_Amount_14(float value)
	{
		___Amount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICLOOKUP_T4028714612_H
#ifndef FASTVIGNETTE_T1694713166_H
#define FASTVIGNETTE_T1694713166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.FastVignette
struct  FastVignette_t1694713166  : public BaseEffect_t1187847871
{
public:
	// Colorful.FastVignette/ColorMode Colorful.FastVignette::Mode
	int32_t ___Mode_6;
	// UnityEngine.Color Colorful.FastVignette::Color
	Color_t2555686324  ___Color_7;
	// UnityEngine.Vector2 Colorful.FastVignette::Center
	Vector2_t2156229523  ___Center_8;
	// System.Single Colorful.FastVignette::Sharpness
	float ___Sharpness_9;
	// System.Single Colorful.FastVignette::Darkness
	float ___Darkness_10;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_Color_7() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Color_7)); }
	inline Color_t2555686324  get_Color_7() const { return ___Color_7; }
	inline Color_t2555686324 * get_address_of_Color_7() { return &___Color_7; }
	inline void set_Color_7(Color_t2555686324  value)
	{
		___Color_7 = value;
	}

	inline static int32_t get_offset_of_Center_8() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Center_8)); }
	inline Vector2_t2156229523  get_Center_8() const { return ___Center_8; }
	inline Vector2_t2156229523 * get_address_of_Center_8() { return &___Center_8; }
	inline void set_Center_8(Vector2_t2156229523  value)
	{
		___Center_8 = value;
	}

	inline static int32_t get_offset_of_Sharpness_9() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Sharpness_9)); }
	inline float get_Sharpness_9() const { return ___Sharpness_9; }
	inline float* get_address_of_Sharpness_9() { return &___Sharpness_9; }
	inline void set_Sharpness_9(float value)
	{
		___Sharpness_9 = value;
	}

	inline static int32_t get_offset_of_Darkness_10() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Darkness_10)); }
	inline float get_Darkness_10() const { return ___Darkness_10; }
	inline float* get_address_of_Darkness_10() { return &___Darkness_10; }
	inline void set_Darkness_10(float value)
	{
		___Darkness_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTVIGNETTE_T1694713166_H
#ifndef FROST_T3645176307_H
#define FROST_T3645176307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Frost
struct  Frost_t3645176307  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Frost::Scale
	float ___Scale_6;
	// System.Single Colorful.Frost::Sharpness
	float ___Sharpness_7;
	// System.Single Colorful.Frost::Darkness
	float ___Darkness_8;
	// System.Boolean Colorful.Frost::EnableVignette
	bool ___EnableVignette_9;

public:
	inline static int32_t get_offset_of_Scale_6() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___Scale_6)); }
	inline float get_Scale_6() const { return ___Scale_6; }
	inline float* get_address_of_Scale_6() { return &___Scale_6; }
	inline void set_Scale_6(float value)
	{
		___Scale_6 = value;
	}

	inline static int32_t get_offset_of_Sharpness_7() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___Sharpness_7)); }
	inline float get_Sharpness_7() const { return ___Sharpness_7; }
	inline float* get_address_of_Sharpness_7() { return &___Sharpness_7; }
	inline void set_Sharpness_7(float value)
	{
		___Sharpness_7 = value;
	}

	inline static int32_t get_offset_of_Darkness_8() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___Darkness_8)); }
	inline float get_Darkness_8() const { return ___Darkness_8; }
	inline float* get_address_of_Darkness_8() { return &___Darkness_8; }
	inline void set_Darkness_8(float value)
	{
		___Darkness_8 = value;
	}

	inline static int32_t get_offset_of_EnableVignette_9() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___EnableVignette_9)); }
	inline bool get_EnableVignette_9() const { return ___EnableVignette_9; }
	inline bool* get_address_of_EnableVignette_9() { return &___EnableVignette_9; }
	inline void set_EnableVignette_9(bool value)
	{
		___EnableVignette_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROST_T3645176307_H
#ifndef GAUSSIANBLUR_T3820976243_H
#define GAUSSIANBLUR_T3820976243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GaussianBlur
struct  GaussianBlur_t3820976243  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.GaussianBlur::Passes
	int32_t ___Passes_6;
	// System.Single Colorful.GaussianBlur::Downscaling
	float ___Downscaling_7;
	// System.Single Colorful.GaussianBlur::Amount
	float ___Amount_8;

public:
	inline static int32_t get_offset_of_Passes_6() { return static_cast<int32_t>(offsetof(GaussianBlur_t3820976243, ___Passes_6)); }
	inline int32_t get_Passes_6() const { return ___Passes_6; }
	inline int32_t* get_address_of_Passes_6() { return &___Passes_6; }
	inline void set_Passes_6(int32_t value)
	{
		___Passes_6 = value;
	}

	inline static int32_t get_offset_of_Downscaling_7() { return static_cast<int32_t>(offsetof(GaussianBlur_t3820976243, ___Downscaling_7)); }
	inline float get_Downscaling_7() const { return ___Downscaling_7; }
	inline float* get_address_of_Downscaling_7() { return &___Downscaling_7; }
	inline void set_Downscaling_7(float value)
	{
		___Downscaling_7 = value;
	}

	inline static int32_t get_offset_of_Amount_8() { return static_cast<int32_t>(offsetof(GaussianBlur_t3820976243, ___Amount_8)); }
	inline float get_Amount_8() const { return ___Amount_8; }
	inline float* get_address_of_Amount_8() { return &___Amount_8; }
	inline void set_Amount_8(float value)
	{
		___Amount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANBLUR_T3820976243_H
#ifndef GLITCH_T3656535212_H
#define GLITCH_T3656535212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch
struct  Glitch_t3656535212  : public BaseEffect_t1187847871
{
public:
	// System.Boolean Colorful.Glitch::RandomActivation
	bool ___RandomActivation_6;
	// UnityEngine.Vector2 Colorful.Glitch::RandomEvery
	Vector2_t2156229523  ___RandomEvery_7;
	// UnityEngine.Vector2 Colorful.Glitch::RandomDuration
	Vector2_t2156229523  ___RandomDuration_8;
	// Colorful.Glitch/GlitchingMode Colorful.Glitch::Mode
	int32_t ___Mode_9;
	// Colorful.Glitch/InterferenceSettings Colorful.Glitch::SettingsInterferences
	InterferenceSettings_t2118005662 * ___SettingsInterferences_10;
	// Colorful.Glitch/TearingSettings Colorful.Glitch::SettingsTearing
	TearingSettings_t1139179787 * ___SettingsTearing_11;
	// System.Boolean Colorful.Glitch::m_Activated
	bool ___m_Activated_12;
	// System.Single Colorful.Glitch::m_EveryTimer
	float ___m_EveryTimer_13;
	// System.Single Colorful.Glitch::m_EveryTimerEnd
	float ___m_EveryTimerEnd_14;
	// System.Single Colorful.Glitch::m_DurationTimer
	float ___m_DurationTimer_15;
	// System.Single Colorful.Glitch::m_DurationTimerEnd
	float ___m_DurationTimerEnd_16;

public:
	inline static int32_t get_offset_of_RandomActivation_6() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___RandomActivation_6)); }
	inline bool get_RandomActivation_6() const { return ___RandomActivation_6; }
	inline bool* get_address_of_RandomActivation_6() { return &___RandomActivation_6; }
	inline void set_RandomActivation_6(bool value)
	{
		___RandomActivation_6 = value;
	}

	inline static int32_t get_offset_of_RandomEvery_7() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___RandomEvery_7)); }
	inline Vector2_t2156229523  get_RandomEvery_7() const { return ___RandomEvery_7; }
	inline Vector2_t2156229523 * get_address_of_RandomEvery_7() { return &___RandomEvery_7; }
	inline void set_RandomEvery_7(Vector2_t2156229523  value)
	{
		___RandomEvery_7 = value;
	}

	inline static int32_t get_offset_of_RandomDuration_8() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___RandomDuration_8)); }
	inline Vector2_t2156229523  get_RandomDuration_8() const { return ___RandomDuration_8; }
	inline Vector2_t2156229523 * get_address_of_RandomDuration_8() { return &___RandomDuration_8; }
	inline void set_RandomDuration_8(Vector2_t2156229523  value)
	{
		___RandomDuration_8 = value;
	}

	inline static int32_t get_offset_of_Mode_9() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___Mode_9)); }
	inline int32_t get_Mode_9() const { return ___Mode_9; }
	inline int32_t* get_address_of_Mode_9() { return &___Mode_9; }
	inline void set_Mode_9(int32_t value)
	{
		___Mode_9 = value;
	}

	inline static int32_t get_offset_of_SettingsInterferences_10() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___SettingsInterferences_10)); }
	inline InterferenceSettings_t2118005662 * get_SettingsInterferences_10() const { return ___SettingsInterferences_10; }
	inline InterferenceSettings_t2118005662 ** get_address_of_SettingsInterferences_10() { return &___SettingsInterferences_10; }
	inline void set_SettingsInterferences_10(InterferenceSettings_t2118005662 * value)
	{
		___SettingsInterferences_10 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsInterferences_10), value);
	}

	inline static int32_t get_offset_of_SettingsTearing_11() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___SettingsTearing_11)); }
	inline TearingSettings_t1139179787 * get_SettingsTearing_11() const { return ___SettingsTearing_11; }
	inline TearingSettings_t1139179787 ** get_address_of_SettingsTearing_11() { return &___SettingsTearing_11; }
	inline void set_SettingsTearing_11(TearingSettings_t1139179787 * value)
	{
		___SettingsTearing_11 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsTearing_11), value);
	}

	inline static int32_t get_offset_of_m_Activated_12() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_Activated_12)); }
	inline bool get_m_Activated_12() const { return ___m_Activated_12; }
	inline bool* get_address_of_m_Activated_12() { return &___m_Activated_12; }
	inline void set_m_Activated_12(bool value)
	{
		___m_Activated_12 = value;
	}

	inline static int32_t get_offset_of_m_EveryTimer_13() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_EveryTimer_13)); }
	inline float get_m_EveryTimer_13() const { return ___m_EveryTimer_13; }
	inline float* get_address_of_m_EveryTimer_13() { return &___m_EveryTimer_13; }
	inline void set_m_EveryTimer_13(float value)
	{
		___m_EveryTimer_13 = value;
	}

	inline static int32_t get_offset_of_m_EveryTimerEnd_14() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_EveryTimerEnd_14)); }
	inline float get_m_EveryTimerEnd_14() const { return ___m_EveryTimerEnd_14; }
	inline float* get_address_of_m_EveryTimerEnd_14() { return &___m_EveryTimerEnd_14; }
	inline void set_m_EveryTimerEnd_14(float value)
	{
		___m_EveryTimerEnd_14 = value;
	}

	inline static int32_t get_offset_of_m_DurationTimer_15() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_DurationTimer_15)); }
	inline float get_m_DurationTimer_15() const { return ___m_DurationTimer_15; }
	inline float* get_address_of_m_DurationTimer_15() { return &___m_DurationTimer_15; }
	inline void set_m_DurationTimer_15(float value)
	{
		___m_DurationTimer_15 = value;
	}

	inline static int32_t get_offset_of_m_DurationTimerEnd_16() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_DurationTimerEnd_16)); }
	inline float get_m_DurationTimerEnd_16() const { return ___m_DurationTimerEnd_16; }
	inline float* get_address_of_m_DurationTimerEnd_16() { return &___m_DurationTimerEnd_16; }
	inline void set_m_DurationTimerEnd_16(float value)
	{
		___m_DurationTimerEnd_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCH_T3656535212_H
#ifndef GRADIENTRAMP_T513234030_H
#define GRADIENTRAMP_T513234030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GradientRamp
struct  GradientRamp_t513234030  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Texture Colorful.GradientRamp::RampTexture
	Texture_t3661962703 * ___RampTexture_6;
	// System.Single Colorful.GradientRamp::Amount
	float ___Amount_7;

public:
	inline static int32_t get_offset_of_RampTexture_6() { return static_cast<int32_t>(offsetof(GradientRamp_t513234030, ___RampTexture_6)); }
	inline Texture_t3661962703 * get_RampTexture_6() const { return ___RampTexture_6; }
	inline Texture_t3661962703 ** get_address_of_RampTexture_6() { return &___RampTexture_6; }
	inline void set_RampTexture_6(Texture_t3661962703 * value)
	{
		___RampTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___RampTexture_6), value);
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(GradientRamp_t513234030, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTRAMP_T513234030_H
#ifndef GRADIENTRAMPDYNAMIC_T2043771246_H
#define GRADIENTRAMPDYNAMIC_T2043771246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GradientRampDynamic
struct  GradientRampDynamic_t2043771246  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Gradient Colorful.GradientRampDynamic::Ramp
	Gradient_t3067099924 * ___Ramp_6;
	// System.Single Colorful.GradientRampDynamic::Amount
	float ___Amount_7;
	// UnityEngine.Texture2D Colorful.GradientRampDynamic::m_RampTexture
	Texture2D_t3840446185 * ___m_RampTexture_8;

public:
	inline static int32_t get_offset_of_Ramp_6() { return static_cast<int32_t>(offsetof(GradientRampDynamic_t2043771246, ___Ramp_6)); }
	inline Gradient_t3067099924 * get_Ramp_6() const { return ___Ramp_6; }
	inline Gradient_t3067099924 ** get_address_of_Ramp_6() { return &___Ramp_6; }
	inline void set_Ramp_6(Gradient_t3067099924 * value)
	{
		___Ramp_6 = value;
		Il2CppCodeGenWriteBarrier((&___Ramp_6), value);
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(GradientRampDynamic_t2043771246, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}

	inline static int32_t get_offset_of_m_RampTexture_8() { return static_cast<int32_t>(offsetof(GradientRampDynamic_t2043771246, ___m_RampTexture_8)); }
	inline Texture2D_t3840446185 * get_m_RampTexture_8() const { return ___m_RampTexture_8; }
	inline Texture2D_t3840446185 ** get_address_of_m_RampTexture_8() { return &___m_RampTexture_8; }
	inline void set_m_RampTexture_8(Texture2D_t3840446185 * value)
	{
		___m_RampTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RampTexture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTRAMPDYNAMIC_T2043771246_H
#ifndef GRAINYBLUR_T745788970_H
#define GRAINYBLUR_T745788970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GrainyBlur
struct  GrainyBlur_t745788970  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.GrainyBlur::Radius
	float ___Radius_6;
	// System.Int32 Colorful.GrainyBlur::Samples
	int32_t ___Samples_7;

public:
	inline static int32_t get_offset_of_Radius_6() { return static_cast<int32_t>(offsetof(GrainyBlur_t745788970, ___Radius_6)); }
	inline float get_Radius_6() const { return ___Radius_6; }
	inline float* get_address_of_Radius_6() { return &___Radius_6; }
	inline void set_Radius_6(float value)
	{
		___Radius_6 = value;
	}

	inline static int32_t get_offset_of_Samples_7() { return static_cast<int32_t>(offsetof(GrainyBlur_t745788970, ___Samples_7)); }
	inline int32_t get_Samples_7() const { return ___Samples_7; }
	inline int32_t* get_address_of_Samples_7() { return &___Samples_7; }
	inline void set_Samples_7(int32_t value)
	{
		___Samples_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINYBLUR_T745788970_H
#ifndef GRAYSCALE_T3716926545_H
#define GRAYSCALE_T3716926545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Grayscale
struct  Grayscale_t3716926545  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Grayscale::RedLuminance
	float ___RedLuminance_6;
	// System.Single Colorful.Grayscale::GreenLuminance
	float ___GreenLuminance_7;
	// System.Single Colorful.Grayscale::BlueLuminance
	float ___BlueLuminance_8;
	// System.Single Colorful.Grayscale::Amount
	float ___Amount_9;

public:
	inline static int32_t get_offset_of_RedLuminance_6() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___RedLuminance_6)); }
	inline float get_RedLuminance_6() const { return ___RedLuminance_6; }
	inline float* get_address_of_RedLuminance_6() { return &___RedLuminance_6; }
	inline void set_RedLuminance_6(float value)
	{
		___RedLuminance_6 = value;
	}

	inline static int32_t get_offset_of_GreenLuminance_7() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___GreenLuminance_7)); }
	inline float get_GreenLuminance_7() const { return ___GreenLuminance_7; }
	inline float* get_address_of_GreenLuminance_7() { return &___GreenLuminance_7; }
	inline void set_GreenLuminance_7(float value)
	{
		___GreenLuminance_7 = value;
	}

	inline static int32_t get_offset_of_BlueLuminance_8() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___BlueLuminance_8)); }
	inline float get_BlueLuminance_8() const { return ___BlueLuminance_8; }
	inline float* get_address_of_BlueLuminance_8() { return &___BlueLuminance_8; }
	inline void set_BlueLuminance_8(float value)
	{
		___BlueLuminance_8 = value;
	}

	inline static int32_t get_offset_of_Amount_9() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___Amount_9)); }
	inline float get_Amount_9() const { return ___Amount_9; }
	inline float* get_address_of_Amount_9() { return &___Amount_9; }
	inline void set_Amount_9(float value)
	{
		___Amount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAYSCALE_T3716926545_H
#ifndef HALFTONE_T1918305288_H
#define HALFTONE_T1918305288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Halftone
struct  Halftone_t1918305288  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Halftone::Scale
	float ___Scale_6;
	// System.Single Colorful.Halftone::DotSize
	float ___DotSize_7;
	// System.Single Colorful.Halftone::Angle
	float ___Angle_8;
	// System.Single Colorful.Halftone::Smoothness
	float ___Smoothness_9;
	// UnityEngine.Vector2 Colorful.Halftone::Center
	Vector2_t2156229523  ___Center_10;
	// System.Boolean Colorful.Halftone::Desaturate
	bool ___Desaturate_11;

public:
	inline static int32_t get_offset_of_Scale_6() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Scale_6)); }
	inline float get_Scale_6() const { return ___Scale_6; }
	inline float* get_address_of_Scale_6() { return &___Scale_6; }
	inline void set_Scale_6(float value)
	{
		___Scale_6 = value;
	}

	inline static int32_t get_offset_of_DotSize_7() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___DotSize_7)); }
	inline float get_DotSize_7() const { return ___DotSize_7; }
	inline float* get_address_of_DotSize_7() { return &___DotSize_7; }
	inline void set_DotSize_7(float value)
	{
		___DotSize_7 = value;
	}

	inline static int32_t get_offset_of_Angle_8() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Angle_8)); }
	inline float get_Angle_8() const { return ___Angle_8; }
	inline float* get_address_of_Angle_8() { return &___Angle_8; }
	inline void set_Angle_8(float value)
	{
		___Angle_8 = value;
	}

	inline static int32_t get_offset_of_Smoothness_9() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Smoothness_9)); }
	inline float get_Smoothness_9() const { return ___Smoothness_9; }
	inline float* get_address_of_Smoothness_9() { return &___Smoothness_9; }
	inline void set_Smoothness_9(float value)
	{
		___Smoothness_9 = value;
	}

	inline static int32_t get_offset_of_Center_10() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Center_10)); }
	inline Vector2_t2156229523  get_Center_10() const { return ___Center_10; }
	inline Vector2_t2156229523 * get_address_of_Center_10() { return &___Center_10; }
	inline void set_Center_10(Vector2_t2156229523  value)
	{
		___Center_10 = value;
	}

	inline static int32_t get_offset_of_Desaturate_11() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Desaturate_11)); }
	inline bool get_Desaturate_11() const { return ___Desaturate_11; }
	inline bool* get_address_of_Desaturate_11() { return &___Desaturate_11; }
	inline void set_Desaturate_11(bool value)
	{
		___Desaturate_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HALFTONE_T1918305288_H
#ifndef HUEFOCUS_T3289091382_H
#define HUEFOCUS_T3289091382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.HueFocus
struct  HueFocus_t3289091382  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.HueFocus::Hue
	float ___Hue_6;
	// System.Single Colorful.HueFocus::Range
	float ___Range_7;
	// System.Single Colorful.HueFocus::Boost
	float ___Boost_8;
	// System.Single Colorful.HueFocus::Amount
	float ___Amount_9;

public:
	inline static int32_t get_offset_of_Hue_6() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Hue_6)); }
	inline float get_Hue_6() const { return ___Hue_6; }
	inline float* get_address_of_Hue_6() { return &___Hue_6; }
	inline void set_Hue_6(float value)
	{
		___Hue_6 = value;
	}

	inline static int32_t get_offset_of_Range_7() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Range_7)); }
	inline float get_Range_7() const { return ___Range_7; }
	inline float* get_address_of_Range_7() { return &___Range_7; }
	inline void set_Range_7(float value)
	{
		___Range_7 = value;
	}

	inline static int32_t get_offset_of_Boost_8() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Boost_8)); }
	inline float get_Boost_8() const { return ___Boost_8; }
	inline float* get_address_of_Boost_8() { return &___Boost_8; }
	inline void set_Boost_8(float value)
	{
		___Boost_8 = value;
	}

	inline static int32_t get_offset_of_Amount_9() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Amount_9)); }
	inline float get_Amount_9() const { return ___Amount_9; }
	inline float* get_address_of_Amount_9() { return &___Amount_9; }
	inline void set_Amount_9(float value)
	{
		___Amount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUEFOCUS_T3289091382_H
#ifndef HUESATURATIONVALUE_T2444304774_H
#define HUESATURATIONVALUE_T2444304774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.HueSaturationValue
struct  HueSaturationValue_t2444304774  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.HueSaturationValue::MasterHue
	float ___MasterHue_6;
	// System.Single Colorful.HueSaturationValue::MasterSaturation
	float ___MasterSaturation_7;
	// System.Single Colorful.HueSaturationValue::MasterValue
	float ___MasterValue_8;
	// System.Single Colorful.HueSaturationValue::RedsHue
	float ___RedsHue_9;
	// System.Single Colorful.HueSaturationValue::RedsSaturation
	float ___RedsSaturation_10;
	// System.Single Colorful.HueSaturationValue::RedsValue
	float ___RedsValue_11;
	// System.Single Colorful.HueSaturationValue::YellowsHue
	float ___YellowsHue_12;
	// System.Single Colorful.HueSaturationValue::YellowsSaturation
	float ___YellowsSaturation_13;
	// System.Single Colorful.HueSaturationValue::YellowsValue
	float ___YellowsValue_14;
	// System.Single Colorful.HueSaturationValue::GreensHue
	float ___GreensHue_15;
	// System.Single Colorful.HueSaturationValue::GreensSaturation
	float ___GreensSaturation_16;
	// System.Single Colorful.HueSaturationValue::GreensValue
	float ___GreensValue_17;
	// System.Single Colorful.HueSaturationValue::CyansHue
	float ___CyansHue_18;
	// System.Single Colorful.HueSaturationValue::CyansSaturation
	float ___CyansSaturation_19;
	// System.Single Colorful.HueSaturationValue::CyansValue
	float ___CyansValue_20;
	// System.Single Colorful.HueSaturationValue::BluesHue
	float ___BluesHue_21;
	// System.Single Colorful.HueSaturationValue::BluesSaturation
	float ___BluesSaturation_22;
	// System.Single Colorful.HueSaturationValue::BluesValue
	float ___BluesValue_23;
	// System.Single Colorful.HueSaturationValue::MagentasHue
	float ___MagentasHue_24;
	// System.Single Colorful.HueSaturationValue::MagentasSaturation
	float ___MagentasSaturation_25;
	// System.Single Colorful.HueSaturationValue::MagentasValue
	float ___MagentasValue_26;
	// System.Boolean Colorful.HueSaturationValue::AdvancedMode
	bool ___AdvancedMode_27;

public:
	inline static int32_t get_offset_of_MasterHue_6() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MasterHue_6)); }
	inline float get_MasterHue_6() const { return ___MasterHue_6; }
	inline float* get_address_of_MasterHue_6() { return &___MasterHue_6; }
	inline void set_MasterHue_6(float value)
	{
		___MasterHue_6 = value;
	}

	inline static int32_t get_offset_of_MasterSaturation_7() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MasterSaturation_7)); }
	inline float get_MasterSaturation_7() const { return ___MasterSaturation_7; }
	inline float* get_address_of_MasterSaturation_7() { return &___MasterSaturation_7; }
	inline void set_MasterSaturation_7(float value)
	{
		___MasterSaturation_7 = value;
	}

	inline static int32_t get_offset_of_MasterValue_8() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MasterValue_8)); }
	inline float get_MasterValue_8() const { return ___MasterValue_8; }
	inline float* get_address_of_MasterValue_8() { return &___MasterValue_8; }
	inline void set_MasterValue_8(float value)
	{
		___MasterValue_8 = value;
	}

	inline static int32_t get_offset_of_RedsHue_9() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___RedsHue_9)); }
	inline float get_RedsHue_9() const { return ___RedsHue_9; }
	inline float* get_address_of_RedsHue_9() { return &___RedsHue_9; }
	inline void set_RedsHue_9(float value)
	{
		___RedsHue_9 = value;
	}

	inline static int32_t get_offset_of_RedsSaturation_10() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___RedsSaturation_10)); }
	inline float get_RedsSaturation_10() const { return ___RedsSaturation_10; }
	inline float* get_address_of_RedsSaturation_10() { return &___RedsSaturation_10; }
	inline void set_RedsSaturation_10(float value)
	{
		___RedsSaturation_10 = value;
	}

	inline static int32_t get_offset_of_RedsValue_11() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___RedsValue_11)); }
	inline float get_RedsValue_11() const { return ___RedsValue_11; }
	inline float* get_address_of_RedsValue_11() { return &___RedsValue_11; }
	inline void set_RedsValue_11(float value)
	{
		___RedsValue_11 = value;
	}

	inline static int32_t get_offset_of_YellowsHue_12() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___YellowsHue_12)); }
	inline float get_YellowsHue_12() const { return ___YellowsHue_12; }
	inline float* get_address_of_YellowsHue_12() { return &___YellowsHue_12; }
	inline void set_YellowsHue_12(float value)
	{
		___YellowsHue_12 = value;
	}

	inline static int32_t get_offset_of_YellowsSaturation_13() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___YellowsSaturation_13)); }
	inline float get_YellowsSaturation_13() const { return ___YellowsSaturation_13; }
	inline float* get_address_of_YellowsSaturation_13() { return &___YellowsSaturation_13; }
	inline void set_YellowsSaturation_13(float value)
	{
		___YellowsSaturation_13 = value;
	}

	inline static int32_t get_offset_of_YellowsValue_14() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___YellowsValue_14)); }
	inline float get_YellowsValue_14() const { return ___YellowsValue_14; }
	inline float* get_address_of_YellowsValue_14() { return &___YellowsValue_14; }
	inline void set_YellowsValue_14(float value)
	{
		___YellowsValue_14 = value;
	}

	inline static int32_t get_offset_of_GreensHue_15() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___GreensHue_15)); }
	inline float get_GreensHue_15() const { return ___GreensHue_15; }
	inline float* get_address_of_GreensHue_15() { return &___GreensHue_15; }
	inline void set_GreensHue_15(float value)
	{
		___GreensHue_15 = value;
	}

	inline static int32_t get_offset_of_GreensSaturation_16() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___GreensSaturation_16)); }
	inline float get_GreensSaturation_16() const { return ___GreensSaturation_16; }
	inline float* get_address_of_GreensSaturation_16() { return &___GreensSaturation_16; }
	inline void set_GreensSaturation_16(float value)
	{
		___GreensSaturation_16 = value;
	}

	inline static int32_t get_offset_of_GreensValue_17() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___GreensValue_17)); }
	inline float get_GreensValue_17() const { return ___GreensValue_17; }
	inline float* get_address_of_GreensValue_17() { return &___GreensValue_17; }
	inline void set_GreensValue_17(float value)
	{
		___GreensValue_17 = value;
	}

	inline static int32_t get_offset_of_CyansHue_18() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___CyansHue_18)); }
	inline float get_CyansHue_18() const { return ___CyansHue_18; }
	inline float* get_address_of_CyansHue_18() { return &___CyansHue_18; }
	inline void set_CyansHue_18(float value)
	{
		___CyansHue_18 = value;
	}

	inline static int32_t get_offset_of_CyansSaturation_19() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___CyansSaturation_19)); }
	inline float get_CyansSaturation_19() const { return ___CyansSaturation_19; }
	inline float* get_address_of_CyansSaturation_19() { return &___CyansSaturation_19; }
	inline void set_CyansSaturation_19(float value)
	{
		___CyansSaturation_19 = value;
	}

	inline static int32_t get_offset_of_CyansValue_20() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___CyansValue_20)); }
	inline float get_CyansValue_20() const { return ___CyansValue_20; }
	inline float* get_address_of_CyansValue_20() { return &___CyansValue_20; }
	inline void set_CyansValue_20(float value)
	{
		___CyansValue_20 = value;
	}

	inline static int32_t get_offset_of_BluesHue_21() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___BluesHue_21)); }
	inline float get_BluesHue_21() const { return ___BluesHue_21; }
	inline float* get_address_of_BluesHue_21() { return &___BluesHue_21; }
	inline void set_BluesHue_21(float value)
	{
		___BluesHue_21 = value;
	}

	inline static int32_t get_offset_of_BluesSaturation_22() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___BluesSaturation_22)); }
	inline float get_BluesSaturation_22() const { return ___BluesSaturation_22; }
	inline float* get_address_of_BluesSaturation_22() { return &___BluesSaturation_22; }
	inline void set_BluesSaturation_22(float value)
	{
		___BluesSaturation_22 = value;
	}

	inline static int32_t get_offset_of_BluesValue_23() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___BluesValue_23)); }
	inline float get_BluesValue_23() const { return ___BluesValue_23; }
	inline float* get_address_of_BluesValue_23() { return &___BluesValue_23; }
	inline void set_BluesValue_23(float value)
	{
		___BluesValue_23 = value;
	}

	inline static int32_t get_offset_of_MagentasHue_24() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MagentasHue_24)); }
	inline float get_MagentasHue_24() const { return ___MagentasHue_24; }
	inline float* get_address_of_MagentasHue_24() { return &___MagentasHue_24; }
	inline void set_MagentasHue_24(float value)
	{
		___MagentasHue_24 = value;
	}

	inline static int32_t get_offset_of_MagentasSaturation_25() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MagentasSaturation_25)); }
	inline float get_MagentasSaturation_25() const { return ___MagentasSaturation_25; }
	inline float* get_address_of_MagentasSaturation_25() { return &___MagentasSaturation_25; }
	inline void set_MagentasSaturation_25(float value)
	{
		___MagentasSaturation_25 = value;
	}

	inline static int32_t get_offset_of_MagentasValue_26() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MagentasValue_26)); }
	inline float get_MagentasValue_26() const { return ___MagentasValue_26; }
	inline float* get_address_of_MagentasValue_26() { return &___MagentasValue_26; }
	inline void set_MagentasValue_26(float value)
	{
		___MagentasValue_26 = value;
	}

	inline static int32_t get_offset_of_AdvancedMode_27() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___AdvancedMode_27)); }
	inline bool get_AdvancedMode_27() const { return ___AdvancedMode_27; }
	inline bool* get_address_of_AdvancedMode_27() { return &___AdvancedMode_27; }
	inline void set_AdvancedMode_27(bool value)
	{
		___AdvancedMode_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUESATURATIONVALUE_T2444304774_H
#ifndef KUWAHARA_T240745109_H
#define KUWAHARA_T240745109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Kuwahara
struct  Kuwahara_t240745109  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.Kuwahara::Radius
	int32_t ___Radius_6;

public:
	inline static int32_t get_offset_of_Radius_6() { return static_cast<int32_t>(offsetof(Kuwahara_t240745109, ___Radius_6)); }
	inline int32_t get_Radius_6() const { return ___Radius_6; }
	inline int32_t* get_address_of_Radius_6() { return &___Radius_6; }
	inline void set_Radius_6(int32_t value)
	{
		___Radius_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KUWAHARA_T240745109_H
#ifndef LED_T3330730803_H
#define LED_T3330730803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Led
struct  Led_t3330730803  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Led::Scale
	float ___Scale_6;
	// System.Single Colorful.Led::Brightness
	float ___Brightness_7;
	// System.Single Colorful.Led::Shape
	float ___Shape_8;
	// System.Boolean Colorful.Led::AutomaticRatio
	bool ___AutomaticRatio_9;
	// System.Single Colorful.Led::Ratio
	float ___Ratio_10;
	// Colorful.Led/SizeMode Colorful.Led::Mode
	int32_t ___Mode_11;

public:
	inline static int32_t get_offset_of_Scale_6() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Scale_6)); }
	inline float get_Scale_6() const { return ___Scale_6; }
	inline float* get_address_of_Scale_6() { return &___Scale_6; }
	inline void set_Scale_6(float value)
	{
		___Scale_6 = value;
	}

	inline static int32_t get_offset_of_Brightness_7() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Brightness_7)); }
	inline float get_Brightness_7() const { return ___Brightness_7; }
	inline float* get_address_of_Brightness_7() { return &___Brightness_7; }
	inline void set_Brightness_7(float value)
	{
		___Brightness_7 = value;
	}

	inline static int32_t get_offset_of_Shape_8() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Shape_8)); }
	inline float get_Shape_8() const { return ___Shape_8; }
	inline float* get_address_of_Shape_8() { return &___Shape_8; }
	inline void set_Shape_8(float value)
	{
		___Shape_8 = value;
	}

	inline static int32_t get_offset_of_AutomaticRatio_9() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___AutomaticRatio_9)); }
	inline bool get_AutomaticRatio_9() const { return ___AutomaticRatio_9; }
	inline bool* get_address_of_AutomaticRatio_9() { return &___AutomaticRatio_9; }
	inline void set_AutomaticRatio_9(bool value)
	{
		___AutomaticRatio_9 = value;
	}

	inline static int32_t get_offset_of_Ratio_10() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Ratio_10)); }
	inline float get_Ratio_10() const { return ___Ratio_10; }
	inline float* get_address_of_Ratio_10() { return &___Ratio_10; }
	inline void set_Ratio_10(float value)
	{
		___Ratio_10 = value;
	}

	inline static int32_t get_offset_of_Mode_11() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Mode_11)); }
	inline int32_t get_Mode_11() const { return ___Mode_11; }
	inline int32_t* get_address_of_Mode_11() { return &___Mode_11; }
	inline void set_Mode_11(int32_t value)
	{
		___Mode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LED_T3330730803_H
#ifndef LENSDISTORTIONBLUR_T2959671745_H
#define LENSDISTORTIONBLUR_T2959671745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LensDistortionBlur
struct  LensDistortionBlur_t2959671745  : public BaseEffect_t1187847871
{
public:
	// Colorful.LensDistortionBlur/QualityPreset Colorful.LensDistortionBlur::Quality
	int32_t ___Quality_6;
	// System.Int32 Colorful.LensDistortionBlur::Samples
	int32_t ___Samples_7;
	// System.Single Colorful.LensDistortionBlur::Distortion
	float ___Distortion_8;
	// System.Single Colorful.LensDistortionBlur::CubicDistortion
	float ___CubicDistortion_9;
	// System.Single Colorful.LensDistortionBlur::Scale
	float ___Scale_10;

public:
	inline static int32_t get_offset_of_Quality_6() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Quality_6)); }
	inline int32_t get_Quality_6() const { return ___Quality_6; }
	inline int32_t* get_address_of_Quality_6() { return &___Quality_6; }
	inline void set_Quality_6(int32_t value)
	{
		___Quality_6 = value;
	}

	inline static int32_t get_offset_of_Samples_7() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Samples_7)); }
	inline int32_t get_Samples_7() const { return ___Samples_7; }
	inline int32_t* get_address_of_Samples_7() { return &___Samples_7; }
	inline void set_Samples_7(int32_t value)
	{
		___Samples_7 = value;
	}

	inline static int32_t get_offset_of_Distortion_8() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Distortion_8)); }
	inline float get_Distortion_8() const { return ___Distortion_8; }
	inline float* get_address_of_Distortion_8() { return &___Distortion_8; }
	inline void set_Distortion_8(float value)
	{
		___Distortion_8 = value;
	}

	inline static int32_t get_offset_of_CubicDistortion_9() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___CubicDistortion_9)); }
	inline float get_CubicDistortion_9() const { return ___CubicDistortion_9; }
	inline float* get_address_of_CubicDistortion_9() { return &___CubicDistortion_9; }
	inline void set_CubicDistortion_9(float value)
	{
		___CubicDistortion_9 = value;
	}

	inline static int32_t get_offset_of_Scale_10() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Scale_10)); }
	inline float get_Scale_10() const { return ___Scale_10; }
	inline float* get_address_of_Scale_10() { return &___Scale_10; }
	inline void set_Scale_10(float value)
	{
		___Scale_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSDISTORTIONBLUR_T2959671745_H
#ifndef LETTERBOX_T3911859827_H
#define LETTERBOX_T3911859827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Letterbox
struct  Letterbox_t3911859827  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Letterbox::Aspect
	float ___Aspect_6;
	// UnityEngine.Color Colorful.Letterbox::FillColor
	Color_t2555686324  ___FillColor_7;

public:
	inline static int32_t get_offset_of_Aspect_6() { return static_cast<int32_t>(offsetof(Letterbox_t3911859827, ___Aspect_6)); }
	inline float get_Aspect_6() const { return ___Aspect_6; }
	inline float* get_address_of_Aspect_6() { return &___Aspect_6; }
	inline void set_Aspect_6(float value)
	{
		___Aspect_6 = value;
	}

	inline static int32_t get_offset_of_FillColor_7() { return static_cast<int32_t>(offsetof(Letterbox_t3911859827, ___FillColor_7)); }
	inline Color_t2555686324  get_FillColor_7() const { return ___FillColor_7; }
	inline Color_t2555686324 * get_address_of_FillColor_7() { return &___FillColor_7; }
	inline void set_FillColor_7(Color_t2555686324  value)
	{
		___FillColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERBOX_T3911859827_H
#ifndef LEVELS_T1894673157_H
#define LEVELS_T1894673157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Levels
struct  Levels_t1894673157  : public BaseEffect_t1187847871
{
public:
	// Colorful.Levels/ColorMode Colorful.Levels::Mode
	int32_t ___Mode_6;
	// UnityEngine.Vector3 Colorful.Levels::InputL
	Vector3_t3722313464  ___InputL_7;
	// UnityEngine.Vector3 Colorful.Levels::InputR
	Vector3_t3722313464  ___InputR_8;
	// UnityEngine.Vector3 Colorful.Levels::InputG
	Vector3_t3722313464  ___InputG_9;
	// UnityEngine.Vector3 Colorful.Levels::InputB
	Vector3_t3722313464  ___InputB_10;
	// UnityEngine.Vector2 Colorful.Levels::OutputL
	Vector2_t2156229523  ___OutputL_11;
	// UnityEngine.Vector2 Colorful.Levels::OutputR
	Vector2_t2156229523  ___OutputR_12;
	// UnityEngine.Vector2 Colorful.Levels::OutputG
	Vector2_t2156229523  ___OutputG_13;
	// UnityEngine.Vector2 Colorful.Levels::OutputB
	Vector2_t2156229523  ___OutputB_14;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_InputL_7() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputL_7)); }
	inline Vector3_t3722313464  get_InputL_7() const { return ___InputL_7; }
	inline Vector3_t3722313464 * get_address_of_InputL_7() { return &___InputL_7; }
	inline void set_InputL_7(Vector3_t3722313464  value)
	{
		___InputL_7 = value;
	}

	inline static int32_t get_offset_of_InputR_8() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputR_8)); }
	inline Vector3_t3722313464  get_InputR_8() const { return ___InputR_8; }
	inline Vector3_t3722313464 * get_address_of_InputR_8() { return &___InputR_8; }
	inline void set_InputR_8(Vector3_t3722313464  value)
	{
		___InputR_8 = value;
	}

	inline static int32_t get_offset_of_InputG_9() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputG_9)); }
	inline Vector3_t3722313464  get_InputG_9() const { return ___InputG_9; }
	inline Vector3_t3722313464 * get_address_of_InputG_9() { return &___InputG_9; }
	inline void set_InputG_9(Vector3_t3722313464  value)
	{
		___InputG_9 = value;
	}

	inline static int32_t get_offset_of_InputB_10() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputB_10)); }
	inline Vector3_t3722313464  get_InputB_10() const { return ___InputB_10; }
	inline Vector3_t3722313464 * get_address_of_InputB_10() { return &___InputB_10; }
	inline void set_InputB_10(Vector3_t3722313464  value)
	{
		___InputB_10 = value;
	}

	inline static int32_t get_offset_of_OutputL_11() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputL_11)); }
	inline Vector2_t2156229523  get_OutputL_11() const { return ___OutputL_11; }
	inline Vector2_t2156229523 * get_address_of_OutputL_11() { return &___OutputL_11; }
	inline void set_OutputL_11(Vector2_t2156229523  value)
	{
		___OutputL_11 = value;
	}

	inline static int32_t get_offset_of_OutputR_12() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputR_12)); }
	inline Vector2_t2156229523  get_OutputR_12() const { return ___OutputR_12; }
	inline Vector2_t2156229523 * get_address_of_OutputR_12() { return &___OutputR_12; }
	inline void set_OutputR_12(Vector2_t2156229523  value)
	{
		___OutputR_12 = value;
	}

	inline static int32_t get_offset_of_OutputG_13() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputG_13)); }
	inline Vector2_t2156229523  get_OutputG_13() const { return ___OutputG_13; }
	inline Vector2_t2156229523 * get_address_of_OutputG_13() { return &___OutputG_13; }
	inline void set_OutputG_13(Vector2_t2156229523  value)
	{
		___OutputG_13 = value;
	}

	inline static int32_t get_offset_of_OutputB_14() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputB_14)); }
	inline Vector2_t2156229523  get_OutputB_14() const { return ___OutputB_14; }
	inline Vector2_t2156229523 * get_address_of_OutputB_14() { return &___OutputB_14; }
	inline void set_OutputB_14(Vector2_t2156229523  value)
	{
		___OutputB_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELS_T1894673157_H
#ifndef LOFIPALETTE_T3780803221_H
#define LOFIPALETTE_T3780803221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LoFiPalette
struct  LoFiPalette_t3780803221  : public LookupFilter3D_t3361124730
{
public:
	// Colorful.LoFiPalette/Preset Colorful.LoFiPalette::Palette
	int32_t ___Palette_14;
	// System.Boolean Colorful.LoFiPalette::Pixelize
	bool ___Pixelize_15;
	// System.Single Colorful.LoFiPalette::PixelSize
	float ___PixelSize_16;
	// Colorful.LoFiPalette/Preset Colorful.LoFiPalette::m_CurrentPreset
	int32_t ___m_CurrentPreset_17;

public:
	inline static int32_t get_offset_of_Palette_14() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___Palette_14)); }
	inline int32_t get_Palette_14() const { return ___Palette_14; }
	inline int32_t* get_address_of_Palette_14() { return &___Palette_14; }
	inline void set_Palette_14(int32_t value)
	{
		___Palette_14 = value;
	}

	inline static int32_t get_offset_of_Pixelize_15() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___Pixelize_15)); }
	inline bool get_Pixelize_15() const { return ___Pixelize_15; }
	inline bool* get_address_of_Pixelize_15() { return &___Pixelize_15; }
	inline void set_Pixelize_15(bool value)
	{
		___Pixelize_15 = value;
	}

	inline static int32_t get_offset_of_PixelSize_16() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___PixelSize_16)); }
	inline float get_PixelSize_16() const { return ___PixelSize_16; }
	inline float* get_address_of_PixelSize_16() { return &___PixelSize_16; }
	inline void set_PixelSize_16(float value)
	{
		___PixelSize_16 = value;
	}

	inline static int32_t get_offset_of_m_CurrentPreset_17() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___m_CurrentPreset_17)); }
	inline int32_t get_m_CurrentPreset_17() const { return ___m_CurrentPreset_17; }
	inline int32_t* get_address_of_m_CurrentPreset_17() { return &___m_CurrentPreset_17; }
	inline void set_m_CurrentPreset_17(int32_t value)
	{
		___m_CurrentPreset_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOFIPALETTE_T3780803221_H
#ifndef LOOKUPFILTER_T4073759679_H
#define LOOKUPFILTER_T4073759679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LookupFilter
struct  LookupFilter_t4073759679  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Texture Colorful.LookupFilter::LookupTexture
	Texture_t3661962703 * ___LookupTexture_6;
	// System.Single Colorful.LookupFilter::Amount
	float ___Amount_7;

public:
	inline static int32_t get_offset_of_LookupTexture_6() { return static_cast<int32_t>(offsetof(LookupFilter_t4073759679, ___LookupTexture_6)); }
	inline Texture_t3661962703 * get_LookupTexture_6() const { return ___LookupTexture_6; }
	inline Texture_t3661962703 ** get_address_of_LookupTexture_6() { return &___LookupTexture_6; }
	inline void set_LookupTexture_6(Texture_t3661962703 * value)
	{
		___LookupTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___LookupTexture_6), value);
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(LookupFilter_t4073759679, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKUPFILTER_T4073759679_H
#ifndef NEGATIVE_T2741149473_H
#define NEGATIVE_T2741149473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Negative
struct  Negative_t2741149473  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Negative::Amount
	float ___Amount_6;

public:
	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(Negative_t2741149473, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGATIVE_T2741149473_H
#ifndef NOISE_T3271901434_H
#define NOISE_T3271901434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Noise
struct  Noise_t3271901434  : public BaseEffect_t1187847871
{
public:
	// Colorful.Noise/ColorMode Colorful.Noise::Mode
	int32_t ___Mode_6;
	// System.Boolean Colorful.Noise::Animate
	bool ___Animate_7;
	// System.Single Colorful.Noise::Seed
	float ___Seed_8;
	// System.Single Colorful.Noise::Strength
	float ___Strength_9;
	// System.Single Colorful.Noise::LumContribution
	float ___LumContribution_10;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_Animate_7() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Animate_7)); }
	inline bool get_Animate_7() const { return ___Animate_7; }
	inline bool* get_address_of_Animate_7() { return &___Animate_7; }
	inline void set_Animate_7(bool value)
	{
		___Animate_7 = value;
	}

	inline static int32_t get_offset_of_Seed_8() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Seed_8)); }
	inline float get_Seed_8() const { return ___Seed_8; }
	inline float* get_address_of_Seed_8() { return &___Seed_8; }
	inline void set_Seed_8(float value)
	{
		___Seed_8 = value;
	}

	inline static int32_t get_offset_of_Strength_9() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Strength_9)); }
	inline float get_Strength_9() const { return ___Strength_9; }
	inline float* get_address_of_Strength_9() { return &___Strength_9; }
	inline void set_Strength_9(float value)
	{
		___Strength_9 = value;
	}

	inline static int32_t get_offset_of_LumContribution_10() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___LumContribution_10)); }
	inline float get_LumContribution_10() const { return ___LumContribution_10; }
	inline float* get_address_of_LumContribution_10() { return &___LumContribution_10; }
	inline void set_LumContribution_10(float value)
	{
		___LumContribution_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISE_T3271901434_H
#ifndef PHOTOFILTER_T50036371_H
#define PHOTOFILTER_T50036371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.PhotoFilter
struct  PhotoFilter_t50036371  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Color Colorful.PhotoFilter::Color
	Color_t2555686324  ___Color_6;
	// System.Single Colorful.PhotoFilter::Density
	float ___Density_7;

public:
	inline static int32_t get_offset_of_Color_6() { return static_cast<int32_t>(offsetof(PhotoFilter_t50036371, ___Color_6)); }
	inline Color_t2555686324  get_Color_6() const { return ___Color_6; }
	inline Color_t2555686324 * get_address_of_Color_6() { return &___Color_6; }
	inline void set_Color_6(Color_t2555686324  value)
	{
		___Color_6 = value;
	}

	inline static int32_t get_offset_of_Density_7() { return static_cast<int32_t>(offsetof(PhotoFilter_t50036371, ___Density_7)); }
	inline float get_Density_7() const { return ___Density_7; }
	inline float* get_address_of_Density_7() { return &___Density_7; }
	inline void set_Density_7(float value)
	{
		___Density_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOFILTER_T50036371_H
#ifndef PIXELMATRIX_T927513071_H
#define PIXELMATRIX_T927513071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.PixelMatrix
struct  PixelMatrix_t927513071  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.PixelMatrix::Size
	int32_t ___Size_6;
	// System.Single Colorful.PixelMatrix::Brightness
	float ___Brightness_7;
	// System.Boolean Colorful.PixelMatrix::BlackBorder
	bool ___BlackBorder_8;

public:
	inline static int32_t get_offset_of_Size_6() { return static_cast<int32_t>(offsetof(PixelMatrix_t927513071, ___Size_6)); }
	inline int32_t get_Size_6() const { return ___Size_6; }
	inline int32_t* get_address_of_Size_6() { return &___Size_6; }
	inline void set_Size_6(int32_t value)
	{
		___Size_6 = value;
	}

	inline static int32_t get_offset_of_Brightness_7() { return static_cast<int32_t>(offsetof(PixelMatrix_t927513071, ___Brightness_7)); }
	inline float get_Brightness_7() const { return ___Brightness_7; }
	inline float* get_address_of_Brightness_7() { return &___Brightness_7; }
	inline void set_Brightness_7(float value)
	{
		___Brightness_7 = value;
	}

	inline static int32_t get_offset_of_BlackBorder_8() { return static_cast<int32_t>(offsetof(PixelMatrix_t927513071, ___BlackBorder_8)); }
	inline bool get_BlackBorder_8() const { return ___BlackBorder_8; }
	inline bool* get_address_of_BlackBorder_8() { return &___BlackBorder_8; }
	inline void set_BlackBorder_8(bool value)
	{
		___BlackBorder_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELMATRIX_T927513071_H
#ifndef PIXELATE_T1523178201_H
#define PIXELATE_T1523178201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Pixelate
struct  Pixelate_t1523178201  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Pixelate::Scale
	float ___Scale_6;
	// System.Boolean Colorful.Pixelate::AutomaticRatio
	bool ___AutomaticRatio_7;
	// System.Single Colorful.Pixelate::Ratio
	float ___Ratio_8;
	// Colorful.Pixelate/SizeMode Colorful.Pixelate::Mode
	int32_t ___Mode_9;

public:
	inline static int32_t get_offset_of_Scale_6() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___Scale_6)); }
	inline float get_Scale_6() const { return ___Scale_6; }
	inline float* get_address_of_Scale_6() { return &___Scale_6; }
	inline void set_Scale_6(float value)
	{
		___Scale_6 = value;
	}

	inline static int32_t get_offset_of_AutomaticRatio_7() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___AutomaticRatio_7)); }
	inline bool get_AutomaticRatio_7() const { return ___AutomaticRatio_7; }
	inline bool* get_address_of_AutomaticRatio_7() { return &___AutomaticRatio_7; }
	inline void set_AutomaticRatio_7(bool value)
	{
		___AutomaticRatio_7 = value;
	}

	inline static int32_t get_offset_of_Ratio_8() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___Ratio_8)); }
	inline float get_Ratio_8() const { return ___Ratio_8; }
	inline float* get_address_of_Ratio_8() { return &___Ratio_8; }
	inline void set_Ratio_8(float value)
	{
		___Ratio_8 = value;
	}

	inline static int32_t get_offset_of_Mode_9() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___Mode_9)); }
	inline int32_t get_Mode_9() const { return ___Mode_9; }
	inline int32_t* get_address_of_Mode_9() { return &___Mode_9; }
	inline void set_Mode_9(int32_t value)
	{
		___Mode_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELATE_T1523178201_H
#ifndef POSTERIZE_T3175588324_H
#define POSTERIZE_T3175588324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Posterize
struct  Posterize_t3175588324  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.Posterize::Levels
	int32_t ___Levels_6;
	// System.Single Colorful.Posterize::Amount
	float ___Amount_7;
	// System.Boolean Colorful.Posterize::LuminosityOnly
	bool ___LuminosityOnly_8;

public:
	inline static int32_t get_offset_of_Levels_6() { return static_cast<int32_t>(offsetof(Posterize_t3175588324, ___Levels_6)); }
	inline int32_t get_Levels_6() const { return ___Levels_6; }
	inline int32_t* get_address_of_Levels_6() { return &___Levels_6; }
	inline void set_Levels_6(int32_t value)
	{
		___Levels_6 = value;
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(Posterize_t3175588324, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}

	inline static int32_t get_offset_of_LuminosityOnly_8() { return static_cast<int32_t>(offsetof(Posterize_t3175588324, ___LuminosityOnly_8)); }
	inline bool get_LuminosityOnly_8() const { return ___LuminosityOnly_8; }
	inline bool* get_address_of_LuminosityOnly_8() { return &___LuminosityOnly_8; }
	inline void set_LuminosityOnly_8(bool value)
	{
		___LuminosityOnly_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTERIZE_T3175588324_H
#ifndef RGBSPLIT_T656033929_H
#define RGBSPLIT_T656033929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.RGBSplit
struct  RGBSplit_t656033929  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.RGBSplit::Amount
	float ___Amount_6;
	// System.Single Colorful.RGBSplit::Angle
	float ___Angle_7;

public:
	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(RGBSplit_t656033929, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}

	inline static int32_t get_offset_of_Angle_7() { return static_cast<int32_t>(offsetof(RGBSplit_t656033929, ___Angle_7)); }
	inline float get_Angle_7() const { return ___Angle_7; }
	inline float* get_address_of_Angle_7() { return &___Angle_7; }
	inline void set_Angle_7(float value)
	{
		___Angle_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RGBSPLIT_T656033929_H
#ifndef RADIALBLUR_T147563976_H
#define RADIALBLUR_T147563976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.RadialBlur
struct  RadialBlur_t147563976  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.RadialBlur::Strength
	float ___Strength_6;
	// System.Int32 Colorful.RadialBlur::Samples
	int32_t ___Samples_7;
	// UnityEngine.Vector2 Colorful.RadialBlur::Center
	Vector2_t2156229523  ___Center_8;
	// Colorful.RadialBlur/QualityPreset Colorful.RadialBlur::Quality
	int32_t ___Quality_9;
	// System.Single Colorful.RadialBlur::Sharpness
	float ___Sharpness_10;
	// System.Single Colorful.RadialBlur::Darkness
	float ___Darkness_11;
	// System.Boolean Colorful.RadialBlur::EnableVignette
	bool ___EnableVignette_12;

public:
	inline static int32_t get_offset_of_Strength_6() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Strength_6)); }
	inline float get_Strength_6() const { return ___Strength_6; }
	inline float* get_address_of_Strength_6() { return &___Strength_6; }
	inline void set_Strength_6(float value)
	{
		___Strength_6 = value;
	}

	inline static int32_t get_offset_of_Samples_7() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Samples_7)); }
	inline int32_t get_Samples_7() const { return ___Samples_7; }
	inline int32_t* get_address_of_Samples_7() { return &___Samples_7; }
	inline void set_Samples_7(int32_t value)
	{
		___Samples_7 = value;
	}

	inline static int32_t get_offset_of_Center_8() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Center_8)); }
	inline Vector2_t2156229523  get_Center_8() const { return ___Center_8; }
	inline Vector2_t2156229523 * get_address_of_Center_8() { return &___Center_8; }
	inline void set_Center_8(Vector2_t2156229523  value)
	{
		___Center_8 = value;
	}

	inline static int32_t get_offset_of_Quality_9() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Quality_9)); }
	inline int32_t get_Quality_9() const { return ___Quality_9; }
	inline int32_t* get_address_of_Quality_9() { return &___Quality_9; }
	inline void set_Quality_9(int32_t value)
	{
		___Quality_9 = value;
	}

	inline static int32_t get_offset_of_Sharpness_10() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Sharpness_10)); }
	inline float get_Sharpness_10() const { return ___Sharpness_10; }
	inline float* get_address_of_Sharpness_10() { return &___Sharpness_10; }
	inline void set_Sharpness_10(float value)
	{
		___Sharpness_10 = value;
	}

	inline static int32_t get_offset_of_Darkness_11() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Darkness_11)); }
	inline float get_Darkness_11() const { return ___Darkness_11; }
	inline float* get_address_of_Darkness_11() { return &___Darkness_11; }
	inline void set_Darkness_11(float value)
	{
		___Darkness_11 = value;
	}

	inline static int32_t get_offset_of_EnableVignette_12() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___EnableVignette_12)); }
	inline bool get_EnableVignette_12() const { return ___EnableVignette_12; }
	inline bool* get_address_of_EnableVignette_12() { return &___EnableVignette_12; }
	inline void set_EnableVignette_12(bool value)
	{
		___EnableVignette_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALBLUR_T147563976_H
#ifndef SCURVECONTRAST_T1916604434_H
#define SCURVECONTRAST_T1916604434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.SCurveContrast
struct  SCurveContrast_t1916604434  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.SCurveContrast::RedSteepness
	float ___RedSteepness_6;
	// System.Single Colorful.SCurveContrast::RedGamma
	float ___RedGamma_7;
	// System.Single Colorful.SCurveContrast::GreenSteepness
	float ___GreenSteepness_8;
	// System.Single Colorful.SCurveContrast::GreenGamma
	float ___GreenGamma_9;
	// System.Single Colorful.SCurveContrast::BlueSteepness
	float ___BlueSteepness_10;
	// System.Single Colorful.SCurveContrast::BlueGamma
	float ___BlueGamma_11;

public:
	inline static int32_t get_offset_of_RedSteepness_6() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___RedSteepness_6)); }
	inline float get_RedSteepness_6() const { return ___RedSteepness_6; }
	inline float* get_address_of_RedSteepness_6() { return &___RedSteepness_6; }
	inline void set_RedSteepness_6(float value)
	{
		___RedSteepness_6 = value;
	}

	inline static int32_t get_offset_of_RedGamma_7() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___RedGamma_7)); }
	inline float get_RedGamma_7() const { return ___RedGamma_7; }
	inline float* get_address_of_RedGamma_7() { return &___RedGamma_7; }
	inline void set_RedGamma_7(float value)
	{
		___RedGamma_7 = value;
	}

	inline static int32_t get_offset_of_GreenSteepness_8() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___GreenSteepness_8)); }
	inline float get_GreenSteepness_8() const { return ___GreenSteepness_8; }
	inline float* get_address_of_GreenSteepness_8() { return &___GreenSteepness_8; }
	inline void set_GreenSteepness_8(float value)
	{
		___GreenSteepness_8 = value;
	}

	inline static int32_t get_offset_of_GreenGamma_9() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___GreenGamma_9)); }
	inline float get_GreenGamma_9() const { return ___GreenGamma_9; }
	inline float* get_address_of_GreenGamma_9() { return &___GreenGamma_9; }
	inline void set_GreenGamma_9(float value)
	{
		___GreenGamma_9 = value;
	}

	inline static int32_t get_offset_of_BlueSteepness_10() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___BlueSteepness_10)); }
	inline float get_BlueSteepness_10() const { return ___BlueSteepness_10; }
	inline float* get_address_of_BlueSteepness_10() { return &___BlueSteepness_10; }
	inline void set_BlueSteepness_10(float value)
	{
		___BlueSteepness_10 = value;
	}

	inline static int32_t get_offset_of_BlueGamma_11() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___BlueGamma_11)); }
	inline float get_BlueGamma_11() const { return ___BlueGamma_11; }
	inline float* get_address_of_BlueGamma_11() { return &___BlueGamma_11; }
	inline void set_BlueGamma_11(float value)
	{
		___BlueGamma_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCURVECONTRAST_T1916604434_H
#ifndef SHADOWSMIDTONESHIGHLIGHTS_T2681697010_H
#define SHADOWSMIDTONESHIGHLIGHTS_T2681697010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ShadowsMidtonesHighlights
struct  ShadowsMidtonesHighlights_t2681697010  : public BaseEffect_t1187847871
{
public:
	// Colorful.ShadowsMidtonesHighlights/ColorMode Colorful.ShadowsMidtonesHighlights::Mode
	int32_t ___Mode_6;
	// UnityEngine.Color Colorful.ShadowsMidtonesHighlights::Shadows
	Color_t2555686324  ___Shadows_7;
	// UnityEngine.Color Colorful.ShadowsMidtonesHighlights::Midtones
	Color_t2555686324  ___Midtones_8;
	// UnityEngine.Color Colorful.ShadowsMidtonesHighlights::Highlights
	Color_t2555686324  ___Highlights_9;
	// System.Single Colorful.ShadowsMidtonesHighlights::Amount
	float ___Amount_10;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_Shadows_7() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Shadows_7)); }
	inline Color_t2555686324  get_Shadows_7() const { return ___Shadows_7; }
	inline Color_t2555686324 * get_address_of_Shadows_7() { return &___Shadows_7; }
	inline void set_Shadows_7(Color_t2555686324  value)
	{
		___Shadows_7 = value;
	}

	inline static int32_t get_offset_of_Midtones_8() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Midtones_8)); }
	inline Color_t2555686324  get_Midtones_8() const { return ___Midtones_8; }
	inline Color_t2555686324 * get_address_of_Midtones_8() { return &___Midtones_8; }
	inline void set_Midtones_8(Color_t2555686324  value)
	{
		___Midtones_8 = value;
	}

	inline static int32_t get_offset_of_Highlights_9() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Highlights_9)); }
	inline Color_t2555686324  get_Highlights_9() const { return ___Highlights_9; }
	inline Color_t2555686324 * get_address_of_Highlights_9() { return &___Highlights_9; }
	inline void set_Highlights_9(Color_t2555686324  value)
	{
		___Highlights_9 = value;
	}

	inline static int32_t get_offset_of_Amount_10() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Amount_10)); }
	inline float get_Amount_10() const { return ___Amount_10; }
	inline float* get_address_of_Amount_10() { return &___Amount_10; }
	inline void set_Amount_10(float value)
	{
		___Amount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSMIDTONESHIGHLIGHTS_T2681697010_H
#ifndef SHARPEN_T1993388953_H
#define SHARPEN_T1993388953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Sharpen
struct  Sharpen_t1993388953  : public BaseEffect_t1187847871
{
public:
	// Colorful.Sharpen/Algorithm Colorful.Sharpen::Mode
	int32_t ___Mode_6;
	// System.Single Colorful.Sharpen::Strength
	float ___Strength_7;
	// System.Single Colorful.Sharpen::Clamp
	float ___Clamp_8;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(Sharpen_t1993388953, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_Strength_7() { return static_cast<int32_t>(offsetof(Sharpen_t1993388953, ___Strength_7)); }
	inline float get_Strength_7() const { return ___Strength_7; }
	inline float* get_address_of_Strength_7() { return &___Strength_7; }
	inline void set_Strength_7(float value)
	{
		___Strength_7 = value;
	}

	inline static int32_t get_offset_of_Clamp_8() { return static_cast<int32_t>(offsetof(Sharpen_t1993388953, ___Clamp_8)); }
	inline float get_Clamp_8() const { return ___Clamp_8; }
	inline float* get_address_of_Clamp_8() { return &___Clamp_8; }
	inline void set_Clamp_8(float value)
	{
		___Clamp_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARPEN_T1993388953_H
#ifndef SMARTSATURATION_T3817639239_H
#define SMARTSATURATION_T3817639239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.SmartSaturation
struct  SmartSaturation_t3817639239  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.SmartSaturation::Boost
	float ___Boost_6;
	// UnityEngine.AnimationCurve Colorful.SmartSaturation::Curve
	AnimationCurve_t3046754366 * ___Curve_7;
	// UnityEngine.Texture2D Colorful.SmartSaturation::_CurveTexture
	Texture2D_t3840446185 * ____CurveTexture_8;

public:
	inline static int32_t get_offset_of_Boost_6() { return static_cast<int32_t>(offsetof(SmartSaturation_t3817639239, ___Boost_6)); }
	inline float get_Boost_6() const { return ___Boost_6; }
	inline float* get_address_of_Boost_6() { return &___Boost_6; }
	inline void set_Boost_6(float value)
	{
		___Boost_6 = value;
	}

	inline static int32_t get_offset_of_Curve_7() { return static_cast<int32_t>(offsetof(SmartSaturation_t3817639239, ___Curve_7)); }
	inline AnimationCurve_t3046754366 * get_Curve_7() const { return ___Curve_7; }
	inline AnimationCurve_t3046754366 ** get_address_of_Curve_7() { return &___Curve_7; }
	inline void set_Curve_7(AnimationCurve_t3046754366 * value)
	{
		___Curve_7 = value;
		Il2CppCodeGenWriteBarrier((&___Curve_7), value);
	}

	inline static int32_t get_offset_of__CurveTexture_8() { return static_cast<int32_t>(offsetof(SmartSaturation_t3817639239, ____CurveTexture_8)); }
	inline Texture2D_t3840446185 * get__CurveTexture_8() const { return ____CurveTexture_8; }
	inline Texture2D_t3840446185 ** get_address_of__CurveTexture_8() { return &____CurveTexture_8; }
	inline void set__CurveTexture_8(Texture2D_t3840446185 * value)
	{
		____CurveTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&____CurveTexture_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTSATURATION_T3817639239_H
#ifndef STROKES_T892401832_H
#define STROKES_T892401832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Strokes
struct  Strokes_t892401832  : public BaseEffect_t1187847871
{
public:
	// Colorful.Strokes/ColorMode Colorful.Strokes::Mode
	int32_t ___Mode_6;
	// System.Single Colorful.Strokes::Amplitude
	float ___Amplitude_7;
	// System.Single Colorful.Strokes::Frequency
	float ___Frequency_8;
	// System.Single Colorful.Strokes::Scaling
	float ___Scaling_9;
	// System.Single Colorful.Strokes::MaxThickness
	float ___MaxThickness_10;
	// System.Single Colorful.Strokes::Threshold
	float ___Threshold_11;
	// System.Single Colorful.Strokes::Harshness
	float ___Harshness_12;
	// System.Single Colorful.Strokes::RedLuminance
	float ___RedLuminance_13;
	// System.Single Colorful.Strokes::GreenLuminance
	float ___GreenLuminance_14;
	// System.Single Colorful.Strokes::BlueLuminance
	float ___BlueLuminance_15;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_Amplitude_7() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Amplitude_7)); }
	inline float get_Amplitude_7() const { return ___Amplitude_7; }
	inline float* get_address_of_Amplitude_7() { return &___Amplitude_7; }
	inline void set_Amplitude_7(float value)
	{
		___Amplitude_7 = value;
	}

	inline static int32_t get_offset_of_Frequency_8() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Frequency_8)); }
	inline float get_Frequency_8() const { return ___Frequency_8; }
	inline float* get_address_of_Frequency_8() { return &___Frequency_8; }
	inline void set_Frequency_8(float value)
	{
		___Frequency_8 = value;
	}

	inline static int32_t get_offset_of_Scaling_9() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Scaling_9)); }
	inline float get_Scaling_9() const { return ___Scaling_9; }
	inline float* get_address_of_Scaling_9() { return &___Scaling_9; }
	inline void set_Scaling_9(float value)
	{
		___Scaling_9 = value;
	}

	inline static int32_t get_offset_of_MaxThickness_10() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___MaxThickness_10)); }
	inline float get_MaxThickness_10() const { return ___MaxThickness_10; }
	inline float* get_address_of_MaxThickness_10() { return &___MaxThickness_10; }
	inline void set_MaxThickness_10(float value)
	{
		___MaxThickness_10 = value;
	}

	inline static int32_t get_offset_of_Threshold_11() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Threshold_11)); }
	inline float get_Threshold_11() const { return ___Threshold_11; }
	inline float* get_address_of_Threshold_11() { return &___Threshold_11; }
	inline void set_Threshold_11(float value)
	{
		___Threshold_11 = value;
	}

	inline static int32_t get_offset_of_Harshness_12() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Harshness_12)); }
	inline float get_Harshness_12() const { return ___Harshness_12; }
	inline float* get_address_of_Harshness_12() { return &___Harshness_12; }
	inline void set_Harshness_12(float value)
	{
		___Harshness_12 = value;
	}

	inline static int32_t get_offset_of_RedLuminance_13() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___RedLuminance_13)); }
	inline float get_RedLuminance_13() const { return ___RedLuminance_13; }
	inline float* get_address_of_RedLuminance_13() { return &___RedLuminance_13; }
	inline void set_RedLuminance_13(float value)
	{
		___RedLuminance_13 = value;
	}

	inline static int32_t get_offset_of_GreenLuminance_14() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___GreenLuminance_14)); }
	inline float get_GreenLuminance_14() const { return ___GreenLuminance_14; }
	inline float* get_address_of_GreenLuminance_14() { return &___GreenLuminance_14; }
	inline void set_GreenLuminance_14(float value)
	{
		___GreenLuminance_14 = value;
	}

	inline static int32_t get_offset_of_BlueLuminance_15() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___BlueLuminance_15)); }
	inline float get_BlueLuminance_15() const { return ___BlueLuminance_15; }
	inline float* get_address_of_BlueLuminance_15() { return &___BlueLuminance_15; }
	inline void set_BlueLuminance_15(float value)
	{
		___BlueLuminance_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STROKES_T892401832_H
#ifndef TVVIGNETTE_T3643404851_H
#define TVVIGNETTE_T3643404851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.TVVignette
struct  TVVignette_t3643404851  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.TVVignette::Size
	float ___Size_6;
	// System.Single Colorful.TVVignette::Offset
	float ___Offset_7;

public:
	inline static int32_t get_offset_of_Size_6() { return static_cast<int32_t>(offsetof(TVVignette_t3643404851, ___Size_6)); }
	inline float get_Size_6() const { return ___Size_6; }
	inline float* get_address_of_Size_6() { return &___Size_6; }
	inline void set_Size_6(float value)
	{
		___Size_6 = value;
	}

	inline static int32_t get_offset_of_Offset_7() { return static_cast<int32_t>(offsetof(TVVignette_t3643404851, ___Offset_7)); }
	inline float get_Offset_7() const { return ___Offset_7; }
	inline float* get_address_of_Offset_7() { return &___Offset_7; }
	inline void set_Offset_7(float value)
	{
		___Offset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TVVIGNETTE_T3643404851_H
#ifndef TECHNICOLOR_T2196134954_H
#define TECHNICOLOR_T2196134954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Technicolor
struct  Technicolor_t2196134954  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Technicolor::Exposure
	float ___Exposure_6;
	// UnityEngine.Vector3 Colorful.Technicolor::Balance
	Vector3_t3722313464  ___Balance_7;
	// System.Single Colorful.Technicolor::Amount
	float ___Amount_8;

public:
	inline static int32_t get_offset_of_Exposure_6() { return static_cast<int32_t>(offsetof(Technicolor_t2196134954, ___Exposure_6)); }
	inline float get_Exposure_6() const { return ___Exposure_6; }
	inline float* get_address_of_Exposure_6() { return &___Exposure_6; }
	inline void set_Exposure_6(float value)
	{
		___Exposure_6 = value;
	}

	inline static int32_t get_offset_of_Balance_7() { return static_cast<int32_t>(offsetof(Technicolor_t2196134954, ___Balance_7)); }
	inline Vector3_t3722313464  get_Balance_7() const { return ___Balance_7; }
	inline Vector3_t3722313464 * get_address_of_Balance_7() { return &___Balance_7; }
	inline void set_Balance_7(Vector3_t3722313464  value)
	{
		___Balance_7 = value;
	}

	inline static int32_t get_offset_of_Amount_8() { return static_cast<int32_t>(offsetof(Technicolor_t2196134954, ___Amount_8)); }
	inline float get_Amount_8() const { return ___Amount_8; }
	inline float* get_address_of_Amount_8() { return &___Amount_8; }
	inline void set_Amount_8(float value)
	{
		___Amount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TECHNICOLOR_T2196134954_H
#ifndef THRESHOLD_T1192544618_H
#define THRESHOLD_T1192544618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Threshold
struct  Threshold_t1192544618  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Threshold::Value
	float ___Value_6;
	// System.Single Colorful.Threshold::NoiseRange
	float ___NoiseRange_7;
	// System.Boolean Colorful.Threshold::UseNoise
	bool ___UseNoise_8;

public:
	inline static int32_t get_offset_of_Value_6() { return static_cast<int32_t>(offsetof(Threshold_t1192544618, ___Value_6)); }
	inline float get_Value_6() const { return ___Value_6; }
	inline float* get_address_of_Value_6() { return &___Value_6; }
	inline void set_Value_6(float value)
	{
		___Value_6 = value;
	}

	inline static int32_t get_offset_of_NoiseRange_7() { return static_cast<int32_t>(offsetof(Threshold_t1192544618, ___NoiseRange_7)); }
	inline float get_NoiseRange_7() const { return ___NoiseRange_7; }
	inline float* get_address_of_NoiseRange_7() { return &___NoiseRange_7; }
	inline void set_NoiseRange_7(float value)
	{
		___NoiseRange_7 = value;
	}

	inline static int32_t get_offset_of_UseNoise_8() { return static_cast<int32_t>(offsetof(Threshold_t1192544618, ___UseNoise_8)); }
	inline bool get_UseNoise_8() const { return ___UseNoise_8; }
	inline bool* get_address_of_UseNoise_8() { return &___UseNoise_8; }
	inline void set_UseNoise_8(bool value)
	{
		___UseNoise_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THRESHOLD_T1192544618_H
#ifndef VIBRANCE_T2988503215_H
#define VIBRANCE_T2988503215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Vibrance
struct  Vibrance_t2988503215  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Vibrance::Amount
	float ___Amount_6;
	// System.Single Colorful.Vibrance::RedChannel
	float ___RedChannel_7;
	// System.Single Colorful.Vibrance::GreenChannel
	float ___GreenChannel_8;
	// System.Single Colorful.Vibrance::BlueChannel
	float ___BlueChannel_9;
	// System.Boolean Colorful.Vibrance::AdvancedMode
	bool ___AdvancedMode_10;

public:
	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}

	inline static int32_t get_offset_of_RedChannel_7() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___RedChannel_7)); }
	inline float get_RedChannel_7() const { return ___RedChannel_7; }
	inline float* get_address_of_RedChannel_7() { return &___RedChannel_7; }
	inline void set_RedChannel_7(float value)
	{
		___RedChannel_7 = value;
	}

	inline static int32_t get_offset_of_GreenChannel_8() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___GreenChannel_8)); }
	inline float get_GreenChannel_8() const { return ___GreenChannel_8; }
	inline float* get_address_of_GreenChannel_8() { return &___GreenChannel_8; }
	inline void set_GreenChannel_8(float value)
	{
		___GreenChannel_8 = value;
	}

	inline static int32_t get_offset_of_BlueChannel_9() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___BlueChannel_9)); }
	inline float get_BlueChannel_9() const { return ___BlueChannel_9; }
	inline float* get_address_of_BlueChannel_9() { return &___BlueChannel_9; }
	inline void set_BlueChannel_9(float value)
	{
		___BlueChannel_9 = value;
	}

	inline static int32_t get_offset_of_AdvancedMode_10() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___AdvancedMode_10)); }
	inline bool get_AdvancedMode_10() const { return ___AdvancedMode_10; }
	inline bool* get_address_of_AdvancedMode_10() { return &___AdvancedMode_10; }
	inline void set_AdvancedMode_10(bool value)
	{
		___AdvancedMode_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIBRANCE_T2988503215_H
#ifndef VINTAGEFAST_T3040349303_H
#define VINTAGEFAST_T3040349303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.VintageFast
struct  VintageFast_t3040349303  : public LookupFilter3D_t3361124730
{
public:
	// Colorful.Vintage/InstragramFilter Colorful.VintageFast::Filter
	int32_t ___Filter_14;
	// Colorful.Vintage/InstragramFilter Colorful.VintageFast::m_CurrentFilter
	int32_t ___m_CurrentFilter_15;

public:
	inline static int32_t get_offset_of_Filter_14() { return static_cast<int32_t>(offsetof(VintageFast_t3040349303, ___Filter_14)); }
	inline int32_t get_Filter_14() const { return ___Filter_14; }
	inline int32_t* get_address_of_Filter_14() { return &___Filter_14; }
	inline void set_Filter_14(int32_t value)
	{
		___Filter_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFilter_15() { return static_cast<int32_t>(offsetof(VintageFast_t3040349303, ___m_CurrentFilter_15)); }
	inline int32_t get_m_CurrentFilter_15() const { return ___m_CurrentFilter_15; }
	inline int32_t* get_address_of_m_CurrentFilter_15() { return &___m_CurrentFilter_15; }
	inline void set_m_CurrentFilter_15(int32_t value)
	{
		___m_CurrentFilter_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VINTAGEFAST_T3040349303_H
#ifndef WAVEDISTORTION_T3073420756_H
#define WAVEDISTORTION_T3073420756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.WaveDistortion
struct  WaveDistortion_t3073420756  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.WaveDistortion::Amplitude
	float ___Amplitude_6;
	// System.Single Colorful.WaveDistortion::Waves
	float ___Waves_7;
	// System.Single Colorful.WaveDistortion::ColorGlitch
	float ___ColorGlitch_8;
	// System.Single Colorful.WaveDistortion::Phase
	float ___Phase_9;

public:
	inline static int32_t get_offset_of_Amplitude_6() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___Amplitude_6)); }
	inline float get_Amplitude_6() const { return ___Amplitude_6; }
	inline float* get_address_of_Amplitude_6() { return &___Amplitude_6; }
	inline void set_Amplitude_6(float value)
	{
		___Amplitude_6 = value;
	}

	inline static int32_t get_offset_of_Waves_7() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___Waves_7)); }
	inline float get_Waves_7() const { return ___Waves_7; }
	inline float* get_address_of_Waves_7() { return &___Waves_7; }
	inline void set_Waves_7(float value)
	{
		___Waves_7 = value;
	}

	inline static int32_t get_offset_of_ColorGlitch_8() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___ColorGlitch_8)); }
	inline float get_ColorGlitch_8() const { return ___ColorGlitch_8; }
	inline float* get_address_of_ColorGlitch_8() { return &___ColorGlitch_8; }
	inline void set_ColorGlitch_8(float value)
	{
		___ColorGlitch_8 = value;
	}

	inline static int32_t get_offset_of_Phase_9() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___Phase_9)); }
	inline float get_Phase_9() const { return ___Phase_9; }
	inline float* get_address_of_Phase_9() { return &___Phase_9; }
	inline void set_Phase_9(float value)
	{
		___Phase_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEDISTORTION_T3073420756_H
#ifndef WHITEBALANCE_T1550391260_H
#define WHITEBALANCE_T1550391260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.WhiteBalance
struct  WhiteBalance_t1550391260  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Color Colorful.WhiteBalance::White
	Color_t2555686324  ___White_6;
	// Colorful.WhiteBalance/BalanceMode Colorful.WhiteBalance::Mode
	int32_t ___Mode_7;

public:
	inline static int32_t get_offset_of_White_6() { return static_cast<int32_t>(offsetof(WhiteBalance_t1550391260, ___White_6)); }
	inline Color_t2555686324  get_White_6() const { return ___White_6; }
	inline Color_t2555686324 * get_address_of_White_6() { return &___White_6; }
	inline void set_White_6(Color_t2555686324  value)
	{
		___White_6 = value;
	}

	inline static int32_t get_offset_of_Mode_7() { return static_cast<int32_t>(offsetof(WhiteBalance_t1550391260, ___Mode_7)); }
	inline int32_t get_Mode_7() const { return ___Mode_7; }
	inline int32_t* get_address_of_Mode_7() { return &___Mode_7; }
	inline void set_Mode_7(int32_t value)
	{
		___Mode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITEBALANCE_T1550391260_H
#ifndef WIGGLE_T4291210129_H
#define WIGGLE_T4291210129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Wiggle
struct  Wiggle_t4291210129  : public BaseEffect_t1187847871
{
public:
	// Colorful.Wiggle/Algorithm Colorful.Wiggle::Mode
	int32_t ___Mode_6;
	// System.Single Colorful.Wiggle::Timer
	float ___Timer_7;
	// System.Single Colorful.Wiggle::Speed
	float ___Speed_8;
	// System.Single Colorful.Wiggle::Frequency
	float ___Frequency_9;
	// System.Single Colorful.Wiggle::Amplitude
	float ___Amplitude_10;
	// System.Boolean Colorful.Wiggle::AutomaticTimer
	bool ___AutomaticTimer_11;

public:
	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}

	inline static int32_t get_offset_of_Timer_7() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Timer_7)); }
	inline float get_Timer_7() const { return ___Timer_7; }
	inline float* get_address_of_Timer_7() { return &___Timer_7; }
	inline void set_Timer_7(float value)
	{
		___Timer_7 = value;
	}

	inline static int32_t get_offset_of_Speed_8() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Speed_8)); }
	inline float get_Speed_8() const { return ___Speed_8; }
	inline float* get_address_of_Speed_8() { return &___Speed_8; }
	inline void set_Speed_8(float value)
	{
		___Speed_8 = value;
	}

	inline static int32_t get_offset_of_Frequency_9() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Frequency_9)); }
	inline float get_Frequency_9() const { return ___Frequency_9; }
	inline float* get_address_of_Frequency_9() { return &___Frequency_9; }
	inline void set_Frequency_9(float value)
	{
		___Frequency_9 = value;
	}

	inline static int32_t get_offset_of_Amplitude_10() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Amplitude_10)); }
	inline float get_Amplitude_10() const { return ___Amplitude_10; }
	inline float* get_address_of_Amplitude_10() { return &___Amplitude_10; }
	inline void set_Amplitude_10(float value)
	{
		___Amplitude_10 = value;
	}

	inline static int32_t get_offset_of_AutomaticTimer_11() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___AutomaticTimer_11)); }
	inline bool get_AutomaticTimer_11() const { return ___AutomaticTimer_11; }
	inline bool* get_address_of_AutomaticTimer_11() { return &___AutomaticTimer_11; }
	inline void set_AutomaticTimer_11(bool value)
	{
		___AutomaticTimer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIGGLE_T4291210129_H
#ifndef VINTAGE_T2551956078_H
#define VINTAGE_T2551956078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Vintage
struct  Vintage_t2551956078  : public LookupFilter_t4073759679
{
public:
	// Colorful.Vintage/InstragramFilter Colorful.Vintage::Filter
	int32_t ___Filter_8;
	// Colorful.Vintage/InstragramFilter Colorful.Vintage::m_CurrentFilter
	int32_t ___m_CurrentFilter_9;

public:
	inline static int32_t get_offset_of_Filter_8() { return static_cast<int32_t>(offsetof(Vintage_t2551956078, ___Filter_8)); }
	inline int32_t get_Filter_8() const { return ___Filter_8; }
	inline int32_t* get_address_of_Filter_8() { return &___Filter_8; }
	inline void set_Filter_8(int32_t value)
	{
		___Filter_8 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFilter_9() { return static_cast<int32_t>(offsetof(Vintage_t2551956078, ___m_CurrentFilter_9)); }
	inline int32_t get_m_CurrentFilter_9() const { return ___m_CurrentFilter_9; }
	inline int32_t* get_address_of_m_CurrentFilter_9() { return &___m_CurrentFilter_9; }
	inline void set_m_CurrentFilter_9(int32_t value)
	{
		___m_CurrentFilter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VINTAGE_T2551956078_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (U3CCustomUpdateU3Ec__Iterator0_t97322721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2900[4] = 
{
	U3CCustomUpdateU3Ec__Iterator0_t97322721::get_offset_of_U24this_0(),
	U3CCustomUpdateU3Ec__Iterator0_t97322721::get_offset_of_U24current_1(),
	U3CCustomUpdateU3Ec__Iterator0_t97322721::get_offset_of_U24disposing_2(),
	U3CCustomUpdateU3Ec__Iterator0_t97322721::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (FollowTarget_t1456261924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2901[1] = 
{
	FollowTarget_t1456261924::get_offset_of_target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (NYARSetup_t1519484188), -1, sizeof(NYARSetup_t1519484188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2902[8] = 
{
	NYARSetup_t1519484188_StaticFields::get_offset_of_Instance_4(),
	NYARSetup_t1519484188::get_offset_of_BGCam_5(),
	NYARSetup_t1519484188::get_offset_of_projectorMat_6(),
	NYARSetup_t1519484188::get_offset_of_projectorIgnoreLayerMask_7(),
	NYARSetup_t1519484188::get_offset_of_bgCamLayerMask_8(),
	NYARSetup_t1519484188::get_offset_of_arCameraScreenShot_9(),
	NYARSetup_t1519484188::get_offset_of_RTtPScript_10(),
	NYARSetup_t1519484188::get_offset_of_uiFrontCameraImage_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (ProjectorCopyCameraData_t3967883643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[1] = 
{
	ProjectorCopyCameraData_t3967883643::get_offset_of_targetCamera_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (RenderTextureToProjector_t1529163424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[2] = 
{
	RenderTextureToProjector_t1529163424::get_offset_of__texture_4(),
	RenderTextureToProjector_t1529163424::get_offset_of__projector_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (MinAttribute_t1755422200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[1] = 
{
	MinAttribute_t1755422200::get_offset_of_Min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (BaseEffect_t1187847871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[2] = 
{
	BaseEffect_t1187847871::get_offset_of_Shader_4(),
	BaseEffect_t1187847871::get_offset_of_m_Material_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (CLib_t276046374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (AnalogTV_t1398652140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[11] = 
{
	AnalogTV_t1398652140::get_offset_of_AutomaticPhase_6(),
	AnalogTV_t1398652140::get_offset_of_Phase_7(),
	AnalogTV_t1398652140::get_offset_of_ConvertToGrayscale_8(),
	AnalogTV_t1398652140::get_offset_of_NoiseIntensity_9(),
	AnalogTV_t1398652140::get_offset_of_ScanlinesIntensity_10(),
	AnalogTV_t1398652140::get_offset_of_ScanlinesCount_11(),
	AnalogTV_t1398652140::get_offset_of_ScanlinesOffset_12(),
	AnalogTV_t1398652140::get_offset_of_VerticalScanlines_13(),
	AnalogTV_t1398652140::get_offset_of_Distortion_14(),
	AnalogTV_t1398652140::get_offset_of_CubicDistortion_15(),
	AnalogTV_t1398652140::get_offset_of_Scale_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (BilateralGaussianBlur_t938878669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[3] = 
{
	BilateralGaussianBlur_t938878669::get_offset_of_Passes_6(),
	BilateralGaussianBlur_t938878669::get_offset_of_Threshold_7(),
	BilateralGaussianBlur_t938878669::get_offset_of_Amount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (BleachBypass_t2568391998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[1] = 
{
	BleachBypass_t2568391998::get_offset_of_Amount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (Blend_t936097170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[3] = 
{
	Blend_t936097170::get_offset_of_Texture_6(),
	Blend_t936097170::get_offset_of_Amount_7(),
	Blend_t936097170::get_offset_of_Mode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (BlendingMode_t1651387113)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2912[22] = 
{
	BlendingMode_t1651387113::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (BrightnessContrastGamma_t158109514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[4] = 
{
	BrightnessContrastGamma_t158109514::get_offset_of_Brightness_6(),
	BrightnessContrastGamma_t158109514::get_offset_of_Contrast_7(),
	BrightnessContrastGamma_t158109514::get_offset_of_ContrastCoeff_8(),
	BrightnessContrastGamma_t158109514::get_offset_of_Gamma_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (ChannelClamper_t4227510363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[3] = 
{
	ChannelClamper_t4227510363::get_offset_of_Red_6(),
	ChannelClamper_t4227510363::get_offset_of_Green_7(),
	ChannelClamper_t4227510363::get_offset_of_Blue_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (ChannelMixer_t2404774860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[4] = 
{
	ChannelMixer_t2404774860::get_offset_of_Red_6(),
	ChannelMixer_t2404774860::get_offset_of_Green_7(),
	ChannelMixer_t2404774860::get_offset_of_Blue_8(),
	ChannelMixer_t2404774860::get_offset_of_Constant_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (ChannelSwapper_t1718792779), -1, sizeof(ChannelSwapper_t1718792779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2916[4] = 
{
	ChannelSwapper_t1718792779::get_offset_of_RedSource_6(),
	ChannelSwapper_t1718792779::get_offset_of_GreenSource_7(),
	ChannelSwapper_t1718792779::get_offset_of_BlueSource_8(),
	ChannelSwapper_t1718792779_StaticFields::get_offset_of_m_Channels_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (Channel_t1767287641)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2917[4] = 
{
	Channel_t1767287641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (ChromaticAberration_t2529873745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2918[4] = 
{
	ChromaticAberration_t2529873745::get_offset_of_RedRefraction_6(),
	ChromaticAberration_t2529873745::get_offset_of_GreenRefraction_7(),
	ChromaticAberration_t2529873745::get_offset_of_BlueRefraction_8(),
	ChromaticAberration_t2529873745::get_offset_of_PreserveAlpha_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (ComicBook_t2243422766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[12] = 
{
	ComicBook_t2243422766::get_offset_of_StripAngle_6(),
	ComicBook_t2243422766::get_offset_of_StripDensity_7(),
	ComicBook_t2243422766::get_offset_of_StripThickness_8(),
	ComicBook_t2243422766::get_offset_of_StripLimits_9(),
	ComicBook_t2243422766::get_offset_of_StripInnerColor_10(),
	ComicBook_t2243422766::get_offset_of_StripOuterColor_11(),
	ComicBook_t2243422766::get_offset_of_FillColor_12(),
	ComicBook_t2243422766::get_offset_of_BackgroundColor_13(),
	ComicBook_t2243422766::get_offset_of_EdgeDetection_14(),
	ComicBook_t2243422766::get_offset_of_EdgeThreshold_15(),
	ComicBook_t2243422766::get_offset_of_EdgeColor_16(),
	ComicBook_t2243422766::get_offset_of_Amount_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (ContrastGain_t1677431729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[1] = 
{
	ContrastGain_t1677431729::get_offset_of_Gain_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (ContrastVignette_t2834033980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2921[6] = 
{
	ContrastVignette_t2834033980::get_offset_of_Center_6(),
	ContrastVignette_t2834033980::get_offset_of_Sharpness_7(),
	ContrastVignette_t2834033980::get_offset_of_Darkness_8(),
	ContrastVignette_t2834033980::get_offset_of_Contrast_9(),
	ContrastVignette_t2834033980::get_offset_of_ContrastCoeff_10(),
	ContrastVignette_t2834033980::get_offset_of_EdgeBlending_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (Convolution3x3_t1353237119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[5] = 
{
	Convolution3x3_t1353237119::get_offset_of_KernelTop_6(),
	Convolution3x3_t1353237119::get_offset_of_KernelMiddle_7(),
	Convolution3x3_t1353237119::get_offset_of_KernelBottom_8(),
	Convolution3x3_t1353237119::get_offset_of_Divisor_9(),
	Convolution3x3_t1353237119::get_offset_of_Amount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (CrossStitch_t1042481064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[4] = 
{
	CrossStitch_t1042481064::get_offset_of_Size_6(),
	CrossStitch_t1042481064::get_offset_of_Brightness_7(),
	CrossStitch_t1042481064::get_offset_of_Invert_8(),
	CrossStitch_t1042481064::get_offset_of_Pixelize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (DirectionalBlur_t13325309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	DirectionalBlur_t13325309::get_offset_of_Quality_6(),
	DirectionalBlur_t13325309::get_offset_of_Samples_7(),
	DirectionalBlur_t13325309::get_offset_of_Strength_8(),
	DirectionalBlur_t13325309::get_offset_of_Angle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (QualityPreset_t119956193)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2925[5] = 
{
	QualityPreset_t119956193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (Dithering_t358002770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[7] = 
{
	Dithering_t358002770::get_offset_of_ShowOriginal_6(),
	Dithering_t358002770::get_offset_of_ConvertToGrayscale_7(),
	Dithering_t358002770::get_offset_of_RedLuminance_8(),
	Dithering_t358002770::get_offset_of_GreenLuminance_9(),
	Dithering_t358002770::get_offset_of_BlueLuminance_10(),
	Dithering_t358002770::get_offset_of_Amount_11(),
	Dithering_t358002770::get_offset_of_m_DitherPattern_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (DoubleVision_t3179333181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[2] = 
{
	DoubleVision_t3179333181::get_offset_of_Displace_6(),
	DoubleVision_t3179333181::get_offset_of_Amount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (DynamicLookup_t4028714612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[9] = 
{
	DynamicLookup_t4028714612::get_offset_of_White_6(),
	DynamicLookup_t4028714612::get_offset_of_Black_7(),
	DynamicLookup_t4028714612::get_offset_of_Red_8(),
	DynamicLookup_t4028714612::get_offset_of_Green_9(),
	DynamicLookup_t4028714612::get_offset_of_Blue_10(),
	DynamicLookup_t4028714612::get_offset_of_Yellow_11(),
	DynamicLookup_t4028714612::get_offset_of_Magenta_12(),
	DynamicLookup_t4028714612::get_offset_of_Cyan_13(),
	DynamicLookup_t4028714612::get_offset_of_Amount_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (FastVignette_t1694713166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[5] = 
{
	FastVignette_t1694713166::get_offset_of_Mode_6(),
	FastVignette_t1694713166::get_offset_of_Color_7(),
	FastVignette_t1694713166::get_offset_of_Center_8(),
	FastVignette_t1694713166::get_offset_of_Sharpness_9(),
	FastVignette_t1694713166::get_offset_of_Darkness_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (ColorMode_t3494539450)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2930[4] = 
{
	ColorMode_t3494539450::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (Frost_t3645176307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[4] = 
{
	Frost_t3645176307::get_offset_of_Scale_6(),
	Frost_t3645176307::get_offset_of_Sharpness_7(),
	Frost_t3645176307::get_offset_of_Darkness_8(),
	Frost_t3645176307::get_offset_of_EnableVignette_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (GaussianBlur_t3820976243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[3] = 
{
	GaussianBlur_t3820976243::get_offset_of_Passes_6(),
	GaussianBlur_t3820976243::get_offset_of_Downscaling_7(),
	GaussianBlur_t3820976243::get_offset_of_Amount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (Glitch_t3656535212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[11] = 
{
	Glitch_t3656535212::get_offset_of_RandomActivation_6(),
	Glitch_t3656535212::get_offset_of_RandomEvery_7(),
	Glitch_t3656535212::get_offset_of_RandomDuration_8(),
	Glitch_t3656535212::get_offset_of_Mode_9(),
	Glitch_t3656535212::get_offset_of_SettingsInterferences_10(),
	Glitch_t3656535212::get_offset_of_SettingsTearing_11(),
	Glitch_t3656535212::get_offset_of_m_Activated_12(),
	Glitch_t3656535212::get_offset_of_m_EveryTimer_13(),
	Glitch_t3656535212::get_offset_of_m_EveryTimerEnd_14(),
	Glitch_t3656535212::get_offset_of_m_DurationTimer_15(),
	Glitch_t3656535212::get_offset_of_m_DurationTimerEnd_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (GlitchingMode_t3305168335)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2934[4] = 
{
	GlitchingMode_t3305168335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (InterferenceSettings_t2118005662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[3] = 
{
	InterferenceSettings_t2118005662::get_offset_of_Speed_0(),
	InterferenceSettings_t2118005662::get_offset_of_Density_1(),
	InterferenceSettings_t2118005662::get_offset_of_MaxDisplacement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (TearingSettings_t1139179787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2936[6] = 
{
	TearingSettings_t1139179787::get_offset_of_Speed_0(),
	TearingSettings_t1139179787::get_offset_of_Intensity_1(),
	TearingSettings_t1139179787::get_offset_of_MaxDisplacement_2(),
	TearingSettings_t1139179787::get_offset_of_AllowFlipping_3(),
	TearingSettings_t1139179787::get_offset_of_YuvColorBleeding_4(),
	TearingSettings_t1139179787::get_offset_of_YuvOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (GradientRamp_t513234030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[2] = 
{
	GradientRamp_t513234030::get_offset_of_RampTexture_6(),
	GradientRamp_t513234030::get_offset_of_Amount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (GradientRampDynamic_t2043771246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[3] = 
{
	GradientRampDynamic_t2043771246::get_offset_of_Ramp_6(),
	GradientRampDynamic_t2043771246::get_offset_of_Amount_7(),
	GradientRampDynamic_t2043771246::get_offset_of_m_RampTexture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (GrainyBlur_t745788970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[2] = 
{
	GrainyBlur_t745788970::get_offset_of_Radius_6(),
	GrainyBlur_t745788970::get_offset_of_Samples_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (Grayscale_t3716926545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[4] = 
{
	Grayscale_t3716926545::get_offset_of_RedLuminance_6(),
	Grayscale_t3716926545::get_offset_of_GreenLuminance_7(),
	Grayscale_t3716926545::get_offset_of_BlueLuminance_8(),
	Grayscale_t3716926545::get_offset_of_Amount_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (Halftone_t1918305288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[6] = 
{
	Halftone_t1918305288::get_offset_of_Scale_6(),
	Halftone_t1918305288::get_offset_of_DotSize_7(),
	Halftone_t1918305288::get_offset_of_Angle_8(),
	Halftone_t1918305288::get_offset_of_Smoothness_9(),
	Halftone_t1918305288::get_offset_of_Center_10(),
	Halftone_t1918305288::get_offset_of_Desaturate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (Histogram_t2046042414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (HueFocus_t3289091382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[4] = 
{
	HueFocus_t3289091382::get_offset_of_Hue_6(),
	HueFocus_t3289091382::get_offset_of_Range_7(),
	HueFocus_t3289091382::get_offset_of_Boost_8(),
	HueFocus_t3289091382::get_offset_of_Amount_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (HueSaturationValue_t2444304774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[22] = 
{
	HueSaturationValue_t2444304774::get_offset_of_MasterHue_6(),
	HueSaturationValue_t2444304774::get_offset_of_MasterSaturation_7(),
	HueSaturationValue_t2444304774::get_offset_of_MasterValue_8(),
	HueSaturationValue_t2444304774::get_offset_of_RedsHue_9(),
	HueSaturationValue_t2444304774::get_offset_of_RedsSaturation_10(),
	HueSaturationValue_t2444304774::get_offset_of_RedsValue_11(),
	HueSaturationValue_t2444304774::get_offset_of_YellowsHue_12(),
	HueSaturationValue_t2444304774::get_offset_of_YellowsSaturation_13(),
	HueSaturationValue_t2444304774::get_offset_of_YellowsValue_14(),
	HueSaturationValue_t2444304774::get_offset_of_GreensHue_15(),
	HueSaturationValue_t2444304774::get_offset_of_GreensSaturation_16(),
	HueSaturationValue_t2444304774::get_offset_of_GreensValue_17(),
	HueSaturationValue_t2444304774::get_offset_of_CyansHue_18(),
	HueSaturationValue_t2444304774::get_offset_of_CyansSaturation_19(),
	HueSaturationValue_t2444304774::get_offset_of_CyansValue_20(),
	HueSaturationValue_t2444304774::get_offset_of_BluesHue_21(),
	HueSaturationValue_t2444304774::get_offset_of_BluesSaturation_22(),
	HueSaturationValue_t2444304774::get_offset_of_BluesValue_23(),
	HueSaturationValue_t2444304774::get_offset_of_MagentasHue_24(),
	HueSaturationValue_t2444304774::get_offset_of_MagentasSaturation_25(),
	HueSaturationValue_t2444304774::get_offset_of_MagentasValue_26(),
	HueSaturationValue_t2444304774::get_offset_of_AdvancedMode_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (Kuwahara_t240745109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	Kuwahara_t240745109::get_offset_of_Radius_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (Led_t3330730803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[6] = 
{
	Led_t3330730803::get_offset_of_Scale_6(),
	Led_t3330730803::get_offset_of_Brightness_7(),
	Led_t3330730803::get_offset_of_Shape_8(),
	Led_t3330730803::get_offset_of_AutomaticRatio_9(),
	Led_t3330730803::get_offset_of_Ratio_10(),
	Led_t3330730803::get_offset_of_Mode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (SizeMode_t1428983497)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2947[3] = 
{
	SizeMode_t1428983497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (LensDistortionBlur_t2959671745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2948[5] = 
{
	LensDistortionBlur_t2959671745::get_offset_of_Quality_6(),
	LensDistortionBlur_t2959671745::get_offset_of_Samples_7(),
	LensDistortionBlur_t2959671745::get_offset_of_Distortion_8(),
	LensDistortionBlur_t2959671745::get_offset_of_CubicDistortion_9(),
	LensDistortionBlur_t2959671745::get_offset_of_Scale_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (QualityPreset_t618172025)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2949[5] = 
{
	QualityPreset_t618172025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (Letterbox_t3911859827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	Letterbox_t3911859827::get_offset_of_Aspect_6(),
	Letterbox_t3911859827::get_offset_of_FillColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (Levels_t1894673157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[9] = 
{
	Levels_t1894673157::get_offset_of_Mode_6(),
	Levels_t1894673157::get_offset_of_InputL_7(),
	Levels_t1894673157::get_offset_of_InputR_8(),
	Levels_t1894673157::get_offset_of_InputG_9(),
	Levels_t1894673157::get_offset_of_InputB_10(),
	Levels_t1894673157::get_offset_of_OutputL_11(),
	Levels_t1894673157::get_offset_of_OutputR_12(),
	Levels_t1894673157::get_offset_of_OutputG_13(),
	Levels_t1894673157::get_offset_of_OutputB_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (ColorMode_t803725769)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2952[3] = 
{
	ColorMode_t803725769::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (LoFiPalette_t3780803221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[4] = 
{
	LoFiPalette_t3780803221::get_offset_of_Palette_14(),
	LoFiPalette_t3780803221::get_offset_of_Pixelize_15(),
	LoFiPalette_t3780803221::get_offset_of_PixelSize_16(),
	LoFiPalette_t3780803221::get_offset_of_m_CurrentPreset_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (Preset_t400928058)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2954[33] = 
{
	Preset_t400928058::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (LookupFilter_t4073759679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[2] = 
{
	LookupFilter_t4073759679::get_offset_of_LookupTexture_6(),
	LookupFilter_t4073759679::get_offset_of_Amount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (LookupFilter3D_t3361124730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[10] = 
{
	LookupFilter3D_t3361124730::get_offset_of_LookupTexture_4(),
	LookupFilter3D_t3361124730::get_offset_of_Amount_5(),
	LookupFilter3D_t3361124730::get_offset_of_ForceCompatibility_6(),
	LookupFilter3D_t3361124730::get_offset_of_m_Lut3D_7(),
	LookupFilter3D_t3361124730::get_offset_of_m_BaseTextureName_8(),
	LookupFilter3D_t3361124730::get_offset_of_m_Use2DLut_9(),
	LookupFilter3D_t3361124730::get_offset_of_Shader2D_10(),
	LookupFilter3D_t3361124730::get_offset_of_Shader3D_11(),
	LookupFilter3D_t3361124730::get_offset_of_m_Material2D_12(),
	LookupFilter3D_t3361124730::get_offset_of_m_Material3D_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (Negative_t2741149473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[1] = 
{
	Negative_t2741149473::get_offset_of_Amount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (Noise_t3271901434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[5] = 
{
	Noise_t3271901434::get_offset_of_Mode_6(),
	Noise_t3271901434::get_offset_of_Animate_7(),
	Noise_t3271901434::get_offset_of_Seed_8(),
	Noise_t3271901434::get_offset_of_Strength_9(),
	Noise_t3271901434::get_offset_of_LumContribution_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (ColorMode_t2454795589)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2959[3] = 
{
	ColorMode_t2454795589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (PhotoFilter_t50036371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2960[2] = 
{
	PhotoFilter_t50036371::get_offset_of_Color_6(),
	PhotoFilter_t50036371::get_offset_of_Density_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (Pixelate_t1523178201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[4] = 
{
	Pixelate_t1523178201::get_offset_of_Scale_6(),
	Pixelate_t1523178201::get_offset_of_AutomaticRatio_7(),
	Pixelate_t1523178201::get_offset_of_Ratio_8(),
	Pixelate_t1523178201::get_offset_of_Mode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (SizeMode_t3498286277)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2962[3] = 
{
	SizeMode_t3498286277::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (PixelMatrix_t927513071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[3] = 
{
	PixelMatrix_t927513071::get_offset_of_Size_6(),
	PixelMatrix_t927513071::get_offset_of_Brightness_7(),
	PixelMatrix_t927513071::get_offset_of_BlackBorder_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (Posterize_t3175588324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[3] = 
{
	Posterize_t3175588324::get_offset_of_Levels_6(),
	Posterize_t3175588324::get_offset_of_Amount_7(),
	Posterize_t3175588324::get_offset_of_LuminosityOnly_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (RadialBlur_t147563976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[7] = 
{
	RadialBlur_t147563976::get_offset_of_Strength_6(),
	RadialBlur_t147563976::get_offset_of_Samples_7(),
	RadialBlur_t147563976::get_offset_of_Center_8(),
	RadialBlur_t147563976::get_offset_of_Quality_9(),
	RadialBlur_t147563976::get_offset_of_Sharpness_10(),
	RadialBlur_t147563976::get_offset_of_Darkness_11(),
	RadialBlur_t147563976::get_offset_of_EnableVignette_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (QualityPreset_t1040903510)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2966[5] = 
{
	QualityPreset_t1040903510::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (RGBSplit_t656033929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2967[2] = 
{
	RGBSplit_t656033929::get_offset_of_Amount_6(),
	RGBSplit_t656033929::get_offset_of_Angle_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2968 = { sizeof (SCurveContrast_t1916604434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2968[6] = 
{
	SCurveContrast_t1916604434::get_offset_of_RedSteepness_6(),
	SCurveContrast_t1916604434::get_offset_of_RedGamma_7(),
	SCurveContrast_t1916604434::get_offset_of_GreenSteepness_8(),
	SCurveContrast_t1916604434::get_offset_of_GreenGamma_9(),
	SCurveContrast_t1916604434::get_offset_of_BlueSteepness_10(),
	SCurveContrast_t1916604434::get_offset_of_BlueGamma_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2969 = { sizeof (ShadowsMidtonesHighlights_t2681697010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2969[5] = 
{
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Mode_6(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Shadows_7(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Midtones_8(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Highlights_9(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Amount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2970 = { sizeof (ColorMode_t2800918933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2970[3] = 
{
	ColorMode_t2800918933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2971 = { sizeof (Sharpen_t1993388953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2971[3] = 
{
	Sharpen_t1993388953::get_offset_of_Mode_6(),
	Sharpen_t1993388953::get_offset_of_Strength_7(),
	Sharpen_t1993388953::get_offset_of_Clamp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2972 = { sizeof (Algorithm_t3262665680)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2972[3] = 
{
	Algorithm_t3262665680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2973 = { sizeof (SmartSaturation_t3817639239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2973[3] = 
{
	SmartSaturation_t3817639239::get_offset_of_Boost_6(),
	SmartSaturation_t3817639239::get_offset_of_Curve_7(),
	SmartSaturation_t3817639239::get_offset_of__CurveTexture_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2974 = { sizeof (Strokes_t892401832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2974[10] = 
{
	Strokes_t892401832::get_offset_of_Mode_6(),
	Strokes_t892401832::get_offset_of_Amplitude_7(),
	Strokes_t892401832::get_offset_of_Frequency_8(),
	Strokes_t892401832::get_offset_of_Scaling_9(),
	Strokes_t892401832::get_offset_of_MaxThickness_10(),
	Strokes_t892401832::get_offset_of_Threshold_11(),
	Strokes_t892401832::get_offset_of_Harshness_12(),
	Strokes_t892401832::get_offset_of_RedLuminance_13(),
	Strokes_t892401832::get_offset_of_GreenLuminance_14(),
	Strokes_t892401832::get_offset_of_BlueLuminance_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2975 = { sizeof (ColorMode_t650858100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2975[7] = 
{
	ColorMode_t650858100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2976 = { sizeof (Technicolor_t2196134954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2976[3] = 
{
	Technicolor_t2196134954::get_offset_of_Exposure_6(),
	Technicolor_t2196134954::get_offset_of_Balance_7(),
	Technicolor_t2196134954::get_offset_of_Amount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2977 = { sizeof (Threshold_t1192544618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2977[3] = 
{
	Threshold_t1192544618::get_offset_of_Value_6(),
	Threshold_t1192544618::get_offset_of_NoiseRange_7(),
	Threshold_t1192544618::get_offset_of_UseNoise_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2978 = { sizeof (TVVignette_t3643404851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2978[2] = 
{
	TVVignette_t3643404851::get_offset_of_Size_6(),
	TVVignette_t3643404851::get_offset_of_Offset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2979 = { sizeof (Vibrance_t2988503215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2979[5] = 
{
	Vibrance_t2988503215::get_offset_of_Amount_6(),
	Vibrance_t2988503215::get_offset_of_RedChannel_7(),
	Vibrance_t2988503215::get_offset_of_GreenChannel_8(),
	Vibrance_t2988503215::get_offset_of_BlueChannel_9(),
	Vibrance_t2988503215::get_offset_of_AdvancedMode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2980 = { sizeof (Vintage_t2551956078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2980[2] = 
{
	Vintage_t2551956078::get_offset_of_Filter_8(),
	Vintage_t2551956078::get_offset_of_m_CurrentFilter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2981 = { sizeof (InstragramFilter_t685827072)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2981[29] = 
{
	InstragramFilter_t685827072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2982 = { sizeof (VintageFast_t3040349303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2982[2] = 
{
	VintageFast_t3040349303::get_offset_of_Filter_14(),
	VintageFast_t3040349303::get_offset_of_m_CurrentFilter_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2983 = { sizeof (WaveDistortion_t3073420756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2983[4] = 
{
	WaveDistortion_t3073420756::get_offset_of_Amplitude_6(),
	WaveDistortion_t3073420756::get_offset_of_Waves_7(),
	WaveDistortion_t3073420756::get_offset_of_ColorGlitch_8(),
	WaveDistortion_t3073420756::get_offset_of_Phase_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2984 = { sizeof (WhiteBalance_t1550391260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2984[2] = 
{
	WhiteBalance_t1550391260::get_offset_of_White_6(),
	WhiteBalance_t1550391260::get_offset_of_Mode_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2985 = { sizeof (BalanceMode_t686342893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2985[3] = 
{
	BalanceMode_t686342893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2986 = { sizeof (Wiggle_t4291210129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2986[6] = 
{
	Wiggle_t4291210129::get_offset_of_Mode_6(),
	Wiggle_t4291210129::get_offset_of_Timer_7(),
	Wiggle_t4291210129::get_offset_of_Speed_8(),
	Wiggle_t4291210129::get_offset_of_Frequency_9(),
	Wiggle_t4291210129::get_offset_of_Amplitude_10(),
	Wiggle_t4291210129::get_offset_of_AutomaticTimer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2987 = { sizeof (Algorithm_t3898688573)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2987[3] = 
{
	Algorithm_t3898688573::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2988 = { sizeof (Test_t650638817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2988[1] = 
{
	Test_t650638817::get_offset_of_ScreenshotName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2989 = { sizeof (U3CdelayedShareU3Ec__Iterator0_t2132201013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2989[5] = 
{
	U3CdelayedShareU3Ec__Iterator0_t2132201013::get_offset_of_screenShotPath_0(),
	U3CdelayedShareU3Ec__Iterator0_t2132201013::get_offset_of_text_1(),
	U3CdelayedShareU3Ec__Iterator0_t2132201013::get_offset_of_U24current_2(),
	U3CdelayedShareU3Ec__Iterator0_t2132201013::get_offset_of_U24disposing_3(),
	U3CdelayedShareU3Ec__Iterator0_t2132201013::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2990 = { sizeof (U3CGetScreenshotU3Ec__Iterator1_t1577098861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2990[5] = 
{
	U3CGetScreenshotU3Ec__Iterator1_t1577098861::get_offset_of_U3CscreenshotU3E__0_0(),
	U3CGetScreenshotU3Ec__Iterator1_t1577098861::get_offset_of_U24this_1(),
	U3CGetScreenshotU3Ec__Iterator1_t1577098861::get_offset_of_U24current_2(),
	U3CGetScreenshotU3Ec__Iterator1_t1577098861::get_offset_of_U24disposing_3(),
	U3CGetScreenshotU3Ec__Iterator1_t1577098861::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2991 = { sizeof (U3CDelayedShare_ImageU3Ec__Iterator2_t908651865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2991[5] = 
{
	U3CDelayedShare_ImageU3Ec__Iterator2_t908651865::get_offset_of_screenShotPath_0(),
	U3CDelayedShare_ImageU3Ec__Iterator2_t908651865::get_offset_of_U24this_1(),
	U3CDelayedShare_ImageU3Ec__Iterator2_t908651865::get_offset_of_U24current_2(),
	U3CDelayedShare_ImageU3Ec__Iterator2_t908651865::get_offset_of_U24disposing_3(),
	U3CDelayedShare_ImageU3Ec__Iterator2_t908651865::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2992 = { sizeof (NativeShare_t4027546635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2993 = { sizeof (ConfigStruct_t1540812723)+ sizeof (RuntimeObject), sizeof(ConfigStruct_t1540812723_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2993[2] = 
{
	ConfigStruct_t1540812723::get_offset_of_title_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigStruct_t1540812723::get_offset_of_message_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2994 = { sizeof (SocialSharingStruct_t2558757706)+ sizeof (RuntimeObject), sizeof(SocialSharingStruct_t2558757706_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2994[3] = 
{
	SocialSharingStruct_t2558757706::get_offset_of_text_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SocialSharingStruct_t2558757706::get_offset_of_subject_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SocialSharingStruct_t2558757706::get_offset_of_filePaths_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2995 = { sizeof (dance_mocap_01_follow_player_t692022596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2995[3] = 
{
	dance_mocap_01_follow_player_t692022596::get_offset_of_Player_4(),
	dance_mocap_01_follow_player_t692022596::get_offset_of_cameraHeight_5(),
	dance_mocap_01_follow_player_t692022596::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2996 = { sizeof (dance_mocap_02_follow_player_t3181538630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2996[3] = 
{
	dance_mocap_02_follow_player_t3181538630::get_offset_of_Player_4(),
	dance_mocap_02_follow_player_t3181538630::get_offset_of_cameraHeight_5(),
	dance_mocap_02_follow_player_t3181538630::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2997 = { sizeof (dance_mocap_03_follow_player_t545090885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2997[3] = 
{
	dance_mocap_03_follow_player_t545090885::get_offset_of_Player_4(),
	dance_mocap_03_follow_player_t545090885::get_offset_of_cameraHeight_5(),
	dance_mocap_03_follow_player_t545090885::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2998 = { sizeof (camera_follow_dance_pack_05_t545415106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2998[3] = 
{
	camera_follow_dance_pack_05_t545415106::get_offset_of_player_4(),
	camera_follow_dance_pack_05_t545415106::get_offset_of_cameraHeight_5(),
	camera_follow_dance_pack_05_t545415106::get_offset_of_cameraDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2999 = { sizeof (follow_player_t1274992039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2999[3] = 
{
	follow_player_t1274992039::get_offset_of_player_4(),
	follow_player_t1274992039::get_offset_of_cameraHeight_5(),
	follow_player_t1274992039::get_offset_of_cameraDistance_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
