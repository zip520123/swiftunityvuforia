﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AREndingEventSender
struct AREndingEventSender_t1071853717;
// LotteryWebApi
struct LotteryWebApi_t3108453562;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// StageJumpAnimation
struct StageJumpAnimation_t2635268956;
// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable>
struct Action_3_t3876410139;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute>
struct Dictionary_2_t907029268;
// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Playables.Playable>
struct Dictionary_2_t3310777870;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t3973553389;
// System.Collections.Generic.HashSet`1<UnityEngine.Playables.PlayableDirector>
struct HashSet_1_t3368433767;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset>
struct IEnumerable_1_t1808561134;
// System.Collections.Generic.List`1<UnityEngine.ParticleSystem>
struct List_1_t3272854023;
// System.Collections.Generic.List`1<UnityEngine.Playables.PlayableDirector>
struct List_1_t1980591739;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_t4000433264;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo>
struct List_1_t3454752663;
// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback>
struct List_1_t2313598679;
// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement>
struct List_1_t3540616959;
// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip>
struct List_1_t3950300692;
// System.Comparison`1<UnityEngine.Timeline.TimelineClip>
struct Comparison_1_t2253157129;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t148644517;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UIPhotoGalleryManager
struct UIPhotoGalleryManager_t2672000691;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t143221404;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AvatarMask
struct AvatarMask_t1182482518;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement>
struct IntervalTree_1_t478208184;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Playables.PlayableAsset
struct PlayableAsset_t3219022681;
// UnityEngine.Playables.PlayableBinding/CreateOutputMethod
struct CreateOutputMethod_t2301811773;
// UnityEngine.Playables.PlayableBinding[]
struct PlayableBindingU5BU5D_t829358056;
// UnityEngine.Playables.PlayableDirector
struct PlayableDirector_t508516997;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.Timeline.ActivationMixerPlayable
struct ActivationMixerPlayable_t2212523045;
// UnityEngine.Timeline.AnimationPlayableAsset
struct AnimationPlayableAsset_t734882934;
// UnityEngine.Timeline.AnimationTrack
struct AnimationTrack_t3872729528;
// UnityEngine.Timeline.AudioPlayableAsset
struct AudioPlayableAsset_t2575444864;
// UnityEngine.Timeline.AudioTrack
struct AudioTrack_t1549411460;
// UnityEngine.Timeline.ITimeControl
struct ITimeControl_t1934324661;
// UnityEngine.Timeline.TimelineClip
struct TimelineClip_t2478225950;
// UnityEngine.Timeline.TimelineClip[]
struct TimelineClipU5BU5D_t140784555;
// UnityEngine.Timeline.TrackAsset
struct TrackAsset_t2828708245;
// UnityEngine.Timeline.TrackAsset[]
struct TrackAssetU5BU5D_t52518008;
// UnityEngine.Timeline.TrackBindingTypeAttribute
struct TrackBindingTypeAttribute_t2757649500;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1514609158;

struct Object_t631007953_marshaled_com;



#ifndef U3CMODULEU3E_T692745559_H
#define U3CMODULEU3E_T692745559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745559 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745559_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CARENDINGU3EC__ITERATOR0_T1051696702_H
#define U3CARENDINGU3EC__ITERATOR0_T1051696702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AREndingEventSender/<AREnding>c__Iterator0
struct  U3CAREndingU3Ec__Iterator0_t1051696702  : public RuntimeObject
{
public:
	// AREndingEventSender AREndingEventSender/<AREnding>c__Iterator0::$this
	AREndingEventSender_t1071853717 * ___U24this_0;
	// System.Object AREndingEventSender/<AREnding>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AREndingEventSender/<AREnding>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AREndingEventSender/<AREnding>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAREndingU3Ec__Iterator0_t1051696702, ___U24this_0)); }
	inline AREndingEventSender_t1071853717 * get_U24this_0() const { return ___U24this_0; }
	inline AREndingEventSender_t1071853717 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AREndingEventSender_t1071853717 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAREndingU3Ec__Iterator0_t1051696702, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAREndingU3Ec__Iterator0_t1051696702, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAREndingU3Ec__Iterator0_t1051696702, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CARENDINGU3EC__ITERATOR0_T1051696702_H
#ifndef U3CDOLOTTERYU3EC__ITERATOR0_T3551506773_H
#define U3CDOLOTTERYU3EC__ITERATOR0_T3551506773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LotteryWebApi/<DoLottery>c__Iterator0
struct  U3CDoLotteryU3Ec__Iterator0_t3551506773  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm LotteryWebApi/<DoLottery>c__Iterator0::<userData>__0
	WWWForm_t4064702195 * ___U3CuserDataU3E__0_0;
	// UnityEngine.WWW LotteryWebApi/<DoLottery>c__Iterator0::<lotteryWWW>__0
	WWW_t3688466362 * ___U3ClotteryWWWU3E__0_1;
	// LotteryWebApi LotteryWebApi/<DoLottery>c__Iterator0::$this
	LotteryWebApi_t3108453562 * ___U24this_2;
	// System.Object LotteryWebApi/<DoLottery>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LotteryWebApi/<DoLottery>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 LotteryWebApi/<DoLottery>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CuserDataU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoLotteryU3Ec__Iterator0_t3551506773, ___U3CuserDataU3E__0_0)); }
	inline WWWForm_t4064702195 * get_U3CuserDataU3E__0_0() const { return ___U3CuserDataU3E__0_0; }
	inline WWWForm_t4064702195 ** get_address_of_U3CuserDataU3E__0_0() { return &___U3CuserDataU3E__0_0; }
	inline void set_U3CuserDataU3E__0_0(WWWForm_t4064702195 * value)
	{
		___U3CuserDataU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserDataU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClotteryWWWU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoLotteryU3Ec__Iterator0_t3551506773, ___U3ClotteryWWWU3E__0_1)); }
	inline WWW_t3688466362 * get_U3ClotteryWWWU3E__0_1() const { return ___U3ClotteryWWWU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3ClotteryWWWU3E__0_1() { return &___U3ClotteryWWWU3E__0_1; }
	inline void set_U3ClotteryWWWU3E__0_1(WWW_t3688466362 * value)
	{
		___U3ClotteryWWWU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClotteryWWWU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDoLotteryU3Ec__Iterator0_t3551506773, ___U24this_2)); }
	inline LotteryWebApi_t3108453562 * get_U24this_2() const { return ___U24this_2; }
	inline LotteryWebApi_t3108453562 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LotteryWebApi_t3108453562 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDoLotteryU3Ec__Iterator0_t3551506773, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CDoLotteryU3Ec__Iterator0_t3551506773, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDoLotteryU3Ec__Iterator0_t3551506773, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOLOTTERYU3EC__ITERATOR0_T3551506773_H
#ifndef U3CMONITORDANCESTATUSU3EC__ITERATOR1_T2575928893_H
#define U3CMONITORDANCESTATUSU3EC__ITERATOR1_T2575928893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StageJumpAnimation/<MonitorDanceStatus>c__Iterator1
struct  U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893  : public RuntimeObject
{
public:
	// UnityEngine.Animator StageJumpAnimation/<MonitorDanceStatus>c__Iterator1::<_animator>__0
	Animator_t434523843 * ___U3C_animatorU3E__0_0;
	// System.Single StageJumpAnimation/<MonitorDanceStatus>c__Iterator1::<nowTime>__0
	float ___U3CnowTimeU3E__0_1;
	// StageJumpAnimation StageJumpAnimation/<MonitorDanceStatus>c__Iterator1::$this
	StageJumpAnimation_t2635268956 * ___U24this_2;
	// System.Object StageJumpAnimation/<MonitorDanceStatus>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean StageJumpAnimation/<MonitorDanceStatus>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 StageJumpAnimation/<MonitorDanceStatus>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3C_animatorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893, ___U3C_animatorU3E__0_0)); }
	inline Animator_t434523843 * get_U3C_animatorU3E__0_0() const { return ___U3C_animatorU3E__0_0; }
	inline Animator_t434523843 ** get_address_of_U3C_animatorU3E__0_0() { return &___U3C_animatorU3E__0_0; }
	inline void set_U3C_animatorU3E__0_0(Animator_t434523843 * value)
	{
		___U3C_animatorU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_animatorU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CnowTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893, ___U3CnowTimeU3E__0_1)); }
	inline float get_U3CnowTimeU3E__0_1() const { return ___U3CnowTimeU3E__0_1; }
	inline float* get_address_of_U3CnowTimeU3E__0_1() { return &___U3CnowTimeU3E__0_1; }
	inline void set_U3CnowTimeU3E__0_1(float value)
	{
		___U3CnowTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893, ___U24this_2)); }
	inline StageJumpAnimation_t2635268956 * get_U24this_2() const { return ___U24this_2; }
	inline StageJumpAnimation_t2635268956 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(StageJumpAnimation_t2635268956 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMONITORDANCESTATUSU3EC__ITERATOR1_T2575928893_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef PLAYABLEBEHAVIOUR_T4203540982_H
#define PLAYABLEBEHAVIOUR_T4203540982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBehaviour
struct  PlayableBehaviour_t4203540982  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEBEHAVIOUR_T4203540982_H
#ifndef ANIMATIONPLAYABLEASSETUPGRADE_T3688040745_H
#define ANIMATIONPLAYABLEASSETUPGRADE_T3688040745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/AnimationPlayableAssetUpgrade
struct  AnimationPlayableAssetUpgrade_t3688040745  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEASSETUPGRADE_T3688040745_H
#ifndef ANIMATIONTRACKUPGRADE_T2920516161_H
#define ANIMATIONTRACKUPGRADE_T2920516161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack/AnimationTrackUpgrade
struct  AnimationTrackUpgrade_t2920516161  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRACKUPGRADE_T2920516161_H
#ifndef U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3018919025_H
#define U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3018919025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0
struct  U3CGetControlableScriptsU3Ec__Iterator0_t3018919025  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::root
	GameObject_t1113636619 * ___root_0;
	// UnityEngine.MonoBehaviour[] UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar0
	MonoBehaviourU5BU5D_t2007329276* ___U24locvar0_1;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::<script>__1
	MonoBehaviour_t3962482529 * ___U3CscriptU3E__1_3;
	// UnityEngine.MonoBehaviour UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$current
	MonoBehaviour_t3962482529 * ___U24current_4;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UnityEngine.Timeline.ControlPlayableAsset/<GetControlableScripts>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___root_0)); }
	inline GameObject_t1113636619 * get_root_0() const { return ___root_0; }
	inline GameObject_t1113636619 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GameObject_t1113636619 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24locvar0_1)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CscriptU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U3CscriptU3E__1_3)); }
	inline MonoBehaviour_t3962482529 * get_U3CscriptU3E__1_3() const { return ___U3CscriptU3E__1_3; }
	inline MonoBehaviour_t3962482529 ** get_address_of_U3CscriptU3E__1_3() { return &___U3CscriptU3E__1_3; }
	inline void set_U3CscriptU3E__1_3(MonoBehaviour_t3962482529 * value)
	{
		___U3CscriptU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscriptU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24current_4)); }
	inline MonoBehaviour_t3962482529 * get_U24current_4() const { return ___U24current_4; }
	inline MonoBehaviour_t3962482529 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(MonoBehaviour_t3962482529 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetControlableScriptsU3Ec__Iterator0_t3018919025, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCONTROLABLESCRIPTSU3EC__ITERATOR0_T3018919025_H
#ifndef EXTRAPOLATION_T625958692_H
#define EXTRAPOLATION_T625958692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.Extrapolation
struct  Extrapolation_t625958692  : public RuntimeObject
{
public:

public:
};

struct Extrapolation_t625958692_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.Extrapolation::kMinExtrapolationTime
	double ___kMinExtrapolationTime_0;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.Extrapolation::<>f__am$cache0
	Comparison_1_t2253157129 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_kMinExtrapolationTime_0() { return static_cast<int32_t>(offsetof(Extrapolation_t625958692_StaticFields, ___kMinExtrapolationTime_0)); }
	inline double get_kMinExtrapolationTime_0() const { return ___kMinExtrapolationTime_0; }
	inline double* get_address_of_kMinExtrapolationTime_0() { return &___kMinExtrapolationTime_0; }
	inline void set_kMinExtrapolationTime_0(double value)
	{
		___kMinExtrapolationTime_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Extrapolation_t625958692_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Comparison_1_t2253157129 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Comparison_1_t2253157129 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Comparison_1_t2253157129 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRAPOLATION_T625958692_H
#ifndef HASHUTILITY_T2883916303_H
#define HASHUTILITY_T2883916303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.HashUtility
struct  HashUtility_t2883916303  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHUTILITY_T2883916303_H
#ifndef RUNTIMEELEMENT_T2068542217_H
#define RUNTIMEELEMENT_T2068542217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeElement
struct  RuntimeElement_t2068542217  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Timeline.RuntimeElement::<intervalBit>k__BackingField
	int32_t ___U3CintervalBitU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CintervalBitU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RuntimeElement_t2068542217, ___U3CintervalBitU3Ek__BackingField_0)); }
	inline int32_t get_U3CintervalBitU3Ek__BackingField_0() const { return ___U3CintervalBitU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CintervalBitU3Ek__BackingField_0() { return &___U3CintervalBitU3Ek__BackingField_0; }
	inline void set_U3CintervalBitU3Ek__BackingField_0(int32_t value)
	{
		___U3CintervalBitU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEELEMENT_T2068542217_H
#ifndef TIMEUTILITY_T877350212_H
#define TIMEUTILITY_T877350212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeUtility
struct  TimeUtility_t877350212  : public RuntimeObject
{
public:

public:
};

struct TimeUtility_t877350212_StaticFields
{
public:
	// System.Double UnityEngine.Timeline.TimeUtility::kTimeEpsilon
	double ___kTimeEpsilon_0;
	// System.Double UnityEngine.Timeline.TimeUtility::kFrameRateEpsilon
	double ___kFrameRateEpsilon_1;
	// System.Double UnityEngine.Timeline.TimeUtility::k_MaxTimelineDurationInSeconds
	double ___k_MaxTimelineDurationInSeconds_2;
	// System.Func`2<System.Char,System.Boolean> UnityEngine.Timeline.TimeUtility::<>f__am$cache0
	Func_2_t148644517 * ___U3CU3Ef__amU24cache0_3;
	// System.Func`2<System.Char,System.Boolean> UnityEngine.Timeline.TimeUtility::<>f__am$cache1
	Func_2_t148644517 * ___U3CU3Ef__amU24cache1_4;

public:
	inline static int32_t get_offset_of_kTimeEpsilon_0() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___kTimeEpsilon_0)); }
	inline double get_kTimeEpsilon_0() const { return ___kTimeEpsilon_0; }
	inline double* get_address_of_kTimeEpsilon_0() { return &___kTimeEpsilon_0; }
	inline void set_kTimeEpsilon_0(double value)
	{
		___kTimeEpsilon_0 = value;
	}

	inline static int32_t get_offset_of_kFrameRateEpsilon_1() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___kFrameRateEpsilon_1)); }
	inline double get_kFrameRateEpsilon_1() const { return ___kFrameRateEpsilon_1; }
	inline double* get_address_of_kFrameRateEpsilon_1() { return &___kFrameRateEpsilon_1; }
	inline void set_kFrameRateEpsilon_1(double value)
	{
		___kFrameRateEpsilon_1 = value;
	}

	inline static int32_t get_offset_of_k_MaxTimelineDurationInSeconds_2() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___k_MaxTimelineDurationInSeconds_2)); }
	inline double get_k_MaxTimelineDurationInSeconds_2() const { return ___k_MaxTimelineDurationInSeconds_2; }
	inline double* get_address_of_k_MaxTimelineDurationInSeconds_2() { return &___k_MaxTimelineDurationInSeconds_2; }
	inline void set_k_MaxTimelineDurationInSeconds_2(double value)
	{
		___k_MaxTimelineDurationInSeconds_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t148644517 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t148644517 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t148644517 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(TimeUtility_t877350212_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline Func_2_t148644517 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline Func_2_t148644517 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(Func_2_t148644517 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILITY_T877350212_H
#ifndef TIMELINECREATEUTILITIES_T4099873628_H
#define TIMELINECREATEUTILITIES_T4099873628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities
struct  TimelineCreateUtilities_t4099873628  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINECREATEUTILITIES_T4099873628_H
#ifndef U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T930638780_H
#define U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T930638780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0
struct  U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY0_T930638780_H
#ifndef U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY1_T3269290940_H
#define U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY1_T3269290940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey1
struct  U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t3269290940  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineCreateUtilities/<GenerateUniqueActorName>c__AnonStorey1::result
	String_t* ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t3269290940, ___result_0)); }
	inline String_t* get_result_0() const { return ___result_0; }
	inline String_t** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(String_t* value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEUNIQUEACTORNAMEU3EC__ANONSTOREY1_T3269290940_H
#ifndef TIMELINEMARKER_T2195886951_H
#define TIMELINEMARKER_T2195886951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineMarker
struct  TimelineMarker_t2195886951  : public RuntimeObject
{
public:
	// System.String UnityEngine.Timeline.TimelineMarker::m_Key
	String_t* ___m_Key_0;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TimelineMarker::m_ParentTrack
	TrackAsset_t2828708245 * ___m_ParentTrack_1;
	// System.Double UnityEngine.Timeline.TimelineMarker::m_Time
	double ___m_Time_2;
	// System.Boolean UnityEngine.Timeline.TimelineMarker::m_Selected
	bool ___m_Selected_3;

public:
	inline static int32_t get_offset_of_m_Key_0() { return static_cast<int32_t>(offsetof(TimelineMarker_t2195886951, ___m_Key_0)); }
	inline String_t* get_m_Key_0() const { return ___m_Key_0; }
	inline String_t** get_address_of_m_Key_0() { return &___m_Key_0; }
	inline void set_m_Key_0(String_t* value)
	{
		___m_Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Key_0), value);
	}

	inline static int32_t get_offset_of_m_ParentTrack_1() { return static_cast<int32_t>(offsetof(TimelineMarker_t2195886951, ___m_ParentTrack_1)); }
	inline TrackAsset_t2828708245 * get_m_ParentTrack_1() const { return ___m_ParentTrack_1; }
	inline TrackAsset_t2828708245 ** get_address_of_m_ParentTrack_1() { return &___m_ParentTrack_1; }
	inline void set_m_ParentTrack_1(TrackAsset_t2828708245 * value)
	{
		___m_ParentTrack_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentTrack_1), value);
	}

	inline static int32_t get_offset_of_m_Time_2() { return static_cast<int32_t>(offsetof(TimelineMarker_t2195886951, ___m_Time_2)); }
	inline double get_m_Time_2() const { return ___m_Time_2; }
	inline double* get_address_of_m_Time_2() { return &___m_Time_2; }
	inline void set_m_Time_2(double value)
	{
		___m_Time_2 = value;
	}

	inline static int32_t get_offset_of_m_Selected_3() { return static_cast<int32_t>(offsetof(TimelineMarker_t2195886951, ___m_Selected_3)); }
	inline bool get_m_Selected_3() const { return ___m_Selected_3; }
	inline bool* get_address_of_m_Selected_3() { return &___m_Selected_3; }
	inline void set_m_Selected_3(bool value)
	{
		___m_Selected_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEMARKER_T2195886951_H
#ifndef TIMELINEUNDO_T898409293_H
#define TIMELINEUNDO_T898409293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineUndo
struct  TimelineUndo_t898409293  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEUNDO_T898409293_H
#ifndef TRACKASSETUPGRADE_T4097356561_H
#define TRACKASSETUPGRADE_T4097356561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset/TrackAssetUpgrade
struct  TrackAssetUpgrade_t4097356561  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSETUPGRADE_T4097356561_H
#ifndef TRACKASSETEXTENSIONS_T812533753_H
#define TRACKASSETEXTENSIONS_T812533753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAssetExtensions
struct  TrackAssetExtensions_t812533753  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSETEXTENSIONS_T812533753_H
#ifndef WEIGHTUTILITY_T2276937658_H
#define WEIGHTUTILITY_T2276937658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.WeightUtility
struct  WeightUtility_t2276937658  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEIGHTUTILITY_T2276937658_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef PROPERTYNAME_T3749835189_H
#define PROPERTYNAME_T3749835189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t3749835189 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t3749835189, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T3749835189_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef DIRECTORCONTROLPLAYABLE_T2642348650_H
#define DIRECTORCONTROLPLAYABLE_T2642348650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DirectorControlPlayable
struct  DirectorControlPlayable_t2642348650  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.Playables.PlayableDirector UnityEngine.Timeline.DirectorControlPlayable::director
	PlayableDirector_t508516997 * ___director_0;
	// System.Boolean UnityEngine.Timeline.DirectorControlPlayable::m_SyncTime
	bool ___m_SyncTime_1;
	// System.Double UnityEngine.Timeline.DirectorControlPlayable::m_AssetDuration
	double ___m_AssetDuration_2;

public:
	inline static int32_t get_offset_of_director_0() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t2642348650, ___director_0)); }
	inline PlayableDirector_t508516997 * get_director_0() const { return ___director_0; }
	inline PlayableDirector_t508516997 ** get_address_of_director_0() { return &___director_0; }
	inline void set_director_0(PlayableDirector_t508516997 * value)
	{
		___director_0 = value;
		Il2CppCodeGenWriteBarrier((&___director_0), value);
	}

	inline static int32_t get_offset_of_m_SyncTime_1() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t2642348650, ___m_SyncTime_1)); }
	inline bool get_m_SyncTime_1() const { return ___m_SyncTime_1; }
	inline bool* get_address_of_m_SyncTime_1() { return &___m_SyncTime_1; }
	inline void set_m_SyncTime_1(bool value)
	{
		___m_SyncTime_1 = value;
	}

	inline static int32_t get_offset_of_m_AssetDuration_2() { return static_cast<int32_t>(offsetof(DirectorControlPlayable_t2642348650, ___m_AssetDuration_2)); }
	inline double get_m_AssetDuration_2() const { return ___m_AssetDuration_2; }
	inline double* get_address_of_m_AssetDuration_2() { return &___m_AssetDuration_2; }
	inline void set_m_AssetDuration_2(double value)
	{
		___m_AssetDuration_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTORCONTROLPLAYABLE_T2642348650_H
#ifndef DISCRETETIME_T924799574_H
#define DISCRETETIME_T924799574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.DiscreteTime
struct  DiscreteTime_t924799574 
{
public:
	// System.Int64 UnityEngine.Timeline.DiscreteTime::m_DiscreteTime
	int64_t ___m_DiscreteTime_2;

public:
	inline static int32_t get_offset_of_m_DiscreteTime_2() { return static_cast<int32_t>(offsetof(DiscreteTime_t924799574, ___m_DiscreteTime_2)); }
	inline int64_t get_m_DiscreteTime_2() const { return ___m_DiscreteTime_2; }
	inline int64_t* get_address_of_m_DiscreteTime_2() { return &___m_DiscreteTime_2; }
	inline void set_m_DiscreteTime_2(int64_t value)
	{
		___m_DiscreteTime_2 = value;
	}
};

struct DiscreteTime_t924799574_StaticFields
{
public:
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.DiscreteTime::kMaxTime
	DiscreteTime_t924799574  ___kMaxTime_1;

public:
	inline static int32_t get_offset_of_kMaxTime_1() { return static_cast<int32_t>(offsetof(DiscreteTime_t924799574_StaticFields, ___kMaxTime_1)); }
	inline DiscreteTime_t924799574  get_kMaxTime_1() const { return ___kMaxTime_1; }
	inline DiscreteTime_t924799574 * get_address_of_kMaxTime_1() { return &___kMaxTime_1; }
	inline void set_kMaxTime_1(DiscreteTime_t924799574  value)
	{
		___kMaxTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISCRETETIME_T924799574_H
#ifndef EVENTPLAYABLE_T1880911929_H
#define EVENTPLAYABLE_T1880911929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.EventPlayable
struct  EventPlayable_t1880911929  : public PlayableBehaviour_t4203540982
{
public:
	// System.Double UnityEngine.Timeline.EventPlayable::<triggerTime>k__BackingField
	double ___U3CtriggerTimeU3Ek__BackingField_0;
	// System.Boolean UnityEngine.Timeline.EventPlayable::m_HasFired
	bool ___m_HasFired_1;
	// System.Double UnityEngine.Timeline.EventPlayable::m_PreviousTime
	double ___m_PreviousTime_2;

public:
	inline static int32_t get_offset_of_U3CtriggerTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventPlayable_t1880911929, ___U3CtriggerTimeU3Ek__BackingField_0)); }
	inline double get_U3CtriggerTimeU3Ek__BackingField_0() const { return ___U3CtriggerTimeU3Ek__BackingField_0; }
	inline double* get_address_of_U3CtriggerTimeU3Ek__BackingField_0() { return &___U3CtriggerTimeU3Ek__BackingField_0; }
	inline void set_U3CtriggerTimeU3Ek__BackingField_0(double value)
	{
		___U3CtriggerTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_HasFired_1() { return static_cast<int32_t>(offsetof(EventPlayable_t1880911929, ___m_HasFired_1)); }
	inline bool get_m_HasFired_1() const { return ___m_HasFired_1; }
	inline bool* get_address_of_m_HasFired_1() { return &___m_HasFired_1; }
	inline void set_m_HasFired_1(bool value)
	{
		___m_HasFired_1 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTime_2() { return static_cast<int32_t>(offsetof(EventPlayable_t1880911929, ___m_PreviousTime_2)); }
	inline double get_m_PreviousTime_2() const { return ___m_PreviousTime_2; }
	inline double* get_address_of_m_PreviousTime_2() { return &___m_PreviousTime_2; }
	inline void set_m_PreviousTime_2(double value)
	{
		___m_PreviousTime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTPLAYABLE_T1880911929_H
#ifndef PARTICLECONTROLPLAYABLE_T3220249377_H
#define PARTICLECONTROLPLAYABLE_T3220249377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ParticleControlPlayable
struct  ParticleControlPlayable_t3220249377  : public PlayableBehaviour_t4203540982
{
public:
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_LastTime
	float ___m_LastTime_1;
	// System.UInt32 UnityEngine.Timeline.ParticleControlPlayable::m_RandomSeed
	uint32_t ___m_RandomSeed_2;
	// System.Single UnityEngine.Timeline.ParticleControlPlayable::m_SystemTime
	float ___m_SystemTime_3;
	// UnityEngine.ParticleSystem UnityEngine.Timeline.ParticleControlPlayable::<particleSystem>k__BackingField
	ParticleSystem_t1800779281 * ___U3CparticleSystemU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_LastTime_1() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___m_LastTime_1)); }
	inline float get_m_LastTime_1() const { return ___m_LastTime_1; }
	inline float* get_address_of_m_LastTime_1() { return &___m_LastTime_1; }
	inline void set_m_LastTime_1(float value)
	{
		___m_LastTime_1 = value;
	}

	inline static int32_t get_offset_of_m_RandomSeed_2() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___m_RandomSeed_2)); }
	inline uint32_t get_m_RandomSeed_2() const { return ___m_RandomSeed_2; }
	inline uint32_t* get_address_of_m_RandomSeed_2() { return &___m_RandomSeed_2; }
	inline void set_m_RandomSeed_2(uint32_t value)
	{
		___m_RandomSeed_2 = value;
	}

	inline static int32_t get_offset_of_m_SystemTime_3() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___m_SystemTime_3)); }
	inline float get_m_SystemTime_3() const { return ___m_SystemTime_3; }
	inline float* get_address_of_m_SystemTime_3() { return &___m_SystemTime_3; }
	inline void set_m_SystemTime_3(float value)
	{
		___m_SystemTime_3 = value;
	}

	inline static int32_t get_offset_of_U3CparticleSystemU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ParticleControlPlayable_t3220249377, ___U3CparticleSystemU3Ek__BackingField_4)); }
	inline ParticleSystem_t1800779281 * get_U3CparticleSystemU3Ek__BackingField_4() const { return ___U3CparticleSystemU3Ek__BackingField_4; }
	inline ParticleSystem_t1800779281 ** get_address_of_U3CparticleSystemU3Ek__BackingField_4() { return &___U3CparticleSystemU3Ek__BackingField_4; }
	inline void set_U3CparticleSystemU3Ek__BackingField_4(ParticleSystem_t1800779281 * value)
	{
		___U3CparticleSystemU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparticleSystemU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECONTROLPLAYABLE_T3220249377_H
#ifndef PREFABCONTROLPLAYABLE_T3058657425_H
#define PREFABCONTROLPLAYABLE_T3058657425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PrefabControlPlayable
struct  PrefabControlPlayable_t3058657425  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.PrefabControlPlayable::m_Instance
	GameObject_t1113636619 * ___m_Instance_0;

public:
	inline static int32_t get_offset_of_m_Instance_0() { return static_cast<int32_t>(offsetof(PrefabControlPlayable_t3058657425, ___m_Instance_0)); }
	inline GameObject_t1113636619 * get_m_Instance_0() const { return ___m_Instance_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Instance_0() { return &___m_Instance_0; }
	inline void set_m_Instance_0(GameObject_t1113636619 * value)
	{
		___m_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABCONTROLPLAYABLE_T3058657425_H
#ifndef RUNTIMECLIPBASE_T197358283_H
#define RUNTIMECLIPBASE_T197358283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClipBase
struct  RuntimeClipBase_t197358283  : public RuntimeElement_t2068542217
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIPBASE_T197358283_H
#ifndef TIMECONTROLPLAYABLE_T1010398267_H
#define TIMECONTROLPLAYABLE_T1010398267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimeControlPlayable
struct  TimeControlPlayable_t1010398267  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.Timeline.ITimeControl UnityEngine.Timeline.TimeControlPlayable::m_timeControl
	RuntimeObject* ___m_timeControl_0;
	// System.Boolean UnityEngine.Timeline.TimeControlPlayable::m_started
	bool ___m_started_1;

public:
	inline static int32_t get_offset_of_m_timeControl_0() { return static_cast<int32_t>(offsetof(TimeControlPlayable_t1010398267, ___m_timeControl_0)); }
	inline RuntimeObject* get_m_timeControl_0() const { return ___m_timeControl_0; }
	inline RuntimeObject** get_address_of_m_timeControl_0() { return &___m_timeControl_0; }
	inline void set_m_timeControl_0(RuntimeObject* value)
	{
		___m_timeControl_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_timeControl_0), value);
	}

	inline static int32_t get_offset_of_m_started_1() { return static_cast<int32_t>(offsetof(TimeControlPlayable_t1010398267, ___m_started_1)); }
	inline bool get_m_started_1() const { return ___m_started_1; }
	inline bool* get_address_of_m_started_1() { return &___m_started_1; }
	inline void set_m_started_1(bool value)
	{
		___m_started_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECONTROLPLAYABLE_T1010398267_H
#ifndef TIMELINEPLAYABLE_T2938744123_H
#define TIMELINEPLAYABLE_T2938744123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelinePlayable
struct  TimelinePlayable_t2938744123  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.IntervalTree`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_IntervalTree
	IntervalTree_1_t478208184 * ___m_IntervalTree_0;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_ActiveClips
	List_1_t3540616959 * ___m_ActiveClips_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.RuntimeElement> UnityEngine.Timeline.TimelinePlayable::m_CurrentListOfActiveClips
	List_1_t3540616959 * ___m_CurrentListOfActiveClips_2;
	// System.Int32 UnityEngine.Timeline.TimelinePlayable::m_ActiveBit
	int32_t ___m_ActiveBit_3;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.ITimelineEvaluateCallback> UnityEngine.Timeline.TimelinePlayable::m_EvaluateCallbacks
	List_1_t2313598679 * ___m_EvaluateCallbacks_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Timeline.TrackAsset,UnityEngine.Playables.Playable> UnityEngine.Timeline.TimelinePlayable::m_PlayableCache
	Dictionary_2_t3310777870 * ___m_PlayableCache_5;

public:
	inline static int32_t get_offset_of_m_IntervalTree_0() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_IntervalTree_0)); }
	inline IntervalTree_1_t478208184 * get_m_IntervalTree_0() const { return ___m_IntervalTree_0; }
	inline IntervalTree_1_t478208184 ** get_address_of_m_IntervalTree_0() { return &___m_IntervalTree_0; }
	inline void set_m_IntervalTree_0(IntervalTree_1_t478208184 * value)
	{
		___m_IntervalTree_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_IntervalTree_0), value);
	}

	inline static int32_t get_offset_of_m_ActiveClips_1() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_ActiveClips_1)); }
	inline List_1_t3540616959 * get_m_ActiveClips_1() const { return ___m_ActiveClips_1; }
	inline List_1_t3540616959 ** get_address_of_m_ActiveClips_1() { return &___m_ActiveClips_1; }
	inline void set_m_ActiveClips_1(List_1_t3540616959 * value)
	{
		___m_ActiveClips_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveClips_1), value);
	}

	inline static int32_t get_offset_of_m_CurrentListOfActiveClips_2() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_CurrentListOfActiveClips_2)); }
	inline List_1_t3540616959 * get_m_CurrentListOfActiveClips_2() const { return ___m_CurrentListOfActiveClips_2; }
	inline List_1_t3540616959 ** get_address_of_m_CurrentListOfActiveClips_2() { return &___m_CurrentListOfActiveClips_2; }
	inline void set_m_CurrentListOfActiveClips_2(List_1_t3540616959 * value)
	{
		___m_CurrentListOfActiveClips_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentListOfActiveClips_2), value);
	}

	inline static int32_t get_offset_of_m_ActiveBit_3() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_ActiveBit_3)); }
	inline int32_t get_m_ActiveBit_3() const { return ___m_ActiveBit_3; }
	inline int32_t* get_address_of_m_ActiveBit_3() { return &___m_ActiveBit_3; }
	inline void set_m_ActiveBit_3(int32_t value)
	{
		___m_ActiveBit_3 = value;
	}

	inline static int32_t get_offset_of_m_EvaluateCallbacks_4() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_EvaluateCallbacks_4)); }
	inline List_1_t2313598679 * get_m_EvaluateCallbacks_4() const { return ___m_EvaluateCallbacks_4; }
	inline List_1_t2313598679 ** get_address_of_m_EvaluateCallbacks_4() { return &___m_EvaluateCallbacks_4; }
	inline void set_m_EvaluateCallbacks_4(List_1_t2313598679 * value)
	{
		___m_EvaluateCallbacks_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EvaluateCallbacks_4), value);
	}

	inline static int32_t get_offset_of_m_PlayableCache_5() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123, ___m_PlayableCache_5)); }
	inline Dictionary_2_t3310777870 * get_m_PlayableCache_5() const { return ___m_PlayableCache_5; }
	inline Dictionary_2_t3310777870 ** get_address_of_m_PlayableCache_5() { return &___m_PlayableCache_5; }
	inline void set_m_PlayableCache_5(Dictionary_2_t3310777870 * value)
	{
		___m_PlayableCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayableCache_5), value);
	}
};

struct TimelinePlayable_t2938744123_StaticFields
{
public:
	// System.Boolean UnityEngine.Timeline.TimelinePlayable::muteAudioScrubbing
	bool ___muteAudioScrubbing_6;

public:
	inline static int32_t get_offset_of_muteAudioScrubbing_6() { return static_cast<int32_t>(offsetof(TimelinePlayable_t2938744123_StaticFields, ___muteAudioScrubbing_6)); }
	inline bool get_muteAudioScrubbing_6() const { return ___muteAudioScrubbing_6; }
	inline bool* get_address_of_muteAudioScrubbing_6() { return &___muteAudioScrubbing_6; }
	inline void set_muteAudioScrubbing_6(bool value)
	{
		___muteAudioScrubbing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEPLAYABLE_T2938744123_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SAVEFOLDERTYPE_T939074619_H
#define SAVEFOLDERTYPE_T939074619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveFolderType
struct  SaveFolderType_t939074619 
{
public:
	// System.Int32 SaveFolderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SaveFolderType_t939074619, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEFOLDERTYPE_T939074619_H
#ifndef U3CDOJUMPMOVINGU3EC__ITERATOR0_T871786035_H
#define U3CDOJUMPMOVINGU3EC__ITERATOR0_T871786035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StageJumpAnimation/<DoJumpMoving>c__Iterator0
struct  U3CDoJumpMovingU3Ec__Iterator0_t871786035  : public RuntimeObject
{
public:
	// System.Single StageJumpAnimation/<DoJumpMoving>c__Iterator0::time
	float ___time_0;
	// System.Int32 StageJumpAnimation/<DoJumpMoving>c__Iterator0::<frames>__0
	int32_t ___U3CframesU3E__0_1;
	// System.Single StageJumpAnimation/<DoJumpMoving>c__Iterator0::<step>__0
	float ___U3CstepU3E__0_2;
	// System.Single StageJumpAnimation/<DoJumpMoving>c__Iterator0::<t>__0
	float ___U3CtU3E__0_3;
	// System.Int32 StageJumpAnimation/<DoJumpMoving>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_4;
	// UnityEngine.Vector3 StageJumpAnimation/<DoJumpMoving>c__Iterator0::fromPoint
	Vector3_t3722313464  ___fromPoint_5;
	// UnityEngine.Vector3 StageJumpAnimation/<DoJumpMoving>c__Iterator0::toPoint
	Vector3_t3722313464  ___toPoint_6;
	// UnityEngine.AnimationCurve StageJumpAnimation/<DoJumpMoving>c__Iterator0::curve
	AnimationCurve_t3046754366 * ___curve_7;
	// StageJumpAnimation StageJumpAnimation/<DoJumpMoving>c__Iterator0::$this
	StageJumpAnimation_t2635268956 * ___U24this_8;
	// System.Object StageJumpAnimation/<DoJumpMoving>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean StageJumpAnimation/<DoJumpMoving>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 StageJumpAnimation/<DoJumpMoving>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_U3CframesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U3CframesU3E__0_1)); }
	inline int32_t get_U3CframesU3E__0_1() const { return ___U3CframesU3E__0_1; }
	inline int32_t* get_address_of_U3CframesU3E__0_1() { return &___U3CframesU3E__0_1; }
	inline void set_U3CframesU3E__0_1(int32_t value)
	{
		___U3CframesU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U3CstepU3E__0_2)); }
	inline float get_U3CstepU3E__0_2() const { return ___U3CstepU3E__0_2; }
	inline float* get_address_of_U3CstepU3E__0_2() { return &___U3CstepU3E__0_2; }
	inline void set_U3CstepU3E__0_2(float value)
	{
		___U3CstepU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U3CtU3E__0_3)); }
	inline float get_U3CtU3E__0_3() const { return ___U3CtU3E__0_3; }
	inline float* get_address_of_U3CtU3E__0_3() { return &___U3CtU3E__0_3; }
	inline void set_U3CtU3E__0_3(float value)
	{
		___U3CtU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_4() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U3CiU3E__1_4)); }
	inline int32_t get_U3CiU3E__1_4() const { return ___U3CiU3E__1_4; }
	inline int32_t* get_address_of_U3CiU3E__1_4() { return &___U3CiU3E__1_4; }
	inline void set_U3CiU3E__1_4(int32_t value)
	{
		___U3CiU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_fromPoint_5() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___fromPoint_5)); }
	inline Vector3_t3722313464  get_fromPoint_5() const { return ___fromPoint_5; }
	inline Vector3_t3722313464 * get_address_of_fromPoint_5() { return &___fromPoint_5; }
	inline void set_fromPoint_5(Vector3_t3722313464  value)
	{
		___fromPoint_5 = value;
	}

	inline static int32_t get_offset_of_toPoint_6() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___toPoint_6)); }
	inline Vector3_t3722313464  get_toPoint_6() const { return ___toPoint_6; }
	inline Vector3_t3722313464 * get_address_of_toPoint_6() { return &___toPoint_6; }
	inline void set_toPoint_6(Vector3_t3722313464  value)
	{
		___toPoint_6 = value;
	}

	inline static int32_t get_offset_of_curve_7() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___curve_7)); }
	inline AnimationCurve_t3046754366 * get_curve_7() const { return ___curve_7; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_7() { return &___curve_7; }
	inline void set_curve_7(AnimationCurve_t3046754366 * value)
	{
		___curve_7 = value;
		Il2CppCodeGenWriteBarrier((&___curve_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U24this_8)); }
	inline StageJumpAnimation_t2635268956 * get_U24this_8() const { return ___U24this_8; }
	inline StageJumpAnimation_t2635268956 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(StageJumpAnimation_t2635268956 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDoJumpMovingU3Ec__Iterator0_t871786035, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOJUMPMOVINGU3EC__ITERATOR0_T871786035_H
#ifndef U3CGENERATEPHOTOGALLERYANIMATEDU3EC__ITERATOR0_T1817645962_H
#define U3CGENERATEPHOTOGALLERYANIMATEDU3EC__ITERATOR0_T1817645962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0
struct  U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<randomPos>__0
	Vector3_t3722313464  ___U3CrandomPosU3E__0_0;
	// System.Single UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<randomRotZ>__0
	float ___U3CrandomRotZU3E__0_1;
	// System.Single UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<t>__0
	float ___U3CtU3E__0_2;
	// System.Single UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<step>__0
	float ___U3CstepU3E__0_3;
	// System.Int32 UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_4;
	// UnityEngine.GameObject UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<tempObj>__2
	GameObject_t1113636619 * ___U3CtempObjU3E__2_5;
	// UnityEngine.RectTransform UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::<rectT>__2
	RectTransform_t3704657025 * ___U3CrectTU3E__2_6;
	// UIPhotoGalleryManager UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::$this
	UIPhotoGalleryManager_t2672000691 * ___U24this_7;
	// System.Object UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 UIPhotoGalleryManager/<GeneratePhotoGalleryAnimated>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CrandomPosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CrandomPosU3E__0_0)); }
	inline Vector3_t3722313464  get_U3CrandomPosU3E__0_0() const { return ___U3CrandomPosU3E__0_0; }
	inline Vector3_t3722313464 * get_address_of_U3CrandomPosU3E__0_0() { return &___U3CrandomPosU3E__0_0; }
	inline void set_U3CrandomPosU3E__0_0(Vector3_t3722313464  value)
	{
		___U3CrandomPosU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrandomRotZU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CrandomRotZU3E__0_1)); }
	inline float get_U3CrandomRotZU3E__0_1() const { return ___U3CrandomRotZU3E__0_1; }
	inline float* get_address_of_U3CrandomRotZU3E__0_1() { return &___U3CrandomRotZU3E__0_1; }
	inline void set_U3CrandomRotZU3E__0_1(float value)
	{
		___U3CrandomRotZU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CtU3E__0_2)); }
	inline float get_U3CtU3E__0_2() const { return ___U3CtU3E__0_2; }
	inline float* get_address_of_U3CtU3E__0_2() { return &___U3CtU3E__0_2; }
	inline void set_U3CtU3E__0_2(float value)
	{
		___U3CtU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E__0_3() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CstepU3E__0_3)); }
	inline float get_U3CstepU3E__0_3() const { return ___U3CstepU3E__0_3; }
	inline float* get_address_of_U3CstepU3E__0_3() { return &___U3CstepU3E__0_3; }
	inline void set_U3CstepU3E__0_3(float value)
	{
		___U3CstepU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_4() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CiU3E__1_4)); }
	inline int32_t get_U3CiU3E__1_4() const { return ___U3CiU3E__1_4; }
	inline int32_t* get_address_of_U3CiU3E__1_4() { return &___U3CiU3E__1_4; }
	inline void set_U3CiU3E__1_4(int32_t value)
	{
		___U3CiU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CtempObjU3E__2_5() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CtempObjU3E__2_5)); }
	inline GameObject_t1113636619 * get_U3CtempObjU3E__2_5() const { return ___U3CtempObjU3E__2_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CtempObjU3E__2_5() { return &___U3CtempObjU3E__2_5; }
	inline void set_U3CtempObjU3E__2_5(GameObject_t1113636619 * value)
	{
		___U3CtempObjU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempObjU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3CrectTU3E__2_6() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U3CrectTU3E__2_6)); }
	inline RectTransform_t3704657025 * get_U3CrectTU3E__2_6() const { return ___U3CrectTU3E__2_6; }
	inline RectTransform_t3704657025 ** get_address_of_U3CrectTU3E__2_6() { return &___U3CrectTU3E__2_6; }
	inline void set_U3CrectTU3E__2_6(RectTransform_t3704657025 * value)
	{
		___U3CrectTU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrectTU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U24this_7)); }
	inline UIPhotoGalleryManager_t2672000691 * get_U24this_7() const { return ___U24this_7; }
	inline UIPhotoGalleryManager_t2672000691 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(UIPhotoGalleryManager_t2672000691 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEPHOTOGALLERYANIMATEDU3EC__ITERATOR0_T1817645962_H
#ifndef EXPOSEDREFERENCE_1_T1378690540_H
#define EXPOSEDREFERENCE_1_T1378690540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ExposedReference`1<UnityEngine.GameObject>
struct  ExposedReference_1_t1378690540 
{
public:
	// UnityEngine.PropertyName UnityEngine.ExposedReference`1::exposedName
	PropertyName_t3749835189  ___exposedName_0;
	// UnityEngine.Object UnityEngine.ExposedReference`1::defaultValue
	Object_t631007953 * ___defaultValue_1;

public:
	inline static int32_t get_offset_of_exposedName_0() { return static_cast<int32_t>(offsetof(ExposedReference_1_t1378690540, ___exposedName_0)); }
	inline PropertyName_t3749835189  get_exposedName_0() const { return ___exposedName_0; }
	inline PropertyName_t3749835189 * get_address_of_exposedName_0() { return &___exposedName_0; }
	inline void set_exposedName_0(PropertyName_t3749835189  value)
	{
		___exposedName_0 = value;
	}

	inline static int32_t get_offset_of_defaultValue_1() { return static_cast<int32_t>(offsetof(ExposedReference_1_t1378690540, ___defaultValue_1)); }
	inline Object_t631007953 * get_defaultValue_1() const { return ___defaultValue_1; }
	inline Object_t631007953 ** get_address_of_defaultValue_1() { return &___defaultValue_1; }
	inline void set_defaultValue_1(Object_t631007953 * value)
	{
		___defaultValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSEDREFERENCE_1_T1378690540_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PLAYABLEHANDLE_T1095853803_H
#define PLAYABLEHANDLE_T1095853803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1095853803 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1095853803_H
#ifndef PLAYABLEOUTPUTHANDLE_T4208053793_H
#define PLAYABLEOUTPUTHANDLE_T4208053793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t4208053793 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t4208053793, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t4208053793, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T4208053793_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef INITIALSTATE_T1844636896_H
#define INITIALSTATE_T1844636896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable/InitialState
struct  InitialState_t1844636896 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationControlPlayable/InitialState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitialState_t1844636896, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALSTATE_T1844636896_H
#ifndef POSTPLAYBACKSTATE_T1986969933_H
#define POSTPLAYBACKSTATE_T1986969933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState
struct  PostPlaybackState_t1986969933 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PostPlaybackState_t1986969933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T1986969933_H
#ifndef POSTPLAYBACKSTATE_T4203018085_H
#define POSTPLAYBACKSTATE_T4203018085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack/PostPlaybackState
struct  PostPlaybackState_t4203018085 
{
public:
	// System.Int32 UnityEngine.Timeline.ActivationTrack/PostPlaybackState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PostPlaybackState_t4203018085, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPLAYBACKSTATE_T4203018085_H
#ifndef VERSIONS_T439386478_H
#define VERSIONS_T439386478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/Versions
struct  Versions_t439386478 
{
public:
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/Versions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Versions_t439386478, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONS_T439386478_H
#ifndef MATCHTARGETFIELDS_T3959953937_H
#define MATCHTARGETFIELDS_T3959953937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFields
struct  MatchTargetFields_t3959953937 
{
public:
	// System.Int32 UnityEngine.Timeline.MatchTargetFields::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MatchTargetFields_t3959953937, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDS_T3959953937_H
#ifndef CLIPEXTRAPOLATION_T324732067_H
#define CLIPEXTRAPOLATION_T324732067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TimelineClip/ClipExtrapolation
struct  ClipExtrapolation_t324732067 
{
public:
	// System.Int32 UnityEngine.Timeline.TimelineClip/ClipExtrapolation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClipExtrapolation_t324732067, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPEXTRAPOLATION_T324732067_H
#ifndef VERSIONS_T3704484929_H
#define VERSIONS_T3704484929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset/Versions
struct  Versions_t3704484929 
{
public:
	// System.Int32 UnityEngine.Timeline.TrackAsset/Versions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Versions_t3704484929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONS_T3704484929_H
#ifndef TRACKCOLORATTRIBUTE_T2898993029_H
#define TRACKCOLORATTRIBUTE_T2898993029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackColorAttribute
struct  TrackColorAttribute_t2898993029  : public Attribute_t861562559
{
public:
	// UnityEngine.Color UnityEngine.Timeline.TrackColorAttribute::m_Color
	Color_t2555686324  ___m_Color_0;

public:
	inline static int32_t get_offset_of_m_Color_0() { return static_cast<int32_t>(offsetof(TrackColorAttribute_t2898993029, ___m_Color_0)); }
	inline Color_t2555686324  get_m_Color_0() const { return ___m_Color_0; }
	inline Color_t2555686324 * get_address_of_m_Color_0() { return &___m_Color_0; }
	inline void set_m_Color_0(Color_t2555686324  value)
	{
		___m_Color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKCOLORATTRIBUTE_T2898993029_H
#ifndef USERSEX_T1993248581_H
#define USERSEX_T1993248581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UserSex
struct  UserSex_t1993248581 
{
public:
	// System.Int32 UserSex::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UserSex_t1993248581, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERSEX_T1993248581_H
#ifndef ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#define ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationLayerMixerPlayable
struct  AnimationLayerMixerPlayable_t3631223897 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationLayerMixerPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t3631223897, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationLayerMixerPlayable_t3631223897_StaticFields
{
public:
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Animations.AnimationLayerMixerPlayable::m_NullPlayable
	AnimationLayerMixerPlayable_t3631223897  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationLayerMixerPlayable_t3631223897_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationLayerMixerPlayable_t3631223897  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationLayerMixerPlayable_t3631223897 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationLayerMixerPlayable_t3631223897  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONLAYERMIXERPLAYABLE_T3631223897_H
#ifndef ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#define ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_t1918618239 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t4208053793  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_t1918618239, ___m_Handle_0)); }
	inline PlayableOutputHandle_t4208053793  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t4208053793 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t4208053793  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEOUTPUT_T1918618239_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef PLAYABLE_T459825607_H
#define PLAYABLE_T459825607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.Playable
struct  Playable_t459825607 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Playable_t459825607, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

struct Playable_t459825607_StaticFields
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Playables.Playable::m_NullPlayable
	Playable_t459825607  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(Playable_t459825607_StaticFields, ___m_NullPlayable_1)); }
	inline Playable_t459825607  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline Playable_t459825607 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(Playable_t459825607  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLE_T459825607_H
#ifndef PLAYABLEBINDING_T354260709_H
#define PLAYABLEBINDING_T354260709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableBinding
struct  PlayableBinding_t354260709 
{
public:
	// System.String UnityEngine.Playables.PlayableBinding::m_StreamName
	String_t* ___m_StreamName_0;
	// UnityEngine.Object UnityEngine.Playables.PlayableBinding::m_SourceObject
	Object_t631007953 * ___m_SourceObject_1;
	// System.Type UnityEngine.Playables.PlayableBinding::m_SourceBindingType
	Type_t * ___m_SourceBindingType_2;
	// UnityEngine.Playables.PlayableBinding/CreateOutputMethod UnityEngine.Playables.PlayableBinding::m_CreateOutputMethod
	CreateOutputMethod_t2301811773 * ___m_CreateOutputMethod_3;

public:
	inline static int32_t get_offset_of_m_StreamName_0() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_StreamName_0)); }
	inline String_t* get_m_StreamName_0() const { return ___m_StreamName_0; }
	inline String_t** get_address_of_m_StreamName_0() { return &___m_StreamName_0; }
	inline void set_m_StreamName_0(String_t* value)
	{
		___m_StreamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_StreamName_0), value);
	}

	inline static int32_t get_offset_of_m_SourceObject_1() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_SourceObject_1)); }
	inline Object_t631007953 * get_m_SourceObject_1() const { return ___m_SourceObject_1; }
	inline Object_t631007953 ** get_address_of_m_SourceObject_1() { return &___m_SourceObject_1; }
	inline void set_m_SourceObject_1(Object_t631007953 * value)
	{
		___m_SourceObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceObject_1), value);
	}

	inline static int32_t get_offset_of_m_SourceBindingType_2() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_SourceBindingType_2)); }
	inline Type_t * get_m_SourceBindingType_2() const { return ___m_SourceBindingType_2; }
	inline Type_t ** get_address_of_m_SourceBindingType_2() { return &___m_SourceBindingType_2; }
	inline void set_m_SourceBindingType_2(Type_t * value)
	{
		___m_SourceBindingType_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceBindingType_2), value);
	}

	inline static int32_t get_offset_of_m_CreateOutputMethod_3() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709, ___m_CreateOutputMethod_3)); }
	inline CreateOutputMethod_t2301811773 * get_m_CreateOutputMethod_3() const { return ___m_CreateOutputMethod_3; }
	inline CreateOutputMethod_t2301811773 ** get_address_of_m_CreateOutputMethod_3() { return &___m_CreateOutputMethod_3; }
	inline void set_m_CreateOutputMethod_3(CreateOutputMethod_t2301811773 * value)
	{
		___m_CreateOutputMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CreateOutputMethod_3), value);
	}
};

struct PlayableBinding_t354260709_StaticFields
{
public:
	// UnityEngine.Playables.PlayableBinding[] UnityEngine.Playables.PlayableBinding::None
	PlayableBindingU5BU5D_t829358056* ___None_4;
	// System.Double UnityEngine.Playables.PlayableBinding::DefaultDuration
	double ___DefaultDuration_5;

public:
	inline static int32_t get_offset_of_None_4() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709_StaticFields, ___None_4)); }
	inline PlayableBindingU5BU5D_t829358056* get_None_4() const { return ___None_4; }
	inline PlayableBindingU5BU5D_t829358056** get_address_of_None_4() { return &___None_4; }
	inline void set_None_4(PlayableBindingU5BU5D_t829358056* value)
	{
		___None_4 = value;
		Il2CppCodeGenWriteBarrier((&___None_4), value);
	}

	inline static int32_t get_offset_of_DefaultDuration_5() { return static_cast<int32_t>(offsetof(PlayableBinding_t354260709_StaticFields, ___DefaultDuration_5)); }
	inline double get_DefaultDuration_5() const { return ___DefaultDuration_5; }
	inline double* get_address_of_DefaultDuration_5() { return &___DefaultDuration_5; }
	inline void set_DefaultDuration_5(double value)
	{
		___DefaultDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t354260709_marshaled_pinvoke
{
	char* ___m_StreamName_0;
	Object_t631007953_marshaled_pinvoke ___m_SourceObject_1;
	Type_t * ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
// Native definition for COM marshalling of UnityEngine.Playables.PlayableBinding
struct PlayableBinding_t354260709_marshaled_com
{
	Il2CppChar* ___m_StreamName_0;
	Object_t631007953_marshaled_com* ___m_SourceObject_1;
	Type_t * ___m_SourceBindingType_2;
	Il2CppMethodPointer ___m_CreateOutputMethod_3;
};
#endif // PLAYABLEBINDING_T354260709_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ACTIVATIONCONTROLPLAYABLE_T426148305_H
#define ACTIVATIONCONTROLPLAYABLE_T426148305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationControlPlayable
struct  ActivationControlPlayable_t426148305  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationControlPlayable::gameObject
	GameObject_t1113636619 * ___gameObject_0;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ActivationControlPlayable::postPlayback
	int32_t ___postPlayback_1;
	// UnityEngine.Timeline.ActivationControlPlayable/InitialState UnityEngine.Timeline.ActivationControlPlayable::m_InitialState
	int32_t ___m_InitialState_2;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t426148305, ___gameObject_0)); }
	inline GameObject_t1113636619 * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_t1113636619 * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_postPlayback_1() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t426148305, ___postPlayback_1)); }
	inline int32_t get_postPlayback_1() const { return ___postPlayback_1; }
	inline int32_t* get_address_of_postPlayback_1() { return &___postPlayback_1; }
	inline void set_postPlayback_1(int32_t value)
	{
		___postPlayback_1 = value;
	}

	inline static int32_t get_offset_of_m_InitialState_2() { return static_cast<int32_t>(offsetof(ActivationControlPlayable_t426148305, ___m_InitialState_2)); }
	inline int32_t get_m_InitialState_2() const { return ___m_InitialState_2; }
	inline int32_t* get_address_of_m_InitialState_2() { return &___m_InitialState_2; }
	inline void set_m_InitialState_2(int32_t value)
	{
		___m_InitialState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONCONTROLPLAYABLE_T426148305_H
#ifndef ACTIVATIONMIXERPLAYABLE_T2212523045_H
#define ACTIVATIONMIXERPLAYABLE_T2212523045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationMixerPlayable
struct  ActivationMixerPlayable_t2212523045  : public PlayableBehaviour_t4203540982
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationMixerPlayable::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_0;
	// System.Boolean UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObjectInitialStateIsActive
	bool ___m_BoundGameObjectInitialStateIsActive_1;
	// UnityEngine.GameObject UnityEngine.Timeline.ActivationMixerPlayable::m_BoundGameObject
	GameObject_t1113636619 * ___m_BoundGameObject_2;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_0() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t2212523045, ___m_PostPlaybackState_0)); }
	inline int32_t get_m_PostPlaybackState_0() const { return ___m_PostPlaybackState_0; }
	inline int32_t* get_address_of_m_PostPlaybackState_0() { return &___m_PostPlaybackState_0; }
	inline void set_m_PostPlaybackState_0(int32_t value)
	{
		___m_PostPlaybackState_0 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObjectInitialStateIsActive_1() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t2212523045, ___m_BoundGameObjectInitialStateIsActive_1)); }
	inline bool get_m_BoundGameObjectInitialStateIsActive_1() const { return ___m_BoundGameObjectInitialStateIsActive_1; }
	inline bool* get_address_of_m_BoundGameObjectInitialStateIsActive_1() { return &___m_BoundGameObjectInitialStateIsActive_1; }
	inline void set_m_BoundGameObjectInitialStateIsActive_1(bool value)
	{
		___m_BoundGameObjectInitialStateIsActive_1 = value;
	}

	inline static int32_t get_offset_of_m_BoundGameObject_2() { return static_cast<int32_t>(offsetof(ActivationMixerPlayable_t2212523045, ___m_BoundGameObject_2)); }
	inline GameObject_t1113636619 * get_m_BoundGameObject_2() const { return ___m_BoundGameObject_2; }
	inline GameObject_t1113636619 ** get_address_of_m_BoundGameObject_2() { return &___m_BoundGameObject_2; }
	inline void set_m_BoundGameObject_2(GameObject_t1113636619 * value)
	{
		___m_BoundGameObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoundGameObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONMIXERPLAYABLE_T2212523045_H
#ifndef MATCHTARGETFIELDCONSTANTS_T2682568617_H
#define MATCHTARGETFIELDCONSTANTS_T2682568617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.MatchTargetFieldConstants
struct  MatchTargetFieldConstants_t2682568617  : public RuntimeObject
{
public:

public:
};

struct MatchTargetFieldConstants_t2682568617_StaticFields
{
public:
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::All
	int32_t ___All_0;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::None
	int32_t ___None_1;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Position
	int32_t ___Position_2;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.MatchTargetFieldConstants::Rotation
	int32_t ___Rotation_3;

public:
	inline static int32_t get_offset_of_All_0() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___All_0)); }
	inline int32_t get_All_0() const { return ___All_0; }
	inline int32_t* get_address_of_All_0() { return &___All_0; }
	inline void set_All_0(int32_t value)
	{
		___All_0 = value;
	}

	inline static int32_t get_offset_of_None_1() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___None_1)); }
	inline int32_t get_None_1() const { return ___None_1; }
	inline int32_t* get_address_of_None_1() { return &___None_1; }
	inline void set_None_1(int32_t value)
	{
		___None_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(MatchTargetFieldConstants_t2682568617_StaticFields, ___Rotation_3)); }
	inline int32_t get_Rotation_3() const { return ___Rotation_3; }
	inline int32_t* get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(int32_t value)
	{
		___Rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHTARGETFIELDCONSTANTS_T2682568617_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef PLAYABLEASSET_T3219022681_H
#define PLAYABLEASSET_T3219022681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableAsset
struct  PlayableAsset_t3219022681  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEASSET_T3219022681_H
#ifndef ANIMATIONOUTPUTWEIGHTPROCESSOR_T2670190175_H
#define ANIMATIONOUTPUTWEIGHTPROCESSOR_T2670190175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor
struct  AnimationOutputWeightProcessor_t2670190175  : public RuntimeObject
{
public:
	// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Output
	AnimationPlayableOutput_t1918618239  ___m_Output_0;
	// UnityEngine.Animations.AnimationLayerMixerPlayable UnityEngine.Timeline.AnimationOutputWeightProcessor::m_LayerMixer
	AnimationLayerMixerPlayable_t3631223897  ___m_LayerMixer_1;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo> UnityEngine.Timeline.AnimationOutputWeightProcessor::m_Mixers
	List_1_t3454752663 * ___m_Mixers_2;

public:
	inline static int32_t get_offset_of_m_Output_0() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t2670190175, ___m_Output_0)); }
	inline AnimationPlayableOutput_t1918618239  get_m_Output_0() const { return ___m_Output_0; }
	inline AnimationPlayableOutput_t1918618239 * get_address_of_m_Output_0() { return &___m_Output_0; }
	inline void set_m_Output_0(AnimationPlayableOutput_t1918618239  value)
	{
		___m_Output_0 = value;
	}

	inline static int32_t get_offset_of_m_LayerMixer_1() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t2670190175, ___m_LayerMixer_1)); }
	inline AnimationLayerMixerPlayable_t3631223897  get_m_LayerMixer_1() const { return ___m_LayerMixer_1; }
	inline AnimationLayerMixerPlayable_t3631223897 * get_address_of_m_LayerMixer_1() { return &___m_LayerMixer_1; }
	inline void set_m_LayerMixer_1(AnimationLayerMixerPlayable_t3631223897  value)
	{
		___m_LayerMixer_1 = value;
	}

	inline static int32_t get_offset_of_m_Mixers_2() { return static_cast<int32_t>(offsetof(AnimationOutputWeightProcessor_t2670190175, ___m_Mixers_2)); }
	inline List_1_t3454752663 * get_m_Mixers_2() const { return ___m_Mixers_2; }
	inline List_1_t3454752663 ** get_address_of_m_Mixers_2() { return &___m_Mixers_2; }
	inline void set_m_Mixers_2(List_1_t3454752663 * value)
	{
		___m_Mixers_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mixers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONOUTPUTWEIGHTPROCESSOR_T2670190175_H
#ifndef WEIGHTINFO_T1982677921_H
#define WEIGHTINFO_T1982677921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct  WeightInfo_t1982677921 
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::mixer
	Playable_t459825607  ___mixer_0;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::parentMixer
	Playable_t459825607  ___parentMixer_1;
	// System.Int32 UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::port
	int32_t ___port_2;
	// System.Boolean UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo::modulate
	bool ___modulate_3;

public:
	inline static int32_t get_offset_of_mixer_0() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___mixer_0)); }
	inline Playable_t459825607  get_mixer_0() const { return ___mixer_0; }
	inline Playable_t459825607 * get_address_of_mixer_0() { return &___mixer_0; }
	inline void set_mixer_0(Playable_t459825607  value)
	{
		___mixer_0 = value;
	}

	inline static int32_t get_offset_of_parentMixer_1() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___parentMixer_1)); }
	inline Playable_t459825607  get_parentMixer_1() const { return ___parentMixer_1; }
	inline Playable_t459825607 * get_address_of_parentMixer_1() { return &___parentMixer_1; }
	inline void set_parentMixer_1(Playable_t459825607  value)
	{
		___parentMixer_1 = value;
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_modulate_3() { return static_cast<int32_t>(offsetof(WeightInfo_t1982677921, ___modulate_3)); }
	inline bool get_modulate_3() const { return ___modulate_3; }
	inline bool* get_address_of_modulate_3() { return &___modulate_3; }
	inline void set_modulate_3(bool value)
	{
		___modulate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t1982677921_marshaled_pinvoke
{
	Playable_t459825607  ___mixer_0;
	Playable_t459825607  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
// Native definition for COM marshalling of UnityEngine.Timeline.AnimationOutputWeightProcessor/WeightInfo
struct WeightInfo_t1982677921_marshaled_com
{
	Playable_t459825607  ___mixer_0;
	Playable_t459825607  ___parentMixer_1;
	int32_t ___port_2;
	int32_t ___modulate_3;
};
#endif // WEIGHTINFO_T1982677921_H
#ifndef U3CU3EC__ITERATOR0_T4165699274_H
#define U3CU3EC__ITERATOR0_T4165699274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t4165699274  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AnimationPlayableAsset UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$this
	AnimationPlayableAsset_t734882934 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24this_0)); }
	inline AnimationPlayableAsset_t734882934 * get_U24this_0() const { return ___U24this_0; }
	inline AnimationPlayableAsset_t734882934 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimationPlayableAsset_t734882934 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24current_1)); }
	inline PlayableBinding_t354260709  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t354260709  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t4165699274, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T4165699274_H
#ifndef U3CU3EC__ITERATOR0_T1104488729_H
#define U3CU3EC__ITERATOR0_T1104488729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1104488729  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AnimationTrack UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$this
	AnimationTrack_t3872729528 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AnimationTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24this_0)); }
	inline AnimationTrack_t3872729528 * get_U24this_0() const { return ___U24this_0; }
	inline AnimationTrack_t3872729528 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnimationTrack_t3872729528 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24current_1)); }
	inline PlayableBinding_t354260709  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t354260709  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1104488729, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1104488729_H
#ifndef U3CU3EC__ITERATOR0_T2611944586_H
#define U3CU3EC__ITERATOR0_T2611944586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2611944586  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AudioPlayableAsset UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$this
	AudioPlayableAsset_t2575444864 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AudioPlayableAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24this_0)); }
	inline AudioPlayableAsset_t2575444864 * get_U24this_0() const { return ___U24this_0; }
	inline AudioPlayableAsset_t2575444864 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioPlayableAsset_t2575444864 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24current_1)); }
	inline PlayableBinding_t354260709  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t354260709  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2611944586, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2611944586_H
#ifndef U3CU3EC__ITERATOR0_T717920108_H
#define U3CU3EC__ITERATOR0_T717920108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t717920108  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.AudioTrack UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$this
	AudioTrack_t1549411460 * ___U24this_0;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_1;
	// System.Boolean UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Timeline.AudioTrack/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24this_0)); }
	inline AudioTrack_t1549411460 * get_U24this_0() const { return ___U24this_0; }
	inline AudioTrack_t1549411460 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AudioTrack_t1549411460 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24current_1)); }
	inline PlayableBinding_t354260709  get_U24current_1() const { return ___U24current_1; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(PlayableBinding_t354260709  value)
	{
		___U24current_1 = value;
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t717920108, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T717920108_H
#ifndef BASICPLAYABLEBEHAVIOUR_T1482445671_H
#define BASICPLAYABLEBEHAVIOUR_T1482445671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.BasicPlayableBehaviour
struct  BasicPlayableBehaviour_t1482445671  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICPLAYABLEBEHAVIOUR_T1482445671_H
#ifndef INFINITERUNTIMECLIP_T156380358_H
#define INFINITERUNTIMECLIP_T156380358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.InfiniteRuntimeClip
struct  InfiniteRuntimeClip_t156380358  : public RuntimeElement_t2068542217
{
public:
	// UnityEngine.Playables.Playable UnityEngine.Timeline.InfiniteRuntimeClip::m_Playable
	Playable_t459825607  ___m_Playable_1;

public:
	inline static int32_t get_offset_of_m_Playable_1() { return static_cast<int32_t>(offsetof(InfiniteRuntimeClip_t156380358, ___m_Playable_1)); }
	inline Playable_t459825607  get_m_Playable_1() const { return ___m_Playable_1; }
	inline Playable_t459825607 * get_address_of_m_Playable_1() { return &___m_Playable_1; }
	inline void set_m_Playable_1(Playable_t459825607  value)
	{
		___m_Playable_1 = value;
	}
};

struct InfiniteRuntimeClip_t156380358_StaticFields
{
public:
	// System.Int64 UnityEngine.Timeline.InfiniteRuntimeClip::kIntervalEnd
	int64_t ___kIntervalEnd_2;

public:
	inline static int32_t get_offset_of_kIntervalEnd_2() { return static_cast<int32_t>(offsetof(InfiniteRuntimeClip_t156380358_StaticFields, ___kIntervalEnd_2)); }
	inline int64_t get_kIntervalEnd_2() const { return ___kIntervalEnd_2; }
	inline int64_t* get_address_of_kIntervalEnd_2() { return &___kIntervalEnd_2; }
	inline void set_kIntervalEnd_2(int64_t value)
	{
		___kIntervalEnd_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITERUNTIMECLIP_T156380358_H
#ifndef RUNTIMECLIP_T583365242_H
#define RUNTIMECLIP_T583365242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.RuntimeClip
struct  RuntimeClip_t583365242  : public RuntimeClipBase_t197358283
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.RuntimeClip::m_Clip
	TimelineClip_t2478225950 * ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_Playable
	Playable_t459825607  ___m_Playable_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.RuntimeClip::m_ParentMixer
	Playable_t459825607  ___m_ParentMixer_3;

public:
	inline static int32_t get_offset_of_m_Clip_1() { return static_cast<int32_t>(offsetof(RuntimeClip_t583365242, ___m_Clip_1)); }
	inline TimelineClip_t2478225950 * get_m_Clip_1() const { return ___m_Clip_1; }
	inline TimelineClip_t2478225950 ** get_address_of_m_Clip_1() { return &___m_Clip_1; }
	inline void set_m_Clip_1(TimelineClip_t2478225950 * value)
	{
		___m_Clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_1), value);
	}

	inline static int32_t get_offset_of_m_Playable_2() { return static_cast<int32_t>(offsetof(RuntimeClip_t583365242, ___m_Playable_2)); }
	inline Playable_t459825607  get_m_Playable_2() const { return ___m_Playable_2; }
	inline Playable_t459825607 * get_address_of_m_Playable_2() { return &___m_Playable_2; }
	inline void set_m_Playable_2(Playable_t459825607  value)
	{
		___m_Playable_2 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_3() { return static_cast<int32_t>(offsetof(RuntimeClip_t583365242, ___m_ParentMixer_3)); }
	inline Playable_t459825607  get_m_ParentMixer_3() const { return ___m_ParentMixer_3; }
	inline Playable_t459825607 * get_address_of_m_ParentMixer_3() { return &___m_ParentMixer_3; }
	inline void set_m_ParentMixer_3(Playable_t459825607  value)
	{
		___m_ParentMixer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECLIP_T583365242_H
#ifndef SCHEDULERUNTIMECLIP_T283756268_H
#define SCHEDULERUNTIMECLIP_T283756268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ScheduleRuntimeClip
struct  ScheduleRuntimeClip_t283756268  : public RuntimeClipBase_t197358283
{
public:
	// UnityEngine.Timeline.TimelineClip UnityEngine.Timeline.ScheduleRuntimeClip::m_Clip
	TimelineClip_t2478225950 * ___m_Clip_1;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_Playable
	Playable_t459825607  ___m_Playable_2;
	// UnityEngine.Playables.Playable UnityEngine.Timeline.ScheduleRuntimeClip::m_ParentMixer
	Playable_t459825607  ___m_ParentMixer_3;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_StartDelay
	double ___m_StartDelay_4;
	// System.Double UnityEngine.Timeline.ScheduleRuntimeClip::m_FinishTail
	double ___m_FinishTail_5;
	// System.Boolean UnityEngine.Timeline.ScheduleRuntimeClip::m_Started
	bool ___m_Started_6;

public:
	inline static int32_t get_offset_of_m_Clip_1() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_Clip_1)); }
	inline TimelineClip_t2478225950 * get_m_Clip_1() const { return ___m_Clip_1; }
	inline TimelineClip_t2478225950 ** get_address_of_m_Clip_1() { return &___m_Clip_1; }
	inline void set_m_Clip_1(TimelineClip_t2478225950 * value)
	{
		___m_Clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_1), value);
	}

	inline static int32_t get_offset_of_m_Playable_2() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_Playable_2)); }
	inline Playable_t459825607  get_m_Playable_2() const { return ___m_Playable_2; }
	inline Playable_t459825607 * get_address_of_m_Playable_2() { return &___m_Playable_2; }
	inline void set_m_Playable_2(Playable_t459825607  value)
	{
		___m_Playable_2 = value;
	}

	inline static int32_t get_offset_of_m_ParentMixer_3() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_ParentMixer_3)); }
	inline Playable_t459825607  get_m_ParentMixer_3() const { return ___m_ParentMixer_3; }
	inline Playable_t459825607 * get_address_of_m_ParentMixer_3() { return &___m_ParentMixer_3; }
	inline void set_m_ParentMixer_3(Playable_t459825607  value)
	{
		___m_ParentMixer_3 = value;
	}

	inline static int32_t get_offset_of_m_StartDelay_4() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_StartDelay_4)); }
	inline double get_m_StartDelay_4() const { return ___m_StartDelay_4; }
	inline double* get_address_of_m_StartDelay_4() { return &___m_StartDelay_4; }
	inline void set_m_StartDelay_4(double value)
	{
		___m_StartDelay_4 = value;
	}

	inline static int32_t get_offset_of_m_FinishTail_5() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_FinishTail_5)); }
	inline double get_m_FinishTail_5() const { return ___m_FinishTail_5; }
	inline double* get_address_of_m_FinishTail_5() { return &___m_FinishTail_5; }
	inline void set_m_FinishTail_5(double value)
	{
		___m_FinishTail_5 = value;
	}

	inline static int32_t get_offset_of_m_Started_6() { return static_cast<int32_t>(offsetof(ScheduleRuntimeClip_t283756268, ___m_Started_6)); }
	inline bool get_m_Started_6() const { return ___m_Started_6; }
	inline bool* get_address_of_m_Started_6() { return &___m_Started_6; }
	inline void set_m_Started_6(bool value)
	{
		___m_Started_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULERUNTIMECLIP_T283756268_H
#ifndef U3CU3EC__ITERATOR0_T3478306766_H
#define U3CU3EC__ITERATOR0_T3478306766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t3478306766  : public RuntimeObject
{
public:
	// UnityEngine.Timeline.TrackBindingTypeAttribute UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<attribute>__0
	TrackBindingTypeAttribute_t2757649500 * ___U3CattributeU3E__0_0;
	// System.Type UnityEngine.Timeline.TrackAsset/<>c__Iterator0::<trackBindingType>__0
	Type_t * ___U3CtrackBindingTypeU3E__0_1;
	// UnityEngine.Timeline.TrackAsset UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$this
	TrackAsset_t2828708245 * ___U24this_2;
	// UnityEngine.Playables.PlayableBinding UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$current
	PlayableBinding_t354260709  ___U24current_3;
	// System.Boolean UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.Timeline.TrackAsset/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CattributeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U3CattributeU3E__0_0)); }
	inline TrackBindingTypeAttribute_t2757649500 * get_U3CattributeU3E__0_0() const { return ___U3CattributeU3E__0_0; }
	inline TrackBindingTypeAttribute_t2757649500 ** get_address_of_U3CattributeU3E__0_0() { return &___U3CattributeU3E__0_0; }
	inline void set_U3CattributeU3E__0_0(TrackBindingTypeAttribute_t2757649500 * value)
	{
		___U3CattributeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CattributeU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CtrackBindingTypeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U3CtrackBindingTypeU3E__0_1)); }
	inline Type_t * get_U3CtrackBindingTypeU3E__0_1() const { return ___U3CtrackBindingTypeU3E__0_1; }
	inline Type_t ** get_address_of_U3CtrackBindingTypeU3E__0_1() { return &___U3CtrackBindingTypeU3E__0_1; }
	inline void set_U3CtrackBindingTypeU3E__0_1(Type_t * value)
	{
		___U3CtrackBindingTypeU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtrackBindingTypeU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24this_2)); }
	inline TrackAsset_t2828708245 * get_U24this_2() const { return ___U24this_2; }
	inline TrackAsset_t2828708245 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TrackAsset_t2828708245 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24current_3)); }
	inline PlayableBinding_t354260709  get_U24current_3() const { return ___U24current_3; }
	inline PlayableBinding_t354260709 * get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(PlayableBinding_t354260709  value)
	{
		___U24current_3 = value;
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3478306766, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T3478306766_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ACTIVATIONPLAYABLEASSET_T2332007632_H
#define ACTIVATIONPLAYABLEASSET_T2332007632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationPlayableAsset
struct  ActivationPlayableAsset_t2332007632  : public PlayableAsset_t3219022681
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONPLAYABLEASSET_T2332007632_H
#ifndef ANIMATIONPLAYABLEASSET_T734882934_H
#define ANIMATIONPLAYABLEASSET_T734882934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationPlayableAsset
struct  AnimationPlayableAsset_t734882934  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.AnimationClip UnityEngine.Timeline.AnimationPlayableAsset::m_Clip
	AnimationClip_t2318505987 * ___m_Clip_4;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_Position
	Vector3_t3722313464  ___m_Position_5;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationPlayableAsset::m_EulerAngles
	Vector3_t3722313464  ___m_EulerAngles_6;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_UseTrackMatchFields
	bool ___m_UseTrackMatchFields_7;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationPlayableAsset::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_8;
	// System.Boolean UnityEngine.Timeline.AnimationPlayableAsset::m_RemoveStartOffset
	bool ___m_RemoveStartOffset_9;
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset::m_Version
	int32_t ___m_Version_11;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationPlayableAsset::m_Rotation
	Quaternion_t2301928331  ___m_Rotation_12;

public:
	inline static int32_t get_offset_of_m_Clip_4() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Clip_4)); }
	inline AnimationClip_t2318505987 * get_m_Clip_4() const { return ___m_Clip_4; }
	inline AnimationClip_t2318505987 ** get_address_of_m_Clip_4() { return &___m_Clip_4; }
	inline void set_m_Clip_4(AnimationClip_t2318505987 * value)
	{
		___m_Clip_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_4), value);
	}

	inline static int32_t get_offset_of_m_Position_5() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Position_5)); }
	inline Vector3_t3722313464  get_m_Position_5() const { return ___m_Position_5; }
	inline Vector3_t3722313464 * get_address_of_m_Position_5() { return &___m_Position_5; }
	inline void set_m_Position_5(Vector3_t3722313464  value)
	{
		___m_Position_5 = value;
	}

	inline static int32_t get_offset_of_m_EulerAngles_6() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_EulerAngles_6)); }
	inline Vector3_t3722313464  get_m_EulerAngles_6() const { return ___m_EulerAngles_6; }
	inline Vector3_t3722313464 * get_address_of_m_EulerAngles_6() { return &___m_EulerAngles_6; }
	inline void set_m_EulerAngles_6(Vector3_t3722313464  value)
	{
		___m_EulerAngles_6 = value;
	}

	inline static int32_t get_offset_of_m_UseTrackMatchFields_7() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_UseTrackMatchFields_7)); }
	inline bool get_m_UseTrackMatchFields_7() const { return ___m_UseTrackMatchFields_7; }
	inline bool* get_address_of_m_UseTrackMatchFields_7() { return &___m_UseTrackMatchFields_7; }
	inline void set_m_UseTrackMatchFields_7(bool value)
	{
		___m_UseTrackMatchFields_7 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_8() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_MatchTargetFields_8)); }
	inline int32_t get_m_MatchTargetFields_8() const { return ___m_MatchTargetFields_8; }
	inline int32_t* get_address_of_m_MatchTargetFields_8() { return &___m_MatchTargetFields_8; }
	inline void set_m_MatchTargetFields_8(int32_t value)
	{
		___m_MatchTargetFields_8 = value;
	}

	inline static int32_t get_offset_of_m_RemoveStartOffset_9() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_RemoveStartOffset_9)); }
	inline bool get_m_RemoveStartOffset_9() const { return ___m_RemoveStartOffset_9; }
	inline bool* get_address_of_m_RemoveStartOffset_9() { return &___m_RemoveStartOffset_9; }
	inline void set_m_RemoveStartOffset_9(bool value)
	{
		___m_RemoveStartOffset_9 = value;
	}

	inline static int32_t get_offset_of_m_Version_11() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Version_11)); }
	inline int32_t get_m_Version_11() const { return ___m_Version_11; }
	inline int32_t* get_address_of_m_Version_11() { return &___m_Version_11; }
	inline void set_m_Version_11(int32_t value)
	{
		___m_Version_11 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_12() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934, ___m_Rotation_12)); }
	inline Quaternion_t2301928331  get_m_Rotation_12() const { return ___m_Rotation_12; }
	inline Quaternion_t2301928331 * get_address_of_m_Rotation_12() { return &___m_Rotation_12; }
	inline void set_m_Rotation_12(Quaternion_t2301928331  value)
	{
		___m_Rotation_12 = value;
	}
};

struct AnimationPlayableAsset_t734882934_StaticFields
{
public:
	// System.Int32 UnityEngine.Timeline.AnimationPlayableAsset::k_LatestVersion
	int32_t ___k_LatestVersion_10;

public:
	inline static int32_t get_offset_of_k_LatestVersion_10() { return static_cast<int32_t>(offsetof(AnimationPlayableAsset_t734882934_StaticFields, ___k_LatestVersion_10)); }
	inline int32_t get_k_LatestVersion_10() const { return ___k_LatestVersion_10; }
	inline int32_t* get_address_of_k_LatestVersion_10() { return &___k_LatestVersion_10; }
	inline void set_k_LatestVersion_10(int32_t value)
	{
		___k_LatestVersion_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPLAYABLEASSET_T734882934_H
#ifndef AUDIOPLAYABLEASSET_T2575444864_H
#define AUDIOPLAYABLEASSET_T2575444864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioPlayableAsset
struct  AudioPlayableAsset_t2575444864  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.AudioClip UnityEngine.Timeline.AudioPlayableAsset::m_Clip
	AudioClip_t3680889665 * ___m_Clip_4;
	// System.Boolean UnityEngine.Timeline.AudioPlayableAsset::m_Loop
	bool ___m_Loop_5;
	// System.Single UnityEngine.Timeline.AudioPlayableAsset::m_bufferingTime
	float ___m_bufferingTime_6;

public:
	inline static int32_t get_offset_of_m_Clip_4() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t2575444864, ___m_Clip_4)); }
	inline AudioClip_t3680889665 * get_m_Clip_4() const { return ___m_Clip_4; }
	inline AudioClip_t3680889665 ** get_address_of_m_Clip_4() { return &___m_Clip_4; }
	inline void set_m_Clip_4(AudioClip_t3680889665 * value)
	{
		___m_Clip_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clip_4), value);
	}

	inline static int32_t get_offset_of_m_Loop_5() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t2575444864, ___m_Loop_5)); }
	inline bool get_m_Loop_5() const { return ___m_Loop_5; }
	inline bool* get_address_of_m_Loop_5() { return &___m_Loop_5; }
	inline void set_m_Loop_5(bool value)
	{
		___m_Loop_5 = value;
	}

	inline static int32_t get_offset_of_m_bufferingTime_6() { return static_cast<int32_t>(offsetof(AudioPlayableAsset_t2575444864, ___m_bufferingTime_6)); }
	inline float get_m_bufferingTime_6() const { return ___m_bufferingTime_6; }
	inline float* get_address_of_m_bufferingTime_6() { return &___m_bufferingTime_6; }
	inline void set_m_bufferingTime_6(float value)
	{
		___m_bufferingTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEASSET_T2575444864_H
#ifndef CONTROLPLAYABLEASSET_T3597738600_H
#define CONTROLPLAYABLEASSET_T3597738600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlPlayableAsset
struct  ControlPlayableAsset_t3597738600  : public PlayableAsset_t3219022681
{
public:
	// UnityEngine.ExposedReference`1<UnityEngine.GameObject> UnityEngine.Timeline.ControlPlayableAsset::sourceGameObject
	ExposedReference_1_t1378690540  ___sourceGameObject_7;
	// UnityEngine.GameObject UnityEngine.Timeline.ControlPlayableAsset::prefabGameObject
	GameObject_t1113636619 * ___prefabGameObject_8;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateParticle
	bool ___updateParticle_9;
	// System.UInt32 UnityEngine.Timeline.ControlPlayableAsset::particleRandomSeed
	uint32_t ___particleRandomSeed_10;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateDirector
	bool ___updateDirector_11;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::updateITimeControl
	bool ___updateITimeControl_12;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::searchHierarchy
	bool ___searchHierarchy_13;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::active
	bool ___active_14;
	// UnityEngine.Timeline.ActivationControlPlayable/PostPlaybackState UnityEngine.Timeline.ControlPlayableAsset::postPlayback
	int32_t ___postPlayback_15;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.ControlPlayableAsset::m_ControlDirectorAsset
	PlayableAsset_t3219022681 * ___m_ControlDirectorAsset_16;
	// System.Double UnityEngine.Timeline.ControlPlayableAsset::m_Duration
	double ___m_Duration_17;
	// System.Boolean UnityEngine.Timeline.ControlPlayableAsset::m_SupportLoop
	bool ___m_SupportLoop_18;

public:
	inline static int32_t get_offset_of_sourceGameObject_7() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___sourceGameObject_7)); }
	inline ExposedReference_1_t1378690540  get_sourceGameObject_7() const { return ___sourceGameObject_7; }
	inline ExposedReference_1_t1378690540 * get_address_of_sourceGameObject_7() { return &___sourceGameObject_7; }
	inline void set_sourceGameObject_7(ExposedReference_1_t1378690540  value)
	{
		___sourceGameObject_7 = value;
	}

	inline static int32_t get_offset_of_prefabGameObject_8() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___prefabGameObject_8)); }
	inline GameObject_t1113636619 * get_prefabGameObject_8() const { return ___prefabGameObject_8; }
	inline GameObject_t1113636619 ** get_address_of_prefabGameObject_8() { return &___prefabGameObject_8; }
	inline void set_prefabGameObject_8(GameObject_t1113636619 * value)
	{
		___prefabGameObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefabGameObject_8), value);
	}

	inline static int32_t get_offset_of_updateParticle_9() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___updateParticle_9)); }
	inline bool get_updateParticle_9() const { return ___updateParticle_9; }
	inline bool* get_address_of_updateParticle_9() { return &___updateParticle_9; }
	inline void set_updateParticle_9(bool value)
	{
		___updateParticle_9 = value;
	}

	inline static int32_t get_offset_of_particleRandomSeed_10() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___particleRandomSeed_10)); }
	inline uint32_t get_particleRandomSeed_10() const { return ___particleRandomSeed_10; }
	inline uint32_t* get_address_of_particleRandomSeed_10() { return &___particleRandomSeed_10; }
	inline void set_particleRandomSeed_10(uint32_t value)
	{
		___particleRandomSeed_10 = value;
	}

	inline static int32_t get_offset_of_updateDirector_11() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___updateDirector_11)); }
	inline bool get_updateDirector_11() const { return ___updateDirector_11; }
	inline bool* get_address_of_updateDirector_11() { return &___updateDirector_11; }
	inline void set_updateDirector_11(bool value)
	{
		___updateDirector_11 = value;
	}

	inline static int32_t get_offset_of_updateITimeControl_12() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___updateITimeControl_12)); }
	inline bool get_updateITimeControl_12() const { return ___updateITimeControl_12; }
	inline bool* get_address_of_updateITimeControl_12() { return &___updateITimeControl_12; }
	inline void set_updateITimeControl_12(bool value)
	{
		___updateITimeControl_12 = value;
	}

	inline static int32_t get_offset_of_searchHierarchy_13() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___searchHierarchy_13)); }
	inline bool get_searchHierarchy_13() const { return ___searchHierarchy_13; }
	inline bool* get_address_of_searchHierarchy_13() { return &___searchHierarchy_13; }
	inline void set_searchHierarchy_13(bool value)
	{
		___searchHierarchy_13 = value;
	}

	inline static int32_t get_offset_of_active_14() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___active_14)); }
	inline bool get_active_14() const { return ___active_14; }
	inline bool* get_address_of_active_14() { return &___active_14; }
	inline void set_active_14(bool value)
	{
		___active_14 = value;
	}

	inline static int32_t get_offset_of_postPlayback_15() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___postPlayback_15)); }
	inline int32_t get_postPlayback_15() const { return ___postPlayback_15; }
	inline int32_t* get_address_of_postPlayback_15() { return &___postPlayback_15; }
	inline void set_postPlayback_15(int32_t value)
	{
		___postPlayback_15 = value;
	}

	inline static int32_t get_offset_of_m_ControlDirectorAsset_16() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___m_ControlDirectorAsset_16)); }
	inline PlayableAsset_t3219022681 * get_m_ControlDirectorAsset_16() const { return ___m_ControlDirectorAsset_16; }
	inline PlayableAsset_t3219022681 ** get_address_of_m_ControlDirectorAsset_16() { return &___m_ControlDirectorAsset_16; }
	inline void set_m_ControlDirectorAsset_16(PlayableAsset_t3219022681 * value)
	{
		___m_ControlDirectorAsset_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ControlDirectorAsset_16), value);
	}

	inline static int32_t get_offset_of_m_Duration_17() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___m_Duration_17)); }
	inline double get_m_Duration_17() const { return ___m_Duration_17; }
	inline double* get_address_of_m_Duration_17() { return &___m_Duration_17; }
	inline void set_m_Duration_17(double value)
	{
		___m_Duration_17 = value;
	}

	inline static int32_t get_offset_of_m_SupportLoop_18() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600, ___m_SupportLoop_18)); }
	inline bool get_m_SupportLoop_18() const { return ___m_SupportLoop_18; }
	inline bool* get_address_of_m_SupportLoop_18() { return &___m_SupportLoop_18; }
	inline void set_m_SupportLoop_18(bool value)
	{
		___m_SupportLoop_18 = value;
	}
};

struct ControlPlayableAsset_t3597738600_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Timeline.ControlPlayableAsset::k_EmptyDirectorsList
	List_1_t1980591739 * ___k_EmptyDirectorsList_5;
	// System.Collections.Generic.List`1<UnityEngine.ParticleSystem> UnityEngine.Timeline.ControlPlayableAsset::k_EmptyParticlesList
	List_1_t3272854023 * ___k_EmptyParticlesList_6;
	// System.Collections.Generic.HashSet`1<UnityEngine.Playables.PlayableDirector> UnityEngine.Timeline.ControlPlayableAsset::s_ProcessedDirectors
	HashSet_1_t3368433767 * ___s_ProcessedDirectors_19;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> UnityEngine.Timeline.ControlPlayableAsset::s_CreatedPrefabs
	HashSet_1_t3973553389 * ___s_CreatedPrefabs_20;

public:
	inline static int32_t get_offset_of_k_EmptyDirectorsList_5() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600_StaticFields, ___k_EmptyDirectorsList_5)); }
	inline List_1_t1980591739 * get_k_EmptyDirectorsList_5() const { return ___k_EmptyDirectorsList_5; }
	inline List_1_t1980591739 ** get_address_of_k_EmptyDirectorsList_5() { return &___k_EmptyDirectorsList_5; }
	inline void set_k_EmptyDirectorsList_5(List_1_t1980591739 * value)
	{
		___k_EmptyDirectorsList_5 = value;
		Il2CppCodeGenWriteBarrier((&___k_EmptyDirectorsList_5), value);
	}

	inline static int32_t get_offset_of_k_EmptyParticlesList_6() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600_StaticFields, ___k_EmptyParticlesList_6)); }
	inline List_1_t3272854023 * get_k_EmptyParticlesList_6() const { return ___k_EmptyParticlesList_6; }
	inline List_1_t3272854023 ** get_address_of_k_EmptyParticlesList_6() { return &___k_EmptyParticlesList_6; }
	inline void set_k_EmptyParticlesList_6(List_1_t3272854023 * value)
	{
		___k_EmptyParticlesList_6 = value;
		Il2CppCodeGenWriteBarrier((&___k_EmptyParticlesList_6), value);
	}

	inline static int32_t get_offset_of_s_ProcessedDirectors_19() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600_StaticFields, ___s_ProcessedDirectors_19)); }
	inline HashSet_1_t3368433767 * get_s_ProcessedDirectors_19() const { return ___s_ProcessedDirectors_19; }
	inline HashSet_1_t3368433767 ** get_address_of_s_ProcessedDirectors_19() { return &___s_ProcessedDirectors_19; }
	inline void set_s_ProcessedDirectors_19(HashSet_1_t3368433767 * value)
	{
		___s_ProcessedDirectors_19 = value;
		Il2CppCodeGenWriteBarrier((&___s_ProcessedDirectors_19), value);
	}

	inline static int32_t get_offset_of_s_CreatedPrefabs_20() { return static_cast<int32_t>(offsetof(ControlPlayableAsset_t3597738600_StaticFields, ___s_CreatedPrefabs_20)); }
	inline HashSet_1_t3973553389 * get_s_CreatedPrefabs_20() const { return ___s_CreatedPrefabs_20; }
	inline HashSet_1_t3973553389 ** get_address_of_s_CreatedPrefabs_20() { return &___s_CreatedPrefabs_20; }
	inline void set_s_CreatedPrefabs_20(HashSet_1_t3973553389 * value)
	{
		___s_CreatedPrefabs_20 = value;
		Il2CppCodeGenWriteBarrier((&___s_CreatedPrefabs_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPLAYABLEASSET_T3597738600_H
#ifndef TRACKASSET_T2828708245_H
#define TRACKASSET_T2828708245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.TrackAsset
struct  TrackAsset_t2828708245  : public PlayableAsset_t3219022681
{
public:
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Locked
	bool ___m_Locked_4;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_Muted
	bool ___m_Muted_5;
	// System.String UnityEngine.Timeline.TrackAsset::m_CustomPlayableFullTypename
	String_t* ___m_CustomPlayableFullTypename_6;
	// UnityEngine.AnimationClip UnityEngine.Timeline.TrackAsset::m_AnimClip
	AnimationClip_t2318505987 * ___m_AnimClip_7;
	// UnityEngine.Playables.PlayableAsset UnityEngine.Timeline.TrackAsset::m_Parent
	PlayableAsset_t3219022681 * ___m_Parent_8;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> UnityEngine.Timeline.TrackAsset::m_Children
	List_1_t4000433264 * ___m_Children_9;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_ItemsHash
	int32_t ___m_ItemsHash_10;
	// UnityEngine.Timeline.TimelineClip[] UnityEngine.Timeline.TrackAsset::m_ClipsCache
	TimelineClipU5BU5D_t140784555* ___m_ClipsCache_11;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_Start
	DiscreteTime_t924799574  ___m_Start_12;
	// UnityEngine.Timeline.DiscreteTime UnityEngine.Timeline.TrackAsset::m_End
	DiscreteTime_t924799574  ___m_End_13;
	// System.Boolean UnityEngine.Timeline.TrackAsset::m_CacheSorted
	bool ___m_CacheSorted_14;
	// System.Collections.Generic.IEnumerable`1<UnityEngine.Timeline.TrackAsset> UnityEngine.Timeline.TrackAsset::m_ChildTrackCache
	RuntimeObject* ___m_ChildTrackCache_16;
	// System.Collections.Generic.List`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::m_Clips
	List_1_t3950300692 * ___m_Clips_18;
	// System.Int32 UnityEngine.Timeline.TrackAsset::m_Version
	int32_t ___m_Version_21;

public:
	inline static int32_t get_offset_of_m_Locked_4() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Locked_4)); }
	inline bool get_m_Locked_4() const { return ___m_Locked_4; }
	inline bool* get_address_of_m_Locked_4() { return &___m_Locked_4; }
	inline void set_m_Locked_4(bool value)
	{
		___m_Locked_4 = value;
	}

	inline static int32_t get_offset_of_m_Muted_5() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Muted_5)); }
	inline bool get_m_Muted_5() const { return ___m_Muted_5; }
	inline bool* get_address_of_m_Muted_5() { return &___m_Muted_5; }
	inline void set_m_Muted_5(bool value)
	{
		___m_Muted_5 = value;
	}

	inline static int32_t get_offset_of_m_CustomPlayableFullTypename_6() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_CustomPlayableFullTypename_6)); }
	inline String_t* get_m_CustomPlayableFullTypename_6() const { return ___m_CustomPlayableFullTypename_6; }
	inline String_t** get_address_of_m_CustomPlayableFullTypename_6() { return &___m_CustomPlayableFullTypename_6; }
	inline void set_m_CustomPlayableFullTypename_6(String_t* value)
	{
		___m_CustomPlayableFullTypename_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomPlayableFullTypename_6), value);
	}

	inline static int32_t get_offset_of_m_AnimClip_7() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_AnimClip_7)); }
	inline AnimationClip_t2318505987 * get_m_AnimClip_7() const { return ___m_AnimClip_7; }
	inline AnimationClip_t2318505987 ** get_address_of_m_AnimClip_7() { return &___m_AnimClip_7; }
	inline void set_m_AnimClip_7(AnimationClip_t2318505987 * value)
	{
		___m_AnimClip_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimClip_7), value);
	}

	inline static int32_t get_offset_of_m_Parent_8() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Parent_8)); }
	inline PlayableAsset_t3219022681 * get_m_Parent_8() const { return ___m_Parent_8; }
	inline PlayableAsset_t3219022681 ** get_address_of_m_Parent_8() { return &___m_Parent_8; }
	inline void set_m_Parent_8(PlayableAsset_t3219022681 * value)
	{
		___m_Parent_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parent_8), value);
	}

	inline static int32_t get_offset_of_m_Children_9() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Children_9)); }
	inline List_1_t4000433264 * get_m_Children_9() const { return ___m_Children_9; }
	inline List_1_t4000433264 ** get_address_of_m_Children_9() { return &___m_Children_9; }
	inline void set_m_Children_9(List_1_t4000433264 * value)
	{
		___m_Children_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Children_9), value);
	}

	inline static int32_t get_offset_of_m_ItemsHash_10() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ItemsHash_10)); }
	inline int32_t get_m_ItemsHash_10() const { return ___m_ItemsHash_10; }
	inline int32_t* get_address_of_m_ItemsHash_10() { return &___m_ItemsHash_10; }
	inline void set_m_ItemsHash_10(int32_t value)
	{
		___m_ItemsHash_10 = value;
	}

	inline static int32_t get_offset_of_m_ClipsCache_11() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ClipsCache_11)); }
	inline TimelineClipU5BU5D_t140784555* get_m_ClipsCache_11() const { return ___m_ClipsCache_11; }
	inline TimelineClipU5BU5D_t140784555** get_address_of_m_ClipsCache_11() { return &___m_ClipsCache_11; }
	inline void set_m_ClipsCache_11(TimelineClipU5BU5D_t140784555* value)
	{
		___m_ClipsCache_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipsCache_11), value);
	}

	inline static int32_t get_offset_of_m_Start_12() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Start_12)); }
	inline DiscreteTime_t924799574  get_m_Start_12() const { return ___m_Start_12; }
	inline DiscreteTime_t924799574 * get_address_of_m_Start_12() { return &___m_Start_12; }
	inline void set_m_Start_12(DiscreteTime_t924799574  value)
	{
		___m_Start_12 = value;
	}

	inline static int32_t get_offset_of_m_End_13() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_End_13)); }
	inline DiscreteTime_t924799574  get_m_End_13() const { return ___m_End_13; }
	inline DiscreteTime_t924799574 * get_address_of_m_End_13() { return &___m_End_13; }
	inline void set_m_End_13(DiscreteTime_t924799574  value)
	{
		___m_End_13 = value;
	}

	inline static int32_t get_offset_of_m_CacheSorted_14() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_CacheSorted_14)); }
	inline bool get_m_CacheSorted_14() const { return ___m_CacheSorted_14; }
	inline bool* get_address_of_m_CacheSorted_14() { return &___m_CacheSorted_14; }
	inline void set_m_CacheSorted_14(bool value)
	{
		___m_CacheSorted_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildTrackCache_16() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_ChildTrackCache_16)); }
	inline RuntimeObject* get_m_ChildTrackCache_16() const { return ___m_ChildTrackCache_16; }
	inline RuntimeObject** get_address_of_m_ChildTrackCache_16() { return &___m_ChildTrackCache_16; }
	inline void set_m_ChildTrackCache_16(RuntimeObject* value)
	{
		___m_ChildTrackCache_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChildTrackCache_16), value);
	}

	inline static int32_t get_offset_of_m_Clips_18() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Clips_18)); }
	inline List_1_t3950300692 * get_m_Clips_18() const { return ___m_Clips_18; }
	inline List_1_t3950300692 ** get_address_of_m_Clips_18() { return &___m_Clips_18; }
	inline void set_m_Clips_18(List_1_t3950300692 * value)
	{
		___m_Clips_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clips_18), value);
	}

	inline static int32_t get_offset_of_m_Version_21() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245, ___m_Version_21)); }
	inline int32_t get_m_Version_21() const { return ___m_Version_21; }
	inline int32_t* get_address_of_m_Version_21() { return &___m_Version_21; }
	inline void set_m_Version_21(int32_t value)
	{
		___m_Version_21 = value;
	}
};

struct TrackAsset_t2828708245_StaticFields
{
public:
	// UnityEngine.Timeline.TrackAsset[] UnityEngine.Timeline.TrackAsset::s_EmptyCache
	TrackAssetU5BU5D_t52518008* ___s_EmptyCache_15;
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.Timeline.TrackBindingTypeAttribute> UnityEngine.Timeline.TrackAsset::s_TrackBindingTypeAttributeCache
	Dictionary_2_t907029268 * ___s_TrackBindingTypeAttributeCache_17;
	// System.Action`3<UnityEngine.Timeline.TimelineClip,UnityEngine.GameObject,UnityEngine.Playables.Playable> UnityEngine.Timeline.TrackAsset::OnPlayableCreate
	Action_3_t3876410139 * ___OnPlayableCreate_19;
	// System.Comparison`1<UnityEngine.Timeline.TimelineClip> UnityEngine.Timeline.TrackAsset::<>f__am$cache0
	Comparison_1_t2253157129 * ___U3CU3Ef__amU24cache0_22;

public:
	inline static int32_t get_offset_of_s_EmptyCache_15() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___s_EmptyCache_15)); }
	inline TrackAssetU5BU5D_t52518008* get_s_EmptyCache_15() const { return ___s_EmptyCache_15; }
	inline TrackAssetU5BU5D_t52518008** get_address_of_s_EmptyCache_15() { return &___s_EmptyCache_15; }
	inline void set_s_EmptyCache_15(TrackAssetU5BU5D_t52518008* value)
	{
		___s_EmptyCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyCache_15), value);
	}

	inline static int32_t get_offset_of_s_TrackBindingTypeAttributeCache_17() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___s_TrackBindingTypeAttributeCache_17)); }
	inline Dictionary_2_t907029268 * get_s_TrackBindingTypeAttributeCache_17() const { return ___s_TrackBindingTypeAttributeCache_17; }
	inline Dictionary_2_t907029268 ** get_address_of_s_TrackBindingTypeAttributeCache_17() { return &___s_TrackBindingTypeAttributeCache_17; }
	inline void set_s_TrackBindingTypeAttributeCache_17(Dictionary_2_t907029268 * value)
	{
		___s_TrackBindingTypeAttributeCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackBindingTypeAttributeCache_17), value);
	}

	inline static int32_t get_offset_of_OnPlayableCreate_19() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___OnPlayableCreate_19)); }
	inline Action_3_t3876410139 * get_OnPlayableCreate_19() const { return ___OnPlayableCreate_19; }
	inline Action_3_t3876410139 ** get_address_of_OnPlayableCreate_19() { return &___OnPlayableCreate_19; }
	inline void set_OnPlayableCreate_19(Action_3_t3876410139 * value)
	{
		___OnPlayableCreate_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayableCreate_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_22() { return static_cast<int32_t>(offsetof(TrackAsset_t2828708245_StaticFields, ___U3CU3Ef__amU24cache0_22)); }
	inline Comparison_1_t2253157129 * get_U3CU3Ef__amU24cache0_22() const { return ___U3CU3Ef__amU24cache0_22; }
	inline Comparison_1_t2253157129 ** get_address_of_U3CU3Ef__amU24cache0_22() { return &___U3CU3Ef__amU24cache0_22; }
	inline void set_U3CU3Ef__amU24cache0_22(Comparison_1_t2253157129 * value)
	{
		___U3CU3Ef__amU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKASSET_T2828708245_H
#ifndef ARENDINGEVENTSENDER_T1071853717_H
#define ARENDINGEVENTSENDER_T1071853717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AREndingEventSender
struct  AREndingEventSender_t1071853717  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AREndingEventSender::timeBeforeEnding
	float ___timeBeforeEnding_4;

public:
	inline static int32_t get_offset_of_timeBeforeEnding_4() { return static_cast<int32_t>(offsetof(AREndingEventSender_t1071853717, ___timeBeforeEnding_4)); }
	inline float get_timeBeforeEnding_4() const { return ___timeBeforeEnding_4; }
	inline float* get_address_of_timeBeforeEnding_4() { return &___timeBeforeEnding_4; }
	inline void set_timeBeforeEnding_4(float value)
	{
		___timeBeforeEnding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARENDINGEVENTSENDER_T1071853717_H
#ifndef ARSCREENSHOTSAVER_T4164871774_H
#define ARSCREENSHOTSAVER_T4164871774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARScreenShotSaver
struct  ARScreenShotSaver_t4164871774  : public MonoBehaviour_t3962482529
{
public:
	// System.String ARScreenShotSaver::imageName
	String_t* ___imageName_5;
	// System.String ARScreenShotSaver::saveFolder
	String_t* ___saveFolder_6;
	// SaveFolderType ARScreenShotSaver::folderType
	int32_t ___folderType_7;
	// UnityEngine.TextureFormat ARScreenShotSaver::saveFormat
	int32_t ___saveFormat_8;
	// UnityEngine.RenderTexture ARScreenShotSaver::rendT
	RenderTexture_t2108887433 * ___rendT_9;
	// System.Int32 ARScreenShotSaver::photoCount
	int32_t ___photoCount_10;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> ARScreenShotSaver::savedScreenShots
	List_1_t1017553631 * ___savedScreenShots_11;

public:
	inline static int32_t get_offset_of_imageName_5() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___imageName_5)); }
	inline String_t* get_imageName_5() const { return ___imageName_5; }
	inline String_t** get_address_of_imageName_5() { return &___imageName_5; }
	inline void set_imageName_5(String_t* value)
	{
		___imageName_5 = value;
		Il2CppCodeGenWriteBarrier((&___imageName_5), value);
	}

	inline static int32_t get_offset_of_saveFolder_6() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___saveFolder_6)); }
	inline String_t* get_saveFolder_6() const { return ___saveFolder_6; }
	inline String_t** get_address_of_saveFolder_6() { return &___saveFolder_6; }
	inline void set_saveFolder_6(String_t* value)
	{
		___saveFolder_6 = value;
		Il2CppCodeGenWriteBarrier((&___saveFolder_6), value);
	}

	inline static int32_t get_offset_of_folderType_7() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___folderType_7)); }
	inline int32_t get_folderType_7() const { return ___folderType_7; }
	inline int32_t* get_address_of_folderType_7() { return &___folderType_7; }
	inline void set_folderType_7(int32_t value)
	{
		___folderType_7 = value;
	}

	inline static int32_t get_offset_of_saveFormat_8() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___saveFormat_8)); }
	inline int32_t get_saveFormat_8() const { return ___saveFormat_8; }
	inline int32_t* get_address_of_saveFormat_8() { return &___saveFormat_8; }
	inline void set_saveFormat_8(int32_t value)
	{
		___saveFormat_8 = value;
	}

	inline static int32_t get_offset_of_rendT_9() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___rendT_9)); }
	inline RenderTexture_t2108887433 * get_rendT_9() const { return ___rendT_9; }
	inline RenderTexture_t2108887433 ** get_address_of_rendT_9() { return &___rendT_9; }
	inline void set_rendT_9(RenderTexture_t2108887433 * value)
	{
		___rendT_9 = value;
		Il2CppCodeGenWriteBarrier((&___rendT_9), value);
	}

	inline static int32_t get_offset_of_photoCount_10() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___photoCount_10)); }
	inline int32_t get_photoCount_10() const { return ___photoCount_10; }
	inline int32_t* get_address_of_photoCount_10() { return &___photoCount_10; }
	inline void set_photoCount_10(int32_t value)
	{
		___photoCount_10 = value;
	}

	inline static int32_t get_offset_of_savedScreenShots_11() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774, ___savedScreenShots_11)); }
	inline List_1_t1017553631 * get_savedScreenShots_11() const { return ___savedScreenShots_11; }
	inline List_1_t1017553631 ** get_address_of_savedScreenShots_11() { return &___savedScreenShots_11; }
	inline void set_savedScreenShots_11(List_1_t1017553631 * value)
	{
		___savedScreenShots_11 = value;
		Il2CppCodeGenWriteBarrier((&___savedScreenShots_11), value);
	}
};

struct ARScreenShotSaver_t4164871774_StaticFields
{
public:
	// ARScreenShotSaver ARScreenShotSaver::Instance
	ARScreenShotSaver_t4164871774 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(ARScreenShotSaver_t4164871774_StaticFields, ___Instance_4)); }
	inline ARScreenShotSaver_t4164871774 * get_Instance_4() const { return ___Instance_4; }
	inline ARScreenShotSaver_t4164871774 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(ARScreenShotSaver_t4164871774 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSCREENSHOTSAVER_T4164871774_H
#ifndef ARSTARTER_T598103033_H
#define ARSTARTER_T598103033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARStarter
struct  ARStarter_t598103033  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ARStarter::planeRotater
	Transform_t3600365921 * ___planeRotater_4;
	// UnityEngine.GameObject ARStarter::arObjSet
	GameObject_t1113636619 * ___arObjSet_5;

public:
	inline static int32_t get_offset_of_planeRotater_4() { return static_cast<int32_t>(offsetof(ARStarter_t598103033, ___planeRotater_4)); }
	inline Transform_t3600365921 * get_planeRotater_4() const { return ___planeRotater_4; }
	inline Transform_t3600365921 ** get_address_of_planeRotater_4() { return &___planeRotater_4; }
	inline void set_planeRotater_4(Transform_t3600365921 * value)
	{
		___planeRotater_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeRotater_4), value);
	}

	inline static int32_t get_offset_of_arObjSet_5() { return static_cast<int32_t>(offsetof(ARStarter_t598103033, ___arObjSet_5)); }
	inline GameObject_t1113636619 * get_arObjSet_5() const { return ___arObjSet_5; }
	inline GameObject_t1113636619 ** get_address_of_arObjSet_5() { return &___arObjSet_5; }
	inline void set_arObjSet_5(GameObject_t1113636619 * value)
	{
		___arObjSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___arObjSet_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSTARTER_T598103033_H
#ifndef APPLYUSERFACEONENABLE_T259624351_H
#define APPLYUSERFACEONENABLE_T259624351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplyUserFaceOnEnable
struct  ApplyUserFaceOnEnable_t259624351  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLYUSERFACEONENABLE_T259624351_H
#ifndef APPLYUSERSCREENSHOT_T2947430595_H
#define APPLYUSERSCREENSHOT_T2947430595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplyUserScreenShot
struct  ApplyUserScreenShot_t2947430595  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLYUSERSCREENSHOT_T2947430595_H
#ifndef AUDIOBUTTONRANDOMPITCH_T1382649135_H
#define AUDIOBUTTONRANDOMPITCH_T1382649135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioButtonRandomPitch
struct  AudioButtonRandomPitch_t1382649135  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource AudioButtonRandomPitch::_audio
	AudioSource_t3935305588 * ____audio_4;
	// UnityEngine.AudioClip[] AudioButtonRandomPitch::clips
	AudioClipU5BU5D_t143221404* ___clips_5;
	// UnityEngine.Vector2 AudioButtonRandomPitch::pitchRange
	Vector2_t2156229523  ___pitchRange_6;

public:
	inline static int32_t get_offset_of__audio_4() { return static_cast<int32_t>(offsetof(AudioButtonRandomPitch_t1382649135, ____audio_4)); }
	inline AudioSource_t3935305588 * get__audio_4() const { return ____audio_4; }
	inline AudioSource_t3935305588 ** get_address_of__audio_4() { return &____audio_4; }
	inline void set__audio_4(AudioSource_t3935305588 * value)
	{
		____audio_4 = value;
		Il2CppCodeGenWriteBarrier((&____audio_4), value);
	}

	inline static int32_t get_offset_of_clips_5() { return static_cast<int32_t>(offsetof(AudioButtonRandomPitch_t1382649135, ___clips_5)); }
	inline AudioClipU5BU5D_t143221404* get_clips_5() const { return ___clips_5; }
	inline AudioClipU5BU5D_t143221404** get_address_of_clips_5() { return &___clips_5; }
	inline void set_clips_5(AudioClipU5BU5D_t143221404* value)
	{
		___clips_5 = value;
		Il2CppCodeGenWriteBarrier((&___clips_5), value);
	}

	inline static int32_t get_offset_of_pitchRange_6() { return static_cast<int32_t>(offsetof(AudioButtonRandomPitch_t1382649135, ___pitchRange_6)); }
	inline Vector2_t2156229523  get_pitchRange_6() const { return ___pitchRange_6; }
	inline Vector2_t2156229523 * get_address_of_pitchRange_6() { return &___pitchRange_6; }
	inline void set_pitchRange_6(Vector2_t2156229523  value)
	{
		___pitchRange_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBUTTONRANDOMPITCH_T1382649135_H
#ifndef CAMERASTATUSMONITOR_T741672456_H
#define CAMERASTATUSMONITOR_T741672456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraStatusMonitor
struct  CameraStatusMonitor_t741672456  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera CameraStatusMonitor::_cam
	Camera_t4157153871 * ____cam_4;
	// UnityEngine.UI.Text CameraStatusMonitor::fovText
	Text_t1901882714 * ___fovText_5;
	// UnityEngine.UI.Text CameraStatusMonitor::ratioText
	Text_t1901882714 * ___ratioText_6;

public:
	inline static int32_t get_offset_of__cam_4() { return static_cast<int32_t>(offsetof(CameraStatusMonitor_t741672456, ____cam_4)); }
	inline Camera_t4157153871 * get__cam_4() const { return ____cam_4; }
	inline Camera_t4157153871 ** get_address_of__cam_4() { return &____cam_4; }
	inline void set__cam_4(Camera_t4157153871 * value)
	{
		____cam_4 = value;
		Il2CppCodeGenWriteBarrier((&____cam_4), value);
	}

	inline static int32_t get_offset_of_fovText_5() { return static_cast<int32_t>(offsetof(CameraStatusMonitor_t741672456, ___fovText_5)); }
	inline Text_t1901882714 * get_fovText_5() const { return ___fovText_5; }
	inline Text_t1901882714 ** get_address_of_fovText_5() { return &___fovText_5; }
	inline void set_fovText_5(Text_t1901882714 * value)
	{
		___fovText_5 = value;
		Il2CppCodeGenWriteBarrier((&___fovText_5), value);
	}

	inline static int32_t get_offset_of_ratioText_6() { return static_cast<int32_t>(offsetof(CameraStatusMonitor_t741672456, ___ratioText_6)); }
	inline Text_t1901882714 * get_ratioText_6() const { return ___ratioText_6; }
	inline Text_t1901882714 ** get_address_of_ratioText_6() { return &___ratioText_6; }
	inline void set_ratioText_6(Text_t1901882714 * value)
	{
		___ratioText_6 = value;
		Il2CppCodeGenWriteBarrier((&___ratioText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASTATUSMONITOR_T741672456_H
#ifndef CHARACTERANIMATIONSTARTER_T4195249726_H
#define CHARACTERANIMATIONSTARTER_T4195249726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterAnimationStarter
struct  CharacterAnimationStarter_t4195249726  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CharacterAnimationStarter::charactersRoot
	Transform_t3600365921 * ___charactersRoot_4;
	// System.Boolean CharacterAnimationStarter::startKey
	bool ___startKey_5;
	// System.Boolean CharacterAnimationStarter::isStarted
	bool ___isStarted_6;

public:
	inline static int32_t get_offset_of_charactersRoot_4() { return static_cast<int32_t>(offsetof(CharacterAnimationStarter_t4195249726, ___charactersRoot_4)); }
	inline Transform_t3600365921 * get_charactersRoot_4() const { return ___charactersRoot_4; }
	inline Transform_t3600365921 ** get_address_of_charactersRoot_4() { return &___charactersRoot_4; }
	inline void set_charactersRoot_4(Transform_t3600365921 * value)
	{
		___charactersRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&___charactersRoot_4), value);
	}

	inline static int32_t get_offset_of_startKey_5() { return static_cast<int32_t>(offsetof(CharacterAnimationStarter_t4195249726, ___startKey_5)); }
	inline bool get_startKey_5() const { return ___startKey_5; }
	inline bool* get_address_of_startKey_5() { return &___startKey_5; }
	inline void set_startKey_5(bool value)
	{
		___startKey_5 = value;
	}

	inline static int32_t get_offset_of_isStarted_6() { return static_cast<int32_t>(offsetof(CharacterAnimationStarter_t4195249726, ___isStarted_6)); }
	inline bool get_isStarted_6() const { return ___isStarted_6; }
	inline bool* get_address_of_isStarted_6() { return &___isStarted_6; }
	inline void set_isStarted_6(bool value)
	{
		___isStarted_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERANIMATIONSTARTER_T4195249726_H
#ifndef COPYCAMERADATA_T869109636_H
#define COPYCAMERADATA_T869109636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CopyCameraData
struct  CopyCameraData_t869109636  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera CopyCameraData::targetCamera
	Camera_t4157153871 * ___targetCamera_4;

public:
	inline static int32_t get_offset_of_targetCamera_4() { return static_cast<int32_t>(offsetof(CopyCameraData_t869109636, ___targetCamera_4)); }
	inline Camera_t4157153871 * get_targetCamera_4() const { return ___targetCamera_4; }
	inline Camera_t4157153871 ** get_address_of_targetCamera_4() { return &___targetCamera_4; }
	inline void set_targetCamera_4(Camera_t4157153871 * value)
	{
		___targetCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetCamera_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYCAMERADATA_T869109636_H
#ifndef DYNAMICSHADOWDISTANCE_T2173449775_H
#define DYNAMICSHADOWDISTANCE_T2173449775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicShadowDistance
struct  DynamicShadowDistance_t2173449775  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform DynamicShadowDistance::targetObj
	Transform_t3600365921 * ___targetObj_4;
	// System.Single DynamicShadowDistance::camToObjDist
	float ___camToObjDist_5;

public:
	inline static int32_t get_offset_of_targetObj_4() { return static_cast<int32_t>(offsetof(DynamicShadowDistance_t2173449775, ___targetObj_4)); }
	inline Transform_t3600365921 * get_targetObj_4() const { return ___targetObj_4; }
	inline Transform_t3600365921 ** get_address_of_targetObj_4() { return &___targetObj_4; }
	inline void set_targetObj_4(Transform_t3600365921 * value)
	{
		___targetObj_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetObj_4), value);
	}

	inline static int32_t get_offset_of_camToObjDist_5() { return static_cast<int32_t>(offsetof(DynamicShadowDistance_t2173449775, ___camToObjDist_5)); }
	inline float get_camToObjDist_5() const { return ___camToObjDist_5; }
	inline float* get_address_of_camToObjDist_5() { return &___camToObjDist_5; }
	inline void set_camToObjDist_5(float value)
	{
		___camToObjDist_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWDISTANCE_T2173449775_H
#ifndef FACECAMERACONTROLLER_T3256806653_H
#define FACECAMERACONTROLLER_T3256806653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCameraController
struct  FaceCameraController_t3256806653  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.WebCamTexture FaceCameraController::frontCamTexture
	WebCamTexture_t1514609158 * ___frontCamTexture_4;
	// UnityEngine.MeshRenderer FaceCameraController::applyQuad
	MeshRenderer_t587009260 * ___applyQuad_5;
	// UnityEngine.UI.RawImage FaceCameraController::uiQuad
	RawImage_t3182918964 * ___uiQuad_6;
	// System.Boolean FaceCameraController::useIndex
	bool ___useIndex_7;
	// System.Int32 FaceCameraController::camIndex
	int32_t ___camIndex_8;

public:
	inline static int32_t get_offset_of_frontCamTexture_4() { return static_cast<int32_t>(offsetof(FaceCameraController_t3256806653, ___frontCamTexture_4)); }
	inline WebCamTexture_t1514609158 * get_frontCamTexture_4() const { return ___frontCamTexture_4; }
	inline WebCamTexture_t1514609158 ** get_address_of_frontCamTexture_4() { return &___frontCamTexture_4; }
	inline void set_frontCamTexture_4(WebCamTexture_t1514609158 * value)
	{
		___frontCamTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___frontCamTexture_4), value);
	}

	inline static int32_t get_offset_of_applyQuad_5() { return static_cast<int32_t>(offsetof(FaceCameraController_t3256806653, ___applyQuad_5)); }
	inline MeshRenderer_t587009260 * get_applyQuad_5() const { return ___applyQuad_5; }
	inline MeshRenderer_t587009260 ** get_address_of_applyQuad_5() { return &___applyQuad_5; }
	inline void set_applyQuad_5(MeshRenderer_t587009260 * value)
	{
		___applyQuad_5 = value;
		Il2CppCodeGenWriteBarrier((&___applyQuad_5), value);
	}

	inline static int32_t get_offset_of_uiQuad_6() { return static_cast<int32_t>(offsetof(FaceCameraController_t3256806653, ___uiQuad_6)); }
	inline RawImage_t3182918964 * get_uiQuad_6() const { return ___uiQuad_6; }
	inline RawImage_t3182918964 ** get_address_of_uiQuad_6() { return &___uiQuad_6; }
	inline void set_uiQuad_6(RawImage_t3182918964 * value)
	{
		___uiQuad_6 = value;
		Il2CppCodeGenWriteBarrier((&___uiQuad_6), value);
	}

	inline static int32_t get_offset_of_useIndex_7() { return static_cast<int32_t>(offsetof(FaceCameraController_t3256806653, ___useIndex_7)); }
	inline bool get_useIndex_7() const { return ___useIndex_7; }
	inline bool* get_address_of_useIndex_7() { return &___useIndex_7; }
	inline void set_useIndex_7(bool value)
	{
		___useIndex_7 = value;
	}

	inline static int32_t get_offset_of_camIndex_8() { return static_cast<int32_t>(offsetof(FaceCameraController_t3256806653, ___camIndex_8)); }
	inline int32_t get_camIndex_8() const { return ___camIndex_8; }
	inline int32_t* get_address_of_camIndex_8() { return &___camIndex_8; }
	inline void set_camIndex_8(int32_t value)
	{
		___camIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECAMERACONTROLLER_T3256806653_H
#ifndef FACETEXTURECONTAINER_T816841679_H
#define FACETEXTURECONTAINER_T816841679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceTextureContainer
struct  FaceTextureContainer_t816841679  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera FaceTextureContainer::faceTextureCam
	Camera_t4157153871 * ___faceTextureCam_5;
	// UnityEngine.RenderTexture FaceTextureContainer::faceCamTexture
	RenderTexture_t2108887433 * ___faceCamTexture_6;
	// UnityEngine.MeshRenderer FaceTextureContainer::combineFaceQuad
	MeshRenderer_t587009260 * ___combineFaceQuad_7;
	// UnityEngine.Texture2D FaceTextureContainer::faceTexture
	Texture2D_t3840446185 * ___faceTexture_8;
	// UnityEngine.UI.RawImage FaceTextureContainer::demoUI
	RawImage_t3182918964 * ___demoUI_9;

public:
	inline static int32_t get_offset_of_faceTextureCam_5() { return static_cast<int32_t>(offsetof(FaceTextureContainer_t816841679, ___faceTextureCam_5)); }
	inline Camera_t4157153871 * get_faceTextureCam_5() const { return ___faceTextureCam_5; }
	inline Camera_t4157153871 ** get_address_of_faceTextureCam_5() { return &___faceTextureCam_5; }
	inline void set_faceTextureCam_5(Camera_t4157153871 * value)
	{
		___faceTextureCam_5 = value;
		Il2CppCodeGenWriteBarrier((&___faceTextureCam_5), value);
	}

	inline static int32_t get_offset_of_faceCamTexture_6() { return static_cast<int32_t>(offsetof(FaceTextureContainer_t816841679, ___faceCamTexture_6)); }
	inline RenderTexture_t2108887433 * get_faceCamTexture_6() const { return ___faceCamTexture_6; }
	inline RenderTexture_t2108887433 ** get_address_of_faceCamTexture_6() { return &___faceCamTexture_6; }
	inline void set_faceCamTexture_6(RenderTexture_t2108887433 * value)
	{
		___faceCamTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___faceCamTexture_6), value);
	}

	inline static int32_t get_offset_of_combineFaceQuad_7() { return static_cast<int32_t>(offsetof(FaceTextureContainer_t816841679, ___combineFaceQuad_7)); }
	inline MeshRenderer_t587009260 * get_combineFaceQuad_7() const { return ___combineFaceQuad_7; }
	inline MeshRenderer_t587009260 ** get_address_of_combineFaceQuad_7() { return &___combineFaceQuad_7; }
	inline void set_combineFaceQuad_7(MeshRenderer_t587009260 * value)
	{
		___combineFaceQuad_7 = value;
		Il2CppCodeGenWriteBarrier((&___combineFaceQuad_7), value);
	}

	inline static int32_t get_offset_of_faceTexture_8() { return static_cast<int32_t>(offsetof(FaceTextureContainer_t816841679, ___faceTexture_8)); }
	inline Texture2D_t3840446185 * get_faceTexture_8() const { return ___faceTexture_8; }
	inline Texture2D_t3840446185 ** get_address_of_faceTexture_8() { return &___faceTexture_8; }
	inline void set_faceTexture_8(Texture2D_t3840446185 * value)
	{
		___faceTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___faceTexture_8), value);
	}

	inline static int32_t get_offset_of_demoUI_9() { return static_cast<int32_t>(offsetof(FaceTextureContainer_t816841679, ___demoUI_9)); }
	inline RawImage_t3182918964 * get_demoUI_9() const { return ___demoUI_9; }
	inline RawImage_t3182918964 ** get_address_of_demoUI_9() { return &___demoUI_9; }
	inline void set_demoUI_9(RawImage_t3182918964 * value)
	{
		___demoUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___demoUI_9), value);
	}
};

struct FaceTextureContainer_t816841679_StaticFields
{
public:
	// FaceTextureContainer FaceTextureContainer::Instance
	FaceTextureContainer_t816841679 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(FaceTextureContainer_t816841679_StaticFields, ___Instance_4)); }
	inline FaceTextureContainer_t816841679 * get_Instance_4() const { return ___Instance_4; }
	inline FaceTextureContainer_t816841679 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(FaceTextureContainer_t816841679 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACETEXTURECONTAINER_T816841679_H
#ifndef FOLLOWTARGETTRANSFORM_T402793746_H
#define FOLLOWTARGETTRANSFORM_T402793746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTargetTransform
struct  FollowTargetTransform_t402793746  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FollowTargetTransform::_target
	Transform_t3600365921 * ____target_4;

public:
	inline static int32_t get_offset_of__target_4() { return static_cast<int32_t>(offsetof(FollowTargetTransform_t402793746, ____target_4)); }
	inline Transform_t3600365921 * get__target_4() const { return ____target_4; }
	inline Transform_t3600365921 ** get_address_of__target_4() { return &____target_4; }
	inline void set__target_4(Transform_t3600365921 * value)
	{
		____target_4 = value;
		Il2CppCodeGenWriteBarrier((&____target_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGETTRANSFORM_T402793746_H
#ifndef LOTTERYWEBAPI_T3108453562_H
#define LOTTERYWEBAPI_T3108453562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LotteryWebApi
struct  LotteryWebApi_t3108453562  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField LotteryWebApi::nameInput
	InputField_t3762917431 * ___nameInput_5;
	// UnityEngine.UI.InputField LotteryWebApi::phoneInput
	InputField_t3762917431 * ___phoneInput_6;
	// UnityEngine.UI.InputField LotteryWebApi::addressInput
	InputField_t3762917431 * ___addressInput_7;
	// UnityEngine.Animator LotteryWebApi::alertAnimationObj
	Animator_t434523843 * ___alertAnimationObj_8;
	// UnityEngine.GameObject LotteryWebApi::uploadingTextObj
	GameObject_t1113636619 * ___uploadingTextObj_9;
	// UnityEngine.GameObject LotteryWebApi::uploadOkTextObj
	GameObject_t1113636619 * ___uploadOkTextObj_10;

public:
	inline static int32_t get_offset_of_nameInput_5() { return static_cast<int32_t>(offsetof(LotteryWebApi_t3108453562, ___nameInput_5)); }
	inline InputField_t3762917431 * get_nameInput_5() const { return ___nameInput_5; }
	inline InputField_t3762917431 ** get_address_of_nameInput_5() { return &___nameInput_5; }
	inline void set_nameInput_5(InputField_t3762917431 * value)
	{
		___nameInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameInput_5), value);
	}

	inline static int32_t get_offset_of_phoneInput_6() { return static_cast<int32_t>(offsetof(LotteryWebApi_t3108453562, ___phoneInput_6)); }
	inline InputField_t3762917431 * get_phoneInput_6() const { return ___phoneInput_6; }
	inline InputField_t3762917431 ** get_address_of_phoneInput_6() { return &___phoneInput_6; }
	inline void set_phoneInput_6(InputField_t3762917431 * value)
	{
		___phoneInput_6 = value;
		Il2CppCodeGenWriteBarrier((&___phoneInput_6), value);
	}

	inline static int32_t get_offset_of_addressInput_7() { return static_cast<int32_t>(offsetof(LotteryWebApi_t3108453562, ___addressInput_7)); }
	inline InputField_t3762917431 * get_addressInput_7() const { return ___addressInput_7; }
	inline InputField_t3762917431 ** get_address_of_addressInput_7() { return &___addressInput_7; }
	inline void set_addressInput_7(InputField_t3762917431 * value)
	{
		___addressInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___addressInput_7), value);
	}

	inline static int32_t get_offset_of_alertAnimationObj_8() { return static_cast<int32_t>(offsetof(LotteryWebApi_t3108453562, ___alertAnimationObj_8)); }
	inline Animator_t434523843 * get_alertAnimationObj_8() const { return ___alertAnimationObj_8; }
	inline Animator_t434523843 ** get_address_of_alertAnimationObj_8() { return &___alertAnimationObj_8; }
	inline void set_alertAnimationObj_8(Animator_t434523843 * value)
	{
		___alertAnimationObj_8 = value;
		Il2CppCodeGenWriteBarrier((&___alertAnimationObj_8), value);
	}

	inline static int32_t get_offset_of_uploadingTextObj_9() { return static_cast<int32_t>(offsetof(LotteryWebApi_t3108453562, ___uploadingTextObj_9)); }
	inline GameObject_t1113636619 * get_uploadingTextObj_9() const { return ___uploadingTextObj_9; }
	inline GameObject_t1113636619 ** get_address_of_uploadingTextObj_9() { return &___uploadingTextObj_9; }
	inline void set_uploadingTextObj_9(GameObject_t1113636619 * value)
	{
		___uploadingTextObj_9 = value;
		Il2CppCodeGenWriteBarrier((&___uploadingTextObj_9), value);
	}

	inline static int32_t get_offset_of_uploadOkTextObj_10() { return static_cast<int32_t>(offsetof(LotteryWebApi_t3108453562, ___uploadOkTextObj_10)); }
	inline GameObject_t1113636619 * get_uploadOkTextObj_10() const { return ___uploadOkTextObj_10; }
	inline GameObject_t1113636619 ** get_address_of_uploadOkTextObj_10() { return &___uploadOkTextObj_10; }
	inline void set_uploadOkTextObj_10(GameObject_t1113636619 * value)
	{
		___uploadOkTextObj_10 = value;
		Il2CppCodeGenWriteBarrier((&___uploadOkTextObj_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOTTERYWEBAPI_T3108453562_H
#ifndef MAINDATACONTAINER_T187929543_H
#define MAINDATACONTAINER_T187929543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainDataContainer
struct  MainDataContainer_t187929543  : public MonoBehaviour_t3962482529
{
public:
	// UserSex MainDataContainer::userSex
	int32_t ___userSex_5;
	// UnityEngine.GameObject MainDataContainer::mainCharacter
	GameObject_t1113636619 * ___mainCharacter_6;

public:
	inline static int32_t get_offset_of_userSex_5() { return static_cast<int32_t>(offsetof(MainDataContainer_t187929543, ___userSex_5)); }
	inline int32_t get_userSex_5() const { return ___userSex_5; }
	inline int32_t* get_address_of_userSex_5() { return &___userSex_5; }
	inline void set_userSex_5(int32_t value)
	{
		___userSex_5 = value;
	}

	inline static int32_t get_offset_of_mainCharacter_6() { return static_cast<int32_t>(offsetof(MainDataContainer_t187929543, ___mainCharacter_6)); }
	inline GameObject_t1113636619 * get_mainCharacter_6() const { return ___mainCharacter_6; }
	inline GameObject_t1113636619 ** get_address_of_mainCharacter_6() { return &___mainCharacter_6; }
	inline void set_mainCharacter_6(GameObject_t1113636619 * value)
	{
		___mainCharacter_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainCharacter_6), value);
	}
};

struct MainDataContainer_t187929543_StaticFields
{
public:
	// MainDataContainer MainDataContainer::Instance
	MainDataContainer_t187929543 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(MainDataContainer_t187929543_StaticFields, ___Instance_4)); }
	inline MainDataContainer_t187929543 * get_Instance_4() const { return ___Instance_4; }
	inline MainDataContainer_t187929543 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(MainDataContainer_t187929543 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINDATACONTAINER_T187929543_H
#ifndef PHOTOOBJBEHAVIOR_T3978052948_H
#define PHOTOOBJBEHAVIOR_T3978052948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotoObjBehavior
struct  PhotoObjBehavior_t3978052948  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.RawImage PhotoObjBehavior::photoImg
	RawImage_t3182918964 * ___photoImg_4;

public:
	inline static int32_t get_offset_of_photoImg_4() { return static_cast<int32_t>(offsetof(PhotoObjBehavior_t3978052948, ___photoImg_4)); }
	inline RawImage_t3182918964 * get_photoImg_4() const { return ___photoImg_4; }
	inline RawImage_t3182918964 ** get_address_of_photoImg_4() { return &___photoImg_4; }
	inline void set_photoImg_4(RawImage_t3182918964 * value)
	{
		___photoImg_4 = value;
		Il2CppCodeGenWriteBarrier((&___photoImg_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOOBJBEHAVIOR_T3978052948_H
#ifndef PHOTOPICKARROW_T727380732_H
#define PHOTOPICKARROW_T727380732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotoPickArrow
struct  PhotoPickArrow_t727380732  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PhotoPickArrow::eventCatcher
	GameObject_t1113636619 * ___eventCatcher_4;
	// System.String PhotoPickArrow::eventName
	String_t* ___eventName_5;

public:
	inline static int32_t get_offset_of_eventCatcher_4() { return static_cast<int32_t>(offsetof(PhotoPickArrow_t727380732, ___eventCatcher_4)); }
	inline GameObject_t1113636619 * get_eventCatcher_4() const { return ___eventCatcher_4; }
	inline GameObject_t1113636619 ** get_address_of_eventCatcher_4() { return &___eventCatcher_4; }
	inline void set_eventCatcher_4(GameObject_t1113636619 * value)
	{
		___eventCatcher_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventCatcher_4), value);
	}

	inline static int32_t get_offset_of_eventName_5() { return static_cast<int32_t>(offsetof(PhotoPickArrow_t727380732, ___eventName_5)); }
	inline String_t* get_eventName_5() const { return ___eventName_5; }
	inline String_t** get_address_of_eventName_5() { return &___eventName_5; }
	inline void set_eventName_5(String_t* value)
	{
		___eventName_5 = value;
		Il2CppCodeGenWriteBarrier((&___eventName_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOPICKARROW_T727380732_H
#ifndef PHOTOPICKERANIMATIONMOVERBEHAVIOR_T470882942_H
#define PHOTOPICKERANIMATIONMOVERBEHAVIOR_T470882942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotoPickerAnimationMoverBehavior
struct  PhotoPickerAnimationMoverBehavior_t470882942  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PhotoPickerAnimationMoverBehavior::sortingEventCatcher
	GameObject_t1113636619 * ___sortingEventCatcher_4;

public:
	inline static int32_t get_offset_of_sortingEventCatcher_4() { return static_cast<int32_t>(offsetof(PhotoPickerAnimationMoverBehavior_t470882942, ___sortingEventCatcher_4)); }
	inline GameObject_t1113636619 * get_sortingEventCatcher_4() const { return ___sortingEventCatcher_4; }
	inline GameObject_t1113636619 ** get_address_of_sortingEventCatcher_4() { return &___sortingEventCatcher_4; }
	inline void set_sortingEventCatcher_4(GameObject_t1113636619 * value)
	{
		___sortingEventCatcher_4 = value;
		Il2CppCodeGenWriteBarrier((&___sortingEventCatcher_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOPICKERANIMATIONMOVERBEHAVIOR_T470882942_H
#ifndef RANDOMAWARDCONTROLLER_T1714827868_H
#define RANDOMAWARDCONTROLLER_T1714827868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomAwardController
struct  RandomAwardController_t1714827868  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer RandomAwardController::letterAwardDisplayMesh
	MeshRenderer_t587009260 * ___letterAwardDisplayMesh_4;
	// UnityEngine.MeshRenderer RandomAwardController::letterAwardDescriptionMesh
	MeshRenderer_t587009260 * ___letterAwardDescriptionMesh_5;
	// UnityEngine.Texture2D[] RandomAwardController::awardTitleImgs
	Texture2DU5BU5D_t149664596* ___awardTitleImgs_6;
	// UnityEngine.Texture2D[] RandomAwardController::awardDescImgs
	Texture2DU5BU5D_t149664596* ___awardDescImgs_7;
	// UnityEngine.GameObject[] RandomAwardController::awardCharacterBoys
	GameObjectU5BU5D_t3328599146* ___awardCharacterBoys_8;
	// UnityEngine.GameObject[] RandomAwardController::awardCharacterGirls
	GameObjectU5BU5D_t3328599146* ___awardCharacterGirls_9;

public:
	inline static int32_t get_offset_of_letterAwardDisplayMesh_4() { return static_cast<int32_t>(offsetof(RandomAwardController_t1714827868, ___letterAwardDisplayMesh_4)); }
	inline MeshRenderer_t587009260 * get_letterAwardDisplayMesh_4() const { return ___letterAwardDisplayMesh_4; }
	inline MeshRenderer_t587009260 ** get_address_of_letterAwardDisplayMesh_4() { return &___letterAwardDisplayMesh_4; }
	inline void set_letterAwardDisplayMesh_4(MeshRenderer_t587009260 * value)
	{
		___letterAwardDisplayMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___letterAwardDisplayMesh_4), value);
	}

	inline static int32_t get_offset_of_letterAwardDescriptionMesh_5() { return static_cast<int32_t>(offsetof(RandomAwardController_t1714827868, ___letterAwardDescriptionMesh_5)); }
	inline MeshRenderer_t587009260 * get_letterAwardDescriptionMesh_5() const { return ___letterAwardDescriptionMesh_5; }
	inline MeshRenderer_t587009260 ** get_address_of_letterAwardDescriptionMesh_5() { return &___letterAwardDescriptionMesh_5; }
	inline void set_letterAwardDescriptionMesh_5(MeshRenderer_t587009260 * value)
	{
		___letterAwardDescriptionMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___letterAwardDescriptionMesh_5), value);
	}

	inline static int32_t get_offset_of_awardTitleImgs_6() { return static_cast<int32_t>(offsetof(RandomAwardController_t1714827868, ___awardTitleImgs_6)); }
	inline Texture2DU5BU5D_t149664596* get_awardTitleImgs_6() const { return ___awardTitleImgs_6; }
	inline Texture2DU5BU5D_t149664596** get_address_of_awardTitleImgs_6() { return &___awardTitleImgs_6; }
	inline void set_awardTitleImgs_6(Texture2DU5BU5D_t149664596* value)
	{
		___awardTitleImgs_6 = value;
		Il2CppCodeGenWriteBarrier((&___awardTitleImgs_6), value);
	}

	inline static int32_t get_offset_of_awardDescImgs_7() { return static_cast<int32_t>(offsetof(RandomAwardController_t1714827868, ___awardDescImgs_7)); }
	inline Texture2DU5BU5D_t149664596* get_awardDescImgs_7() const { return ___awardDescImgs_7; }
	inline Texture2DU5BU5D_t149664596** get_address_of_awardDescImgs_7() { return &___awardDescImgs_7; }
	inline void set_awardDescImgs_7(Texture2DU5BU5D_t149664596* value)
	{
		___awardDescImgs_7 = value;
		Il2CppCodeGenWriteBarrier((&___awardDescImgs_7), value);
	}

	inline static int32_t get_offset_of_awardCharacterBoys_8() { return static_cast<int32_t>(offsetof(RandomAwardController_t1714827868, ___awardCharacterBoys_8)); }
	inline GameObjectU5BU5D_t3328599146* get_awardCharacterBoys_8() const { return ___awardCharacterBoys_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_awardCharacterBoys_8() { return &___awardCharacterBoys_8; }
	inline void set_awardCharacterBoys_8(GameObjectU5BU5D_t3328599146* value)
	{
		___awardCharacterBoys_8 = value;
		Il2CppCodeGenWriteBarrier((&___awardCharacterBoys_8), value);
	}

	inline static int32_t get_offset_of_awardCharacterGirls_9() { return static_cast<int32_t>(offsetof(RandomAwardController_t1714827868, ___awardCharacterGirls_9)); }
	inline GameObjectU5BU5D_t3328599146* get_awardCharacterGirls_9() const { return ___awardCharacterGirls_9; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_awardCharacterGirls_9() { return &___awardCharacterGirls_9; }
	inline void set_awardCharacterGirls_9(GameObjectU5BU5D_t3328599146* value)
	{
		___awardCharacterGirls_9 = value;
		Il2CppCodeGenWriteBarrier((&___awardCharacterGirls_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMAWARDCONTROLLER_T1714827868_H
#ifndef RANDOMPICKER_T1486247225_H
#define RANDOMPICKER_T1486247225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomPicker
struct  RandomPicker_t1486247225  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] RandomPicker::targetObjs
	GameObjectU5BU5D_t3328599146* ___targetObjs_4;

public:
	inline static int32_t get_offset_of_targetObjs_4() { return static_cast<int32_t>(offsetof(RandomPicker_t1486247225, ___targetObjs_4)); }
	inline GameObjectU5BU5D_t3328599146* get_targetObjs_4() const { return ___targetObjs_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_targetObjs_4() { return &___targetObjs_4; }
	inline void set_targetObjs_4(GameObjectU5BU5D_t3328599146* value)
	{
		___targetObjs_4 = value;
		Il2CppCodeGenWriteBarrier((&___targetObjs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMPICKER_T1486247225_H
#ifndef SAVERENDERTEXTURE_T2520455591_H
#define SAVERENDERTEXTURE_T2520455591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveRenderTexture
struct  SaveRenderTexture_t2520455591  : public MonoBehaviour_t3962482529
{
public:
	// System.String SaveRenderTexture::imageName
	String_t* ___imageName_4;
	// System.String SaveRenderTexture::saveFolder
	String_t* ___saveFolder_5;
	// SaveFolderType SaveRenderTexture::folderType
	int32_t ___folderType_6;
	// UnityEngine.TextureFormat SaveRenderTexture::saveFormat
	int32_t ___saveFormat_7;
	// UnityEngine.RenderTexture SaveRenderTexture::targetRenderTexture
	RenderTexture_t2108887433 * ___targetRenderTexture_8;
	// UnityEngine.KeyCode SaveRenderTexture::saveKey
	int32_t ___saveKey_9;

public:
	inline static int32_t get_offset_of_imageName_4() { return static_cast<int32_t>(offsetof(SaveRenderTexture_t2520455591, ___imageName_4)); }
	inline String_t* get_imageName_4() const { return ___imageName_4; }
	inline String_t** get_address_of_imageName_4() { return &___imageName_4; }
	inline void set_imageName_4(String_t* value)
	{
		___imageName_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageName_4), value);
	}

	inline static int32_t get_offset_of_saveFolder_5() { return static_cast<int32_t>(offsetof(SaveRenderTexture_t2520455591, ___saveFolder_5)); }
	inline String_t* get_saveFolder_5() const { return ___saveFolder_5; }
	inline String_t** get_address_of_saveFolder_5() { return &___saveFolder_5; }
	inline void set_saveFolder_5(String_t* value)
	{
		___saveFolder_5 = value;
		Il2CppCodeGenWriteBarrier((&___saveFolder_5), value);
	}

	inline static int32_t get_offset_of_folderType_6() { return static_cast<int32_t>(offsetof(SaveRenderTexture_t2520455591, ___folderType_6)); }
	inline int32_t get_folderType_6() const { return ___folderType_6; }
	inline int32_t* get_address_of_folderType_6() { return &___folderType_6; }
	inline void set_folderType_6(int32_t value)
	{
		___folderType_6 = value;
	}

	inline static int32_t get_offset_of_saveFormat_7() { return static_cast<int32_t>(offsetof(SaveRenderTexture_t2520455591, ___saveFormat_7)); }
	inline int32_t get_saveFormat_7() const { return ___saveFormat_7; }
	inline int32_t* get_address_of_saveFormat_7() { return &___saveFormat_7; }
	inline void set_saveFormat_7(int32_t value)
	{
		___saveFormat_7 = value;
	}

	inline static int32_t get_offset_of_targetRenderTexture_8() { return static_cast<int32_t>(offsetof(SaveRenderTexture_t2520455591, ___targetRenderTexture_8)); }
	inline RenderTexture_t2108887433 * get_targetRenderTexture_8() const { return ___targetRenderTexture_8; }
	inline RenderTexture_t2108887433 ** get_address_of_targetRenderTexture_8() { return &___targetRenderTexture_8; }
	inline void set_targetRenderTexture_8(RenderTexture_t2108887433 * value)
	{
		___targetRenderTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetRenderTexture_8), value);
	}

	inline static int32_t get_offset_of_saveKey_9() { return static_cast<int32_t>(offsetof(SaveRenderTexture_t2520455591, ___saveKey_9)); }
	inline int32_t get_saveKey_9() const { return ___saveKey_9; }
	inline int32_t* get_address_of_saveKey_9() { return &___saveKey_9; }
	inline void set_saveKey_9(int32_t value)
	{
		___saveKey_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVERENDERTEXTURE_T2520455591_H
#ifndef SETUPFACECOMBINE_T3988824787_H
#define SETUPFACECOMBINE_T3988824787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetupFaceCombine
struct  SetupFaceCombine_t3988824787  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer SetupFaceCombine::faceQuad
	MeshRenderer_t587009260 * ___faceQuad_4;

public:
	inline static int32_t get_offset_of_faceQuad_4() { return static_cast<int32_t>(offsetof(SetupFaceCombine_t3988824787, ___faceQuad_4)); }
	inline MeshRenderer_t587009260 * get_faceQuad_4() const { return ___faceQuad_4; }
	inline MeshRenderer_t587009260 ** get_address_of_faceQuad_4() { return &___faceQuad_4; }
	inline void set_faceQuad_4(MeshRenderer_t587009260 * value)
	{
		___faceQuad_4 = value;
		Il2CppCodeGenWriteBarrier((&___faceQuad_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETUPFACECOMBINE_T3988824787_H
#ifndef SHAREFBPOST_T4271702581_H
#define SHAREFBPOST_T4271702581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShareFBPost
struct  ShareFBPost_t4271702581  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREFBPOST_T4271702581_H
#ifndef STAGEJUMPANIMATION_T2635268956_H
#define STAGEJUMPANIMATION_T2635268956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StageJumpAnimation
struct  StageJumpAnimation_t2635268956  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationCurve StageJumpAnimation::offStageCurve
	AnimationCurve_t3046754366 * ___offStageCurve_4;
	// UnityEngine.AnimationCurve StageJumpAnimation::onStageCurve
	AnimationCurve_t3046754366 * ___onStageCurve_5;
	// System.Single StageJumpAnimation::jumpTime
	float ___jumpTime_6;
	// UnityEngine.Transform StageJumpAnimation::onStagePoint
	Transform_t3600365921 * ___onStagePoint_7;
	// UnityEngine.Transform StageJumpAnimation::offStagePoint
	Transform_t3600365921 * ___offStagePoint_8;

public:
	inline static int32_t get_offset_of_offStageCurve_4() { return static_cast<int32_t>(offsetof(StageJumpAnimation_t2635268956, ___offStageCurve_4)); }
	inline AnimationCurve_t3046754366 * get_offStageCurve_4() const { return ___offStageCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_offStageCurve_4() { return &___offStageCurve_4; }
	inline void set_offStageCurve_4(AnimationCurve_t3046754366 * value)
	{
		___offStageCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___offStageCurve_4), value);
	}

	inline static int32_t get_offset_of_onStageCurve_5() { return static_cast<int32_t>(offsetof(StageJumpAnimation_t2635268956, ___onStageCurve_5)); }
	inline AnimationCurve_t3046754366 * get_onStageCurve_5() const { return ___onStageCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_onStageCurve_5() { return &___onStageCurve_5; }
	inline void set_onStageCurve_5(AnimationCurve_t3046754366 * value)
	{
		___onStageCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___onStageCurve_5), value);
	}

	inline static int32_t get_offset_of_jumpTime_6() { return static_cast<int32_t>(offsetof(StageJumpAnimation_t2635268956, ___jumpTime_6)); }
	inline float get_jumpTime_6() const { return ___jumpTime_6; }
	inline float* get_address_of_jumpTime_6() { return &___jumpTime_6; }
	inline void set_jumpTime_6(float value)
	{
		___jumpTime_6 = value;
	}

	inline static int32_t get_offset_of_onStagePoint_7() { return static_cast<int32_t>(offsetof(StageJumpAnimation_t2635268956, ___onStagePoint_7)); }
	inline Transform_t3600365921 * get_onStagePoint_7() const { return ___onStagePoint_7; }
	inline Transform_t3600365921 ** get_address_of_onStagePoint_7() { return &___onStagePoint_7; }
	inline void set_onStagePoint_7(Transform_t3600365921 * value)
	{
		___onStagePoint_7 = value;
		Il2CppCodeGenWriteBarrier((&___onStagePoint_7), value);
	}

	inline static int32_t get_offset_of_offStagePoint_8() { return static_cast<int32_t>(offsetof(StageJumpAnimation_t2635268956, ___offStagePoint_8)); }
	inline Transform_t3600365921 * get_offStagePoint_8() const { return ___offStagePoint_8; }
	inline Transform_t3600365921 ** get_address_of_offStagePoint_8() { return &___offStagePoint_8; }
	inline void set_offStagePoint_8(Transform_t3600365921 * value)
	{
		___offStagePoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___offStagePoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STAGEJUMPANIMATION_T2635268956_H
#ifndef TIMELINEPLAYER_T3698074230_H
#define TIMELINEPLAYER_T3698074230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimelinePlayer
struct  TimelinePlayer_t3698074230  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Playables.PlayableDirector TimelinePlayer::_mainTimeline
	PlayableDirector_t508516997 * ____mainTimeline_4;

public:
	inline static int32_t get_offset_of__mainTimeline_4() { return static_cast<int32_t>(offsetof(TimelinePlayer_t3698074230, ____mainTimeline_4)); }
	inline PlayableDirector_t508516997 * get__mainTimeline_4() const { return ____mainTimeline_4; }
	inline PlayableDirector_t508516997 ** get_address_of__mainTimeline_4() { return &____mainTimeline_4; }
	inline void set__mainTimeline_4(PlayableDirector_t508516997 * value)
	{
		____mainTimeline_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainTimeline_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINEPLAYER_T3698074230_H
#ifndef TRACKINGEVENTHANDLER_T2200158480_H
#define TRACKINGEVENTHANDLER_T2200158480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackingEventHandler
struct  TrackingEventHandler_t2200158480  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TrackingEventHandler::arCamObj
	GameObject_t1113636619 * ___arCamObj_4;
	// UnityEngine.Playables.PlayableDirector TrackingEventHandler::_mainTimeline
	PlayableDirector_t508516997 * ____mainTimeline_5;
	// UnityEngine.GameObject TrackingEventHandler::arSet
	GameObject_t1113636619 * ___arSet_6;
	// System.Boolean TrackingEventHandler::canPlay
	bool ___canPlay_7;
	// System.Boolean TrackingEventHandler::isFirstPlay
	bool ___isFirstPlay_8;
	// System.Boolean TrackingEventHandler::isArOn
	bool ___isArOn_9;

public:
	inline static int32_t get_offset_of_arCamObj_4() { return static_cast<int32_t>(offsetof(TrackingEventHandler_t2200158480, ___arCamObj_4)); }
	inline GameObject_t1113636619 * get_arCamObj_4() const { return ___arCamObj_4; }
	inline GameObject_t1113636619 ** get_address_of_arCamObj_4() { return &___arCamObj_4; }
	inline void set_arCamObj_4(GameObject_t1113636619 * value)
	{
		___arCamObj_4 = value;
		Il2CppCodeGenWriteBarrier((&___arCamObj_4), value);
	}

	inline static int32_t get_offset_of__mainTimeline_5() { return static_cast<int32_t>(offsetof(TrackingEventHandler_t2200158480, ____mainTimeline_5)); }
	inline PlayableDirector_t508516997 * get__mainTimeline_5() const { return ____mainTimeline_5; }
	inline PlayableDirector_t508516997 ** get_address_of__mainTimeline_5() { return &____mainTimeline_5; }
	inline void set__mainTimeline_5(PlayableDirector_t508516997 * value)
	{
		____mainTimeline_5 = value;
		Il2CppCodeGenWriteBarrier((&____mainTimeline_5), value);
	}

	inline static int32_t get_offset_of_arSet_6() { return static_cast<int32_t>(offsetof(TrackingEventHandler_t2200158480, ___arSet_6)); }
	inline GameObject_t1113636619 * get_arSet_6() const { return ___arSet_6; }
	inline GameObject_t1113636619 ** get_address_of_arSet_6() { return &___arSet_6; }
	inline void set_arSet_6(GameObject_t1113636619 * value)
	{
		___arSet_6 = value;
		Il2CppCodeGenWriteBarrier((&___arSet_6), value);
	}

	inline static int32_t get_offset_of_canPlay_7() { return static_cast<int32_t>(offsetof(TrackingEventHandler_t2200158480, ___canPlay_7)); }
	inline bool get_canPlay_7() const { return ___canPlay_7; }
	inline bool* get_address_of_canPlay_7() { return &___canPlay_7; }
	inline void set_canPlay_7(bool value)
	{
		___canPlay_7 = value;
	}

	inline static int32_t get_offset_of_isFirstPlay_8() { return static_cast<int32_t>(offsetof(TrackingEventHandler_t2200158480, ___isFirstPlay_8)); }
	inline bool get_isFirstPlay_8() const { return ___isFirstPlay_8; }
	inline bool* get_address_of_isFirstPlay_8() { return &___isFirstPlay_8; }
	inline void set_isFirstPlay_8(bool value)
	{
		___isFirstPlay_8 = value;
	}

	inline static int32_t get_offset_of_isArOn_9() { return static_cast<int32_t>(offsetof(TrackingEventHandler_t2200158480, ___isArOn_9)); }
	inline bool get_isArOn_9() const { return ___isArOn_9; }
	inline bool* get_address_of_isArOn_9() { return &___isArOn_9; }
	inline void set_isArOn_9(bool value)
	{
		___isArOn_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGEVENTHANDLER_T2200158480_H
#ifndef UIAPPLYFACETEXTURE_T2177684982_H
#define UIAPPLYFACETEXTURE_T2177684982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIApplyFaceTexture
struct  UIApplyFaceTexture_t2177684982  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.RawImage UIApplyFaceTexture::_img
	RawImage_t3182918964 * ____img_4;

public:
	inline static int32_t get_offset_of__img_4() { return static_cast<int32_t>(offsetof(UIApplyFaceTexture_t2177684982, ____img_4)); }
	inline RawImage_t3182918964 * get__img_4() const { return ____img_4; }
	inline RawImage_t3182918964 ** get_address_of__img_4() { return &____img_4; }
	inline void set__img_4(RawImage_t3182918964 * value)
	{
		____img_4 = value;
		Il2CppCodeGenWriteBarrier((&____img_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIAPPLYFACETEXTURE_T2177684982_H
#ifndef UIFSMEVENTSENDER_T3608744296_H
#define UIFSMEVENTSENDER_T3608744296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFSMEventSender
struct  UIFSMEventSender_t3608744296  : public MonoBehaviour_t3962482529
{
public:
	// PlayMakerFSM UIFSMEventSender::targetFSM
	PlayMakerFSM_t1613010231 * ___targetFSM_5;

public:
	inline static int32_t get_offset_of_targetFSM_5() { return static_cast<int32_t>(offsetof(UIFSMEventSender_t3608744296, ___targetFSM_5)); }
	inline PlayMakerFSM_t1613010231 * get_targetFSM_5() const { return ___targetFSM_5; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_targetFSM_5() { return &___targetFSM_5; }
	inline void set_targetFSM_5(PlayMakerFSM_t1613010231 * value)
	{
		___targetFSM_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetFSM_5), value);
	}
};

struct UIFSMEventSender_t3608744296_StaticFields
{
public:
	// UIFSMEventSender UIFSMEventSender::Instance
	UIFSMEventSender_t3608744296 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(UIFSMEventSender_t3608744296_StaticFields, ___Instance_4)); }
	inline UIFSMEventSender_t3608744296 * get_Instance_4() const { return ___Instance_4; }
	inline UIFSMEventSender_t3608744296 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(UIFSMEventSender_t3608744296 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFSMEVENTSENDER_T3608744296_H
#ifndef UIFACESETTINGANIMATIONBYSEX_T2841082276_H
#define UIFACESETTINGANIMATIONBYSEX_T2841082276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIFaceSettingAnimationBySex
struct  UIFaceSettingAnimationBySex_t2841082276  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIFACESETTINGANIMATIONBYSEX_T2841082276_H
#ifndef UIPHOTOGALLERYMANAGER_T2672000691_H
#define UIPHOTOGALLERYMANAGER_T2672000691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIPhotoGalleryManager
struct  UIPhotoGalleryManager_t2672000691  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject UIPhotoGalleryManager::photoObjPrefab
	GameObject_t1113636619 * ___photoObjPrefab_4;
	// UnityEngine.Transform UIPhotoGalleryManager::pickedPhotoArea
	Transform_t3600365921 * ___pickedPhotoArea_5;
	// UnityEngine.RectTransform UIPhotoGalleryManager::movingArea
	RectTransform_t3704657025 * ___movingArea_6;
	// UnityEngine.Transform UIPhotoGalleryManager::otherPhotoArea
	Transform_t3600365921 * ___otherPhotoArea_7;
	// UnityEngine.Texture2D[] UIPhotoGalleryManager::testPics
	Texture2DU5BU5D_t149664596* ___testPics_8;
	// UnityEngine.Texture2D[] UIPhotoGalleryManager::loadedTextures
	Texture2DU5BU5D_t149664596* ___loadedTextures_9;
	// UnityEngine.GameObject[] UIPhotoGalleryManager::photoObjs
	GameObjectU5BU5D_t3328599146* ___photoObjs_10;
	// UnityEngine.UI.Text UIPhotoGalleryManager::indexDisplayText
	Text_t1901882714 * ___indexDisplayText_11;
	// System.String[] UIPhotoGalleryManager::photoPaths
	StringU5BU5D_t1281789340* ___photoPaths_12;
	// System.Int32 UIPhotoGalleryManager::nowPickIndex
	int32_t ___nowPickIndex_13;
	// System.Boolean UIPhotoGalleryManager::canAnimate
	bool ___canAnimate_14;

public:
	inline static int32_t get_offset_of_photoObjPrefab_4() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___photoObjPrefab_4)); }
	inline GameObject_t1113636619 * get_photoObjPrefab_4() const { return ___photoObjPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_photoObjPrefab_4() { return &___photoObjPrefab_4; }
	inline void set_photoObjPrefab_4(GameObject_t1113636619 * value)
	{
		___photoObjPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___photoObjPrefab_4), value);
	}

	inline static int32_t get_offset_of_pickedPhotoArea_5() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___pickedPhotoArea_5)); }
	inline Transform_t3600365921 * get_pickedPhotoArea_5() const { return ___pickedPhotoArea_5; }
	inline Transform_t3600365921 ** get_address_of_pickedPhotoArea_5() { return &___pickedPhotoArea_5; }
	inline void set_pickedPhotoArea_5(Transform_t3600365921 * value)
	{
		___pickedPhotoArea_5 = value;
		Il2CppCodeGenWriteBarrier((&___pickedPhotoArea_5), value);
	}

	inline static int32_t get_offset_of_movingArea_6() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___movingArea_6)); }
	inline RectTransform_t3704657025 * get_movingArea_6() const { return ___movingArea_6; }
	inline RectTransform_t3704657025 ** get_address_of_movingArea_6() { return &___movingArea_6; }
	inline void set_movingArea_6(RectTransform_t3704657025 * value)
	{
		___movingArea_6 = value;
		Il2CppCodeGenWriteBarrier((&___movingArea_6), value);
	}

	inline static int32_t get_offset_of_otherPhotoArea_7() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___otherPhotoArea_7)); }
	inline Transform_t3600365921 * get_otherPhotoArea_7() const { return ___otherPhotoArea_7; }
	inline Transform_t3600365921 ** get_address_of_otherPhotoArea_7() { return &___otherPhotoArea_7; }
	inline void set_otherPhotoArea_7(Transform_t3600365921 * value)
	{
		___otherPhotoArea_7 = value;
		Il2CppCodeGenWriteBarrier((&___otherPhotoArea_7), value);
	}

	inline static int32_t get_offset_of_testPics_8() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___testPics_8)); }
	inline Texture2DU5BU5D_t149664596* get_testPics_8() const { return ___testPics_8; }
	inline Texture2DU5BU5D_t149664596** get_address_of_testPics_8() { return &___testPics_8; }
	inline void set_testPics_8(Texture2DU5BU5D_t149664596* value)
	{
		___testPics_8 = value;
		Il2CppCodeGenWriteBarrier((&___testPics_8), value);
	}

	inline static int32_t get_offset_of_loadedTextures_9() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___loadedTextures_9)); }
	inline Texture2DU5BU5D_t149664596* get_loadedTextures_9() const { return ___loadedTextures_9; }
	inline Texture2DU5BU5D_t149664596** get_address_of_loadedTextures_9() { return &___loadedTextures_9; }
	inline void set_loadedTextures_9(Texture2DU5BU5D_t149664596* value)
	{
		___loadedTextures_9 = value;
		Il2CppCodeGenWriteBarrier((&___loadedTextures_9), value);
	}

	inline static int32_t get_offset_of_photoObjs_10() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___photoObjs_10)); }
	inline GameObjectU5BU5D_t3328599146* get_photoObjs_10() const { return ___photoObjs_10; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_photoObjs_10() { return &___photoObjs_10; }
	inline void set_photoObjs_10(GameObjectU5BU5D_t3328599146* value)
	{
		___photoObjs_10 = value;
		Il2CppCodeGenWriteBarrier((&___photoObjs_10), value);
	}

	inline static int32_t get_offset_of_indexDisplayText_11() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___indexDisplayText_11)); }
	inline Text_t1901882714 * get_indexDisplayText_11() const { return ___indexDisplayText_11; }
	inline Text_t1901882714 ** get_address_of_indexDisplayText_11() { return &___indexDisplayText_11; }
	inline void set_indexDisplayText_11(Text_t1901882714 * value)
	{
		___indexDisplayText_11 = value;
		Il2CppCodeGenWriteBarrier((&___indexDisplayText_11), value);
	}

	inline static int32_t get_offset_of_photoPaths_12() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___photoPaths_12)); }
	inline StringU5BU5D_t1281789340* get_photoPaths_12() const { return ___photoPaths_12; }
	inline StringU5BU5D_t1281789340** get_address_of_photoPaths_12() { return &___photoPaths_12; }
	inline void set_photoPaths_12(StringU5BU5D_t1281789340* value)
	{
		___photoPaths_12 = value;
		Il2CppCodeGenWriteBarrier((&___photoPaths_12), value);
	}

	inline static int32_t get_offset_of_nowPickIndex_13() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___nowPickIndex_13)); }
	inline int32_t get_nowPickIndex_13() const { return ___nowPickIndex_13; }
	inline int32_t* get_address_of_nowPickIndex_13() { return &___nowPickIndex_13; }
	inline void set_nowPickIndex_13(int32_t value)
	{
		___nowPickIndex_13 = value;
	}

	inline static int32_t get_offset_of_canAnimate_14() { return static_cast<int32_t>(offsetof(UIPhotoGalleryManager_t2672000691, ___canAnimate_14)); }
	inline bool get_canAnimate_14() const { return ___canAnimate_14; }
	inline bool* get_address_of_canAnimate_14() { return &___canAnimate_14; }
	inline void set_canAnimate_14(bool value)
	{
		___canAnimate_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIPHOTOGALLERYMANAGER_T2672000691_H
#ifndef ACTIVATIONTRACK_T4003533622_H
#define ACTIVATIONTRACK_T4003533622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ActivationTrack
struct  ActivationTrack_t4003533622  : public TrackAsset_t2828708245
{
public:
	// UnityEngine.Timeline.ActivationTrack/PostPlaybackState UnityEngine.Timeline.ActivationTrack::m_PostPlaybackState
	int32_t ___m_PostPlaybackState_23;
	// UnityEngine.Timeline.ActivationMixerPlayable UnityEngine.Timeline.ActivationTrack::m_ActivationMixer
	ActivationMixerPlayable_t2212523045 * ___m_ActivationMixer_24;

public:
	inline static int32_t get_offset_of_m_PostPlaybackState_23() { return static_cast<int32_t>(offsetof(ActivationTrack_t4003533622, ___m_PostPlaybackState_23)); }
	inline int32_t get_m_PostPlaybackState_23() const { return ___m_PostPlaybackState_23; }
	inline int32_t* get_address_of_m_PostPlaybackState_23() { return &___m_PostPlaybackState_23; }
	inline void set_m_PostPlaybackState_23(int32_t value)
	{
		___m_PostPlaybackState_23 = value;
	}

	inline static int32_t get_offset_of_m_ActivationMixer_24() { return static_cast<int32_t>(offsetof(ActivationTrack_t4003533622, ___m_ActivationMixer_24)); }
	inline ActivationMixerPlayable_t2212523045 * get_m_ActivationMixer_24() const { return ___m_ActivationMixer_24; }
	inline ActivationMixerPlayable_t2212523045 ** get_address_of_m_ActivationMixer_24() { return &___m_ActivationMixer_24; }
	inline void set_m_ActivationMixer_24(ActivationMixerPlayable_t2212523045 * value)
	{
		___m_ActivationMixer_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActivationMixer_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATIONTRACK_T4003533622_H
#ifndef ANIMATIONTRACK_T3872729528_H
#define ANIMATIONTRACK_T3872729528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AnimationTrack
struct  AnimationTrack_t3872729528  : public TrackAsset_t2828708245
{
public:
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPreExtrapolation
	int32_t ___m_OpenClipPreExtrapolation_23;
	// UnityEngine.Timeline.TimelineClip/ClipExtrapolation UnityEngine.Timeline.AnimationTrack::m_OpenClipPostExtrapolation
	int32_t ___m_OpenClipPostExtrapolation_24;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetPosition
	Vector3_t3722313464  ___m_OpenClipOffsetPosition_25;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetEulerAngles
	Vector3_t3722313464  ___m_OpenClipOffsetEulerAngles_26;
	// System.Double UnityEngine.Timeline.AnimationTrack::m_OpenClipTimeOffset
	double ___m_OpenClipTimeOffset_27;
	// UnityEngine.Timeline.MatchTargetFields UnityEngine.Timeline.AnimationTrack::m_MatchTargetFields
	int32_t ___m_MatchTargetFields_28;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_Position
	Vector3_t3722313464  ___m_Position_29;
	// UnityEngine.Vector3 UnityEngine.Timeline.AnimationTrack::m_EulerAngles
	Vector3_t3722313464  ___m_EulerAngles_30;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyOffsets
	bool ___m_ApplyOffsets_31;
	// UnityEngine.AvatarMask UnityEngine.Timeline.AnimationTrack::m_AvatarMask
	AvatarMask_t1182482518 * ___m_AvatarMask_32;
	// System.Boolean UnityEngine.Timeline.AnimationTrack::m_ApplyAvatarMask
	bool ___m_ApplyAvatarMask_33;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_OpenClipOffsetRotation
	Quaternion_t2301928331  ___m_OpenClipOffsetRotation_34;
	// UnityEngine.Quaternion UnityEngine.Timeline.AnimationTrack::m_Rotation
	Quaternion_t2301928331  ___m_Rotation_35;

public:
	inline static int32_t get_offset_of_m_OpenClipPreExtrapolation_23() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipPreExtrapolation_23)); }
	inline int32_t get_m_OpenClipPreExtrapolation_23() const { return ___m_OpenClipPreExtrapolation_23; }
	inline int32_t* get_address_of_m_OpenClipPreExtrapolation_23() { return &___m_OpenClipPreExtrapolation_23; }
	inline void set_m_OpenClipPreExtrapolation_23(int32_t value)
	{
		___m_OpenClipPreExtrapolation_23 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipPostExtrapolation_24() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipPostExtrapolation_24)); }
	inline int32_t get_m_OpenClipPostExtrapolation_24() const { return ___m_OpenClipPostExtrapolation_24; }
	inline int32_t* get_address_of_m_OpenClipPostExtrapolation_24() { return &___m_OpenClipPostExtrapolation_24; }
	inline void set_m_OpenClipPostExtrapolation_24(int32_t value)
	{
		___m_OpenClipPostExtrapolation_24 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetPosition_25() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipOffsetPosition_25)); }
	inline Vector3_t3722313464  get_m_OpenClipOffsetPosition_25() const { return ___m_OpenClipOffsetPosition_25; }
	inline Vector3_t3722313464 * get_address_of_m_OpenClipOffsetPosition_25() { return &___m_OpenClipOffsetPosition_25; }
	inline void set_m_OpenClipOffsetPosition_25(Vector3_t3722313464  value)
	{
		___m_OpenClipOffsetPosition_25 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetEulerAngles_26() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipOffsetEulerAngles_26)); }
	inline Vector3_t3722313464  get_m_OpenClipOffsetEulerAngles_26() const { return ___m_OpenClipOffsetEulerAngles_26; }
	inline Vector3_t3722313464 * get_address_of_m_OpenClipOffsetEulerAngles_26() { return &___m_OpenClipOffsetEulerAngles_26; }
	inline void set_m_OpenClipOffsetEulerAngles_26(Vector3_t3722313464  value)
	{
		___m_OpenClipOffsetEulerAngles_26 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipTimeOffset_27() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipTimeOffset_27)); }
	inline double get_m_OpenClipTimeOffset_27() const { return ___m_OpenClipTimeOffset_27; }
	inline double* get_address_of_m_OpenClipTimeOffset_27() { return &___m_OpenClipTimeOffset_27; }
	inline void set_m_OpenClipTimeOffset_27(double value)
	{
		___m_OpenClipTimeOffset_27 = value;
	}

	inline static int32_t get_offset_of_m_MatchTargetFields_28() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_MatchTargetFields_28)); }
	inline int32_t get_m_MatchTargetFields_28() const { return ___m_MatchTargetFields_28; }
	inline int32_t* get_address_of_m_MatchTargetFields_28() { return &___m_MatchTargetFields_28; }
	inline void set_m_MatchTargetFields_28(int32_t value)
	{
		___m_MatchTargetFields_28 = value;
	}

	inline static int32_t get_offset_of_m_Position_29() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_Position_29)); }
	inline Vector3_t3722313464  get_m_Position_29() const { return ___m_Position_29; }
	inline Vector3_t3722313464 * get_address_of_m_Position_29() { return &___m_Position_29; }
	inline void set_m_Position_29(Vector3_t3722313464  value)
	{
		___m_Position_29 = value;
	}

	inline static int32_t get_offset_of_m_EulerAngles_30() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_EulerAngles_30)); }
	inline Vector3_t3722313464  get_m_EulerAngles_30() const { return ___m_EulerAngles_30; }
	inline Vector3_t3722313464 * get_address_of_m_EulerAngles_30() { return &___m_EulerAngles_30; }
	inline void set_m_EulerAngles_30(Vector3_t3722313464  value)
	{
		___m_EulerAngles_30 = value;
	}

	inline static int32_t get_offset_of_m_ApplyOffsets_31() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_ApplyOffsets_31)); }
	inline bool get_m_ApplyOffsets_31() const { return ___m_ApplyOffsets_31; }
	inline bool* get_address_of_m_ApplyOffsets_31() { return &___m_ApplyOffsets_31; }
	inline void set_m_ApplyOffsets_31(bool value)
	{
		___m_ApplyOffsets_31 = value;
	}

	inline static int32_t get_offset_of_m_AvatarMask_32() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_AvatarMask_32)); }
	inline AvatarMask_t1182482518 * get_m_AvatarMask_32() const { return ___m_AvatarMask_32; }
	inline AvatarMask_t1182482518 ** get_address_of_m_AvatarMask_32() { return &___m_AvatarMask_32; }
	inline void set_m_AvatarMask_32(AvatarMask_t1182482518 * value)
	{
		___m_AvatarMask_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_AvatarMask_32), value);
	}

	inline static int32_t get_offset_of_m_ApplyAvatarMask_33() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_ApplyAvatarMask_33)); }
	inline bool get_m_ApplyAvatarMask_33() const { return ___m_ApplyAvatarMask_33; }
	inline bool* get_address_of_m_ApplyAvatarMask_33() { return &___m_ApplyAvatarMask_33; }
	inline void set_m_ApplyAvatarMask_33(bool value)
	{
		___m_ApplyAvatarMask_33 = value;
	}

	inline static int32_t get_offset_of_m_OpenClipOffsetRotation_34() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_OpenClipOffsetRotation_34)); }
	inline Quaternion_t2301928331  get_m_OpenClipOffsetRotation_34() const { return ___m_OpenClipOffsetRotation_34; }
	inline Quaternion_t2301928331 * get_address_of_m_OpenClipOffsetRotation_34() { return &___m_OpenClipOffsetRotation_34; }
	inline void set_m_OpenClipOffsetRotation_34(Quaternion_t2301928331  value)
	{
		___m_OpenClipOffsetRotation_34 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_35() { return static_cast<int32_t>(offsetof(AnimationTrack_t3872729528, ___m_Rotation_35)); }
	inline Quaternion_t2301928331  get_m_Rotation_35() const { return ___m_Rotation_35; }
	inline Quaternion_t2301928331 * get_address_of_m_Rotation_35() { return &___m_Rotation_35; }
	inline void set_m_Rotation_35(Quaternion_t2301928331  value)
	{
		___m_Rotation_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRACK_T3872729528_H
#ifndef AUDIOTRACK_T1549411460_H
#define AUDIOTRACK_T1549411460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.AudioTrack
struct  AudioTrack_t1549411460  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOTRACK_T1549411460_H
#ifndef CONTROLTRACK_T3308451304_H
#define CONTROLTRACK_T3308451304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.ControlTrack
struct  ControlTrack_t3308451304  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLTRACK_T3308451304_H
#ifndef PLAYABLETRACK_T2508018810_H
#define PLAYABLETRACK_T2508018810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Timeline.PlayableTrack
struct  PlayableTrack_t2508018810  : public TrackAsset_t2828708245
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLETRACK_T2508018810_H
#ifndef UPLOADPHOTOPICKER_T3945345849_H
#define UPLOADPHOTOPICKER_T3945345849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UploadPhotoPicker
struct  UploadPhotoPicker_t3945345849  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.RawImage UploadPhotoPicker::pickerUiPhotoDisplayImg
	RawImage_t3182918964 * ___pickerUiPhotoDisplayImg_4;
	// UnityEngine.Texture2D[] UploadPhotoPicker::loadedTextures
	Texture2DU5BU5D_t149664596* ___loadedTextures_5;
	// System.String[] UploadPhotoPicker::photoPaths
	StringU5BU5D_t1281789340* ___photoPaths_6;
	// System.Int32 UploadPhotoPicker::nowPickIndex
	int32_t ___nowPickIndex_7;

public:
	inline static int32_t get_offset_of_pickerUiPhotoDisplayImg_4() { return static_cast<int32_t>(offsetof(UploadPhotoPicker_t3945345849, ___pickerUiPhotoDisplayImg_4)); }
	inline RawImage_t3182918964 * get_pickerUiPhotoDisplayImg_4() const { return ___pickerUiPhotoDisplayImg_4; }
	inline RawImage_t3182918964 ** get_address_of_pickerUiPhotoDisplayImg_4() { return &___pickerUiPhotoDisplayImg_4; }
	inline void set_pickerUiPhotoDisplayImg_4(RawImage_t3182918964 * value)
	{
		___pickerUiPhotoDisplayImg_4 = value;
		Il2CppCodeGenWriteBarrier((&___pickerUiPhotoDisplayImg_4), value);
	}

	inline static int32_t get_offset_of_loadedTextures_5() { return static_cast<int32_t>(offsetof(UploadPhotoPicker_t3945345849, ___loadedTextures_5)); }
	inline Texture2DU5BU5D_t149664596* get_loadedTextures_5() const { return ___loadedTextures_5; }
	inline Texture2DU5BU5D_t149664596** get_address_of_loadedTextures_5() { return &___loadedTextures_5; }
	inline void set_loadedTextures_5(Texture2DU5BU5D_t149664596* value)
	{
		___loadedTextures_5 = value;
		Il2CppCodeGenWriteBarrier((&___loadedTextures_5), value);
	}

	inline static int32_t get_offset_of_photoPaths_6() { return static_cast<int32_t>(offsetof(UploadPhotoPicker_t3945345849, ___photoPaths_6)); }
	inline StringU5BU5D_t1281789340* get_photoPaths_6() const { return ___photoPaths_6; }
	inline StringU5BU5D_t1281789340** get_address_of_photoPaths_6() { return &___photoPaths_6; }
	inline void set_photoPaths_6(StringU5BU5D_t1281789340* value)
	{
		___photoPaths_6 = value;
		Il2CppCodeGenWriteBarrier((&___photoPaths_6), value);
	}

	inline static int32_t get_offset_of_nowPickIndex_7() { return static_cast<int32_t>(offsetof(UploadPhotoPicker_t3945345849, ___nowPickIndex_7)); }
	inline int32_t get_nowPickIndex_7() const { return ___nowPickIndex_7; }
	inline int32_t* get_address_of_nowPickIndex_7() { return &___nowPickIndex_7; }
	inline void set_nowPickIndex_7(int32_t value)
	{
		___nowPickIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPHOTOPICKER_T3945345849_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (TimelineMarker_t2195886951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[4] = 
{
	TimelineMarker_t2195886951::get_offset_of_m_Key_0(),
	TimelineMarker_t2195886951::get_offset_of_m_ParentTrack_1(),
	TimelineMarker_t2195886951::get_offset_of_m_Time_2(),
	TimelineMarker_t2195886951::get_offset_of_m_Selected_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (TimelinePlayable_t2938744123), -1, sizeof(TimelinePlayable_t2938744123_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2802[7] = 
{
	TimelinePlayable_t2938744123::get_offset_of_m_IntervalTree_0(),
	TimelinePlayable_t2938744123::get_offset_of_m_ActiveClips_1(),
	TimelinePlayable_t2938744123::get_offset_of_m_CurrentListOfActiveClips_2(),
	TimelinePlayable_t2938744123::get_offset_of_m_ActiveBit_3(),
	TimelinePlayable_t2938744123::get_offset_of_m_EvaluateCallbacks_4(),
	TimelinePlayable_t2938744123::get_offset_of_m_PlayableCache_5(),
	TimelinePlayable_t2938744123_StaticFields::get_offset_of_muteAudioScrubbing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (TrackAsset_t2828708245), -1, sizeof(TrackAsset_t2828708245_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2803[19] = 
{
	TrackAsset_t2828708245::get_offset_of_m_Locked_4(),
	TrackAsset_t2828708245::get_offset_of_m_Muted_5(),
	TrackAsset_t2828708245::get_offset_of_m_CustomPlayableFullTypename_6(),
	TrackAsset_t2828708245::get_offset_of_m_AnimClip_7(),
	TrackAsset_t2828708245::get_offset_of_m_Parent_8(),
	TrackAsset_t2828708245::get_offset_of_m_Children_9(),
	TrackAsset_t2828708245::get_offset_of_m_ItemsHash_10(),
	TrackAsset_t2828708245::get_offset_of_m_ClipsCache_11(),
	TrackAsset_t2828708245::get_offset_of_m_Start_12(),
	TrackAsset_t2828708245::get_offset_of_m_End_13(),
	TrackAsset_t2828708245::get_offset_of_m_CacheSorted_14(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_s_EmptyCache_15(),
	TrackAsset_t2828708245::get_offset_of_m_ChildTrackCache_16(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_s_TrackBindingTypeAttributeCache_17(),
	TrackAsset_t2828708245::get_offset_of_m_Clips_18(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_OnPlayableCreate_19(),
	0,
	TrackAsset_t2828708245::get_offset_of_m_Version_21(),
	TrackAsset_t2828708245_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (Versions_t3704484929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2804[3] = 
{
	Versions_t3704484929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (TrackAssetUpgrade_t4097356561), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (U3CU3Ec__Iterator0_t3478306766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[6] = 
{
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U3CattributeU3E__0_0(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U3CtrackBindingTypeU3E__0_1(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t3478306766::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (ActivationMixerPlayable_t2212523045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[3] = 
{
	ActivationMixerPlayable_t2212523045::get_offset_of_m_PostPlaybackState_0(),
	ActivationMixerPlayable_t2212523045::get_offset_of_m_BoundGameObjectInitialStateIsActive_1(),
	ActivationMixerPlayable_t2212523045::get_offset_of_m_BoundGameObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (ActivationPlayableAsset_t2332007632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (ActivationTrack_t4003533622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[2] = 
{
	ActivationTrack_t4003533622::get_offset_of_m_PostPlaybackState_23(),
	ActivationTrack_t4003533622::get_offset_of_m_ActivationMixer_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (PostPlaybackState_t4203018085)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2810[5] = 
{
	PostPlaybackState_t4203018085::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (AnimationOutputWeightProcessor_t2670190175), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[3] = 
{
	AnimationOutputWeightProcessor_t2670190175::get_offset_of_m_Output_0(),
	AnimationOutputWeightProcessor_t2670190175::get_offset_of_m_LayerMixer_1(),
	AnimationOutputWeightProcessor_t2670190175::get_offset_of_m_Mixers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (WeightInfo_t1982677921)+ sizeof (RuntimeObject), sizeof(WeightInfo_t1982677921_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2812[4] = 
{
	WeightInfo_t1982677921::get_offset_of_mixer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t1982677921::get_offset_of_parentMixer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t1982677921::get_offset_of_port_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WeightInfo_t1982677921::get_offset_of_modulate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (AnimationPlayableAsset_t734882934), -1, sizeof(AnimationPlayableAsset_t734882934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2813[9] = 
{
	AnimationPlayableAsset_t734882934::get_offset_of_m_Clip_4(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_Position_5(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_EulerAngles_6(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_UseTrackMatchFields_7(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_MatchTargetFields_8(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_RemoveStartOffset_9(),
	AnimationPlayableAsset_t734882934_StaticFields::get_offset_of_k_LatestVersion_10(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_Version_11(),
	AnimationPlayableAsset_t734882934::get_offset_of_m_Rotation_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (Versions_t439386478)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2814[3] = 
{
	Versions_t439386478::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (AnimationPlayableAssetUpgrade_t3688040745), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (U3CU3Ec__Iterator0_t4165699274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[4] = 
{
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t4165699274::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (MatchTargetFields_t3959953937)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2817[7] = 
{
	MatchTargetFields_t3959953937::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (MatchTargetFieldConstants_t2682568617), -1, sizeof(MatchTargetFieldConstants_t2682568617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2818[4] = 
{
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_All_0(),
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_None_1(),
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_Position_2(),
	MatchTargetFieldConstants_t2682568617_StaticFields::get_offset_of_Rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (AnimationTrack_t3872729528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[13] = 
{
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipPreExtrapolation_23(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipPostExtrapolation_24(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipOffsetPosition_25(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipOffsetEulerAngles_26(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipTimeOffset_27(),
	AnimationTrack_t3872729528::get_offset_of_m_MatchTargetFields_28(),
	AnimationTrack_t3872729528::get_offset_of_m_Position_29(),
	AnimationTrack_t3872729528::get_offset_of_m_EulerAngles_30(),
	AnimationTrack_t3872729528::get_offset_of_m_ApplyOffsets_31(),
	AnimationTrack_t3872729528::get_offset_of_m_AvatarMask_32(),
	AnimationTrack_t3872729528::get_offset_of_m_ApplyAvatarMask_33(),
	AnimationTrack_t3872729528::get_offset_of_m_OpenClipOffsetRotation_34(),
	AnimationTrack_t3872729528::get_offset_of_m_Rotation_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (AnimationTrackUpgrade_t2920516161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (U3CU3Ec__Iterator0_t1104488729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[4] = 
{
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t1104488729::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (TrackColorAttribute_t2898993029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[1] = 
{
	TrackColorAttribute_t2898993029::get_offset_of_m_Color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (AudioPlayableAsset_t2575444864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[3] = 
{
	AudioPlayableAsset_t2575444864::get_offset_of_m_Clip_4(),
	AudioPlayableAsset_t2575444864::get_offset_of_m_Loop_5(),
	AudioPlayableAsset_t2575444864::get_offset_of_m_bufferingTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (U3CU3Ec__Iterator0_t2611944586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t2611944586::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (AudioTrack_t1549411460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (U3CU3Ec__Iterator0_t717920108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[4] = 
{
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t717920108::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (ControlPlayableAsset_t3597738600), -1, sizeof(ControlPlayableAsset_t3597738600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2827[17] = 
{
	0,
	ControlPlayableAsset_t3597738600_StaticFields::get_offset_of_k_EmptyDirectorsList_5(),
	ControlPlayableAsset_t3597738600_StaticFields::get_offset_of_k_EmptyParticlesList_6(),
	ControlPlayableAsset_t3597738600::get_offset_of_sourceGameObject_7(),
	ControlPlayableAsset_t3597738600::get_offset_of_prefabGameObject_8(),
	ControlPlayableAsset_t3597738600::get_offset_of_updateParticle_9(),
	ControlPlayableAsset_t3597738600::get_offset_of_particleRandomSeed_10(),
	ControlPlayableAsset_t3597738600::get_offset_of_updateDirector_11(),
	ControlPlayableAsset_t3597738600::get_offset_of_updateITimeControl_12(),
	ControlPlayableAsset_t3597738600::get_offset_of_searchHierarchy_13(),
	ControlPlayableAsset_t3597738600::get_offset_of_active_14(),
	ControlPlayableAsset_t3597738600::get_offset_of_postPlayback_15(),
	ControlPlayableAsset_t3597738600::get_offset_of_m_ControlDirectorAsset_16(),
	ControlPlayableAsset_t3597738600::get_offset_of_m_Duration_17(),
	ControlPlayableAsset_t3597738600::get_offset_of_m_SupportLoop_18(),
	ControlPlayableAsset_t3597738600_StaticFields::get_offset_of_s_ProcessedDirectors_19(),
	ControlPlayableAsset_t3597738600_StaticFields::get_offset_of_s_CreatedPrefabs_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (U3CGetControlableScriptsU3Ec__Iterator0_t3018919025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[7] = 
{
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_root_0(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24locvar0_1(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24locvar1_2(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U3CscriptU3E__1_3(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24current_4(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24disposing_5(),
	U3CGetControlableScriptsU3Ec__Iterator0_t3018919025::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (ControlTrack_t3308451304), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (InfiniteRuntimeClip_t156380358), -1, sizeof(InfiniteRuntimeClip_t156380358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2830[2] = 
{
	InfiniteRuntimeClip_t156380358::get_offset_of_m_Playable_1(),
	InfiniteRuntimeClip_t156380358_StaticFields::get_offset_of_kIntervalEnd_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (RuntimeClip_t583365242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[3] = 
{
	RuntimeClip_t583365242::get_offset_of_m_Clip_1(),
	RuntimeClip_t583365242::get_offset_of_m_Playable_2(),
	RuntimeClip_t583365242::get_offset_of_m_ParentMixer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (RuntimeClipBase_t197358283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (RuntimeElement_t2068542217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[1] = 
{
	RuntimeElement_t2068542217::get_offset_of_U3CintervalBitU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (ScheduleRuntimeClip_t283756268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[6] = 
{
	ScheduleRuntimeClip_t283756268::get_offset_of_m_Clip_1(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_Playable_2(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_ParentMixer_3(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_StartDelay_4(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_FinishTail_5(),
	ScheduleRuntimeClip_t283756268::get_offset_of_m_Started_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (TrackAssetExtensions_t812533753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (ActivationControlPlayable_t426148305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[3] = 
{
	ActivationControlPlayable_t426148305::get_offset_of_gameObject_0(),
	ActivationControlPlayable_t426148305::get_offset_of_postPlayback_1(),
	ActivationControlPlayable_t426148305::get_offset_of_m_InitialState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (PostPlaybackState_t1986969933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2840[4] = 
{
	PostPlaybackState_t1986969933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (InitialState_t1844636896)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2841[4] = 
{
	InitialState_t1844636896::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (BasicPlayableBehaviour_t1482445671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (DirectorControlPlayable_t2642348650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[3] = 
{
	DirectorControlPlayable_t2642348650::get_offset_of_director_0(),
	DirectorControlPlayable_t2642348650::get_offset_of_m_SyncTime_1(),
	DirectorControlPlayable_t2642348650::get_offset_of_m_AssetDuration_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (EventPlayable_t1880911929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[3] = 
{
	EventPlayable_t1880911929::get_offset_of_U3CtriggerTimeU3Ek__BackingField_0(),
	EventPlayable_t1880911929::get_offset_of_m_HasFired_1(),
	EventPlayable_t1880911929::get_offset_of_m_PreviousTime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (ParticleControlPlayable_t3220249377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[5] = 
{
	0,
	ParticleControlPlayable_t3220249377::get_offset_of_m_LastTime_1(),
	ParticleControlPlayable_t3220249377::get_offset_of_m_RandomSeed_2(),
	ParticleControlPlayable_t3220249377::get_offset_of_m_SystemTime_3(),
	ParticleControlPlayable_t3220249377::get_offset_of_U3CparticleSystemU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (PrefabControlPlayable_t3058657425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[1] = 
{
	PrefabControlPlayable_t3058657425::get_offset_of_m_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (TimeControlPlayable_t1010398267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[2] = 
{
	TimeControlPlayable_t1010398267::get_offset_of_m_timeControl_0(),
	TimeControlPlayable_t1010398267::get_offset_of_m_started_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (PlayableTrack_t2508018810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (Extrapolation_t625958692), -1, sizeof(Extrapolation_t625958692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2850[2] = 
{
	Extrapolation_t625958692_StaticFields::get_offset_of_kMinExtrapolationTime_0(),
	Extrapolation_t625958692_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (HashUtility_t2883916303), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (TimelineCreateUtilities_t4099873628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[1] = 
{
	U3CGenerateUniqueActorNameU3Ec__AnonStorey0_t930638780::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t3269290940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[1] = 
{
	U3CGenerateUniqueActorNameU3Ec__AnonStorey1_t3269290940::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (TimelineUndo_t898409293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (TimeUtility_t877350212), -1, sizeof(TimeUtility_t877350212_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2859[5] = 
{
	TimeUtility_t877350212_StaticFields::get_offset_of_kTimeEpsilon_0(),
	TimeUtility_t877350212_StaticFields::get_offset_of_kFrameRateEpsilon_1(),
	TimeUtility_t877350212_StaticFields::get_offset_of_k_MaxTimelineDurationInSeconds_2(),
	TimeUtility_t877350212_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
	TimeUtility_t877350212_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (WeightUtility_t2276937658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (U3CModuleU3E_t692745559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (SaveRenderTexture_t2520455591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[6] = 
{
	SaveRenderTexture_t2520455591::get_offset_of_imageName_4(),
	SaveRenderTexture_t2520455591::get_offset_of_saveFolder_5(),
	SaveRenderTexture_t2520455591::get_offset_of_folderType_6(),
	SaveRenderTexture_t2520455591::get_offset_of_saveFormat_7(),
	SaveRenderTexture_t2520455591::get_offset_of_targetRenderTexture_8(),
	SaveRenderTexture_t2520455591::get_offset_of_saveKey_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (SaveFolderType_t939074619)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2863[3] = 
{
	SaveFolderType_t939074619::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (SetupFaceCombine_t3988824787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[1] = 
{
	SetupFaceCombine_t3988824787::get_offset_of_faceQuad_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (ApplyUserFaceOnEnable_t259624351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (ApplyUserScreenShot_t2947430595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (AREndingEventSender_t1071853717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[1] = 
{
	AREndingEventSender_t1071853717::get_offset_of_timeBeforeEnding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (U3CAREndingU3Ec__Iterator0_t1051696702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[4] = 
{
	U3CAREndingU3Ec__Iterator0_t1051696702::get_offset_of_U24this_0(),
	U3CAREndingU3Ec__Iterator0_t1051696702::get_offset_of_U24current_1(),
	U3CAREndingU3Ec__Iterator0_t1051696702::get_offset_of_U24disposing_2(),
	U3CAREndingU3Ec__Iterator0_t1051696702::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (ARScreenShotSaver_t4164871774), -1, sizeof(ARScreenShotSaver_t4164871774_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2869[8] = 
{
	ARScreenShotSaver_t4164871774_StaticFields::get_offset_of_Instance_4(),
	ARScreenShotSaver_t4164871774::get_offset_of_imageName_5(),
	ARScreenShotSaver_t4164871774::get_offset_of_saveFolder_6(),
	ARScreenShotSaver_t4164871774::get_offset_of_folderType_7(),
	ARScreenShotSaver_t4164871774::get_offset_of_saveFormat_8(),
	ARScreenShotSaver_t4164871774::get_offset_of_rendT_9(),
	ARScreenShotSaver_t4164871774::get_offset_of_photoCount_10(),
	ARScreenShotSaver_t4164871774::get_offset_of_savedScreenShots_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (ARStarter_t598103033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[2] = 
{
	ARStarter_t598103033::get_offset_of_planeRotater_4(),
	ARStarter_t598103033::get_offset_of_arObjSet_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (AudioButtonRandomPitch_t1382649135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[3] = 
{
	AudioButtonRandomPitch_t1382649135::get_offset_of__audio_4(),
	AudioButtonRandomPitch_t1382649135::get_offset_of_clips_5(),
	AudioButtonRandomPitch_t1382649135::get_offset_of_pitchRange_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (CameraStatusMonitor_t741672456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[3] = 
{
	CameraStatusMonitor_t741672456::get_offset_of__cam_4(),
	CameraStatusMonitor_t741672456::get_offset_of_fovText_5(),
	CameraStatusMonitor_t741672456::get_offset_of_ratioText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (CharacterAnimationStarter_t4195249726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[3] = 
{
	CharacterAnimationStarter_t4195249726::get_offset_of_charactersRoot_4(),
	CharacterAnimationStarter_t4195249726::get_offset_of_startKey_5(),
	CharacterAnimationStarter_t4195249726::get_offset_of_isStarted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (FaceCameraController_t3256806653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[5] = 
{
	FaceCameraController_t3256806653::get_offset_of_frontCamTexture_4(),
	FaceCameraController_t3256806653::get_offset_of_applyQuad_5(),
	FaceCameraController_t3256806653::get_offset_of_uiQuad_6(),
	FaceCameraController_t3256806653::get_offset_of_useIndex_7(),
	FaceCameraController_t3256806653::get_offset_of_camIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (FaceTextureContainer_t816841679), -1, sizeof(FaceTextureContainer_t816841679_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2875[6] = 
{
	FaceTextureContainer_t816841679_StaticFields::get_offset_of_Instance_4(),
	FaceTextureContainer_t816841679::get_offset_of_faceTextureCam_5(),
	FaceTextureContainer_t816841679::get_offset_of_faceCamTexture_6(),
	FaceTextureContainer_t816841679::get_offset_of_combineFaceQuad_7(),
	FaceTextureContainer_t816841679::get_offset_of_faceTexture_8(),
	FaceTextureContainer_t816841679::get_offset_of_demoUI_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (ShareFBPost_t4271702581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (FollowTargetTransform_t402793746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[1] = 
{
	FollowTargetTransform_t402793746::get_offset_of__target_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (LotteryWebApi_t3108453562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[7] = 
{
	0,
	LotteryWebApi_t3108453562::get_offset_of_nameInput_5(),
	LotteryWebApi_t3108453562::get_offset_of_phoneInput_6(),
	LotteryWebApi_t3108453562::get_offset_of_addressInput_7(),
	LotteryWebApi_t3108453562::get_offset_of_alertAnimationObj_8(),
	LotteryWebApi_t3108453562::get_offset_of_uploadingTextObj_9(),
	LotteryWebApi_t3108453562::get_offset_of_uploadOkTextObj_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (U3CDoLotteryU3Ec__Iterator0_t3551506773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[6] = 
{
	U3CDoLotteryU3Ec__Iterator0_t3551506773::get_offset_of_U3CuserDataU3E__0_0(),
	U3CDoLotteryU3Ec__Iterator0_t3551506773::get_offset_of_U3ClotteryWWWU3E__0_1(),
	U3CDoLotteryU3Ec__Iterator0_t3551506773::get_offset_of_U24this_2(),
	U3CDoLotteryU3Ec__Iterator0_t3551506773::get_offset_of_U24current_3(),
	U3CDoLotteryU3Ec__Iterator0_t3551506773::get_offset_of_U24disposing_4(),
	U3CDoLotteryU3Ec__Iterator0_t3551506773::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (MainDataContainer_t187929543), -1, sizeof(MainDataContainer_t187929543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2880[3] = 
{
	MainDataContainer_t187929543_StaticFields::get_offset_of_Instance_4(),
	MainDataContainer_t187929543::get_offset_of_userSex_5(),
	MainDataContainer_t187929543::get_offset_of_mainCharacter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (UserSex_t1993248581)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2881[3] = 
{
	UserSex_t1993248581::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (RandomAwardController_t1714827868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[6] = 
{
	RandomAwardController_t1714827868::get_offset_of_letterAwardDisplayMesh_4(),
	RandomAwardController_t1714827868::get_offset_of_letterAwardDescriptionMesh_5(),
	RandomAwardController_t1714827868::get_offset_of_awardTitleImgs_6(),
	RandomAwardController_t1714827868::get_offset_of_awardDescImgs_7(),
	RandomAwardController_t1714827868::get_offset_of_awardCharacterBoys_8(),
	RandomAwardController_t1714827868::get_offset_of_awardCharacterGirls_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (RandomPicker_t1486247225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[1] = 
{
	RandomPicker_t1486247225::get_offset_of_targetObjs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (StageJumpAnimation_t2635268956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2884[5] = 
{
	StageJumpAnimation_t2635268956::get_offset_of_offStageCurve_4(),
	StageJumpAnimation_t2635268956::get_offset_of_onStageCurve_5(),
	StageJumpAnimation_t2635268956::get_offset_of_jumpTime_6(),
	StageJumpAnimation_t2635268956::get_offset_of_onStagePoint_7(),
	StageJumpAnimation_t2635268956::get_offset_of_offStagePoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (U3CDoJumpMovingU3Ec__Iterator0_t871786035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[12] = 
{
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_time_0(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U3CframesU3E__0_1(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U3CstepU3E__0_2(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U3CtU3E__0_3(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U3CiU3E__1_4(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_fromPoint_5(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_toPoint_6(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_curve_7(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U24this_8(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U24current_9(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U24disposing_10(),
	U3CDoJumpMovingU3Ec__Iterator0_t871786035::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[6] = 
{
	U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893::get_offset_of_U3C_animatorU3E__0_0(),
	U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893::get_offset_of_U3CnowTimeU3E__0_1(),
	U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893::get_offset_of_U24this_2(),
	U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893::get_offset_of_U24current_3(),
	U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893::get_offset_of_U24disposing_4(),
	U3CMonitorDanceStatusU3Ec__Iterator1_t2575928893::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (TimelinePlayer_t3698074230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[1] = 
{
	TimelinePlayer_t3698074230::get_offset_of__mainTimeline_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (TrackingEventHandler_t2200158480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[6] = 
{
	TrackingEventHandler_t2200158480::get_offset_of_arCamObj_4(),
	TrackingEventHandler_t2200158480::get_offset_of__mainTimeline_5(),
	TrackingEventHandler_t2200158480::get_offset_of_arSet_6(),
	TrackingEventHandler_t2200158480::get_offset_of_canPlay_7(),
	TrackingEventHandler_t2200158480::get_offset_of_isFirstPlay_8(),
	TrackingEventHandler_t2200158480::get_offset_of_isArOn_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (UIApplyFaceTexture_t2177684982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[1] = 
{
	UIApplyFaceTexture_t2177684982::get_offset_of__img_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (UIFSMEventSender_t3608744296), -1, sizeof(UIFSMEventSender_t3608744296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2890[2] = 
{
	UIFSMEventSender_t3608744296_StaticFields::get_offset_of_Instance_4(),
	UIFSMEventSender_t3608744296::get_offset_of_targetFSM_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (UploadPhotoPicker_t3945345849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[4] = 
{
	UploadPhotoPicker_t3945345849::get_offset_of_pickerUiPhotoDisplayImg_4(),
	UploadPhotoPicker_t3945345849::get_offset_of_loadedTextures_5(),
	UploadPhotoPicker_t3945345849::get_offset_of_photoPaths_6(),
	UploadPhotoPicker_t3945345849::get_offset_of_nowPickIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (PhotoObjBehavior_t3978052948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[1] = 
{
	PhotoObjBehavior_t3978052948::get_offset_of_photoImg_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (PhotoPickArrow_t727380732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[2] = 
{
	PhotoPickArrow_t727380732::get_offset_of_eventCatcher_4(),
	PhotoPickArrow_t727380732::get_offset_of_eventName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (PhotoPickerAnimationMoverBehavior_t470882942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[1] = 
{
	PhotoPickerAnimationMoverBehavior_t470882942::get_offset_of_sortingEventCatcher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (UIFaceSettingAnimationBySex_t2841082276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (UIPhotoGalleryManager_t2672000691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[11] = 
{
	UIPhotoGalleryManager_t2672000691::get_offset_of_photoObjPrefab_4(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_pickedPhotoArea_5(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_movingArea_6(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_otherPhotoArea_7(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_testPics_8(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_loadedTextures_9(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_photoObjs_10(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_indexDisplayText_11(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_photoPaths_12(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_nowPickIndex_13(),
	UIPhotoGalleryManager_t2672000691::get_offset_of_canAnimate_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[11] = 
{
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CrandomPosU3E__0_0(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CrandomRotZU3E__0_1(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CtU3E__0_2(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CstepU3E__0_3(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CiU3E__1_4(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CtempObjU3E__2_5(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U3CrectTU3E__2_6(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U24this_7(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U24current_8(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U24disposing_9(),
	U3CGeneratePhotoGalleryAnimatedU3Ec__Iterator0_t1817645962::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (CopyCameraData_t869109636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[1] = 
{
	CopyCameraData_t869109636::get_offset_of_targetCamera_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (DynamicShadowDistance_t2173449775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[2] = 
{
	DynamicShadowDistance_t2173449775::get_offset_of_targetObj_4(),
	DynamicShadowDistance_t2173449775::get_offset_of_camToObjDist_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
