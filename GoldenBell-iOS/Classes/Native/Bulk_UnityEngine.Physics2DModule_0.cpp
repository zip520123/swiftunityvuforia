﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_t2411569343;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295;
// UnityEngine.Collision2D
struct Collision2D_t2842956331;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t96683501;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.HingeJoint2D
struct HingeJoint2D_t3442948838;
// UnityEngine.Joint2D
struct Joint2D_t4180440564;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t331219942;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t385786356;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.WheelJoint2D
struct WheelJoint2D_t1756757149;

extern RuntimeClass* Collider2D_t2806799626_il2cpp_TypeInfo_var;
extern RuntimeClass* ContactPoint2DU5BU5D_t96683501_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2411569343_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern RuntimeClass* Rigidbody2D_t939494601_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2466177744_RuntimeMethod_var;
extern const uint32_t Collision2D_get_collider_m4087612183_MetadataUsageId;
extern const uint32_t Collision2D_get_contacts_m696521713_MetadataUsageId;
extern const uint32_t Collision2D_get_gameObject_m1443495885_MetadataUsageId;
extern const uint32_t Collision2D_get_rigidbody_m716857034_MetadataUsageId;
extern const uint32_t ContactFilter2D_CheckConsistency_m222060761_MetadataUsageId;
extern const uint32_t ContactFilter2D_CreateLegacyFilter_m1873816896_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_Internal_m643548862_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m1507505063_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m1670084419_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m2567878449_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionNonAlloc_m2525277886_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionNonAlloc_m2666102441_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionNonAlloc_m3412378039_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersection_Internal_m1872178675_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersection_m1808353572_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersection_m662508963_MetadataUsageId;
extern const uint32_t Physics2D_LinecastAll_Internal_m604725090_MetadataUsageId;
extern const uint32_t Physics2D_LinecastAll_m3604445636_MetadataUsageId;
extern const uint32_t Physics2D_LinecastAll_m4019250736_MetadataUsageId;
extern const uint32_t Physics2D_Linecast_Internal_m2067808837_MetadataUsageId;
extern const uint32_t Physics2D_Linecast_m2284594132_MetadataUsageId;
extern const uint32_t Physics2D_Linecast_m2347689996_MetadataUsageId;
extern const uint32_t Physics2D_OverlapAreaAllToBox_Internal_m4180410992_MetadataUsageId;
extern const uint32_t Physics2D_OverlapAreaAll_m3532436837_MetadataUsageId;
extern const uint32_t Physics2D_OverlapAreaAll_m3957673898_MetadataUsageId;
extern const uint32_t Physics2D_OverlapBoxAll_Internal_m2950777821_MetadataUsageId;
extern const uint32_t Physics2D_OverlapBoxAll_m2279087762_MetadataUsageId;
extern const uint32_t Physics2D_OverlapCircleAll_Internal_m1158092355_MetadataUsageId;
extern const uint32_t Physics2D_OverlapCircleAll_m1634860779_MetadataUsageId;
extern const uint32_t Physics2D_OverlapCircleAll_m638049410_MetadataUsageId;
extern const uint32_t Physics2D_OverlapPointAll_Internal_m3057315634_MetadataUsageId;
extern const uint32_t Physics2D_OverlapPointAll_m1050872217_MetadataUsageId;
extern const uint32_t Physics2D_OverlapPointAll_m2300095884_MetadataUsageId;
extern const uint32_t Physics2D_RaycastAll_Internal_m3190530210_MetadataUsageId;
extern const uint32_t Physics2D_RaycastAll_m3915954823_MetadataUsageId;
extern const uint32_t Physics2D_RaycastAll_m399425645_MetadataUsageId;
extern const uint32_t Physics2D_RaycastNonAlloc_Internal_m4193835177_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_Internal_m2084501876_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m1377762210_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m1407159225_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m1700114981_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m1728056151_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2082490717_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2341153778_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2858866636_MetadataUsageId;
extern const uint32_t Physics2D__cctor_m143866755_MetadataUsageId;
extern const uint32_t Physics2D_set_gravity_m2648827161_MetadataUsageId;
extern const uint32_t PhysicsMaterial2D__ctor_m2702207403_MetadataUsageId;
extern const uint32_t RaycastHit2D_get_collider_m1549426026_MetadataUsageId;
extern const uint32_t RaycastHit2D_get_rigidbody_m942928380_MetadataUsageId;
extern const uint32_t RaycastHit2D_get_transform_m2048267409_MetadataUsageId;
struct ContactPoint2D_t3390240644 ;

struct Collider2DU5BU5D_t1693969295;
struct ContactPoint2DU5BU5D_t96683501;
struct RaycastHit2DU5BU5D_t4286651560;


#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T2411569343_H
#define LIST_1_T2411569343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct  List_1_t2411569343  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Rigidbody2DU5BU5D_t385786356* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2411569343, ____items_1)); }
	inline Rigidbody2DU5BU5D_t385786356* get__items_1() const { return ____items_1; }
	inline Rigidbody2DU5BU5D_t385786356** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Rigidbody2DU5BU5D_t385786356* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2411569343, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2411569343, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2411569343_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Rigidbody2DU5BU5D_t385786356* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2411569343_StaticFields, ___EmptyArray_4)); }
	inline Rigidbody2DU5BU5D_t385786356* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Rigidbody2DU5BU5D_t385786356** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Rigidbody2DU5BU5D_t385786356* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2411569343_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef PHYSICS2D_T1528932956_H
#define PHYSICS2D_T1528932956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Physics2D
struct  Physics2D_t1528932956  : public RuntimeObject
{
public:

public:
};

struct Physics2D_t1528932956_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_t2411569343 * ___m_LastDisabledRigidbody2D_0;

public:
	inline static int32_t get_offset_of_m_LastDisabledRigidbody2D_0() { return static_cast<int32_t>(offsetof(Physics2D_t1528932956_StaticFields, ___m_LastDisabledRigidbody2D_0)); }
	inline List_1_t2411569343 * get_m_LastDisabledRigidbody2D_0() const { return ___m_LastDisabledRigidbody2D_0; }
	inline List_1_t2411569343 ** get_address_of_m_LastDisabledRigidbody2D_0() { return &___m_LastDisabledRigidbody2D_0; }
	inline void set_m_LastDisabledRigidbody2D_0(List_1_t2411569343 * value)
	{
		___m_LastDisabledRigidbody2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastDisabledRigidbody2D_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2D_T1528932956_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef JOINTANGLELIMITS2D_T972279075_H
#define JOINTANGLELIMITS2D_T972279075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointAngleLimits2D
struct  JointAngleLimits2D_t972279075 
{
public:
	// System.Single UnityEngine.JointAngleLimits2D::m_LowerAngle
	float ___m_LowerAngle_0;
	// System.Single UnityEngine.JointAngleLimits2D::m_UpperAngle
	float ___m_UpperAngle_1;

public:
	inline static int32_t get_offset_of_m_LowerAngle_0() { return static_cast<int32_t>(offsetof(JointAngleLimits2D_t972279075, ___m_LowerAngle_0)); }
	inline float get_m_LowerAngle_0() const { return ___m_LowerAngle_0; }
	inline float* get_address_of_m_LowerAngle_0() { return &___m_LowerAngle_0; }
	inline void set_m_LowerAngle_0(float value)
	{
		___m_LowerAngle_0 = value;
	}

	inline static int32_t get_offset_of_m_UpperAngle_1() { return static_cast<int32_t>(offsetof(JointAngleLimits2D_t972279075, ___m_UpperAngle_1)); }
	inline float get_m_UpperAngle_1() const { return ___m_UpperAngle_1; }
	inline float* get_address_of_m_UpperAngle_1() { return &___m_UpperAngle_1; }
	inline void set_m_UpperAngle_1(float value)
	{
		___m_UpperAngle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTANGLELIMITS2D_T972279075_H
#ifndef JOINTMOTOR2D_T142461323_H
#define JOINTMOTOR2D_T142461323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointMotor2D
struct  JointMotor2D_t142461323 
{
public:
	// System.Single UnityEngine.JointMotor2D::m_MotorSpeed
	float ___m_MotorSpeed_0;
	// System.Single UnityEngine.JointMotor2D::m_MaximumMotorTorque
	float ___m_MaximumMotorTorque_1;

public:
	inline static int32_t get_offset_of_m_MotorSpeed_0() { return static_cast<int32_t>(offsetof(JointMotor2D_t142461323, ___m_MotorSpeed_0)); }
	inline float get_m_MotorSpeed_0() const { return ___m_MotorSpeed_0; }
	inline float* get_address_of_m_MotorSpeed_0() { return &___m_MotorSpeed_0; }
	inline void set_m_MotorSpeed_0(float value)
	{
		___m_MotorSpeed_0 = value;
	}

	inline static int32_t get_offset_of_m_MaximumMotorTorque_1() { return static_cast<int32_t>(offsetof(JointMotor2D_t142461323, ___m_MaximumMotorTorque_1)); }
	inline float get_m_MaximumMotorTorque_1() const { return ___m_MaximumMotorTorque_1; }
	inline float* get_address_of_m_MaximumMotorTorque_1() { return &___m_MaximumMotorTorque_1; }
	inline void set_m_MaximumMotorTorque_1(float value)
	{
		___m_MaximumMotorTorque_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTMOTOR2D_T142461323_H
#ifndef JOINTSUSPENSION2D_T3350541332_H
#define JOINTSUSPENSION2D_T3350541332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.JointSuspension2D
struct  JointSuspension2D_t3350541332 
{
public:
	// System.Single UnityEngine.JointSuspension2D::m_DampingRatio
	float ___m_DampingRatio_0;
	// System.Single UnityEngine.JointSuspension2D::m_Frequency
	float ___m_Frequency_1;
	// System.Single UnityEngine.JointSuspension2D::m_Angle
	float ___m_Angle_2;

public:
	inline static int32_t get_offset_of_m_DampingRatio_0() { return static_cast<int32_t>(offsetof(JointSuspension2D_t3350541332, ___m_DampingRatio_0)); }
	inline float get_m_DampingRatio_0() const { return ___m_DampingRatio_0; }
	inline float* get_address_of_m_DampingRatio_0() { return &___m_DampingRatio_0; }
	inline void set_m_DampingRatio_0(float value)
	{
		___m_DampingRatio_0 = value;
	}

	inline static int32_t get_offset_of_m_Frequency_1() { return static_cast<int32_t>(offsetof(JointSuspension2D_t3350541332, ___m_Frequency_1)); }
	inline float get_m_Frequency_1() const { return ___m_Frequency_1; }
	inline float* get_address_of_m_Frequency_1() { return &___m_Frequency_1; }
	inline void set_m_Frequency_1(float value)
	{
		___m_Frequency_1 = value;
	}

	inline static int32_t get_offset_of_m_Angle_2() { return static_cast<int32_t>(offsetof(JointSuspension2D_t3350541332, ___m_Angle_2)); }
	inline float get_m_Angle_2() const { return ___m_Angle_2; }
	inline float* get_address_of_m_Angle_2() { return &___m_Angle_2; }
	inline void set_m_Angle_2(float value)
	{
		___m_Angle_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTSUSPENSION2D_T3350541332_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef CONTACTFILTER2D_T3805203441_H
#define CONTACTFILTER2D_T3805203441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContactFilter2D
struct  ContactFilter2D_t3805203441 
{
public:
	// System.Boolean UnityEngine.ContactFilter2D::useTriggers
	bool ___useTriggers_0;
	// System.Boolean UnityEngine.ContactFilter2D::useLayerMask
	bool ___useLayerMask_1;
	// System.Boolean UnityEngine.ContactFilter2D::useDepth
	bool ___useDepth_2;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideDepth
	bool ___useOutsideDepth_3;
	// System.Boolean UnityEngine.ContactFilter2D::useNormalAngle
	bool ___useNormalAngle_4;
	// System.Boolean UnityEngine.ContactFilter2D::useOutsideNormalAngle
	bool ___useOutsideNormalAngle_5;
	// UnityEngine.LayerMask UnityEngine.ContactFilter2D::layerMask
	LayerMask_t3493934918  ___layerMask_6;
	// System.Single UnityEngine.ContactFilter2D::minDepth
	float ___minDepth_7;
	// System.Single UnityEngine.ContactFilter2D::maxDepth
	float ___maxDepth_8;
	// System.Single UnityEngine.ContactFilter2D::minNormalAngle
	float ___minNormalAngle_9;
	// System.Single UnityEngine.ContactFilter2D::maxNormalAngle
	float ___maxNormalAngle_10;

public:
	inline static int32_t get_offset_of_useTriggers_0() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___useTriggers_0)); }
	inline bool get_useTriggers_0() const { return ___useTriggers_0; }
	inline bool* get_address_of_useTriggers_0() { return &___useTriggers_0; }
	inline void set_useTriggers_0(bool value)
	{
		___useTriggers_0 = value;
	}

	inline static int32_t get_offset_of_useLayerMask_1() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___useLayerMask_1)); }
	inline bool get_useLayerMask_1() const { return ___useLayerMask_1; }
	inline bool* get_address_of_useLayerMask_1() { return &___useLayerMask_1; }
	inline void set_useLayerMask_1(bool value)
	{
		___useLayerMask_1 = value;
	}

	inline static int32_t get_offset_of_useDepth_2() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___useDepth_2)); }
	inline bool get_useDepth_2() const { return ___useDepth_2; }
	inline bool* get_address_of_useDepth_2() { return &___useDepth_2; }
	inline void set_useDepth_2(bool value)
	{
		___useDepth_2 = value;
	}

	inline static int32_t get_offset_of_useOutsideDepth_3() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___useOutsideDepth_3)); }
	inline bool get_useOutsideDepth_3() const { return ___useOutsideDepth_3; }
	inline bool* get_address_of_useOutsideDepth_3() { return &___useOutsideDepth_3; }
	inline void set_useOutsideDepth_3(bool value)
	{
		___useOutsideDepth_3 = value;
	}

	inline static int32_t get_offset_of_useNormalAngle_4() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___useNormalAngle_4)); }
	inline bool get_useNormalAngle_4() const { return ___useNormalAngle_4; }
	inline bool* get_address_of_useNormalAngle_4() { return &___useNormalAngle_4; }
	inline void set_useNormalAngle_4(bool value)
	{
		___useNormalAngle_4 = value;
	}

	inline static int32_t get_offset_of_useOutsideNormalAngle_5() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___useOutsideNormalAngle_5)); }
	inline bool get_useOutsideNormalAngle_5() const { return ___useOutsideNormalAngle_5; }
	inline bool* get_address_of_useOutsideNormalAngle_5() { return &___useOutsideNormalAngle_5; }
	inline void set_useOutsideNormalAngle_5(bool value)
	{
		___useOutsideNormalAngle_5 = value;
	}

	inline static int32_t get_offset_of_layerMask_6() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___layerMask_6)); }
	inline LayerMask_t3493934918  get_layerMask_6() const { return ___layerMask_6; }
	inline LayerMask_t3493934918 * get_address_of_layerMask_6() { return &___layerMask_6; }
	inline void set_layerMask_6(LayerMask_t3493934918  value)
	{
		___layerMask_6 = value;
	}

	inline static int32_t get_offset_of_minDepth_7() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___minDepth_7)); }
	inline float get_minDepth_7() const { return ___minDepth_7; }
	inline float* get_address_of_minDepth_7() { return &___minDepth_7; }
	inline void set_minDepth_7(float value)
	{
		___minDepth_7 = value;
	}

	inline static int32_t get_offset_of_maxDepth_8() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___maxDepth_8)); }
	inline float get_maxDepth_8() const { return ___maxDepth_8; }
	inline float* get_address_of_maxDepth_8() { return &___maxDepth_8; }
	inline void set_maxDepth_8(float value)
	{
		___maxDepth_8 = value;
	}

	inline static int32_t get_offset_of_minNormalAngle_9() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___minNormalAngle_9)); }
	inline float get_minNormalAngle_9() const { return ___minNormalAngle_9; }
	inline float* get_address_of_minNormalAngle_9() { return &___minNormalAngle_9; }
	inline void set_minNormalAngle_9(float value)
	{
		___minNormalAngle_9 = value;
	}

	inline static int32_t get_offset_of_maxNormalAngle_10() { return static_cast<int32_t>(offsetof(ContactFilter2D_t3805203441, ___maxNormalAngle_10)); }
	inline float get_maxNormalAngle_10() const { return ___maxNormalAngle_10; }
	inline float* get_address_of_maxNormalAngle_10() { return &___maxNormalAngle_10; }
	inline void set_maxNormalAngle_10(float value)
	{
		___maxNormalAngle_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t3805203441_marshaled_pinvoke
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t3493934918  ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};
// Native definition for COM marshalling of UnityEngine.ContactFilter2D
struct ContactFilter2D_t3805203441_marshaled_com
{
	int32_t ___useTriggers_0;
	int32_t ___useLayerMask_1;
	int32_t ___useDepth_2;
	int32_t ___useOutsideDepth_3;
	int32_t ___useNormalAngle_4;
	int32_t ___useOutsideNormalAngle_5;
	LayerMask_t3493934918  ___layerMask_6;
	float ___minDepth_7;
	float ___maxDepth_8;
	float ___minNormalAngle_9;
	float ___maxNormalAngle_10;
};
#endif // CONTACTFILTER2D_T3805203441_H
#ifndef CONTACTPOINT2D_T3390240644_H
#define CONTACTPOINT2D_T3390240644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ContactPoint2D
struct  ContactPoint2D_t3390240644 
{
public:
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_t2156229523  ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_t2156229523  ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_t2156229523  ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Point_0)); }
	inline Vector2_t2156229523  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector2_t2156229523 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector2_t2156229523  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Normal_1)); }
	inline Vector2_t2156229523  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector2_t2156229523  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_2() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_RelativeVelocity_2)); }
	inline Vector2_t2156229523  get_m_RelativeVelocity_2() const { return ___m_RelativeVelocity_2; }
	inline Vector2_t2156229523 * get_address_of_m_RelativeVelocity_2() { return &___m_RelativeVelocity_2; }
	inline void set_m_RelativeVelocity_2(Vector2_t2156229523  value)
	{
		___m_RelativeVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_Separation_3() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Separation_3)); }
	inline float get_m_Separation_3() const { return ___m_Separation_3; }
	inline float* get_address_of_m_Separation_3() { return &___m_Separation_3; }
	inline void set_m_Separation_3(float value)
	{
		___m_Separation_3 = value;
	}

	inline static int32_t get_offset_of_m_NormalImpulse_4() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_NormalImpulse_4)); }
	inline float get_m_NormalImpulse_4() const { return ___m_NormalImpulse_4; }
	inline float* get_address_of_m_NormalImpulse_4() { return &___m_NormalImpulse_4; }
	inline void set_m_NormalImpulse_4(float value)
	{
		___m_NormalImpulse_4 = value;
	}

	inline static int32_t get_offset_of_m_TangentImpulse_5() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_TangentImpulse_5)); }
	inline float get_m_TangentImpulse_5() const { return ___m_TangentImpulse_5; }
	inline float* get_address_of_m_TangentImpulse_5() { return &___m_TangentImpulse_5; }
	inline void set_m_TangentImpulse_5(float value)
	{
		___m_TangentImpulse_5 = value;
	}

	inline static int32_t get_offset_of_m_Collider_6() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Collider_6)); }
	inline int32_t get_m_Collider_6() const { return ___m_Collider_6; }
	inline int32_t* get_address_of_m_Collider_6() { return &___m_Collider_6; }
	inline void set_m_Collider_6(int32_t value)
	{
		___m_Collider_6 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_7() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_OtherCollider_7)); }
	inline int32_t get_m_OtherCollider_7() const { return ___m_OtherCollider_7; }
	inline int32_t* get_address_of_m_OtherCollider_7() { return &___m_OtherCollider_7; }
	inline void set_m_OtherCollider_7(int32_t value)
	{
		___m_OtherCollider_7 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_8() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Rigidbody_8)); }
	inline int32_t get_m_Rigidbody_8() const { return ___m_Rigidbody_8; }
	inline int32_t* get_address_of_m_Rigidbody_8() { return &___m_Rigidbody_8; }
	inline void set_m_Rigidbody_8(int32_t value)
	{
		___m_Rigidbody_8 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_9() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_OtherRigidbody_9)); }
	inline int32_t get_m_OtherRigidbody_9() const { return ___m_OtherRigidbody_9; }
	inline int32_t* get_address_of_m_OtherRigidbody_9() { return &___m_OtherRigidbody_9; }
	inline void set_m_OtherRigidbody_9(int32_t value)
	{
		___m_OtherRigidbody_9 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_10() { return static_cast<int32_t>(offsetof(ContactPoint2D_t3390240644, ___m_Enabled_10)); }
	inline int32_t get_m_Enabled_10() const { return ___m_Enabled_10; }
	inline int32_t* get_address_of_m_Enabled_10() { return &___m_Enabled_10; }
	inline void set_m_Enabled_10(int32_t value)
	{
		___m_Enabled_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTACTPOINT2D_T3390240644_H
#ifndef FORCEMODE2D_T255358695_H
#define FORCEMODE2D_T255358695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ForceMode2D
struct  ForceMode2D_t255358695 
{
public:
	// System.Int32 UnityEngine.ForceMode2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ForceMode2D_t255358695, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEMODE2D_T255358695_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef RIGIDBODYCONSTRAINTS2D_T462572363_H
#define RIGIDBODYCONSTRAINTS2D_T462572363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyConstraints2D
struct  RigidbodyConstraints2D_t462572363 
{
public:
	// System.Int32 UnityEngine.RigidbodyConstraints2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyConstraints2D_t462572363, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYCONSTRAINTS2D_T462572363_H
#ifndef RIGIDBODYTYPE2D_T1648102732_H
#define RIGIDBODYTYPE2D_T1648102732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyType2D
struct  RigidbodyType2D_t1648102732 
{
public:
	// System.Int32 UnityEngine.RigidbodyType2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyType2D_t1648102732, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYTYPE2D_T1648102732_H
#ifndef CACHEDCONTACTPOINTS2D_T2523437281_H
#define CACHEDCONTACTPOINTS2D_T2523437281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CachedContactPoints2D
struct  CachedContactPoints2D_t2523437281 
{
public:
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact0
	ContactPoint2D_t3390240644  ___m_Contact0_0;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact1
	ContactPoint2D_t3390240644  ___m_Contact1_1;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact2
	ContactPoint2D_t3390240644  ___m_Contact2_2;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact3
	ContactPoint2D_t3390240644  ___m_Contact3_3;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact4
	ContactPoint2D_t3390240644  ___m_Contact4_4;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact5
	ContactPoint2D_t3390240644  ___m_Contact5_5;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact6
	ContactPoint2D_t3390240644  ___m_Contact6_6;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact7
	ContactPoint2D_t3390240644  ___m_Contact7_7;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact8
	ContactPoint2D_t3390240644  ___m_Contact8_8;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact9
	ContactPoint2D_t3390240644  ___m_Contact9_9;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact10
	ContactPoint2D_t3390240644  ___m_Contact10_10;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact11
	ContactPoint2D_t3390240644  ___m_Contact11_11;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact12
	ContactPoint2D_t3390240644  ___m_Contact12_12;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact13
	ContactPoint2D_t3390240644  ___m_Contact13_13;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact14
	ContactPoint2D_t3390240644  ___m_Contact14_14;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact15
	ContactPoint2D_t3390240644  ___m_Contact15_15;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact16
	ContactPoint2D_t3390240644  ___m_Contact16_16;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact17
	ContactPoint2D_t3390240644  ___m_Contact17_17;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact18
	ContactPoint2D_t3390240644  ___m_Contact18_18;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact19
	ContactPoint2D_t3390240644  ___m_Contact19_19;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact20
	ContactPoint2D_t3390240644  ___m_Contact20_20;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact21
	ContactPoint2D_t3390240644  ___m_Contact21_21;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact22
	ContactPoint2D_t3390240644  ___m_Contact22_22;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact23
	ContactPoint2D_t3390240644  ___m_Contact23_23;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact24
	ContactPoint2D_t3390240644  ___m_Contact24_24;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact25
	ContactPoint2D_t3390240644  ___m_Contact25_25;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact26
	ContactPoint2D_t3390240644  ___m_Contact26_26;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact27
	ContactPoint2D_t3390240644  ___m_Contact27_27;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact28
	ContactPoint2D_t3390240644  ___m_Contact28_28;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact29
	ContactPoint2D_t3390240644  ___m_Contact29_29;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact30
	ContactPoint2D_t3390240644  ___m_Contact30_30;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact31
	ContactPoint2D_t3390240644  ___m_Contact31_31;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact32
	ContactPoint2D_t3390240644  ___m_Contact32_32;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact33
	ContactPoint2D_t3390240644  ___m_Contact33_33;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact34
	ContactPoint2D_t3390240644  ___m_Contact34_34;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact35
	ContactPoint2D_t3390240644  ___m_Contact35_35;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact36
	ContactPoint2D_t3390240644  ___m_Contact36_36;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact37
	ContactPoint2D_t3390240644  ___m_Contact37_37;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact38
	ContactPoint2D_t3390240644  ___m_Contact38_38;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact39
	ContactPoint2D_t3390240644  ___m_Contact39_39;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact40
	ContactPoint2D_t3390240644  ___m_Contact40_40;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact41
	ContactPoint2D_t3390240644  ___m_Contact41_41;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact42
	ContactPoint2D_t3390240644  ___m_Contact42_42;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact43
	ContactPoint2D_t3390240644  ___m_Contact43_43;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact44
	ContactPoint2D_t3390240644  ___m_Contact44_44;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact45
	ContactPoint2D_t3390240644  ___m_Contact45_45;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact46
	ContactPoint2D_t3390240644  ___m_Contact46_46;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact47
	ContactPoint2D_t3390240644  ___m_Contact47_47;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact48
	ContactPoint2D_t3390240644  ___m_Contact48_48;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact49
	ContactPoint2D_t3390240644  ___m_Contact49_49;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact50
	ContactPoint2D_t3390240644  ___m_Contact50_50;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact51
	ContactPoint2D_t3390240644  ___m_Contact51_51;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact52
	ContactPoint2D_t3390240644  ___m_Contact52_52;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact53
	ContactPoint2D_t3390240644  ___m_Contact53_53;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact54
	ContactPoint2D_t3390240644  ___m_Contact54_54;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact55
	ContactPoint2D_t3390240644  ___m_Contact55_55;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact56
	ContactPoint2D_t3390240644  ___m_Contact56_56;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact57
	ContactPoint2D_t3390240644  ___m_Contact57_57;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact58
	ContactPoint2D_t3390240644  ___m_Contact58_58;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact59
	ContactPoint2D_t3390240644  ___m_Contact59_59;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact60
	ContactPoint2D_t3390240644  ___m_Contact60_60;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact61
	ContactPoint2D_t3390240644  ___m_Contact61_61;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact62
	ContactPoint2D_t3390240644  ___m_Contact62_62;
	// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::m_Contact63
	ContactPoint2D_t3390240644  ___m_Contact63_63;

public:
	inline static int32_t get_offset_of_m_Contact0_0() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact0_0)); }
	inline ContactPoint2D_t3390240644  get_m_Contact0_0() const { return ___m_Contact0_0; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact0_0() { return &___m_Contact0_0; }
	inline void set_m_Contact0_0(ContactPoint2D_t3390240644  value)
	{
		___m_Contact0_0 = value;
	}

	inline static int32_t get_offset_of_m_Contact1_1() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact1_1)); }
	inline ContactPoint2D_t3390240644  get_m_Contact1_1() const { return ___m_Contact1_1; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact1_1() { return &___m_Contact1_1; }
	inline void set_m_Contact1_1(ContactPoint2D_t3390240644  value)
	{
		___m_Contact1_1 = value;
	}

	inline static int32_t get_offset_of_m_Contact2_2() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact2_2)); }
	inline ContactPoint2D_t3390240644  get_m_Contact2_2() const { return ___m_Contact2_2; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact2_2() { return &___m_Contact2_2; }
	inline void set_m_Contact2_2(ContactPoint2D_t3390240644  value)
	{
		___m_Contact2_2 = value;
	}

	inline static int32_t get_offset_of_m_Contact3_3() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact3_3)); }
	inline ContactPoint2D_t3390240644  get_m_Contact3_3() const { return ___m_Contact3_3; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact3_3() { return &___m_Contact3_3; }
	inline void set_m_Contact3_3(ContactPoint2D_t3390240644  value)
	{
		___m_Contact3_3 = value;
	}

	inline static int32_t get_offset_of_m_Contact4_4() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact4_4)); }
	inline ContactPoint2D_t3390240644  get_m_Contact4_4() const { return ___m_Contact4_4; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact4_4() { return &___m_Contact4_4; }
	inline void set_m_Contact4_4(ContactPoint2D_t3390240644  value)
	{
		___m_Contact4_4 = value;
	}

	inline static int32_t get_offset_of_m_Contact5_5() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact5_5)); }
	inline ContactPoint2D_t3390240644  get_m_Contact5_5() const { return ___m_Contact5_5; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact5_5() { return &___m_Contact5_5; }
	inline void set_m_Contact5_5(ContactPoint2D_t3390240644  value)
	{
		___m_Contact5_5 = value;
	}

	inline static int32_t get_offset_of_m_Contact6_6() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact6_6)); }
	inline ContactPoint2D_t3390240644  get_m_Contact6_6() const { return ___m_Contact6_6; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact6_6() { return &___m_Contact6_6; }
	inline void set_m_Contact6_6(ContactPoint2D_t3390240644  value)
	{
		___m_Contact6_6 = value;
	}

	inline static int32_t get_offset_of_m_Contact7_7() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact7_7)); }
	inline ContactPoint2D_t3390240644  get_m_Contact7_7() const { return ___m_Contact7_7; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact7_7() { return &___m_Contact7_7; }
	inline void set_m_Contact7_7(ContactPoint2D_t3390240644  value)
	{
		___m_Contact7_7 = value;
	}

	inline static int32_t get_offset_of_m_Contact8_8() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact8_8)); }
	inline ContactPoint2D_t3390240644  get_m_Contact8_8() const { return ___m_Contact8_8; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact8_8() { return &___m_Contact8_8; }
	inline void set_m_Contact8_8(ContactPoint2D_t3390240644  value)
	{
		___m_Contact8_8 = value;
	}

	inline static int32_t get_offset_of_m_Contact9_9() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact9_9)); }
	inline ContactPoint2D_t3390240644  get_m_Contact9_9() const { return ___m_Contact9_9; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact9_9() { return &___m_Contact9_9; }
	inline void set_m_Contact9_9(ContactPoint2D_t3390240644  value)
	{
		___m_Contact9_9 = value;
	}

	inline static int32_t get_offset_of_m_Contact10_10() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact10_10)); }
	inline ContactPoint2D_t3390240644  get_m_Contact10_10() const { return ___m_Contact10_10; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact10_10() { return &___m_Contact10_10; }
	inline void set_m_Contact10_10(ContactPoint2D_t3390240644  value)
	{
		___m_Contact10_10 = value;
	}

	inline static int32_t get_offset_of_m_Contact11_11() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact11_11)); }
	inline ContactPoint2D_t3390240644  get_m_Contact11_11() const { return ___m_Contact11_11; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact11_11() { return &___m_Contact11_11; }
	inline void set_m_Contact11_11(ContactPoint2D_t3390240644  value)
	{
		___m_Contact11_11 = value;
	}

	inline static int32_t get_offset_of_m_Contact12_12() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact12_12)); }
	inline ContactPoint2D_t3390240644  get_m_Contact12_12() const { return ___m_Contact12_12; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact12_12() { return &___m_Contact12_12; }
	inline void set_m_Contact12_12(ContactPoint2D_t3390240644  value)
	{
		___m_Contact12_12 = value;
	}

	inline static int32_t get_offset_of_m_Contact13_13() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact13_13)); }
	inline ContactPoint2D_t3390240644  get_m_Contact13_13() const { return ___m_Contact13_13; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact13_13() { return &___m_Contact13_13; }
	inline void set_m_Contact13_13(ContactPoint2D_t3390240644  value)
	{
		___m_Contact13_13 = value;
	}

	inline static int32_t get_offset_of_m_Contact14_14() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact14_14)); }
	inline ContactPoint2D_t3390240644  get_m_Contact14_14() const { return ___m_Contact14_14; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact14_14() { return &___m_Contact14_14; }
	inline void set_m_Contact14_14(ContactPoint2D_t3390240644  value)
	{
		___m_Contact14_14 = value;
	}

	inline static int32_t get_offset_of_m_Contact15_15() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact15_15)); }
	inline ContactPoint2D_t3390240644  get_m_Contact15_15() const { return ___m_Contact15_15; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact15_15() { return &___m_Contact15_15; }
	inline void set_m_Contact15_15(ContactPoint2D_t3390240644  value)
	{
		___m_Contact15_15 = value;
	}

	inline static int32_t get_offset_of_m_Contact16_16() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact16_16)); }
	inline ContactPoint2D_t3390240644  get_m_Contact16_16() const { return ___m_Contact16_16; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact16_16() { return &___m_Contact16_16; }
	inline void set_m_Contact16_16(ContactPoint2D_t3390240644  value)
	{
		___m_Contact16_16 = value;
	}

	inline static int32_t get_offset_of_m_Contact17_17() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact17_17)); }
	inline ContactPoint2D_t3390240644  get_m_Contact17_17() const { return ___m_Contact17_17; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact17_17() { return &___m_Contact17_17; }
	inline void set_m_Contact17_17(ContactPoint2D_t3390240644  value)
	{
		___m_Contact17_17 = value;
	}

	inline static int32_t get_offset_of_m_Contact18_18() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact18_18)); }
	inline ContactPoint2D_t3390240644  get_m_Contact18_18() const { return ___m_Contact18_18; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact18_18() { return &___m_Contact18_18; }
	inline void set_m_Contact18_18(ContactPoint2D_t3390240644  value)
	{
		___m_Contact18_18 = value;
	}

	inline static int32_t get_offset_of_m_Contact19_19() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact19_19)); }
	inline ContactPoint2D_t3390240644  get_m_Contact19_19() const { return ___m_Contact19_19; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact19_19() { return &___m_Contact19_19; }
	inline void set_m_Contact19_19(ContactPoint2D_t3390240644  value)
	{
		___m_Contact19_19 = value;
	}

	inline static int32_t get_offset_of_m_Contact20_20() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact20_20)); }
	inline ContactPoint2D_t3390240644  get_m_Contact20_20() const { return ___m_Contact20_20; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact20_20() { return &___m_Contact20_20; }
	inline void set_m_Contact20_20(ContactPoint2D_t3390240644  value)
	{
		___m_Contact20_20 = value;
	}

	inline static int32_t get_offset_of_m_Contact21_21() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact21_21)); }
	inline ContactPoint2D_t3390240644  get_m_Contact21_21() const { return ___m_Contact21_21; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact21_21() { return &___m_Contact21_21; }
	inline void set_m_Contact21_21(ContactPoint2D_t3390240644  value)
	{
		___m_Contact21_21 = value;
	}

	inline static int32_t get_offset_of_m_Contact22_22() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact22_22)); }
	inline ContactPoint2D_t3390240644  get_m_Contact22_22() const { return ___m_Contact22_22; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact22_22() { return &___m_Contact22_22; }
	inline void set_m_Contact22_22(ContactPoint2D_t3390240644  value)
	{
		___m_Contact22_22 = value;
	}

	inline static int32_t get_offset_of_m_Contact23_23() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact23_23)); }
	inline ContactPoint2D_t3390240644  get_m_Contact23_23() const { return ___m_Contact23_23; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact23_23() { return &___m_Contact23_23; }
	inline void set_m_Contact23_23(ContactPoint2D_t3390240644  value)
	{
		___m_Contact23_23 = value;
	}

	inline static int32_t get_offset_of_m_Contact24_24() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact24_24)); }
	inline ContactPoint2D_t3390240644  get_m_Contact24_24() const { return ___m_Contact24_24; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact24_24() { return &___m_Contact24_24; }
	inline void set_m_Contact24_24(ContactPoint2D_t3390240644  value)
	{
		___m_Contact24_24 = value;
	}

	inline static int32_t get_offset_of_m_Contact25_25() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact25_25)); }
	inline ContactPoint2D_t3390240644  get_m_Contact25_25() const { return ___m_Contact25_25; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact25_25() { return &___m_Contact25_25; }
	inline void set_m_Contact25_25(ContactPoint2D_t3390240644  value)
	{
		___m_Contact25_25 = value;
	}

	inline static int32_t get_offset_of_m_Contact26_26() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact26_26)); }
	inline ContactPoint2D_t3390240644  get_m_Contact26_26() const { return ___m_Contact26_26; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact26_26() { return &___m_Contact26_26; }
	inline void set_m_Contact26_26(ContactPoint2D_t3390240644  value)
	{
		___m_Contact26_26 = value;
	}

	inline static int32_t get_offset_of_m_Contact27_27() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact27_27)); }
	inline ContactPoint2D_t3390240644  get_m_Contact27_27() const { return ___m_Contact27_27; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact27_27() { return &___m_Contact27_27; }
	inline void set_m_Contact27_27(ContactPoint2D_t3390240644  value)
	{
		___m_Contact27_27 = value;
	}

	inline static int32_t get_offset_of_m_Contact28_28() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact28_28)); }
	inline ContactPoint2D_t3390240644  get_m_Contact28_28() const { return ___m_Contact28_28; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact28_28() { return &___m_Contact28_28; }
	inline void set_m_Contact28_28(ContactPoint2D_t3390240644  value)
	{
		___m_Contact28_28 = value;
	}

	inline static int32_t get_offset_of_m_Contact29_29() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact29_29)); }
	inline ContactPoint2D_t3390240644  get_m_Contact29_29() const { return ___m_Contact29_29; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact29_29() { return &___m_Contact29_29; }
	inline void set_m_Contact29_29(ContactPoint2D_t3390240644  value)
	{
		___m_Contact29_29 = value;
	}

	inline static int32_t get_offset_of_m_Contact30_30() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact30_30)); }
	inline ContactPoint2D_t3390240644  get_m_Contact30_30() const { return ___m_Contact30_30; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact30_30() { return &___m_Contact30_30; }
	inline void set_m_Contact30_30(ContactPoint2D_t3390240644  value)
	{
		___m_Contact30_30 = value;
	}

	inline static int32_t get_offset_of_m_Contact31_31() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact31_31)); }
	inline ContactPoint2D_t3390240644  get_m_Contact31_31() const { return ___m_Contact31_31; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact31_31() { return &___m_Contact31_31; }
	inline void set_m_Contact31_31(ContactPoint2D_t3390240644  value)
	{
		___m_Contact31_31 = value;
	}

	inline static int32_t get_offset_of_m_Contact32_32() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact32_32)); }
	inline ContactPoint2D_t3390240644  get_m_Contact32_32() const { return ___m_Contact32_32; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact32_32() { return &___m_Contact32_32; }
	inline void set_m_Contact32_32(ContactPoint2D_t3390240644  value)
	{
		___m_Contact32_32 = value;
	}

	inline static int32_t get_offset_of_m_Contact33_33() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact33_33)); }
	inline ContactPoint2D_t3390240644  get_m_Contact33_33() const { return ___m_Contact33_33; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact33_33() { return &___m_Contact33_33; }
	inline void set_m_Contact33_33(ContactPoint2D_t3390240644  value)
	{
		___m_Contact33_33 = value;
	}

	inline static int32_t get_offset_of_m_Contact34_34() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact34_34)); }
	inline ContactPoint2D_t3390240644  get_m_Contact34_34() const { return ___m_Contact34_34; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact34_34() { return &___m_Contact34_34; }
	inline void set_m_Contact34_34(ContactPoint2D_t3390240644  value)
	{
		___m_Contact34_34 = value;
	}

	inline static int32_t get_offset_of_m_Contact35_35() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact35_35)); }
	inline ContactPoint2D_t3390240644  get_m_Contact35_35() const { return ___m_Contact35_35; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact35_35() { return &___m_Contact35_35; }
	inline void set_m_Contact35_35(ContactPoint2D_t3390240644  value)
	{
		___m_Contact35_35 = value;
	}

	inline static int32_t get_offset_of_m_Contact36_36() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact36_36)); }
	inline ContactPoint2D_t3390240644  get_m_Contact36_36() const { return ___m_Contact36_36; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact36_36() { return &___m_Contact36_36; }
	inline void set_m_Contact36_36(ContactPoint2D_t3390240644  value)
	{
		___m_Contact36_36 = value;
	}

	inline static int32_t get_offset_of_m_Contact37_37() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact37_37)); }
	inline ContactPoint2D_t3390240644  get_m_Contact37_37() const { return ___m_Contact37_37; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact37_37() { return &___m_Contact37_37; }
	inline void set_m_Contact37_37(ContactPoint2D_t3390240644  value)
	{
		___m_Contact37_37 = value;
	}

	inline static int32_t get_offset_of_m_Contact38_38() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact38_38)); }
	inline ContactPoint2D_t3390240644  get_m_Contact38_38() const { return ___m_Contact38_38; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact38_38() { return &___m_Contact38_38; }
	inline void set_m_Contact38_38(ContactPoint2D_t3390240644  value)
	{
		___m_Contact38_38 = value;
	}

	inline static int32_t get_offset_of_m_Contact39_39() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact39_39)); }
	inline ContactPoint2D_t3390240644  get_m_Contact39_39() const { return ___m_Contact39_39; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact39_39() { return &___m_Contact39_39; }
	inline void set_m_Contact39_39(ContactPoint2D_t3390240644  value)
	{
		___m_Contact39_39 = value;
	}

	inline static int32_t get_offset_of_m_Contact40_40() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact40_40)); }
	inline ContactPoint2D_t3390240644  get_m_Contact40_40() const { return ___m_Contact40_40; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact40_40() { return &___m_Contact40_40; }
	inline void set_m_Contact40_40(ContactPoint2D_t3390240644  value)
	{
		___m_Contact40_40 = value;
	}

	inline static int32_t get_offset_of_m_Contact41_41() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact41_41)); }
	inline ContactPoint2D_t3390240644  get_m_Contact41_41() const { return ___m_Contact41_41; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact41_41() { return &___m_Contact41_41; }
	inline void set_m_Contact41_41(ContactPoint2D_t3390240644  value)
	{
		___m_Contact41_41 = value;
	}

	inline static int32_t get_offset_of_m_Contact42_42() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact42_42)); }
	inline ContactPoint2D_t3390240644  get_m_Contact42_42() const { return ___m_Contact42_42; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact42_42() { return &___m_Contact42_42; }
	inline void set_m_Contact42_42(ContactPoint2D_t3390240644  value)
	{
		___m_Contact42_42 = value;
	}

	inline static int32_t get_offset_of_m_Contact43_43() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact43_43)); }
	inline ContactPoint2D_t3390240644  get_m_Contact43_43() const { return ___m_Contact43_43; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact43_43() { return &___m_Contact43_43; }
	inline void set_m_Contact43_43(ContactPoint2D_t3390240644  value)
	{
		___m_Contact43_43 = value;
	}

	inline static int32_t get_offset_of_m_Contact44_44() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact44_44)); }
	inline ContactPoint2D_t3390240644  get_m_Contact44_44() const { return ___m_Contact44_44; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact44_44() { return &___m_Contact44_44; }
	inline void set_m_Contact44_44(ContactPoint2D_t3390240644  value)
	{
		___m_Contact44_44 = value;
	}

	inline static int32_t get_offset_of_m_Contact45_45() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact45_45)); }
	inline ContactPoint2D_t3390240644  get_m_Contact45_45() const { return ___m_Contact45_45; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact45_45() { return &___m_Contact45_45; }
	inline void set_m_Contact45_45(ContactPoint2D_t3390240644  value)
	{
		___m_Contact45_45 = value;
	}

	inline static int32_t get_offset_of_m_Contact46_46() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact46_46)); }
	inline ContactPoint2D_t3390240644  get_m_Contact46_46() const { return ___m_Contact46_46; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact46_46() { return &___m_Contact46_46; }
	inline void set_m_Contact46_46(ContactPoint2D_t3390240644  value)
	{
		___m_Contact46_46 = value;
	}

	inline static int32_t get_offset_of_m_Contact47_47() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact47_47)); }
	inline ContactPoint2D_t3390240644  get_m_Contact47_47() const { return ___m_Contact47_47; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact47_47() { return &___m_Contact47_47; }
	inline void set_m_Contact47_47(ContactPoint2D_t3390240644  value)
	{
		___m_Contact47_47 = value;
	}

	inline static int32_t get_offset_of_m_Contact48_48() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact48_48)); }
	inline ContactPoint2D_t3390240644  get_m_Contact48_48() const { return ___m_Contact48_48; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact48_48() { return &___m_Contact48_48; }
	inline void set_m_Contact48_48(ContactPoint2D_t3390240644  value)
	{
		___m_Contact48_48 = value;
	}

	inline static int32_t get_offset_of_m_Contact49_49() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact49_49)); }
	inline ContactPoint2D_t3390240644  get_m_Contact49_49() const { return ___m_Contact49_49; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact49_49() { return &___m_Contact49_49; }
	inline void set_m_Contact49_49(ContactPoint2D_t3390240644  value)
	{
		___m_Contact49_49 = value;
	}

	inline static int32_t get_offset_of_m_Contact50_50() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact50_50)); }
	inline ContactPoint2D_t3390240644  get_m_Contact50_50() const { return ___m_Contact50_50; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact50_50() { return &___m_Contact50_50; }
	inline void set_m_Contact50_50(ContactPoint2D_t3390240644  value)
	{
		___m_Contact50_50 = value;
	}

	inline static int32_t get_offset_of_m_Contact51_51() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact51_51)); }
	inline ContactPoint2D_t3390240644  get_m_Contact51_51() const { return ___m_Contact51_51; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact51_51() { return &___m_Contact51_51; }
	inline void set_m_Contact51_51(ContactPoint2D_t3390240644  value)
	{
		___m_Contact51_51 = value;
	}

	inline static int32_t get_offset_of_m_Contact52_52() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact52_52)); }
	inline ContactPoint2D_t3390240644  get_m_Contact52_52() const { return ___m_Contact52_52; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact52_52() { return &___m_Contact52_52; }
	inline void set_m_Contact52_52(ContactPoint2D_t3390240644  value)
	{
		___m_Contact52_52 = value;
	}

	inline static int32_t get_offset_of_m_Contact53_53() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact53_53)); }
	inline ContactPoint2D_t3390240644  get_m_Contact53_53() const { return ___m_Contact53_53; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact53_53() { return &___m_Contact53_53; }
	inline void set_m_Contact53_53(ContactPoint2D_t3390240644  value)
	{
		___m_Contact53_53 = value;
	}

	inline static int32_t get_offset_of_m_Contact54_54() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact54_54)); }
	inline ContactPoint2D_t3390240644  get_m_Contact54_54() const { return ___m_Contact54_54; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact54_54() { return &___m_Contact54_54; }
	inline void set_m_Contact54_54(ContactPoint2D_t3390240644  value)
	{
		___m_Contact54_54 = value;
	}

	inline static int32_t get_offset_of_m_Contact55_55() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact55_55)); }
	inline ContactPoint2D_t3390240644  get_m_Contact55_55() const { return ___m_Contact55_55; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact55_55() { return &___m_Contact55_55; }
	inline void set_m_Contact55_55(ContactPoint2D_t3390240644  value)
	{
		___m_Contact55_55 = value;
	}

	inline static int32_t get_offset_of_m_Contact56_56() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact56_56)); }
	inline ContactPoint2D_t3390240644  get_m_Contact56_56() const { return ___m_Contact56_56; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact56_56() { return &___m_Contact56_56; }
	inline void set_m_Contact56_56(ContactPoint2D_t3390240644  value)
	{
		___m_Contact56_56 = value;
	}

	inline static int32_t get_offset_of_m_Contact57_57() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact57_57)); }
	inline ContactPoint2D_t3390240644  get_m_Contact57_57() const { return ___m_Contact57_57; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact57_57() { return &___m_Contact57_57; }
	inline void set_m_Contact57_57(ContactPoint2D_t3390240644  value)
	{
		___m_Contact57_57 = value;
	}

	inline static int32_t get_offset_of_m_Contact58_58() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact58_58)); }
	inline ContactPoint2D_t3390240644  get_m_Contact58_58() const { return ___m_Contact58_58; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact58_58() { return &___m_Contact58_58; }
	inline void set_m_Contact58_58(ContactPoint2D_t3390240644  value)
	{
		___m_Contact58_58 = value;
	}

	inline static int32_t get_offset_of_m_Contact59_59() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact59_59)); }
	inline ContactPoint2D_t3390240644  get_m_Contact59_59() const { return ___m_Contact59_59; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact59_59() { return &___m_Contact59_59; }
	inline void set_m_Contact59_59(ContactPoint2D_t3390240644  value)
	{
		___m_Contact59_59 = value;
	}

	inline static int32_t get_offset_of_m_Contact60_60() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact60_60)); }
	inline ContactPoint2D_t3390240644  get_m_Contact60_60() const { return ___m_Contact60_60; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact60_60() { return &___m_Contact60_60; }
	inline void set_m_Contact60_60(ContactPoint2D_t3390240644  value)
	{
		___m_Contact60_60 = value;
	}

	inline static int32_t get_offset_of_m_Contact61_61() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact61_61)); }
	inline ContactPoint2D_t3390240644  get_m_Contact61_61() const { return ___m_Contact61_61; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact61_61() { return &___m_Contact61_61; }
	inline void set_m_Contact61_61(ContactPoint2D_t3390240644  value)
	{
		___m_Contact61_61 = value;
	}

	inline static int32_t get_offset_of_m_Contact62_62() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact62_62)); }
	inline ContactPoint2D_t3390240644  get_m_Contact62_62() const { return ___m_Contact62_62; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact62_62() { return &___m_Contact62_62; }
	inline void set_m_Contact62_62(ContactPoint2D_t3390240644  value)
	{
		___m_Contact62_62 = value;
	}

	inline static int32_t get_offset_of_m_Contact63_63() { return static_cast<int32_t>(offsetof(CachedContactPoints2D_t2523437281, ___m_Contact63_63)); }
	inline ContactPoint2D_t3390240644  get_m_Contact63_63() const { return ___m_Contact63_63; }
	inline ContactPoint2D_t3390240644 * get_address_of_m_Contact63_63() { return &___m_Contact63_63; }
	inline void set_m_Contact63_63(ContactPoint2D_t3390240644  value)
	{
		___m_Contact63_63 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDCONTACTPOINTS2D_T2523437281_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef PHYSICSMATERIAL2D_T331219942_H
#define PHYSICSMATERIAL2D_T331219942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PhysicsMaterial2D
struct  PhysicsMaterial2D_t331219942  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICSMATERIAL2D_T331219942_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef COLLISION2D_T2842956331_H
#define COLLISION2D_T2842956331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision2D
struct  Collision2D_t2842956331  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t2156229523  ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.CachedContactPoints2D UnityEngine.Collision2D::m_CachedContactPoints
	CachedContactPoints2D_t2523437281  ___m_CachedContactPoints_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContactArray
	ContactPoint2DU5BU5D_t96683501* ___m_LegacyContactArray_8;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_4() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_RelativeVelocity_4)); }
	inline Vector2_t2156229523  get_m_RelativeVelocity_4() const { return ___m_RelativeVelocity_4; }
	inline Vector2_t2156229523 * get_address_of_m_RelativeVelocity_4() { return &___m_RelativeVelocity_4; }
	inline void set_m_RelativeVelocity_4(Vector2_t2156229523  value)
	{
		___m_RelativeVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_5() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_Enabled_5)); }
	inline int32_t get_m_Enabled_5() const { return ___m_Enabled_5; }
	inline int32_t* get_address_of_m_Enabled_5() { return &___m_Enabled_5; }
	inline void set_m_Enabled_5(int32_t value)
	{
		___m_Enabled_5 = value;
	}

	inline static int32_t get_offset_of_m_ContactCount_6() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_ContactCount_6)); }
	inline int32_t get_m_ContactCount_6() const { return ___m_ContactCount_6; }
	inline int32_t* get_address_of_m_ContactCount_6() { return &___m_ContactCount_6; }
	inline void set_m_ContactCount_6(int32_t value)
	{
		___m_ContactCount_6 = value;
	}

	inline static int32_t get_offset_of_m_CachedContactPoints_7() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_CachedContactPoints_7)); }
	inline CachedContactPoints2D_t2523437281  get_m_CachedContactPoints_7() const { return ___m_CachedContactPoints_7; }
	inline CachedContactPoints2D_t2523437281 * get_address_of_m_CachedContactPoints_7() { return &___m_CachedContactPoints_7; }
	inline void set_m_CachedContactPoints_7(CachedContactPoints2D_t2523437281  value)
	{
		___m_CachedContactPoints_7 = value;
	}

	inline static int32_t get_offset_of_m_LegacyContactArray_8() { return static_cast<int32_t>(offsetof(Collision2D_t2842956331, ___m_LegacyContactArray_8)); }
	inline ContactPoint2DU5BU5D_t96683501* get_m_LegacyContactArray_8() const { return ___m_LegacyContactArray_8; }
	inline ContactPoint2DU5BU5D_t96683501** get_address_of_m_LegacyContactArray_8() { return &___m_LegacyContactArray_8; }
	inline void set_m_LegacyContactArray_8(ContactPoint2DU5BU5D_t96683501* value)
	{
		___m_LegacyContactArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LegacyContactArray_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t2842956331_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t2156229523  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	CachedContactPoints2D_t2523437281  ___m_CachedContactPoints_7;
	ContactPoint2D_t3390240644 * ___m_LegacyContactArray_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t2842956331_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_t2156229523  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	CachedContactPoints2D_t2523437281  ___m_CachedContactPoints_7;
	ContactPoint2D_t3390240644 * ___m_LegacyContactArray_8;
};
#endif // COLLISION2D_T2842956331_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef JOINT2D_T4180440564_H
#define JOINT2D_T4180440564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Joint2D
struct  Joint2D_t4180440564  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINT2D_T4180440564_H
#ifndef ANCHOREDJOINT2D_T3747633318_H
#define ANCHOREDJOINT2D_T3747633318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnchoredJoint2D
struct  AnchoredJoint2D_t3747633318  : public Joint2D_t4180440564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOREDJOINT2D_T3747633318_H
#ifndef HINGEJOINT2D_T3442948838_H
#define HINGEJOINT2D_T3442948838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HingeJoint2D
struct  HingeJoint2D_t3442948838  : public AnchoredJoint2D_t3747633318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HINGEJOINT2D_T3442948838_H
#ifndef WHEELJOINT2D_T1756757149_H
#define WHEELJOINT2D_T1756757149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WheelJoint2D
struct  WheelJoint2D_t1756757149  : public AnchoredJoint2D_t3747633318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHEELJOINT2D_T1756757149_H
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t96683501  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ContactPoint2D_t3390240644  m_Items[1];

public:
	inline ContactPoint2D_t3390240644  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContactPoint2D_t3390240644 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContactPoint2D_t3390240644  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ContactPoint2D_t3390240644  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContactPoint2D_t3390240644 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContactPoint2D_t3390240644  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastHit2D_t2279581989  m_Items[1];

public:
	inline RaycastHit2D_t2279581989  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit2D_t2279581989 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit2D_t2279581989  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit2D_t2279581989  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit2D_t2279581989 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit2D_t2279581989  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t1693969295  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider2D_t2806799626 * m_Items[1];

public:
	inline Collider2D_t2806799626 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider2D_t2806799626 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider2D_t2806799626 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider2D_t2806799626 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider2D_t2806799626 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider2D_t2806799626 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);

// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ContactPoint2D_t3390240644  CachedContactPoints2D_get_Item_m3233576848 (CachedContactPoints2D_t2523437281 * __this, int32_t ___i0, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Behaviour__ctor_m346897018 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Object::FindObjectFromInstanceID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * Object_FindObjectFromInstanceID_m235838713 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern "C" IL2CPP_METHOD_ATTR Rigidbody2D_t939494601 * Collision2D_get_rigidbody_m716857034 (Collision2D_t2842956331 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C" IL2CPP_METHOD_ATTR Collider2D_t2806799626 * Collision2D_get_collider_m4087612183 (Collision2D_t2842956331 * __this, const RuntimeMethod* method);
// System.Boolean System.Single::IsNaN(System.Single)
extern "C" IL2CPP_METHOD_ATTR bool Single_IsNaN_m4024467661 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern "C" IL2CPP_METHOD_ATTR void ContactFilter2D_CheckConsistency_m222060761 (ContactFilter2D_t3805203441 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern "C" IL2CPP_METHOD_ATTR void ContactFilter2D_SetLayerMask_m3440334821 (ContactFilter2D_t3805203441 * __this, LayerMask_t3493934918  ___layerMask0, const RuntimeMethod* method);
// System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void ContactFilter2D_SetDepth_m3691256496 (ContactFilter2D_t3805203441 * __this, float ___minDepth0, float ___maxDepth1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern "C" IL2CPP_METHOD_ATTR bool Physics2D_get_queriesHitTriggers_m1249674012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C" IL2CPP_METHOD_ATTR LayerMask_t3493934918  LayerMask_op_Implicit_m90232283 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  ContactPoint2D_get_point_m346644250 (ContactPoint2D_t3390240644 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_normal()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  ContactPoint2D_get_normal_m641220705 (ContactPoint2D_t3390240644 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HingeJoint2D::get_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_get_motor_Injected_m3349627322 (HingeJoint2D_t3442948838 * __this, JointMotor2D_t142461323 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.HingeJoint2D::set_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_motor_Injected_m265480475 (HingeJoint2D_t3442948838 * __this, JointMotor2D_t142461323 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HingeJoint2D::get_limits_Injected(UnityEngine.JointAngleLimits2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_get_limits_Injected_m407444726 (HingeJoint2D_t3442948838 * __this, JointAngleLimits2D_t972279075 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.HingeJoint2D::set_limits_Injected(UnityEngine.JointAngleLimits2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_limits_Injected_m3913299803 (HingeJoint2D_t3442948838 * __this, JointAngleLimits2D_t972279075 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Joint2D::get_reactionForce_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Joint2D_get_reactionForce_Injected_m1258031357 (Joint2D_t4180440564 * __this, Vector2_t2156229523 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.JointAngleLimits2D::set_min(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointAngleLimits2D_set_min_m2880987945 (JointAngleLimits2D_t972279075 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointAngleLimits2D::set_max(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointAngleLimits2D_set_max_m2252990911 (JointAngleLimits2D_t972279075 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointMotor2D::set_motorSpeed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointMotor2D_set_motorSpeed_m1876890553 (JointMotor2D_t142461323 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointMotor2D::set_maxMotorTorque(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointMotor2D_set_maxMotorTorque_m668541733 (JointMotor2D_t142461323 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointSuspension2D::set_dampingRatio(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointSuspension2D_set_dampingRatio_m2619162143 (JointSuspension2D_t3350541332 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointSuspension2D::set_frequency(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointSuspension2D_set_frequency_m305231415 (JointSuspension2D_t3350541332 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.JointSuspension2D::set_angle(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointSuspension2D_set_angle_m4263311628 (JointSuspension2D_t3350541332 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Physics2D::set_gravity_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_set_gravity_Injected_m2797569484 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___value0, const RuntimeMethod* method);
// UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR ContactFilter2D_t3805203441  ContactFilter2D_CreateLegacyFilter_m1873816896 (RuntimeObject * __this /* static, unused */, int32_t ___layerMask0, float ___minDepth1, float ___maxDepth2, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast_Internal(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Linecast_Internal_m2067808837 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, ContactFilter2D_t3805203441  ___contactFilter2, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll_Internal(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_LinecastAll_Internal_m604725090 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, ContactFilter2D_t3805203441  ___contactFilter2, const RuntimeMethod* method);
// System.Void UnityEngine.Physics2D::Linecast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_Linecast_Internal_Injected_m4053345089 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___start0, Vector2_t2156229523 * ___end1, ContactFilter2D_t3805203441 * ___contactFilter2, RaycastHit2D_t2279581989 * ___ret3, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_LinecastAll_Internal_Injected_m3181521651 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___start0, Vector2_t2156229523 * ___end1, ContactFilter2D_t3805203441 * ___contactFilter2, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_Internal_m2084501876 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, const RuntimeMethod* method);
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_RaycastNonAlloc_Internal_m4193835177 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_Internal_m3190530210 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, const RuntimeMethod* method);
// System.Void UnityEngine.Physics2D::Raycast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_Raycast_Internal_Injected_m1647830047 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2D_t2279581989 * ___ret4, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_Internal_Injected_m2949536273 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, const RuntimeMethod* method);
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_RaycastNonAlloc_Internal_Injected_m1762831718 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Ray_get_origin_m2819290985 (Ray_t3785851493 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Ray_get_direction_m761601601 (Ray_t3785851493 * __this, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection_Internal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_GetRayIntersection_Internal_m1872178675 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_Internal_m643548862 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method);
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc_Internal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___distance2, int32_t ___layerMask3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method);
// System.Void UnityEngine.Physics2D::GetRayIntersection_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_GetRayIntersection_Internal_Injected_m3464046400 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___distance2, int32_t ___layerMask3, RaycastHit2D_t2279581989 * ___ret4, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_Internal_Injected_m1793592321 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method);
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_Internal_Injected_m3091622535 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___distance2, int32_t ___layerMask3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll_Internal(UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapPointAll_Internal_m3057315634 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, ContactFilter2D_t3805203441  ___contactFilter1, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapPointAll_Internal_Injected_m3237007287 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, ContactFilter2D_t3805203441 * ___contactFilter1, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal(UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_Internal_m1158092355 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, float ___radius1, ContactFilter2D_t3805203441  ___contactFilter2, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_Internal_Injected_m1699159137 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, float ___radius1, ContactFilter2D_t3805203441 * ___contactFilter2, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapBoxAll_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapBoxAll_Internal_m2950777821 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, Vector2_t2156229523  ___size1, float ___angle2, ContactFilter2D_t3805203441  ___contactFilter3, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapBoxAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapBoxAll_Internal_Injected_m50526180 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, Vector2_t2156229523 * ___size1, float ___angle2, ContactFilter2D_t3805203441 * ___contactFilter3, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAllToBox_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapAreaAllToBox_Internal_m4180410992 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___pointA0, Vector2_t2156229523  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapBoxAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapBoxAll_m2279087762 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, Vector2_t2156229523  ___size1, float ___angle2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
inline void List_1__ctor_m2466177744 (List_1_t2411569343 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2411569343 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m1087895580 (Object_t631007953 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.PhysicsMaterial2D::Create_Internal(UnityEngine.PhysicsMaterial2D,System.String)
extern "C" IL2CPP_METHOD_ATTR void PhysicsMaterial2D_Create_Internal_m3464072492 (RuntimeObject * __this /* static, unused */, PhysicsMaterial2D_t331219942 * ___scriptMaterial0, String_t* ___name1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RaycastHit2D_get_point_m1586138107 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RaycastHit2D_get_normal_m772343376 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C" IL2CPP_METHOD_ATTR float RaycastHit2D_get_distance_m382898860 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" IL2CPP_METHOD_ATTR float RaycastHit2D_get_fraction_m2673798080 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" IL2CPP_METHOD_ATTR Collider2D_t2806799626 * RaycastHit2D_get_collider_m1549426026 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" IL2CPP_METHOD_ATTR Rigidbody2D_t939494601 * Collider2D_get_attachedRigidbody_m729069942 (Collider2D_t2806799626 * __this, const RuntimeMethod* method);
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" IL2CPP_METHOD_ATTR Rigidbody2D_t939494601 * RaycastHit2D_get_rigidbody_m942928380 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * RaycastHit2D_get_transform_m2048267409 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Component::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Component__ctor_m1928064382 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_get_velocity_Injected_m2170787103 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_Injected_m1505462814 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___value0, const RuntimeMethod* method);
// UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
extern "C" IL2CPP_METHOD_ATTR int32_t Rigidbody2D_get_bodyType_m541082067 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_bodyType_m1814100804 (Rigidbody2D_t939494601 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_Injected_m534098484 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___force0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddRelativeForce_Injected_m3238506296 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___relativeForce0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForceAtPosition_Injected_m220971265 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___force0, Vector2_t2156229523 * ___position1, int32_t ___mode2, const RuntimeMethod* method);
// System.Void UnityEngine.WheelJoint2D::get_suspension_Injected(UnityEngine.JointSuspension2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_get_suspension_Injected_m1956465358 (WheelJoint2D_t1756757149 * __this, JointSuspension2D_t3350541332 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.WheelJoint2D::set_suspension_Injected(UnityEngine.JointSuspension2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_suspension_Injected_m332949253 (WheelJoint2D_t1756757149 * __this, JointSuspension2D_t3350541332 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.WheelJoint2D::get_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_get_motor_Injected_m1737353361 (WheelJoint2D_t1756757149 * __this, JointMotor2D_t142461323 * ___ret0, const RuntimeMethod* method);
// System.Void UnityEngine.WheelJoint2D::set_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_motor_Injected_m3222271241 (WheelJoint2D_t1756757149 * __this, JointMotor2D_t142461323 * ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.ContactPoint2D UnityEngine.CachedContactPoints2D::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR ContactPoint2D_t3390240644  CachedContactPoints2D_get_Item_m3233576848 (CachedContactPoints2D_t2523437281 * __this, int32_t ___i0, const RuntimeMethod* method)
{
	ContactPoint2D_t3390240644 * V_0 = NULL;
	ContactPoint2D_t3390240644  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ContactPoint2D_t3390240644 * L_0 = __this->get_address_of_m_Contact0_0();
		V_0 = (ContactPoint2D_t3390240644 *)L_0;
		ContactPoint2D_t3390240644 * L_1 = V_0;
		int32_t L_2 = ___i0;
		uint32_t L_3 = sizeof(ContactPoint2D_t3390240644 );
		V_1 = (*(ContactPoint2D_t3390240644 *)((ContactPoint2D_t3390240644 *)il2cpp_codegen_add((intptr_t)L_1, (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)(((intptr_t)L_2)), (int32_t)L_3)))));
		goto IL_001f;
	}

IL_001f:
	{
		ContactPoint2D_t3390240644  L_4 = V_1;
		return L_4;
	}
}
extern "C"  ContactPoint2D_t3390240644  CachedContactPoints2D_get_Item_m3233576848_AdjustorThunk (RuntimeObject * __this, int32_t ___i0, const RuntimeMethod* method)
{
	CachedContactPoints2D_t2523437281 * _thisAdjusted = reinterpret_cast<CachedContactPoints2D_t2523437281 *>(__this + 1);
	return CachedContactPoints2D_get_Item_m3233576848(_thisAdjusted, ___i0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Collider2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Collider2D__ctor_m2102044678 (Collider2D_t2806799626 * __this, const RuntimeMethod* method)
{
	{
		Behaviour__ctor_m346897018(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Collider2D_set_isTrigger_m2923405871 (Collider2D_t2806799626 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*Collider2D_set_isTrigger_m2923405871_ftn) (Collider2D_t2806799626 *, bool);
	static Collider2D_set_isTrigger_m2923405871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_set_isTrigger_m2923405871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::set_isTrigger(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" IL2CPP_METHOD_ATTR Rigidbody2D_t939494601 * Collider2D_get_attachedRigidbody_m729069942 (Collider2D_t2806799626 * __this, const RuntimeMethod* method)
{
	typedef Rigidbody2D_t939494601 * (*Collider2D_get_attachedRigidbody_m729069942_ftn) (Collider2D_t2806799626 *);
	static Collider2D_get_attachedRigidbody_m729069942_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m729069942_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	Rigidbody2D_t939494601 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Collider2D::get_shapeCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Collider2D_get_shapeCount_m4070377044 (Collider2D_t2806799626 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Collider2D_get_shapeCount_m4070377044_ftn) (Collider2D_t2806799626 *);
	static Collider2D_get_shapeCount_m4070377044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_shapeCount_m4070377044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_shapeCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
extern "C" IL2CPP_METHOD_ATTR PhysicsMaterial2D_t331219942 * Collider2D_get_sharedMaterial_m2442303403 (Collider2D_t2806799626 * __this, const RuntimeMethod* method)
{
	typedef PhysicsMaterial2D_t331219942 * (*Collider2D_get_sharedMaterial_m2442303403_ftn) (Collider2D_t2806799626 *);
	static Collider2D_get_sharedMaterial_m2442303403_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_sharedMaterial_m2442303403_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_sharedMaterial()");
	PhysicsMaterial2D_t331219942 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_pinvoke(const Collision2D_t2842956331& unmarshaled, Collision2D_t2842956331_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_LegacyContactArray_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LegacyContactArray' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LegacyContactArray_8Exception, NULL, NULL);
}
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(const Collision2D_t2842956331_marshaled_pinvoke& marshaled, Collision2D_t2842956331& unmarshaled)
{
	Exception_t* ___m_LegacyContactArray_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LegacyContactArray' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LegacyContactArray_8Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(Collision2D_t2842956331_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_com(const Collision2D_t2842956331& unmarshaled, Collision2D_t2842956331_marshaled_com& marshaled)
{
	Exception_t* ___m_LegacyContactArray_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LegacyContactArray' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LegacyContactArray_8Exception, NULL, NULL);
}
extern "C" void Collision2D_t2842956331_marshal_com_back(const Collision2D_t2842956331_marshaled_com& marshaled, Collision2D_t2842956331& unmarshaled)
{
	Exception_t* ___m_LegacyContactArray_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LegacyContactArray' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LegacyContactArray_8Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_com_cleanup(Collision2D_t2842956331_marshaled_com& marshaled)
{
}
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C" IL2CPP_METHOD_ATTR Collider2D_t2806799626 * Collision2D_get_collider_m4087612183 (Collision2D_t2842956331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_collider_m4087612183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2D_t2806799626 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_Collider_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_1 = Object_FindObjectFromInstanceID_m235838713(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Collider2D_t2806799626 *)IsInstClass((RuntimeObject*)L_1, Collider2D_t2806799626_il2cpp_TypeInfo_var));
		goto IL_0017;
	}

IL_0017:
	{
		Collider2D_t2806799626 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern "C" IL2CPP_METHOD_ATTR Rigidbody2D_t939494601 * Collision2D_get_rigidbody_m716857034 (Collision2D_t2842956331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_rigidbody_m716857034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_1 = Object_FindObjectFromInstanceID_m235838713(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Rigidbody2D_t939494601 *)IsInstSealed((RuntimeObject*)L_1, Rigidbody2D_t939494601_il2cpp_TypeInfo_var));
		goto IL_0017;
	}

IL_0017:
	{
		Rigidbody2D_t939494601 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Collision2D_get_gameObject_m1443495885 (Collision2D_t2842956331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_gameObject_m1443495885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * G_B3_0 = NULL;
	{
		Rigidbody2D_t939494601 * L_0 = Collision2D_get_rigidbody_m716857034(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Rigidbody2D_t939494601 * L_2 = Collision2D_get_rigidbody_m716857034(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002d;
	}

IL_0022:
	{
		Collider2D_t2806799626 * L_4 = Collision2D_get_collider_m4087612183(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002d:
	{
		V_0 = G_B3_0;
		goto IL_0033;
	}

IL_0033:
	{
		GameObject_t1113636619 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Collision2D_get_relativeVelocity_m2786346242 (Collision2D_t2842956331 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_RelativeVelocity_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::get_contacts()
extern "C" IL2CPP_METHOD_ATTR ContactPoint2DU5BU5D_t96683501* Collision2D_get_contacts_m696521713 (Collision2D_t2842956331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_contacts_m696521713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ContactPoint2DU5BU5D_t96683501* V_1 = NULL;
	{
		ContactPoint2DU5BU5D_t96683501* L_0 = __this->get_m_LegacyContactArray_8();
		if (L_0)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_1 = __this->get_m_ContactCount_6();
		ContactPoint2DU5BU5D_t96683501* L_2 = (ContactPoint2DU5BU5D_t96683501*)SZArrayNew(ContactPoint2DU5BU5D_t96683501_il2cpp_TypeInfo_var, (uint32_t)L_1);
		__this->set_m_LegacyContactArray_8(L_2);
		int32_t L_3 = __this->get_m_ContactCount_6();
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0064;
		}
	}
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_0033:
	{
		ContactPoint2DU5BU5D_t96683501* L_4 = __this->get_m_LegacyContactArray_8();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		CachedContactPoints2D_t2523437281 * L_6 = __this->get_address_of_m_CachedContactPoints_7();
		int32_t L_7 = V_0;
		ContactPoint2D_t3390240644  L_8 = CachedContactPoints2D_get_Item_m3233576848((CachedContactPoints2D_t2523437281 *)L_6, L_7, /*hidden argument*/NULL);
		*(ContactPoint2D_t3390240644 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))) = L_8;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0056:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_m_ContactCount_6();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0033;
		}
	}
	{
	}

IL_0064:
	{
	}

IL_0065:
	{
		ContactPoint2DU5BU5D_t96683501* L_12 = __this->get_m_LegacyContactArray_8();
		V_1 = L_12;
		goto IL_0071;
	}

IL_0071:
	{
		ContactPoint2DU5BU5D_t96683501* L_13 = V_1;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(const ContactFilter2D_t3805203441& unmarshaled, ContactFilter2D_t3805203441_marshaled_pinvoke& marshaled)
{
	marshaled.___useTriggers_0 = static_cast<int32_t>(unmarshaled.get_useTriggers_0());
	marshaled.___useLayerMask_1 = static_cast<int32_t>(unmarshaled.get_useLayerMask_1());
	marshaled.___useDepth_2 = static_cast<int32_t>(unmarshaled.get_useDepth_2());
	marshaled.___useOutsideDepth_3 = static_cast<int32_t>(unmarshaled.get_useOutsideDepth_3());
	marshaled.___useNormalAngle_4 = static_cast<int32_t>(unmarshaled.get_useNormalAngle_4());
	marshaled.___useOutsideNormalAngle_5 = static_cast<int32_t>(unmarshaled.get_useOutsideNormalAngle_5());
	marshaled.___layerMask_6 = unmarshaled.get_layerMask_6();
	marshaled.___minDepth_7 = unmarshaled.get_minDepth_7();
	marshaled.___maxDepth_8 = unmarshaled.get_maxDepth_8();
	marshaled.___minNormalAngle_9 = unmarshaled.get_minNormalAngle_9();
	marshaled.___maxNormalAngle_10 = unmarshaled.get_maxNormalAngle_10();
}
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(const ContactFilter2D_t3805203441_marshaled_pinvoke& marshaled, ContactFilter2D_t3805203441& unmarshaled)
{
	bool unmarshaled_useTriggers_temp_0 = false;
	unmarshaled_useTriggers_temp_0 = static_cast<bool>(marshaled.___useTriggers_0);
	unmarshaled.set_useTriggers_0(unmarshaled_useTriggers_temp_0);
	bool unmarshaled_useLayerMask_temp_1 = false;
	unmarshaled_useLayerMask_temp_1 = static_cast<bool>(marshaled.___useLayerMask_1);
	unmarshaled.set_useLayerMask_1(unmarshaled_useLayerMask_temp_1);
	bool unmarshaled_useDepth_temp_2 = false;
	unmarshaled_useDepth_temp_2 = static_cast<bool>(marshaled.___useDepth_2);
	unmarshaled.set_useDepth_2(unmarshaled_useDepth_temp_2);
	bool unmarshaled_useOutsideDepth_temp_3 = false;
	unmarshaled_useOutsideDepth_temp_3 = static_cast<bool>(marshaled.___useOutsideDepth_3);
	unmarshaled.set_useOutsideDepth_3(unmarshaled_useOutsideDepth_temp_3);
	bool unmarshaled_useNormalAngle_temp_4 = false;
	unmarshaled_useNormalAngle_temp_4 = static_cast<bool>(marshaled.___useNormalAngle_4);
	unmarshaled.set_useNormalAngle_4(unmarshaled_useNormalAngle_temp_4);
	bool unmarshaled_useOutsideNormalAngle_temp_5 = false;
	unmarshaled_useOutsideNormalAngle_temp_5 = static_cast<bool>(marshaled.___useOutsideNormalAngle_5);
	unmarshaled.set_useOutsideNormalAngle_5(unmarshaled_useOutsideNormalAngle_temp_5);
	LayerMask_t3493934918  unmarshaled_layerMask_temp_6;
	memset(&unmarshaled_layerMask_temp_6, 0, sizeof(unmarshaled_layerMask_temp_6));
	unmarshaled_layerMask_temp_6 = marshaled.___layerMask_6;
	unmarshaled.set_layerMask_6(unmarshaled_layerMask_temp_6);
	float unmarshaled_minDepth_temp_7 = 0.0f;
	unmarshaled_minDepth_temp_7 = marshaled.___minDepth_7;
	unmarshaled.set_minDepth_7(unmarshaled_minDepth_temp_7);
	float unmarshaled_maxDepth_temp_8 = 0.0f;
	unmarshaled_maxDepth_temp_8 = marshaled.___maxDepth_8;
	unmarshaled.set_maxDepth_8(unmarshaled_maxDepth_temp_8);
	float unmarshaled_minNormalAngle_temp_9 = 0.0f;
	unmarshaled_minNormalAngle_temp_9 = marshaled.___minNormalAngle_9;
	unmarshaled.set_minNormalAngle_9(unmarshaled_minNormalAngle_temp_9);
	float unmarshaled_maxNormalAngle_temp_10 = 0.0f;
	unmarshaled_maxNormalAngle_temp_10 = marshaled.___maxNormalAngle_10;
	unmarshaled.set_maxNormalAngle_10(unmarshaled_maxNormalAngle_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(ContactFilter2D_t3805203441_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_com(const ContactFilter2D_t3805203441& unmarshaled, ContactFilter2D_t3805203441_marshaled_com& marshaled)
{
	marshaled.___useTriggers_0 = static_cast<int32_t>(unmarshaled.get_useTriggers_0());
	marshaled.___useLayerMask_1 = static_cast<int32_t>(unmarshaled.get_useLayerMask_1());
	marshaled.___useDepth_2 = static_cast<int32_t>(unmarshaled.get_useDepth_2());
	marshaled.___useOutsideDepth_3 = static_cast<int32_t>(unmarshaled.get_useOutsideDepth_3());
	marshaled.___useNormalAngle_4 = static_cast<int32_t>(unmarshaled.get_useNormalAngle_4());
	marshaled.___useOutsideNormalAngle_5 = static_cast<int32_t>(unmarshaled.get_useOutsideNormalAngle_5());
	marshaled.___layerMask_6 = unmarshaled.get_layerMask_6();
	marshaled.___minDepth_7 = unmarshaled.get_minDepth_7();
	marshaled.___maxDepth_8 = unmarshaled.get_maxDepth_8();
	marshaled.___minNormalAngle_9 = unmarshaled.get_minNormalAngle_9();
	marshaled.___maxNormalAngle_10 = unmarshaled.get_maxNormalAngle_10();
}
extern "C" void ContactFilter2D_t3805203441_marshal_com_back(const ContactFilter2D_t3805203441_marshaled_com& marshaled, ContactFilter2D_t3805203441& unmarshaled)
{
	bool unmarshaled_useTriggers_temp_0 = false;
	unmarshaled_useTriggers_temp_0 = static_cast<bool>(marshaled.___useTriggers_0);
	unmarshaled.set_useTriggers_0(unmarshaled_useTriggers_temp_0);
	bool unmarshaled_useLayerMask_temp_1 = false;
	unmarshaled_useLayerMask_temp_1 = static_cast<bool>(marshaled.___useLayerMask_1);
	unmarshaled.set_useLayerMask_1(unmarshaled_useLayerMask_temp_1);
	bool unmarshaled_useDepth_temp_2 = false;
	unmarshaled_useDepth_temp_2 = static_cast<bool>(marshaled.___useDepth_2);
	unmarshaled.set_useDepth_2(unmarshaled_useDepth_temp_2);
	bool unmarshaled_useOutsideDepth_temp_3 = false;
	unmarshaled_useOutsideDepth_temp_3 = static_cast<bool>(marshaled.___useOutsideDepth_3);
	unmarshaled.set_useOutsideDepth_3(unmarshaled_useOutsideDepth_temp_3);
	bool unmarshaled_useNormalAngle_temp_4 = false;
	unmarshaled_useNormalAngle_temp_4 = static_cast<bool>(marshaled.___useNormalAngle_4);
	unmarshaled.set_useNormalAngle_4(unmarshaled_useNormalAngle_temp_4);
	bool unmarshaled_useOutsideNormalAngle_temp_5 = false;
	unmarshaled_useOutsideNormalAngle_temp_5 = static_cast<bool>(marshaled.___useOutsideNormalAngle_5);
	unmarshaled.set_useOutsideNormalAngle_5(unmarshaled_useOutsideNormalAngle_temp_5);
	LayerMask_t3493934918  unmarshaled_layerMask_temp_6;
	memset(&unmarshaled_layerMask_temp_6, 0, sizeof(unmarshaled_layerMask_temp_6));
	unmarshaled_layerMask_temp_6 = marshaled.___layerMask_6;
	unmarshaled.set_layerMask_6(unmarshaled_layerMask_temp_6);
	float unmarshaled_minDepth_temp_7 = 0.0f;
	unmarshaled_minDepth_temp_7 = marshaled.___minDepth_7;
	unmarshaled.set_minDepth_7(unmarshaled_minDepth_temp_7);
	float unmarshaled_maxDepth_temp_8 = 0.0f;
	unmarshaled_maxDepth_temp_8 = marshaled.___maxDepth_8;
	unmarshaled.set_maxDepth_8(unmarshaled_maxDepth_temp_8);
	float unmarshaled_minNormalAngle_temp_9 = 0.0f;
	unmarshaled_minNormalAngle_temp_9 = marshaled.___minNormalAngle_9;
	unmarshaled.set_minNormalAngle_9(unmarshaled_minNormalAngle_temp_9);
	float unmarshaled_maxNormalAngle_temp_10 = 0.0f;
	unmarshaled_maxNormalAngle_temp_10 = marshaled.___maxNormalAngle_10;
	unmarshaled.set_maxNormalAngle_10(unmarshaled_maxNormalAngle_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_com_cleanup(ContactFilter2D_t3805203441_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern "C" IL2CPP_METHOD_ATTR void ContactFilter2D_CheckConsistency_m222060761 (ContactFilter2D_t3805203441 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContactFilter2D_CheckConsistency_m222060761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	ContactFilter2D_t3805203441 * G_B3_0 = NULL;
	ContactFilter2D_t3805203441 * G_B1_0 = NULL;
	ContactFilter2D_t3805203441 * G_B2_0 = NULL;
	ContactFilter2D_t3805203441 * G_B4_0 = NULL;
	float G_B5_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B5_1 = NULL;
	ContactFilter2D_t3805203441 * G_B8_0 = NULL;
	ContactFilter2D_t3805203441 * G_B6_0 = NULL;
	ContactFilter2D_t3805203441 * G_B7_0 = NULL;
	ContactFilter2D_t3805203441 * G_B9_0 = NULL;
	float G_B10_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B10_1 = NULL;
	ContactFilter2D_t3805203441 * G_B14_0 = NULL;
	ContactFilter2D_t3805203441 * G_B13_0 = NULL;
	float G_B15_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B15_1 = NULL;
	ContactFilter2D_t3805203441 * G_B17_0 = NULL;
	ContactFilter2D_t3805203441 * G_B16_0 = NULL;
	float G_B18_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B18_1 = NULL;
	{
		float L_0 = __this->get_minDepth_7();
		G_B1_0 = __this;
		if ((((float)L_0) == ((float)(-std::numeric_limits<float>::infinity()))))
		{
			G_B3_0 = __this;
			goto IL_0032;
		}
	}
	{
		float L_1 = __this->get_minDepth_7();
		G_B2_0 = G_B1_0;
		if ((((float)L_1) == ((float)(std::numeric_limits<float>::infinity()))))
		{
			G_B3_0 = G_B1_0;
			goto IL_0032;
		}
	}
	{
		float L_2 = __this->get_minDepth_7();
		bool L_3 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = G_B2_0;
		if (!L_3)
		{
			G_B4_0 = G_B2_0;
			goto IL_003c;
		}
	}

IL_0032:
	{
		G_B5_0 = (-std::numeric_limits<float>::max());
		G_B5_1 = G_B3_0;
		goto IL_0042;
	}

IL_003c:
	{
		float L_4 = __this->get_minDepth_7();
		G_B5_0 = L_4;
		G_B5_1 = G_B4_0;
	}

IL_0042:
	{
		G_B5_1->set_minDepth_7(G_B5_0);
		float L_5 = __this->get_maxDepth_8();
		G_B6_0 = __this;
		if ((((float)L_5) == ((float)(-std::numeric_limits<float>::infinity()))))
		{
			G_B8_0 = __this;
			goto IL_0078;
		}
	}
	{
		float L_6 = __this->get_maxDepth_8();
		G_B7_0 = G_B6_0;
		if ((((float)L_6) == ((float)(std::numeric_limits<float>::infinity()))))
		{
			G_B8_0 = G_B6_0;
			goto IL_0078;
		}
	}
	{
		float L_7 = __this->get_maxDepth_8();
		bool L_8 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
		if (!L_8)
		{
			G_B9_0 = G_B7_0;
			goto IL_0082;
		}
	}

IL_0078:
	{
		G_B10_0 = (std::numeric_limits<float>::max());
		G_B10_1 = G_B8_0;
		goto IL_0088;
	}

IL_0082:
	{
		float L_9 = __this->get_maxDepth_8();
		G_B10_0 = L_9;
		G_B10_1 = G_B9_0;
	}

IL_0088:
	{
		G_B10_1->set_maxDepth_8(G_B10_0);
		float L_10 = __this->get_minDepth_7();
		float L_11 = __this->get_maxDepth_8();
		if ((!(((float)L_10) > ((float)L_11))))
		{
			goto IL_00ba;
		}
	}
	{
		float L_12 = __this->get_minDepth_7();
		V_0 = L_12;
		float L_13 = __this->get_maxDepth_8();
		__this->set_minDepth_7(L_13);
		float L_14 = V_0;
		__this->set_maxDepth_8(L_14);
	}

IL_00ba:
	{
		float L_15 = __this->get_minNormalAngle_9();
		bool L_16 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		G_B13_0 = __this;
		if (!L_16)
		{
			G_B14_0 = __this;
			goto IL_00d5;
		}
	}
	{
		G_B15_0 = (0.0f);
		G_B15_1 = G_B13_0;
		goto IL_00ea;
	}

IL_00d5:
	{
		float L_17 = __this->get_minNormalAngle_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_17, (0.0f), (359.9999f), /*hidden argument*/NULL);
		G_B15_0 = L_18;
		G_B15_1 = G_B14_0;
	}

IL_00ea:
	{
		G_B15_1->set_minNormalAngle_9(G_B15_0);
		float L_19 = __this->get_maxNormalAngle_10();
		bool L_20 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		G_B16_0 = __this;
		if (!L_20)
		{
			G_B17_0 = __this;
			goto IL_010a;
		}
	}
	{
		G_B18_0 = (359.9999f);
		G_B18_1 = G_B16_0;
		goto IL_011f;
	}

IL_010a:
	{
		float L_21 = __this->get_maxNormalAngle_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_21, (0.0f), (359.9999f), /*hidden argument*/NULL);
		G_B18_0 = L_22;
		G_B18_1 = G_B17_0;
	}

IL_011f:
	{
		G_B18_1->set_maxNormalAngle_10(G_B18_0);
		float L_23 = __this->get_minNormalAngle_9();
		float L_24 = __this->get_maxNormalAngle_10();
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_0151;
		}
	}
	{
		float L_25 = __this->get_minNormalAngle_9();
		V_1 = L_25;
		float L_26 = __this->get_maxNormalAngle_10();
		__this->set_minNormalAngle_9(L_26);
		float L_27 = V_1;
		__this->set_maxNormalAngle_10(L_27);
	}

IL_0151:
	{
		return;
	}
}
extern "C"  void ContactFilter2D_CheckConsistency_m222060761_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ContactFilter2D_t3805203441 * _thisAdjusted = reinterpret_cast<ContactFilter2D_t3805203441 *>(__this + 1);
	ContactFilter2D_CheckConsistency_m222060761(_thisAdjusted, method);
}
// System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern "C" IL2CPP_METHOD_ATTR void ContactFilter2D_SetLayerMask_m3440334821 (ContactFilter2D_t3805203441 * __this, LayerMask_t3493934918  ___layerMask0, const RuntimeMethod* method)
{
	{
		LayerMask_t3493934918  L_0 = ___layerMask0;
		__this->set_layerMask_6(L_0);
		__this->set_useLayerMask_1((bool)1);
		return;
	}
}
extern "C"  void ContactFilter2D_SetLayerMask_m3440334821_AdjustorThunk (RuntimeObject * __this, LayerMask_t3493934918  ___layerMask0, const RuntimeMethod* method)
{
	ContactFilter2D_t3805203441 * _thisAdjusted = reinterpret_cast<ContactFilter2D_t3805203441 *>(__this + 1);
	ContactFilter2D_SetLayerMask_m3440334821(_thisAdjusted, ___layerMask0, method);
}
// System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void ContactFilter2D_SetDepth_m3691256496 (ContactFilter2D_t3805203441 * __this, float ___minDepth0, float ___maxDepth1, const RuntimeMethod* method)
{
	{
		float L_0 = ___minDepth0;
		__this->set_minDepth_7(L_0);
		float L_1 = ___maxDepth1;
		__this->set_maxDepth_8(L_1);
		__this->set_useDepth_2((bool)1);
		ContactFilter2D_CheckConsistency_m222060761((ContactFilter2D_t3805203441 *)__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ContactFilter2D_SetDepth_m3691256496_AdjustorThunk (RuntimeObject * __this, float ___minDepth0, float ___maxDepth1, const RuntimeMethod* method)
{
	ContactFilter2D_t3805203441 * _thisAdjusted = reinterpret_cast<ContactFilter2D_t3805203441 *>(__this + 1);
	ContactFilter2D_SetDepth_m3691256496(_thisAdjusted, ___minDepth0, ___maxDepth1, method);
}
// UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR ContactFilter2D_t3805203441  ContactFilter2D_CreateLegacyFilter_m1873816896 (RuntimeObject * __this /* static, unused */, int32_t ___layerMask0, float ___minDepth1, float ___maxDepth2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContactFilter2D_CreateLegacyFilter_m1873816896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ContactFilter2D_t3805203441  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(ContactFilter2D_t3805203441 ));
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		bool L_0 = Physics2D_get_queriesHitTriggers_m1249674012(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_useTriggers_0(L_0);
		int32_t L_1 = ___layerMask0;
		LayerMask_t3493934918  L_2 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ContactFilter2D_SetLayerMask_m3440334821((ContactFilter2D_t3805203441 *)(&V_0), L_2, /*hidden argument*/NULL);
		float L_3 = ___minDepth1;
		float L_4 = ___maxDepth2;
		ContactFilter2D_SetDepth_m3691256496((ContactFilter2D_t3805203441 *)(&V_0), L_3, L_4, /*hidden argument*/NULL);
		ContactFilter2D_t3805203441  L_5 = V_0;
		V_1 = L_5;
		goto IL_0032;
	}

IL_0032:
	{
		ContactFilter2D_t3805203441  L_6 = V_1;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  ContactPoint2D_get_point_m346644250 (ContactPoint2D_t3390240644 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2156229523  ContactPoint2D_get_point_m346644250_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ContactPoint2D_t3390240644 * _thisAdjusted = reinterpret_cast<ContactPoint2D_t3390240644 *>(__this + 1);
	return ContactPoint2D_get_point_m346644250(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_normal()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  ContactPoint2D_get_normal_m641220705 (ContactPoint2D_t3390240644 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2156229523  ContactPoint2D_get_normal_m641220705_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	ContactPoint2D_t3390240644 * _thisAdjusted = reinterpret_cast<ContactPoint2D_t3390240644 *>(__this + 1);
	return ContactPoint2D_get_normal_m641220705(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.HingeJoint2D::set_useMotor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_useMotor_m3798396905 (HingeJoint2D_t3442948838 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint2D_set_useMotor_m3798396905_ftn) (HingeJoint2D_t3442948838 *, bool);
	static HingeJoint2D_set_useMotor_m3798396905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint2D_set_useMotor_m3798396905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint2D::set_useMotor(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_useLimits_m3291420575 (HingeJoint2D_t3442948838 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint2D_set_useLimits_m3291420575_ftn) (HingeJoint2D_t3442948838 *, bool);
	static HingeJoint2D_set_useLimits_m3291420575_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint2D_set_useLimits_m3291420575_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.JointMotor2D UnityEngine.HingeJoint2D::get_motor()
extern "C" IL2CPP_METHOD_ATTR JointMotor2D_t142461323  HingeJoint2D_get_motor_m4163422608 (HingeJoint2D_t3442948838 * __this, const RuntimeMethod* method)
{
	JointMotor2D_t142461323  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HingeJoint2D_get_motor_Injected_m3349627322(__this, (JointMotor2D_t142461323 *)(&V_0), /*hidden argument*/NULL);
		JointMotor2D_t142461323  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.HingeJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_motor_m3755430374 (HingeJoint2D_t3442948838 * __this, JointMotor2D_t142461323  ___value0, const RuntimeMethod* method)
{
	{
		HingeJoint2D_set_motor_Injected_m265480475(__this, (JointMotor2D_t142461323 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.JointAngleLimits2D UnityEngine.HingeJoint2D::get_limits()
extern "C" IL2CPP_METHOD_ATTR JointAngleLimits2D_t972279075  HingeJoint2D_get_limits_m2559278166 (HingeJoint2D_t3442948838 * __this, const RuntimeMethod* method)
{
	JointAngleLimits2D_t972279075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HingeJoint2D_get_limits_Injected_m407444726(__this, (JointAngleLimits2D_t972279075 *)(&V_0), /*hidden argument*/NULL);
		JointAngleLimits2D_t972279075  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.HingeJoint2D::set_limits(UnityEngine.JointAngleLimits2D)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_limits_m2397734673 (HingeJoint2D_t3442948838 * __this, JointAngleLimits2D_t972279075  ___value0, const RuntimeMethod* method)
{
	{
		HingeJoint2D_set_limits_Injected_m3913299803(__this, (JointAngleLimits2D_t972279075 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.HingeJoint2D::get_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_get_motor_Injected_m3349627322 (HingeJoint2D_t3442948838 * __this, JointMotor2D_t142461323 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint2D_get_motor_Injected_m3349627322_ftn) (HingeJoint2D_t3442948838 *, JointMotor2D_t142461323 *);
	static HingeJoint2D_get_motor_Injected_m3349627322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint2D_get_motor_Injected_m3349627322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint2D::get_motor_Injected(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.HingeJoint2D::set_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_motor_Injected_m265480475 (HingeJoint2D_t3442948838 * __this, JointMotor2D_t142461323 * ___value0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint2D_set_motor_Injected_m265480475_ftn) (HingeJoint2D_t3442948838 *, JointMotor2D_t142461323 *);
	static HingeJoint2D_set_motor_Injected_m265480475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint2D_set_motor_Injected_m265480475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint2D::set_motor_Injected(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.HingeJoint2D::get_limits_Injected(UnityEngine.JointAngleLimits2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_get_limits_Injected_m407444726 (HingeJoint2D_t3442948838 * __this, JointAngleLimits2D_t972279075 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint2D_get_limits_Injected_m407444726_ftn) (HingeJoint2D_t3442948838 *, JointAngleLimits2D_t972279075 *);
	static HingeJoint2D_get_limits_Injected_m407444726_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint2D_get_limits_Injected_m407444726_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint2D::get_limits_Injected(UnityEngine.JointAngleLimits2D&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.HingeJoint2D::set_limits_Injected(UnityEngine.JointAngleLimits2D&)
extern "C" IL2CPP_METHOD_ATTR void HingeJoint2D_set_limits_Injected_m3913299803 (HingeJoint2D_t3442948838 * __this, JointAngleLimits2D_t972279075 * ___value0, const RuntimeMethod* method)
{
	typedef void (*HingeJoint2D_set_limits_Injected_m3913299803_ftn) (HingeJoint2D_t3442948838 *, JointAngleLimits2D_t972279075 *);
	static HingeJoint2D_set_limits_Injected_m3913299803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (HingeJoint2D_set_limits_Injected_m3913299803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.HingeJoint2D::set_limits_Injected(UnityEngine.JointAngleLimits2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 UnityEngine.Joint2D::get_reactionForce()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Joint2D_get_reactionForce_m2456931782 (Joint2D_t4180440564 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Joint2D_get_reactionForce_Injected_m1258031357(__this, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		return L_0;
	}
}
// System.Single UnityEngine.Joint2D::get_reactionTorque()
extern "C" IL2CPP_METHOD_ATTR float Joint2D_get_reactionTorque_m3340844127 (Joint2D_t4180440564 * __this, const RuntimeMethod* method)
{
	typedef float (*Joint2D_get_reactionTorque_m3340844127_ftn) (Joint2D_t4180440564 *);
	static Joint2D_get_reactionTorque_m3340844127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint2D_get_reactionTorque_m3340844127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint2D::get_reactionTorque()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Joint2D::get_reactionForce_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Joint2D_get_reactionForce_Injected_m1258031357 (Joint2D_t4180440564 * __this, Vector2_t2156229523 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Joint2D_get_reactionForce_Injected_m1258031357_ftn) (Joint2D_t4180440564 *, Vector2_t2156229523 *);
	static Joint2D_get_reactionForce_Injected_m1258031357_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Joint2D_get_reactionForce_Injected_m1258031357_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Joint2D::get_reactionForce_Injected(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___ret0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.JointAngleLimits2D::set_min(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointAngleLimits2D_set_min_m2880987945 (JointAngleLimits2D_t972279075 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_LowerAngle_0(L_0);
		return;
	}
}
extern "C"  void JointAngleLimits2D_set_min_m2880987945_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointAngleLimits2D_t972279075 * _thisAdjusted = reinterpret_cast<JointAngleLimits2D_t972279075 *>(__this + 1);
	JointAngleLimits2D_set_min_m2880987945(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.JointAngleLimits2D::set_max(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointAngleLimits2D_set_max_m2252990911 (JointAngleLimits2D_t972279075 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_UpperAngle_1(L_0);
		return;
	}
}
extern "C"  void JointAngleLimits2D_set_max_m2252990911_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointAngleLimits2D_t972279075 * _thisAdjusted = reinterpret_cast<JointAngleLimits2D_t972279075 *>(__this + 1);
	JointAngleLimits2D_set_max_m2252990911(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.JointMotor2D::set_motorSpeed(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointMotor2D_set_motorSpeed_m1876890553 (JointMotor2D_t142461323 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_MotorSpeed_0(L_0);
		return;
	}
}
extern "C"  void JointMotor2D_set_motorSpeed_m1876890553_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointMotor2D_t142461323 * _thisAdjusted = reinterpret_cast<JointMotor2D_t142461323 *>(__this + 1);
	JointMotor2D_set_motorSpeed_m1876890553(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.JointMotor2D::set_maxMotorTorque(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointMotor2D_set_maxMotorTorque_m668541733 (JointMotor2D_t142461323 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_MaximumMotorTorque_1(L_0);
		return;
	}
}
extern "C"  void JointMotor2D_set_maxMotorTorque_m668541733_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointMotor2D_t142461323 * _thisAdjusted = reinterpret_cast<JointMotor2D_t142461323 *>(__this + 1);
	JointMotor2D_set_maxMotorTorque_m668541733(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.JointSuspension2D::set_dampingRatio(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointSuspension2D_set_dampingRatio_m2619162143 (JointSuspension2D_t3350541332 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_DampingRatio_0(L_0);
		return;
	}
}
extern "C"  void JointSuspension2D_set_dampingRatio_m2619162143_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointSuspension2D_t3350541332 * _thisAdjusted = reinterpret_cast<JointSuspension2D_t3350541332 *>(__this + 1);
	JointSuspension2D_set_dampingRatio_m2619162143(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.JointSuspension2D::set_frequency(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointSuspension2D_set_frequency_m305231415 (JointSuspension2D_t3350541332 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Frequency_1(L_0);
		return;
	}
}
extern "C"  void JointSuspension2D_set_frequency_m305231415_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointSuspension2D_t3350541332 * _thisAdjusted = reinterpret_cast<JointSuspension2D_t3350541332 *>(__this + 1);
	JointSuspension2D_set_frequency_m305231415(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.JointSuspension2D::set_angle(System.Single)
extern "C" IL2CPP_METHOD_ATTR void JointSuspension2D_set_angle_m4263311628 (JointSuspension2D_t3350541332 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Angle_2(L_0);
		return;
	}
}
extern "C"  void JointSuspension2D_set_angle_m4263311628_AdjustorThunk (RuntimeObject * __this, float ___value0, const RuntimeMethod* method)
{
	JointSuspension2D_t3350541332 * _thisAdjusted = reinterpret_cast<JointSuspension2D_t3350541332 *>(__this + 1);
	JointSuspension2D_set_angle_m4263311628(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Physics2D::set_gravity(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_set_gravity_m2648827161 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_set_gravity_m2648827161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_set_gravity_Injected_m2797569484(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern "C" IL2CPP_METHOD_ATTR bool Physics2D_get_queriesHitTriggers_m1249674012 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef bool (*Physics2D_get_queriesHitTriggers_m1249674012_ftn) ();
	static Physics2D_get_queriesHitTriggers_m1249674012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_get_queriesHitTriggers_m1249674012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::get_queriesHitTriggers()");
	bool retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Linecast_m2347689996 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_m2347689996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerMask2;
		ContactFilter2D_t3805203441  L_1 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2156229523  L_2 = ___start0;
		Vector2_t2156229523  L_3 = ___end1;
		ContactFilter2D_t3805203441  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_5 = Physics2D_Linecast_Internal_m2067808837(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		RaycastHit2D_t2279581989  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Linecast_m2284594132 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_m2284594132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___start0;
		Vector2_t2156229523  L_5 = ___end1;
		ContactFilter2D_t3805203441  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_7 = Physics2D_Linecast_Internal_m2067808837(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2D_t2279581989  L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_LinecastAll_m3604445636 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_m3604445636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2DU5BU5D_t4286651560* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask2;
		ContactFilter2D_t3805203441  L_1 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2156229523  L_2 = ___start0;
		Vector2_t2156229523  L_3 = ___end1;
		ContactFilter2D_t3805203441  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_5 = Physics2D_LinecastAll_Internal_m604725090(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		RaycastHit2DU5BU5D_t4286651560* L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_LinecastAll_m4019250736 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_m4019250736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2DU5BU5D_t4286651560* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___start0;
		Vector2_t2156229523  L_5 = ___end1;
		ContactFilter2D_t3805203441  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_7 = Physics2D_LinecastAll_Internal_m604725090(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2DU5BU5D_t4286651560* L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast_Internal(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Linecast_Internal_m2067808837 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, ContactFilter2D_t3805203441  ___contactFilter2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_Internal_m2067808837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t2279581989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_Linecast_Internal_Injected_m4053345089(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___start0), (Vector2_t2156229523 *)(&___end1), (ContactFilter2D_t3805203441 *)(&___contactFilter2), (RaycastHit2D_t2279581989 *)(&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t2279581989  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll_Internal(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_LinecastAll_Internal_m604725090 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___start0, Vector2_t2156229523  ___end1, ContactFilter2D_t3805203441  ___contactFilter2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_Internal_m604725090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_0 = Physics2D_LinecastAll_Internal_Injected_m3181521651(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___start0), (Vector2_t2156229523 *)(&___end1), (ContactFilter2D_t3805203441 *)(&___contactFilter2), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_m2341153778 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2341153778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ContactFilter2D_t3805203441  L_0 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, ((int32_t)-5), (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_0;
		Vector2_t2156229523  L_1 = ___origin0;
		Vector2_t2156229523  L_2 = ___direction1;
		ContactFilter2D_t3805203441  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_4 = Physics2D_Raycast_Internal_m2084501876(NULL /*static, unused*/, L_1, L_2, (std::numeric_limits<float>::infinity()), L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0026;
	}

IL_0026:
	{
		RaycastHit2D_t2279581989  L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_m2082490717 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2082490717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ContactFilter2D_t3805203441  L_0 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, ((int32_t)-5), (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_0;
		Vector2_t2156229523  L_1 = ___origin0;
		Vector2_t2156229523  L_2 = ___direction1;
		float L_3 = ___distance2;
		ContactFilter2D_t3805203441  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_5 = Physics2D_Raycast_Internal_m2084501876(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0022;
	}

IL_0022:
	{
		RaycastHit2D_t2279581989  L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_m2858866636 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2858866636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerMask3;
		ContactFilter2D_t3805203441  L_1 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2156229523  L_2 = ___origin0;
		Vector2_t2156229523  L_3 = ___direction1;
		float L_4 = ___distance2;
		ContactFilter2D_t3805203441  L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_6 = Physics2D_Raycast_Internal_m2084501876(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		RaycastHit2D_t2279581989  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_m1728056151 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1728056151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerMask3;
		float L_1 = ___minDepth4;
		ContactFilter2D_t3805203441  L_2 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_t2156229523  L_3 = ___origin0;
		Vector2_t2156229523  L_4 = ___direction1;
		float L_5 = ___distance2;
		ContactFilter2D_t3805203441  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_7 = Physics2D_Raycast_Internal_m2084501876(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHit2D_t2279581989  L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_m1407159225 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1407159225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___layerMask3;
		float L_1 = ___minDepth4;
		float L_2 = ___maxDepth5;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___origin0;
		Vector2_t2156229523  L_5 = ___direction1;
		float L_6 = ___distance2;
		ContactFilter2D_t3805203441  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_8 = Physics2D_Raycast_Internal_m2084501876(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHit2D_t2279581989  L_9 = V_1;
		return L_9;
	}
}
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_Raycast_m1377762210 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, ContactFilter2D_t3805203441  ___contactFilter2, RaycastHit2DU5BU5D_t4286651560* ___results3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1377762210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		ContactFilter2D_t3805203441  L_2 = ___contactFilter2;
		RaycastHit2DU5BU5D_t4286651560* L_3 = ___results3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_4 = Physics2D_RaycastNonAlloc_Internal_m4193835177(NULL /*static, unused*/, L_0, L_1, (std::numeric_limits<float>::infinity()), L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_Raycast_m1700114981 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, ContactFilter2D_t3805203441  ___contactFilter2, RaycastHit2DU5BU5D_t4286651560* ___results3, float ___distance4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m1700114981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		float L_2 = ___distance4;
		ContactFilter2D_t3805203441  L_3 = ___contactFilter2;
		RaycastHit2DU5BU5D_t4286651560* L_4 = ___results3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_5 = Physics2D_RaycastNonAlloc_Internal_m4193835177(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_m3915954823 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_m3915954823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2DU5BU5D_t4286651560* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask3;
		ContactFilter2D_t3805203441  L_1 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2156229523  L_2 = ___origin0;
		Vector2_t2156229523  L_3 = ___direction1;
		float L_4 = ___distance2;
		ContactFilter2D_t3805203441  L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_6 = Physics2D_RaycastAll_Internal_m3190530210(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		RaycastHit2DU5BU5D_t4286651560* L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_m399425645 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_m399425645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2DU5BU5D_t4286651560* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask3;
		float L_1 = ___minDepth4;
		float L_2 = ___maxDepth5;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___origin0;
		Vector2_t2156229523  L_5 = ___direction1;
		float L_6 = ___distance2;
		ContactFilter2D_t3805203441  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_8 = Physics2D_RaycastAll_Internal_m3190530210(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHit2DU5BU5D_t4286651560* L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_Raycast_Internal_m2084501876 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_Internal_m2084501876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t2279581989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___distance2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_Raycast_Internal_Injected_m1647830047(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___origin0), (Vector2_t2156229523 *)(&___direction1), L_0, (ContactFilter2D_t3805203441 *)(&___contactFilter3), (RaycastHit2D_t2279581989 *)(&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t2279581989  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_Internal_m3190530210 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_Internal_m3190530210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_1 = Physics2D_RaycastAll_Internal_Injected_m2949536273(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___origin0), (Vector2_t2156229523 *)(&___direction1), L_0, (ContactFilter2D_t3805203441 *)(&___contactFilter3), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_RaycastNonAlloc_Internal_m4193835177 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastNonAlloc_Internal_m4193835177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		RaycastHit2DU5BU5D_t4286651560* L_1 = ___results4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_2 = Physics2D_RaycastNonAlloc_Internal_Injected_m1762831718(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___origin0), (Vector2_t2156229523 *)(&___direction1), L_0, (ContactFilter2D_t3805203441 *)(&___contactFilter3), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_GetRayIntersection_m662508963 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___distance1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_m662508963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t2279581989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___distance1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_3 = Physics2D_GetRayIntersection_Internal_m1872178675(NULL /*static, unused*/, L_0, L_1, L_2, ((int32_t)-5), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001d;
	}

IL_001d:
	{
		RaycastHit2D_t2279581989  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_GetRayIntersection_m1808353572 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_m1808353572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t2279581989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___distance1;
		int32_t L_3 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_4 = Physics2D_GetRayIntersection_Internal_m1872178675(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		RaycastHit2D_t2279581989  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_m1507505063 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m1507505063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2DU5BU5D_t4286651560* V_0 = NULL;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_2 = Physics2D_GetRayIntersectionAll_Internal_m643548862(NULL /*static, unused*/, L_0, L_1, (std::numeric_limits<float>::infinity()), ((int32_t)-5), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0021:
	{
		RaycastHit2DU5BU5D_t4286651560* L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_m1670084419 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___distance1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m1670084419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2DU5BU5D_t4286651560* V_0 = NULL;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___distance1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_3 = Physics2D_GetRayIntersectionAll_Internal_m643548862(NULL /*static, unused*/, L_0, L_1, L_2, ((int32_t)-5), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_001d;
	}

IL_001d:
	{
		RaycastHit2DU5BU5D_t4286651560* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_m2567878449 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___distance1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m2567878449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2DU5BU5D_t4286651560* V_0 = NULL;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___distance1;
		int32_t L_3 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_4 = Physics2D_GetRayIntersectionAll_Internal_m643548862(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		RaycastHit2DU5BU5D_t4286651560* L_5 = V_0;
		return L_5;
	}
}
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_m3412378039 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit2DU5BU5D_t4286651560* ___results1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionNonAlloc_m3412378039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		RaycastHit2DU5BU5D_t4286651560* L_2 = ___results1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_3 = Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563(NULL /*static, unused*/, L_0, L_1, (std::numeric_limits<float>::infinity()), ((int32_t)-5), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_m2666102441 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit2DU5BU5D_t4286651560* ___results1, float ___distance2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionNonAlloc_m2666102441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___distance2;
		RaycastHit2DU5BU5D_t4286651560* L_3 = ___results1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_4 = Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563(NULL /*static, unused*/, L_0, L_1, L_2, ((int32_t)-5), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_m2525277886 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit2DU5BU5D_t4286651560* ___results1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionNonAlloc_m2525277886_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m2819290985((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m761601601((Ray_t3785851493 *)(&___ray0), /*hidden argument*/NULL);
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		RaycastHit2DU5BU5D_t4286651560* L_4 = ___results1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_5 = Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection_Internal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2D_t2279581989  Physics2D_GetRayIntersection_Internal_m1872178675 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_Internal_m1872178675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t2279581989  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_GetRayIntersection_Internal_Injected_m3464046400(NULL /*static, unused*/, (Vector3_t3722313464 *)(&___origin0), (Vector3_t3722313464 *)(&___direction1), L_0, L_1, (RaycastHit2D_t2279581989 *)(&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t2279581989  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_Internal_m643548862 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_Internal_m643548862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_2 = Physics2D_GetRayIntersectionAll_Internal_Injected_m1793592321(NULL /*static, unused*/, (Vector3_t3722313464 *)(&___origin0), (Vector3_t3722313464 *)(&___direction1), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc_Internal(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___distance2, int32_t ___layerMask3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionNonAlloc_Internal_m177321563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		RaycastHit2DU5BU5D_t4286651560* L_2 = ___results4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_3 = Physics2D_GetRayIntersectionNonAlloc_Internal_Injected_m3091622535(NULL /*static, unused*/, (Vector3_t3722313464 *)(&___origin0), (Vector3_t3722313464 *)(&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapPointAll_m2300095884 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, int32_t ___layerMask1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_m2300095884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2DU5BU5D_t1693969295* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask1;
		ContactFilter2D_t3805203441  L_1 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2156229523  L_2 = ___point0;
		ContactFilter2D_t3805203441  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_4 = Physics2D_OverlapPointAll_Internal_m3057315634(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		Collider2DU5BU5D_t1693969295* L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapPointAll_m1050872217 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_m1050872217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2DU5BU5D_t1693969295* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask1;
		float L_1 = ___minDepth2;
		float L_2 = ___maxDepth3;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___point0;
		ContactFilter2D_t3805203441  L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_6 = Physics2D_OverlapPointAll_Internal_m3057315634(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		Collider2DU5BU5D_t1693969295* L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll_Internal(UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapPointAll_Internal_m3057315634 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, ContactFilter2D_t3805203441  ___contactFilter1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_Internal_m3057315634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_0 = Physics2D_OverlapPointAll_Internal_Injected_m3237007287(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___point0), (ContactFilter2D_t3805203441 *)(&___contactFilter1), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_m638049410 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, float ___radius1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_m638049410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2DU5BU5D_t1693969295* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask2;
		ContactFilter2D_t3805203441  L_1 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t2156229523  L_2 = ___point0;
		float L_3 = ___radius1;
		ContactFilter2D_t3805203441  L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_5 = Physics2D_OverlapCircleAll_Internal_m1158092355(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		Collider2DU5BU5D_t1693969295* L_6 = V_1;
		return L_6;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_m1634860779 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_m1634860779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2DU5BU5D_t1693969295* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___point0;
		float L_5 = ___radius1;
		ContactFilter2D_t3805203441  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_7 = Physics2D_OverlapCircleAll_Internal_m1158092355(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0019;
	}

IL_0019:
	{
		Collider2DU5BU5D_t1693969295* L_8 = V_1;
		return L_8;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal(UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_Internal_m1158092355 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, float ___radius1, ContactFilter2D_t3805203441  ___contactFilter2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_Internal_m1158092355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___radius1;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_1 = Physics2D_OverlapCircleAll_Internal_Injected_m1699159137(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___point0), L_0, (ContactFilter2D_t3805203441 *)(&___contactFilter2), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapBoxAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapBoxAll_m2279087762 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, Vector2_t2156229523  ___size1, float ___angle2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapBoxAll_m2279087762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2DU5BU5D_t1693969295* V_1 = NULL;
	{
		int32_t L_0 = ___layerMask3;
		float L_1 = ___minDepth4;
		float L_2 = ___maxDepth5;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m1873816896(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___point0;
		Vector2_t2156229523  L_5 = ___size1;
		float L_6 = ___angle2;
		ContactFilter2D_t3805203441  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_8 = Physics2D_OverlapBoxAll_Internal_m2950777821(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_001b;
	}

IL_001b:
	{
		Collider2DU5BU5D_t1693969295* L_9 = V_1;
		return L_9;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapBoxAll_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapBoxAll_Internal_m2950777821 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___point0, Vector2_t2156229523  ___size1, float ___angle2, ContactFilter2D_t3805203441  ___contactFilter3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapBoxAll_Internal_m2950777821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___angle2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_1 = Physics2D_OverlapBoxAll_Internal_Injected_m50526180(NULL /*static, unused*/, (Vector2_t2156229523 *)(&___point0), (Vector2_t2156229523 *)(&___size1), L_0, (ContactFilter2D_t3805203441 *)(&___contactFilter3), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapAreaAll_m3532436837 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___pointA0, Vector2_t2156229523  ___pointB1, int32_t ___layerMask2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAll_m3532436837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2DU5BU5D_t1693969295* V_0 = NULL;
	{
		Vector2_t2156229523  L_0 = ___pointA0;
		Vector2_t2156229523  L_1 = ___pointB1;
		int32_t L_2 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_3 = Physics2D_OverlapAreaAllToBox_Internal_m4180410992(NULL /*static, unused*/, L_0, L_1, L_2, (-std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0019;
	}

IL_0019:
	{
		Collider2DU5BU5D_t1693969295* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapAreaAll_m3957673898 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___pointA0, Vector2_t2156229523  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAll_m3957673898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2DU5BU5D_t1693969295* V_0 = NULL;
	{
		Vector2_t2156229523  L_0 = ___pointA0;
		Vector2_t2156229523  L_1 = ___pointB1;
		int32_t L_2 = ___layerMask2;
		float L_3 = ___minDepth3;
		float L_4 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_5 = Physics2D_OverlapAreaAllToBox_Internal_m4180410992(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		Collider2DU5BU5D_t1693969295* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAllToBox_Internal(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapAreaAllToBox_Internal_m4180410992 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  ___pointA0, Vector2_t2156229523  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAllToBox_Internal_m4180410992_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Collider2DU5BU5D_t1693969295* V_2 = NULL;
	{
		Vector2_t2156229523  L_0 = ___pointA0;
		Vector2_t2156229523  L_1 = ___pointB1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&___pointA0)->get_x_0();
		float L_5 = (&___pointB1)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_6 = fabsf(((float)il2cpp_codegen_subtract((float)L_4, (float)L_5)));
		float L_7 = (&___pointA0)->get_y_1();
		float L_8 = (&___pointB1)->get_y_1();
		float L_9 = fabsf(((float)il2cpp_codegen_subtract((float)L_7, (float)L_8)));
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_1), L_6, L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = V_0;
		Vector2_t2156229523  L_11 = V_1;
		int32_t L_12 = ___layerMask2;
		float L_13 = ___minDepth3;
		float L_14 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t1693969295* L_15 = Physics2D_OverlapBoxAll_m2279087762(NULL /*static, unused*/, L_10, L_11, (0.0f), L_12, L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		goto IL_0058;
	}

IL_0058:
	{
		Collider2DU5BU5D_t1693969295* L_16 = V_2;
		return L_16;
	}
}
// System.Void UnityEngine.Physics2D::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Physics2D__cctor_m143866755 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m143866755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2411569343 * L_0 = (List_1_t2411569343 *)il2cpp_codegen_object_new(List_1_t2411569343_il2cpp_TypeInfo_var);
		List_1__ctor_m2466177744(L_0, /*hidden argument*/List_1__ctor_m2466177744_RuntimeMethod_var);
		((Physics2D_t1528932956_StaticFields*)il2cpp_codegen_static_fields_for(Physics2D_t1528932956_il2cpp_TypeInfo_var))->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Physics2D::set_gravity_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_set_gravity_Injected_m2797569484 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Physics2D_set_gravity_Injected_m2797569484_ftn) (Vector2_t2156229523 *);
	static Physics2D_set_gravity_Injected_m2797569484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_set_gravity_Injected_m2797569484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::set_gravity_Injected(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Physics2D::Linecast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_Linecast_Internal_Injected_m4053345089 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___start0, Vector2_t2156229523 * ___end1, ContactFilter2D_t3805203441 * ___contactFilter2, RaycastHit2D_t2279581989 * ___ret3, const RuntimeMethod* method)
{
	typedef void (*Physics2D_Linecast_Internal_Injected_m4053345089_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, ContactFilter2D_t3805203441 *, RaycastHit2D_t2279581989 *);
	static Physics2D_Linecast_Internal_Injected_m4053345089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_Linecast_Internal_Injected_m4053345089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::Linecast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___start0, ___end1, ___contactFilter2, ___ret3);
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_LinecastAll_Internal_Injected_m3181521651 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___start0, Vector2_t2156229523 * ___end1, ContactFilter2D_t3805203441 * ___contactFilter2, const RuntimeMethod* method)
{
	typedef RaycastHit2DU5BU5D_t4286651560* (*Physics2D_LinecastAll_Internal_Injected_m3181521651_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, ContactFilter2D_t3805203441 *);
	static Physics2D_LinecastAll_Internal_Injected_m3181521651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_LinecastAll_Internal_Injected_m3181521651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::LinecastAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)");
	RaycastHit2DU5BU5D_t4286651560* retVal = _il2cpp_icall_func(___start0, ___end1, ___contactFilter2);
	return retVal;
}
// System.Void UnityEngine.Physics2D::Raycast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_Raycast_Internal_Injected_m1647830047 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2D_t2279581989 * ___ret4, const RuntimeMethod* method)
{
	typedef void (*Physics2D_Raycast_Internal_Injected_m1647830047_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *, RaycastHit2D_t2279581989 *);
	static Physics2D_Raycast_Internal_Injected_m1647830047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_Raycast_Internal_Injected_m1647830047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::Raycast_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3, ___ret4);
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_Internal_Injected_m2949536273 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, const RuntimeMethod* method)
{
	typedef RaycastHit2DU5BU5D_t4286651560* (*Physics2D_RaycastAll_Internal_Injected_m2949536273_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *);
	static Physics2D_RaycastAll_Internal_Injected_m2949536273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_RaycastAll_Internal_Injected_m2949536273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::RaycastAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)");
	RaycastHit2DU5BU5D_t4286651560* retVal = _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3);
	return retVal;
}
// System.Int32 UnityEngine.Physics2D::RaycastNonAlloc_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_RaycastNonAlloc_Internal_Injected_m1762831718 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method)
{
	typedef int32_t (*Physics2D_RaycastNonAlloc_Internal_Injected_m1762831718_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *, RaycastHit2DU5BU5D_t4286651560*);
	static Physics2D_RaycastNonAlloc_Internal_Injected_m1762831718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_RaycastNonAlloc_Internal_Injected_m1762831718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::RaycastNonAlloc_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])");
	int32_t retVal = _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3, ___results4);
	return retVal;
}
// System.Void UnityEngine.Physics2D::GetRayIntersection_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_GetRayIntersection_Internal_Injected_m3464046400 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___distance2, int32_t ___layerMask3, RaycastHit2D_t2279581989 * ___ret4, const RuntimeMethod* method)
{
	typedef void (*Physics2D_GetRayIntersection_Internal_Injected_m3464046400_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, float, int32_t, RaycastHit2D_t2279581989 *);
	static Physics2D_GetRayIntersection_Internal_Injected_m3464046400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetRayIntersection_Internal_Injected_m3464046400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetRayIntersection_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___ret4);
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_Internal_Injected_m1793592321 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___distance2, int32_t ___layerMask3, const RuntimeMethod* method)
{
	typedef RaycastHit2DU5BU5D_t4286651560* (*Physics2D_GetRayIntersectionAll_Internal_Injected_m1793592321_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, float, int32_t);
	static Physics2D_GetRayIntersectionAll_Internal_Injected_m1793592321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetRayIntersectionAll_Internal_Injected_m1793592321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)");
	RaycastHit2DU5BU5D_t4286651560* retVal = _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3);
	return retVal;
}
// System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern "C" IL2CPP_METHOD_ATTR int32_t Physics2D_GetRayIntersectionNonAlloc_Internal_Injected_m3091622535 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___distance2, int32_t ___layerMask3, RaycastHit2DU5BU5D_t4286651560* ___results4, const RuntimeMethod* method)
{
	typedef int32_t (*Physics2D_GetRayIntersectionNonAlloc_Internal_Injected_m3091622535_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, float, int32_t, RaycastHit2DU5BU5D_t4286651560*);
	static Physics2D_GetRayIntersectionNonAlloc_Internal_Injected_m3091622535_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetRayIntersectionNonAlloc_Internal_Injected_m3091622535_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetRayIntersectionNonAlloc_Internal_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])");
	int32_t retVal = _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___results4);
	return retVal;
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapPointAll_Internal_Injected_m3237007287 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, ContactFilter2D_t3805203441 * ___contactFilter1, const RuntimeMethod* method)
{
	typedef Collider2DU5BU5D_t1693969295* (*Physics2D_OverlapPointAll_Internal_Injected_m3237007287_ftn) (Vector2_t2156229523 *, ContactFilter2D_t3805203441 *);
	static Physics2D_OverlapPointAll_Internal_Injected_m3237007287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_OverlapPointAll_Internal_Injected_m3237007287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::OverlapPointAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)");
	Collider2DU5BU5D_t1693969295* retVal = _il2cpp_icall_func(___point0, ___contactFilter1);
	return retVal;
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapCircleAll_Internal_Injected_m1699159137 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, float ___radius1, ContactFilter2D_t3805203441 * ___contactFilter2, const RuntimeMethod* method)
{
	typedef Collider2DU5BU5D_t1693969295* (*Physics2D_OverlapCircleAll_Internal_Injected_m1699159137_ftn) (Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *);
	static Physics2D_OverlapCircleAll_Internal_Injected_m1699159137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_OverlapCircleAll_Internal_Injected_m1699159137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::OverlapCircleAll_Internal_Injected(UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)");
	Collider2DU5BU5D_t1693969295* retVal = _il2cpp_icall_func(___point0, ___radius1, ___contactFilter2);
	return retVal;
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapBoxAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
extern "C" IL2CPP_METHOD_ATTR Collider2DU5BU5D_t1693969295* Physics2D_OverlapBoxAll_Internal_Injected_m50526180 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, Vector2_t2156229523 * ___size1, float ___angle2, ContactFilter2D_t3805203441 * ___contactFilter3, const RuntimeMethod* method)
{
	typedef Collider2DU5BU5D_t1693969295* (*Physics2D_OverlapBoxAll_Internal_Injected_m50526180_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *);
	static Physics2D_OverlapBoxAll_Internal_Injected_m50526180_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_OverlapBoxAll_Internal_Injected_m50526180_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::OverlapBoxAll_Internal_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)");
	Collider2DU5BU5D_t1693969295* retVal = _il2cpp_icall_func(___point0, ___size1, ___angle2, ___contactFilter3);
	return retVal;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.PhysicsMaterial2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PhysicsMaterial2D__ctor_m2702207403 (PhysicsMaterial2D_t331219942 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhysicsMaterial2D__ctor_m2702207403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object__ctor_m1087895580(__this, /*hidden argument*/NULL);
		PhysicsMaterial2D_Create_Internal_m3464072492(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PhysicsMaterial2D::Create_Internal(UnityEngine.PhysicsMaterial2D,System.String)
extern "C" IL2CPP_METHOD_ATTR void PhysicsMaterial2D_Create_Internal_m3464072492 (RuntimeObject * __this /* static, unused */, PhysicsMaterial2D_t331219942 * ___scriptMaterial0, String_t* ___name1, const RuntimeMethod* method)
{
	typedef void (*PhysicsMaterial2D_Create_Internal_m3464072492_ftn) (PhysicsMaterial2D_t331219942 *, String_t*);
	static PhysicsMaterial2D_Create_Internal_m3464072492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PhysicsMaterial2D_Create_Internal_m3464072492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PhysicsMaterial2D::Create_Internal(UnityEngine.PhysicsMaterial2D,System.String)");
	_il2cpp_icall_func(___scriptMaterial0, ___name1);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RaycastHit2D_get_point_m1586138107 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_Point_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2156229523  RaycastHit2D_get_point_m1586138107_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_point_m1586138107(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RaycastHit2D_get_normal_m772343376 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_Normal_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2156229523  RaycastHit2D_get_normal_m772343376_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_normal_m772343376(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C" IL2CPP_METHOD_ATTR float RaycastHit2D_get_distance_m382898860 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit2D_get_distance_m382898860_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_distance_m382898860(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" IL2CPP_METHOD_ATTR float RaycastHit2D_get_fraction_m2673798080 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Fraction_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m2673798080_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_fraction_m2673798080(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" IL2CPP_METHOD_ATTR Collider2D_t2806799626 * RaycastHit2D_get_collider_m1549426026 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_collider_m1549426026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2D_t2806799626 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_Collider_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_1 = Object_FindObjectFromInstanceID_m235838713(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Collider2D_t2806799626 *)IsInstClass((RuntimeObject*)L_1, Collider2D_t2806799626_il2cpp_TypeInfo_var));
		goto IL_0017;
	}

IL_0017:
	{
		Collider2D_t2806799626 * L_2 = V_0;
		return L_2;
	}
}
extern "C"  Collider2D_t2806799626 * RaycastHit2D_get_collider_m1549426026_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_collider_m1549426026(_thisAdjusted, method);
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" IL2CPP_METHOD_ATTR Rigidbody2D_t939494601 * RaycastHit2D_get_rigidbody_m942928380 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_rigidbody_m942928380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	Rigidbody2D_t939494601 * G_B3_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = RaycastHit2D_get_collider_m1549426026((RaycastHit2D_t2279581989 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Collider2D_t2806799626 * L_2 = RaycastHit2D_get_collider_m1549426026((RaycastHit2D_t2279581989 *)__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t939494601 * L_3 = Collider2D_get_attachedRigidbody_m729069942(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((Rigidbody2D_t939494601 *)(NULL));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		Rigidbody2D_t939494601 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Rigidbody2D_t939494601 * RaycastHit2D_get_rigidbody_m942928380_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_rigidbody_m942928380(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * RaycastHit2D_get_transform_m2048267409 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_transform_m2048267409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	{
		Rigidbody2D_t939494601 * L_0 = RaycastHit2D_get_rigidbody_m942928380((RaycastHit2D_t2279581989 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t939494601 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Rigidbody2D_t939494601 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0049;
	}

IL_0020:
	{
		Collider2D_t2806799626 * L_5 = RaycastHit2D_get_collider_m1549426026((RaycastHit2D_t2279581989 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Collider2D_t2806799626 * L_7 = RaycastHit2D_get_collider_m1549426026((RaycastHit2D_t2279581989 *)__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_0049;
	}

IL_0042:
	{
		V_1 = (Transform_t3600365921 *)NULL;
		goto IL_0049;
	}

IL_0049:
	{
		Transform_t3600365921 * L_9 = V_1;
		return L_9;
	}
}
extern "C"  Transform_t3600365921 * RaycastHit2D_get_transform_m2048267409_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_transform_m2048267409(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Rigidbody2D::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D__ctor_m3242337055 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	{
		Component__ctor_m1928064382(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Rigidbody2D_get_velocity_m366589732 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_get_velocity_Injected_m2170787103(__this, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m2898400508 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___value0, const RuntimeMethod* method)
{
	{
		Rigidbody2D_set_velocity_Injected_m1505462814(__this, (Vector2_t2156229523 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Rigidbody2D::get_mass()
extern "C" IL2CPP_METHOD_ATTR float Rigidbody2D_get_mass_m816391807 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	typedef float (*Rigidbody2D_get_mass_m816391807_ftn) (Rigidbody2D_t939494601 *);
	static Rigidbody2D_get_mass_m816391807_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_mass_m816391807_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_mass()");
	float retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_mass_m3739581102 (Rigidbody2D_t939494601 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_set_mass_m3739581102_ftn) (Rigidbody2D_t939494601 *, float);
	static Rigidbody2D_set_mass_m3739581102_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_mass_m3739581102_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_gravityScale_m4129954518 (Rigidbody2D_t939494601 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_set_gravityScale_m4129954518_ftn) (Rigidbody2D_t939494601 *, float);
	static Rigidbody2D_set_gravityScale_m4129954518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_gravityScale_m4129954518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_gravityScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
extern "C" IL2CPP_METHOD_ATTR int32_t Rigidbody2D_get_bodyType_m541082067 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Rigidbody2D_get_bodyType_m541082067_ftn) (Rigidbody2D_t939494601 *);
	static Rigidbody2D_get_bodyType_m541082067_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_bodyType_m541082067_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_bodyType()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_bodyType_m1814100804 (Rigidbody2D_t939494601 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_set_bodyType_m1814100804_ftn) (Rigidbody2D_t939494601 *, int32_t);
	static Rigidbody2D_set_bodyType_m1814100804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_bodyType_m1814100804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern "C" IL2CPP_METHOD_ATTR bool Rigidbody2D_get_isKinematic_m2747706347 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = Rigidbody2D_get_bodyType_m541082067(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_isKinematic_m1070983191 (Rigidbody2D_t939494601 * __this, bool ___value0, const RuntimeMethod* method)
{
	Rigidbody2D_t939494601 * G_B2_0 = NULL;
	Rigidbody2D_t939494601 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Rigidbody2D_t939494601 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		Rigidbody2D_set_bodyType_m1814100804(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
extern "C" IL2CPP_METHOD_ATTR int32_t Rigidbody2D_get_constraints_m3318795165 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Rigidbody2D_get_constraints_m3318795165_ftn) (Rigidbody2D_t939494601 *);
	static Rigidbody2D_get_constraints_m3318795165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_constraints_m3318795165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_constraints()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_constraints_m439345971 (Rigidbody2D_t939494601 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_set_constraints_m439345971_ftn) (Rigidbody2D_t939494601 *, int32_t);
	static Rigidbody2D_set_constraints_m439345971_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_constraints_m439345971_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
extern "C" IL2CPP_METHOD_ATTR bool Rigidbody2D_IsSleeping_m292342806 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	typedef bool (*Rigidbody2D_IsSleeping_m292342806_ftn) (Rigidbody2D_t939494601 *);
	static Rigidbody2D_IsSleeping_m292342806_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsSleeping_m292342806_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsSleeping()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Rigidbody2D::Sleep()
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_Sleep_m2313562144 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_Sleep_m2313562144_ftn) (Rigidbody2D_t939494601 *);
	static Rigidbody2D_Sleep_m2313562144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_Sleep_m2313562144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::Sleep()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::WakeUp()
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_WakeUp_m2594938906 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_WakeUp_m2594938906_ftn) (Rigidbody2D_t939494601 *);
	static Rigidbody2D_WakeUp_m2594938906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_WakeUp_m2594938906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::WakeUp()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_m1099013366 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___force0, int32_t ___mode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_AddForce_Injected_m534098484(__this, (Vector2_t2156229523 *)(&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddRelativeForce_m472257838 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___relativeForce0, int32_t ___mode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_AddRelativeForce_Injected_m3238506296(__this, (Vector2_t2156229523 *)(&___relativeForce0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForceAtPosition_m845589146 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___force0, Vector2_t2156229523  ___position1, int32_t ___mode2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody2D_AddForceAtPosition_Injected_m220971265(__this, (Vector2_t2156229523 *)(&___force0), (Vector2_t2156229523 *)(&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddTorque_m3570114758 (Rigidbody2D_t939494601 * __this, float ___torque0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_AddTorque_m3570114758_ftn) (Rigidbody2D_t939494601 *, float, int32_t);
	static Rigidbody2D_AddTorque_m3570114758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddTorque_m3570114758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___torque0, ___mode1);
}
// System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_get_velocity_Injected_m2170787103 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_get_velocity_Injected_m2170787103_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *);
	static Rigidbody2D_get_velocity_Injected_m2170787103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_velocity_Injected_m2170787103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_Injected_m1505462814 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_set_velocity_Injected_m1505462814_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *);
	static Rigidbody2D_set_velocity_Injected_m1505462814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_velocity_Injected_m1505462814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::AddForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForce_Injected_m534098484 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___force0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_AddForce_Injected_m534098484_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *, int32_t);
	static Rigidbody2D_AddForce_Injected_m534098484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddForce_Injected_m534098484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___force0, ___mode1);
}
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddRelativeForce_Injected_m3238506296 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___relativeForce0, int32_t ___mode1, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_AddRelativeForce_Injected_m3238506296_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *, int32_t);
	static Rigidbody2D_AddRelativeForce_Injected_m3238506296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddRelativeForce_Injected_m3238506296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddRelativeForce_Injected(UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___relativeForce0, ___mode1);
}
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_AddForceAtPosition_Injected_m220971265 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___force0, Vector2_t2156229523 * ___position1, int32_t ___mode2, const RuntimeMethod* method)
{
	typedef void (*Rigidbody2D_AddForceAtPosition_Injected_m220971265_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *, Vector2_t2156229523 *, int32_t);
	static Rigidbody2D_AddForceAtPosition_Injected_m220971265_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddForceAtPosition_Injected_m220971265_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddForceAtPosition_Injected(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___force0, ___position1, ___mode2);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.JointSuspension2D UnityEngine.WheelJoint2D::get_suspension()
extern "C" IL2CPP_METHOD_ATTR JointSuspension2D_t3350541332  WheelJoint2D_get_suspension_m2921118779 (WheelJoint2D_t1756757149 * __this, const RuntimeMethod* method)
{
	JointSuspension2D_t3350541332  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		WheelJoint2D_get_suspension_Injected_m1956465358(__this, (JointSuspension2D_t3350541332 *)(&V_0), /*hidden argument*/NULL);
		JointSuspension2D_t3350541332  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.WheelJoint2D::set_suspension(UnityEngine.JointSuspension2D)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_suspension_m489707990 (WheelJoint2D_t1756757149 * __this, JointSuspension2D_t3350541332  ___value0, const RuntimeMethod* method)
{
	{
		WheelJoint2D_set_suspension_Injected_m332949253(__this, (JointSuspension2D_t3350541332 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_useMotor_m2386615263 (WheelJoint2D_t1756757149 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*WheelJoint2D_set_useMotor_m2386615263_ftn) (WheelJoint2D_t1756757149 *, bool);
	static WheelJoint2D_set_useMotor_m2386615263_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_set_useMotor_m2386615263_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.JointMotor2D UnityEngine.WheelJoint2D::get_motor()
extern "C" IL2CPP_METHOD_ATTR JointMotor2D_t142461323  WheelJoint2D_get_motor_m2209139078 (WheelJoint2D_t1756757149 * __this, const RuntimeMethod* method)
{
	JointMotor2D_t142461323  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		WheelJoint2D_get_motor_Injected_m1737353361(__this, (JointMotor2D_t142461323 *)(&V_0), /*hidden argument*/NULL);
		JointMotor2D_t142461323  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.WheelJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_motor_m1655428421 (WheelJoint2D_t1756757149 * __this, JointMotor2D_t142461323  ___value0, const RuntimeMethod* method)
{
	{
		WheelJoint2D_set_motor_Injected_m3222271241(__this, (JointMotor2D_t142461323 *)(&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WheelJoint2D::get_suspension_Injected(UnityEngine.JointSuspension2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_get_suspension_Injected_m1956465358 (WheelJoint2D_t1756757149 * __this, JointSuspension2D_t3350541332 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*WheelJoint2D_get_suspension_Injected_m1956465358_ftn) (WheelJoint2D_t1756757149 *, JointSuspension2D_t3350541332 *);
	static WheelJoint2D_get_suspension_Injected_m1956465358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_get_suspension_Injected_m1956465358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::get_suspension_Injected(UnityEngine.JointSuspension2D&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.WheelJoint2D::set_suspension_Injected(UnityEngine.JointSuspension2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_suspension_Injected_m332949253 (WheelJoint2D_t1756757149 * __this, JointSuspension2D_t3350541332 * ___value0, const RuntimeMethod* method)
{
	typedef void (*WheelJoint2D_set_suspension_Injected_m332949253_ftn) (WheelJoint2D_t1756757149 *, JointSuspension2D_t3350541332 *);
	static WheelJoint2D_set_suspension_Injected_m332949253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_set_suspension_Injected_m332949253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::set_suspension_Injected(UnityEngine.JointSuspension2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WheelJoint2D::get_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_get_motor_Injected_m1737353361 (WheelJoint2D_t1756757149 * __this, JointMotor2D_t142461323 * ___ret0, const RuntimeMethod* method)
{
	typedef void (*WheelJoint2D_get_motor_Injected_m1737353361_ftn) (WheelJoint2D_t1756757149 *, JointMotor2D_t142461323 *);
	static WheelJoint2D_get_motor_Injected_m1737353361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_get_motor_Injected_m1737353361_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::get_motor_Injected(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___ret0);
}
// System.Void UnityEngine.WheelJoint2D::set_motor_Injected(UnityEngine.JointMotor2D&)
extern "C" IL2CPP_METHOD_ATTR void WheelJoint2D_set_motor_Injected_m3222271241 (WheelJoint2D_t1756757149 * __this, JointMotor2D_t142461323 * ___value0, const RuntimeMethod* method)
{
	typedef void (*WheelJoint2D_set_motor_Injected_m3222271241_ftn) (WheelJoint2D_t1756757149 *, JointMotor2D_t142461323 *);
	static WheelJoint2D_set_motor_Injected_m3222271241_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_set_motor_Injected_m3222271241_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::set_motor_Injected(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
