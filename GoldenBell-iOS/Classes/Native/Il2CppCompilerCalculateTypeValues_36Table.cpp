﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction
struct EasingFunction_t338733390;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t3987626228;
// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t1756862219;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t919699796;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2511618479;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t2057874452;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t4259038327;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t3649179877;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t522200103;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t1738787045;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2413660594;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t863727431;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2965096677;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t1722713284;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t187323908;
// HutongGames.PlayMaker.INamedVariable[]
struct INamedVariableU5BU5D_t3347979629;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t2808389578;
// HutongGames.PlayMaker.NamedVariable[]
struct NamedVariableU5BU5D_t58099151;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef EASETYPE_T1028795025_H
#define EASETYPE_T1028795025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType
struct  EaseType_t1028795025 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t1028795025, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T1028795025_H
#ifndef TIMEINFO_T2103361344_H
#define TIMEINFO_T2103361344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTimeInfo/TimeInfo
struct  TimeInfo_t2103361344 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetTimeInfo/TimeInfo::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TimeInfo_t2103361344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEINFO_T2103361344_H
#ifndef MESSAGETYPE_T2684925397_H
#define MESSAGETYPE_T2684925397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendMessage/MessageType
struct  MessageType_t2684925397 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.SendMessage/MessageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MessageType_t2684925397, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETYPE_T2684925397_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef SENDMESSAGEOPTIONS_T3580193095_H
#define SENDMESSAGEOPTIONS_T3580193095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t3580193095 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t3580193095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T3580193095_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef ADDSCRIPT_T769303531_H
#define ADDSCRIPT_T769303531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AddScript
struct  AddScript_t769303531  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.AddScript::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AddScript::script
	FsmString_t1785915204 * ___script_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.AddScript::removeOnExit
	FsmBool_t163807967 * ___removeOnExit_16;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.AddScript::addedComponent
	Component_t1923634451 * ___addedComponent_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(AddScript_t769303531, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_script_15() { return static_cast<int32_t>(offsetof(AddScript_t769303531, ___script_15)); }
	inline FsmString_t1785915204 * get_script_15() const { return ___script_15; }
	inline FsmString_t1785915204 ** get_address_of_script_15() { return &___script_15; }
	inline void set_script_15(FsmString_t1785915204 * value)
	{
		___script_15 = value;
		Il2CppCodeGenWriteBarrier((&___script_15), value);
	}

	inline static int32_t get_offset_of_removeOnExit_16() { return static_cast<int32_t>(offsetof(AddScript_t769303531, ___removeOnExit_16)); }
	inline FsmBool_t163807967 * get_removeOnExit_16() const { return ___removeOnExit_16; }
	inline FsmBool_t163807967 ** get_address_of_removeOnExit_16() { return &___removeOnExit_16; }
	inline void set_removeOnExit_16(FsmBool_t163807967 * value)
	{
		___removeOnExit_16 = value;
		Il2CppCodeGenWriteBarrier((&___removeOnExit_16), value);
	}

	inline static int32_t get_offset_of_addedComponent_17() { return static_cast<int32_t>(offsetof(AddScript_t769303531, ___addedComponent_17)); }
	inline Component_t1923634451 * get_addedComponent_17() const { return ___addedComponent_17; }
	inline Component_t1923634451 ** get_address_of_addedComponent_17() { return &___addedComponent_17; }
	inline void set_addedComponent_17(Component_t1923634451 * value)
	{
		___addedComponent_17 = value;
		Il2CppCodeGenWriteBarrier((&___addedComponent_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDSCRIPT_T769303531_H
#ifndef AXISEVENT_T353447656_H
#define AXISEVENT_T353447656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AxisEvent
struct  AxisEvent_t353447656  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AxisEvent::horizontalAxis
	FsmString_t1785915204 * ___horizontalAxis_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.AxisEvent::verticalAxis
	FsmString_t1785915204 * ___verticalAxis_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::leftEvent
	FsmEvent_t3736299882 * ___leftEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::rightEvent
	FsmEvent_t3736299882 * ___rightEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::upEvent
	FsmEvent_t3736299882 * ___upEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::downEvent
	FsmEvent_t3736299882 * ___downEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::anyDirection
	FsmEvent_t3736299882 * ___anyDirection_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AxisEvent::noDirection
	FsmEvent_t3736299882 * ___noDirection_21;

public:
	inline static int32_t get_offset_of_horizontalAxis_14() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___horizontalAxis_14)); }
	inline FsmString_t1785915204 * get_horizontalAxis_14() const { return ___horizontalAxis_14; }
	inline FsmString_t1785915204 ** get_address_of_horizontalAxis_14() { return &___horizontalAxis_14; }
	inline void set_horizontalAxis_14(FsmString_t1785915204 * value)
	{
		___horizontalAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_14), value);
	}

	inline static int32_t get_offset_of_verticalAxis_15() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___verticalAxis_15)); }
	inline FsmString_t1785915204 * get_verticalAxis_15() const { return ___verticalAxis_15; }
	inline FsmString_t1785915204 ** get_address_of_verticalAxis_15() { return &___verticalAxis_15; }
	inline void set_verticalAxis_15(FsmString_t1785915204 * value)
	{
		___verticalAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_15), value);
	}

	inline static int32_t get_offset_of_leftEvent_16() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___leftEvent_16)); }
	inline FsmEvent_t3736299882 * get_leftEvent_16() const { return ___leftEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_leftEvent_16() { return &___leftEvent_16; }
	inline void set_leftEvent_16(FsmEvent_t3736299882 * value)
	{
		___leftEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___leftEvent_16), value);
	}

	inline static int32_t get_offset_of_rightEvent_17() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___rightEvent_17)); }
	inline FsmEvent_t3736299882 * get_rightEvent_17() const { return ___rightEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_rightEvent_17() { return &___rightEvent_17; }
	inline void set_rightEvent_17(FsmEvent_t3736299882 * value)
	{
		___rightEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___rightEvent_17), value);
	}

	inline static int32_t get_offset_of_upEvent_18() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___upEvent_18)); }
	inline FsmEvent_t3736299882 * get_upEvent_18() const { return ___upEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_upEvent_18() { return &___upEvent_18; }
	inline void set_upEvent_18(FsmEvent_t3736299882 * value)
	{
		___upEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___upEvent_18), value);
	}

	inline static int32_t get_offset_of_downEvent_19() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___downEvent_19)); }
	inline FsmEvent_t3736299882 * get_downEvent_19() const { return ___downEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_downEvent_19() { return &___downEvent_19; }
	inline void set_downEvent_19(FsmEvent_t3736299882 * value)
	{
		___downEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___downEvent_19), value);
	}

	inline static int32_t get_offset_of_anyDirection_20() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___anyDirection_20)); }
	inline FsmEvent_t3736299882 * get_anyDirection_20() const { return ___anyDirection_20; }
	inline FsmEvent_t3736299882 ** get_address_of_anyDirection_20() { return &___anyDirection_20; }
	inline void set_anyDirection_20(FsmEvent_t3736299882 * value)
	{
		___anyDirection_20 = value;
		Il2CppCodeGenWriteBarrier((&___anyDirection_20), value);
	}

	inline static int32_t get_offset_of_noDirection_21() { return static_cast<int32_t>(offsetof(AxisEvent_t353447656, ___noDirection_21)); }
	inline FsmEvent_t3736299882 * get_noDirection_21() const { return ___noDirection_21; }
	inline FsmEvent_t3736299882 ** get_address_of_noDirection_21() { return &___noDirection_21; }
	inline void set_noDirection_21(FsmEvent_t3736299882 * value)
	{
		___noDirection_21 = value;
		Il2CppCodeGenWriteBarrier((&___noDirection_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENT_T353447656_H
#ifndef BASEFSMVARIABLEACTION_T2713817827_H
#define BASEFSMVARIABLEACTION_T2713817827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseFsmVariableAction
struct  BaseFsmVariableAction_t2713817827  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableAction::fsmNotFound
	FsmEvent_t3736299882 * ___fsmNotFound_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableAction::variableNotFound
	FsmEvent_t3736299882 * ___variableNotFound_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.BaseFsmVariableAction::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_16;
	// System.String HutongGames.PlayMaker.Actions.BaseFsmVariableAction::cachedFsmName
	String_t* ___cachedFsmName_17;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.BaseFsmVariableAction::fsm
	PlayMakerFSM_t1613010231 * ___fsm_18;

public:
	inline static int32_t get_offset_of_fsmNotFound_14() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___fsmNotFound_14)); }
	inline FsmEvent_t3736299882 * get_fsmNotFound_14() const { return ___fsmNotFound_14; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmNotFound_14() { return &___fsmNotFound_14; }
	inline void set_fsmNotFound_14(FsmEvent_t3736299882 * value)
	{
		___fsmNotFound_14 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNotFound_14), value);
	}

	inline static int32_t get_offset_of_variableNotFound_15() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___variableNotFound_15)); }
	inline FsmEvent_t3736299882 * get_variableNotFound_15() const { return ___variableNotFound_15; }
	inline FsmEvent_t3736299882 ** get_address_of_variableNotFound_15() { return &___variableNotFound_15; }
	inline void set_variableNotFound_15(FsmEvent_t3736299882 * value)
	{
		___variableNotFound_15 = value;
		Il2CppCodeGenWriteBarrier((&___variableNotFound_15), value);
	}

	inline static int32_t get_offset_of_cachedGameObject_16() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___cachedGameObject_16)); }
	inline GameObject_t1113636619 * get_cachedGameObject_16() const { return ___cachedGameObject_16; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_16() { return &___cachedGameObject_16; }
	inline void set_cachedGameObject_16(GameObject_t1113636619 * value)
	{
		___cachedGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_16), value);
	}

	inline static int32_t get_offset_of_cachedFsmName_17() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___cachedFsmName_17)); }
	inline String_t* get_cachedFsmName_17() const { return ___cachedFsmName_17; }
	inline String_t** get_address_of_cachedFsmName_17() { return &___cachedFsmName_17; }
	inline void set_cachedFsmName_17(String_t* value)
	{
		___cachedFsmName_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFsmName_17), value);
	}

	inline static int32_t get_offset_of_fsm_18() { return static_cast<int32_t>(offsetof(BaseFsmVariableAction_t2713817827, ___fsm_18)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_18() const { return ___fsm_18; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_18() { return &___fsm_18; }
	inline void set_fsm_18(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_18 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFSMVARIABLEACTION_T2713817827_H
#ifndef BASEFSMVARIABLEINDEXACTION_T3448140877_H
#define BASEFSMVARIABLEINDEXACTION_T3448140877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction
struct  BaseFsmVariableIndexAction_t3448140877  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::indexOutOfRange
	FsmEvent_t3736299882 * ___indexOutOfRange_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::fsmNotFound
	FsmEvent_t3736299882 * ___fsmNotFound_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::variableNotFound
	FsmEvent_t3736299882 * ___variableNotFound_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_17;
	// System.String HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::cachedFsmName
	String_t* ___cachedFsmName_18;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::fsm
	PlayMakerFSM_t1613010231 * ___fsm_19;

public:
	inline static int32_t get_offset_of_indexOutOfRange_14() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___indexOutOfRange_14)); }
	inline FsmEvent_t3736299882 * get_indexOutOfRange_14() const { return ___indexOutOfRange_14; }
	inline FsmEvent_t3736299882 ** get_address_of_indexOutOfRange_14() { return &___indexOutOfRange_14; }
	inline void set_indexOutOfRange_14(FsmEvent_t3736299882 * value)
	{
		___indexOutOfRange_14 = value;
		Il2CppCodeGenWriteBarrier((&___indexOutOfRange_14), value);
	}

	inline static int32_t get_offset_of_fsmNotFound_15() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___fsmNotFound_15)); }
	inline FsmEvent_t3736299882 * get_fsmNotFound_15() const { return ___fsmNotFound_15; }
	inline FsmEvent_t3736299882 ** get_address_of_fsmNotFound_15() { return &___fsmNotFound_15; }
	inline void set_fsmNotFound_15(FsmEvent_t3736299882 * value)
	{
		___fsmNotFound_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNotFound_15), value);
	}

	inline static int32_t get_offset_of_variableNotFound_16() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___variableNotFound_16)); }
	inline FsmEvent_t3736299882 * get_variableNotFound_16() const { return ___variableNotFound_16; }
	inline FsmEvent_t3736299882 ** get_address_of_variableNotFound_16() { return &___variableNotFound_16; }
	inline void set_variableNotFound_16(FsmEvent_t3736299882 * value)
	{
		___variableNotFound_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableNotFound_16), value);
	}

	inline static int32_t get_offset_of_cachedGameObject_17() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___cachedGameObject_17)); }
	inline GameObject_t1113636619 * get_cachedGameObject_17() const { return ___cachedGameObject_17; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_17() { return &___cachedGameObject_17; }
	inline void set_cachedGameObject_17(GameObject_t1113636619 * value)
	{
		___cachedGameObject_17 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_17), value);
	}

	inline static int32_t get_offset_of_cachedFsmName_18() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___cachedFsmName_18)); }
	inline String_t* get_cachedFsmName_18() const { return ___cachedFsmName_18; }
	inline String_t** get_address_of_cachedFsmName_18() { return &___cachedFsmName_18; }
	inline void set_cachedFsmName_18(String_t* value)
	{
		___cachedFsmName_18 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFsmName_18), value);
	}

	inline static int32_t get_offset_of_fsm_19() { return static_cast<int32_t>(offsetof(BaseFsmVariableIndexAction_t3448140877, ___fsm_19)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_19() const { return ___fsm_19; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_19() { return &___fsm_19; }
	inline void set_fsm_19(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_19 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEFSMVARIABLEINDEXACTION_T3448140877_H
#ifndef BROADCASTEVENT_T1114459934_H
#define BROADCASTEVENT_T1114459934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BroadcastEvent
struct  BroadcastEvent_t1114459934  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BroadcastEvent::broadcastEvent
	FsmString_t1785915204 * ___broadcastEvent_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.BroadcastEvent::gameObject
	FsmGameObject_t3581898942 * ___gameObject_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BroadcastEvent::sendToChildren
	FsmBool_t163807967 * ___sendToChildren_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BroadcastEvent::excludeSelf
	FsmBool_t163807967 * ___excludeSelf_17;

public:
	inline static int32_t get_offset_of_broadcastEvent_14() { return static_cast<int32_t>(offsetof(BroadcastEvent_t1114459934, ___broadcastEvent_14)); }
	inline FsmString_t1785915204 * get_broadcastEvent_14() const { return ___broadcastEvent_14; }
	inline FsmString_t1785915204 ** get_address_of_broadcastEvent_14() { return &___broadcastEvent_14; }
	inline void set_broadcastEvent_14(FsmString_t1785915204 * value)
	{
		___broadcastEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___broadcastEvent_14), value);
	}

	inline static int32_t get_offset_of_gameObject_15() { return static_cast<int32_t>(offsetof(BroadcastEvent_t1114459934, ___gameObject_15)); }
	inline FsmGameObject_t3581898942 * get_gameObject_15() const { return ___gameObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_15() { return &___gameObject_15; }
	inline void set_gameObject_15(FsmGameObject_t3581898942 * value)
	{
		___gameObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_15), value);
	}

	inline static int32_t get_offset_of_sendToChildren_16() { return static_cast<int32_t>(offsetof(BroadcastEvent_t1114459934, ___sendToChildren_16)); }
	inline FsmBool_t163807967 * get_sendToChildren_16() const { return ___sendToChildren_16; }
	inline FsmBool_t163807967 ** get_address_of_sendToChildren_16() { return &___sendToChildren_16; }
	inline void set_sendToChildren_16(FsmBool_t163807967 * value)
	{
		___sendToChildren_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendToChildren_16), value);
	}

	inline static int32_t get_offset_of_excludeSelf_17() { return static_cast<int32_t>(offsetof(BroadcastEvent_t1114459934, ___excludeSelf_17)); }
	inline FsmBool_t163807967 * get_excludeSelf_17() const { return ___excludeSelf_17; }
	inline FsmBool_t163807967 ** get_address_of_excludeSelf_17() { return &___excludeSelf_17; }
	inline void set_excludeSelf_17(FsmBool_t163807967 * value)
	{
		___excludeSelf_17 = value;
		Il2CppCodeGenWriteBarrier((&___excludeSelf_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROADCASTEVENT_T1114459934_H
#ifndef BUILDSTRING_T3135795457_H
#define BUILDSTRING_T3135795457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BuildString
struct  BuildString_t3135795457  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.BuildString::stringParts
	FsmStringU5BU5D_t252501805* ___stringParts_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BuildString::separator
	FsmString_t1785915204 * ___separator_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BuildString::addToEnd
	FsmBool_t163807967 * ___addToEnd_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.BuildString::storeResult
	FsmString_t1785915204 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.BuildString::everyFrame
	bool ___everyFrame_18;
	// System.String HutongGames.PlayMaker.Actions.BuildString::result
	String_t* ___result_19;

public:
	inline static int32_t get_offset_of_stringParts_14() { return static_cast<int32_t>(offsetof(BuildString_t3135795457, ___stringParts_14)); }
	inline FsmStringU5BU5D_t252501805* get_stringParts_14() const { return ___stringParts_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_stringParts_14() { return &___stringParts_14; }
	inline void set_stringParts_14(FsmStringU5BU5D_t252501805* value)
	{
		___stringParts_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringParts_14), value);
	}

	inline static int32_t get_offset_of_separator_15() { return static_cast<int32_t>(offsetof(BuildString_t3135795457, ___separator_15)); }
	inline FsmString_t1785915204 * get_separator_15() const { return ___separator_15; }
	inline FsmString_t1785915204 ** get_address_of_separator_15() { return &___separator_15; }
	inline void set_separator_15(FsmString_t1785915204 * value)
	{
		___separator_15 = value;
		Il2CppCodeGenWriteBarrier((&___separator_15), value);
	}

	inline static int32_t get_offset_of_addToEnd_16() { return static_cast<int32_t>(offsetof(BuildString_t3135795457, ___addToEnd_16)); }
	inline FsmBool_t163807967 * get_addToEnd_16() const { return ___addToEnd_16; }
	inline FsmBool_t163807967 ** get_address_of_addToEnd_16() { return &___addToEnd_16; }
	inline void set_addToEnd_16(FsmBool_t163807967 * value)
	{
		___addToEnd_16 = value;
		Il2CppCodeGenWriteBarrier((&___addToEnd_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(BuildString_t3135795457, ___storeResult_17)); }
	inline FsmString_t1785915204 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmString_t1785915204 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(BuildString_t3135795457, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_result_19() { return static_cast<int32_t>(offsetof(BuildString_t3135795457, ___result_19)); }
	inline String_t* get_result_19() const { return ___result_19; }
	inline String_t** get_address_of_result_19() { return &___result_19; }
	inline void set_result_19(String_t* value)
	{
		___result_19 = value;
		Il2CppCodeGenWriteBarrier((&___result_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDSTRING_T3135795457_H
#ifndef CALLMETHOD_T2402822853_H
#define CALLMETHOD_T2402822853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CallMethod
struct  CallMethod_t2402822853  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.CallMethod::behaviour
	FsmObject_t2606870197 * ___behaviour_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CallMethod::methodName
	FsmString_t1785915204 * ___methodName_15;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.CallMethod::parameters
	FsmVarU5BU5D_t863727431* ___parameters_16;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.CallMethod::storeResult
	FsmVar_t2413660594 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.CallMethod::everyFrame
	bool ___everyFrame_18;
	// System.Boolean HutongGames.PlayMaker.Actions.CallMethod::manualUI
	bool ___manualUI_19;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.CallMethod::cachedBehaviour
	FsmObject_t2606870197 * ___cachedBehaviour_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CallMethod::cachedMethodName
	FsmString_t1785915204 * ___cachedMethodName_21;
	// System.Type HutongGames.PlayMaker.Actions.CallMethod::cachedType
	Type_t * ___cachedType_22;
	// System.Reflection.MethodInfo HutongGames.PlayMaker.Actions.CallMethod::cachedMethodInfo
	MethodInfo_t * ___cachedMethodInfo_23;
	// System.Reflection.ParameterInfo[] HutongGames.PlayMaker.Actions.CallMethod::cachedParameterInfo
	ParameterInfoU5BU5D_t390618515* ___cachedParameterInfo_24;
	// System.Object[] HutongGames.PlayMaker.Actions.CallMethod::parametersArray
	ObjectU5BU5D_t2843939325* ___parametersArray_25;
	// System.String HutongGames.PlayMaker.Actions.CallMethod::errorString
	String_t* ___errorString_26;

public:
	inline static int32_t get_offset_of_behaviour_14() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___behaviour_14)); }
	inline FsmObject_t2606870197 * get_behaviour_14() const { return ___behaviour_14; }
	inline FsmObject_t2606870197 ** get_address_of_behaviour_14() { return &___behaviour_14; }
	inline void set_behaviour_14(FsmObject_t2606870197 * value)
	{
		___behaviour_14 = value;
		Il2CppCodeGenWriteBarrier((&___behaviour_14), value);
	}

	inline static int32_t get_offset_of_methodName_15() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___methodName_15)); }
	inline FsmString_t1785915204 * get_methodName_15() const { return ___methodName_15; }
	inline FsmString_t1785915204 ** get_address_of_methodName_15() { return &___methodName_15; }
	inline void set_methodName_15(FsmString_t1785915204 * value)
	{
		___methodName_15 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_15), value);
	}

	inline static int32_t get_offset_of_parameters_16() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___parameters_16)); }
	inline FsmVarU5BU5D_t863727431* get_parameters_16() const { return ___parameters_16; }
	inline FsmVarU5BU5D_t863727431** get_address_of_parameters_16() { return &___parameters_16; }
	inline void set_parameters_16(FsmVarU5BU5D_t863727431* value)
	{
		___parameters_16 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___storeResult_17)); }
	inline FsmVar_t2413660594 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmVar_t2413660594 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmVar_t2413660594 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_manualUI_19() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___manualUI_19)); }
	inline bool get_manualUI_19() const { return ___manualUI_19; }
	inline bool* get_address_of_manualUI_19() { return &___manualUI_19; }
	inline void set_manualUI_19(bool value)
	{
		___manualUI_19 = value;
	}

	inline static int32_t get_offset_of_cachedBehaviour_20() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___cachedBehaviour_20)); }
	inline FsmObject_t2606870197 * get_cachedBehaviour_20() const { return ___cachedBehaviour_20; }
	inline FsmObject_t2606870197 ** get_address_of_cachedBehaviour_20() { return &___cachedBehaviour_20; }
	inline void set_cachedBehaviour_20(FsmObject_t2606870197 * value)
	{
		___cachedBehaviour_20 = value;
		Il2CppCodeGenWriteBarrier((&___cachedBehaviour_20), value);
	}

	inline static int32_t get_offset_of_cachedMethodName_21() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___cachedMethodName_21)); }
	inline FsmString_t1785915204 * get_cachedMethodName_21() const { return ___cachedMethodName_21; }
	inline FsmString_t1785915204 ** get_address_of_cachedMethodName_21() { return &___cachedMethodName_21; }
	inline void set_cachedMethodName_21(FsmString_t1785915204 * value)
	{
		___cachedMethodName_21 = value;
		Il2CppCodeGenWriteBarrier((&___cachedMethodName_21), value);
	}

	inline static int32_t get_offset_of_cachedType_22() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___cachedType_22)); }
	inline Type_t * get_cachedType_22() const { return ___cachedType_22; }
	inline Type_t ** get_address_of_cachedType_22() { return &___cachedType_22; }
	inline void set_cachedType_22(Type_t * value)
	{
		___cachedType_22 = value;
		Il2CppCodeGenWriteBarrier((&___cachedType_22), value);
	}

	inline static int32_t get_offset_of_cachedMethodInfo_23() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___cachedMethodInfo_23)); }
	inline MethodInfo_t * get_cachedMethodInfo_23() const { return ___cachedMethodInfo_23; }
	inline MethodInfo_t ** get_address_of_cachedMethodInfo_23() { return &___cachedMethodInfo_23; }
	inline void set_cachedMethodInfo_23(MethodInfo_t * value)
	{
		___cachedMethodInfo_23 = value;
		Il2CppCodeGenWriteBarrier((&___cachedMethodInfo_23), value);
	}

	inline static int32_t get_offset_of_cachedParameterInfo_24() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___cachedParameterInfo_24)); }
	inline ParameterInfoU5BU5D_t390618515* get_cachedParameterInfo_24() const { return ___cachedParameterInfo_24; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_cachedParameterInfo_24() { return &___cachedParameterInfo_24; }
	inline void set_cachedParameterInfo_24(ParameterInfoU5BU5D_t390618515* value)
	{
		___cachedParameterInfo_24 = value;
		Il2CppCodeGenWriteBarrier((&___cachedParameterInfo_24), value);
	}

	inline static int32_t get_offset_of_parametersArray_25() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___parametersArray_25)); }
	inline ObjectU5BU5D_t2843939325* get_parametersArray_25() const { return ___parametersArray_25; }
	inline ObjectU5BU5D_t2843939325** get_address_of_parametersArray_25() { return &___parametersArray_25; }
	inline void set_parametersArray_25(ObjectU5BU5D_t2843939325* value)
	{
		___parametersArray_25 = value;
		Il2CppCodeGenWriteBarrier((&___parametersArray_25), value);
	}

	inline static int32_t get_offset_of_errorString_26() { return static_cast<int32_t>(offsetof(CallMethod_t2402822853, ___errorString_26)); }
	inline String_t* get_errorString_26() const { return ___errorString_26; }
	inline String_t** get_address_of_errorString_26() { return &___errorString_26; }
	inline void set_errorString_26(String_t* value)
	{
		___errorString_26 = value;
		Il2CppCodeGenWriteBarrier((&___errorString_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLMETHOD_T2402822853_H
#ifndef CALLSTATICMETHOD_T3442263793_H
#define CALLSTATICMETHOD_T3442263793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CallStaticMethod
struct  CallStaticMethod_t3442263793  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CallStaticMethod::className
	FsmString_t1785915204 * ___className_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.CallStaticMethod::methodName
	FsmString_t1785915204 * ___methodName_15;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.CallStaticMethod::parameters
	FsmVarU5BU5D_t863727431* ___parameters_16;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.CallStaticMethod::storeResult
	FsmVar_t2413660594 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.CallStaticMethod::everyFrame
	bool ___everyFrame_18;
	// System.Type HutongGames.PlayMaker.Actions.CallStaticMethod::cachedType
	Type_t * ___cachedType_19;
	// System.String HutongGames.PlayMaker.Actions.CallStaticMethod::cachedClassName
	String_t* ___cachedClassName_20;
	// System.String HutongGames.PlayMaker.Actions.CallStaticMethod::cachedMethodName
	String_t* ___cachedMethodName_21;
	// System.Reflection.MethodInfo HutongGames.PlayMaker.Actions.CallStaticMethod::cachedMethodInfo
	MethodInfo_t * ___cachedMethodInfo_22;
	// System.Reflection.ParameterInfo[] HutongGames.PlayMaker.Actions.CallStaticMethod::cachedParameterInfo
	ParameterInfoU5BU5D_t390618515* ___cachedParameterInfo_23;
	// System.Object[] HutongGames.PlayMaker.Actions.CallStaticMethod::parametersArray
	ObjectU5BU5D_t2843939325* ___parametersArray_24;
	// System.String HutongGames.PlayMaker.Actions.CallStaticMethod::errorString
	String_t* ___errorString_25;

public:
	inline static int32_t get_offset_of_className_14() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___className_14)); }
	inline FsmString_t1785915204 * get_className_14() const { return ___className_14; }
	inline FsmString_t1785915204 ** get_address_of_className_14() { return &___className_14; }
	inline void set_className_14(FsmString_t1785915204 * value)
	{
		___className_14 = value;
		Il2CppCodeGenWriteBarrier((&___className_14), value);
	}

	inline static int32_t get_offset_of_methodName_15() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___methodName_15)); }
	inline FsmString_t1785915204 * get_methodName_15() const { return ___methodName_15; }
	inline FsmString_t1785915204 ** get_address_of_methodName_15() { return &___methodName_15; }
	inline void set_methodName_15(FsmString_t1785915204 * value)
	{
		___methodName_15 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_15), value);
	}

	inline static int32_t get_offset_of_parameters_16() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___parameters_16)); }
	inline FsmVarU5BU5D_t863727431* get_parameters_16() const { return ___parameters_16; }
	inline FsmVarU5BU5D_t863727431** get_address_of_parameters_16() { return &___parameters_16; }
	inline void set_parameters_16(FsmVarU5BU5D_t863727431* value)
	{
		___parameters_16 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___storeResult_17)); }
	inline FsmVar_t2413660594 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmVar_t2413660594 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmVar_t2413660594 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_cachedType_19() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___cachedType_19)); }
	inline Type_t * get_cachedType_19() const { return ___cachedType_19; }
	inline Type_t ** get_address_of_cachedType_19() { return &___cachedType_19; }
	inline void set_cachedType_19(Type_t * value)
	{
		___cachedType_19 = value;
		Il2CppCodeGenWriteBarrier((&___cachedType_19), value);
	}

	inline static int32_t get_offset_of_cachedClassName_20() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___cachedClassName_20)); }
	inline String_t* get_cachedClassName_20() const { return ___cachedClassName_20; }
	inline String_t** get_address_of_cachedClassName_20() { return &___cachedClassName_20; }
	inline void set_cachedClassName_20(String_t* value)
	{
		___cachedClassName_20 = value;
		Il2CppCodeGenWriteBarrier((&___cachedClassName_20), value);
	}

	inline static int32_t get_offset_of_cachedMethodName_21() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___cachedMethodName_21)); }
	inline String_t* get_cachedMethodName_21() const { return ___cachedMethodName_21; }
	inline String_t** get_address_of_cachedMethodName_21() { return &___cachedMethodName_21; }
	inline void set_cachedMethodName_21(String_t* value)
	{
		___cachedMethodName_21 = value;
		Il2CppCodeGenWriteBarrier((&___cachedMethodName_21), value);
	}

	inline static int32_t get_offset_of_cachedMethodInfo_22() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___cachedMethodInfo_22)); }
	inline MethodInfo_t * get_cachedMethodInfo_22() const { return ___cachedMethodInfo_22; }
	inline MethodInfo_t ** get_address_of_cachedMethodInfo_22() { return &___cachedMethodInfo_22; }
	inline void set_cachedMethodInfo_22(MethodInfo_t * value)
	{
		___cachedMethodInfo_22 = value;
		Il2CppCodeGenWriteBarrier((&___cachedMethodInfo_22), value);
	}

	inline static int32_t get_offset_of_cachedParameterInfo_23() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___cachedParameterInfo_23)); }
	inline ParameterInfoU5BU5D_t390618515* get_cachedParameterInfo_23() const { return ___cachedParameterInfo_23; }
	inline ParameterInfoU5BU5D_t390618515** get_address_of_cachedParameterInfo_23() { return &___cachedParameterInfo_23; }
	inline void set_cachedParameterInfo_23(ParameterInfoU5BU5D_t390618515* value)
	{
		___cachedParameterInfo_23 = value;
		Il2CppCodeGenWriteBarrier((&___cachedParameterInfo_23), value);
	}

	inline static int32_t get_offset_of_parametersArray_24() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___parametersArray_24)); }
	inline ObjectU5BU5D_t2843939325* get_parametersArray_24() const { return ___parametersArray_24; }
	inline ObjectU5BU5D_t2843939325** get_address_of_parametersArray_24() { return &___parametersArray_24; }
	inline void set_parametersArray_24(ObjectU5BU5D_t2843939325* value)
	{
		___parametersArray_24 = value;
		Il2CppCodeGenWriteBarrier((&___parametersArray_24), value);
	}

	inline static int32_t get_offset_of_errorString_25() { return static_cast<int32_t>(offsetof(CallStaticMethod_t3442263793, ___errorString_25)); }
	inline String_t* get_errorString_25() const { return ___errorString_25; }
	inline String_t** get_address_of_errorString_25() { return &___errorString_25; }
	inline void set_errorString_25(String_t* value)
	{
		___errorString_25 = value;
		Il2CppCodeGenWriteBarrier((&___errorString_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSTATICMETHOD_T3442263793_H
#ifndef EASEFSMACTION_T2585076379_H
#define EASEFSMACTION_T2585076379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EaseFsmAction
struct  EaseFsmAction_t2585076379  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::time
	FsmFloat_t2883254149 * ___time_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::speed
	FsmFloat_t2883254149 * ___speed_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EaseFsmAction::delay
	FsmFloat_t2883254149 * ___delay_16;
	// HutongGames.PlayMaker.Actions.EaseFsmAction/EaseType HutongGames.PlayMaker.Actions.EaseFsmAction::easeType
	int32_t ___easeType_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EaseFsmAction::reverse
	FsmBool_t163807967 * ___reverse_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EaseFsmAction::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_19;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::realTime
	bool ___realTime_20;
	// HutongGames.PlayMaker.Actions.EaseFsmAction/EasingFunction HutongGames.PlayMaker.Actions.EaseFsmAction::ease
	EasingFunction_t338733390 * ___ease_21;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::runningTime
	float ___runningTime_22;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::lastTime
	float ___lastTime_23;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::startTime
	float ___startTime_24;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::deltaTime
	float ___deltaTime_25;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::delayTime
	float ___delayTime_26;
	// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::percentage
	float ___percentage_27;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::fromFloats
	SingleU5BU5D_t1444911251* ___fromFloats_28;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::toFloats
	SingleU5BU5D_t1444911251* ___toFloats_29;
	// System.Single[] HutongGames.PlayMaker.Actions.EaseFsmAction::resultFloats
	SingleU5BU5D_t1444911251* ___resultFloats_30;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::finishAction
	bool ___finishAction_31;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::start
	bool ___start_32;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::finished
	bool ___finished_33;
	// System.Boolean HutongGames.PlayMaker.Actions.EaseFsmAction::isRunning
	bool ___isRunning_34;

public:
	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___time_14)); }
	inline FsmFloat_t2883254149 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2883254149 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2883254149 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier((&___time_14), value);
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___speed_15)); }
	inline FsmFloat_t2883254149 * get_speed_15() const { return ___speed_15; }
	inline FsmFloat_t2883254149 ** get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(FsmFloat_t2883254149 * value)
	{
		___speed_15 = value;
		Il2CppCodeGenWriteBarrier((&___speed_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_easeType_17() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___easeType_17)); }
	inline int32_t get_easeType_17() const { return ___easeType_17; }
	inline int32_t* get_address_of_easeType_17() { return &___easeType_17; }
	inline void set_easeType_17(int32_t value)
	{
		___easeType_17 = value;
	}

	inline static int32_t get_offset_of_reverse_18() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___reverse_18)); }
	inline FsmBool_t163807967 * get_reverse_18() const { return ___reverse_18; }
	inline FsmBool_t163807967 ** get_address_of_reverse_18() { return &___reverse_18; }
	inline void set_reverse_18(FsmBool_t163807967 * value)
	{
		___reverse_18 = value;
		Il2CppCodeGenWriteBarrier((&___reverse_18), value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___finishEvent_19)); }
	inline FsmEvent_t3736299882 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t3736299882 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_19), value);
	}

	inline static int32_t get_offset_of_realTime_20() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___realTime_20)); }
	inline bool get_realTime_20() const { return ___realTime_20; }
	inline bool* get_address_of_realTime_20() { return &___realTime_20; }
	inline void set_realTime_20(bool value)
	{
		___realTime_20 = value;
	}

	inline static int32_t get_offset_of_ease_21() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___ease_21)); }
	inline EasingFunction_t338733390 * get_ease_21() const { return ___ease_21; }
	inline EasingFunction_t338733390 ** get_address_of_ease_21() { return &___ease_21; }
	inline void set_ease_21(EasingFunction_t338733390 * value)
	{
		___ease_21 = value;
		Il2CppCodeGenWriteBarrier((&___ease_21), value);
	}

	inline static int32_t get_offset_of_runningTime_22() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___runningTime_22)); }
	inline float get_runningTime_22() const { return ___runningTime_22; }
	inline float* get_address_of_runningTime_22() { return &___runningTime_22; }
	inline void set_runningTime_22(float value)
	{
		___runningTime_22 = value;
	}

	inline static int32_t get_offset_of_lastTime_23() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___lastTime_23)); }
	inline float get_lastTime_23() const { return ___lastTime_23; }
	inline float* get_address_of_lastTime_23() { return &___lastTime_23; }
	inline void set_lastTime_23(float value)
	{
		___lastTime_23 = value;
	}

	inline static int32_t get_offset_of_startTime_24() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___startTime_24)); }
	inline float get_startTime_24() const { return ___startTime_24; }
	inline float* get_address_of_startTime_24() { return &___startTime_24; }
	inline void set_startTime_24(float value)
	{
		___startTime_24 = value;
	}

	inline static int32_t get_offset_of_deltaTime_25() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___deltaTime_25)); }
	inline float get_deltaTime_25() const { return ___deltaTime_25; }
	inline float* get_address_of_deltaTime_25() { return &___deltaTime_25; }
	inline void set_deltaTime_25(float value)
	{
		___deltaTime_25 = value;
	}

	inline static int32_t get_offset_of_delayTime_26() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___delayTime_26)); }
	inline float get_delayTime_26() const { return ___delayTime_26; }
	inline float* get_address_of_delayTime_26() { return &___delayTime_26; }
	inline void set_delayTime_26(float value)
	{
		___delayTime_26 = value;
	}

	inline static int32_t get_offset_of_percentage_27() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___percentage_27)); }
	inline float get_percentage_27() const { return ___percentage_27; }
	inline float* get_address_of_percentage_27() { return &___percentage_27; }
	inline void set_percentage_27(float value)
	{
		___percentage_27 = value;
	}

	inline static int32_t get_offset_of_fromFloats_28() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___fromFloats_28)); }
	inline SingleU5BU5D_t1444911251* get_fromFloats_28() const { return ___fromFloats_28; }
	inline SingleU5BU5D_t1444911251** get_address_of_fromFloats_28() { return &___fromFloats_28; }
	inline void set_fromFloats_28(SingleU5BU5D_t1444911251* value)
	{
		___fromFloats_28 = value;
		Il2CppCodeGenWriteBarrier((&___fromFloats_28), value);
	}

	inline static int32_t get_offset_of_toFloats_29() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___toFloats_29)); }
	inline SingleU5BU5D_t1444911251* get_toFloats_29() const { return ___toFloats_29; }
	inline SingleU5BU5D_t1444911251** get_address_of_toFloats_29() { return &___toFloats_29; }
	inline void set_toFloats_29(SingleU5BU5D_t1444911251* value)
	{
		___toFloats_29 = value;
		Il2CppCodeGenWriteBarrier((&___toFloats_29), value);
	}

	inline static int32_t get_offset_of_resultFloats_30() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___resultFloats_30)); }
	inline SingleU5BU5D_t1444911251* get_resultFloats_30() const { return ___resultFloats_30; }
	inline SingleU5BU5D_t1444911251** get_address_of_resultFloats_30() { return &___resultFloats_30; }
	inline void set_resultFloats_30(SingleU5BU5D_t1444911251* value)
	{
		___resultFloats_30 = value;
		Il2CppCodeGenWriteBarrier((&___resultFloats_30), value);
	}

	inline static int32_t get_offset_of_finishAction_31() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___finishAction_31)); }
	inline bool get_finishAction_31() const { return ___finishAction_31; }
	inline bool* get_address_of_finishAction_31() { return &___finishAction_31; }
	inline void set_finishAction_31(bool value)
	{
		___finishAction_31 = value;
	}

	inline static int32_t get_offset_of_start_32() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___start_32)); }
	inline bool get_start_32() const { return ___start_32; }
	inline bool* get_address_of_start_32() { return &___start_32; }
	inline void set_start_32(bool value)
	{
		___start_32 = value;
	}

	inline static int32_t get_offset_of_finished_33() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___finished_33)); }
	inline bool get_finished_33() const { return ___finished_33; }
	inline bool* get_address_of_finished_33() { return &___finished_33; }
	inline void set_finished_33(bool value)
	{
		___finished_33 = value;
	}

	inline static int32_t get_offset_of_isRunning_34() { return static_cast<int32_t>(offsetof(EaseFsmAction_t2585076379, ___isRunning_34)); }
	inline bool get_isRunning_34() const { return ___isRunning_34; }
	inline bool* get_address_of_isRunning_34() { return &___isRunning_34; }
	inline void set_isRunning_34(bool value)
	{
		___isRunning_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFSMACTION_T2585076379_H
#ifndef ENABLEBEHAVIOUR_T124468037_H
#define ENABLEBEHAVIOUR_T124468037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableBehaviour
struct  EnableBehaviour_t124468037  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableBehaviour::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableBehaviour::behaviour
	FsmString_t1785915204 * ___behaviour_15;
	// UnityEngine.Component HutongGames.PlayMaker.Actions.EnableBehaviour::component
	Component_t1923634451 * ___component_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableBehaviour::enable
	FsmBool_t163807967 * ___enable_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableBehaviour::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_18;
	// UnityEngine.Behaviour HutongGames.PlayMaker.Actions.EnableBehaviour::componentTarget
	Behaviour_t1437897464 * ___componentTarget_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(EnableBehaviour_t124468037, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_behaviour_15() { return static_cast<int32_t>(offsetof(EnableBehaviour_t124468037, ___behaviour_15)); }
	inline FsmString_t1785915204 * get_behaviour_15() const { return ___behaviour_15; }
	inline FsmString_t1785915204 ** get_address_of_behaviour_15() { return &___behaviour_15; }
	inline void set_behaviour_15(FsmString_t1785915204 * value)
	{
		___behaviour_15 = value;
		Il2CppCodeGenWriteBarrier((&___behaviour_15), value);
	}

	inline static int32_t get_offset_of_component_16() { return static_cast<int32_t>(offsetof(EnableBehaviour_t124468037, ___component_16)); }
	inline Component_t1923634451 * get_component_16() const { return ___component_16; }
	inline Component_t1923634451 ** get_address_of_component_16() { return &___component_16; }
	inline void set_component_16(Component_t1923634451 * value)
	{
		___component_16 = value;
		Il2CppCodeGenWriteBarrier((&___component_16), value);
	}

	inline static int32_t get_offset_of_enable_17() { return static_cast<int32_t>(offsetof(EnableBehaviour_t124468037, ___enable_17)); }
	inline FsmBool_t163807967 * get_enable_17() const { return ___enable_17; }
	inline FsmBool_t163807967 ** get_address_of_enable_17() { return &___enable_17; }
	inline void set_enable_17(FsmBool_t163807967 * value)
	{
		___enable_17 = value;
		Il2CppCodeGenWriteBarrier((&___enable_17), value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(EnableBehaviour_t124468037, ___resetOnExit_18)); }
	inline FsmBool_t163807967 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t163807967 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_18), value);
	}

	inline static int32_t get_offset_of_componentTarget_19() { return static_cast<int32_t>(offsetof(EnableBehaviour_t124468037, ___componentTarget_19)); }
	inline Behaviour_t1437897464 * get_componentTarget_19() const { return ___componentTarget_19; }
	inline Behaviour_t1437897464 ** get_address_of_componentTarget_19() { return &___componentTarget_19; }
	inline void set_componentTarget_19(Behaviour_t1437897464 * value)
	{
		___componentTarget_19 = value;
		Il2CppCodeGenWriteBarrier((&___componentTarget_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEBEHAVIOUR_T124468037_H
#ifndef ENABLEFSM_T3113159649_H
#define ENABLEFSM_T3113159649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnableFSM
struct  EnableFSM_t3113159649  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.EnableFSM::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EnableFSM::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableFSM::enable
	FsmBool_t163807967 * ___enable_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnableFSM::resetOnExit
	FsmBool_t163807967 * ___resetOnExit_17;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.EnableFSM::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(EnableFSM_t3113159649, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(EnableFSM_t3113159649, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_enable_16() { return static_cast<int32_t>(offsetof(EnableFSM_t3113159649, ___enable_16)); }
	inline FsmBool_t163807967 * get_enable_16() const { return ___enable_16; }
	inline FsmBool_t163807967 ** get_address_of_enable_16() { return &___enable_16; }
	inline void set_enable_16(FsmBool_t163807967 * value)
	{
		___enable_16 = value;
		Il2CppCodeGenWriteBarrier((&___enable_16), value);
	}

	inline static int32_t get_offset_of_resetOnExit_17() { return static_cast<int32_t>(offsetof(EnableFSM_t3113159649, ___resetOnExit_17)); }
	inline FsmBool_t163807967 * get_resetOnExit_17() const { return ___resetOnExit_17; }
	inline FsmBool_t163807967 ** get_address_of_resetOnExit_17() { return &___resetOnExit_17; }
	inline void set_resetOnExit_17(FsmBool_t163807967 * value)
	{
		___resetOnExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___resetOnExit_17), value);
	}

	inline static int32_t get_offset_of_fsmComponent_18() { return static_cast<int32_t>(offsetof(EnableFSM_t3113159649, ___fsmComponent_18)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_18() const { return ___fsmComponent_18; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_18() { return &___fsmComponent_18; }
	inline void set_fsmComponent_18(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEFSM_T3113159649_H
#ifndef FINISHFSM_T747447064_H
#define FINISHFSM_T747447064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FinishFSM
struct  FinishFSM_t747447064  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINISHFSM_T747447064_H
#ifndef FORMATSTRING_T3925645673_H
#define FORMATSTRING_T3925645673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FormatString
struct  FormatString_t3925645673  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FormatString::format
	FsmString_t1785915204 * ___format_14;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.FormatString::variables
	FsmVarU5BU5D_t863727431* ___variables_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FormatString::storeResult
	FsmString_t1785915204 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.FormatString::everyFrame
	bool ___everyFrame_17;
	// System.Object[] HutongGames.PlayMaker.Actions.FormatString::objectArray
	ObjectU5BU5D_t2843939325* ___objectArray_18;

public:
	inline static int32_t get_offset_of_format_14() { return static_cast<int32_t>(offsetof(FormatString_t3925645673, ___format_14)); }
	inline FsmString_t1785915204 * get_format_14() const { return ___format_14; }
	inline FsmString_t1785915204 ** get_address_of_format_14() { return &___format_14; }
	inline void set_format_14(FsmString_t1785915204 * value)
	{
		___format_14 = value;
		Il2CppCodeGenWriteBarrier((&___format_14), value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(FormatString_t3925645673, ___variables_15)); }
	inline FsmVarU5BU5D_t863727431* get_variables_15() const { return ___variables_15; }
	inline FsmVarU5BU5D_t863727431** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmVarU5BU5D_t863727431* value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier((&___variables_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(FormatString_t3925645673, ___storeResult_16)); }
	inline FsmString_t1785915204 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmString_t1785915204 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(FormatString_t3925645673, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_objectArray_18() { return static_cast<int32_t>(offsetof(FormatString_t3925645673, ___objectArray_18)); }
	inline ObjectU5BU5D_t2843939325* get_objectArray_18() const { return ___objectArray_18; }
	inline ObjectU5BU5D_t2843939325** get_address_of_objectArray_18() { return &___objectArray_18; }
	inline void set_objectArray_18(ObjectU5BU5D_t2843939325* value)
	{
		___objectArray_18 = value;
		Il2CppCodeGenWriteBarrier((&___objectArray_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATSTRING_T3925645673_H
#ifndef FORWARDALLEVENTS_T2091258839_H
#define FORWARDALLEVENTS_T2091258839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ForwardAllEvents
struct  ForwardAllEvents_t2091258839  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.ForwardAllEvents::forwardTo
	FsmEventTarget_t919699796 * ___forwardTo_14;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.ForwardAllEvents::exceptThese
	FsmEventU5BU5D_t2511618479* ___exceptThese_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ForwardAllEvents::eatEvents
	bool ___eatEvents_16;

public:
	inline static int32_t get_offset_of_forwardTo_14() { return static_cast<int32_t>(offsetof(ForwardAllEvents_t2091258839, ___forwardTo_14)); }
	inline FsmEventTarget_t919699796 * get_forwardTo_14() const { return ___forwardTo_14; }
	inline FsmEventTarget_t919699796 ** get_address_of_forwardTo_14() { return &___forwardTo_14; }
	inline void set_forwardTo_14(FsmEventTarget_t919699796 * value)
	{
		___forwardTo_14 = value;
		Il2CppCodeGenWriteBarrier((&___forwardTo_14), value);
	}

	inline static int32_t get_offset_of_exceptThese_15() { return static_cast<int32_t>(offsetof(ForwardAllEvents_t2091258839, ___exceptThese_15)); }
	inline FsmEventU5BU5D_t2511618479* get_exceptThese_15() const { return ___exceptThese_15; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_exceptThese_15() { return &___exceptThese_15; }
	inline void set_exceptThese_15(FsmEventU5BU5D_t2511618479* value)
	{
		___exceptThese_15 = value;
		Il2CppCodeGenWriteBarrier((&___exceptThese_15), value);
	}

	inline static int32_t get_offset_of_eatEvents_16() { return static_cast<int32_t>(offsetof(ForwardAllEvents_t2091258839, ___eatEvents_16)); }
	inline bool get_eatEvents_16() const { return ___eatEvents_16; }
	inline bool* get_address_of_eatEvents_16() { return &___eatEvents_16; }
	inline void set_eatEvents_16(bool value)
	{
		___eatEvents_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDALLEVENTS_T2091258839_H
#ifndef FORWARDEVENT_T1540014318_H
#define FORWARDEVENT_T1540014318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ForwardEvent
struct  ForwardEvent_t1540014318  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.ForwardEvent::forwardTo
	FsmEventTarget_t919699796 * ___forwardTo_14;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.ForwardEvent::eventsToForward
	FsmEventU5BU5D_t2511618479* ___eventsToForward_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ForwardEvent::eatEvents
	bool ___eatEvents_16;

public:
	inline static int32_t get_offset_of_forwardTo_14() { return static_cast<int32_t>(offsetof(ForwardEvent_t1540014318, ___forwardTo_14)); }
	inline FsmEventTarget_t919699796 * get_forwardTo_14() const { return ___forwardTo_14; }
	inline FsmEventTarget_t919699796 ** get_address_of_forwardTo_14() { return &___forwardTo_14; }
	inline void set_forwardTo_14(FsmEventTarget_t919699796 * value)
	{
		___forwardTo_14 = value;
		Il2CppCodeGenWriteBarrier((&___forwardTo_14), value);
	}

	inline static int32_t get_offset_of_eventsToForward_15() { return static_cast<int32_t>(offsetof(ForwardEvent_t1540014318, ___eventsToForward_15)); }
	inline FsmEventU5BU5D_t2511618479* get_eventsToForward_15() const { return ___eventsToForward_15; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_eventsToForward_15() { return &___eventsToForward_15; }
	inline void set_eventsToForward_15(FsmEventU5BU5D_t2511618479* value)
	{
		___eventsToForward_15 = value;
		Il2CppCodeGenWriteBarrier((&___eventsToForward_15), value);
	}

	inline static int32_t get_offset_of_eatEvents_16() { return static_cast<int32_t>(offsetof(ForwardEvent_t1540014318, ___eatEvents_16)); }
	inline bool get_eatEvents_16() const { return ___eatEvents_16; }
	inline bool* get_address_of_eatEvents_16() { return &___eatEvents_16; }
	inline void set_eatEvents_16(bool value)
	{
		___eatEvents_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORWARDEVENT_T1540014318_H
#ifndef GETANGLETOTARGET_T2389188411_H
#define GETANGLETOTARGET_T2389188411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAngleToTarget
struct  GetAngleToTarget_t2389188411  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetAngleToTarget::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAngleToTarget::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAngleToTarget::targetPosition
	FsmVector3_t626444517 * ___targetPosition_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetAngleToTarget::ignoreHeight
	FsmBool_t163807967 * ___ignoreHeight_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAngleToTarget::storeAngle
	FsmFloat_t2883254149 * ___storeAngle_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAngleToTarget::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetAngleToTarget_t2389188411, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(GetAngleToTarget_t2389188411, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_targetPosition_16() { return static_cast<int32_t>(offsetof(GetAngleToTarget_t2389188411, ___targetPosition_16)); }
	inline FsmVector3_t626444517 * get_targetPosition_16() const { return ___targetPosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_16() { return &___targetPosition_16; }
	inline void set_targetPosition_16(FsmVector3_t626444517 * value)
	{
		___targetPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_16), value);
	}

	inline static int32_t get_offset_of_ignoreHeight_17() { return static_cast<int32_t>(offsetof(GetAngleToTarget_t2389188411, ___ignoreHeight_17)); }
	inline FsmBool_t163807967 * get_ignoreHeight_17() const { return ___ignoreHeight_17; }
	inline FsmBool_t163807967 ** get_address_of_ignoreHeight_17() { return &___ignoreHeight_17; }
	inline void set_ignoreHeight_17(FsmBool_t163807967 * value)
	{
		___ignoreHeight_17 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreHeight_17), value);
	}

	inline static int32_t get_offset_of_storeAngle_18() { return static_cast<int32_t>(offsetof(GetAngleToTarget_t2389188411, ___storeAngle_18)); }
	inline FsmFloat_t2883254149 * get_storeAngle_18() const { return ___storeAngle_18; }
	inline FsmFloat_t2883254149 ** get_address_of_storeAngle_18() { return &___storeAngle_18; }
	inline void set_storeAngle_18(FsmFloat_t2883254149 * value)
	{
		___storeAngle_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeAngle_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GetAngleToTarget_t2389188411, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETANGLETOTARGET_T2389188411_H
#ifndef GETEVENTBOOLDATA_T956908412_H
#define GETEVENTBOOLDATA_T956908412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventBoolData
struct  GetEventBoolData_t956908412  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetEventBoolData::getBoolData
	FsmBool_t163807967 * ___getBoolData_14;

public:
	inline static int32_t get_offset_of_getBoolData_14() { return static_cast<int32_t>(offsetof(GetEventBoolData_t956908412, ___getBoolData_14)); }
	inline FsmBool_t163807967 * get_getBoolData_14() const { return ___getBoolData_14; }
	inline FsmBool_t163807967 ** get_address_of_getBoolData_14() { return &___getBoolData_14; }
	inline void set_getBoolData_14(FsmBool_t163807967 * value)
	{
		___getBoolData_14 = value;
		Il2CppCodeGenWriteBarrier((&___getBoolData_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTBOOLDATA_T956908412_H
#ifndef GETEVENTFLOATDATA_T1922157318_H
#define GETEVENTFLOATDATA_T1922157318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventFloatData
struct  GetEventFloatData_t1922157318  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetEventFloatData::getFloatData
	FsmFloat_t2883254149 * ___getFloatData_14;

public:
	inline static int32_t get_offset_of_getFloatData_14() { return static_cast<int32_t>(offsetof(GetEventFloatData_t1922157318, ___getFloatData_14)); }
	inline FsmFloat_t2883254149 * get_getFloatData_14() const { return ___getFloatData_14; }
	inline FsmFloat_t2883254149 ** get_address_of_getFloatData_14() { return &___getFloatData_14; }
	inline void set_getFloatData_14(FsmFloat_t2883254149 * value)
	{
		___getFloatData_14 = value;
		Il2CppCodeGenWriteBarrier((&___getFloatData_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTFLOATDATA_T1922157318_H
#ifndef GETEVENTINFO_T4153126524_H
#define GETEVENTINFO_T4153126524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventInfo
struct  GetEventInfo_t4153126524  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetEventInfo::sentByGameObject
	FsmGameObject_t3581898942 * ___sentByGameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventInfo::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetEventInfo::getBoolData
	FsmBool_t163807967 * ___getBoolData_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetEventInfo::getIntData
	FsmInt_t874273141 * ___getIntData_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetEventInfo::getFloatData
	FsmFloat_t2883254149 * ___getFloatData_18;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetEventInfo::getVector2Data
	FsmVector2_t2965096677 * ___getVector2Data_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetEventInfo::getVector3Data
	FsmVector3_t626444517 * ___getVector3Data_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventInfo::getStringData
	FsmString_t1785915204 * ___getStringData_21;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetEventInfo::getGameObjectData
	FsmGameObject_t3581898942 * ___getGameObjectData_22;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GetEventInfo::getRectData
	FsmRect_t3649179877 * ___getRectData_23;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetEventInfo::getQuaternionData
	FsmQuaternion_t4259038327 * ___getQuaternionData_24;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.GetEventInfo::getMaterialData
	FsmMaterial_t2057874452 * ___getMaterialData_25;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GetEventInfo::getTextureData
	FsmTexture_t1738787045 * ___getTextureData_26;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.GetEventInfo::getColorData
	FsmColor_t1738900188 * ___getColorData_27;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetEventInfo::getObjectData
	FsmObject_t2606870197 * ___getObjectData_28;

public:
	inline static int32_t get_offset_of_sentByGameObject_14() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___sentByGameObject_14)); }
	inline FsmGameObject_t3581898942 * get_sentByGameObject_14() const { return ___sentByGameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_sentByGameObject_14() { return &___sentByGameObject_14; }
	inline void set_sentByGameObject_14(FsmGameObject_t3581898942 * value)
	{
		___sentByGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___sentByGameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_getBoolData_16() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getBoolData_16)); }
	inline FsmBool_t163807967 * get_getBoolData_16() const { return ___getBoolData_16; }
	inline FsmBool_t163807967 ** get_address_of_getBoolData_16() { return &___getBoolData_16; }
	inline void set_getBoolData_16(FsmBool_t163807967 * value)
	{
		___getBoolData_16 = value;
		Il2CppCodeGenWriteBarrier((&___getBoolData_16), value);
	}

	inline static int32_t get_offset_of_getIntData_17() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getIntData_17)); }
	inline FsmInt_t874273141 * get_getIntData_17() const { return ___getIntData_17; }
	inline FsmInt_t874273141 ** get_address_of_getIntData_17() { return &___getIntData_17; }
	inline void set_getIntData_17(FsmInt_t874273141 * value)
	{
		___getIntData_17 = value;
		Il2CppCodeGenWriteBarrier((&___getIntData_17), value);
	}

	inline static int32_t get_offset_of_getFloatData_18() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getFloatData_18)); }
	inline FsmFloat_t2883254149 * get_getFloatData_18() const { return ___getFloatData_18; }
	inline FsmFloat_t2883254149 ** get_address_of_getFloatData_18() { return &___getFloatData_18; }
	inline void set_getFloatData_18(FsmFloat_t2883254149 * value)
	{
		___getFloatData_18 = value;
		Il2CppCodeGenWriteBarrier((&___getFloatData_18), value);
	}

	inline static int32_t get_offset_of_getVector2Data_19() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getVector2Data_19)); }
	inline FsmVector2_t2965096677 * get_getVector2Data_19() const { return ___getVector2Data_19; }
	inline FsmVector2_t2965096677 ** get_address_of_getVector2Data_19() { return &___getVector2Data_19; }
	inline void set_getVector2Data_19(FsmVector2_t2965096677 * value)
	{
		___getVector2Data_19 = value;
		Il2CppCodeGenWriteBarrier((&___getVector2Data_19), value);
	}

	inline static int32_t get_offset_of_getVector3Data_20() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getVector3Data_20)); }
	inline FsmVector3_t626444517 * get_getVector3Data_20() const { return ___getVector3Data_20; }
	inline FsmVector3_t626444517 ** get_address_of_getVector3Data_20() { return &___getVector3Data_20; }
	inline void set_getVector3Data_20(FsmVector3_t626444517 * value)
	{
		___getVector3Data_20 = value;
		Il2CppCodeGenWriteBarrier((&___getVector3Data_20), value);
	}

	inline static int32_t get_offset_of_getStringData_21() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getStringData_21)); }
	inline FsmString_t1785915204 * get_getStringData_21() const { return ___getStringData_21; }
	inline FsmString_t1785915204 ** get_address_of_getStringData_21() { return &___getStringData_21; }
	inline void set_getStringData_21(FsmString_t1785915204 * value)
	{
		___getStringData_21 = value;
		Il2CppCodeGenWriteBarrier((&___getStringData_21), value);
	}

	inline static int32_t get_offset_of_getGameObjectData_22() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getGameObjectData_22)); }
	inline FsmGameObject_t3581898942 * get_getGameObjectData_22() const { return ___getGameObjectData_22; }
	inline FsmGameObject_t3581898942 ** get_address_of_getGameObjectData_22() { return &___getGameObjectData_22; }
	inline void set_getGameObjectData_22(FsmGameObject_t3581898942 * value)
	{
		___getGameObjectData_22 = value;
		Il2CppCodeGenWriteBarrier((&___getGameObjectData_22), value);
	}

	inline static int32_t get_offset_of_getRectData_23() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getRectData_23)); }
	inline FsmRect_t3649179877 * get_getRectData_23() const { return ___getRectData_23; }
	inline FsmRect_t3649179877 ** get_address_of_getRectData_23() { return &___getRectData_23; }
	inline void set_getRectData_23(FsmRect_t3649179877 * value)
	{
		___getRectData_23 = value;
		Il2CppCodeGenWriteBarrier((&___getRectData_23), value);
	}

	inline static int32_t get_offset_of_getQuaternionData_24() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getQuaternionData_24)); }
	inline FsmQuaternion_t4259038327 * get_getQuaternionData_24() const { return ___getQuaternionData_24; }
	inline FsmQuaternion_t4259038327 ** get_address_of_getQuaternionData_24() { return &___getQuaternionData_24; }
	inline void set_getQuaternionData_24(FsmQuaternion_t4259038327 * value)
	{
		___getQuaternionData_24 = value;
		Il2CppCodeGenWriteBarrier((&___getQuaternionData_24), value);
	}

	inline static int32_t get_offset_of_getMaterialData_25() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getMaterialData_25)); }
	inline FsmMaterial_t2057874452 * get_getMaterialData_25() const { return ___getMaterialData_25; }
	inline FsmMaterial_t2057874452 ** get_address_of_getMaterialData_25() { return &___getMaterialData_25; }
	inline void set_getMaterialData_25(FsmMaterial_t2057874452 * value)
	{
		___getMaterialData_25 = value;
		Il2CppCodeGenWriteBarrier((&___getMaterialData_25), value);
	}

	inline static int32_t get_offset_of_getTextureData_26() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getTextureData_26)); }
	inline FsmTexture_t1738787045 * get_getTextureData_26() const { return ___getTextureData_26; }
	inline FsmTexture_t1738787045 ** get_address_of_getTextureData_26() { return &___getTextureData_26; }
	inline void set_getTextureData_26(FsmTexture_t1738787045 * value)
	{
		___getTextureData_26 = value;
		Il2CppCodeGenWriteBarrier((&___getTextureData_26), value);
	}

	inline static int32_t get_offset_of_getColorData_27() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getColorData_27)); }
	inline FsmColor_t1738900188 * get_getColorData_27() const { return ___getColorData_27; }
	inline FsmColor_t1738900188 ** get_address_of_getColorData_27() { return &___getColorData_27; }
	inline void set_getColorData_27(FsmColor_t1738900188 * value)
	{
		___getColorData_27 = value;
		Il2CppCodeGenWriteBarrier((&___getColorData_27), value);
	}

	inline static int32_t get_offset_of_getObjectData_28() { return static_cast<int32_t>(offsetof(GetEventInfo_t4153126524, ___getObjectData_28)); }
	inline FsmObject_t2606870197 * get_getObjectData_28() const { return ___getObjectData_28; }
	inline FsmObject_t2606870197 ** get_address_of_getObjectData_28() { return &___getObjectData_28; }
	inline void set_getObjectData_28(FsmObject_t2606870197 * value)
	{
		___getObjectData_28 = value;
		Il2CppCodeGenWriteBarrier((&___getObjectData_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTINFO_T4153126524_H
#ifndef GETEVENTINTDATA_T4132126548_H
#define GETEVENTINTDATA_T4132126548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventIntData
struct  GetEventIntData_t4132126548  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetEventIntData::getIntData
	FsmInt_t874273141 * ___getIntData_14;

public:
	inline static int32_t get_offset_of_getIntData_14() { return static_cast<int32_t>(offsetof(GetEventIntData_t4132126548, ___getIntData_14)); }
	inline FsmInt_t874273141 * get_getIntData_14() const { return ___getIntData_14; }
	inline FsmInt_t874273141 ** get_address_of_getIntData_14() { return &___getIntData_14; }
	inline void set_getIntData_14(FsmInt_t874273141 * value)
	{
		___getIntData_14 = value;
		Il2CppCodeGenWriteBarrier((&___getIntData_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTINTDATA_T4132126548_H
#ifndef GETEVENTSENTBY_T154290135_H
#define GETEVENTSENTBY_T154290135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventSentBy
struct  GetEventSentBy_t154290135  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetEventSentBy::sentByGameObject
	FsmGameObject_t3581898942 * ___sentByGameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventSentBy::gameObjectName
	FsmString_t1785915204 * ___gameObjectName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventSentBy::fsmName
	FsmString_t1785915204 * ___fsmName_16;

public:
	inline static int32_t get_offset_of_sentByGameObject_14() { return static_cast<int32_t>(offsetof(GetEventSentBy_t154290135, ___sentByGameObject_14)); }
	inline FsmGameObject_t3581898942 * get_sentByGameObject_14() const { return ___sentByGameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_sentByGameObject_14() { return &___sentByGameObject_14; }
	inline void set_sentByGameObject_14(FsmGameObject_t3581898942 * value)
	{
		___sentByGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___sentByGameObject_14), value);
	}

	inline static int32_t get_offset_of_gameObjectName_15() { return static_cast<int32_t>(offsetof(GetEventSentBy_t154290135, ___gameObjectName_15)); }
	inline FsmString_t1785915204 * get_gameObjectName_15() const { return ___gameObjectName_15; }
	inline FsmString_t1785915204 ** get_address_of_gameObjectName_15() { return &___gameObjectName_15; }
	inline void set_gameObjectName_15(FsmString_t1785915204 * value)
	{
		___gameObjectName_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectName_15), value);
	}

	inline static int32_t get_offset_of_fsmName_16() { return static_cast<int32_t>(offsetof(GetEventSentBy_t154290135, ___fsmName_16)); }
	inline FsmString_t1785915204 * get_fsmName_16() const { return ___fsmName_16; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_16() { return &___fsmName_16; }
	inline void set_fsmName_16(FsmString_t1785915204 * value)
	{
		___fsmName_16 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTSENTBY_T154290135_H
#ifndef GETEVENTSTRINGDATA_T1465947128_H
#define GETEVENTSTRINGDATA_T1465947128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventStringData
struct  GetEventStringData_t1465947128  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetEventStringData::getStringData
	FsmString_t1785915204 * ___getStringData_14;

public:
	inline static int32_t get_offset_of_getStringData_14() { return static_cast<int32_t>(offsetof(GetEventStringData_t1465947128, ___getStringData_14)); }
	inline FsmString_t1785915204 * get_getStringData_14() const { return ___getStringData_14; }
	inline FsmString_t1785915204 ** get_address_of_getStringData_14() { return &___getStringData_14; }
	inline void set_getStringData_14(FsmString_t1785915204 * value)
	{
		___getStringData_14 = value;
		Il2CppCodeGenWriteBarrier((&___getStringData_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTSTRINGDATA_T1465947128_H
#ifndef GETEVENTVECTOR2DATA_T369842387_H
#define GETEVENTVECTOR2DATA_T369842387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventVector2Data
struct  GetEventVector2Data_t369842387  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetEventVector2Data::getVector2Data
	FsmVector2_t2965096677 * ___getVector2Data_14;

public:
	inline static int32_t get_offset_of_getVector2Data_14() { return static_cast<int32_t>(offsetof(GetEventVector2Data_t369842387, ___getVector2Data_14)); }
	inline FsmVector2_t2965096677 * get_getVector2Data_14() const { return ___getVector2Data_14; }
	inline FsmVector2_t2965096677 ** get_address_of_getVector2Data_14() { return &___getVector2Data_14; }
	inline void set_getVector2Data_14(FsmVector2_t2965096677 * value)
	{
		___getVector2Data_14 = value;
		Il2CppCodeGenWriteBarrier((&___getVector2Data_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTVECTOR2DATA_T369842387_H
#ifndef GETEVENTVECTOR3DATA_T369842354_H
#define GETEVENTVECTOR3DATA_T369842354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventVector3Data
struct  GetEventVector3Data_t369842354  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetEventVector3Data::getVector3Data
	FsmVector3_t626444517 * ___getVector3Data_14;

public:
	inline static int32_t get_offset_of_getVector3Data_14() { return static_cast<int32_t>(offsetof(GetEventVector3Data_t369842354, ___getVector3Data_14)); }
	inline FsmVector3_t626444517 * get_getVector3Data_14() const { return ___getVector3Data_14; }
	inline FsmVector3_t626444517 ** get_address_of_getVector3Data_14() { return &___getVector3Data_14; }
	inline void set_getVector3Data_14(FsmVector3_t626444517 * value)
	{
		___getVector3Data_14 = value;
		Il2CppCodeGenWriteBarrier((&___getVector3Data_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETEVENTVECTOR3DATA_T369842354_H
#ifndef GETFSMBOOL_T2453160037_H
#define GETFSMBOOL_T2453160037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmBool
struct  GetFsmBool_t2453160037  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmBool::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmBool::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmBool::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetFsmBool::storeValue
	FsmBool_t163807967 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmBool::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmBool::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmBool::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___storeValue_17)); }
	inline FsmBool_t163807967 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmBool_t163807967 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmBool_t163807967 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmBool_t2453160037, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMBOOL_T2453160037_H
#ifndef GETFSMCOLOR_T803516999_H
#define GETFSMCOLOR_T803516999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmColor
struct  GetFsmColor_t803516999  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmColor::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmColor::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.GetFsmColor::storeValue
	FsmColor_t1738900188 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmColor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmColor::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmColor::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___storeValue_17)); }
	inline FsmColor_t1738900188 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmColor_t1738900188 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmColor_t1738900188 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmColor_t803516999, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMCOLOR_T803516999_H
#ifndef GETFSMENUM_T921011402_H
#define GETFSMENUM_T921011402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmEnum
struct  GetFsmEnum_t921011402  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmEnum::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmEnum::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmEnum::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.GetFsmEnum::storeValue
	FsmEnum_t2861764163 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmEnum::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmEnum::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmEnum::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___storeValue_17)); }
	inline FsmEnum_t2861764163 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmEnum_t2861764163 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmEnum_t2861764163 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmEnum_t921011402, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMENUM_T921011402_H
#ifndef GETFSMFLOAT_T817976599_H
#define GETFSMFLOAT_T817976599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmFloat
struct  GetFsmFloat_t817976599  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmFloat::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmFloat::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmFloat::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetFsmFloat::storeValue
	FsmFloat_t2883254149 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmFloat::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmFloat::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmFloat::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___storeValue_17)); }
	inline FsmFloat_t2883254149 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmFloat_t2883254149 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmFloat_t817976599, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMFLOAT_T817976599_H
#ifndef GETFSMGAMEOBJECT_T2251453113_H
#define GETFSMGAMEOBJECT_T2251453113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmGameObject
struct  GetFsmGameObject_t2251453113  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmGameObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmGameObject::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmGameObject::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetFsmGameObject::storeValue
	FsmGameObject_t3581898942 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmGameObject::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmGameObject::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmGameObject::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___storeValue_17)); }
	inline FsmGameObject_t3581898942 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmGameObject_t3581898942 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmGameObject_t2251453113, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMGAMEOBJECT_T2251453113_H
#ifndef GETFSMINT_T3401447785_H
#define GETFSMINT_T3401447785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmInt
struct  GetFsmInt_t3401447785  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmInt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmInt::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmInt::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetFsmInt::storeValue
	FsmInt_t874273141 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmInt::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmInt::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmInt::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___storeValue_17)); }
	inline FsmInt_t874273141 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmInt_t874273141 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmInt_t874273141 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmInt_t3401447785, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMINT_T3401447785_H
#ifndef GETFSMMATERIAL_T2481100960_H
#define GETFSMMATERIAL_T2481100960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmMaterial
struct  GetFsmMaterial_t2481100960  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmMaterial::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmMaterial::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmMaterial::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.GetFsmMaterial::storeValue
	FsmMaterial_t2057874452 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmMaterial::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmMaterial::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmMaterial::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___storeValue_17)); }
	inline FsmMaterial_t2057874452 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmMaterial_t2057874452 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmMaterial_t2057874452 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmMaterial_t2481100960, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMMATERIAL_T2481100960_H
#ifndef GETFSMOBJECT_T783588600_H
#define GETFSMOBJECT_T783588600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmObject
struct  GetFsmObject_t783588600  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmObject::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmObject::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.GetFsmObject::storeValue
	FsmObject_t2606870197 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmObject::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmObject::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmObject::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___storeValue_17)); }
	inline FsmObject_t2606870197 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmObject_t2606870197 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmObject_t2606870197 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmObject_t783588600, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMOBJECT_T783588600_H
#ifndef GETFSMQUATERNION_T511116542_H
#define GETFSMQUATERNION_T511116542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmQuaternion
struct  GetFsmQuaternion_t511116542  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmQuaternion::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmQuaternion::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmQuaternion::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetFsmQuaternion::storeValue
	FsmQuaternion_t4259038327 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmQuaternion::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmQuaternion::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmQuaternion::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___storeValue_17)); }
	inline FsmQuaternion_t4259038327 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmQuaternion_t4259038327 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmQuaternion_t511116542, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMQUATERNION_T511116542_H
#ifndef GETFSMRECT_T2368714681_H
#define GETFSMRECT_T2368714681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmRect
struct  GetFsmRect_t2368714681  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmRect::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmRect::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmRect::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.GetFsmRect::storeValue
	FsmRect_t3649179877 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmRect::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmRect::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmRect::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___storeValue_17)); }
	inline FsmRect_t3649179877 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmRect_t3649179877 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmRect_t3649179877 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmRect_t2368714681, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMRECT_T2368714681_H
#ifndef GETFSMSTATE_T653440757_H
#define GETFSMSTATE_T653440757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmState
struct  GetFsmState_t653440757  : public FsmStateAction_t3801304101
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmState::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_14;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmState::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmState::fsmName
	FsmString_t1785915204 * ___fsmName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmState::storeResult
	FsmString_t1785915204 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmState::everyFrame
	bool ___everyFrame_18;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmState::fsm
	PlayMakerFSM_t1613010231 * ___fsm_19;

public:
	inline static int32_t get_offset_of_fsmComponent_14() { return static_cast<int32_t>(offsetof(GetFsmState_t653440757, ___fsmComponent_14)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_14() const { return ___fsmComponent_14; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_14() { return &___fsmComponent_14; }
	inline void set_fsmComponent_14(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_14 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_14), value);
	}

	inline static int32_t get_offset_of_gameObject_15() { return static_cast<int32_t>(offsetof(GetFsmState_t653440757, ___gameObject_15)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_15() const { return ___gameObject_15; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_15() { return &___gameObject_15; }
	inline void set_gameObject_15(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_15), value);
	}

	inline static int32_t get_offset_of_fsmName_16() { return static_cast<int32_t>(offsetof(GetFsmState_t653440757, ___fsmName_16)); }
	inline FsmString_t1785915204 * get_fsmName_16() const { return ___fsmName_16; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_16() { return &___fsmName_16; }
	inline void set_fsmName_16(FsmString_t1785915204 * value)
	{
		___fsmName_16 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetFsmState_t653440757, ___storeResult_17)); }
	inline FsmString_t1785915204 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmString_t1785915204 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmState_t653440757, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_fsm_19() { return static_cast<int32_t>(offsetof(GetFsmState_t653440757, ___fsm_19)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_19() const { return ___fsm_19; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_19() { return &___fsm_19; }
	inline void set_fsm_19(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_19 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMSTATE_T653440757_H
#ifndef GETFSMSTRING_T2701129905_H
#define GETFSMSTRING_T2701129905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmString
struct  GetFsmString_t2701129905  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmString::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmString::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmString::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmString::storeValue
	FsmString_t1785915204 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmString::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmString::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmString::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___storeValue_17)); }
	inline FsmString_t1785915204 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmString_t1785915204 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmString_t1785915204 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmString_t2701129905, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMSTRING_T2701129905_H
#ifndef GETFSMTEXTURE_T3574507122_H
#define GETFSMTEXTURE_T3574507122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmTexture
struct  GetFsmTexture_t3574507122  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmTexture::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmTexture::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmTexture::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GetFsmTexture::storeValue
	FsmTexture_t1738787045 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmTexture::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmTexture::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmTexture::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___storeValue_17)); }
	inline FsmTexture_t1738787045 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmTexture_t1738787045 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmTexture_t1738787045 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmTexture_t3574507122, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMTEXTURE_T3574507122_H
#ifndef GETFSMVARIABLE_T1296996537_H
#define GETFSMVARIABLE_T1296996537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmVariable
struct  GetFsmVariable_t1296996537  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmVariable::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVariable::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.GetFsmVariable::storeValue
	FsmVar_t2413660594 * ___storeValue_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmVariable::everyFrame
	bool ___everyFrame_17;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmVariable::cachedGO
	GameObject_t1113636619 * ___cachedGO_18;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmVariable::sourceFsm
	PlayMakerFSM_t1613010231 * ___sourceFsm_19;
	// HutongGames.PlayMaker.INamedVariable HutongGames.PlayMaker.Actions.GetFsmVariable::sourceVariable
	RuntimeObject* ___sourceVariable_20;
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.Actions.GetFsmVariable::targetVariable
	NamedVariable_t2808389578 * ___targetVariable_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_storeValue_16() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___storeValue_16)); }
	inline FsmVar_t2413660594 * get_storeValue_16() const { return ___storeValue_16; }
	inline FsmVar_t2413660594 ** get_address_of_storeValue_16() { return &___storeValue_16; }
	inline void set_storeValue_16(FsmVar_t2413660594 * value)
	{
		___storeValue_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_cachedGO_18() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___cachedGO_18)); }
	inline GameObject_t1113636619 * get_cachedGO_18() const { return ___cachedGO_18; }
	inline GameObject_t1113636619 ** get_address_of_cachedGO_18() { return &___cachedGO_18; }
	inline void set_cachedGO_18(GameObject_t1113636619 * value)
	{
		___cachedGO_18 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGO_18), value);
	}

	inline static int32_t get_offset_of_sourceFsm_19() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___sourceFsm_19)); }
	inline PlayMakerFSM_t1613010231 * get_sourceFsm_19() const { return ___sourceFsm_19; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_sourceFsm_19() { return &___sourceFsm_19; }
	inline void set_sourceFsm_19(PlayMakerFSM_t1613010231 * value)
	{
		___sourceFsm_19 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFsm_19), value);
	}

	inline static int32_t get_offset_of_sourceVariable_20() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___sourceVariable_20)); }
	inline RuntimeObject* get_sourceVariable_20() const { return ___sourceVariable_20; }
	inline RuntimeObject** get_address_of_sourceVariable_20() { return &___sourceVariable_20; }
	inline void set_sourceVariable_20(RuntimeObject* value)
	{
		___sourceVariable_20 = value;
		Il2CppCodeGenWriteBarrier((&___sourceVariable_20), value);
	}

	inline static int32_t get_offset_of_targetVariable_21() { return static_cast<int32_t>(offsetof(GetFsmVariable_t1296996537, ___targetVariable_21)); }
	inline NamedVariable_t2808389578 * get_targetVariable_21() const { return ___targetVariable_21; }
	inline NamedVariable_t2808389578 ** get_address_of_targetVariable_21() { return &___targetVariable_21; }
	inline void set_targetVariable_21(NamedVariable_t2808389578 * value)
	{
		___targetVariable_21 = value;
		Il2CppCodeGenWriteBarrier((&___targetVariable_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMVARIABLE_T1296996537_H
#ifndef GETFSMVARIABLES_T1675002777_H
#define GETFSMVARIABLES_T1675002777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmVariables
struct  GetFsmVariables_t1675002777  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmVariables::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVariables::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.GetFsmVariables::getVariables
	FsmVarU5BU5D_t863727431* ___getVariables_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmVariables::everyFrame
	bool ___everyFrame_17;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmVariables::cachedGO
	GameObject_t1113636619 * ___cachedGO_18;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmVariables::sourceFsm
	PlayMakerFSM_t1613010231 * ___sourceFsm_19;
	// HutongGames.PlayMaker.INamedVariable[] HutongGames.PlayMaker.Actions.GetFsmVariables::sourceVariables
	INamedVariableU5BU5D_t3347979629* ___sourceVariables_20;
	// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.Actions.GetFsmVariables::targetVariables
	NamedVariableU5BU5D_t58099151* ___targetVariables_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_getVariables_16() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___getVariables_16)); }
	inline FsmVarU5BU5D_t863727431* get_getVariables_16() const { return ___getVariables_16; }
	inline FsmVarU5BU5D_t863727431** get_address_of_getVariables_16() { return &___getVariables_16; }
	inline void set_getVariables_16(FsmVarU5BU5D_t863727431* value)
	{
		___getVariables_16 = value;
		Il2CppCodeGenWriteBarrier((&___getVariables_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_cachedGO_18() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___cachedGO_18)); }
	inline GameObject_t1113636619 * get_cachedGO_18() const { return ___cachedGO_18; }
	inline GameObject_t1113636619 ** get_address_of_cachedGO_18() { return &___cachedGO_18; }
	inline void set_cachedGO_18(GameObject_t1113636619 * value)
	{
		___cachedGO_18 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGO_18), value);
	}

	inline static int32_t get_offset_of_sourceFsm_19() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___sourceFsm_19)); }
	inline PlayMakerFSM_t1613010231 * get_sourceFsm_19() const { return ___sourceFsm_19; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_sourceFsm_19() { return &___sourceFsm_19; }
	inline void set_sourceFsm_19(PlayMakerFSM_t1613010231 * value)
	{
		___sourceFsm_19 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFsm_19), value);
	}

	inline static int32_t get_offset_of_sourceVariables_20() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___sourceVariables_20)); }
	inline INamedVariableU5BU5D_t3347979629* get_sourceVariables_20() const { return ___sourceVariables_20; }
	inline INamedVariableU5BU5D_t3347979629** get_address_of_sourceVariables_20() { return &___sourceVariables_20; }
	inline void set_sourceVariables_20(INamedVariableU5BU5D_t3347979629* value)
	{
		___sourceVariables_20 = value;
		Il2CppCodeGenWriteBarrier((&___sourceVariables_20), value);
	}

	inline static int32_t get_offset_of_targetVariables_21() { return static_cast<int32_t>(offsetof(GetFsmVariables_t1675002777, ___targetVariables_21)); }
	inline NamedVariableU5BU5D_t58099151* get_targetVariables_21() const { return ___targetVariables_21; }
	inline NamedVariableU5BU5D_t58099151** get_address_of_targetVariables_21() { return &___targetVariables_21; }
	inline void set_targetVariables_21(NamedVariableU5BU5D_t58099151* value)
	{
		___targetVariables_21 = value;
		Il2CppCodeGenWriteBarrier((&___targetVariables_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMVARIABLES_T1675002777_H
#ifndef GETFSMVECTOR2_T3769113750_H
#define GETFSMVECTOR2_T3769113750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmVector2
struct  GetFsmVector2_t3769113750  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmVector2::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVector2::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVector2::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetFsmVector2::storeValue
	FsmVector2_t2965096677 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmVector2::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmVector2::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmVector2::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___storeValue_17)); }
	inline FsmVector2_t2965096677 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmVector2_t2965096677 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmVector2_t2965096677 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmVector2_t3769113750, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMVECTOR2_T3769113750_H
#ifndef GETFSMVECTOR3_T2203029809_H
#define GETFSMVECTOR3_T2203029809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetFsmVector3
struct  GetFsmVector3_t2203029809  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetFsmVector3::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVector3::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetFsmVector3::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetFsmVector3::storeValue
	FsmVector3_t626444517 * ___storeValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetFsmVector3::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetFsmVector3::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.GetFsmVector3::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_storeValue_17() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___storeValue_17)); }
	inline FsmVector3_t626444517 * get_storeValue_17() const { return ___storeValue_17; }
	inline FsmVector3_t626444517 ** get_address_of_storeValue_17() { return &___storeValue_17; }
	inline void set_storeValue_17(FsmVector3_t626444517 * value)
	{
		___storeValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(GetFsmVector3_t2203029809, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFSMVECTOR3_T2203029809_H
#ifndef GETLASTEVENT_T2408635592_H
#define GETLASTEVENT_T2408635592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetLastEvent
struct  GetLastEvent_t2408635592  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetLastEvent::storeEvent
	FsmString_t1785915204 * ___storeEvent_14;

public:
	inline static int32_t get_offset_of_storeEvent_14() { return static_cast<int32_t>(offsetof(GetLastEvent_t2408635592, ___storeEvent_14)); }
	inline FsmString_t1785915204 * get_storeEvent_14() const { return ___storeEvent_14; }
	inline FsmString_t1785915204 ** get_address_of_storeEvent_14() { return &___storeEvent_14; }
	inline void set_storeEvent_14(FsmString_t1785915204 * value)
	{
		___storeEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeEvent_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLASTEVENT_T2408635592_H
#ifndef GETPOSITION_T3877072205_H
#define GETPOSITION_T3877072205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetPosition
struct  GetPosition_t3877072205  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetPosition::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetPosition::vector
	FsmVector3_t626444517 * ___vector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetPosition::x
	FsmFloat_t2883254149 * ___x_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetPosition::y
	FsmFloat_t2883254149 * ___y_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetPosition::z
	FsmFloat_t2883254149 * ___z_18;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetPosition::space
	int32_t ___space_19;
	// System.Boolean HutongGames.PlayMaker.Actions.GetPosition::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector_15() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___vector_15)); }
	inline FsmVector3_t626444517 * get_vector_15() const { return ___vector_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector_15() { return &___vector_15; }
	inline void set_vector_15(FsmVector3_t626444517 * value)
	{
		___vector_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector_15), value);
	}

	inline static int32_t get_offset_of_x_16() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___x_16)); }
	inline FsmFloat_t2883254149 * get_x_16() const { return ___x_16; }
	inline FsmFloat_t2883254149 ** get_address_of_x_16() { return &___x_16; }
	inline void set_x_16(FsmFloat_t2883254149 * value)
	{
		___x_16 = value;
		Il2CppCodeGenWriteBarrier((&___x_16), value);
	}

	inline static int32_t get_offset_of_y_17() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___y_17)); }
	inline FsmFloat_t2883254149 * get_y_17() const { return ___y_17; }
	inline FsmFloat_t2883254149 ** get_address_of_y_17() { return &___y_17; }
	inline void set_y_17(FsmFloat_t2883254149 * value)
	{
		___y_17 = value;
		Il2CppCodeGenWriteBarrier((&___y_17), value);
	}

	inline static int32_t get_offset_of_z_18() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___z_18)); }
	inline FsmFloat_t2883254149 * get_z_18() const { return ___z_18; }
	inline FsmFloat_t2883254149 ** get_address_of_z_18() { return &___z_18; }
	inline void set_z_18(FsmFloat_t2883254149 * value)
	{
		___z_18 = value;
		Il2CppCodeGenWriteBarrier((&___z_18), value);
	}

	inline static int32_t get_offset_of_space_19() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___space_19)); }
	inline int32_t get_space_19() const { return ___space_19; }
	inline int32_t* get_address_of_space_19() { return &___space_19; }
	inline void set_space_19(int32_t value)
	{
		___space_19 = value;
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(GetPosition_t3877072205, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPOSITION_T3877072205_H
#ifndef GETPREVIOUSSTATENAME_T1603361608_H
#define GETPREVIOUSSTATENAME_T1603361608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetPreviousStateName
struct  GetPreviousStateName_t1603361608  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetPreviousStateName::storeName
	FsmString_t1785915204 * ___storeName_14;

public:
	inline static int32_t get_offset_of_storeName_14() { return static_cast<int32_t>(offsetof(GetPreviousStateName_t1603361608, ___storeName_14)); }
	inline FsmString_t1785915204 * get_storeName_14() const { return ___storeName_14; }
	inline FsmString_t1785915204 ** get_address_of_storeName_14() { return &___storeName_14; }
	inline void set_storeName_14(FsmString_t1785915204 * value)
	{
		___storeName_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeName_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPREVIOUSSTATENAME_T1603361608_H
#ifndef GETROTATION_T4042812502_H
#define GETROTATION_T4042812502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetRotation
struct  GetRotation_t4042812502  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetRotation::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.GetRotation::quaternion
	FsmQuaternion_t4259038327 * ___quaternion_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetRotation::vector
	FsmVector3_t626444517 * ___vector_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRotation::xAngle
	FsmFloat_t2883254149 * ___xAngle_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRotation::yAngle
	FsmFloat_t2883254149 * ___yAngle_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetRotation::zAngle
	FsmFloat_t2883254149 * ___zAngle_19;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetRotation::space
	int32_t ___space_20;
	// System.Boolean HutongGames.PlayMaker.Actions.GetRotation::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_quaternion_15() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___quaternion_15)); }
	inline FsmQuaternion_t4259038327 * get_quaternion_15() const { return ___quaternion_15; }
	inline FsmQuaternion_t4259038327 ** get_address_of_quaternion_15() { return &___quaternion_15; }
	inline void set_quaternion_15(FsmQuaternion_t4259038327 * value)
	{
		___quaternion_15 = value;
		Il2CppCodeGenWriteBarrier((&___quaternion_15), value);
	}

	inline static int32_t get_offset_of_vector_16() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___vector_16)); }
	inline FsmVector3_t626444517 * get_vector_16() const { return ___vector_16; }
	inline FsmVector3_t626444517 ** get_address_of_vector_16() { return &___vector_16; }
	inline void set_vector_16(FsmVector3_t626444517 * value)
	{
		___vector_16 = value;
		Il2CppCodeGenWriteBarrier((&___vector_16), value);
	}

	inline static int32_t get_offset_of_xAngle_17() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___xAngle_17)); }
	inline FsmFloat_t2883254149 * get_xAngle_17() const { return ___xAngle_17; }
	inline FsmFloat_t2883254149 ** get_address_of_xAngle_17() { return &___xAngle_17; }
	inline void set_xAngle_17(FsmFloat_t2883254149 * value)
	{
		___xAngle_17 = value;
		Il2CppCodeGenWriteBarrier((&___xAngle_17), value);
	}

	inline static int32_t get_offset_of_yAngle_18() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___yAngle_18)); }
	inline FsmFloat_t2883254149 * get_yAngle_18() const { return ___yAngle_18; }
	inline FsmFloat_t2883254149 ** get_address_of_yAngle_18() { return &___yAngle_18; }
	inline void set_yAngle_18(FsmFloat_t2883254149 * value)
	{
		___yAngle_18 = value;
		Il2CppCodeGenWriteBarrier((&___yAngle_18), value);
	}

	inline static int32_t get_offset_of_zAngle_19() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___zAngle_19)); }
	inline FsmFloat_t2883254149 * get_zAngle_19() const { return ___zAngle_19; }
	inline FsmFloat_t2883254149 ** get_address_of_zAngle_19() { return &___zAngle_19; }
	inline void set_zAngle_19(FsmFloat_t2883254149 * value)
	{
		___zAngle_19 = value;
		Il2CppCodeGenWriteBarrier((&___zAngle_19), value);
	}

	inline static int32_t get_offset_of_space_20() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___space_20)); }
	inline int32_t get_space_20() const { return ___space_20; }
	inline int32_t* get_address_of_space_20() { return &___space_20; }
	inline void set_space_20(int32_t value)
	{
		___space_20 = value;
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(GetRotation_t4042812502, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETROTATION_T4042812502_H
#ifndef GETSCALE_T75781051_H
#define GETSCALE_T75781051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetScale
struct  GetScale_t75781051  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetScale::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetScale::vector
	FsmVector3_t626444517 * ___vector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScale::xScale
	FsmFloat_t2883254149 * ___xScale_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScale::yScale
	FsmFloat_t2883254149 * ___yScale_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetScale::zScale
	FsmFloat_t2883254149 * ___zScale_18;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.GetScale::space
	int32_t ___space_19;
	// System.Boolean HutongGames.PlayMaker.Actions.GetScale::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector_15() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___vector_15)); }
	inline FsmVector3_t626444517 * get_vector_15() const { return ___vector_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector_15() { return &___vector_15; }
	inline void set_vector_15(FsmVector3_t626444517 * value)
	{
		___vector_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector_15), value);
	}

	inline static int32_t get_offset_of_xScale_16() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___xScale_16)); }
	inline FsmFloat_t2883254149 * get_xScale_16() const { return ___xScale_16; }
	inline FsmFloat_t2883254149 ** get_address_of_xScale_16() { return &___xScale_16; }
	inline void set_xScale_16(FsmFloat_t2883254149 * value)
	{
		___xScale_16 = value;
		Il2CppCodeGenWriteBarrier((&___xScale_16), value);
	}

	inline static int32_t get_offset_of_yScale_17() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___yScale_17)); }
	inline FsmFloat_t2883254149 * get_yScale_17() const { return ___yScale_17; }
	inline FsmFloat_t2883254149 ** get_address_of_yScale_17() { return &___yScale_17; }
	inline void set_yScale_17(FsmFloat_t2883254149 * value)
	{
		___yScale_17 = value;
		Il2CppCodeGenWriteBarrier((&___yScale_17), value);
	}

	inline static int32_t get_offset_of_zScale_18() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___zScale_18)); }
	inline FsmFloat_t2883254149 * get_zScale_18() const { return ___zScale_18; }
	inline FsmFloat_t2883254149 ** get_address_of_zScale_18() { return &___zScale_18; }
	inline void set_zScale_18(FsmFloat_t2883254149 * value)
	{
		___zScale_18 = value;
		Il2CppCodeGenWriteBarrier((&___zScale_18), value);
	}

	inline static int32_t get_offset_of_space_19() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___space_19)); }
	inline int32_t get_space_19() const { return ___space_19; }
	inline int32_t* get_address_of_space_19() { return &___space_19; }
	inline void set_space_19(int32_t value)
	{
		___space_19 = value;
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(GetScale_t75781051, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSCALE_T75781051_H
#ifndef GETSTRINGLEFT_T3395224379_H
#define GETSTRINGLEFT_T3395224379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetStringLeft
struct  GetStringLeft_t3395224379  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringLeft::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetStringLeft::charCount
	FsmInt_t874273141 * ___charCount_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringLeft::storeResult
	FsmString_t1785915204 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetStringLeft::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(GetStringLeft_t3395224379, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_charCount_15() { return static_cast<int32_t>(offsetof(GetStringLeft_t3395224379, ___charCount_15)); }
	inline FsmInt_t874273141 * get_charCount_15() const { return ___charCount_15; }
	inline FsmInt_t874273141 ** get_address_of_charCount_15() { return &___charCount_15; }
	inline void set_charCount_15(FsmInt_t874273141 * value)
	{
		___charCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___charCount_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetStringLeft_t3395224379, ___storeResult_16)); }
	inline FsmString_t1785915204 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmString_t1785915204 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetStringLeft_t3395224379, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSTRINGLEFT_T3395224379_H
#ifndef GETSTRINGLENGTH_T1039889994_H
#define GETSTRINGLENGTH_T1039889994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetStringLength
struct  GetStringLength_t1039889994  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringLength::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetStringLength::storeResult
	FsmInt_t874273141 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetStringLength::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(GetStringLength_t1039889994, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetStringLength_t1039889994, ___storeResult_15)); }
	inline FsmInt_t874273141 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmInt_t874273141 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmInt_t874273141 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetStringLength_t1039889994, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSTRINGLENGTH_T1039889994_H
#ifndef GETSTRINGRIGHT_T3697075995_H
#define GETSTRINGRIGHT_T3697075995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetStringRight
struct  GetStringRight_t3697075995  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringRight::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetStringRight::charCount
	FsmInt_t874273141 * ___charCount_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetStringRight::storeResult
	FsmString_t1785915204 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetStringRight::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(GetStringRight_t3697075995, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_charCount_15() { return static_cast<int32_t>(offsetof(GetStringRight_t3697075995, ___charCount_15)); }
	inline FsmInt_t874273141 * get_charCount_15() const { return ___charCount_15; }
	inline FsmInt_t874273141 ** get_address_of_charCount_15() { return &___charCount_15; }
	inline void set_charCount_15(FsmInt_t874273141 * value)
	{
		___charCount_15 = value;
		Il2CppCodeGenWriteBarrier((&___charCount_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetStringRight_t3697075995, ___storeResult_16)); }
	inline FsmString_t1785915204 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmString_t1785915204 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetStringRight_t3697075995, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSTRINGRIGHT_T3697075995_H
#ifndef GETSUBSTRING_T621261119_H
#define GETSUBSTRING_T621261119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSubstring
struct  GetSubstring_t621261119  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSubstring::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSubstring::startIndex
	FsmInt_t874273141 * ___startIndex_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSubstring::length
	FsmInt_t874273141 * ___length_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSubstring::storeResult
	FsmString_t1785915204 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSubstring::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(GetSubstring_t621261119, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_startIndex_15() { return static_cast<int32_t>(offsetof(GetSubstring_t621261119, ___startIndex_15)); }
	inline FsmInt_t874273141 * get_startIndex_15() const { return ___startIndex_15; }
	inline FsmInt_t874273141 ** get_address_of_startIndex_15() { return &___startIndex_15; }
	inline void set_startIndex_15(FsmInt_t874273141 * value)
	{
		___startIndex_15 = value;
		Il2CppCodeGenWriteBarrier((&___startIndex_15), value);
	}

	inline static int32_t get_offset_of_length_16() { return static_cast<int32_t>(offsetof(GetSubstring_t621261119, ___length_16)); }
	inline FsmInt_t874273141 * get_length_16() const { return ___length_16; }
	inline FsmInt_t874273141 ** get_address_of_length_16() { return &___length_16; }
	inline void set_length_16(FsmInt_t874273141 * value)
	{
		___length_16 = value;
		Il2CppCodeGenWriteBarrier((&___length_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GetSubstring_t621261119, ___storeResult_17)); }
	inline FsmString_t1785915204 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmString_t1785915204 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GetSubstring_t621261119, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSUBSTRING_T621261119_H
#ifndef GETSYSTEMDATETIME_T2558928986_H
#define GETSYSTEMDATETIME_T2558928986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSystemDateTime
struct  GetSystemDateTime_t2558928986  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSystemDateTime::storeString
	FsmString_t1785915204 * ___storeString_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSystemDateTime::format
	FsmString_t1785915204 * ___format_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetSystemDateTime::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_storeString_14() { return static_cast<int32_t>(offsetof(GetSystemDateTime_t2558928986, ___storeString_14)); }
	inline FsmString_t1785915204 * get_storeString_14() const { return ___storeString_14; }
	inline FsmString_t1785915204 ** get_address_of_storeString_14() { return &___storeString_14; }
	inline void set_storeString_14(FsmString_t1785915204 * value)
	{
		___storeString_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeString_14), value);
	}

	inline static int32_t get_offset_of_format_15() { return static_cast<int32_t>(offsetof(GetSystemDateTime_t2558928986, ___format_15)); }
	inline FsmString_t1785915204 * get_format_15() const { return ___format_15; }
	inline FsmString_t1785915204 ** get_address_of_format_15() { return &___format_15; }
	inline void set_format_15(FsmString_t1785915204 * value)
	{
		___format_15 = value;
		Il2CppCodeGenWriteBarrier((&___format_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetSystemDateTime_t2558928986, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSYSTEMDATETIME_T2558928986_H
#ifndef GETTIMEINFO_T3431284632_H
#define GETTIMEINFO_T3431284632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetTimeInfo
struct  GetTimeInfo_t3431284632  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Actions.GetTimeInfo/TimeInfo HutongGames.PlayMaker.Actions.GetTimeInfo::getInfo
	int32_t ___getInfo_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetTimeInfo::storeValue
	FsmFloat_t2883254149 * ___storeValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetTimeInfo::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_getInfo_14() { return static_cast<int32_t>(offsetof(GetTimeInfo_t3431284632, ___getInfo_14)); }
	inline int32_t get_getInfo_14() const { return ___getInfo_14; }
	inline int32_t* get_address_of_getInfo_14() { return &___getInfo_14; }
	inline void set_getInfo_14(int32_t value)
	{
		___getInfo_14 = value;
	}

	inline static int32_t get_offset_of_storeValue_15() { return static_cast<int32_t>(offsetof(GetTimeInfo_t3431284632, ___storeValue_15)); }
	inline FsmFloat_t2883254149 * get_storeValue_15() const { return ___storeValue_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeValue_15() { return &___storeValue_15; }
	inline void set_storeValue_15(FsmFloat_t2883254149 * value)
	{
		___storeValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetTimeInfo_t3431284632, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTIMEINFO_T3431284632_H
#ifndef GOTOPREVIOUSSTATE_T3673902729_H
#define GOTOPREVIOUSSTATE_T3673902729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GotoPreviousState
struct  GotoPreviousState_t3673902729  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOTOPREVIOUSSTATE_T3673902729_H
#ifndef INVERSETRANSFORMDIRECTION_T2869898513_H
#define INVERSETRANSFORMDIRECTION_T2869898513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.InverseTransformDirection
struct  InverseTransformDirection_t2869898513  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.InverseTransformDirection::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.InverseTransformDirection::worldDirection
	FsmVector3_t626444517 * ___worldDirection_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.InverseTransformDirection::storeResult
	FsmVector3_t626444517 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.InverseTransformDirection::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(InverseTransformDirection_t2869898513, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_worldDirection_15() { return static_cast<int32_t>(offsetof(InverseTransformDirection_t2869898513, ___worldDirection_15)); }
	inline FsmVector3_t626444517 * get_worldDirection_15() const { return ___worldDirection_15; }
	inline FsmVector3_t626444517 ** get_address_of_worldDirection_15() { return &___worldDirection_15; }
	inline void set_worldDirection_15(FsmVector3_t626444517 * value)
	{
		___worldDirection_15 = value;
		Il2CppCodeGenWriteBarrier((&___worldDirection_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(InverseTransformDirection_t2869898513, ___storeResult_16)); }
	inline FsmVector3_t626444517 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmVector3_t626444517 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmVector3_t626444517 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(InverseTransformDirection_t2869898513, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVERSETRANSFORMDIRECTION_T2869898513_H
#ifndef INVERSETRANSFORMPOINT_T2243770383_H
#define INVERSETRANSFORMPOINT_T2243770383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.InverseTransformPoint
struct  InverseTransformPoint_t2243770383  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.InverseTransformPoint::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.InverseTransformPoint::worldPosition
	FsmVector3_t626444517 * ___worldPosition_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.InverseTransformPoint::storeResult
	FsmVector3_t626444517 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.InverseTransformPoint::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(InverseTransformPoint_t2243770383, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_worldPosition_15() { return static_cast<int32_t>(offsetof(InverseTransformPoint_t2243770383, ___worldPosition_15)); }
	inline FsmVector3_t626444517 * get_worldPosition_15() const { return ___worldPosition_15; }
	inline FsmVector3_t626444517 ** get_address_of_worldPosition_15() { return &___worldPosition_15; }
	inline void set_worldPosition_15(FsmVector3_t626444517 * value)
	{
		___worldPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___worldPosition_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(InverseTransformPoint_t2243770383, ___storeResult_16)); }
	inline FsmVector3_t626444517 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmVector3_t626444517 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmVector3_t626444517 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(InverseTransformPoint_t2243770383, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVERSETRANSFORMPOINT_T2243770383_H
#ifndef INVOKEMETHOD_T124369765_H
#define INVOKEMETHOD_T124369765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.InvokeMethod
struct  InvokeMethod_t124369765  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.InvokeMethod::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.InvokeMethod::behaviour
	FsmString_t1785915204 * ___behaviour_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.InvokeMethod::methodName
	FsmString_t1785915204 * ___methodName_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.InvokeMethod::delay
	FsmFloat_t2883254149 * ___delay_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.InvokeMethod::repeating
	FsmBool_t163807967 * ___repeating_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.InvokeMethod::repeatDelay
	FsmFloat_t2883254149 * ___repeatDelay_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.InvokeMethod::cancelOnExit
	FsmBool_t163807967 * ___cancelOnExit_20;
	// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Actions.InvokeMethod::component
	MonoBehaviour_t3962482529 * ___component_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_behaviour_15() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___behaviour_15)); }
	inline FsmString_t1785915204 * get_behaviour_15() const { return ___behaviour_15; }
	inline FsmString_t1785915204 ** get_address_of_behaviour_15() { return &___behaviour_15; }
	inline void set_behaviour_15(FsmString_t1785915204 * value)
	{
		___behaviour_15 = value;
		Il2CppCodeGenWriteBarrier((&___behaviour_15), value);
	}

	inline static int32_t get_offset_of_methodName_16() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___methodName_16)); }
	inline FsmString_t1785915204 * get_methodName_16() const { return ___methodName_16; }
	inline FsmString_t1785915204 ** get_address_of_methodName_16() { return &___methodName_16; }
	inline void set_methodName_16(FsmString_t1785915204 * value)
	{
		___methodName_16 = value;
		Il2CppCodeGenWriteBarrier((&___methodName_16), value);
	}

	inline static int32_t get_offset_of_delay_17() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___delay_17)); }
	inline FsmFloat_t2883254149 * get_delay_17() const { return ___delay_17; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_17() { return &___delay_17; }
	inline void set_delay_17(FsmFloat_t2883254149 * value)
	{
		___delay_17 = value;
		Il2CppCodeGenWriteBarrier((&___delay_17), value);
	}

	inline static int32_t get_offset_of_repeating_18() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___repeating_18)); }
	inline FsmBool_t163807967 * get_repeating_18() const { return ___repeating_18; }
	inline FsmBool_t163807967 ** get_address_of_repeating_18() { return &___repeating_18; }
	inline void set_repeating_18(FsmBool_t163807967 * value)
	{
		___repeating_18 = value;
		Il2CppCodeGenWriteBarrier((&___repeating_18), value);
	}

	inline static int32_t get_offset_of_repeatDelay_19() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___repeatDelay_19)); }
	inline FsmFloat_t2883254149 * get_repeatDelay_19() const { return ___repeatDelay_19; }
	inline FsmFloat_t2883254149 ** get_address_of_repeatDelay_19() { return &___repeatDelay_19; }
	inline void set_repeatDelay_19(FsmFloat_t2883254149 * value)
	{
		___repeatDelay_19 = value;
		Il2CppCodeGenWriteBarrier((&___repeatDelay_19), value);
	}

	inline static int32_t get_offset_of_cancelOnExit_20() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___cancelOnExit_20)); }
	inline FsmBool_t163807967 * get_cancelOnExit_20() const { return ___cancelOnExit_20; }
	inline FsmBool_t163807967 ** get_address_of_cancelOnExit_20() { return &___cancelOnExit_20; }
	inline void set_cancelOnExit_20(FsmBool_t163807967 * value)
	{
		___cancelOnExit_20 = value;
		Il2CppCodeGenWriteBarrier((&___cancelOnExit_20), value);
	}

	inline static int32_t get_offset_of_component_21() { return static_cast<int32_t>(offsetof(InvokeMethod_t124369765, ___component_21)); }
	inline MonoBehaviour_t3962482529 * get_component_21() const { return ___component_21; }
	inline MonoBehaviour_t3962482529 ** get_address_of_component_21() { return &___component_21; }
	inline void set_component_21(MonoBehaviour_t3962482529 * value)
	{
		___component_21 = value;
		Il2CppCodeGenWriteBarrier((&___component_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKEMETHOD_T124369765_H
#ifndef KILLDELAYEDEVENTS_T3303987224_H
#define KILLDELAYEDEVENTS_T3303987224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.KillDelayedEvents
struct  KillDelayedEvents_t3303987224  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KILLDELAYEDEVENTS_T3303987224_H
#ifndef LOOKAT_T873698241_H
#define LOOKAT_T873698241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LookAt
struct  LookAt_t873698241  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.LookAt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.LookAt::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.LookAt::targetPosition
	FsmVector3_t626444517 * ___targetPosition_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.LookAt::upVector
	FsmVector3_t626444517 * ___upVector_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LookAt::keepVertical
	FsmBool_t163807967 * ___keepVertical_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LookAt::debug
	FsmBool_t163807967 * ___debug_19;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.LookAt::debugLineColor
	FsmColor_t1738900188 * ___debugLineColor_20;
	// System.Boolean HutongGames.PlayMaker.Actions.LookAt::everyFrame
	bool ___everyFrame_21;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.LookAt::go
	GameObject_t1113636619 * ___go_22;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.LookAt::goTarget
	GameObject_t1113636619 * ___goTarget_23;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::lookAtPos
	Vector3_t3722313464  ___lookAtPos_24;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::lookAtPosWithVertical
	Vector3_t3722313464  ___lookAtPosWithVertical_25;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_targetPosition_16() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___targetPosition_16)); }
	inline FsmVector3_t626444517 * get_targetPosition_16() const { return ___targetPosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_16() { return &___targetPosition_16; }
	inline void set_targetPosition_16(FsmVector3_t626444517 * value)
	{
		___targetPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_16), value);
	}

	inline static int32_t get_offset_of_upVector_17() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___upVector_17)); }
	inline FsmVector3_t626444517 * get_upVector_17() const { return ___upVector_17; }
	inline FsmVector3_t626444517 ** get_address_of_upVector_17() { return &___upVector_17; }
	inline void set_upVector_17(FsmVector3_t626444517 * value)
	{
		___upVector_17 = value;
		Il2CppCodeGenWriteBarrier((&___upVector_17), value);
	}

	inline static int32_t get_offset_of_keepVertical_18() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___keepVertical_18)); }
	inline FsmBool_t163807967 * get_keepVertical_18() const { return ___keepVertical_18; }
	inline FsmBool_t163807967 ** get_address_of_keepVertical_18() { return &___keepVertical_18; }
	inline void set_keepVertical_18(FsmBool_t163807967 * value)
	{
		___keepVertical_18 = value;
		Il2CppCodeGenWriteBarrier((&___keepVertical_18), value);
	}

	inline static int32_t get_offset_of_debug_19() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___debug_19)); }
	inline FsmBool_t163807967 * get_debug_19() const { return ___debug_19; }
	inline FsmBool_t163807967 ** get_address_of_debug_19() { return &___debug_19; }
	inline void set_debug_19(FsmBool_t163807967 * value)
	{
		___debug_19 = value;
		Il2CppCodeGenWriteBarrier((&___debug_19), value);
	}

	inline static int32_t get_offset_of_debugLineColor_20() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___debugLineColor_20)); }
	inline FsmColor_t1738900188 * get_debugLineColor_20() const { return ___debugLineColor_20; }
	inline FsmColor_t1738900188 ** get_address_of_debugLineColor_20() { return &___debugLineColor_20; }
	inline void set_debugLineColor_20(FsmColor_t1738900188 * value)
	{
		___debugLineColor_20 = value;
		Il2CppCodeGenWriteBarrier((&___debugLineColor_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_go_22() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___go_22)); }
	inline GameObject_t1113636619 * get_go_22() const { return ___go_22; }
	inline GameObject_t1113636619 ** get_address_of_go_22() { return &___go_22; }
	inline void set_go_22(GameObject_t1113636619 * value)
	{
		___go_22 = value;
		Il2CppCodeGenWriteBarrier((&___go_22), value);
	}

	inline static int32_t get_offset_of_goTarget_23() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___goTarget_23)); }
	inline GameObject_t1113636619 * get_goTarget_23() const { return ___goTarget_23; }
	inline GameObject_t1113636619 ** get_address_of_goTarget_23() { return &___goTarget_23; }
	inline void set_goTarget_23(GameObject_t1113636619 * value)
	{
		___goTarget_23 = value;
		Il2CppCodeGenWriteBarrier((&___goTarget_23), value);
	}

	inline static int32_t get_offset_of_lookAtPos_24() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___lookAtPos_24)); }
	inline Vector3_t3722313464  get_lookAtPos_24() const { return ___lookAtPos_24; }
	inline Vector3_t3722313464 * get_address_of_lookAtPos_24() { return &___lookAtPos_24; }
	inline void set_lookAtPos_24(Vector3_t3722313464  value)
	{
		___lookAtPos_24 = value;
	}

	inline static int32_t get_offset_of_lookAtPosWithVertical_25() { return static_cast<int32_t>(offsetof(LookAt_t873698241, ___lookAtPosWithVertical_25)); }
	inline Vector3_t3722313464  get_lookAtPosWithVertical_25() const { return ___lookAtPosWithVertical_25; }
	inline Vector3_t3722313464 * get_address_of_lookAtPosWithVertical_25() { return &___lookAtPosWithVertical_25; }
	inline void set_lookAtPosWithVertical_25(Vector3_t3722313464  value)
	{
		___lookAtPosWithVertical_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKAT_T873698241_H
#ifndef MOVETOWARDS_T1086117126_H
#define MOVETOWARDS_T1086117126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MoveTowards
struct  MoveTowards_t1086117126  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MoveTowards::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.MoveTowards::targetObject
	FsmGameObject_t3581898942 * ___targetObject_15;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveTowards::targetPosition
	FsmVector3_t626444517 * ___targetPosition_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MoveTowards::ignoreVertical
	FsmBool_t163807967 * ___ignoreVertical_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MoveTowards::maxSpeed
	FsmFloat_t2883254149 * ___maxSpeed_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MoveTowards::finishDistance
	FsmFloat_t2883254149 * ___finishDistance_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MoveTowards::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_20;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.MoveTowards::go
	GameObject_t1113636619 * ___go_21;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.MoveTowards::goTarget
	GameObject_t1113636619 * ___goTarget_22;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::targetPos
	Vector3_t3722313464  ___targetPos_23;
	// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::targetPosWithVertical
	Vector3_t3722313464  ___targetPosWithVertical_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_targetObject_15() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___targetObject_15)); }
	inline FsmGameObject_t3581898942 * get_targetObject_15() const { return ___targetObject_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_targetObject_15() { return &___targetObject_15; }
	inline void set_targetObject_15(FsmGameObject_t3581898942 * value)
	{
		___targetObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_15), value);
	}

	inline static int32_t get_offset_of_targetPosition_16() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___targetPosition_16)); }
	inline FsmVector3_t626444517 * get_targetPosition_16() const { return ___targetPosition_16; }
	inline FsmVector3_t626444517 ** get_address_of_targetPosition_16() { return &___targetPosition_16; }
	inline void set_targetPosition_16(FsmVector3_t626444517 * value)
	{
		___targetPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetPosition_16), value);
	}

	inline static int32_t get_offset_of_ignoreVertical_17() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___ignoreVertical_17)); }
	inline FsmBool_t163807967 * get_ignoreVertical_17() const { return ___ignoreVertical_17; }
	inline FsmBool_t163807967 ** get_address_of_ignoreVertical_17() { return &___ignoreVertical_17; }
	inline void set_ignoreVertical_17(FsmBool_t163807967 * value)
	{
		___ignoreVertical_17 = value;
		Il2CppCodeGenWriteBarrier((&___ignoreVertical_17), value);
	}

	inline static int32_t get_offset_of_maxSpeed_18() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___maxSpeed_18)); }
	inline FsmFloat_t2883254149 * get_maxSpeed_18() const { return ___maxSpeed_18; }
	inline FsmFloat_t2883254149 ** get_address_of_maxSpeed_18() { return &___maxSpeed_18; }
	inline void set_maxSpeed_18(FsmFloat_t2883254149 * value)
	{
		___maxSpeed_18 = value;
		Il2CppCodeGenWriteBarrier((&___maxSpeed_18), value);
	}

	inline static int32_t get_offset_of_finishDistance_19() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___finishDistance_19)); }
	inline FsmFloat_t2883254149 * get_finishDistance_19() const { return ___finishDistance_19; }
	inline FsmFloat_t2883254149 ** get_address_of_finishDistance_19() { return &___finishDistance_19; }
	inline void set_finishDistance_19(FsmFloat_t2883254149 * value)
	{
		___finishDistance_19 = value;
		Il2CppCodeGenWriteBarrier((&___finishDistance_19), value);
	}

	inline static int32_t get_offset_of_finishEvent_20() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___finishEvent_20)); }
	inline FsmEvent_t3736299882 * get_finishEvent_20() const { return ___finishEvent_20; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_20() { return &___finishEvent_20; }
	inline void set_finishEvent_20(FsmEvent_t3736299882 * value)
	{
		___finishEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_20), value);
	}

	inline static int32_t get_offset_of_go_21() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___go_21)); }
	inline GameObject_t1113636619 * get_go_21() const { return ___go_21; }
	inline GameObject_t1113636619 ** get_address_of_go_21() { return &___go_21; }
	inline void set_go_21(GameObject_t1113636619 * value)
	{
		___go_21 = value;
		Il2CppCodeGenWriteBarrier((&___go_21), value);
	}

	inline static int32_t get_offset_of_goTarget_22() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___goTarget_22)); }
	inline GameObject_t1113636619 * get_goTarget_22() const { return ___goTarget_22; }
	inline GameObject_t1113636619 ** get_address_of_goTarget_22() { return &___goTarget_22; }
	inline void set_goTarget_22(GameObject_t1113636619 * value)
	{
		___goTarget_22 = value;
		Il2CppCodeGenWriteBarrier((&___goTarget_22), value);
	}

	inline static int32_t get_offset_of_targetPos_23() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___targetPos_23)); }
	inline Vector3_t3722313464  get_targetPos_23() const { return ___targetPos_23; }
	inline Vector3_t3722313464 * get_address_of_targetPos_23() { return &___targetPos_23; }
	inline void set_targetPos_23(Vector3_t3722313464  value)
	{
		___targetPos_23 = value;
	}

	inline static int32_t get_offset_of_targetPosWithVertical_24() { return static_cast<int32_t>(offsetof(MoveTowards_t1086117126, ___targetPosWithVertical_24)); }
	inline Vector3_t3722313464  get_targetPosWithVertical_24() const { return ___targetPosWithVertical_24; }
	inline Vector3_t3722313464 * get_address_of_targetPosWithVertical_24() { return &___targetPosWithVertical_24; }
	inline void set_targetPosWithVertical_24(Vector3_t3722313464  value)
	{
		___targetPosWithVertical_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVETOWARDS_T1086117126_H
#ifndef NEXTFRAMEEVENT_T736269368_H
#define NEXTFRAMEEVENT_T736269368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NextFrameEvent
struct  NextFrameEvent_t736269368  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NextFrameEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_14;

public:
	inline static int32_t get_offset_of_sendEvent_14() { return static_cast<int32_t>(offsetof(NextFrameEvent_t736269368, ___sendEvent_14)); }
	inline FsmEvent_t3736299882 * get_sendEvent_14() const { return ___sendEvent_14; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_14() { return &___sendEvent_14; }
	inline void set_sendEvent_14(FsmEvent_t3736299882 * value)
	{
		___sendEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEXTFRAMEEVENT_T736269368_H
#ifndef PERSECOND_T246664478_H
#define PERSECOND_T246664478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PerSecond
struct  PerSecond_t246664478  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PerSecond::floatValue
	FsmFloat_t2883254149 * ___floatValue_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PerSecond::storeResult
	FsmFloat_t2883254149 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.PerSecond::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_floatValue_14() { return static_cast<int32_t>(offsetof(PerSecond_t246664478, ___floatValue_14)); }
	inline FsmFloat_t2883254149 * get_floatValue_14() const { return ___floatValue_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatValue_14() { return &___floatValue_14; }
	inline void set_floatValue_14(FsmFloat_t2883254149 * value)
	{
		___floatValue_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatValue_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(PerSecond_t246664478, ___storeResult_15)); }
	inline FsmFloat_t2883254149 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmFloat_t2883254149 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(PerSecond_t246664478, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSECOND_T246664478_H
#ifndef RANDOMEVENT_T733627204_H
#define RANDOMEVENT_T733627204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RandomEvent
struct  RandomEvent_t733627204  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RandomEvent::delay
	FsmFloat_t2883254149 * ___delay_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RandomEvent::noRepeat
	FsmBool_t163807967 * ___noRepeat_15;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.RandomEvent::delayedEvent
	DelayedEvent_t3987626228 * ___delayedEvent_16;
	// System.Int32 HutongGames.PlayMaker.Actions.RandomEvent::randomEventIndex
	int32_t ___randomEventIndex_17;
	// System.Int32 HutongGames.PlayMaker.Actions.RandomEvent::lastEventIndex
	int32_t ___lastEventIndex_18;

public:
	inline static int32_t get_offset_of_delay_14() { return static_cast<int32_t>(offsetof(RandomEvent_t733627204, ___delay_14)); }
	inline FsmFloat_t2883254149 * get_delay_14() const { return ___delay_14; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_14() { return &___delay_14; }
	inline void set_delay_14(FsmFloat_t2883254149 * value)
	{
		___delay_14 = value;
		Il2CppCodeGenWriteBarrier((&___delay_14), value);
	}

	inline static int32_t get_offset_of_noRepeat_15() { return static_cast<int32_t>(offsetof(RandomEvent_t733627204, ___noRepeat_15)); }
	inline FsmBool_t163807967 * get_noRepeat_15() const { return ___noRepeat_15; }
	inline FsmBool_t163807967 ** get_address_of_noRepeat_15() { return &___noRepeat_15; }
	inline void set_noRepeat_15(FsmBool_t163807967 * value)
	{
		___noRepeat_15 = value;
		Il2CppCodeGenWriteBarrier((&___noRepeat_15), value);
	}

	inline static int32_t get_offset_of_delayedEvent_16() { return static_cast<int32_t>(offsetof(RandomEvent_t733627204, ___delayedEvent_16)); }
	inline DelayedEvent_t3987626228 * get_delayedEvent_16() const { return ___delayedEvent_16; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedEvent_16() { return &___delayedEvent_16; }
	inline void set_delayedEvent_16(DelayedEvent_t3987626228 * value)
	{
		___delayedEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvent_16), value);
	}

	inline static int32_t get_offset_of_randomEventIndex_17() { return static_cast<int32_t>(offsetof(RandomEvent_t733627204, ___randomEventIndex_17)); }
	inline int32_t get_randomEventIndex_17() const { return ___randomEventIndex_17; }
	inline int32_t* get_address_of_randomEventIndex_17() { return &___randomEventIndex_17; }
	inline void set_randomEventIndex_17(int32_t value)
	{
		___randomEventIndex_17 = value;
	}

	inline static int32_t get_offset_of_lastEventIndex_18() { return static_cast<int32_t>(offsetof(RandomEvent_t733627204, ___lastEventIndex_18)); }
	inline int32_t get_lastEventIndex_18() const { return ___lastEventIndex_18; }
	inline int32_t* get_address_of_lastEventIndex_18() { return &___lastEventIndex_18; }
	inline void set_lastEventIndex_18(int32_t value)
	{
		___lastEventIndex_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMEVENT_T733627204_H
#ifndef RANDOMWAIT_T1289990085_H
#define RANDOMWAIT_T1289990085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RandomWait
struct  RandomWait_t1289990085  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RandomWait::min
	FsmFloat_t2883254149 * ___min_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RandomWait::max
	FsmFloat_t2883254149 * ___max_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RandomWait::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.RandomWait::realTime
	bool ___realTime_17;
	// System.Single HutongGames.PlayMaker.Actions.RandomWait::startTime
	float ___startTime_18;
	// System.Single HutongGames.PlayMaker.Actions.RandomWait::timer
	float ___timer_19;
	// System.Single HutongGames.PlayMaker.Actions.RandomWait::time
	float ___time_20;

public:
	inline static int32_t get_offset_of_min_14() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___min_14)); }
	inline FsmFloat_t2883254149 * get_min_14() const { return ___min_14; }
	inline FsmFloat_t2883254149 ** get_address_of_min_14() { return &___min_14; }
	inline void set_min_14(FsmFloat_t2883254149 * value)
	{
		___min_14 = value;
		Il2CppCodeGenWriteBarrier((&___min_14), value);
	}

	inline static int32_t get_offset_of_max_15() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___max_15)); }
	inline FsmFloat_t2883254149 * get_max_15() const { return ___max_15; }
	inline FsmFloat_t2883254149 ** get_address_of_max_15() { return &___max_15; }
	inline void set_max_15(FsmFloat_t2883254149 * value)
	{
		___max_15 = value;
		Il2CppCodeGenWriteBarrier((&___max_15), value);
	}

	inline static int32_t get_offset_of_finishEvent_16() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___finishEvent_16)); }
	inline FsmEvent_t3736299882 * get_finishEvent_16() const { return ___finishEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_16() { return &___finishEvent_16; }
	inline void set_finishEvent_16(FsmEvent_t3736299882 * value)
	{
		___finishEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_16), value);
	}

	inline static int32_t get_offset_of_realTime_17() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___realTime_17)); }
	inline bool get_realTime_17() const { return ___realTime_17; }
	inline bool* get_address_of_realTime_17() { return &___realTime_17; }
	inline void set_realTime_17(bool value)
	{
		___realTime_17 = value;
	}

	inline static int32_t get_offset_of_startTime_18() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___startTime_18)); }
	inline float get_startTime_18() const { return ___startTime_18; }
	inline float* get_address_of_startTime_18() { return &___startTime_18; }
	inline void set_startTime_18(float value)
	{
		___startTime_18 = value;
	}

	inline static int32_t get_offset_of_timer_19() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___timer_19)); }
	inline float get_timer_19() const { return ___timer_19; }
	inline float* get_address_of_timer_19() { return &___timer_19; }
	inline void set_timer_19(float value)
	{
		___timer_19 = value;
	}

	inline static int32_t get_offset_of_time_20() { return static_cast<int32_t>(offsetof(RandomWait_t1289990085, ___time_20)); }
	inline float get_time_20() const { return ___time_20; }
	inline float* get_address_of_time_20() { return &___time_20; }
	inline void set_time_20(float value)
	{
		___time_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMWAIT_T1289990085_H
#ifndef ROTATE_T4200752417_H
#define ROTATE_T4200752417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Rotate
struct  Rotate_t4200752417  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.Rotate::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Rotate::vector
	FsmVector3_t626444517 * ___vector_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Rotate::xAngle
	FsmFloat_t2883254149 * ___xAngle_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Rotate::yAngle
	FsmFloat_t2883254149 * ___yAngle_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Rotate::zAngle
	FsmFloat_t2883254149 * ___zAngle_18;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.Rotate::space
	int32_t ___space_19;
	// System.Boolean HutongGames.PlayMaker.Actions.Rotate::perSecond
	bool ___perSecond_20;
	// System.Boolean HutongGames.PlayMaker.Actions.Rotate::everyFrame
	bool ___everyFrame_21;
	// System.Boolean HutongGames.PlayMaker.Actions.Rotate::lateUpdate
	bool ___lateUpdate_22;
	// System.Boolean HutongGames.PlayMaker.Actions.Rotate::fixedUpdate
	bool ___fixedUpdate_23;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_vector_15() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___vector_15)); }
	inline FsmVector3_t626444517 * get_vector_15() const { return ___vector_15; }
	inline FsmVector3_t626444517 ** get_address_of_vector_15() { return &___vector_15; }
	inline void set_vector_15(FsmVector3_t626444517 * value)
	{
		___vector_15 = value;
		Il2CppCodeGenWriteBarrier((&___vector_15), value);
	}

	inline static int32_t get_offset_of_xAngle_16() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___xAngle_16)); }
	inline FsmFloat_t2883254149 * get_xAngle_16() const { return ___xAngle_16; }
	inline FsmFloat_t2883254149 ** get_address_of_xAngle_16() { return &___xAngle_16; }
	inline void set_xAngle_16(FsmFloat_t2883254149 * value)
	{
		___xAngle_16 = value;
		Il2CppCodeGenWriteBarrier((&___xAngle_16), value);
	}

	inline static int32_t get_offset_of_yAngle_17() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___yAngle_17)); }
	inline FsmFloat_t2883254149 * get_yAngle_17() const { return ___yAngle_17; }
	inline FsmFloat_t2883254149 ** get_address_of_yAngle_17() { return &___yAngle_17; }
	inline void set_yAngle_17(FsmFloat_t2883254149 * value)
	{
		___yAngle_17 = value;
		Il2CppCodeGenWriteBarrier((&___yAngle_17), value);
	}

	inline static int32_t get_offset_of_zAngle_18() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___zAngle_18)); }
	inline FsmFloat_t2883254149 * get_zAngle_18() const { return ___zAngle_18; }
	inline FsmFloat_t2883254149 ** get_address_of_zAngle_18() { return &___zAngle_18; }
	inline void set_zAngle_18(FsmFloat_t2883254149 * value)
	{
		___zAngle_18 = value;
		Il2CppCodeGenWriteBarrier((&___zAngle_18), value);
	}

	inline static int32_t get_offset_of_space_19() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___space_19)); }
	inline int32_t get_space_19() const { return ___space_19; }
	inline int32_t* get_address_of_space_19() { return &___space_19; }
	inline void set_space_19(int32_t value)
	{
		___space_19 = value;
	}

	inline static int32_t get_offset_of_perSecond_20() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___perSecond_20)); }
	inline bool get_perSecond_20() const { return ___perSecond_20; }
	inline bool* get_address_of_perSecond_20() { return &___perSecond_20; }
	inline void set_perSecond_20(bool value)
	{
		___perSecond_20 = value;
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}

	inline static int32_t get_offset_of_lateUpdate_22() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___lateUpdate_22)); }
	inline bool get_lateUpdate_22() const { return ___lateUpdate_22; }
	inline bool* get_address_of_lateUpdate_22() { return &___lateUpdate_22; }
	inline void set_lateUpdate_22(bool value)
	{
		___lateUpdate_22 = value;
	}

	inline static int32_t get_offset_of_fixedUpdate_23() { return static_cast<int32_t>(offsetof(Rotate_t4200752417, ___fixedUpdate_23)); }
	inline bool get_fixedUpdate_23() const { return ___fixedUpdate_23; }
	inline bool* get_address_of_fixedUpdate_23() { return &___fixedUpdate_23; }
	inline void set_fixedUpdate_23(bool value)
	{
		___fixedUpdate_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T4200752417_H
#ifndef RUNFSMACTION_T3423579716_H
#define RUNFSMACTION_T3423579716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RunFSMAction
struct  RunFSMAction_t3423579716  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Actions.RunFSMAction::runFsm
	Fsm_t4127147824 * ___runFsm_14;

public:
	inline static int32_t get_offset_of_runFsm_14() { return static_cast<int32_t>(offsetof(RunFSMAction_t3423579716, ___runFsm_14)); }
	inline Fsm_t4127147824 * get_runFsm_14() const { return ___runFsm_14; }
	inline Fsm_t4127147824 ** get_address_of_runFsm_14() { return &___runFsm_14; }
	inline void set_runFsm_14(Fsm_t4127147824 * value)
	{
		___runFsm_14 = value;
		Il2CppCodeGenWriteBarrier((&___runFsm_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNFSMACTION_T3423579716_H
#ifndef SCALETIME_T740334591_H
#define SCALETIME_T740334591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScaleTime
struct  ScaleTime_t740334591  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScaleTime::timeScale
	FsmFloat_t2883254149 * ___timeScale_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScaleTime::adjustFixedDeltaTime
	FsmBool_t163807967 * ___adjustFixedDeltaTime_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ScaleTime::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_timeScale_14() { return static_cast<int32_t>(offsetof(ScaleTime_t740334591, ___timeScale_14)); }
	inline FsmFloat_t2883254149 * get_timeScale_14() const { return ___timeScale_14; }
	inline FsmFloat_t2883254149 ** get_address_of_timeScale_14() { return &___timeScale_14; }
	inline void set_timeScale_14(FsmFloat_t2883254149 * value)
	{
		___timeScale_14 = value;
		Il2CppCodeGenWriteBarrier((&___timeScale_14), value);
	}

	inline static int32_t get_offset_of_adjustFixedDeltaTime_15() { return static_cast<int32_t>(offsetof(ScaleTime_t740334591, ___adjustFixedDeltaTime_15)); }
	inline FsmBool_t163807967 * get_adjustFixedDeltaTime_15() const { return ___adjustFixedDeltaTime_15; }
	inline FsmBool_t163807967 ** get_address_of_adjustFixedDeltaTime_15() { return &___adjustFixedDeltaTime_15; }
	inline void set_adjustFixedDeltaTime_15(FsmBool_t163807967 * value)
	{
		___adjustFixedDeltaTime_15 = value;
		Il2CppCodeGenWriteBarrier((&___adjustFixedDeltaTime_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ScaleTime_t740334591, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALETIME_T740334591_H
#ifndef SELECTRANDOMSTRING_T3157529308_H
#define SELECTRANDOMSTRING_T3157529308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SelectRandomString
struct  SelectRandomString_t3157529308  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.SelectRandomString::strings
	FsmStringU5BU5D_t252501805* ___strings_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SelectRandomString::weights
	FsmFloatU5BU5D_t3637897416* ___weights_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SelectRandomString::storeString
	FsmString_t1785915204 * ___storeString_16;

public:
	inline static int32_t get_offset_of_strings_14() { return static_cast<int32_t>(offsetof(SelectRandomString_t3157529308, ___strings_14)); }
	inline FsmStringU5BU5D_t252501805* get_strings_14() const { return ___strings_14; }
	inline FsmStringU5BU5D_t252501805** get_address_of_strings_14() { return &___strings_14; }
	inline void set_strings_14(FsmStringU5BU5D_t252501805* value)
	{
		___strings_14 = value;
		Il2CppCodeGenWriteBarrier((&___strings_14), value);
	}

	inline static int32_t get_offset_of_weights_15() { return static_cast<int32_t>(offsetof(SelectRandomString_t3157529308, ___weights_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_15() const { return ___weights_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_15() { return &___weights_15; }
	inline void set_weights_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_15 = value;
		Il2CppCodeGenWriteBarrier((&___weights_15), value);
	}

	inline static int32_t get_offset_of_storeString_16() { return static_cast<int32_t>(offsetof(SelectRandomString_t3157529308, ___storeString_16)); }
	inline FsmString_t1785915204 * get_storeString_16() const { return ___storeString_16; }
	inline FsmString_t1785915204 ** get_address_of_storeString_16() { return &___storeString_16; }
	inline void set_storeString_16(FsmString_t1785915204 * value)
	{
		___storeString_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeString_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTRANDOMSTRING_T3157529308_H
#ifndef SENDEVENT_T1101542469_H
#define SENDEVENT_T1101542469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendEvent
struct  SendEvent_t1101542469  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.SendEvent::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.SendEvent::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendEvent::delay
	FsmFloat_t2883254149 * ___delay_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SendEvent::everyFrame
	bool ___everyFrame_17;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendEvent::delayedEvent
	DelayedEvent_t3987626228 * ___delayedEvent_18;

public:
	inline static int32_t get_offset_of_eventTarget_14() { return static_cast<int32_t>(offsetof(SendEvent_t1101542469, ___eventTarget_14)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_14() const { return ___eventTarget_14; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_14() { return &___eventTarget_14; }
	inline void set_eventTarget_14(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(SendEvent_t1101542469, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(SendEvent_t1101542469, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SendEvent_t1101542469, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_delayedEvent_18() { return static_cast<int32_t>(offsetof(SendEvent_t1101542469, ___delayedEvent_18)); }
	inline DelayedEvent_t3987626228 * get_delayedEvent_18() const { return ___delayedEvent_18; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedEvent_18() { return &___delayedEvent_18; }
	inline void set_delayedEvent_18(DelayedEvent_t3987626228 * value)
	{
		___delayedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDEVENT_T1101542469_H
#ifndef SENDEVENTBYNAME_T1677051455_H
#define SENDEVENTBYNAME_T1677051455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendEventByName
struct  SendEventByName_t1677051455  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.SendEventByName::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SendEventByName::sendEvent
	FsmString_t1785915204 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendEventByName::delay
	FsmFloat_t2883254149 * ___delay_16;
	// System.Boolean HutongGames.PlayMaker.Actions.SendEventByName::everyFrame
	bool ___everyFrame_17;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendEventByName::delayedEvent
	DelayedEvent_t3987626228 * ___delayedEvent_18;

public:
	inline static int32_t get_offset_of_eventTarget_14() { return static_cast<int32_t>(offsetof(SendEventByName_t1677051455, ___eventTarget_14)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_14() const { return ___eventTarget_14; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_14() { return &___eventTarget_14; }
	inline void set_eventTarget_14(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(SendEventByName_t1677051455, ___sendEvent_15)); }
	inline FsmString_t1785915204 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmString_t1785915204 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmString_t1785915204 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(SendEventByName_t1677051455, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(SendEventByName_t1677051455, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of_delayedEvent_18() { return static_cast<int32_t>(offsetof(SendEventByName_t1677051455, ___delayedEvent_18)); }
	inline DelayedEvent_t3987626228 * get_delayedEvent_18() const { return ___delayedEvent_18; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedEvent_18() { return &___delayedEvent_18; }
	inline void set_delayedEvent_18(DelayedEvent_t3987626228 * value)
	{
		___delayedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDEVENTBYNAME_T1677051455_H
#ifndef SENDEVENTTOFSM_T486353493_H
#define SENDEVENTTOFSM_T486353493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendEventToFsm
struct  SendEventToFsm_t486353493  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SendEventToFsm::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SendEventToFsm::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SendEventToFsm::sendEvent
	FsmString_t1785915204 * ___sendEvent_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendEventToFsm::delay
	FsmFloat_t2883254149 * ___delay_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SendEventToFsm::requireReceiver
	bool ___requireReceiver_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SendEventToFsm::go
	GameObject_t1113636619 * ___go_19;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendEventToFsm::delayedEvent
	DelayedEvent_t3987626228 * ___delayedEvent_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___sendEvent_16)); }
	inline FsmString_t1785915204 * get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmString_t1785915204 ** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmString_t1785915204 * value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_delay_17() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___delay_17)); }
	inline FsmFloat_t2883254149 * get_delay_17() const { return ___delay_17; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_17() { return &___delay_17; }
	inline void set_delay_17(FsmFloat_t2883254149 * value)
	{
		___delay_17 = value;
		Il2CppCodeGenWriteBarrier((&___delay_17), value);
	}

	inline static int32_t get_offset_of_requireReceiver_18() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___requireReceiver_18)); }
	inline bool get_requireReceiver_18() const { return ___requireReceiver_18; }
	inline bool* get_address_of_requireReceiver_18() { return &___requireReceiver_18; }
	inline void set_requireReceiver_18(bool value)
	{
		___requireReceiver_18 = value;
	}

	inline static int32_t get_offset_of_go_19() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___go_19)); }
	inline GameObject_t1113636619 * get_go_19() const { return ___go_19; }
	inline GameObject_t1113636619 ** get_address_of_go_19() { return &___go_19; }
	inline void set_go_19(GameObject_t1113636619 * value)
	{
		___go_19 = value;
		Il2CppCodeGenWriteBarrier((&___go_19), value);
	}

	inline static int32_t get_offset_of_delayedEvent_20() { return static_cast<int32_t>(offsetof(SendEventToFsm_t486353493, ___delayedEvent_20)); }
	inline DelayedEvent_t3987626228 * get_delayedEvent_20() const { return ___delayedEvent_20; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedEvent_20() { return &___delayedEvent_20; }
	inline void set_delayedEvent_20(DelayedEvent_t3987626228 * value)
	{
		___delayedEvent_20 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvent_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDEVENTTOFSM_T486353493_H
#ifndef SENDMESSAGE_T1630967858_H
#define SENDMESSAGE_T1630967858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendMessage
struct  SendMessage_t1630967858  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SendMessage::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.Actions.SendMessage/MessageType HutongGames.PlayMaker.Actions.SendMessage::delivery
	int32_t ___delivery_15;
	// UnityEngine.SendMessageOptions HutongGames.PlayMaker.Actions.SendMessage::options
	int32_t ___options_16;
	// HutongGames.PlayMaker.FunctionCall HutongGames.PlayMaker.Actions.SendMessage::functionCall
	FunctionCall_t1722713284 * ___functionCall_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SendMessage_t1630967858, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_delivery_15() { return static_cast<int32_t>(offsetof(SendMessage_t1630967858, ___delivery_15)); }
	inline int32_t get_delivery_15() const { return ___delivery_15; }
	inline int32_t* get_address_of_delivery_15() { return &___delivery_15; }
	inline void set_delivery_15(int32_t value)
	{
		___delivery_15 = value;
	}

	inline static int32_t get_offset_of_options_16() { return static_cast<int32_t>(offsetof(SendMessage_t1630967858, ___options_16)); }
	inline int32_t get_options_16() const { return ___options_16; }
	inline int32_t* get_address_of_options_16() { return &___options_16; }
	inline void set_options_16(int32_t value)
	{
		___options_16 = value;
	}

	inline static int32_t get_offset_of_functionCall_17() { return static_cast<int32_t>(offsetof(SendMessage_t1630967858, ___functionCall_17)); }
	inline FunctionCall_t1722713284 * get_functionCall_17() const { return ___functionCall_17; }
	inline FunctionCall_t1722713284 ** get_address_of_functionCall_17() { return &___functionCall_17; }
	inline void set_functionCall_17(FunctionCall_t1722713284 * value)
	{
		___functionCall_17 = value;
		Il2CppCodeGenWriteBarrier((&___functionCall_17), value);
	}
};

struct SendMessage_t1630967858_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> HutongGames.PlayMaker.Actions.SendMessage::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_18() { return static_cast<int32_t>(offsetof(SendMessage_t1630967858_StaticFields, ___U3CU3Ef__switchU24map0_18)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_18() const { return ___U3CU3Ef__switchU24map0_18; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_18() { return &___U3CU3Ef__switchU24map0_18; }
	inline void set_U3CU3Ef__switchU24map0_18(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGE_T1630967858_H
#ifndef SENDRANDOMEVENT_T2238267572_H
#define SENDRANDOMEVENT_T2238267572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SendRandomEvent
struct  SendRandomEvent_t2238267572  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.SendRandomEvent::events
	FsmEventU5BU5D_t2511618479* ___events_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.SendRandomEvent::weights
	FsmFloatU5BU5D_t3637897416* ___weights_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SendRandomEvent::delay
	FsmFloat_t2883254149 * ___delay_16;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SendRandomEvent::delayedEvent
	DelayedEvent_t3987626228 * ___delayedEvent_17;

public:
	inline static int32_t get_offset_of_events_14() { return static_cast<int32_t>(offsetof(SendRandomEvent_t2238267572, ___events_14)); }
	inline FsmEventU5BU5D_t2511618479* get_events_14() const { return ___events_14; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_events_14() { return &___events_14; }
	inline void set_events_14(FsmEventU5BU5D_t2511618479* value)
	{
		___events_14 = value;
		Il2CppCodeGenWriteBarrier((&___events_14), value);
	}

	inline static int32_t get_offset_of_weights_15() { return static_cast<int32_t>(offsetof(SendRandomEvent_t2238267572, ___weights_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_weights_15() const { return ___weights_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_weights_15() { return &___weights_15; }
	inline void set_weights_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___weights_15 = value;
		Il2CppCodeGenWriteBarrier((&___weights_15), value);
	}

	inline static int32_t get_offset_of_delay_16() { return static_cast<int32_t>(offsetof(SendRandomEvent_t2238267572, ___delay_16)); }
	inline FsmFloat_t2883254149 * get_delay_16() const { return ___delay_16; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_16() { return &___delay_16; }
	inline void set_delay_16(FsmFloat_t2883254149 * value)
	{
		___delay_16 = value;
		Il2CppCodeGenWriteBarrier((&___delay_16), value);
	}

	inline static int32_t get_offset_of_delayedEvent_17() { return static_cast<int32_t>(offsetof(SendRandomEvent_t2238267572, ___delayedEvent_17)); }
	inline DelayedEvent_t3987626228 * get_delayedEvent_17() const { return ___delayedEvent_17; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedEvent_17() { return &___delayedEvent_17; }
	inline void set_delayedEvent_17(DelayedEvent_t3987626228 * value)
	{
		___delayedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvent_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDRANDOMEVENT_T2238267572_H
#ifndef SEQUENCEEVENT_T2748694391_H
#define SEQUENCEEVENT_T2748694391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SequenceEvent
struct  SequenceEvent_t2748694391  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SequenceEvent::delay
	FsmFloat_t2883254149 * ___delay_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SequenceEvent::reset
	FsmBool_t163807967 * ___reset_15;
	// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Actions.SequenceEvent::delayedEvent
	DelayedEvent_t3987626228 * ___delayedEvent_16;
	// System.Int32 HutongGames.PlayMaker.Actions.SequenceEvent::eventIndex
	int32_t ___eventIndex_17;

public:
	inline static int32_t get_offset_of_delay_14() { return static_cast<int32_t>(offsetof(SequenceEvent_t2748694391, ___delay_14)); }
	inline FsmFloat_t2883254149 * get_delay_14() const { return ___delay_14; }
	inline FsmFloat_t2883254149 ** get_address_of_delay_14() { return &___delay_14; }
	inline void set_delay_14(FsmFloat_t2883254149 * value)
	{
		___delay_14 = value;
		Il2CppCodeGenWriteBarrier((&___delay_14), value);
	}

	inline static int32_t get_offset_of_reset_15() { return static_cast<int32_t>(offsetof(SequenceEvent_t2748694391, ___reset_15)); }
	inline FsmBool_t163807967 * get_reset_15() const { return ___reset_15; }
	inline FsmBool_t163807967 ** get_address_of_reset_15() { return &___reset_15; }
	inline void set_reset_15(FsmBool_t163807967 * value)
	{
		___reset_15 = value;
		Il2CppCodeGenWriteBarrier((&___reset_15), value);
	}

	inline static int32_t get_offset_of_delayedEvent_16() { return static_cast<int32_t>(offsetof(SequenceEvent_t2748694391, ___delayedEvent_16)); }
	inline DelayedEvent_t3987626228 * get_delayedEvent_16() const { return ___delayedEvent_16; }
	inline DelayedEvent_t3987626228 ** get_address_of_delayedEvent_16() { return &___delayedEvent_16; }
	inline void set_delayedEvent_16(DelayedEvent_t3987626228 * value)
	{
		___delayedEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___delayedEvent_16), value);
	}

	inline static int32_t get_offset_of_eventIndex_17() { return static_cast<int32_t>(offsetof(SequenceEvent_t2748694391, ___eventIndex_17)); }
	inline int32_t get_eventIndex_17() const { return ___eventIndex_17; }
	inline int32_t* get_address_of_eventIndex_17() { return &___eventIndex_17; }
	inline void set_eventIndex_17(int32_t value)
	{
		___eventIndex_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCEEVENT_T2748694391_H
#ifndef SETEVENTDATA_T4059347819_H
#define SETEVENTDATA_T4059347819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetEventData
struct  SetEventData_t4059347819  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetEventData::setGameObjectData
	FsmGameObject_t3581898942 * ___setGameObjectData_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetEventData::setIntData
	FsmInt_t874273141 * ___setIntData_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetEventData::setFloatData
	FsmFloat_t2883254149 * ___setFloatData_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetEventData::setStringData
	FsmString_t1785915204 * ___setStringData_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetEventData::setBoolData
	FsmBool_t163807967 * ___setBoolData_18;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetEventData::setVector2Data
	FsmVector2_t2965096677 * ___setVector2Data_19;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetEventData::setVector3Data
	FsmVector3_t626444517 * ___setVector3Data_20;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetEventData::setRectData
	FsmRect_t3649179877 * ___setRectData_21;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetEventData::setQuaternionData
	FsmQuaternion_t4259038327 * ___setQuaternionData_22;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetEventData::setColorData
	FsmColor_t1738900188 * ___setColorData_23;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetEventData::setMaterialData
	FsmMaterial_t2057874452 * ___setMaterialData_24;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetEventData::setTextureData
	FsmTexture_t1738787045 * ___setTextureData_25;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetEventData::setObjectData
	FsmObject_t2606870197 * ___setObjectData_26;

public:
	inline static int32_t get_offset_of_setGameObjectData_14() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setGameObjectData_14)); }
	inline FsmGameObject_t3581898942 * get_setGameObjectData_14() const { return ___setGameObjectData_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_setGameObjectData_14() { return &___setGameObjectData_14; }
	inline void set_setGameObjectData_14(FsmGameObject_t3581898942 * value)
	{
		___setGameObjectData_14 = value;
		Il2CppCodeGenWriteBarrier((&___setGameObjectData_14), value);
	}

	inline static int32_t get_offset_of_setIntData_15() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setIntData_15)); }
	inline FsmInt_t874273141 * get_setIntData_15() const { return ___setIntData_15; }
	inline FsmInt_t874273141 ** get_address_of_setIntData_15() { return &___setIntData_15; }
	inline void set_setIntData_15(FsmInt_t874273141 * value)
	{
		___setIntData_15 = value;
		Il2CppCodeGenWriteBarrier((&___setIntData_15), value);
	}

	inline static int32_t get_offset_of_setFloatData_16() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setFloatData_16)); }
	inline FsmFloat_t2883254149 * get_setFloatData_16() const { return ___setFloatData_16; }
	inline FsmFloat_t2883254149 ** get_address_of_setFloatData_16() { return &___setFloatData_16; }
	inline void set_setFloatData_16(FsmFloat_t2883254149 * value)
	{
		___setFloatData_16 = value;
		Il2CppCodeGenWriteBarrier((&___setFloatData_16), value);
	}

	inline static int32_t get_offset_of_setStringData_17() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setStringData_17)); }
	inline FsmString_t1785915204 * get_setStringData_17() const { return ___setStringData_17; }
	inline FsmString_t1785915204 ** get_address_of_setStringData_17() { return &___setStringData_17; }
	inline void set_setStringData_17(FsmString_t1785915204 * value)
	{
		___setStringData_17 = value;
		Il2CppCodeGenWriteBarrier((&___setStringData_17), value);
	}

	inline static int32_t get_offset_of_setBoolData_18() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setBoolData_18)); }
	inline FsmBool_t163807967 * get_setBoolData_18() const { return ___setBoolData_18; }
	inline FsmBool_t163807967 ** get_address_of_setBoolData_18() { return &___setBoolData_18; }
	inline void set_setBoolData_18(FsmBool_t163807967 * value)
	{
		___setBoolData_18 = value;
		Il2CppCodeGenWriteBarrier((&___setBoolData_18), value);
	}

	inline static int32_t get_offset_of_setVector2Data_19() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setVector2Data_19)); }
	inline FsmVector2_t2965096677 * get_setVector2Data_19() const { return ___setVector2Data_19; }
	inline FsmVector2_t2965096677 ** get_address_of_setVector2Data_19() { return &___setVector2Data_19; }
	inline void set_setVector2Data_19(FsmVector2_t2965096677 * value)
	{
		___setVector2Data_19 = value;
		Il2CppCodeGenWriteBarrier((&___setVector2Data_19), value);
	}

	inline static int32_t get_offset_of_setVector3Data_20() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setVector3Data_20)); }
	inline FsmVector3_t626444517 * get_setVector3Data_20() const { return ___setVector3Data_20; }
	inline FsmVector3_t626444517 ** get_address_of_setVector3Data_20() { return &___setVector3Data_20; }
	inline void set_setVector3Data_20(FsmVector3_t626444517 * value)
	{
		___setVector3Data_20 = value;
		Il2CppCodeGenWriteBarrier((&___setVector3Data_20), value);
	}

	inline static int32_t get_offset_of_setRectData_21() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setRectData_21)); }
	inline FsmRect_t3649179877 * get_setRectData_21() const { return ___setRectData_21; }
	inline FsmRect_t3649179877 ** get_address_of_setRectData_21() { return &___setRectData_21; }
	inline void set_setRectData_21(FsmRect_t3649179877 * value)
	{
		___setRectData_21 = value;
		Il2CppCodeGenWriteBarrier((&___setRectData_21), value);
	}

	inline static int32_t get_offset_of_setQuaternionData_22() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setQuaternionData_22)); }
	inline FsmQuaternion_t4259038327 * get_setQuaternionData_22() const { return ___setQuaternionData_22; }
	inline FsmQuaternion_t4259038327 ** get_address_of_setQuaternionData_22() { return &___setQuaternionData_22; }
	inline void set_setQuaternionData_22(FsmQuaternion_t4259038327 * value)
	{
		___setQuaternionData_22 = value;
		Il2CppCodeGenWriteBarrier((&___setQuaternionData_22), value);
	}

	inline static int32_t get_offset_of_setColorData_23() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setColorData_23)); }
	inline FsmColor_t1738900188 * get_setColorData_23() const { return ___setColorData_23; }
	inline FsmColor_t1738900188 ** get_address_of_setColorData_23() { return &___setColorData_23; }
	inline void set_setColorData_23(FsmColor_t1738900188 * value)
	{
		___setColorData_23 = value;
		Il2CppCodeGenWriteBarrier((&___setColorData_23), value);
	}

	inline static int32_t get_offset_of_setMaterialData_24() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setMaterialData_24)); }
	inline FsmMaterial_t2057874452 * get_setMaterialData_24() const { return ___setMaterialData_24; }
	inline FsmMaterial_t2057874452 ** get_address_of_setMaterialData_24() { return &___setMaterialData_24; }
	inline void set_setMaterialData_24(FsmMaterial_t2057874452 * value)
	{
		___setMaterialData_24 = value;
		Il2CppCodeGenWriteBarrier((&___setMaterialData_24), value);
	}

	inline static int32_t get_offset_of_setTextureData_25() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setTextureData_25)); }
	inline FsmTexture_t1738787045 * get_setTextureData_25() const { return ___setTextureData_25; }
	inline FsmTexture_t1738787045 ** get_address_of_setTextureData_25() { return &___setTextureData_25; }
	inline void set_setTextureData_25(FsmTexture_t1738787045 * value)
	{
		___setTextureData_25 = value;
		Il2CppCodeGenWriteBarrier((&___setTextureData_25), value);
	}

	inline static int32_t get_offset_of_setObjectData_26() { return static_cast<int32_t>(offsetof(SetEventData_t4059347819, ___setObjectData_26)); }
	inline FsmObject_t2606870197 * get_setObjectData_26() const { return ___setObjectData_26; }
	inline FsmObject_t2606870197 ** get_address_of_setObjectData_26() { return &___setObjectData_26; }
	inline void set_setObjectData_26(FsmObject_t2606870197 * value)
	{
		___setObjectData_26 = value;
		Il2CppCodeGenWriteBarrier((&___setObjectData_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETEVENTDATA_T4059347819_H
#ifndef SETEVENTTARGET_T83275134_H
#define SETEVENTTARGET_T83275134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetEventTarget
struct  SetEventTarget_t83275134  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.SetEventTarget::eventTarget
	FsmEventTarget_t919699796 * ___eventTarget_14;

public:
	inline static int32_t get_offset_of_eventTarget_14() { return static_cast<int32_t>(offsetof(SetEventTarget_t83275134, ___eventTarget_14)); }
	inline FsmEventTarget_t919699796 * get_eventTarget_14() const { return ___eventTarget_14; }
	inline FsmEventTarget_t919699796 ** get_address_of_eventTarget_14() { return &___eventTarget_14; }
	inline void set_eventTarget_14(FsmEventTarget_t919699796 * value)
	{
		___eventTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___eventTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETEVENTTARGET_T83275134_H
#ifndef SETFSMBOOL_T2339160745_H
#define SETFSMBOOL_T2339160745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmBool
struct  SetFsmBool_t2339160745  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmBool::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmBool::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmBool::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetFsmBool::setValue
	FsmBool_t163807967 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmBool::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmBool::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmBool::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmBool::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___setValue_17)); }
	inline FsmBool_t163807967 * get_setValue_17() const { return ___setValue_17; }
	inline FsmBool_t163807967 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmBool_t163807967 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmBool_t2339160745, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMBOOL_T2339160745_H
#ifndef SETFSMCOLOR_T1836528779_H
#define SETFSMCOLOR_T1836528779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmColor
struct  SetFsmColor_t1836528779  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmColor::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmColor::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetFsmColor::setValue
	FsmColor_t1738900188 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmColor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmColor::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmColor::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmColor::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___setValue_17)); }
	inline FsmColor_t1738900188 * get_setValue_17() const { return ___setValue_17; }
	inline FsmColor_t1738900188 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmColor_t1738900188 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmColor_t1836528779, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMCOLOR_T1836528779_H
#ifndef SETFSMENUM_T476888414_H
#define SETFSMENUM_T476888414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmEnum
struct  SetFsmEnum_t476888414  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmEnum::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmEnum::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmEnum::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.SetFsmEnum::setValue
	FsmEnum_t2861764163 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmEnum::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmEnum::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmEnum::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmEnum::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___setValue_17)); }
	inline FsmEnum_t2861764163 * get_setValue_17() const { return ___setValue_17; }
	inline FsmEnum_t2861764163 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmEnum_t2861764163 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmEnum_t476888414, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMENUM_T476888414_H
#ifndef SETFSMFLOAT_T1086314331_H
#define SETFSMFLOAT_T1086314331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmFloat
struct  SetFsmFloat_t1086314331  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmFloat::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmFloat::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmFloat::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetFsmFloat::setValue
	FsmFloat_t2883254149 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmFloat::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmFloat::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmFloat::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmFloat::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___setValue_17)); }
	inline FsmFloat_t2883254149 * get_setValue_17() const { return ___setValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmFloat_t2883254149 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmFloat_t1086314331, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMFLOAT_T1086314331_H
#ifndef SETFSMGAMEOBJECT_T2454320077_H
#define SETFSMGAMEOBJECT_T2454320077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmGameObject
struct  SetFsmGameObject_t2454320077  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmGameObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmGameObject::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmGameObject::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetFsmGameObject::setValue
	FsmGameObject_t3581898942 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmGameObject::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmGameObject::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmGameObject::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmGameObject::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___setValue_17)); }
	inline FsmGameObject_t3581898942 * get_setValue_17() const { return ___setValue_17; }
	inline FsmGameObject_t3581898942 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmGameObject_t3581898942 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmGameObject_t2454320077, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMGAMEOBJECT_T2454320077_H
#ifndef SETFSMINT_T1274753533_H
#define SETFSMINT_T1274753533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmInt
struct  SetFsmInt_t1274753533  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmInt::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmInt::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmInt::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetFsmInt::setValue
	FsmInt_t874273141 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmInt::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmInt::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmInt::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmInt::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___setValue_17)); }
	inline FsmInt_t874273141 * get_setValue_17() const { return ___setValue_17; }
	inline FsmInt_t874273141 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmInt_t874273141 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmInt_t1274753533, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMINT_T1274753533_H
#ifndef SETFSMMATERIAL_T2596332468_H
#define SETFSMMATERIAL_T2596332468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmMaterial
struct  SetFsmMaterial_t2596332468  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmMaterial::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmMaterial::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmMaterial::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetFsmMaterial::setValue
	FsmMaterial_t2057874452 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmMaterial::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmMaterial::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmMaterial::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmMaterial::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___setValue_17)); }
	inline FsmMaterial_t2057874452 * get_setValue_17() const { return ___setValue_17; }
	inline FsmMaterial_t2057874452 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmMaterial_t2057874452 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmMaterial_t2596332468, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMMATERIAL_T2596332468_H
#ifndef SETFSMOBJECT_T695695972_H
#define SETFSMOBJECT_T695695972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmObject
struct  SetFsmObject_t695695972  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmObject::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmObject::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmObject::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetFsmObject::setValue
	FsmObject_t2606870197 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmObject::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmObject::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmObject::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmObject::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___setValue_17)); }
	inline FsmObject_t2606870197 * get_setValue_17() const { return ___setValue_17; }
	inline FsmObject_t2606870197 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmObject_t2606870197 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmObject_t695695972, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMOBJECT_T695695972_H
#ifndef SETFSMQUATERNION_T2442002114_H
#define SETFSMQUATERNION_T2442002114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmQuaternion
struct  SetFsmQuaternion_t2442002114  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmQuaternion::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmQuaternion::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmQuaternion::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetFsmQuaternion::setValue
	FsmQuaternion_t4259038327 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmQuaternion::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmQuaternion::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmQuaternion::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmQuaternion::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___setValue_17)); }
	inline FsmQuaternion_t4259038327 * get_setValue_17() const { return ___setValue_17; }
	inline FsmQuaternion_t4259038327 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmQuaternion_t4259038327 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmQuaternion_t2442002114, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMQUATERNION_T2442002114_H
#ifndef SETFSMRECT_T1186024309_H
#define SETFSMRECT_T1186024309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmRect
struct  SetFsmRect_t1186024309  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmRect::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmRect::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmRect::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetFsmRect::setValue
	FsmRect_t3649179877 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmRect::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmRect::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmRect::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmRect::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___setValue_17)); }
	inline FsmRect_t3649179877 * get_setValue_17() const { return ___setValue_17; }
	inline FsmRect_t3649179877 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmRect_t3649179877 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmRect_t1186024309, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMRECT_T1186024309_H
#ifndef SETFSMSTRING_T1900776557_H
#define SETFSMSTRING_T1900776557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmString
struct  SetFsmString_t1900776557  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmString::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmString::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmString::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmString::setValue
	FsmString_t1785915204 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmString::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmString::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmString::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmString::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___setValue_17)); }
	inline FsmString_t1785915204 * get_setValue_17() const { return ___setValue_17; }
	inline FsmString_t1785915204 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmString_t1785915204 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmString_t1900776557, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMSTRING_T1900776557_H
#ifndef SETFSMTEXTURE_T1298060854_H
#define SETFSMTEXTURE_T1298060854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmTexture
struct  SetFsmTexture_t1298060854  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmTexture::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmTexture::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmTexture::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetFsmTexture::setValue
	FsmTexture_t1738787045 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmTexture::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmTexture::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmTexture::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmTexture::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___setValue_17)); }
	inline FsmTexture_t1738787045 * get_setValue_17() const { return ___setValue_17; }
	inline FsmTexture_t1738787045 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmTexture_t1738787045 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmTexture_t1298060854, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMTEXTURE_T1298060854_H
#ifndef SETFSMVARIABLE_T1599143709_H
#define SETFSMVARIABLE_T1599143709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmVariable
struct  SetFsmVariable_t1599143709  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmVariable::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVariable::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVariable::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.SetFsmVariable::setValue
	FsmVar_t2413660594 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmVariable::everyFrame
	bool ___everyFrame_18;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmVariable::targetFsm
	PlayMakerFSM_t1613010231 * ___targetFsm_19;
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.Actions.SetFsmVariable::targetVariable
	NamedVariable_t2808389578 * ___targetVariable_20;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmVariable::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_21;
	// System.String HutongGames.PlayMaker.Actions.SetFsmVariable::cachedFsmName
	String_t* ___cachedFsmName_22;
	// System.String HutongGames.PlayMaker.Actions.SetFsmVariable::cachedVariableName
	String_t* ___cachedVariableName_23;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___setValue_17)); }
	inline FsmVar_t2413660594 * get_setValue_17() const { return ___setValue_17; }
	inline FsmVar_t2413660594 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmVar_t2413660594 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_targetFsm_19() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___targetFsm_19)); }
	inline PlayMakerFSM_t1613010231 * get_targetFsm_19() const { return ___targetFsm_19; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_targetFsm_19() { return &___targetFsm_19; }
	inline void set_targetFsm_19(PlayMakerFSM_t1613010231 * value)
	{
		___targetFsm_19 = value;
		Il2CppCodeGenWriteBarrier((&___targetFsm_19), value);
	}

	inline static int32_t get_offset_of_targetVariable_20() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___targetVariable_20)); }
	inline NamedVariable_t2808389578 * get_targetVariable_20() const { return ___targetVariable_20; }
	inline NamedVariable_t2808389578 ** get_address_of_targetVariable_20() { return &___targetVariable_20; }
	inline void set_targetVariable_20(NamedVariable_t2808389578 * value)
	{
		___targetVariable_20 = value;
		Il2CppCodeGenWriteBarrier((&___targetVariable_20), value);
	}

	inline static int32_t get_offset_of_cachedGameObject_21() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___cachedGameObject_21)); }
	inline GameObject_t1113636619 * get_cachedGameObject_21() const { return ___cachedGameObject_21; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_21() { return &___cachedGameObject_21; }
	inline void set_cachedGameObject_21(GameObject_t1113636619 * value)
	{
		___cachedGameObject_21 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_21), value);
	}

	inline static int32_t get_offset_of_cachedFsmName_22() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___cachedFsmName_22)); }
	inline String_t* get_cachedFsmName_22() const { return ___cachedFsmName_22; }
	inline String_t** get_address_of_cachedFsmName_22() { return &___cachedFsmName_22; }
	inline void set_cachedFsmName_22(String_t* value)
	{
		___cachedFsmName_22 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFsmName_22), value);
	}

	inline static int32_t get_offset_of_cachedVariableName_23() { return static_cast<int32_t>(offsetof(SetFsmVariable_t1599143709, ___cachedVariableName_23)); }
	inline String_t* get_cachedVariableName_23() const { return ___cachedVariableName_23; }
	inline String_t** get_address_of_cachedVariableName_23() { return &___cachedVariableName_23; }
	inline void set_cachedVariableName_23(String_t* value)
	{
		___cachedVariableName_23 = value;
		Il2CppCodeGenWriteBarrier((&___cachedVariableName_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMVARIABLE_T1599143709_H
#ifndef SETFSMVECTOR2_T3415240114_H
#define SETFSMVECTOR2_T3415240114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmVector2
struct  SetFsmVector2_t3415240114  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmVector2::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVector2::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVector2::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetFsmVector2::setValue
	FsmVector2_t2965096677 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmVector2::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmVector2::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmVector2::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmVector2::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___setValue_17)); }
	inline FsmVector2_t2965096677 * get_setValue_17() const { return ___setValue_17; }
	inline FsmVector2_t2965096677 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmVector2_t2965096677 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmVector2_t3415240114, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMVECTOR2_T3415240114_H
#ifndef SETFSMVECTOR3_T1849156173_H
#define SETFSMVECTOR3_T1849156173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetFsmVector3
struct  SetFsmVector3_t1849156173  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetFsmVector3::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVector3::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetFsmVector3::variableName
	FsmString_t1785915204 * ___variableName_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetFsmVector3::setValue
	FsmVector3_t626444517 * ___setValue_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetFsmVector3::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.SetFsmVector3::goLastFrame
	GameObject_t1113636619 * ___goLastFrame_19;
	// System.String HutongGames.PlayMaker.Actions.SetFsmVector3::fsmNameLastFrame
	String_t* ___fsmNameLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.SetFsmVector3::fsm
	PlayMakerFSM_t1613010231 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_variableName_16() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___variableName_16)); }
	inline FsmString_t1785915204 * get_variableName_16() const { return ___variableName_16; }
	inline FsmString_t1785915204 ** get_address_of_variableName_16() { return &___variableName_16; }
	inline void set_variableName_16(FsmString_t1785915204 * value)
	{
		___variableName_16 = value;
		Il2CppCodeGenWriteBarrier((&___variableName_16), value);
	}

	inline static int32_t get_offset_of_setValue_17() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___setValue_17)); }
	inline FsmVector3_t626444517 * get_setValue_17() const { return ___setValue_17; }
	inline FsmVector3_t626444517 ** get_address_of_setValue_17() { return &___setValue_17; }
	inline void set_setValue_17(FsmVector3_t626444517 * value)
	{
		___setValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___setValue_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_goLastFrame_19() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___goLastFrame_19)); }
	inline GameObject_t1113636619 * get_goLastFrame_19() const { return ___goLastFrame_19; }
	inline GameObject_t1113636619 ** get_address_of_goLastFrame_19() { return &___goLastFrame_19; }
	inline void set_goLastFrame_19(GameObject_t1113636619 * value)
	{
		___goLastFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___goLastFrame_19), value);
	}

	inline static int32_t get_offset_of_fsmNameLastFrame_20() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___fsmNameLastFrame_20)); }
	inline String_t* get_fsmNameLastFrame_20() const { return ___fsmNameLastFrame_20; }
	inline String_t** get_address_of_fsmNameLastFrame_20() { return &___fsmNameLastFrame_20; }
	inline void set_fsmNameLastFrame_20(String_t* value)
	{
		___fsmNameLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsmNameLastFrame_20), value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(SetFsmVector3_t1849156173, ___fsm_21)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETFSMVECTOR3_T1849156173_H
#ifndef SETSTRINGVALUE_T1748950574_H
#define SETSTRINGVALUE_T1748950574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetStringValue
struct  SetStringValue_t1748950574  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetStringValue::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetStringValue::stringValue
	FsmString_t1785915204 * ___stringValue_15;
	// System.Boolean HutongGames.PlayMaker.Actions.SetStringValue::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(SetStringValue_t1748950574, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_stringValue_15() { return static_cast<int32_t>(offsetof(SetStringValue_t1748950574, ___stringValue_15)); }
	inline FsmString_t1785915204 * get_stringValue_15() const { return ___stringValue_15; }
	inline FsmString_t1785915204 ** get_address_of_stringValue_15() { return &___stringValue_15; }
	inline void set_stringValue_15(FsmString_t1785915204 * value)
	{
		___stringValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(SetStringValue_t1748950574, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETSTRINGVALUE_T1748950574_H
#ifndef STARTCOROUTINE_T4019136673_H
#define STARTCOROUTINE_T4019136673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StartCoroutine
struct  StartCoroutine_t4019136673  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.StartCoroutine::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StartCoroutine::behaviour
	FsmString_t1785915204 * ___behaviour_15;
	// HutongGames.PlayMaker.FunctionCall HutongGames.PlayMaker.Actions.StartCoroutine::functionCall
	FunctionCall_t1722713284 * ___functionCall_16;
	// System.Boolean HutongGames.PlayMaker.Actions.StartCoroutine::stopOnExit
	bool ___stopOnExit_17;
	// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Actions.StartCoroutine::component
	MonoBehaviour_t3962482529 * ___component_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(StartCoroutine_t4019136673, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_behaviour_15() { return static_cast<int32_t>(offsetof(StartCoroutine_t4019136673, ___behaviour_15)); }
	inline FsmString_t1785915204 * get_behaviour_15() const { return ___behaviour_15; }
	inline FsmString_t1785915204 ** get_address_of_behaviour_15() { return &___behaviour_15; }
	inline void set_behaviour_15(FsmString_t1785915204 * value)
	{
		___behaviour_15 = value;
		Il2CppCodeGenWriteBarrier((&___behaviour_15), value);
	}

	inline static int32_t get_offset_of_functionCall_16() { return static_cast<int32_t>(offsetof(StartCoroutine_t4019136673, ___functionCall_16)); }
	inline FunctionCall_t1722713284 * get_functionCall_16() const { return ___functionCall_16; }
	inline FunctionCall_t1722713284 ** get_address_of_functionCall_16() { return &___functionCall_16; }
	inline void set_functionCall_16(FunctionCall_t1722713284 * value)
	{
		___functionCall_16 = value;
		Il2CppCodeGenWriteBarrier((&___functionCall_16), value);
	}

	inline static int32_t get_offset_of_stopOnExit_17() { return static_cast<int32_t>(offsetof(StartCoroutine_t4019136673, ___stopOnExit_17)); }
	inline bool get_stopOnExit_17() const { return ___stopOnExit_17; }
	inline bool* get_address_of_stopOnExit_17() { return &___stopOnExit_17; }
	inline void set_stopOnExit_17(bool value)
	{
		___stopOnExit_17 = value;
	}

	inline static int32_t get_offset_of_component_18() { return static_cast<int32_t>(offsetof(StartCoroutine_t4019136673, ___component_18)); }
	inline MonoBehaviour_t3962482529 * get_component_18() const { return ___component_18; }
	inline MonoBehaviour_t3962482529 ** get_address_of_component_18() { return &___component_18; }
	inline void set_component_18(MonoBehaviour_t3962482529 * value)
	{
		___component_18 = value;
		Il2CppCodeGenWriteBarrier((&___component_18), value);
	}
};

struct StartCoroutine_t4019136673_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> HutongGames.PlayMaker.Actions.StartCoroutine::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_19() { return static_cast<int32_t>(offsetof(StartCoroutine_t4019136673_StaticFields, ___U3CU3Ef__switchU24map1_19)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_19() const { return ___U3CU3Ef__switchU24map1_19; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_19() { return &___U3CU3Ef__switchU24map1_19; }
	inline void set_U3CU3Ef__switchU24map1_19(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTCOROUTINE_T4019136673_H
#ifndef STRINGAPPEND_T1745038038_H
#define STRINGAPPEND_T1745038038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringAppend
struct  StringAppend_t1745038038  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringAppend::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringAppend::appendString
	FsmString_t1785915204 * ___appendString_15;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(StringAppend_t1745038038, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_appendString_15() { return static_cast<int32_t>(offsetof(StringAppend_t1745038038, ___appendString_15)); }
	inline FsmString_t1785915204 * get_appendString_15() const { return ___appendString_15; }
	inline FsmString_t1785915204 ** get_address_of_appendString_15() { return &___appendString_15; }
	inline void set_appendString_15(FsmString_t1785915204 * value)
	{
		___appendString_15 = value;
		Il2CppCodeGenWriteBarrier((&___appendString_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGAPPEND_T1745038038_H
#ifndef STRINGJOIN_T844195649_H
#define STRINGJOIN_T844195649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringJoin
struct  StringJoin_t844195649  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.StringJoin::stringArray
	FsmArray_t1756862219 * ___stringArray_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringJoin::separator
	FsmString_t1785915204 * ___separator_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringJoin::storeResult
	FsmString_t1785915204 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_stringArray_14() { return static_cast<int32_t>(offsetof(StringJoin_t844195649, ___stringArray_14)); }
	inline FsmArray_t1756862219 * get_stringArray_14() const { return ___stringArray_14; }
	inline FsmArray_t1756862219 ** get_address_of_stringArray_14() { return &___stringArray_14; }
	inline void set_stringArray_14(FsmArray_t1756862219 * value)
	{
		___stringArray_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringArray_14), value);
	}

	inline static int32_t get_offset_of_separator_15() { return static_cast<int32_t>(offsetof(StringJoin_t844195649, ___separator_15)); }
	inline FsmString_t1785915204 * get_separator_15() const { return ___separator_15; }
	inline FsmString_t1785915204 ** get_address_of_separator_15() { return &___separator_15; }
	inline void set_separator_15(FsmString_t1785915204 * value)
	{
		___separator_15 = value;
		Il2CppCodeGenWriteBarrier((&___separator_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(StringJoin_t844195649, ___storeResult_16)); }
	inline FsmString_t1785915204 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmString_t1785915204 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGJOIN_T844195649_H
#ifndef STRINGREPLACE_T2203809641_H
#define STRINGREPLACE_T2203809641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringReplace
struct  StringReplace_t2203809641  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringReplace::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringReplace::replace
	FsmString_t1785915204 * ___replace_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringReplace::with
	FsmString_t1785915204 * ___with_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringReplace::storeResult
	FsmString_t1785915204 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.StringReplace::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(StringReplace_t2203809641, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_replace_15() { return static_cast<int32_t>(offsetof(StringReplace_t2203809641, ___replace_15)); }
	inline FsmString_t1785915204 * get_replace_15() const { return ___replace_15; }
	inline FsmString_t1785915204 ** get_address_of_replace_15() { return &___replace_15; }
	inline void set_replace_15(FsmString_t1785915204 * value)
	{
		___replace_15 = value;
		Il2CppCodeGenWriteBarrier((&___replace_15), value);
	}

	inline static int32_t get_offset_of_with_16() { return static_cast<int32_t>(offsetof(StringReplace_t2203809641, ___with_16)); }
	inline FsmString_t1785915204 * get_with_16() const { return ___with_16; }
	inline FsmString_t1785915204 ** get_address_of_with_16() { return &___with_16; }
	inline void set_with_16(FsmString_t1785915204 * value)
	{
		___with_16 = value;
		Il2CppCodeGenWriteBarrier((&___with_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(StringReplace_t2203809641, ___storeResult_17)); }
	inline FsmString_t1785915204 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmString_t1785915204 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmString_t1785915204 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(StringReplace_t2203809641, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREPLACE_T2203809641_H
#ifndef STRINGSPLIT_T4020258998_H
#define STRINGSPLIT_T4020258998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringSplit
struct  StringSplit_t4020258998  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringSplit::stringToSplit
	FsmString_t1785915204 * ___stringToSplit_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringSplit::separators
	FsmString_t1785915204 * ___separators_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StringSplit::trimStrings
	FsmBool_t163807967 * ___trimStrings_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringSplit::trimChars
	FsmString_t1785915204 * ___trimChars_17;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.StringSplit::stringArray
	FsmArray_t1756862219 * ___stringArray_18;

public:
	inline static int32_t get_offset_of_stringToSplit_14() { return static_cast<int32_t>(offsetof(StringSplit_t4020258998, ___stringToSplit_14)); }
	inline FsmString_t1785915204 * get_stringToSplit_14() const { return ___stringToSplit_14; }
	inline FsmString_t1785915204 ** get_address_of_stringToSplit_14() { return &___stringToSplit_14; }
	inline void set_stringToSplit_14(FsmString_t1785915204 * value)
	{
		___stringToSplit_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringToSplit_14), value);
	}

	inline static int32_t get_offset_of_separators_15() { return static_cast<int32_t>(offsetof(StringSplit_t4020258998, ___separators_15)); }
	inline FsmString_t1785915204 * get_separators_15() const { return ___separators_15; }
	inline FsmString_t1785915204 ** get_address_of_separators_15() { return &___separators_15; }
	inline void set_separators_15(FsmString_t1785915204 * value)
	{
		___separators_15 = value;
		Il2CppCodeGenWriteBarrier((&___separators_15), value);
	}

	inline static int32_t get_offset_of_trimStrings_16() { return static_cast<int32_t>(offsetof(StringSplit_t4020258998, ___trimStrings_16)); }
	inline FsmBool_t163807967 * get_trimStrings_16() const { return ___trimStrings_16; }
	inline FsmBool_t163807967 ** get_address_of_trimStrings_16() { return &___trimStrings_16; }
	inline void set_trimStrings_16(FsmBool_t163807967 * value)
	{
		___trimStrings_16 = value;
		Il2CppCodeGenWriteBarrier((&___trimStrings_16), value);
	}

	inline static int32_t get_offset_of_trimChars_17() { return static_cast<int32_t>(offsetof(StringSplit_t4020258998, ___trimChars_17)); }
	inline FsmString_t1785915204 * get_trimChars_17() const { return ___trimChars_17; }
	inline FsmString_t1785915204 ** get_address_of_trimChars_17() { return &___trimChars_17; }
	inline void set_trimChars_17(FsmString_t1785915204 * value)
	{
		___trimChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___trimChars_17), value);
	}

	inline static int32_t get_offset_of_stringArray_18() { return static_cast<int32_t>(offsetof(StringSplit_t4020258998, ___stringArray_18)); }
	inline FsmArray_t1756862219 * get_stringArray_18() const { return ___stringArray_18; }
	inline FsmArray_t1756862219 ** get_address_of_stringArray_18() { return &___stringArray_18; }
	inline void set_stringArray_18(FsmArray_t1756862219 * value)
	{
		___stringArray_18 = value;
		Il2CppCodeGenWriteBarrier((&___stringArray_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSPLIT_T4020258998_H
#ifndef WAIT_T2864591912_H
#define WAIT_T2864591912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Wait
struct  Wait_t2864591912  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Wait::time
	FsmFloat_t2883254149 * ___time_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Wait::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_15;
	// System.Boolean HutongGames.PlayMaker.Actions.Wait::realTime
	bool ___realTime_16;
	// System.Single HutongGames.PlayMaker.Actions.Wait::startTime
	float ___startTime_17;
	// System.Single HutongGames.PlayMaker.Actions.Wait::timer
	float ___timer_18;

public:
	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(Wait_t2864591912, ___time_14)); }
	inline FsmFloat_t2883254149 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t2883254149 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t2883254149 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier((&___time_14), value);
	}

	inline static int32_t get_offset_of_finishEvent_15() { return static_cast<int32_t>(offsetof(Wait_t2864591912, ___finishEvent_15)); }
	inline FsmEvent_t3736299882 * get_finishEvent_15() const { return ___finishEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_15() { return &___finishEvent_15; }
	inline void set_finishEvent_15(FsmEvent_t3736299882 * value)
	{
		___finishEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_15), value);
	}

	inline static int32_t get_offset_of_realTime_16() { return static_cast<int32_t>(offsetof(Wait_t2864591912, ___realTime_16)); }
	inline bool get_realTime_16() const { return ___realTime_16; }
	inline bool* get_address_of_realTime_16() { return &___realTime_16; }
	inline void set_realTime_16(bool value)
	{
		___realTime_16 = value;
	}

	inline static int32_t get_offset_of_startTime_17() { return static_cast<int32_t>(offsetof(Wait_t2864591912, ___startTime_17)); }
	inline float get_startTime_17() const { return ___startTime_17; }
	inline float* get_address_of_startTime_17() { return &___startTime_17; }
	inline void set_startTime_17(float value)
	{
		___startTime_17 = value;
	}

	inline static int32_t get_offset_of_timer_18() { return static_cast<int32_t>(offsetof(Wait_t2864591912, ___timer_18)); }
	inline float get_timer_18() const { return ___timer_18; }
	inline float* get_address_of_timer_18() { return &___timer_18; }
	inline void set_timer_18(float value)
	{
		___timer_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAIT_T2864591912_H
#ifndef MOVEOBJECT_T491415109_H
#define MOVEOBJECT_T491415109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MoveObject
struct  MoveObject_t491415109  : public EaseFsmAction_t2585076379
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MoveObject::objectToMove
	FsmOwnerDefault_t3590610434 * ___objectToMove_35;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.MoveObject::destination
	FsmGameObject_t3581898942 * ___destination_36;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveObject::fromValue
	FsmVector3_t626444517 * ___fromValue_37;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveObject::toVector
	FsmVector3_t626444517 * ___toVector_38;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MoveObject::fromVector
	FsmVector3_t626444517 * ___fromVector_39;
	// System.Boolean HutongGames.PlayMaker.Actions.MoveObject::finishInNextStep
	bool ___finishInNextStep_40;

public:
	inline static int32_t get_offset_of_objectToMove_35() { return static_cast<int32_t>(offsetof(MoveObject_t491415109, ___objectToMove_35)); }
	inline FsmOwnerDefault_t3590610434 * get_objectToMove_35() const { return ___objectToMove_35; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_objectToMove_35() { return &___objectToMove_35; }
	inline void set_objectToMove_35(FsmOwnerDefault_t3590610434 * value)
	{
		___objectToMove_35 = value;
		Il2CppCodeGenWriteBarrier((&___objectToMove_35), value);
	}

	inline static int32_t get_offset_of_destination_36() { return static_cast<int32_t>(offsetof(MoveObject_t491415109, ___destination_36)); }
	inline FsmGameObject_t3581898942 * get_destination_36() const { return ___destination_36; }
	inline FsmGameObject_t3581898942 ** get_address_of_destination_36() { return &___destination_36; }
	inline void set_destination_36(FsmGameObject_t3581898942 * value)
	{
		___destination_36 = value;
		Il2CppCodeGenWriteBarrier((&___destination_36), value);
	}

	inline static int32_t get_offset_of_fromValue_37() { return static_cast<int32_t>(offsetof(MoveObject_t491415109, ___fromValue_37)); }
	inline FsmVector3_t626444517 * get_fromValue_37() const { return ___fromValue_37; }
	inline FsmVector3_t626444517 ** get_address_of_fromValue_37() { return &___fromValue_37; }
	inline void set_fromValue_37(FsmVector3_t626444517 * value)
	{
		___fromValue_37 = value;
		Il2CppCodeGenWriteBarrier((&___fromValue_37), value);
	}

	inline static int32_t get_offset_of_toVector_38() { return static_cast<int32_t>(offsetof(MoveObject_t491415109, ___toVector_38)); }
	inline FsmVector3_t626444517 * get_toVector_38() const { return ___toVector_38; }
	inline FsmVector3_t626444517 ** get_address_of_toVector_38() { return &___toVector_38; }
	inline void set_toVector_38(FsmVector3_t626444517 * value)
	{
		___toVector_38 = value;
		Il2CppCodeGenWriteBarrier((&___toVector_38), value);
	}

	inline static int32_t get_offset_of_fromVector_39() { return static_cast<int32_t>(offsetof(MoveObject_t491415109, ___fromVector_39)); }
	inline FsmVector3_t626444517 * get_fromVector_39() const { return ___fromVector_39; }
	inline FsmVector3_t626444517 ** get_address_of_fromVector_39() { return &___fromVector_39; }
	inline void set_fromVector_39(FsmVector3_t626444517 * value)
	{
		___fromVector_39 = value;
		Il2CppCodeGenWriteBarrier((&___fromVector_39), value);
	}

	inline static int32_t get_offset_of_finishInNextStep_40() { return static_cast<int32_t>(offsetof(MoveObject_t491415109, ___finishInNextStep_40)); }
	inline bool get_finishInNextStep_40() const { return ___finishInNextStep_40; }
	inline bool* get_address_of_finishInNextStep_40() { return &___finishInNextStep_40; }
	inline void set_finishInNextStep_40(bool value)
	{
		___finishInNextStep_40 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEOBJECT_T491415109_H
#ifndef RUNFSM_T590382048_H
#define RUNFSM_T590382048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RunFSM
struct  RunFSM_t590382048  : public RunFSMAction_t3423579716
{
public:
	// HutongGames.PlayMaker.FsmTemplateControl HutongGames.PlayMaker.Actions.RunFSM::fsmTemplateControl
	FsmTemplateControl_t522200103 * ___fsmTemplateControl_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RunFSM::storeID
	FsmInt_t874273141 * ___storeID_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RunFSM::finishEvent
	FsmEvent_t3736299882 * ___finishEvent_17;

public:
	inline static int32_t get_offset_of_fsmTemplateControl_15() { return static_cast<int32_t>(offsetof(RunFSM_t590382048, ___fsmTemplateControl_15)); }
	inline FsmTemplateControl_t522200103 * get_fsmTemplateControl_15() const { return ___fsmTemplateControl_15; }
	inline FsmTemplateControl_t522200103 ** get_address_of_fsmTemplateControl_15() { return &___fsmTemplateControl_15; }
	inline void set_fsmTemplateControl_15(FsmTemplateControl_t522200103 * value)
	{
		___fsmTemplateControl_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmTemplateControl_15), value);
	}

	inline static int32_t get_offset_of_storeID_16() { return static_cast<int32_t>(offsetof(RunFSM_t590382048, ___storeID_16)); }
	inline FsmInt_t874273141 * get_storeID_16() const { return ___storeID_16; }
	inline FsmInt_t874273141 ** get_address_of_storeID_16() { return &___storeID_16; }
	inline void set_storeID_16(FsmInt_t874273141 * value)
	{
		___storeID_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeID_16), value);
	}

	inline static int32_t get_offset_of_finishEvent_17() { return static_cast<int32_t>(offsetof(RunFSM_t590382048, ___finishEvent_17)); }
	inline FsmEvent_t3736299882 * get_finishEvent_17() const { return ___finishEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_finishEvent_17() { return &___finishEvent_17; }
	inline void set_finishEvent_17(FsmEvent_t3736299882 * value)
	{
		___finishEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___finishEvent_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNFSM_T590382048_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (AddScript_t769303531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[4] = 
{
	AddScript_t769303531::get_offset_of_gameObject_14(),
	AddScript_t769303531::get_offset_of_script_15(),
	AddScript_t769303531::get_offset_of_removeOnExit_16(),
	AddScript_t769303531::get_offset_of_addedComponent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (AxisEvent_t353447656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[8] = 
{
	AxisEvent_t353447656::get_offset_of_horizontalAxis_14(),
	AxisEvent_t353447656::get_offset_of_verticalAxis_15(),
	AxisEvent_t353447656::get_offset_of_leftEvent_16(),
	AxisEvent_t353447656::get_offset_of_rightEvent_17(),
	AxisEvent_t353447656::get_offset_of_upEvent_18(),
	AxisEvent_t353447656::get_offset_of_downEvent_19(),
	AxisEvent_t353447656::get_offset_of_anyDirection_20(),
	AxisEvent_t353447656::get_offset_of_noDirection_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (CallMethod_t2402822853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[13] = 
{
	CallMethod_t2402822853::get_offset_of_behaviour_14(),
	CallMethod_t2402822853::get_offset_of_methodName_15(),
	CallMethod_t2402822853::get_offset_of_parameters_16(),
	CallMethod_t2402822853::get_offset_of_storeResult_17(),
	CallMethod_t2402822853::get_offset_of_everyFrame_18(),
	CallMethod_t2402822853::get_offset_of_manualUI_19(),
	CallMethod_t2402822853::get_offset_of_cachedBehaviour_20(),
	CallMethod_t2402822853::get_offset_of_cachedMethodName_21(),
	CallMethod_t2402822853::get_offset_of_cachedType_22(),
	CallMethod_t2402822853::get_offset_of_cachedMethodInfo_23(),
	CallMethod_t2402822853::get_offset_of_cachedParameterInfo_24(),
	CallMethod_t2402822853::get_offset_of_parametersArray_25(),
	CallMethod_t2402822853::get_offset_of_errorString_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (CallStaticMethod_t3442263793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3603[12] = 
{
	CallStaticMethod_t3442263793::get_offset_of_className_14(),
	CallStaticMethod_t3442263793::get_offset_of_methodName_15(),
	CallStaticMethod_t3442263793::get_offset_of_parameters_16(),
	CallStaticMethod_t3442263793::get_offset_of_storeResult_17(),
	CallStaticMethod_t3442263793::get_offset_of_everyFrame_18(),
	CallStaticMethod_t3442263793::get_offset_of_cachedType_19(),
	CallStaticMethod_t3442263793::get_offset_of_cachedClassName_20(),
	CallStaticMethod_t3442263793::get_offset_of_cachedMethodName_21(),
	CallStaticMethod_t3442263793::get_offset_of_cachedMethodInfo_22(),
	CallStaticMethod_t3442263793::get_offset_of_cachedParameterInfo_23(),
	CallStaticMethod_t3442263793::get_offset_of_parametersArray_24(),
	CallStaticMethod_t3442263793::get_offset_of_errorString_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (EnableBehaviour_t124468037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3604[6] = 
{
	EnableBehaviour_t124468037::get_offset_of_gameObject_14(),
	EnableBehaviour_t124468037::get_offset_of_behaviour_15(),
	EnableBehaviour_t124468037::get_offset_of_component_16(),
	EnableBehaviour_t124468037::get_offset_of_enable_17(),
	EnableBehaviour_t124468037::get_offset_of_resetOnExit_18(),
	EnableBehaviour_t124468037::get_offset_of_componentTarget_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (InvokeMethod_t124369765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[8] = 
{
	InvokeMethod_t124369765::get_offset_of_gameObject_14(),
	InvokeMethod_t124369765::get_offset_of_behaviour_15(),
	InvokeMethod_t124369765::get_offset_of_methodName_16(),
	InvokeMethod_t124369765::get_offset_of_delay_17(),
	InvokeMethod_t124369765::get_offset_of_repeating_18(),
	InvokeMethod_t124369765::get_offset_of_repeatDelay_19(),
	InvokeMethod_t124369765::get_offset_of_cancelOnExit_20(),
	InvokeMethod_t124369765::get_offset_of_component_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (SendMessage_t1630967858), -1, sizeof(SendMessage_t1630967858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3606[5] = 
{
	SendMessage_t1630967858::get_offset_of_gameObject_14(),
	SendMessage_t1630967858::get_offset_of_delivery_15(),
	SendMessage_t1630967858::get_offset_of_options_16(),
	SendMessage_t1630967858::get_offset_of_functionCall_17(),
	SendMessage_t1630967858_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (MessageType_t2684925397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3607[4] = 
{
	MessageType_t2684925397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (StartCoroutine_t4019136673), -1, sizeof(StartCoroutine_t4019136673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3608[6] = 
{
	StartCoroutine_t4019136673::get_offset_of_gameObject_14(),
	StartCoroutine_t4019136673::get_offset_of_behaviour_15(),
	StartCoroutine_t4019136673::get_offset_of_functionCall_16(),
	StartCoroutine_t4019136673::get_offset_of_stopOnExit_17(),
	StartCoroutine_t4019136673::get_offset_of_component_18(),
	StartCoroutine_t4019136673_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (BaseFsmVariableAction_t2713817827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3609[5] = 
{
	BaseFsmVariableAction_t2713817827::get_offset_of_fsmNotFound_14(),
	BaseFsmVariableAction_t2713817827::get_offset_of_variableNotFound_15(),
	BaseFsmVariableAction_t2713817827::get_offset_of_cachedGameObject_16(),
	BaseFsmVariableAction_t2713817827::get_offset_of_cachedFsmName_17(),
	BaseFsmVariableAction_t2713817827::get_offset_of_fsm_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (BaseFsmVariableIndexAction_t3448140877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3610[6] = 
{
	BaseFsmVariableIndexAction_t3448140877::get_offset_of_indexOutOfRange_14(),
	BaseFsmVariableIndexAction_t3448140877::get_offset_of_fsmNotFound_15(),
	BaseFsmVariableIndexAction_t3448140877::get_offset_of_variableNotFound_16(),
	BaseFsmVariableIndexAction_t3448140877::get_offset_of_cachedGameObject_17(),
	BaseFsmVariableIndexAction_t3448140877::get_offset_of_cachedFsmName_18(),
	BaseFsmVariableIndexAction_t3448140877::get_offset_of_fsm_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (BroadcastEvent_t1114459934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3611[4] = 
{
	BroadcastEvent_t1114459934::get_offset_of_broadcastEvent_14(),
	BroadcastEvent_t1114459934::get_offset_of_gameObject_15(),
	BroadcastEvent_t1114459934::get_offset_of_sendToChildren_16(),
	BroadcastEvent_t1114459934::get_offset_of_excludeSelf_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (EnableFSM_t3113159649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3612[5] = 
{
	EnableFSM_t3113159649::get_offset_of_gameObject_14(),
	EnableFSM_t3113159649::get_offset_of_fsmName_15(),
	EnableFSM_t3113159649::get_offset_of_enable_16(),
	EnableFSM_t3113159649::get_offset_of_resetOnExit_17(),
	EnableFSM_t3113159649::get_offset_of_fsmComponent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (FinishFSM_t747447064), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (ForwardAllEvents_t2091258839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[3] = 
{
	ForwardAllEvents_t2091258839::get_offset_of_forwardTo_14(),
	ForwardAllEvents_t2091258839::get_offset_of_exceptThese_15(),
	ForwardAllEvents_t2091258839::get_offset_of_eatEvents_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (ForwardEvent_t1540014318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[3] = 
{
	ForwardEvent_t1540014318::get_offset_of_forwardTo_14(),
	ForwardEvent_t1540014318::get_offset_of_eventsToForward_15(),
	ForwardEvent_t1540014318::get_offset_of_eatEvents_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (GetEventBoolData_t956908412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[1] = 
{
	GetEventBoolData_t956908412::get_offset_of_getBoolData_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (GetEventFloatData_t1922157318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[1] = 
{
	GetEventFloatData_t1922157318::get_offset_of_getFloatData_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (GetEventInfo_t4153126524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3618[15] = 
{
	GetEventInfo_t4153126524::get_offset_of_sentByGameObject_14(),
	GetEventInfo_t4153126524::get_offset_of_fsmName_15(),
	GetEventInfo_t4153126524::get_offset_of_getBoolData_16(),
	GetEventInfo_t4153126524::get_offset_of_getIntData_17(),
	GetEventInfo_t4153126524::get_offset_of_getFloatData_18(),
	GetEventInfo_t4153126524::get_offset_of_getVector2Data_19(),
	GetEventInfo_t4153126524::get_offset_of_getVector3Data_20(),
	GetEventInfo_t4153126524::get_offset_of_getStringData_21(),
	GetEventInfo_t4153126524::get_offset_of_getGameObjectData_22(),
	GetEventInfo_t4153126524::get_offset_of_getRectData_23(),
	GetEventInfo_t4153126524::get_offset_of_getQuaternionData_24(),
	GetEventInfo_t4153126524::get_offset_of_getMaterialData_25(),
	GetEventInfo_t4153126524::get_offset_of_getTextureData_26(),
	GetEventInfo_t4153126524::get_offset_of_getColorData_27(),
	GetEventInfo_t4153126524::get_offset_of_getObjectData_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (GetEventIntData_t4132126548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3619[1] = 
{
	GetEventIntData_t4132126548::get_offset_of_getIntData_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (GetEventSentBy_t154290135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[3] = 
{
	GetEventSentBy_t154290135::get_offset_of_sentByGameObject_14(),
	GetEventSentBy_t154290135::get_offset_of_gameObjectName_15(),
	GetEventSentBy_t154290135::get_offset_of_fsmName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (GetEventStringData_t1465947128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[1] = 
{
	GetEventStringData_t1465947128::get_offset_of_getStringData_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (GetEventVector2Data_t369842387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[1] = 
{
	GetEventVector2Data_t369842387::get_offset_of_getVector2Data_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (GetEventVector3Data_t369842354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[1] = 
{
	GetEventVector3Data_t369842354::get_offset_of_getVector3Data_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (GetFsmBool_t2453160037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[7] = 
{
	GetFsmBool_t2453160037::get_offset_of_gameObject_14(),
	GetFsmBool_t2453160037::get_offset_of_fsmName_15(),
	GetFsmBool_t2453160037::get_offset_of_variableName_16(),
	GetFsmBool_t2453160037::get_offset_of_storeValue_17(),
	GetFsmBool_t2453160037::get_offset_of_everyFrame_18(),
	GetFsmBool_t2453160037::get_offset_of_goLastFrame_19(),
	GetFsmBool_t2453160037::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (GetFsmColor_t803516999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[7] = 
{
	GetFsmColor_t803516999::get_offset_of_gameObject_14(),
	GetFsmColor_t803516999::get_offset_of_fsmName_15(),
	GetFsmColor_t803516999::get_offset_of_variableName_16(),
	GetFsmColor_t803516999::get_offset_of_storeValue_17(),
	GetFsmColor_t803516999::get_offset_of_everyFrame_18(),
	GetFsmColor_t803516999::get_offset_of_goLastFrame_19(),
	GetFsmColor_t803516999::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (GetFsmEnum_t921011402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[7] = 
{
	GetFsmEnum_t921011402::get_offset_of_gameObject_14(),
	GetFsmEnum_t921011402::get_offset_of_fsmName_15(),
	GetFsmEnum_t921011402::get_offset_of_variableName_16(),
	GetFsmEnum_t921011402::get_offset_of_storeValue_17(),
	GetFsmEnum_t921011402::get_offset_of_everyFrame_18(),
	GetFsmEnum_t921011402::get_offset_of_goLastFrame_19(),
	GetFsmEnum_t921011402::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (GetFsmFloat_t817976599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3627[7] = 
{
	GetFsmFloat_t817976599::get_offset_of_gameObject_14(),
	GetFsmFloat_t817976599::get_offset_of_fsmName_15(),
	GetFsmFloat_t817976599::get_offset_of_variableName_16(),
	GetFsmFloat_t817976599::get_offset_of_storeValue_17(),
	GetFsmFloat_t817976599::get_offset_of_everyFrame_18(),
	GetFsmFloat_t817976599::get_offset_of_goLastFrame_19(),
	GetFsmFloat_t817976599::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (GetFsmGameObject_t2251453113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3628[7] = 
{
	GetFsmGameObject_t2251453113::get_offset_of_gameObject_14(),
	GetFsmGameObject_t2251453113::get_offset_of_fsmName_15(),
	GetFsmGameObject_t2251453113::get_offset_of_variableName_16(),
	GetFsmGameObject_t2251453113::get_offset_of_storeValue_17(),
	GetFsmGameObject_t2251453113::get_offset_of_everyFrame_18(),
	GetFsmGameObject_t2251453113::get_offset_of_goLastFrame_19(),
	GetFsmGameObject_t2251453113::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (GetFsmInt_t3401447785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[7] = 
{
	GetFsmInt_t3401447785::get_offset_of_gameObject_14(),
	GetFsmInt_t3401447785::get_offset_of_fsmName_15(),
	GetFsmInt_t3401447785::get_offset_of_variableName_16(),
	GetFsmInt_t3401447785::get_offset_of_storeValue_17(),
	GetFsmInt_t3401447785::get_offset_of_everyFrame_18(),
	GetFsmInt_t3401447785::get_offset_of_goLastFrame_19(),
	GetFsmInt_t3401447785::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (GetFsmMaterial_t2481100960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[7] = 
{
	GetFsmMaterial_t2481100960::get_offset_of_gameObject_14(),
	GetFsmMaterial_t2481100960::get_offset_of_fsmName_15(),
	GetFsmMaterial_t2481100960::get_offset_of_variableName_16(),
	GetFsmMaterial_t2481100960::get_offset_of_storeValue_17(),
	GetFsmMaterial_t2481100960::get_offset_of_everyFrame_18(),
	GetFsmMaterial_t2481100960::get_offset_of_goLastFrame_19(),
	GetFsmMaterial_t2481100960::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (GetFsmObject_t783588600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[7] = 
{
	GetFsmObject_t783588600::get_offset_of_gameObject_14(),
	GetFsmObject_t783588600::get_offset_of_fsmName_15(),
	GetFsmObject_t783588600::get_offset_of_variableName_16(),
	GetFsmObject_t783588600::get_offset_of_storeValue_17(),
	GetFsmObject_t783588600::get_offset_of_everyFrame_18(),
	GetFsmObject_t783588600::get_offset_of_goLastFrame_19(),
	GetFsmObject_t783588600::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (GetFsmQuaternion_t511116542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[7] = 
{
	GetFsmQuaternion_t511116542::get_offset_of_gameObject_14(),
	GetFsmQuaternion_t511116542::get_offset_of_fsmName_15(),
	GetFsmQuaternion_t511116542::get_offset_of_variableName_16(),
	GetFsmQuaternion_t511116542::get_offset_of_storeValue_17(),
	GetFsmQuaternion_t511116542::get_offset_of_everyFrame_18(),
	GetFsmQuaternion_t511116542::get_offset_of_goLastFrame_19(),
	GetFsmQuaternion_t511116542::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (GetFsmRect_t2368714681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[7] = 
{
	GetFsmRect_t2368714681::get_offset_of_gameObject_14(),
	GetFsmRect_t2368714681::get_offset_of_fsmName_15(),
	GetFsmRect_t2368714681::get_offset_of_variableName_16(),
	GetFsmRect_t2368714681::get_offset_of_storeValue_17(),
	GetFsmRect_t2368714681::get_offset_of_everyFrame_18(),
	GetFsmRect_t2368714681::get_offset_of_goLastFrame_19(),
	GetFsmRect_t2368714681::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (GetFsmState_t653440757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3634[6] = 
{
	GetFsmState_t653440757::get_offset_of_fsmComponent_14(),
	GetFsmState_t653440757::get_offset_of_gameObject_15(),
	GetFsmState_t653440757::get_offset_of_fsmName_16(),
	GetFsmState_t653440757::get_offset_of_storeResult_17(),
	GetFsmState_t653440757::get_offset_of_everyFrame_18(),
	GetFsmState_t653440757::get_offset_of_fsm_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (GetFsmString_t2701129905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[7] = 
{
	GetFsmString_t2701129905::get_offset_of_gameObject_14(),
	GetFsmString_t2701129905::get_offset_of_fsmName_15(),
	GetFsmString_t2701129905::get_offset_of_variableName_16(),
	GetFsmString_t2701129905::get_offset_of_storeValue_17(),
	GetFsmString_t2701129905::get_offset_of_everyFrame_18(),
	GetFsmString_t2701129905::get_offset_of_goLastFrame_19(),
	GetFsmString_t2701129905::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (GetFsmTexture_t3574507122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3636[7] = 
{
	GetFsmTexture_t3574507122::get_offset_of_gameObject_14(),
	GetFsmTexture_t3574507122::get_offset_of_fsmName_15(),
	GetFsmTexture_t3574507122::get_offset_of_variableName_16(),
	GetFsmTexture_t3574507122::get_offset_of_storeValue_17(),
	GetFsmTexture_t3574507122::get_offset_of_everyFrame_18(),
	GetFsmTexture_t3574507122::get_offset_of_goLastFrame_19(),
	GetFsmTexture_t3574507122::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (GetFsmVariable_t1296996537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[8] = 
{
	GetFsmVariable_t1296996537::get_offset_of_gameObject_14(),
	GetFsmVariable_t1296996537::get_offset_of_fsmName_15(),
	GetFsmVariable_t1296996537::get_offset_of_storeValue_16(),
	GetFsmVariable_t1296996537::get_offset_of_everyFrame_17(),
	GetFsmVariable_t1296996537::get_offset_of_cachedGO_18(),
	GetFsmVariable_t1296996537::get_offset_of_sourceFsm_19(),
	GetFsmVariable_t1296996537::get_offset_of_sourceVariable_20(),
	GetFsmVariable_t1296996537::get_offset_of_targetVariable_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (GetFsmVariables_t1675002777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[8] = 
{
	GetFsmVariables_t1675002777::get_offset_of_gameObject_14(),
	GetFsmVariables_t1675002777::get_offset_of_fsmName_15(),
	GetFsmVariables_t1675002777::get_offset_of_getVariables_16(),
	GetFsmVariables_t1675002777::get_offset_of_everyFrame_17(),
	GetFsmVariables_t1675002777::get_offset_of_cachedGO_18(),
	GetFsmVariables_t1675002777::get_offset_of_sourceFsm_19(),
	GetFsmVariables_t1675002777::get_offset_of_sourceVariables_20(),
	GetFsmVariables_t1675002777::get_offset_of_targetVariables_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (GetFsmVector2_t3769113750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3639[7] = 
{
	GetFsmVector2_t3769113750::get_offset_of_gameObject_14(),
	GetFsmVector2_t3769113750::get_offset_of_fsmName_15(),
	GetFsmVector2_t3769113750::get_offset_of_variableName_16(),
	GetFsmVector2_t3769113750::get_offset_of_storeValue_17(),
	GetFsmVector2_t3769113750::get_offset_of_everyFrame_18(),
	GetFsmVector2_t3769113750::get_offset_of_goLastFrame_19(),
	GetFsmVector2_t3769113750::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (GetFsmVector3_t2203029809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3640[7] = 
{
	GetFsmVector3_t2203029809::get_offset_of_gameObject_14(),
	GetFsmVector3_t2203029809::get_offset_of_fsmName_15(),
	GetFsmVector3_t2203029809::get_offset_of_variableName_16(),
	GetFsmVector3_t2203029809::get_offset_of_storeValue_17(),
	GetFsmVector3_t2203029809::get_offset_of_everyFrame_18(),
	GetFsmVector3_t2203029809::get_offset_of_goLastFrame_19(),
	GetFsmVector3_t2203029809::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (GetLastEvent_t2408635592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[1] = 
{
	GetLastEvent_t2408635592::get_offset_of_storeEvent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (GetPreviousStateName_t1603361608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[1] = 
{
	GetPreviousStateName_t1603361608::get_offset_of_storeName_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (GotoPreviousState_t3673902729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (KillDelayedEvents_t3303987224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (NextFrameEvent_t736269368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[1] = 
{
	NextFrameEvent_t736269368::get_offset_of_sendEvent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (RandomEvent_t733627204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3646[5] = 
{
	RandomEvent_t733627204::get_offset_of_delay_14(),
	RandomEvent_t733627204::get_offset_of_noRepeat_15(),
	RandomEvent_t733627204::get_offset_of_delayedEvent_16(),
	RandomEvent_t733627204::get_offset_of_randomEventIndex_17(),
	RandomEvent_t733627204::get_offset_of_lastEventIndex_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (RunFSM_t590382048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3647[3] = 
{
	RunFSM_t590382048::get_offset_of_fsmTemplateControl_15(),
	RunFSM_t590382048::get_offset_of_storeID_16(),
	RunFSM_t590382048::get_offset_of_finishEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (RunFSMAction_t3423579716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3648[1] = 
{
	RunFSMAction_t3423579716::get_offset_of_runFsm_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (SendEvent_t1101542469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[5] = 
{
	SendEvent_t1101542469::get_offset_of_eventTarget_14(),
	SendEvent_t1101542469::get_offset_of_sendEvent_15(),
	SendEvent_t1101542469::get_offset_of_delay_16(),
	SendEvent_t1101542469::get_offset_of_everyFrame_17(),
	SendEvent_t1101542469::get_offset_of_delayedEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (SendEventByName_t1677051455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[5] = 
{
	SendEventByName_t1677051455::get_offset_of_eventTarget_14(),
	SendEventByName_t1677051455::get_offset_of_sendEvent_15(),
	SendEventByName_t1677051455::get_offset_of_delay_16(),
	SendEventByName_t1677051455::get_offset_of_everyFrame_17(),
	SendEventByName_t1677051455::get_offset_of_delayedEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (SendEventToFsm_t486353493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[7] = 
{
	SendEventToFsm_t486353493::get_offset_of_gameObject_14(),
	SendEventToFsm_t486353493::get_offset_of_fsmName_15(),
	SendEventToFsm_t486353493::get_offset_of_sendEvent_16(),
	SendEventToFsm_t486353493::get_offset_of_delay_17(),
	SendEventToFsm_t486353493::get_offset_of_requireReceiver_18(),
	SendEventToFsm_t486353493::get_offset_of_go_19(),
	SendEventToFsm_t486353493::get_offset_of_delayedEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (SendRandomEvent_t2238267572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[4] = 
{
	SendRandomEvent_t2238267572::get_offset_of_events_14(),
	SendRandomEvent_t2238267572::get_offset_of_weights_15(),
	SendRandomEvent_t2238267572::get_offset_of_delay_16(),
	SendRandomEvent_t2238267572::get_offset_of_delayedEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (SequenceEvent_t2748694391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[4] = 
{
	SequenceEvent_t2748694391::get_offset_of_delay_14(),
	SequenceEvent_t2748694391::get_offset_of_reset_15(),
	SequenceEvent_t2748694391::get_offset_of_delayedEvent_16(),
	SequenceEvent_t2748694391::get_offset_of_eventIndex_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (SetEventData_t4059347819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3654[13] = 
{
	SetEventData_t4059347819::get_offset_of_setGameObjectData_14(),
	SetEventData_t4059347819::get_offset_of_setIntData_15(),
	SetEventData_t4059347819::get_offset_of_setFloatData_16(),
	SetEventData_t4059347819::get_offset_of_setStringData_17(),
	SetEventData_t4059347819::get_offset_of_setBoolData_18(),
	SetEventData_t4059347819::get_offset_of_setVector2Data_19(),
	SetEventData_t4059347819::get_offset_of_setVector3Data_20(),
	SetEventData_t4059347819::get_offset_of_setRectData_21(),
	SetEventData_t4059347819::get_offset_of_setQuaternionData_22(),
	SetEventData_t4059347819::get_offset_of_setColorData_23(),
	SetEventData_t4059347819::get_offset_of_setMaterialData_24(),
	SetEventData_t4059347819::get_offset_of_setTextureData_25(),
	SetEventData_t4059347819::get_offset_of_setObjectData_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (SetEventTarget_t83275134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[1] = 
{
	SetEventTarget_t83275134::get_offset_of_eventTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (SetFsmBool_t2339160745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[8] = 
{
	SetFsmBool_t2339160745::get_offset_of_gameObject_14(),
	SetFsmBool_t2339160745::get_offset_of_fsmName_15(),
	SetFsmBool_t2339160745::get_offset_of_variableName_16(),
	SetFsmBool_t2339160745::get_offset_of_setValue_17(),
	SetFsmBool_t2339160745::get_offset_of_everyFrame_18(),
	SetFsmBool_t2339160745::get_offset_of_goLastFrame_19(),
	SetFsmBool_t2339160745::get_offset_of_fsmNameLastFrame_20(),
	SetFsmBool_t2339160745::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (SetFsmColor_t1836528779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3657[8] = 
{
	SetFsmColor_t1836528779::get_offset_of_gameObject_14(),
	SetFsmColor_t1836528779::get_offset_of_fsmName_15(),
	SetFsmColor_t1836528779::get_offset_of_variableName_16(),
	SetFsmColor_t1836528779::get_offset_of_setValue_17(),
	SetFsmColor_t1836528779::get_offset_of_everyFrame_18(),
	SetFsmColor_t1836528779::get_offset_of_goLastFrame_19(),
	SetFsmColor_t1836528779::get_offset_of_fsmNameLastFrame_20(),
	SetFsmColor_t1836528779::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (SetFsmEnum_t476888414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[8] = 
{
	SetFsmEnum_t476888414::get_offset_of_gameObject_14(),
	SetFsmEnum_t476888414::get_offset_of_fsmName_15(),
	SetFsmEnum_t476888414::get_offset_of_variableName_16(),
	SetFsmEnum_t476888414::get_offset_of_setValue_17(),
	SetFsmEnum_t476888414::get_offset_of_everyFrame_18(),
	SetFsmEnum_t476888414::get_offset_of_goLastFrame_19(),
	SetFsmEnum_t476888414::get_offset_of_fsmNameLastFrame_20(),
	SetFsmEnum_t476888414::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (SetFsmFloat_t1086314331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3659[8] = 
{
	SetFsmFloat_t1086314331::get_offset_of_gameObject_14(),
	SetFsmFloat_t1086314331::get_offset_of_fsmName_15(),
	SetFsmFloat_t1086314331::get_offset_of_variableName_16(),
	SetFsmFloat_t1086314331::get_offset_of_setValue_17(),
	SetFsmFloat_t1086314331::get_offset_of_everyFrame_18(),
	SetFsmFloat_t1086314331::get_offset_of_goLastFrame_19(),
	SetFsmFloat_t1086314331::get_offset_of_fsmNameLastFrame_20(),
	SetFsmFloat_t1086314331::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (SetFsmGameObject_t2454320077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[8] = 
{
	SetFsmGameObject_t2454320077::get_offset_of_gameObject_14(),
	SetFsmGameObject_t2454320077::get_offset_of_fsmName_15(),
	SetFsmGameObject_t2454320077::get_offset_of_variableName_16(),
	SetFsmGameObject_t2454320077::get_offset_of_setValue_17(),
	SetFsmGameObject_t2454320077::get_offset_of_everyFrame_18(),
	SetFsmGameObject_t2454320077::get_offset_of_goLastFrame_19(),
	SetFsmGameObject_t2454320077::get_offset_of_fsmNameLastFrame_20(),
	SetFsmGameObject_t2454320077::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (SetFsmInt_t1274753533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[8] = 
{
	SetFsmInt_t1274753533::get_offset_of_gameObject_14(),
	SetFsmInt_t1274753533::get_offset_of_fsmName_15(),
	SetFsmInt_t1274753533::get_offset_of_variableName_16(),
	SetFsmInt_t1274753533::get_offset_of_setValue_17(),
	SetFsmInt_t1274753533::get_offset_of_everyFrame_18(),
	SetFsmInt_t1274753533::get_offset_of_goLastFrame_19(),
	SetFsmInt_t1274753533::get_offset_of_fsmNameLastFrame_20(),
	SetFsmInt_t1274753533::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (SetFsmMaterial_t2596332468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3662[8] = 
{
	SetFsmMaterial_t2596332468::get_offset_of_gameObject_14(),
	SetFsmMaterial_t2596332468::get_offset_of_fsmName_15(),
	SetFsmMaterial_t2596332468::get_offset_of_variableName_16(),
	SetFsmMaterial_t2596332468::get_offset_of_setValue_17(),
	SetFsmMaterial_t2596332468::get_offset_of_everyFrame_18(),
	SetFsmMaterial_t2596332468::get_offset_of_goLastFrame_19(),
	SetFsmMaterial_t2596332468::get_offset_of_fsmNameLastFrame_20(),
	SetFsmMaterial_t2596332468::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (SetFsmObject_t695695972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3663[8] = 
{
	SetFsmObject_t695695972::get_offset_of_gameObject_14(),
	SetFsmObject_t695695972::get_offset_of_fsmName_15(),
	SetFsmObject_t695695972::get_offset_of_variableName_16(),
	SetFsmObject_t695695972::get_offset_of_setValue_17(),
	SetFsmObject_t695695972::get_offset_of_everyFrame_18(),
	SetFsmObject_t695695972::get_offset_of_goLastFrame_19(),
	SetFsmObject_t695695972::get_offset_of_fsmNameLastFrame_20(),
	SetFsmObject_t695695972::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (SetFsmQuaternion_t2442002114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[8] = 
{
	SetFsmQuaternion_t2442002114::get_offset_of_gameObject_14(),
	SetFsmQuaternion_t2442002114::get_offset_of_fsmName_15(),
	SetFsmQuaternion_t2442002114::get_offset_of_variableName_16(),
	SetFsmQuaternion_t2442002114::get_offset_of_setValue_17(),
	SetFsmQuaternion_t2442002114::get_offset_of_everyFrame_18(),
	SetFsmQuaternion_t2442002114::get_offset_of_goLastFrame_19(),
	SetFsmQuaternion_t2442002114::get_offset_of_fsmNameLastFrame_20(),
	SetFsmQuaternion_t2442002114::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (SetFsmRect_t1186024309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3665[8] = 
{
	SetFsmRect_t1186024309::get_offset_of_gameObject_14(),
	SetFsmRect_t1186024309::get_offset_of_fsmName_15(),
	SetFsmRect_t1186024309::get_offset_of_variableName_16(),
	SetFsmRect_t1186024309::get_offset_of_setValue_17(),
	SetFsmRect_t1186024309::get_offset_of_everyFrame_18(),
	SetFsmRect_t1186024309::get_offset_of_goLastFrame_19(),
	SetFsmRect_t1186024309::get_offset_of_fsmNameLastFrame_20(),
	SetFsmRect_t1186024309::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (SetFsmString_t1900776557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[8] = 
{
	SetFsmString_t1900776557::get_offset_of_gameObject_14(),
	SetFsmString_t1900776557::get_offset_of_fsmName_15(),
	SetFsmString_t1900776557::get_offset_of_variableName_16(),
	SetFsmString_t1900776557::get_offset_of_setValue_17(),
	SetFsmString_t1900776557::get_offset_of_everyFrame_18(),
	SetFsmString_t1900776557::get_offset_of_goLastFrame_19(),
	SetFsmString_t1900776557::get_offset_of_fsmNameLastFrame_20(),
	SetFsmString_t1900776557::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (SetFsmTexture_t1298060854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3667[8] = 
{
	SetFsmTexture_t1298060854::get_offset_of_gameObject_14(),
	SetFsmTexture_t1298060854::get_offset_of_fsmName_15(),
	SetFsmTexture_t1298060854::get_offset_of_variableName_16(),
	SetFsmTexture_t1298060854::get_offset_of_setValue_17(),
	SetFsmTexture_t1298060854::get_offset_of_everyFrame_18(),
	SetFsmTexture_t1298060854::get_offset_of_goLastFrame_19(),
	SetFsmTexture_t1298060854::get_offset_of_fsmNameLastFrame_20(),
	SetFsmTexture_t1298060854::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (SetFsmVariable_t1599143709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[10] = 
{
	SetFsmVariable_t1599143709::get_offset_of_gameObject_14(),
	SetFsmVariable_t1599143709::get_offset_of_fsmName_15(),
	SetFsmVariable_t1599143709::get_offset_of_variableName_16(),
	SetFsmVariable_t1599143709::get_offset_of_setValue_17(),
	SetFsmVariable_t1599143709::get_offset_of_everyFrame_18(),
	SetFsmVariable_t1599143709::get_offset_of_targetFsm_19(),
	SetFsmVariable_t1599143709::get_offset_of_targetVariable_20(),
	SetFsmVariable_t1599143709::get_offset_of_cachedGameObject_21(),
	SetFsmVariable_t1599143709::get_offset_of_cachedFsmName_22(),
	SetFsmVariable_t1599143709::get_offset_of_cachedVariableName_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (SetFsmVector2_t3415240114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[8] = 
{
	SetFsmVector2_t3415240114::get_offset_of_gameObject_14(),
	SetFsmVector2_t3415240114::get_offset_of_fsmName_15(),
	SetFsmVector2_t3415240114::get_offset_of_variableName_16(),
	SetFsmVector2_t3415240114::get_offset_of_setValue_17(),
	SetFsmVector2_t3415240114::get_offset_of_everyFrame_18(),
	SetFsmVector2_t3415240114::get_offset_of_goLastFrame_19(),
	SetFsmVector2_t3415240114::get_offset_of_fsmNameLastFrame_20(),
	SetFsmVector2_t3415240114::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (SetFsmVector3_t1849156173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3670[8] = 
{
	SetFsmVector3_t1849156173::get_offset_of_gameObject_14(),
	SetFsmVector3_t1849156173::get_offset_of_fsmName_15(),
	SetFsmVector3_t1849156173::get_offset_of_variableName_16(),
	SetFsmVector3_t1849156173::get_offset_of_setValue_17(),
	SetFsmVector3_t1849156173::get_offset_of_everyFrame_18(),
	SetFsmVector3_t1849156173::get_offset_of_goLastFrame_19(),
	SetFsmVector3_t1849156173::get_offset_of_fsmNameLastFrame_20(),
	SetFsmVector3_t1849156173::get_offset_of_fsm_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (BuildString_t3135795457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[6] = 
{
	BuildString_t3135795457::get_offset_of_stringParts_14(),
	BuildString_t3135795457::get_offset_of_separator_15(),
	BuildString_t3135795457::get_offset_of_addToEnd_16(),
	BuildString_t3135795457::get_offset_of_storeResult_17(),
	BuildString_t3135795457::get_offset_of_everyFrame_18(),
	BuildString_t3135795457::get_offset_of_result_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (FormatString_t3925645673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[5] = 
{
	FormatString_t3925645673::get_offset_of_format_14(),
	FormatString_t3925645673::get_offset_of_variables_15(),
	FormatString_t3925645673::get_offset_of_storeResult_16(),
	FormatString_t3925645673::get_offset_of_everyFrame_17(),
	FormatString_t3925645673::get_offset_of_objectArray_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (GetStringLeft_t3395224379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[4] = 
{
	GetStringLeft_t3395224379::get_offset_of_stringVariable_14(),
	GetStringLeft_t3395224379::get_offset_of_charCount_15(),
	GetStringLeft_t3395224379::get_offset_of_storeResult_16(),
	GetStringLeft_t3395224379::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (GetStringLength_t1039889994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[3] = 
{
	GetStringLength_t1039889994::get_offset_of_stringVariable_14(),
	GetStringLength_t1039889994::get_offset_of_storeResult_15(),
	GetStringLength_t1039889994::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (GetStringRight_t3697075995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[4] = 
{
	GetStringRight_t3697075995::get_offset_of_stringVariable_14(),
	GetStringRight_t3697075995::get_offset_of_charCount_15(),
	GetStringRight_t3697075995::get_offset_of_storeResult_16(),
	GetStringRight_t3697075995::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (GetSubstring_t621261119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[5] = 
{
	GetSubstring_t621261119::get_offset_of_stringVariable_14(),
	GetSubstring_t621261119::get_offset_of_startIndex_15(),
	GetSubstring_t621261119::get_offset_of_length_16(),
	GetSubstring_t621261119::get_offset_of_storeResult_17(),
	GetSubstring_t621261119::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (SelectRandomString_t3157529308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[3] = 
{
	SelectRandomString_t3157529308::get_offset_of_strings_14(),
	SelectRandomString_t3157529308::get_offset_of_weights_15(),
	SelectRandomString_t3157529308::get_offset_of_storeString_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (SetStringValue_t1748950574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[3] = 
{
	SetStringValue_t1748950574::get_offset_of_stringVariable_14(),
	SetStringValue_t1748950574::get_offset_of_stringValue_15(),
	SetStringValue_t1748950574::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (StringAppend_t1745038038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[2] = 
{
	StringAppend_t1745038038::get_offset_of_stringVariable_14(),
	StringAppend_t1745038038::get_offset_of_appendString_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (StringJoin_t844195649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[3] = 
{
	StringJoin_t844195649::get_offset_of_stringArray_14(),
	StringJoin_t844195649::get_offset_of_separator_15(),
	StringJoin_t844195649::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (StringReplace_t2203809641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[5] = 
{
	StringReplace_t2203809641::get_offset_of_stringVariable_14(),
	StringReplace_t2203809641::get_offset_of_replace_15(),
	StringReplace_t2203809641::get_offset_of_with_16(),
	StringReplace_t2203809641::get_offset_of_storeResult_17(),
	StringReplace_t2203809641::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (StringSplit_t4020258998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[5] = 
{
	StringSplit_t4020258998::get_offset_of_stringToSplit_14(),
	StringSplit_t4020258998::get_offset_of_separators_15(),
	StringSplit_t4020258998::get_offset_of_trimStrings_16(),
	StringSplit_t4020258998::get_offset_of_trimChars_17(),
	StringSplit_t4020258998::get_offset_of_stringArray_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (GetSystemDateTime_t2558928986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[3] = 
{
	GetSystemDateTime_t2558928986::get_offset_of_storeString_14(),
	GetSystemDateTime_t2558928986::get_offset_of_format_15(),
	GetSystemDateTime_t2558928986::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (GetTimeInfo_t3431284632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3684[3] = 
{
	GetTimeInfo_t3431284632::get_offset_of_getInfo_14(),
	GetTimeInfo_t3431284632::get_offset_of_storeValue_15(),
	GetTimeInfo_t3431284632::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (TimeInfo_t2103361344)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3685[9] = 
{
	TimeInfo_t2103361344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (PerSecond_t246664478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3686[3] = 
{
	PerSecond_t246664478::get_offset_of_floatValue_14(),
	PerSecond_t246664478::get_offset_of_storeResult_15(),
	PerSecond_t246664478::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (RandomWait_t1289990085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3687[7] = 
{
	RandomWait_t1289990085::get_offset_of_min_14(),
	RandomWait_t1289990085::get_offset_of_max_15(),
	RandomWait_t1289990085::get_offset_of_finishEvent_16(),
	RandomWait_t1289990085::get_offset_of_realTime_17(),
	RandomWait_t1289990085::get_offset_of_startTime_18(),
	RandomWait_t1289990085::get_offset_of_timer_19(),
	RandomWait_t1289990085::get_offset_of_time_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (ScaleTime_t740334591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3688[3] = 
{
	ScaleTime_t740334591::get_offset_of_timeScale_14(),
	ScaleTime_t740334591::get_offset_of_adjustFixedDeltaTime_15(),
	ScaleTime_t740334591::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (Wait_t2864591912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3689[5] = 
{
	Wait_t2864591912::get_offset_of_time_14(),
	Wait_t2864591912::get_offset_of_finishEvent_15(),
	Wait_t2864591912::get_offset_of_realTime_16(),
	Wait_t2864591912::get_offset_of_startTime_17(),
	Wait_t2864591912::get_offset_of_timer_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (GetAngleToTarget_t2389188411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3690[6] = 
{
	GetAngleToTarget_t2389188411::get_offset_of_gameObject_14(),
	GetAngleToTarget_t2389188411::get_offset_of_targetObject_15(),
	GetAngleToTarget_t2389188411::get_offset_of_targetPosition_16(),
	GetAngleToTarget_t2389188411::get_offset_of_ignoreHeight_17(),
	GetAngleToTarget_t2389188411::get_offset_of_storeAngle_18(),
	GetAngleToTarget_t2389188411::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (GetPosition_t3877072205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[7] = 
{
	GetPosition_t3877072205::get_offset_of_gameObject_14(),
	GetPosition_t3877072205::get_offset_of_vector_15(),
	GetPosition_t3877072205::get_offset_of_x_16(),
	GetPosition_t3877072205::get_offset_of_y_17(),
	GetPosition_t3877072205::get_offset_of_z_18(),
	GetPosition_t3877072205::get_offset_of_space_19(),
	GetPosition_t3877072205::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (GetRotation_t4042812502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[8] = 
{
	GetRotation_t4042812502::get_offset_of_gameObject_14(),
	GetRotation_t4042812502::get_offset_of_quaternion_15(),
	GetRotation_t4042812502::get_offset_of_vector_16(),
	GetRotation_t4042812502::get_offset_of_xAngle_17(),
	GetRotation_t4042812502::get_offset_of_yAngle_18(),
	GetRotation_t4042812502::get_offset_of_zAngle_19(),
	GetRotation_t4042812502::get_offset_of_space_20(),
	GetRotation_t4042812502::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (GetScale_t75781051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3693[7] = 
{
	GetScale_t75781051::get_offset_of_gameObject_14(),
	GetScale_t75781051::get_offset_of_vector_15(),
	GetScale_t75781051::get_offset_of_xScale_16(),
	GetScale_t75781051::get_offset_of_yScale_17(),
	GetScale_t75781051::get_offset_of_zScale_18(),
	GetScale_t75781051::get_offset_of_space_19(),
	GetScale_t75781051::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (InverseTransformDirection_t2869898513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[4] = 
{
	InverseTransformDirection_t2869898513::get_offset_of_gameObject_14(),
	InverseTransformDirection_t2869898513::get_offset_of_worldDirection_15(),
	InverseTransformDirection_t2869898513::get_offset_of_storeResult_16(),
	InverseTransformDirection_t2869898513::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (InverseTransformPoint_t2243770383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[4] = 
{
	InverseTransformPoint_t2243770383::get_offset_of_gameObject_14(),
	InverseTransformPoint_t2243770383::get_offset_of_worldPosition_15(),
	InverseTransformPoint_t2243770383::get_offset_of_storeResult_16(),
	InverseTransformPoint_t2243770383::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (LookAt_t873698241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[12] = 
{
	LookAt_t873698241::get_offset_of_gameObject_14(),
	LookAt_t873698241::get_offset_of_targetObject_15(),
	LookAt_t873698241::get_offset_of_targetPosition_16(),
	LookAt_t873698241::get_offset_of_upVector_17(),
	LookAt_t873698241::get_offset_of_keepVertical_18(),
	LookAt_t873698241::get_offset_of_debug_19(),
	LookAt_t873698241::get_offset_of_debugLineColor_20(),
	LookAt_t873698241::get_offset_of_everyFrame_21(),
	LookAt_t873698241::get_offset_of_go_22(),
	LookAt_t873698241::get_offset_of_goTarget_23(),
	LookAt_t873698241::get_offset_of_lookAtPos_24(),
	LookAt_t873698241::get_offset_of_lookAtPosWithVertical_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (MoveObject_t491415109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[6] = 
{
	MoveObject_t491415109::get_offset_of_objectToMove_35(),
	MoveObject_t491415109::get_offset_of_destination_36(),
	MoveObject_t491415109::get_offset_of_fromValue_37(),
	MoveObject_t491415109::get_offset_of_toVector_38(),
	MoveObject_t491415109::get_offset_of_fromVector_39(),
	MoveObject_t491415109::get_offset_of_finishInNextStep_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (MoveTowards_t1086117126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[11] = 
{
	MoveTowards_t1086117126::get_offset_of_gameObject_14(),
	MoveTowards_t1086117126::get_offset_of_targetObject_15(),
	MoveTowards_t1086117126::get_offset_of_targetPosition_16(),
	MoveTowards_t1086117126::get_offset_of_ignoreVertical_17(),
	MoveTowards_t1086117126::get_offset_of_maxSpeed_18(),
	MoveTowards_t1086117126::get_offset_of_finishDistance_19(),
	MoveTowards_t1086117126::get_offset_of_finishEvent_20(),
	MoveTowards_t1086117126::get_offset_of_go_21(),
	MoveTowards_t1086117126::get_offset_of_goTarget_22(),
	MoveTowards_t1086117126::get_offset_of_targetPos_23(),
	MoveTowards_t1086117126::get_offset_of_targetPosWithVertical_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (Rotate_t4200752417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[10] = 
{
	Rotate_t4200752417::get_offset_of_gameObject_14(),
	Rotate_t4200752417::get_offset_of_vector_15(),
	Rotate_t4200752417::get_offset_of_xAngle_16(),
	Rotate_t4200752417::get_offset_of_yAngle_17(),
	Rotate_t4200752417::get_offset_of_zAngle_18(),
	Rotate_t4200752417::get_offset_of_space_19(),
	Rotate_t4200752417::get_offset_of_perSecond_20(),
	Rotate_t4200752417::get_offset_of_everyFrame_21(),
	Rotate_t4200752417::get_offset_of_lateUpdate_22(),
	Rotate_t4200752417::get_offset_of_fixedUpdate_23(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
