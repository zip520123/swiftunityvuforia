﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HutongGames.PlayMaker.Fsm
struct Fsm_t4127147824;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t163807967;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t1155011270;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t1738900188;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2861764163;
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t1010932050;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t3736299882;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t2511618479;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t2883254149;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t3637897416;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3581898942;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t874273141;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2904461592;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t2057874452;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t3870809373;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2606870197;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t3590610434;
// HutongGames.PlayMaker.FsmState
struct FsmState_t4124398234;
// HutongGames.PlayMaker.FsmString
struct FsmString_t1785915204;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t252501805;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t1738787045;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t2313143784;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t626444517;
// HutongGames.PlayMaker.LayoutOption[]
struct LayoutOptionU5BU5D_t80899288;
// PlayMakerFSM
struct PlayMakerFSM_t1613010231;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.Flare
struct Flare_t1765167167;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t2445521510;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef OPERATION_T1809066441_H
#define OPERATION_T1809066441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolOperator/Operation
struct  Operation_t1809066441 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.BoolOperator/Operation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Operation_t1809066441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATION_T1809066441_H
#ifndef AXISPLANE_T3942092997_H
#define AXISPLANE_T3942092997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAxisVector/AxisPlane
struct  AxisPlane_t3942092997 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.GetAxisVector/AxisPlane::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisPlane_t3942092997, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISPLANE_T3942092997_H
#ifndef ROTATIONAXES_T1378766905_H
#define ROTATIONAXES_T1378766905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MouseLook/RotationAxes
struct  RotationAxes_t1378766905 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.MouseLook/RotationAxes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationAxes_t1378766905, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONAXES_T1378766905_H
#ifndef ROTATIONAXES_T2375808645_H
#define ROTATIONAXES_T2375808645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MouseLook2/RotationAxes
struct  RotationAxes_t2375808645 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.MouseLook2/RotationAxes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationAxes_t2375808645, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONAXES_T2375808645_H
#ifndef AXISPLANE_T3471555291_H
#define AXISPLANE_T3471555291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TransformInputToWorldSpace/AxisPlane
struct  AxisPlane_t3471555291 
{
public:
	// System.Int32 HutongGames.PlayMaker.Actions.TransformInputToWorldSpace/AxisPlane::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisPlane_t3471555291, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISPLANE_T3471555291_H
#ifndef FSMSTATEACTION_T3801304101_H
#define FSMSTATEACTION_T3801304101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmStateAction
struct  FsmStateAction_t3801304101  : public RuntimeObject
{
public:
	// System.String HutongGames.PlayMaker.FsmStateAction::name
	String_t* ___name_2;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::enabled
	bool ___enabled_3;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::isOpen
	bool ___isOpen_4;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::active
	bool ___active_5;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::finished
	bool ___finished_6;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::autoName
	bool ___autoName_7;
	// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::owner
	GameObject_t1113636619 * ___owner_8;
	// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::fsmState
	FsmState_t4124398234 * ___fsmState_9;
	// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::fsm
	Fsm_t4127147824 * ___fsm_10;
	// PlayMakerFSM HutongGames.PlayMaker.FsmStateAction::fsmComponent
	PlayMakerFSM_t1613010231 * ___fsmComponent_11;
	// System.String HutongGames.PlayMaker.FsmStateAction::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_12;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::<Entered>k__BackingField
	bool ___U3CEnteredU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}

	inline static int32_t get_offset_of_isOpen_4() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___isOpen_4)); }
	inline bool get_isOpen_4() const { return ___isOpen_4; }
	inline bool* get_address_of_isOpen_4() { return &___isOpen_4; }
	inline void set_isOpen_4(bool value)
	{
		___isOpen_4 = value;
	}

	inline static int32_t get_offset_of_active_5() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___active_5)); }
	inline bool get_active_5() const { return ___active_5; }
	inline bool* get_address_of_active_5() { return &___active_5; }
	inline void set_active_5(bool value)
	{
		___active_5 = value;
	}

	inline static int32_t get_offset_of_finished_6() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___finished_6)); }
	inline bool get_finished_6() const { return ___finished_6; }
	inline bool* get_address_of_finished_6() { return &___finished_6; }
	inline void set_finished_6(bool value)
	{
		___finished_6 = value;
	}

	inline static int32_t get_offset_of_autoName_7() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___autoName_7)); }
	inline bool get_autoName_7() const { return ___autoName_7; }
	inline bool* get_address_of_autoName_7() { return &___autoName_7; }
	inline void set_autoName_7(bool value)
	{
		___autoName_7 = value;
	}

	inline static int32_t get_offset_of_owner_8() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___owner_8)); }
	inline GameObject_t1113636619 * get_owner_8() const { return ___owner_8; }
	inline GameObject_t1113636619 ** get_address_of_owner_8() { return &___owner_8; }
	inline void set_owner_8(GameObject_t1113636619 * value)
	{
		___owner_8 = value;
		Il2CppCodeGenWriteBarrier((&___owner_8), value);
	}

	inline static int32_t get_offset_of_fsmState_9() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmState_9)); }
	inline FsmState_t4124398234 * get_fsmState_9() const { return ___fsmState_9; }
	inline FsmState_t4124398234 ** get_address_of_fsmState_9() { return &___fsmState_9; }
	inline void set_fsmState_9(FsmState_t4124398234 * value)
	{
		___fsmState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fsmState_9), value);
	}

	inline static int32_t get_offset_of_fsm_10() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsm_10)); }
	inline Fsm_t4127147824 * get_fsm_10() const { return ___fsm_10; }
	inline Fsm_t4127147824 ** get_address_of_fsm_10() { return &___fsm_10; }
	inline void set_fsm_10(Fsm_t4127147824 * value)
	{
		___fsm_10 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_10), value);
	}

	inline static int32_t get_offset_of_fsmComponent_11() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___fsmComponent_11)); }
	inline PlayMakerFSM_t1613010231 * get_fsmComponent_11() const { return ___fsmComponent_11; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsmComponent_11() { return &___fsmComponent_11; }
	inline void set_fsmComponent_11(PlayMakerFSM_t1613010231 * value)
	{
		___fsmComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___fsmComponent_11), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CDisplayNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_12() const { return ___U3CDisplayNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_12() { return &___U3CDisplayNameU3Ek__BackingField_12; }
	inline void set_U3CDisplayNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEnteredU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101, ___U3CEnteredU3Ek__BackingField_13)); }
	inline bool get_U3CEnteredU3Ek__BackingField_13() const { return ___U3CEnteredU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CEnteredU3Ek__BackingField_13() { return &___U3CEnteredU3Ek__BackingField_13; }
	inline void set_U3CEnteredU3Ek__BackingField_13(bool value)
	{
		___U3CEnteredU3Ek__BackingField_13 = value;
	}
};

struct FsmStateAction_t3801304101_StaticFields
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmStateAction::ActiveHighlightColor
	Color_t2555686324  ___ActiveHighlightColor_0;
	// System.Boolean HutongGames.PlayMaker.FsmStateAction::Repaint
	bool ___Repaint_1;

public:
	inline static int32_t get_offset_of_ActiveHighlightColor_0() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___ActiveHighlightColor_0)); }
	inline Color_t2555686324  get_ActiveHighlightColor_0() const { return ___ActiveHighlightColor_0; }
	inline Color_t2555686324 * get_address_of_ActiveHighlightColor_0() { return &___ActiveHighlightColor_0; }
	inline void set_ActiveHighlightColor_0(Color_t2555686324  value)
	{
		___ActiveHighlightColor_0 = value;
	}

	inline static int32_t get_offset_of_Repaint_1() { return static_cast<int32_t>(offsetof(FsmStateAction_t3801304101_StaticFields, ___Repaint_1)); }
	inline bool get_Repaint_1() const { return ___Repaint_1; }
	inline bool* get_address_of_Repaint_1() { return &___Repaint_1; }
	inline void set_Repaint_1(bool value)
	{
		___Repaint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATEACTION_T3801304101_H
#ifndef MOUSEBUTTON_T2824504455_H
#define MOUSEBUTTON_T2824504455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.MouseButton
struct  MouseButton_t2824504455 
{
public:
	// System.Int32 HutongGames.PlayMaker.MouseButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseButton_t2824504455, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEBUTTON_T2824504455_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef ANYKEY_T3425725034_H
#define ANYKEY_T3425725034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.AnyKey
struct  AnyKey_t3425725034  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.AnyKey::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_14;

public:
	inline static int32_t get_offset_of_sendEvent_14() { return static_cast<int32_t>(offsetof(AnyKey_t3425725034, ___sendEvent_14)); }
	inline FsmEvent_t3736299882 * get_sendEvent_14() const { return ___sendEvent_14; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_14() { return &___sendEvent_14; }
	inline void set_sendEvent_14(FsmEvent_t3736299882 * value)
	{
		___sendEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANYKEY_T3425725034_H
#ifndef BOOLALLTRUE_T3292571418_H
#define BOOLALLTRUE_T3292571418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolAllTrue
struct  BoolAllTrue_t3292571418  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.Actions.BoolAllTrue::boolVariables
	FsmBoolU5BU5D_t1155011270* ___boolVariables_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolAllTrue::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolAllTrue::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolAllTrue::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_boolVariables_14() { return static_cast<int32_t>(offsetof(BoolAllTrue_t3292571418, ___boolVariables_14)); }
	inline FsmBoolU5BU5D_t1155011270* get_boolVariables_14() const { return ___boolVariables_14; }
	inline FsmBoolU5BU5D_t1155011270** get_address_of_boolVariables_14() { return &___boolVariables_14; }
	inline void set_boolVariables_14(FsmBoolU5BU5D_t1155011270* value)
	{
		___boolVariables_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariables_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(BoolAllTrue_t3292571418, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(BoolAllTrue_t3292571418, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(BoolAllTrue_t3292571418, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLALLTRUE_T3292571418_H
#ifndef BOOLANYTRUE_T565618083_H
#define BOOLANYTRUE_T565618083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolAnyTrue
struct  BoolAnyTrue_t565618083  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.Actions.BoolAnyTrue::boolVariables
	FsmBoolU5BU5D_t1155011270* ___boolVariables_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolAnyTrue::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolAnyTrue::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolAnyTrue::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_boolVariables_14() { return static_cast<int32_t>(offsetof(BoolAnyTrue_t565618083, ___boolVariables_14)); }
	inline FsmBoolU5BU5D_t1155011270* get_boolVariables_14() const { return ___boolVariables_14; }
	inline FsmBoolU5BU5D_t1155011270** get_address_of_boolVariables_14() { return &___boolVariables_14; }
	inline void set_boolVariables_14(FsmBoolU5BU5D_t1155011270* value)
	{
		___boolVariables_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariables_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(BoolAnyTrue_t565618083, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(BoolAnyTrue_t565618083, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(BoolAnyTrue_t565618083, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLANYTRUE_T565618083_H
#ifndef BOOLCHANGED_T1724066173_H
#define BOOLCHANGED_T1724066173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolChanged
struct  BoolChanged_t1724066173  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolChanged::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolChanged::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolChanged::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolChanged::previousValue
	bool ___previousValue_17;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(BoolChanged_t1724066173, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_changedEvent_15() { return static_cast<int32_t>(offsetof(BoolChanged_t1724066173, ___changedEvent_15)); }
	inline FsmEvent_t3736299882 * get_changedEvent_15() const { return ___changedEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_15() { return &___changedEvent_15; }
	inline void set_changedEvent_15(FsmEvent_t3736299882 * value)
	{
		___changedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(BoolChanged_t1724066173, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_previousValue_17() { return static_cast<int32_t>(offsetof(BoolChanged_t1724066173, ___previousValue_17)); }
	inline bool get_previousValue_17() const { return ___previousValue_17; }
	inline bool* get_address_of_previousValue_17() { return &___previousValue_17; }
	inline void set_previousValue_17(bool value)
	{
		___previousValue_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLCHANGED_T1724066173_H
#ifndef BOOLFLIP_T2621011387_H
#define BOOLFLIP_T2621011387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolFlip
struct  BoolFlip_t2621011387  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolFlip::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(BoolFlip_t2621011387, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLFLIP_T2621011387_H
#ifndef BOOLNONETRUE_T3829007969_H
#define BOOLNONETRUE_T3829007969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolNoneTrue
struct  BoolNoneTrue_t3829007969  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.Actions.BoolNoneTrue::boolVariables
	FsmBoolU5BU5D_t1155011270* ___boolVariables_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolNoneTrue::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolNoneTrue::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolNoneTrue::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_boolVariables_14() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t3829007969, ___boolVariables_14)); }
	inline FsmBoolU5BU5D_t1155011270* get_boolVariables_14() const { return ___boolVariables_14; }
	inline FsmBoolU5BU5D_t1155011270** get_address_of_boolVariables_14() { return &___boolVariables_14; }
	inline void set_boolVariables_14(FsmBoolU5BU5D_t1155011270* value)
	{
		___boolVariables_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariables_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t3829007969, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t3829007969, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(BoolNoneTrue_t3829007969, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLNONETRUE_T3829007969_H
#ifndef BOOLOPERATOR_T2450159816_H
#define BOOLOPERATOR_T2450159816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolOperator
struct  BoolOperator_t2450159816  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolOperator::bool1
	FsmBool_t163807967 * ___bool1_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolOperator::bool2
	FsmBool_t163807967 * ___bool2_15;
	// HutongGames.PlayMaker.Actions.BoolOperator/Operation HutongGames.PlayMaker.Actions.BoolOperator::operation
	int32_t ___operation_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolOperator::storeResult
	FsmBool_t163807967 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolOperator::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_bool1_14() { return static_cast<int32_t>(offsetof(BoolOperator_t2450159816, ___bool1_14)); }
	inline FsmBool_t163807967 * get_bool1_14() const { return ___bool1_14; }
	inline FsmBool_t163807967 ** get_address_of_bool1_14() { return &___bool1_14; }
	inline void set_bool1_14(FsmBool_t163807967 * value)
	{
		___bool1_14 = value;
		Il2CppCodeGenWriteBarrier((&___bool1_14), value);
	}

	inline static int32_t get_offset_of_bool2_15() { return static_cast<int32_t>(offsetof(BoolOperator_t2450159816, ___bool2_15)); }
	inline FsmBool_t163807967 * get_bool2_15() const { return ___bool2_15; }
	inline FsmBool_t163807967 ** get_address_of_bool2_15() { return &___bool2_15; }
	inline void set_bool2_15(FsmBool_t163807967 * value)
	{
		___bool2_15 = value;
		Il2CppCodeGenWriteBarrier((&___bool2_15), value);
	}

	inline static int32_t get_offset_of_operation_16() { return static_cast<int32_t>(offsetof(BoolOperator_t2450159816, ___operation_16)); }
	inline int32_t get_operation_16() const { return ___operation_16; }
	inline int32_t* get_address_of_operation_16() { return &___operation_16; }
	inline void set_operation_16(int32_t value)
	{
		___operation_16 = value;
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(BoolOperator_t2450159816, ___storeResult_17)); }
	inline FsmBool_t163807967 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmBool_t163807967 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(BoolOperator_t2450159816, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLOPERATOR_T2450159816_H
#ifndef BOOLTEST_T4231152335_H
#define BOOLTEST_T4231152335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.BoolTest
struct  BoolTest_t4231152335  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.BoolTest::boolVariable
	FsmBool_t163807967 * ___boolVariable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolTest::isTrue
	FsmEvent_t3736299882 * ___isTrue_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.BoolTest::isFalse
	FsmEvent_t3736299882 * ___isFalse_16;
	// System.Boolean HutongGames.PlayMaker.Actions.BoolTest::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_boolVariable_14() { return static_cast<int32_t>(offsetof(BoolTest_t4231152335, ___boolVariable_14)); }
	inline FsmBool_t163807967 * get_boolVariable_14() const { return ___boolVariable_14; }
	inline FsmBool_t163807967 ** get_address_of_boolVariable_14() { return &___boolVariable_14; }
	inline void set_boolVariable_14(FsmBool_t163807967 * value)
	{
		___boolVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___boolVariable_14), value);
	}

	inline static int32_t get_offset_of_isTrue_15() { return static_cast<int32_t>(offsetof(BoolTest_t4231152335, ___isTrue_15)); }
	inline FsmEvent_t3736299882 * get_isTrue_15() const { return ___isTrue_15; }
	inline FsmEvent_t3736299882 ** get_address_of_isTrue_15() { return &___isTrue_15; }
	inline void set_isTrue_15(FsmEvent_t3736299882 * value)
	{
		___isTrue_15 = value;
		Il2CppCodeGenWriteBarrier((&___isTrue_15), value);
	}

	inline static int32_t get_offset_of_isFalse_16() { return static_cast<int32_t>(offsetof(BoolTest_t4231152335, ___isFalse_16)); }
	inline FsmEvent_t3736299882 * get_isFalse_16() const { return ___isFalse_16; }
	inline FsmEvent_t3736299882 ** get_address_of_isFalse_16() { return &___isFalse_16; }
	inline void set_isFalse_16(FsmEvent_t3736299882 * value)
	{
		___isFalse_16 = value;
		Il2CppCodeGenWriteBarrier((&___isFalse_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(BoolTest_t4231152335, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLTEST_T4231152335_H
#ifndef COLORCOMPARE_T718482581_H
#define COLORCOMPARE_T718482581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ColorCompare
struct  ColorCompare_t718482581  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ColorCompare::color1
	FsmColor_t1738900188 * ___color1_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.ColorCompare::color2
	FsmColor_t1738900188 * ___color2_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ColorCompare::tolerance
	FsmFloat_t2883254149 * ___tolerance_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ColorCompare::equal
	FsmEvent_t3736299882 * ___equal_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ColorCompare::notEqual
	FsmEvent_t3736299882 * ___notEqual_18;
	// System.Boolean HutongGames.PlayMaker.Actions.ColorCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_color1_14() { return static_cast<int32_t>(offsetof(ColorCompare_t718482581, ___color1_14)); }
	inline FsmColor_t1738900188 * get_color1_14() const { return ___color1_14; }
	inline FsmColor_t1738900188 ** get_address_of_color1_14() { return &___color1_14; }
	inline void set_color1_14(FsmColor_t1738900188 * value)
	{
		___color1_14 = value;
		Il2CppCodeGenWriteBarrier((&___color1_14), value);
	}

	inline static int32_t get_offset_of_color2_15() { return static_cast<int32_t>(offsetof(ColorCompare_t718482581, ___color2_15)); }
	inline FsmColor_t1738900188 * get_color2_15() const { return ___color2_15; }
	inline FsmColor_t1738900188 ** get_address_of_color2_15() { return &___color2_15; }
	inline void set_color2_15(FsmColor_t1738900188 * value)
	{
		___color2_15 = value;
		Il2CppCodeGenWriteBarrier((&___color2_15), value);
	}

	inline static int32_t get_offset_of_tolerance_16() { return static_cast<int32_t>(offsetof(ColorCompare_t718482581, ___tolerance_16)); }
	inline FsmFloat_t2883254149 * get_tolerance_16() const { return ___tolerance_16; }
	inline FsmFloat_t2883254149 ** get_address_of_tolerance_16() { return &___tolerance_16; }
	inline void set_tolerance_16(FsmFloat_t2883254149 * value)
	{
		___tolerance_16 = value;
		Il2CppCodeGenWriteBarrier((&___tolerance_16), value);
	}

	inline static int32_t get_offset_of_equal_17() { return static_cast<int32_t>(offsetof(ColorCompare_t718482581, ___equal_17)); }
	inline FsmEvent_t3736299882 * get_equal_17() const { return ___equal_17; }
	inline FsmEvent_t3736299882 ** get_address_of_equal_17() { return &___equal_17; }
	inline void set_equal_17(FsmEvent_t3736299882 * value)
	{
		___equal_17 = value;
		Il2CppCodeGenWriteBarrier((&___equal_17), value);
	}

	inline static int32_t get_offset_of_notEqual_18() { return static_cast<int32_t>(offsetof(ColorCompare_t718482581, ___notEqual_18)); }
	inline FsmEvent_t3736299882 * get_notEqual_18() const { return ___notEqual_18; }
	inline FsmEvent_t3736299882 ** get_address_of_notEqual_18() { return &___notEqual_18; }
	inline void set_notEqual_18(FsmEvent_t3736299882 * value)
	{
		___notEqual_18 = value;
		Il2CppCodeGenWriteBarrier((&___notEqual_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(ColorCompare_t718482581, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCOMPARE_T718482581_H
#ifndef COMPONENTACTION_1_T2339132012_H
#define COMPONENTACTION_1_T2339132012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Light>
struct  ComponentAction_1_t2339132012  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Light_t3756812086 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2339132012, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2339132012, ___cachedComponent_15)); }
	inline Light_t3756812086 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Light_t3756812086 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Light_t3756812086 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2339132012_H
#ifndef COMPONENTACTION_1_T1209346957_H
#define COMPONENTACTION_1_T1209346957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Renderer>
struct  ComponentAction_1_t1209346957  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Renderer_t2627027031 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1209346957, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t1209346957, ___cachedComponent_15)); }
	inline Renderer_t2627027031 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Renderer_t2627027031 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Renderer_t2627027031 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T1209346957_H
#ifndef COMPONENTACTION_1_T2499100150_H
#define COMPONENTACTION_1_T2499100150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.Rigidbody>
struct  ComponentAction_1_t2499100150  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ComponentAction`1::cachedGameObject
	GameObject_t1113636619 * ___cachedGameObject_14;
	// T HutongGames.PlayMaker.Actions.ComponentAction`1::cachedComponent
	Rigidbody_t3916780224 * ___cachedComponent_15;

public:
	inline static int32_t get_offset_of_cachedGameObject_14() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2499100150, ___cachedGameObject_14)); }
	inline GameObject_t1113636619 * get_cachedGameObject_14() const { return ___cachedGameObject_14; }
	inline GameObject_t1113636619 ** get_address_of_cachedGameObject_14() { return &___cachedGameObject_14; }
	inline void set_cachedGameObject_14(GameObject_t1113636619 * value)
	{
		___cachedGameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedGameObject_14), value);
	}

	inline static int32_t get_offset_of_cachedComponent_15() { return static_cast<int32_t>(offsetof(ComponentAction_1_t2499100150, ___cachedComponent_15)); }
	inline Rigidbody_t3916780224 * get_cachedComponent_15() const { return ___cachedComponent_15; }
	inline Rigidbody_t3916780224 ** get_address_of_cachedComponent_15() { return &___cachedComponent_15; }
	inline void set_cachedComponent_15(Rigidbody_t3916780224 * value)
	{
		___cachedComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedComponent_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTACTION_1_T2499100150_H
#ifndef DONTDESTROYONLOAD_T3090985723_H
#define DONTDESTROYONLOAD_T3090985723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DontDestroyOnLoad
struct  DontDestroyOnLoad_t3090985723  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DontDestroyOnLoad::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(DontDestroyOnLoad_t3090985723, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_T3090985723_H
#ifndef ENUMCOMPARE_T535967535_H
#define ENUMCOMPARE_T535967535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnumCompare
struct  EnumCompare_t535967535  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.EnumCompare::enumVariable
	FsmEnum_t2861764163 * ___enumVariable_14;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.EnumCompare::compareTo
	FsmEnum_t2861764163 * ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EnumCompare::equalEvent
	FsmEvent_t3736299882 * ___equalEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EnumCompare::notEqualEvent
	FsmEvent_t3736299882 * ___notEqualEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EnumCompare::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.EnumCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_enumVariable_14() { return static_cast<int32_t>(offsetof(EnumCompare_t535967535, ___enumVariable_14)); }
	inline FsmEnum_t2861764163 * get_enumVariable_14() const { return ___enumVariable_14; }
	inline FsmEnum_t2861764163 ** get_address_of_enumVariable_14() { return &___enumVariable_14; }
	inline void set_enumVariable_14(FsmEnum_t2861764163 * value)
	{
		___enumVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(EnumCompare_t535967535, ___compareTo_15)); }
	inline FsmEnum_t2861764163 * get_compareTo_15() const { return ___compareTo_15; }
	inline FsmEnum_t2861764163 ** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmEnum_t2861764163 * value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_equalEvent_16() { return static_cast<int32_t>(offsetof(EnumCompare_t535967535, ___equalEvent_16)); }
	inline FsmEvent_t3736299882 * get_equalEvent_16() const { return ___equalEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_equalEvent_16() { return &___equalEvent_16; }
	inline void set_equalEvent_16(FsmEvent_t3736299882 * value)
	{
		___equalEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___equalEvent_16), value);
	}

	inline static int32_t get_offset_of_notEqualEvent_17() { return static_cast<int32_t>(offsetof(EnumCompare_t535967535, ___notEqualEvent_17)); }
	inline FsmEvent_t3736299882 * get_notEqualEvent_17() const { return ___notEqualEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_notEqualEvent_17() { return &___notEqualEvent_17; }
	inline void set_notEqualEvent_17(FsmEvent_t3736299882 * value)
	{
		___notEqualEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___notEqualEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(EnumCompare_t535967535, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(EnumCompare_t535967535, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMCOMPARE_T535967535_H
#ifndef ENUMSWITCH_T3155250037_H
#define ENUMSWITCH_T3155250037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EnumSwitch
struct  EnumSwitch_t3155250037  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.EnumSwitch::enumVariable
	FsmEnum_t2861764163 * ___enumVariable_14;
	// HutongGames.PlayMaker.FsmEnum[] HutongGames.PlayMaker.Actions.EnumSwitch::compareTo
	FsmEnumU5BU5D_t1010932050* ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.EnumSwitch::sendEvent
	FsmEventU5BU5D_t2511618479* ___sendEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.EnumSwitch::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_enumVariable_14() { return static_cast<int32_t>(offsetof(EnumSwitch_t3155250037, ___enumVariable_14)); }
	inline FsmEnum_t2861764163 * get_enumVariable_14() const { return ___enumVariable_14; }
	inline FsmEnum_t2861764163 ** get_address_of_enumVariable_14() { return &___enumVariable_14; }
	inline void set_enumVariable_14(FsmEnum_t2861764163 * value)
	{
		___enumVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___enumVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(EnumSwitch_t3155250037, ___compareTo_15)); }
	inline FsmEnumU5BU5D_t1010932050* get_compareTo_15() const { return ___compareTo_15; }
	inline FsmEnumU5BU5D_t1010932050** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmEnumU5BU5D_t1010932050* value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(EnumSwitch_t3155250037, ___sendEvent_16)); }
	inline FsmEventU5BU5D_t2511618479* get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEventU5BU5D_t2511618479* value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(EnumSwitch_t3155250037, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMSWITCH_T3155250037_H
#ifndef FLOATABS_T875865308_H
#define FLOATABS_T875865308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatAbs
struct  FloatAbs_t875865308  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatAbs::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatAbs::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatAbs_t875865308, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(FloatAbs_t875865308, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATABS_T875865308_H
#ifndef FLOATADD_T875275478_H
#define FLOATADD_T875275478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatAdd
struct  FloatAdd_t875275478  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatAdd::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatAdd::add
	FsmFloat_t2883254149 * ___add_15;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatAdd::everyFrame
	bool ___everyFrame_16;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatAdd::perSecond
	bool ___perSecond_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatAdd_t875275478, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_add_15() { return static_cast<int32_t>(offsetof(FloatAdd_t875275478, ___add_15)); }
	inline FsmFloat_t2883254149 * get_add_15() const { return ___add_15; }
	inline FsmFloat_t2883254149 ** get_address_of_add_15() { return &___add_15; }
	inline void set_add_15(FsmFloat_t2883254149 * value)
	{
		___add_15 = value;
		Il2CppCodeGenWriteBarrier((&___add_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(FloatAdd_t875275478, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_perSecond_17() { return static_cast<int32_t>(offsetof(FloatAdd_t875275478, ___perSecond_17)); }
	inline bool get_perSecond_17() const { return ___perSecond_17; }
	inline bool* get_address_of_perSecond_17() { return &___perSecond_17; }
	inline void set_perSecond_17(bool value)
	{
		___perSecond_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATADD_T875275478_H
#ifndef FLOATCHANGED_T3801211219_H
#define FLOATCHANGED_T3801211219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatChanged
struct  FloatChanged_t3801211219  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatChanged::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatChanged::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FloatChanged::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Single HutongGames.PlayMaker.Actions.FloatChanged::previousValue
	float ___previousValue_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatChanged_t3801211219, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_changedEvent_15() { return static_cast<int32_t>(offsetof(FloatChanged_t3801211219, ___changedEvent_15)); }
	inline FsmEvent_t3736299882 * get_changedEvent_15() const { return ___changedEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_15() { return &___changedEvent_15; }
	inline void set_changedEvent_15(FsmEvent_t3736299882 * value)
	{
		___changedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(FloatChanged_t3801211219, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_previousValue_17() { return static_cast<int32_t>(offsetof(FloatChanged_t3801211219, ___previousValue_17)); }
	inline float get_previousValue_17() const { return ___previousValue_17; }
	inline float* get_address_of_previousValue_17() { return &___previousValue_17; }
	inline void set_previousValue_17(float value)
	{
		___previousValue_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATCHANGED_T3801211219_H
#ifndef FLOATCOMPARE_T2247882859_H
#define FLOATCOMPARE_T2247882859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatCompare
struct  FloatCompare_t2247882859  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatCompare::float1
	FsmFloat_t2883254149 * ___float1_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatCompare::float2
	FsmFloat_t2883254149 * ___float2_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatCompare::tolerance
	FsmFloat_t2883254149 * ___tolerance_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatCompare::equal
	FsmEvent_t3736299882 * ___equal_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatCompare::lessThan
	FsmEvent_t3736299882 * ___lessThan_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatCompare::greaterThan
	FsmEvent_t3736299882 * ___greaterThan_19;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatCompare::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_float1_14() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___float1_14)); }
	inline FsmFloat_t2883254149 * get_float1_14() const { return ___float1_14; }
	inline FsmFloat_t2883254149 ** get_address_of_float1_14() { return &___float1_14; }
	inline void set_float1_14(FsmFloat_t2883254149 * value)
	{
		___float1_14 = value;
		Il2CppCodeGenWriteBarrier((&___float1_14), value);
	}

	inline static int32_t get_offset_of_float2_15() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___float2_15)); }
	inline FsmFloat_t2883254149 * get_float2_15() const { return ___float2_15; }
	inline FsmFloat_t2883254149 ** get_address_of_float2_15() { return &___float2_15; }
	inline void set_float2_15(FsmFloat_t2883254149 * value)
	{
		___float2_15 = value;
		Il2CppCodeGenWriteBarrier((&___float2_15), value);
	}

	inline static int32_t get_offset_of_tolerance_16() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___tolerance_16)); }
	inline FsmFloat_t2883254149 * get_tolerance_16() const { return ___tolerance_16; }
	inline FsmFloat_t2883254149 ** get_address_of_tolerance_16() { return &___tolerance_16; }
	inline void set_tolerance_16(FsmFloat_t2883254149 * value)
	{
		___tolerance_16 = value;
		Il2CppCodeGenWriteBarrier((&___tolerance_16), value);
	}

	inline static int32_t get_offset_of_equal_17() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___equal_17)); }
	inline FsmEvent_t3736299882 * get_equal_17() const { return ___equal_17; }
	inline FsmEvent_t3736299882 ** get_address_of_equal_17() { return &___equal_17; }
	inline void set_equal_17(FsmEvent_t3736299882 * value)
	{
		___equal_17 = value;
		Il2CppCodeGenWriteBarrier((&___equal_17), value);
	}

	inline static int32_t get_offset_of_lessThan_18() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___lessThan_18)); }
	inline FsmEvent_t3736299882 * get_lessThan_18() const { return ___lessThan_18; }
	inline FsmEvent_t3736299882 ** get_address_of_lessThan_18() { return &___lessThan_18; }
	inline void set_lessThan_18(FsmEvent_t3736299882 * value)
	{
		___lessThan_18 = value;
		Il2CppCodeGenWriteBarrier((&___lessThan_18), value);
	}

	inline static int32_t get_offset_of_greaterThan_19() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___greaterThan_19)); }
	inline FsmEvent_t3736299882 * get_greaterThan_19() const { return ___greaterThan_19; }
	inline FsmEvent_t3736299882 ** get_address_of_greaterThan_19() { return &___greaterThan_19; }
	inline void set_greaterThan_19(FsmEvent_t3736299882 * value)
	{
		___greaterThan_19 = value;
		Il2CppCodeGenWriteBarrier((&___greaterThan_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(FloatCompare_t2247882859, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATCOMPARE_T2247882859_H
#ifndef FLOATSIGNTEST_T1608327474_H
#define FLOATSIGNTEST_T1608327474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatSignTest
struct  FloatSignTest_t1608327474  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSignTest::floatValue
	FsmFloat_t2883254149 * ___floatValue_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatSignTest::isPositive
	FsmEvent_t3736299882 * ___isPositive_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FloatSignTest::isNegative
	FsmEvent_t3736299882 * ___isNegative_16;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSignTest::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_floatValue_14() { return static_cast<int32_t>(offsetof(FloatSignTest_t1608327474, ___floatValue_14)); }
	inline FsmFloat_t2883254149 * get_floatValue_14() const { return ___floatValue_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatValue_14() { return &___floatValue_14; }
	inline void set_floatValue_14(FsmFloat_t2883254149 * value)
	{
		___floatValue_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatValue_14), value);
	}

	inline static int32_t get_offset_of_isPositive_15() { return static_cast<int32_t>(offsetof(FloatSignTest_t1608327474, ___isPositive_15)); }
	inline FsmEvent_t3736299882 * get_isPositive_15() const { return ___isPositive_15; }
	inline FsmEvent_t3736299882 ** get_address_of_isPositive_15() { return &___isPositive_15; }
	inline void set_isPositive_15(FsmEvent_t3736299882 * value)
	{
		___isPositive_15 = value;
		Il2CppCodeGenWriteBarrier((&___isPositive_15), value);
	}

	inline static int32_t get_offset_of_isNegative_16() { return static_cast<int32_t>(offsetof(FloatSignTest_t1608327474, ___isNegative_16)); }
	inline FsmEvent_t3736299882 * get_isNegative_16() const { return ___isNegative_16; }
	inline FsmEvent_t3736299882 ** get_address_of_isNegative_16() { return &___isNegative_16; }
	inline void set_isNegative_16(FsmEvent_t3736299882 * value)
	{
		___isNegative_16 = value;
		Il2CppCodeGenWriteBarrier((&___isNegative_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(FloatSignTest_t1608327474, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATSIGNTEST_T1608327474_H
#ifndef FLOATSWITCH_T1681728769_H
#define FLOATSWITCH_T1681728769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatSwitch
struct  FloatSwitch_t1681728769  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatSwitch::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_14;
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.Actions.FloatSwitch::lessThan
	FsmFloatU5BU5D_t3637897416* ___lessThan_15;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.FloatSwitch::sendEvent
	FsmEventU5BU5D_t2511618479* ___sendEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatSwitch::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_floatVariable_14() { return static_cast<int32_t>(offsetof(FloatSwitch_t1681728769, ___floatVariable_14)); }
	inline FsmFloat_t2883254149 * get_floatVariable_14() const { return ___floatVariable_14; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_14() { return &___floatVariable_14; }
	inline void set_floatVariable_14(FsmFloat_t2883254149 * value)
	{
		___floatVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_14), value);
	}

	inline static int32_t get_offset_of_lessThan_15() { return static_cast<int32_t>(offsetof(FloatSwitch_t1681728769, ___lessThan_15)); }
	inline FsmFloatU5BU5D_t3637897416* get_lessThan_15() const { return ___lessThan_15; }
	inline FsmFloatU5BU5D_t3637897416** get_address_of_lessThan_15() { return &___lessThan_15; }
	inline void set_lessThan_15(FsmFloatU5BU5D_t3637897416* value)
	{
		___lessThan_15 = value;
		Il2CppCodeGenWriteBarrier((&___lessThan_15), value);
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(FloatSwitch_t1681728769, ___sendEvent_16)); }
	inline FsmEventU5BU5D_t2511618479* get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEventU5BU5D_t2511618479* value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(FloatSwitch_t1681728769, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATSWITCH_T1681728769_H
#ifndef FSMSTATESWITCH_T1581756733_H
#define FSMSTATESWITCH_T1581756733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmStateSwitch
struct  FsmStateSwitch_t1581756733  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FsmStateSwitch::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmStateSwitch::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.FsmStateSwitch::compareTo
	FsmStringU5BU5D_t252501805* ___compareTo_16;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.FsmStateSwitch::sendEvent
	FsmEventU5BU5D_t2511618479* ___sendEvent_17;
	// System.Boolean HutongGames.PlayMaker.Actions.FsmStateSwitch::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.FsmStateSwitch::previousGo
	GameObject_t1113636619 * ___previousGo_19;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.FsmStateSwitch::fsm
	PlayMakerFSM_t1613010231 * ___fsm_20;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_compareTo_16() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___compareTo_16)); }
	inline FsmStringU5BU5D_t252501805* get_compareTo_16() const { return ___compareTo_16; }
	inline FsmStringU5BU5D_t252501805** get_address_of_compareTo_16() { return &___compareTo_16; }
	inline void set_compareTo_16(FsmStringU5BU5D_t252501805* value)
	{
		___compareTo_16 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_16), value);
	}

	inline static int32_t get_offset_of_sendEvent_17() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___sendEvent_17)); }
	inline FsmEventU5BU5D_t2511618479* get_sendEvent_17() const { return ___sendEvent_17; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_sendEvent_17() { return &___sendEvent_17; }
	inline void set_sendEvent_17(FsmEventU5BU5D_t2511618479* value)
	{
		___sendEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of_previousGo_19() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___previousGo_19)); }
	inline GameObject_t1113636619 * get_previousGo_19() const { return ___previousGo_19; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_19() { return &___previousGo_19; }
	inline void set_previousGo_19(GameObject_t1113636619 * value)
	{
		___previousGo_19 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_19), value);
	}

	inline static int32_t get_offset_of_fsm_20() { return static_cast<int32_t>(offsetof(FsmStateSwitch_t1581756733, ___fsm_20)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_20() const { return ___fsm_20; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_20() { return &___fsm_20; }
	inline void set_fsm_20(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_20 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATESWITCH_T1581756733_H
#ifndef FSMSTATETEST_T18088997_H
#define FSMSTATETEST_T18088997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmStateTest
struct  FsmStateTest_t18088997  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FsmStateTest::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmStateTest::fsmName
	FsmString_t1785915204 * ___fsmName_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FsmStateTest::stateName
	FsmString_t1785915204 * ___stateName_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FsmStateTest::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FsmStateTest::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.FsmStateTest::storeResult
	FsmBool_t163807967 * ___storeResult_19;
	// System.Boolean HutongGames.PlayMaker.Actions.FsmStateTest::everyFrame
	bool ___everyFrame_20;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.FsmStateTest::previousGo
	GameObject_t1113636619 * ___previousGo_21;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.FsmStateTest::fsm
	PlayMakerFSM_t1613010231 * ___fsm_22;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_fsmName_15() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___fsmName_15)); }
	inline FsmString_t1785915204 * get_fsmName_15() const { return ___fsmName_15; }
	inline FsmString_t1785915204 ** get_address_of_fsmName_15() { return &___fsmName_15; }
	inline void set_fsmName_15(FsmString_t1785915204 * value)
	{
		___fsmName_15 = value;
		Il2CppCodeGenWriteBarrier((&___fsmName_15), value);
	}

	inline static int32_t get_offset_of_stateName_16() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___stateName_16)); }
	inline FsmString_t1785915204 * get_stateName_16() const { return ___stateName_16; }
	inline FsmString_t1785915204 ** get_address_of_stateName_16() { return &___stateName_16; }
	inline void set_stateName_16(FsmString_t1785915204 * value)
	{
		___stateName_16 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_storeResult_19() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___storeResult_19)); }
	inline FsmBool_t163807967 * get_storeResult_19() const { return ___storeResult_19; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_19() { return &___storeResult_19; }
	inline void set_storeResult_19(FsmBool_t163807967 * value)
	{
		___storeResult_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}

	inline static int32_t get_offset_of_previousGo_21() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___previousGo_21)); }
	inline GameObject_t1113636619 * get_previousGo_21() const { return ___previousGo_21; }
	inline GameObject_t1113636619 ** get_address_of_previousGo_21() { return &___previousGo_21; }
	inline void set_previousGo_21(GameObject_t1113636619 * value)
	{
		___previousGo_21 = value;
		Il2CppCodeGenWriteBarrier((&___previousGo_21), value);
	}

	inline static int32_t get_offset_of_fsm_22() { return static_cast<int32_t>(offsetof(FsmStateTest_t18088997, ___fsm_22)); }
	inline PlayMakerFSM_t1613010231 * get_fsm_22() const { return ___fsm_22; }
	inline PlayMakerFSM_t1613010231 ** get_address_of_fsm_22() { return &___fsm_22; }
	inline void set_fsm_22(PlayMakerFSM_t1613010231 * value)
	{
		___fsm_22 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMSTATETEST_T18088997_H
#ifndef GUILAYOUTACTION_T1272787235_H
#define GUILAYOUTACTION_T1272787235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutAction
struct  GUILayoutAction_t1272787235  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.LayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::layoutOptions
	LayoutOptionU5BU5D_t80899288* ___layoutOptions_14;
	// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::options
	GUILayoutOptionU5BU5D_t2510215842* ___options_15;

public:
	inline static int32_t get_offset_of_layoutOptions_14() { return static_cast<int32_t>(offsetof(GUILayoutAction_t1272787235, ___layoutOptions_14)); }
	inline LayoutOptionU5BU5D_t80899288* get_layoutOptions_14() const { return ___layoutOptions_14; }
	inline LayoutOptionU5BU5D_t80899288** get_address_of_layoutOptions_14() { return &___layoutOptions_14; }
	inline void set_layoutOptions_14(LayoutOptionU5BU5D_t80899288* value)
	{
		___layoutOptions_14 = value;
		Il2CppCodeGenWriteBarrier((&___layoutOptions_14), value);
	}

	inline static int32_t get_offset_of_options_15() { return static_cast<int32_t>(offsetof(GUILayoutAction_t1272787235, ___options_15)); }
	inline GUILayoutOptionU5BU5D_t2510215842* get_options_15() const { return ___options_15; }
	inline GUILayoutOptionU5BU5D_t2510215842** get_address_of_options_15() { return &___options_15; }
	inline void set_options_15(GUILayoutOptionU5BU5D_t2510215842* value)
	{
		___options_15 = value;
		Il2CppCodeGenWriteBarrier((&___options_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTACTION_T1272787235_H
#ifndef GUILAYOUTENDSCROLLVIEW_T1918562859_H
#define GUILAYOUTENDSCROLLVIEW_T1918562859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEndScrollView
struct  GUILayoutEndScrollView_t1918562859  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTENDSCROLLVIEW_T1918562859_H
#ifndef GUILAYOUTENDVERTICAL_T1582804470_H
#define GUILAYOUTENDVERTICAL_T1582804470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutEndVertical
struct  GUILayoutEndVertical_t1582804470  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTENDVERTICAL_T1582804470_H
#ifndef GUILAYOUTFLEXIBLESPACE_T2475786328_H
#define GUILAYOUTFLEXIBLESPACE_T2475786328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace
struct  GUILayoutFlexibleSpace_t2475786328  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTFLEXIBLESPACE_T2475786328_H
#ifndef GUILAYOUTSPACE_T330277626_H
#define GUILAYOUTSPACE_T330277626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutSpace
struct  GUILayoutSpace_t330277626  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutSpace::space
	FsmFloat_t2883254149 * ___space_14;

public:
	inline static int32_t get_offset_of_space_14() { return static_cast<int32_t>(offsetof(GUILayoutSpace_t330277626, ___space_14)); }
	inline FsmFloat_t2883254149 * get_space_14() const { return ___space_14; }
	inline FsmFloat_t2883254149 ** get_address_of_space_14() { return &___space_14; }
	inline void set_space_14(FsmFloat_t2883254149 * value)
	{
		___space_14 = value;
		Il2CppCodeGenWriteBarrier((&___space_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTSPACE_T330277626_H
#ifndef GAMEOBJECTCHANGED_T1134353409_H
#define GAMEOBJECTCHANGED_T1134353409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectChanged
struct  GameObjectChanged_t1134353409  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectChanged::gameObjectVariable
	FsmGameObject_t3581898942 * ___gameObjectVariable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectChanged::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectChanged::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GameObjectChanged::previousValue
	GameObject_t1113636619 * ___previousValue_17;

public:
	inline static int32_t get_offset_of_gameObjectVariable_14() { return static_cast<int32_t>(offsetof(GameObjectChanged_t1134353409, ___gameObjectVariable_14)); }
	inline FsmGameObject_t3581898942 * get_gameObjectVariable_14() const { return ___gameObjectVariable_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObjectVariable_14() { return &___gameObjectVariable_14; }
	inline void set_gameObjectVariable_14(FsmGameObject_t3581898942 * value)
	{
		___gameObjectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectVariable_14), value);
	}

	inline static int32_t get_offset_of_changedEvent_15() { return static_cast<int32_t>(offsetof(GameObjectChanged_t1134353409, ___changedEvent_15)); }
	inline FsmEvent_t3736299882 * get_changedEvent_15() const { return ___changedEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_15() { return &___changedEvent_15; }
	inline void set_changedEvent_15(FsmEvent_t3736299882 * value)
	{
		___changedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GameObjectChanged_t1134353409, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_previousValue_17() { return static_cast<int32_t>(offsetof(GameObjectChanged_t1134353409, ___previousValue_17)); }
	inline GameObject_t1113636619 * get_previousValue_17() const { return ___previousValue_17; }
	inline GameObject_t1113636619 ** get_address_of_previousValue_17() { return &___previousValue_17; }
	inline void set_previousValue_17(GameObject_t1113636619 * value)
	{
		___previousValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___previousValue_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCHANGED_T1134353409_H
#ifndef GAMEOBJECTCOMPARE_T368592200_H
#define GAMEOBJECTCOMPARE_T368592200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectCompare
struct  GameObjectCompare_t368592200  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectCompare::gameObjectVariable
	FsmOwnerDefault_t3590610434 * ___gameObjectVariable_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectCompare::compareTo
	FsmGameObject_t3581898942 * ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompare::equalEvent
	FsmEvent_t3736299882 * ___equalEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompare::notEqualEvent
	FsmEvent_t3736299882 * ___notEqualEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectCompare::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObjectVariable_14() { return static_cast<int32_t>(offsetof(GameObjectCompare_t368592200, ___gameObjectVariable_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObjectVariable_14() const { return ___gameObjectVariable_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObjectVariable_14() { return &___gameObjectVariable_14; }
	inline void set_gameObjectVariable_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObjectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(GameObjectCompare_t368592200, ___compareTo_15)); }
	inline FsmGameObject_t3581898942 * get_compareTo_15() const { return ___compareTo_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmGameObject_t3581898942 * value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_equalEvent_16() { return static_cast<int32_t>(offsetof(GameObjectCompare_t368592200, ___equalEvent_16)); }
	inline FsmEvent_t3736299882 * get_equalEvent_16() const { return ___equalEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_equalEvent_16() { return &___equalEvent_16; }
	inline void set_equalEvent_16(FsmEvent_t3736299882 * value)
	{
		___equalEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___equalEvent_16), value);
	}

	inline static int32_t get_offset_of_notEqualEvent_17() { return static_cast<int32_t>(offsetof(GameObjectCompare_t368592200, ___notEqualEvent_17)); }
	inline FsmEvent_t3736299882 * get_notEqualEvent_17() const { return ___notEqualEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_notEqualEvent_17() { return &___notEqualEvent_17; }
	inline void set_notEqualEvent_17(FsmEvent_t3736299882 * value)
	{
		___notEqualEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___notEqualEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(GameObjectCompare_t368592200, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GameObjectCompare_t368592200, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCOMPARE_T368592200_H
#ifndef GAMEOBJECTCOMPARETAG_T973493013_H
#define GAMEOBJECTCOMPARETAG_T973493013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectCompareTag
struct  GameObjectCompareTag_t973493013  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectCompareTag::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GameObjectCompareTag::tag
	FsmString_t1785915204 * ___tag_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompareTag::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectCompareTag::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectCompareTag::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectCompareTag::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t973493013, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_tag_15() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t973493013, ___tag_15)); }
	inline FsmString_t1785915204 * get_tag_15() const { return ___tag_15; }
	inline FsmString_t1785915204 ** get_address_of_tag_15() { return &___tag_15; }
	inline void set_tag_15(FsmString_t1785915204 * value)
	{
		___tag_15 = value;
		Il2CppCodeGenWriteBarrier((&___tag_15), value);
	}

	inline static int32_t get_offset_of_trueEvent_16() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t973493013, ___trueEvent_16)); }
	inline FsmEvent_t3736299882 * get_trueEvent_16() const { return ___trueEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_16() { return &___trueEvent_16; }
	inline void set_trueEvent_16(FsmEvent_t3736299882 * value)
	{
		___trueEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_16), value);
	}

	inline static int32_t get_offset_of_falseEvent_17() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t973493013, ___falseEvent_17)); }
	inline FsmEvent_t3736299882 * get_falseEvent_17() const { return ___falseEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_17() { return &___falseEvent_17; }
	inline void set_falseEvent_17(FsmEvent_t3736299882 * value)
	{
		___falseEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t973493013, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(GameObjectCompareTag_t973493013, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCOMPARETAG_T973493013_H
#ifndef GAMEOBJECTHASCHILDREN_T1821724050_H
#define GAMEOBJECTHASCHILDREN_T1821724050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectHasChildren
struct  GameObjectHasChildren_t1821724050  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectHasChildren::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectHasChildren::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectHasChildren::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectHasChildren::storeResult
	FsmBool_t163807967 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectHasChildren::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t1821724050, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_trueEvent_15() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t1821724050, ___trueEvent_15)); }
	inline FsmEvent_t3736299882 * get_trueEvent_15() const { return ___trueEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_15() { return &___trueEvent_15; }
	inline void set_trueEvent_15(FsmEvent_t3736299882 * value)
	{
		___trueEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_15), value);
	}

	inline static int32_t get_offset_of_falseEvent_16() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t1821724050, ___falseEvent_16)); }
	inline FsmEvent_t3736299882 * get_falseEvent_16() const { return ___falseEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_16() { return &___falseEvent_16; }
	inline void set_falseEvent_16(FsmEvent_t3736299882 * value)
	{
		___falseEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t1821724050, ___storeResult_17)); }
	inline FsmBool_t163807967 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmBool_t163807967 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GameObjectHasChildren_t1821724050, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTHASCHILDREN_T1821724050_H
#ifndef GAMEOBJECTISCHILDOF_T3428217235_H
#define GAMEOBJECTISCHILDOF_T3428217235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsChildOf
struct  GameObjectIsChildOf_t3428217235  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectIsChildOf::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectIsChildOf::isChildOf
	FsmGameObject_t3581898942 * ___isChildOf_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsChildOf::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsChildOf::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsChildOf::storeResult
	FsmBool_t163807967 * ___storeResult_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t3428217235, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_isChildOf_15() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t3428217235, ___isChildOf_15)); }
	inline FsmGameObject_t3581898942 * get_isChildOf_15() const { return ___isChildOf_15; }
	inline FsmGameObject_t3581898942 ** get_address_of_isChildOf_15() { return &___isChildOf_15; }
	inline void set_isChildOf_15(FsmGameObject_t3581898942 * value)
	{
		___isChildOf_15 = value;
		Il2CppCodeGenWriteBarrier((&___isChildOf_15), value);
	}

	inline static int32_t get_offset_of_trueEvent_16() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t3428217235, ___trueEvent_16)); }
	inline FsmEvent_t3736299882 * get_trueEvent_16() const { return ___trueEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_16() { return &___trueEvent_16; }
	inline void set_trueEvent_16(FsmEvent_t3736299882 * value)
	{
		___trueEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_16), value);
	}

	inline static int32_t get_offset_of_falseEvent_17() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t3428217235, ___falseEvent_17)); }
	inline FsmEvent_t3736299882 * get_falseEvent_17() const { return ___falseEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_17() { return &___falseEvent_17; }
	inline void set_falseEvent_17(FsmEvent_t3736299882 * value)
	{
		___falseEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(GameObjectIsChildOf_t3428217235, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTISCHILDOF_T3428217235_H
#ifndef GAMEOBJECTISNULL_T425567640_H
#define GAMEOBJECTISNULL_T425567640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsNull
struct  GameObjectIsNull_t425567640  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectIsNull::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsNull::isNull
	FsmEvent_t3736299882 * ___isNull_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsNull::isNotNull
	FsmEvent_t3736299882 * ___isNotNull_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsNull::storeResult
	FsmBool_t163807967 * ___storeResult_17;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectIsNull::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t425567640, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_isNull_15() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t425567640, ___isNull_15)); }
	inline FsmEvent_t3736299882 * get_isNull_15() const { return ___isNull_15; }
	inline FsmEvent_t3736299882 ** get_address_of_isNull_15() { return &___isNull_15; }
	inline void set_isNull_15(FsmEvent_t3736299882 * value)
	{
		___isNull_15 = value;
		Il2CppCodeGenWriteBarrier((&___isNull_15), value);
	}

	inline static int32_t get_offset_of_isNotNull_16() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t425567640, ___isNotNull_16)); }
	inline FsmEvent_t3736299882 * get_isNotNull_16() const { return ___isNotNull_16; }
	inline FsmEvent_t3736299882 ** get_address_of_isNotNull_16() { return &___isNotNull_16; }
	inline void set_isNotNull_16(FsmEvent_t3736299882 * value)
	{
		___isNotNull_16 = value;
		Il2CppCodeGenWriteBarrier((&___isNotNull_16), value);
	}

	inline static int32_t get_offset_of_storeResult_17() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t425567640, ___storeResult_17)); }
	inline FsmBool_t163807967 * get_storeResult_17() const { return ___storeResult_17; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_17() { return &___storeResult_17; }
	inline void set_storeResult_17(FsmBool_t163807967 * value)
	{
		___storeResult_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(GameObjectIsNull_t425567640, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTISNULL_T425567640_H
#ifndef GAMEOBJECTTAGSWITCH_T3080400232_H
#define GAMEOBJECTTAGSWITCH_T3080400232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectTagSwitch
struct  GameObjectTagSwitch_t3080400232  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectTagSwitch::gameObject
	FsmGameObject_t3581898942 * ___gameObject_14;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.GameObjectTagSwitch::compareTo
	FsmStringU5BU5D_t252501805* ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.GameObjectTagSwitch::sendEvent
	FsmEventU5BU5D_t2511618479* ___sendEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectTagSwitch::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3080400232, ___gameObject_14)); }
	inline FsmGameObject_t3581898942 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmGameObject_t3581898942 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmGameObject_t3581898942 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3080400232, ___compareTo_15)); }
	inline FsmStringU5BU5D_t252501805* get_compareTo_15() const { return ___compareTo_15; }
	inline FsmStringU5BU5D_t252501805** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmStringU5BU5D_t252501805* value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3080400232, ___sendEvent_16)); }
	inline FsmEventU5BU5D_t2511618479* get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEventU5BU5D_t2511618479* value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GameObjectTagSwitch_t3080400232, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTTAGSWITCH_T3080400232_H
#ifndef GETAXIS_T2827048551_H
#define GETAXIS_T2827048551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAxis
struct  GetAxis_t2827048551  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAxis::axisName
	FsmString_t1785915204 * ___axisName_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxis::multiplier
	FsmFloat_t2883254149 * ___multiplier_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxis::store
	FsmFloat_t2883254149 * ___store_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetAxis::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_axisName_14() { return static_cast<int32_t>(offsetof(GetAxis_t2827048551, ___axisName_14)); }
	inline FsmString_t1785915204 * get_axisName_14() const { return ___axisName_14; }
	inline FsmString_t1785915204 ** get_address_of_axisName_14() { return &___axisName_14; }
	inline void set_axisName_14(FsmString_t1785915204 * value)
	{
		___axisName_14 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_14), value);
	}

	inline static int32_t get_offset_of_multiplier_15() { return static_cast<int32_t>(offsetof(GetAxis_t2827048551, ___multiplier_15)); }
	inline FsmFloat_t2883254149 * get_multiplier_15() const { return ___multiplier_15; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplier_15() { return &___multiplier_15; }
	inline void set_multiplier_15(FsmFloat_t2883254149 * value)
	{
		___multiplier_15 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_15), value);
	}

	inline static int32_t get_offset_of_store_16() { return static_cast<int32_t>(offsetof(GetAxis_t2827048551, ___store_16)); }
	inline FsmFloat_t2883254149 * get_store_16() const { return ___store_16; }
	inline FsmFloat_t2883254149 ** get_address_of_store_16() { return &___store_16; }
	inline void set_store_16(FsmFloat_t2883254149 * value)
	{
		___store_16 = value;
		Il2CppCodeGenWriteBarrier((&___store_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(GetAxis_t2827048551, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETAXIS_T2827048551_H
#ifndef GETAXISVECTOR_T2984782593_H
#define GETAXISVECTOR_T2984782593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetAxisVector
struct  GetAxisVector_t2984782593  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAxisVector::horizontalAxis
	FsmString_t1785915204 * ___horizontalAxis_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetAxisVector::verticalAxis
	FsmString_t1785915204 * ___verticalAxis_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxisVector::multiplier
	FsmFloat_t2883254149 * ___multiplier_16;
	// HutongGames.PlayMaker.Actions.GetAxisVector/AxisPlane HutongGames.PlayMaker.Actions.GetAxisVector::mapToPlane
	int32_t ___mapToPlane_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetAxisVector::relativeTo
	FsmGameObject_t3581898942 * ___relativeTo_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetAxisVector::storeVector
	FsmVector3_t626444517 * ___storeVector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetAxisVector::storeMagnitude
	FsmFloat_t2883254149 * ___storeMagnitude_20;

public:
	inline static int32_t get_offset_of_horizontalAxis_14() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___horizontalAxis_14)); }
	inline FsmString_t1785915204 * get_horizontalAxis_14() const { return ___horizontalAxis_14; }
	inline FsmString_t1785915204 ** get_address_of_horizontalAxis_14() { return &___horizontalAxis_14; }
	inline void set_horizontalAxis_14(FsmString_t1785915204 * value)
	{
		___horizontalAxis_14 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxis_14), value);
	}

	inline static int32_t get_offset_of_verticalAxis_15() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___verticalAxis_15)); }
	inline FsmString_t1785915204 * get_verticalAxis_15() const { return ___verticalAxis_15; }
	inline FsmString_t1785915204 ** get_address_of_verticalAxis_15() { return &___verticalAxis_15; }
	inline void set_verticalAxis_15(FsmString_t1785915204 * value)
	{
		___verticalAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxis_15), value);
	}

	inline static int32_t get_offset_of_multiplier_16() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___multiplier_16)); }
	inline FsmFloat_t2883254149 * get_multiplier_16() const { return ___multiplier_16; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplier_16() { return &___multiplier_16; }
	inline void set_multiplier_16(FsmFloat_t2883254149 * value)
	{
		___multiplier_16 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_16), value);
	}

	inline static int32_t get_offset_of_mapToPlane_17() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___mapToPlane_17)); }
	inline int32_t get_mapToPlane_17() const { return ___mapToPlane_17; }
	inline int32_t* get_address_of_mapToPlane_17() { return &___mapToPlane_17; }
	inline void set_mapToPlane_17(int32_t value)
	{
		___mapToPlane_17 = value;
	}

	inline static int32_t get_offset_of_relativeTo_18() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___relativeTo_18)); }
	inline FsmGameObject_t3581898942 * get_relativeTo_18() const { return ___relativeTo_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_relativeTo_18() { return &___relativeTo_18; }
	inline void set_relativeTo_18(FsmGameObject_t3581898942 * value)
	{
		___relativeTo_18 = value;
		Il2CppCodeGenWriteBarrier((&___relativeTo_18), value);
	}

	inline static int32_t get_offset_of_storeVector_19() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___storeVector_19)); }
	inline FsmVector3_t626444517 * get_storeVector_19() const { return ___storeVector_19; }
	inline FsmVector3_t626444517 ** get_address_of_storeVector_19() { return &___storeVector_19; }
	inline void set_storeVector_19(FsmVector3_t626444517 * value)
	{
		___storeVector_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector_19), value);
	}

	inline static int32_t get_offset_of_storeMagnitude_20() { return static_cast<int32_t>(offsetof(GetAxisVector_t2984782593, ___storeMagnitude_20)); }
	inline FsmFloat_t2883254149 * get_storeMagnitude_20() const { return ___storeMagnitude_20; }
	inline FsmFloat_t2883254149 ** get_address_of_storeMagnitude_20() { return &___storeMagnitude_20; }
	inline void set_storeMagnitude_20(FsmFloat_t2883254149 * value)
	{
		___storeMagnitude_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeMagnitude_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETAXISVECTOR_T2984782593_H
#ifndef GETBUTTON_T251209652_H
#define GETBUTTON_T251209652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetButton
struct  GetButton_t251209652  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetButton::buttonName
	FsmString_t1785915204 * ___buttonName_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetButton::storeResult
	FsmBool_t163807967 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetButton::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_buttonName_14() { return static_cast<int32_t>(offsetof(GetButton_t251209652, ___buttonName_14)); }
	inline FsmString_t1785915204 * get_buttonName_14() const { return ___buttonName_14; }
	inline FsmString_t1785915204 ** get_address_of_buttonName_14() { return &___buttonName_14; }
	inline void set_buttonName_14(FsmString_t1785915204 * value)
	{
		___buttonName_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonName_14), value);
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetButton_t251209652, ___storeResult_15)); }
	inline FsmBool_t163807967 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmBool_t163807967 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetButton_t251209652, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETBUTTON_T251209652_H
#ifndef GETBUTTONDOWN_T2627436382_H
#define GETBUTTONDOWN_T2627436382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetButtonDown
struct  GetButtonDown_t2627436382  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetButtonDown::buttonName
	FsmString_t1785915204 * ___buttonName_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetButtonDown::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetButtonDown::storeResult
	FsmBool_t163807967 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_buttonName_14() { return static_cast<int32_t>(offsetof(GetButtonDown_t2627436382, ___buttonName_14)); }
	inline FsmString_t1785915204 * get_buttonName_14() const { return ___buttonName_14; }
	inline FsmString_t1785915204 ** get_address_of_buttonName_14() { return &___buttonName_14; }
	inline void set_buttonName_14(FsmString_t1785915204 * value)
	{
		___buttonName_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonName_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(GetButtonDown_t2627436382, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetButtonDown_t2627436382, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETBUTTONDOWN_T2627436382_H
#ifndef GETBUTTONUP_T3558816326_H
#define GETBUTTONUP_T3558816326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetButtonUp
struct  GetButtonUp_t3558816326  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetButtonUp::buttonName
	FsmString_t1785915204 * ___buttonName_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetButtonUp::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetButtonUp::storeResult
	FsmBool_t163807967 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_buttonName_14() { return static_cast<int32_t>(offsetof(GetButtonUp_t3558816326, ___buttonName_14)); }
	inline FsmString_t1785915204 * get_buttonName_14() const { return ___buttonName_14; }
	inline FsmString_t1785915204 ** get_address_of_buttonName_14() { return &___buttonName_14; }
	inline void set_buttonName_14(FsmString_t1785915204 * value)
	{
		___buttonName_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonName_14), value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(GetButtonUp_t3558816326, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetButtonUp_t3558816326, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETBUTTONUP_T3558816326_H
#ifndef GETKEY_T3009196479_H
#define GETKEY_T3009196479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetKey
struct  GetKey_t3009196479  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.KeyCode HutongGames.PlayMaker.Actions.GetKey::key
	int32_t ___key_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetKey::storeResult
	FsmBool_t163807967 * ___storeResult_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GetKey::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(GetKey_t3009196479, ___key_14)); }
	inline int32_t get_key_14() const { return ___key_14; }
	inline int32_t* get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(int32_t value)
	{
		___key_14 = value;
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetKey_t3009196479, ___storeResult_15)); }
	inline FsmBool_t163807967 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmBool_t163807967 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GetKey_t3009196479, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETKEY_T3009196479_H
#ifndef GETKEYDOWN_T3975729329_H
#define GETKEYDOWN_T3975729329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetKeyDown
struct  GetKeyDown_t3975729329  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.KeyCode HutongGames.PlayMaker.Actions.GetKeyDown::key
	int32_t ___key_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetKeyDown::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetKeyDown::storeResult
	FsmBool_t163807967 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(GetKeyDown_t3975729329, ___key_14)); }
	inline int32_t get_key_14() const { return ___key_14; }
	inline int32_t* get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(int32_t value)
	{
		___key_14 = value;
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(GetKeyDown_t3975729329, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetKeyDown_t3975729329, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETKEYDOWN_T3975729329_H
#ifndef GETKEYUP_T845549446_H
#define GETKEYUP_T845549446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetKeyUp
struct  GetKeyUp_t845549446  : public FsmStateAction_t3801304101
{
public:
	// UnityEngine.KeyCode HutongGames.PlayMaker.Actions.GetKeyUp::key
	int32_t ___key_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetKeyUp::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetKeyUp::storeResult
	FsmBool_t163807967 * ___storeResult_16;

public:
	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(GetKeyUp_t845549446, ___key_14)); }
	inline int32_t get_key_14() const { return ___key_14; }
	inline int32_t* get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(int32_t value)
	{
		___key_14 = value;
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(GetKeyUp_t845549446, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetKeyUp_t845549446, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETKEYUP_T845549446_H
#ifndef GETMOUSEBUTTON_T743773439_H
#define GETMOUSEBUTTON_T743773439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMouseButton
struct  GetMouseButton_t743773439  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.MouseButton HutongGames.PlayMaker.Actions.GetMouseButton::button
	int32_t ___button_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetMouseButton::storeResult
	FsmBool_t163807967 * ___storeResult_15;

public:
	inline static int32_t get_offset_of_button_14() { return static_cast<int32_t>(offsetof(GetMouseButton_t743773439, ___button_14)); }
	inline int32_t get_button_14() const { return ___button_14; }
	inline int32_t* get_address_of_button_14() { return &___button_14; }
	inline void set_button_14(int32_t value)
	{
		___button_14 = value;
	}

	inline static int32_t get_offset_of_storeResult_15() { return static_cast<int32_t>(offsetof(GetMouseButton_t743773439, ___storeResult_15)); }
	inline FsmBool_t163807967 * get_storeResult_15() const { return ___storeResult_15; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_15() { return &___storeResult_15; }
	inline void set_storeResult_15(FsmBool_t163807967 * value)
	{
		___storeResult_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMOUSEBUTTON_T743773439_H
#ifndef GETMOUSEBUTTONDOWN_T2597630687_H
#define GETMOUSEBUTTONDOWN_T2597630687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMouseButtonDown
struct  GetMouseButtonDown_t2597630687  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.MouseButton HutongGames.PlayMaker.Actions.GetMouseButtonDown::button
	int32_t ___button_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetMouseButtonDown::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetMouseButtonDown::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMouseButtonDown::inUpdateOnly
	bool ___inUpdateOnly_17;

public:
	inline static int32_t get_offset_of_button_14() { return static_cast<int32_t>(offsetof(GetMouseButtonDown_t2597630687, ___button_14)); }
	inline int32_t get_button_14() const { return ___button_14; }
	inline int32_t* get_address_of_button_14() { return &___button_14; }
	inline void set_button_14(int32_t value)
	{
		___button_14 = value;
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(GetMouseButtonDown_t2597630687, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetMouseButtonDown_t2597630687, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_inUpdateOnly_17() { return static_cast<int32_t>(offsetof(GetMouseButtonDown_t2597630687, ___inUpdateOnly_17)); }
	inline bool get_inUpdateOnly_17() const { return ___inUpdateOnly_17; }
	inline bool* get_address_of_inUpdateOnly_17() { return &___inUpdateOnly_17; }
	inline void set_inUpdateOnly_17(bool value)
	{
		___inUpdateOnly_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMOUSEBUTTONDOWN_T2597630687_H
#ifndef GETMOUSEBUTTONUP_T4172049925_H
#define GETMOUSEBUTTONUP_T4172049925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMouseButtonUp
struct  GetMouseButtonUp_t4172049925  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.MouseButton HutongGames.PlayMaker.Actions.GetMouseButtonUp::button
	int32_t ___button_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GetMouseButtonUp::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetMouseButtonUp::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMouseButtonUp::inUpdateOnly
	bool ___inUpdateOnly_17;

public:
	inline static int32_t get_offset_of_button_14() { return static_cast<int32_t>(offsetof(GetMouseButtonUp_t4172049925, ___button_14)); }
	inline int32_t get_button_14() const { return ___button_14; }
	inline int32_t* get_address_of_button_14() { return &___button_14; }
	inline void set_button_14(int32_t value)
	{
		___button_14 = value;
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(GetMouseButtonUp_t4172049925, ___sendEvent_15)); }
	inline FsmEvent_t3736299882 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t3736299882 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(GetMouseButtonUp_t4172049925, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_inUpdateOnly_17() { return static_cast<int32_t>(offsetof(GetMouseButtonUp_t4172049925, ___inUpdateOnly_17)); }
	inline bool get_inUpdateOnly_17() const { return ___inUpdateOnly_17; }
	inline bool* get_address_of_inUpdateOnly_17() { return &___inUpdateOnly_17; }
	inline void set_inUpdateOnly_17(bool value)
	{
		___inUpdateOnly_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMOUSEBUTTONUP_T4172049925_H
#ifndef GETMOUSEX_T3553668615_H
#define GETMOUSEX_T3553668615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMouseX
struct  GetMouseX_t3553668615  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetMouseX::storeResult
	FsmFloat_t2883254149 * ___storeResult_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMouseX::normalize
	bool ___normalize_15;

public:
	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(GetMouseX_t3553668615, ___storeResult_14)); }
	inline FsmFloat_t2883254149 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmFloat_t2883254149 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_14), value);
	}

	inline static int32_t get_offset_of_normalize_15() { return static_cast<int32_t>(offsetof(GetMouseX_t3553668615, ___normalize_15)); }
	inline bool get_normalize_15() const { return ___normalize_15; }
	inline bool* get_address_of_normalize_15() { return &___normalize_15; }
	inline void set_normalize_15(bool value)
	{
		___normalize_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMOUSEX_T3553668615_H
#ifndef GETMOUSEY_T824785260_H
#define GETMOUSEY_T824785260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMouseY
struct  GetMouseY_t824785260  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetMouseY::storeResult
	FsmFloat_t2883254149 * ___storeResult_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMouseY::normalize
	bool ___normalize_15;

public:
	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(GetMouseY_t824785260, ___storeResult_14)); }
	inline FsmFloat_t2883254149 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmFloat_t2883254149 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmFloat_t2883254149 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_14), value);
	}

	inline static int32_t get_offset_of_normalize_15() { return static_cast<int32_t>(offsetof(GetMouseY_t824785260, ___normalize_15)); }
	inline bool get_normalize_15() const { return ___normalize_15; }
	inline bool* get_address_of_normalize_15() { return &___normalize_15; }
	inline void set_normalize_15(bool value)
	{
		___normalize_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMOUSEY_T824785260_H
#ifndef INTCHANGED_T1248712380_H
#define INTCHANGED_T1248712380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntChanged
struct  IntChanged_t1248712380  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntChanged::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntChanged::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IntChanged::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.Int32 HutongGames.PlayMaker.Actions.IntChanged::previousValue
	int32_t ___previousValue_17;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(IntChanged_t1248712380, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_changedEvent_15() { return static_cast<int32_t>(offsetof(IntChanged_t1248712380, ___changedEvent_15)); }
	inline FsmEvent_t3736299882 * get_changedEvent_15() const { return ___changedEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_15() { return &___changedEvent_15; }
	inline void set_changedEvent_15(FsmEvent_t3736299882 * value)
	{
		___changedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(IntChanged_t1248712380, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_previousValue_17() { return static_cast<int32_t>(offsetof(IntChanged_t1248712380, ___previousValue_17)); }
	inline int32_t get_previousValue_17() const { return ___previousValue_17; }
	inline int32_t* get_address_of_previousValue_17() { return &___previousValue_17; }
	inline void set_previousValue_17(int32_t value)
	{
		___previousValue_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTCHANGED_T1248712380_H
#ifndef INTCOMPARE_T1091840200_H
#define INTCOMPARE_T1091840200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntCompare
struct  IntCompare_t1091840200  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntCompare::integer1
	FsmInt_t874273141 * ___integer1_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntCompare::integer2
	FsmInt_t874273141 * ___integer2_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare::equal
	FsmEvent_t3736299882 * ___equal_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare::lessThan
	FsmEvent_t3736299882 * ___lessThan_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IntCompare::greaterThan
	FsmEvent_t3736299882 * ___greaterThan_18;
	// System.Boolean HutongGames.PlayMaker.Actions.IntCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_integer1_14() { return static_cast<int32_t>(offsetof(IntCompare_t1091840200, ___integer1_14)); }
	inline FsmInt_t874273141 * get_integer1_14() const { return ___integer1_14; }
	inline FsmInt_t874273141 ** get_address_of_integer1_14() { return &___integer1_14; }
	inline void set_integer1_14(FsmInt_t874273141 * value)
	{
		___integer1_14 = value;
		Il2CppCodeGenWriteBarrier((&___integer1_14), value);
	}

	inline static int32_t get_offset_of_integer2_15() { return static_cast<int32_t>(offsetof(IntCompare_t1091840200, ___integer2_15)); }
	inline FsmInt_t874273141 * get_integer2_15() const { return ___integer2_15; }
	inline FsmInt_t874273141 ** get_address_of_integer2_15() { return &___integer2_15; }
	inline void set_integer2_15(FsmInt_t874273141 * value)
	{
		___integer2_15 = value;
		Il2CppCodeGenWriteBarrier((&___integer2_15), value);
	}

	inline static int32_t get_offset_of_equal_16() { return static_cast<int32_t>(offsetof(IntCompare_t1091840200, ___equal_16)); }
	inline FsmEvent_t3736299882 * get_equal_16() const { return ___equal_16; }
	inline FsmEvent_t3736299882 ** get_address_of_equal_16() { return &___equal_16; }
	inline void set_equal_16(FsmEvent_t3736299882 * value)
	{
		___equal_16 = value;
		Il2CppCodeGenWriteBarrier((&___equal_16), value);
	}

	inline static int32_t get_offset_of_lessThan_17() { return static_cast<int32_t>(offsetof(IntCompare_t1091840200, ___lessThan_17)); }
	inline FsmEvent_t3736299882 * get_lessThan_17() const { return ___lessThan_17; }
	inline FsmEvent_t3736299882 ** get_address_of_lessThan_17() { return &___lessThan_17; }
	inline void set_lessThan_17(FsmEvent_t3736299882 * value)
	{
		___lessThan_17 = value;
		Il2CppCodeGenWriteBarrier((&___lessThan_17), value);
	}

	inline static int32_t get_offset_of_greaterThan_18() { return static_cast<int32_t>(offsetof(IntCompare_t1091840200, ___greaterThan_18)); }
	inline FsmEvent_t3736299882 * get_greaterThan_18() const { return ___greaterThan_18; }
	inline FsmEvent_t3736299882 ** get_address_of_greaterThan_18() { return &___greaterThan_18; }
	inline void set_greaterThan_18(FsmEvent_t3736299882 * value)
	{
		___greaterThan_18 = value;
		Il2CppCodeGenWriteBarrier((&___greaterThan_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(IntCompare_t1091840200, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTCOMPARE_T1091840200_H
#ifndef INTSWITCH_T3633690694_H
#define INTSWITCH_T3633690694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntSwitch
struct  IntSwitch_t3633690694  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntSwitch::intVariable
	FsmInt_t874273141 * ___intVariable_14;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.IntSwitch::compareTo
	FsmIntU5BU5D_t2904461592* ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.IntSwitch::sendEvent
	FsmEventU5BU5D_t2511618479* ___sendEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.IntSwitch::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_intVariable_14() { return static_cast<int32_t>(offsetof(IntSwitch_t3633690694, ___intVariable_14)); }
	inline FsmInt_t874273141 * get_intVariable_14() const { return ___intVariable_14; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_14() { return &___intVariable_14; }
	inline void set_intVariable_14(FsmInt_t874273141 * value)
	{
		___intVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(IntSwitch_t3633690694, ___compareTo_15)); }
	inline FsmIntU5BU5D_t2904461592* get_compareTo_15() const { return ___compareTo_15; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmIntU5BU5D_t2904461592* value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(IntSwitch_t3633690694, ___sendEvent_16)); }
	inline FsmEventU5BU5D_t2511618479* get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEventU5BU5D_t2511618479* value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(IntSwitch_t3633690694, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTSWITCH_T3633690694_H
#ifndef LOADLEVEL_T1802188777_H
#define LOADLEVEL_T1802188777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LoadLevel
struct  LoadLevel_t1802188777  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.LoadLevel::levelName
	FsmString_t1785915204 * ___levelName_14;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadLevel::additive
	bool ___additive_15;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadLevel::async
	bool ___async_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadLevel::loadedEvent
	FsmEvent_t3736299882 * ___loadedEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadLevel::dontDestroyOnLoad
	FsmBool_t163807967 * ___dontDestroyOnLoad_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadLevel::failedEvent
	FsmEvent_t3736299882 * ___failedEvent_19;
	// UnityEngine.AsyncOperation HutongGames.PlayMaker.Actions.LoadLevel::asyncOperation
	AsyncOperation_t1445031843 * ___asyncOperation_20;

public:
	inline static int32_t get_offset_of_levelName_14() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___levelName_14)); }
	inline FsmString_t1785915204 * get_levelName_14() const { return ___levelName_14; }
	inline FsmString_t1785915204 ** get_address_of_levelName_14() { return &___levelName_14; }
	inline void set_levelName_14(FsmString_t1785915204 * value)
	{
		___levelName_14 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_14), value);
	}

	inline static int32_t get_offset_of_additive_15() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___additive_15)); }
	inline bool get_additive_15() const { return ___additive_15; }
	inline bool* get_address_of_additive_15() { return &___additive_15; }
	inline void set_additive_15(bool value)
	{
		___additive_15 = value;
	}

	inline static int32_t get_offset_of_async_16() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___async_16)); }
	inline bool get_async_16() const { return ___async_16; }
	inline bool* get_address_of_async_16() { return &___async_16; }
	inline void set_async_16(bool value)
	{
		___async_16 = value;
	}

	inline static int32_t get_offset_of_loadedEvent_17() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___loadedEvent_17)); }
	inline FsmEvent_t3736299882 * get_loadedEvent_17() const { return ___loadedEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_loadedEvent_17() { return &___loadedEvent_17; }
	inline void set_loadedEvent_17(FsmEvent_t3736299882 * value)
	{
		___loadedEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___loadedEvent_17), value);
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_18() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___dontDestroyOnLoad_18)); }
	inline FsmBool_t163807967 * get_dontDestroyOnLoad_18() const { return ___dontDestroyOnLoad_18; }
	inline FsmBool_t163807967 ** get_address_of_dontDestroyOnLoad_18() { return &___dontDestroyOnLoad_18; }
	inline void set_dontDestroyOnLoad_18(FsmBool_t163807967 * value)
	{
		___dontDestroyOnLoad_18 = value;
		Il2CppCodeGenWriteBarrier((&___dontDestroyOnLoad_18), value);
	}

	inline static int32_t get_offset_of_failedEvent_19() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___failedEvent_19)); }
	inline FsmEvent_t3736299882 * get_failedEvent_19() const { return ___failedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_failedEvent_19() { return &___failedEvent_19; }
	inline void set_failedEvent_19(FsmEvent_t3736299882 * value)
	{
		___failedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___failedEvent_19), value);
	}

	inline static int32_t get_offset_of_asyncOperation_20() { return static_cast<int32_t>(offsetof(LoadLevel_t1802188777, ___asyncOperation_20)); }
	inline AsyncOperation_t1445031843 * get_asyncOperation_20() const { return ___asyncOperation_20; }
	inline AsyncOperation_t1445031843 ** get_address_of_asyncOperation_20() { return &___asyncOperation_20; }
	inline void set_asyncOperation_20(AsyncOperation_t1445031843 * value)
	{
		___asyncOperation_20 = value;
		Il2CppCodeGenWriteBarrier((&___asyncOperation_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADLEVEL_T1802188777_H
#ifndef LOADLEVELNUM_T3244234815_H
#define LOADLEVELNUM_T3244234815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.LoadLevelNum
struct  LoadLevelNum_t3244234815  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.LoadLevelNum::levelIndex
	FsmInt_t874273141 * ___levelIndex_14;
	// System.Boolean HutongGames.PlayMaker.Actions.LoadLevelNum::additive
	bool ___additive_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadLevelNum::loadedEvent
	FsmEvent_t3736299882 * ___loadedEvent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.LoadLevelNum::dontDestroyOnLoad
	FsmBool_t163807967 * ___dontDestroyOnLoad_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.LoadLevelNum::failedEvent
	FsmEvent_t3736299882 * ___failedEvent_18;

public:
	inline static int32_t get_offset_of_levelIndex_14() { return static_cast<int32_t>(offsetof(LoadLevelNum_t3244234815, ___levelIndex_14)); }
	inline FsmInt_t874273141 * get_levelIndex_14() const { return ___levelIndex_14; }
	inline FsmInt_t874273141 ** get_address_of_levelIndex_14() { return &___levelIndex_14; }
	inline void set_levelIndex_14(FsmInt_t874273141 * value)
	{
		___levelIndex_14 = value;
		Il2CppCodeGenWriteBarrier((&___levelIndex_14), value);
	}

	inline static int32_t get_offset_of_additive_15() { return static_cast<int32_t>(offsetof(LoadLevelNum_t3244234815, ___additive_15)); }
	inline bool get_additive_15() const { return ___additive_15; }
	inline bool* get_address_of_additive_15() { return &___additive_15; }
	inline void set_additive_15(bool value)
	{
		___additive_15 = value;
	}

	inline static int32_t get_offset_of_loadedEvent_16() { return static_cast<int32_t>(offsetof(LoadLevelNum_t3244234815, ___loadedEvent_16)); }
	inline FsmEvent_t3736299882 * get_loadedEvent_16() const { return ___loadedEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_loadedEvent_16() { return &___loadedEvent_16; }
	inline void set_loadedEvent_16(FsmEvent_t3736299882 * value)
	{
		___loadedEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___loadedEvent_16), value);
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_17() { return static_cast<int32_t>(offsetof(LoadLevelNum_t3244234815, ___dontDestroyOnLoad_17)); }
	inline FsmBool_t163807967 * get_dontDestroyOnLoad_17() const { return ___dontDestroyOnLoad_17; }
	inline FsmBool_t163807967 ** get_address_of_dontDestroyOnLoad_17() { return &___dontDestroyOnLoad_17; }
	inline void set_dontDestroyOnLoad_17(FsmBool_t163807967 * value)
	{
		___dontDestroyOnLoad_17 = value;
		Il2CppCodeGenWriteBarrier((&___dontDestroyOnLoad_17), value);
	}

	inline static int32_t get_offset_of_failedEvent_18() { return static_cast<int32_t>(offsetof(LoadLevelNum_t3244234815, ___failedEvent_18)); }
	inline FsmEvent_t3736299882 * get_failedEvent_18() const { return ___failedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_failedEvent_18() { return &___failedEvent_18; }
	inline void set_failedEvent_18(FsmEvent_t3736299882 * value)
	{
		___failedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___failedEvent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADLEVELNUM_T3244234815_H
#ifndef MOUSELOOK_T1429053425_H
#define MOUSELOOK_T1429053425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MouseLook
struct  MouseLook_t1429053425  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MouseLook::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_14;
	// HutongGames.PlayMaker.Actions.MouseLook/RotationAxes HutongGames.PlayMaker.Actions.MouseLook::axes
	int32_t ___axes_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::sensitivityX
	FsmFloat_t2883254149 * ___sensitivityX_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::sensitivityY
	FsmFloat_t2883254149 * ___sensitivityY_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::minimumX
	FsmFloat_t2883254149 * ___minimumX_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::maximumX
	FsmFloat_t2883254149 * ___maximumX_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::minimumY
	FsmFloat_t2883254149 * ___minimumY_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook::maximumY
	FsmFloat_t2883254149 * ___maximumY_21;
	// System.Boolean HutongGames.PlayMaker.Actions.MouseLook::everyFrame
	bool ___everyFrame_22;
	// System.Single HutongGames.PlayMaker.Actions.MouseLook::rotationX
	float ___rotationX_23;
	// System.Single HutongGames.PlayMaker.Actions.MouseLook::rotationY
	float ___rotationY_24;

public:
	inline static int32_t get_offset_of_gameObject_14() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___gameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_14() const { return ___gameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_14() { return &___gameObject_14; }
	inline void set_gameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_14), value);
	}

	inline static int32_t get_offset_of_axes_15() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___axes_15)); }
	inline int32_t get_axes_15() const { return ___axes_15; }
	inline int32_t* get_address_of_axes_15() { return &___axes_15; }
	inline void set_axes_15(int32_t value)
	{
		___axes_15 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_16() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___sensitivityX_16)); }
	inline FsmFloat_t2883254149 * get_sensitivityX_16() const { return ___sensitivityX_16; }
	inline FsmFloat_t2883254149 ** get_address_of_sensitivityX_16() { return &___sensitivityX_16; }
	inline void set_sensitivityX_16(FsmFloat_t2883254149 * value)
	{
		___sensitivityX_16 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityX_16), value);
	}

	inline static int32_t get_offset_of_sensitivityY_17() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___sensitivityY_17)); }
	inline FsmFloat_t2883254149 * get_sensitivityY_17() const { return ___sensitivityY_17; }
	inline FsmFloat_t2883254149 ** get_address_of_sensitivityY_17() { return &___sensitivityY_17; }
	inline void set_sensitivityY_17(FsmFloat_t2883254149 * value)
	{
		___sensitivityY_17 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityY_17), value);
	}

	inline static int32_t get_offset_of_minimumX_18() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___minimumX_18)); }
	inline FsmFloat_t2883254149 * get_minimumX_18() const { return ___minimumX_18; }
	inline FsmFloat_t2883254149 ** get_address_of_minimumX_18() { return &___minimumX_18; }
	inline void set_minimumX_18(FsmFloat_t2883254149 * value)
	{
		___minimumX_18 = value;
		Il2CppCodeGenWriteBarrier((&___minimumX_18), value);
	}

	inline static int32_t get_offset_of_maximumX_19() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___maximumX_19)); }
	inline FsmFloat_t2883254149 * get_maximumX_19() const { return ___maximumX_19; }
	inline FsmFloat_t2883254149 ** get_address_of_maximumX_19() { return &___maximumX_19; }
	inline void set_maximumX_19(FsmFloat_t2883254149 * value)
	{
		___maximumX_19 = value;
		Il2CppCodeGenWriteBarrier((&___maximumX_19), value);
	}

	inline static int32_t get_offset_of_minimumY_20() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___minimumY_20)); }
	inline FsmFloat_t2883254149 * get_minimumY_20() const { return ___minimumY_20; }
	inline FsmFloat_t2883254149 ** get_address_of_minimumY_20() { return &___minimumY_20; }
	inline void set_minimumY_20(FsmFloat_t2883254149 * value)
	{
		___minimumY_20 = value;
		Il2CppCodeGenWriteBarrier((&___minimumY_20), value);
	}

	inline static int32_t get_offset_of_maximumY_21() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___maximumY_21)); }
	inline FsmFloat_t2883254149 * get_maximumY_21() const { return ___maximumY_21; }
	inline FsmFloat_t2883254149 ** get_address_of_maximumY_21() { return &___maximumY_21; }
	inline void set_maximumY_21(FsmFloat_t2883254149 * value)
	{
		___maximumY_21 = value;
		Il2CppCodeGenWriteBarrier((&___maximumY_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}

	inline static int32_t get_offset_of_rotationX_23() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___rotationX_23)); }
	inline float get_rotationX_23() const { return ___rotationX_23; }
	inline float* get_address_of_rotationX_23() { return &___rotationX_23; }
	inline void set_rotationX_23(float value)
	{
		___rotationX_23 = value;
	}

	inline static int32_t get_offset_of_rotationY_24() { return static_cast<int32_t>(offsetof(MouseLook_t1429053425, ___rotationY_24)); }
	inline float get_rotationY_24() const { return ___rotationY_24; }
	inline float* get_address_of_rotationY_24() { return &___rotationY_24; }
	inline void set_rotationY_24(float value)
	{
		___rotationY_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSELOOK_T1429053425_H
#ifndef MOUSEPICK_T1965400047_H
#define MOUSEPICK_T1965400047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MousePick
struct  MousePick_t1965400047  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MousePick::rayDistance
	FsmFloat_t2883254149 * ___rayDistance_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick::storeDidPickObject
	FsmBool_t163807967 * ___storeDidPickObject_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.MousePick::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MousePick::storePoint
	FsmVector3_t626444517 * ___storePoint_17;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.MousePick::storeNormal
	FsmVector3_t626444517 * ___storeNormal_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MousePick::storeDistance
	FsmFloat_t2883254149 * ___storeDistance_19;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.MousePick::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePick::invertMask
	FsmBool_t163807967 * ___invertMask_21;
	// System.Boolean HutongGames.PlayMaker.Actions.MousePick::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_rayDistance_14() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___rayDistance_14)); }
	inline FsmFloat_t2883254149 * get_rayDistance_14() const { return ___rayDistance_14; }
	inline FsmFloat_t2883254149 ** get_address_of_rayDistance_14() { return &___rayDistance_14; }
	inline void set_rayDistance_14(FsmFloat_t2883254149 * value)
	{
		___rayDistance_14 = value;
		Il2CppCodeGenWriteBarrier((&___rayDistance_14), value);
	}

	inline static int32_t get_offset_of_storeDidPickObject_15() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___storeDidPickObject_15)); }
	inline FsmBool_t163807967 * get_storeDidPickObject_15() const { return ___storeDidPickObject_15; }
	inline FsmBool_t163807967 ** get_address_of_storeDidPickObject_15() { return &___storeDidPickObject_15; }
	inline void set_storeDidPickObject_15(FsmBool_t163807967 * value)
	{
		___storeDidPickObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidPickObject_15), value);
	}

	inline static int32_t get_offset_of_storeGameObject_16() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___storeGameObject_16)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_16() const { return ___storeGameObject_16; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_16() { return &___storeGameObject_16; }
	inline void set_storeGameObject_16(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_16), value);
	}

	inline static int32_t get_offset_of_storePoint_17() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___storePoint_17)); }
	inline FsmVector3_t626444517 * get_storePoint_17() const { return ___storePoint_17; }
	inline FsmVector3_t626444517 ** get_address_of_storePoint_17() { return &___storePoint_17; }
	inline void set_storePoint_17(FsmVector3_t626444517 * value)
	{
		___storePoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___storePoint_17), value);
	}

	inline static int32_t get_offset_of_storeNormal_18() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___storeNormal_18)); }
	inline FsmVector3_t626444517 * get_storeNormal_18() const { return ___storeNormal_18; }
	inline FsmVector3_t626444517 ** get_address_of_storeNormal_18() { return &___storeNormal_18; }
	inline void set_storeNormal_18(FsmVector3_t626444517 * value)
	{
		___storeNormal_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeNormal_18), value);
	}

	inline static int32_t get_offset_of_storeDistance_19() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___storeDistance_19)); }
	inline FsmFloat_t2883254149 * get_storeDistance_19() const { return ___storeDistance_19; }
	inline FsmFloat_t2883254149 ** get_address_of_storeDistance_19() { return &___storeDistance_19; }
	inline void set_storeDistance_19(FsmFloat_t2883254149 * value)
	{
		___storeDistance_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeDistance_19), value);
	}

	inline static int32_t get_offset_of_layerMask_20() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___layerMask_20)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_20() const { return ___layerMask_20; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_20() { return &___layerMask_20; }
	inline void set_layerMask_20(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_20 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_20), value);
	}

	inline static int32_t get_offset_of_invertMask_21() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___invertMask_21)); }
	inline FsmBool_t163807967 * get_invertMask_21() const { return ___invertMask_21; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_21() { return &___invertMask_21; }
	inline void set_invertMask_21(FsmBool_t163807967 * value)
	{
		___invertMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(MousePick_t1965400047, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEPICK_T1965400047_H
#ifndef MOUSEPICKEVENT_T4003267355_H
#define MOUSEPICKEVENT_T4003267355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MousePickEvent
struct  MousePickEvent_t4003267355  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MousePickEvent::GameObject
	FsmOwnerDefault_t3590610434 * ___GameObject_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MousePickEvent::rayDistance
	FsmFloat_t2883254149 * ___rayDistance_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseOver
	FsmEvent_t3736299882 * ___mouseOver_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseDown
	FsmEvent_t3736299882 * ___mouseDown_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseUp
	FsmEvent_t3736299882 * ___mouseUp_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.MousePickEvent::mouseOff
	FsmEvent_t3736299882 * ___mouseOff_19;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.MousePickEvent::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MousePickEvent::invertMask
	FsmBool_t163807967 * ___invertMask_21;
	// System.Boolean HutongGames.PlayMaker.Actions.MousePickEvent::everyFrame
	bool ___everyFrame_22;

public:
	inline static int32_t get_offset_of_GameObject_14() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___GameObject_14)); }
	inline FsmOwnerDefault_t3590610434 * get_GameObject_14() const { return ___GameObject_14; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_GameObject_14() { return &___GameObject_14; }
	inline void set_GameObject_14(FsmOwnerDefault_t3590610434 * value)
	{
		___GameObject_14 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_14), value);
	}

	inline static int32_t get_offset_of_rayDistance_15() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___rayDistance_15)); }
	inline FsmFloat_t2883254149 * get_rayDistance_15() const { return ___rayDistance_15; }
	inline FsmFloat_t2883254149 ** get_address_of_rayDistance_15() { return &___rayDistance_15; }
	inline void set_rayDistance_15(FsmFloat_t2883254149 * value)
	{
		___rayDistance_15 = value;
		Il2CppCodeGenWriteBarrier((&___rayDistance_15), value);
	}

	inline static int32_t get_offset_of_mouseOver_16() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___mouseOver_16)); }
	inline FsmEvent_t3736299882 * get_mouseOver_16() const { return ___mouseOver_16; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseOver_16() { return &___mouseOver_16; }
	inline void set_mouseOver_16(FsmEvent_t3736299882 * value)
	{
		___mouseOver_16 = value;
		Il2CppCodeGenWriteBarrier((&___mouseOver_16), value);
	}

	inline static int32_t get_offset_of_mouseDown_17() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___mouseDown_17)); }
	inline FsmEvent_t3736299882 * get_mouseDown_17() const { return ___mouseDown_17; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseDown_17() { return &___mouseDown_17; }
	inline void set_mouseDown_17(FsmEvent_t3736299882 * value)
	{
		___mouseDown_17 = value;
		Il2CppCodeGenWriteBarrier((&___mouseDown_17), value);
	}

	inline static int32_t get_offset_of_mouseUp_18() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___mouseUp_18)); }
	inline FsmEvent_t3736299882 * get_mouseUp_18() const { return ___mouseUp_18; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseUp_18() { return &___mouseUp_18; }
	inline void set_mouseUp_18(FsmEvent_t3736299882 * value)
	{
		___mouseUp_18 = value;
		Il2CppCodeGenWriteBarrier((&___mouseUp_18), value);
	}

	inline static int32_t get_offset_of_mouseOff_19() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___mouseOff_19)); }
	inline FsmEvent_t3736299882 * get_mouseOff_19() const { return ___mouseOff_19; }
	inline FsmEvent_t3736299882 ** get_address_of_mouseOff_19() { return &___mouseOff_19; }
	inline void set_mouseOff_19(FsmEvent_t3736299882 * value)
	{
		___mouseOff_19 = value;
		Il2CppCodeGenWriteBarrier((&___mouseOff_19), value);
	}

	inline static int32_t get_offset_of_layerMask_20() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___layerMask_20)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_20() const { return ___layerMask_20; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_20() { return &___layerMask_20; }
	inline void set_layerMask_20(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_20 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_20), value);
	}

	inline static int32_t get_offset_of_invertMask_21() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___invertMask_21)); }
	inline FsmBool_t163807967 * get_invertMask_21() const { return ___invertMask_21; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_21() { return &___invertMask_21; }
	inline void set_invertMask_21(FsmBool_t163807967 * value)
	{
		___invertMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_21), value);
	}

	inline static int32_t get_offset_of_everyFrame_22() { return static_cast<int32_t>(offsetof(MousePickEvent_t4003267355, ___everyFrame_22)); }
	inline bool get_everyFrame_22() const { return ___everyFrame_22; }
	inline bool* get_address_of_everyFrame_22() { return &___everyFrame_22; }
	inline void set_everyFrame_22(bool value)
	{
		___everyFrame_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEPICKEVENT_T4003267355_H
#ifndef OBJECTCOMPARE_T3988140601_H
#define OBJECTCOMPARE_T3988140601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ObjectCompare
struct  ObjectCompare_t3988140601  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.ObjectCompare::objectVariable
	FsmObject_t2606870197 * ___objectVariable_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.ObjectCompare::compareTo
	FsmObject_t2606870197 * ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ObjectCompare::equalEvent
	FsmEvent_t3736299882 * ___equalEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ObjectCompare::notEqualEvent
	FsmEvent_t3736299882 * ___notEqualEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ObjectCompare::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.ObjectCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_objectVariable_14() { return static_cast<int32_t>(offsetof(ObjectCompare_t3988140601, ___objectVariable_14)); }
	inline FsmObject_t2606870197 * get_objectVariable_14() const { return ___objectVariable_14; }
	inline FsmObject_t2606870197 ** get_address_of_objectVariable_14() { return &___objectVariable_14; }
	inline void set_objectVariable_14(FsmObject_t2606870197 * value)
	{
		___objectVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___objectVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(ObjectCompare_t3988140601, ___compareTo_15)); }
	inline FsmObject_t2606870197 * get_compareTo_15() const { return ___compareTo_15; }
	inline FsmObject_t2606870197 ** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmObject_t2606870197 * value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_equalEvent_16() { return static_cast<int32_t>(offsetof(ObjectCompare_t3988140601, ___equalEvent_16)); }
	inline FsmEvent_t3736299882 * get_equalEvent_16() const { return ___equalEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_equalEvent_16() { return &___equalEvent_16; }
	inline void set_equalEvent_16(FsmEvent_t3736299882 * value)
	{
		___equalEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___equalEvent_16), value);
	}

	inline static int32_t get_offset_of_notEqualEvent_17() { return static_cast<int32_t>(offsetof(ObjectCompare_t3988140601, ___notEqualEvent_17)); }
	inline FsmEvent_t3736299882 * get_notEqualEvent_17() const { return ___notEqualEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_notEqualEvent_17() { return &___notEqualEvent_17; }
	inline void set_notEqualEvent_17(FsmEvent_t3736299882 * value)
	{
		___notEqualEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___notEqualEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(ObjectCompare_t3988140601, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(ObjectCompare_t3988140601, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOMPARE_T3988140601_H
#ifndef RESETINPUTAXES_T1994585160_H
#define RESETINPUTAXES_T1994585160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ResetInputAxes
struct  ResetInputAxes_t1994585160  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETINPUTAXES_T1994585160_H
#ifndef RESTARTLEVEL_T2278880271_H
#define RESTARTLEVEL_T2278880271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RestartLevel
struct  RestartLevel_t2278880271  : public FsmStateAction_t3801304101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTARTLEVEL_T2278880271_H
#ifndef SCREENPICK_T795325859_H
#define SCREENPICK_T795325859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenPick
struct  ScreenPick_t795325859  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick::screenVector
	FsmVector3_t626444517 * ___screenVector_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::screenX
	FsmFloat_t2883254149 * ___screenX_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::screenY
	FsmFloat_t2883254149 * ___screenY_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick::normalized
	FsmBool_t163807967 * ___normalized_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::rayDistance
	FsmFloat_t2883254149 * ___rayDistance_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick::storeDidPickObject
	FsmBool_t163807967 * ___storeDidPickObject_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ScreenPick::storeGameObject
	FsmGameObject_t3581898942 * ___storeGameObject_20;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick::storePoint
	FsmVector3_t626444517 * ___storePoint_21;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenPick::storeNormal
	FsmVector3_t626444517 * ___storeNormal_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ScreenPick::storeDistance
	FsmFloat_t2883254149 * ___storeDistance_23;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.ScreenPick::layerMask
	FsmIntU5BU5D_t2904461592* ___layerMask_24;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenPick::invertMask
	FsmBool_t163807967 * ___invertMask_25;
	// System.Boolean HutongGames.PlayMaker.Actions.ScreenPick::everyFrame
	bool ___everyFrame_26;

public:
	inline static int32_t get_offset_of_screenVector_14() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___screenVector_14)); }
	inline FsmVector3_t626444517 * get_screenVector_14() const { return ___screenVector_14; }
	inline FsmVector3_t626444517 ** get_address_of_screenVector_14() { return &___screenVector_14; }
	inline void set_screenVector_14(FsmVector3_t626444517 * value)
	{
		___screenVector_14 = value;
		Il2CppCodeGenWriteBarrier((&___screenVector_14), value);
	}

	inline static int32_t get_offset_of_screenX_15() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___screenX_15)); }
	inline FsmFloat_t2883254149 * get_screenX_15() const { return ___screenX_15; }
	inline FsmFloat_t2883254149 ** get_address_of_screenX_15() { return &___screenX_15; }
	inline void set_screenX_15(FsmFloat_t2883254149 * value)
	{
		___screenX_15 = value;
		Il2CppCodeGenWriteBarrier((&___screenX_15), value);
	}

	inline static int32_t get_offset_of_screenY_16() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___screenY_16)); }
	inline FsmFloat_t2883254149 * get_screenY_16() const { return ___screenY_16; }
	inline FsmFloat_t2883254149 ** get_address_of_screenY_16() { return &___screenY_16; }
	inline void set_screenY_16(FsmFloat_t2883254149 * value)
	{
		___screenY_16 = value;
		Il2CppCodeGenWriteBarrier((&___screenY_16), value);
	}

	inline static int32_t get_offset_of_normalized_17() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___normalized_17)); }
	inline FsmBool_t163807967 * get_normalized_17() const { return ___normalized_17; }
	inline FsmBool_t163807967 ** get_address_of_normalized_17() { return &___normalized_17; }
	inline void set_normalized_17(FsmBool_t163807967 * value)
	{
		___normalized_17 = value;
		Il2CppCodeGenWriteBarrier((&___normalized_17), value);
	}

	inline static int32_t get_offset_of_rayDistance_18() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___rayDistance_18)); }
	inline FsmFloat_t2883254149 * get_rayDistance_18() const { return ___rayDistance_18; }
	inline FsmFloat_t2883254149 ** get_address_of_rayDistance_18() { return &___rayDistance_18; }
	inline void set_rayDistance_18(FsmFloat_t2883254149 * value)
	{
		___rayDistance_18 = value;
		Il2CppCodeGenWriteBarrier((&___rayDistance_18), value);
	}

	inline static int32_t get_offset_of_storeDidPickObject_19() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___storeDidPickObject_19)); }
	inline FsmBool_t163807967 * get_storeDidPickObject_19() const { return ___storeDidPickObject_19; }
	inline FsmBool_t163807967 ** get_address_of_storeDidPickObject_19() { return &___storeDidPickObject_19; }
	inline void set_storeDidPickObject_19(FsmBool_t163807967 * value)
	{
		___storeDidPickObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeDidPickObject_19), value);
	}

	inline static int32_t get_offset_of_storeGameObject_20() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___storeGameObject_20)); }
	inline FsmGameObject_t3581898942 * get_storeGameObject_20() const { return ___storeGameObject_20; }
	inline FsmGameObject_t3581898942 ** get_address_of_storeGameObject_20() { return &___storeGameObject_20; }
	inline void set_storeGameObject_20(FsmGameObject_t3581898942 * value)
	{
		___storeGameObject_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeGameObject_20), value);
	}

	inline static int32_t get_offset_of_storePoint_21() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___storePoint_21)); }
	inline FsmVector3_t626444517 * get_storePoint_21() const { return ___storePoint_21; }
	inline FsmVector3_t626444517 ** get_address_of_storePoint_21() { return &___storePoint_21; }
	inline void set_storePoint_21(FsmVector3_t626444517 * value)
	{
		___storePoint_21 = value;
		Il2CppCodeGenWriteBarrier((&___storePoint_21), value);
	}

	inline static int32_t get_offset_of_storeNormal_22() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___storeNormal_22)); }
	inline FsmVector3_t626444517 * get_storeNormal_22() const { return ___storeNormal_22; }
	inline FsmVector3_t626444517 ** get_address_of_storeNormal_22() { return &___storeNormal_22; }
	inline void set_storeNormal_22(FsmVector3_t626444517 * value)
	{
		___storeNormal_22 = value;
		Il2CppCodeGenWriteBarrier((&___storeNormal_22), value);
	}

	inline static int32_t get_offset_of_storeDistance_23() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___storeDistance_23)); }
	inline FsmFloat_t2883254149 * get_storeDistance_23() const { return ___storeDistance_23; }
	inline FsmFloat_t2883254149 ** get_address_of_storeDistance_23() { return &___storeDistance_23; }
	inline void set_storeDistance_23(FsmFloat_t2883254149 * value)
	{
		___storeDistance_23 = value;
		Il2CppCodeGenWriteBarrier((&___storeDistance_23), value);
	}

	inline static int32_t get_offset_of_layerMask_24() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___layerMask_24)); }
	inline FsmIntU5BU5D_t2904461592* get_layerMask_24() const { return ___layerMask_24; }
	inline FsmIntU5BU5D_t2904461592** get_address_of_layerMask_24() { return &___layerMask_24; }
	inline void set_layerMask_24(FsmIntU5BU5D_t2904461592* value)
	{
		___layerMask_24 = value;
		Il2CppCodeGenWriteBarrier((&___layerMask_24), value);
	}

	inline static int32_t get_offset_of_invertMask_25() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___invertMask_25)); }
	inline FsmBool_t163807967 * get_invertMask_25() const { return ___invertMask_25; }
	inline FsmBool_t163807967 ** get_address_of_invertMask_25() { return &___invertMask_25; }
	inline void set_invertMask_25(FsmBool_t163807967 * value)
	{
		___invertMask_25 = value;
		Il2CppCodeGenWriteBarrier((&___invertMask_25), value);
	}

	inline static int32_t get_offset_of_everyFrame_26() { return static_cast<int32_t>(offsetof(ScreenPick_t795325859, ___everyFrame_26)); }
	inline bool get_everyFrame_26() const { return ___everyFrame_26; }
	inline bool* get_address_of_everyFrame_26() { return &___everyFrame_26; }
	inline void set_everyFrame_26(bool value)
	{
		___everyFrame_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENPICK_T795325859_H
#ifndef STRINGCHANGED_T1640859722_H
#define STRINGCHANGED_T1640859722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringChanged
struct  StringChanged_t1640859722  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringChanged::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringChanged::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StringChanged::storeResult
	FsmBool_t163807967 * ___storeResult_16;
	// System.String HutongGames.PlayMaker.Actions.StringChanged::previousValue
	String_t* ___previousValue_17;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(StringChanged_t1640859722, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_changedEvent_15() { return static_cast<int32_t>(offsetof(StringChanged_t1640859722, ___changedEvent_15)); }
	inline FsmEvent_t3736299882 * get_changedEvent_15() const { return ___changedEvent_15; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_15() { return &___changedEvent_15; }
	inline void set_changedEvent_15(FsmEvent_t3736299882 * value)
	{
		___changedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_15), value);
	}

	inline static int32_t get_offset_of_storeResult_16() { return static_cast<int32_t>(offsetof(StringChanged_t1640859722, ___storeResult_16)); }
	inline FsmBool_t163807967 * get_storeResult_16() const { return ___storeResult_16; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_16() { return &___storeResult_16; }
	inline void set_storeResult_16(FsmBool_t163807967 * value)
	{
		___storeResult_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_16), value);
	}

	inline static int32_t get_offset_of_previousValue_17() { return static_cast<int32_t>(offsetof(StringChanged_t1640859722, ___previousValue_17)); }
	inline String_t* get_previousValue_17() const { return ___previousValue_17; }
	inline String_t** get_address_of_previousValue_17() { return &___previousValue_17; }
	inline void set_previousValue_17(String_t* value)
	{
		___previousValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___previousValue_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCHANGED_T1640859722_H
#ifndef STRINGCOMPARE_T3732240379_H
#define STRINGCOMPARE_T3732240379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringCompare
struct  StringCompare_t3732240379  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringCompare::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringCompare::compareTo
	FsmString_t1785915204 * ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringCompare::equalEvent
	FsmEvent_t3736299882 * ___equalEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringCompare::notEqualEvent
	FsmEvent_t3736299882 * ___notEqualEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StringCompare::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.StringCompare::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(StringCompare_t3732240379, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(StringCompare_t3732240379, ___compareTo_15)); }
	inline FsmString_t1785915204 * get_compareTo_15() const { return ___compareTo_15; }
	inline FsmString_t1785915204 ** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmString_t1785915204 * value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_equalEvent_16() { return static_cast<int32_t>(offsetof(StringCompare_t3732240379, ___equalEvent_16)); }
	inline FsmEvent_t3736299882 * get_equalEvent_16() const { return ___equalEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_equalEvent_16() { return &___equalEvent_16; }
	inline void set_equalEvent_16(FsmEvent_t3736299882 * value)
	{
		___equalEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___equalEvent_16), value);
	}

	inline static int32_t get_offset_of_notEqualEvent_17() { return static_cast<int32_t>(offsetof(StringCompare_t3732240379, ___notEqualEvent_17)); }
	inline FsmEvent_t3736299882 * get_notEqualEvent_17() const { return ___notEqualEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_notEqualEvent_17() { return &___notEqualEvent_17; }
	inline void set_notEqualEvent_17(FsmEvent_t3736299882 * value)
	{
		___notEqualEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___notEqualEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(StringCompare_t3732240379, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(StringCompare_t3732240379, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARE_T3732240379_H
#ifndef STRINGCONTAINS_T4036348803_H
#define STRINGCONTAINS_T4036348803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringContains
struct  StringContains_t4036348803  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringContains::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringContains::containsString
	FsmString_t1785915204 * ___containsString_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringContains::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.StringContains::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.StringContains::storeResult
	FsmBool_t163807967 * ___storeResult_18;
	// System.Boolean HutongGames.PlayMaker.Actions.StringContains::everyFrame
	bool ___everyFrame_19;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(StringContains_t4036348803, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_containsString_15() { return static_cast<int32_t>(offsetof(StringContains_t4036348803, ___containsString_15)); }
	inline FsmString_t1785915204 * get_containsString_15() const { return ___containsString_15; }
	inline FsmString_t1785915204 ** get_address_of_containsString_15() { return &___containsString_15; }
	inline void set_containsString_15(FsmString_t1785915204 * value)
	{
		___containsString_15 = value;
		Il2CppCodeGenWriteBarrier((&___containsString_15), value);
	}

	inline static int32_t get_offset_of_trueEvent_16() { return static_cast<int32_t>(offsetof(StringContains_t4036348803, ___trueEvent_16)); }
	inline FsmEvent_t3736299882 * get_trueEvent_16() const { return ___trueEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_16() { return &___trueEvent_16; }
	inline void set_trueEvent_16(FsmEvent_t3736299882 * value)
	{
		___trueEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_16), value);
	}

	inline static int32_t get_offset_of_falseEvent_17() { return static_cast<int32_t>(offsetof(StringContains_t4036348803, ___falseEvent_17)); }
	inline FsmEvent_t3736299882 * get_falseEvent_17() const { return ___falseEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_17() { return &___falseEvent_17; }
	inline void set_falseEvent_17(FsmEvent_t3736299882 * value)
	{
		___falseEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_17), value);
	}

	inline static int32_t get_offset_of_storeResult_18() { return static_cast<int32_t>(offsetof(StringContains_t4036348803, ___storeResult_18)); }
	inline FsmBool_t163807967 * get_storeResult_18() const { return ___storeResult_18; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_18() { return &___storeResult_18; }
	inline void set_storeResult_18(FsmBool_t163807967 * value)
	{
		___storeResult_18 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_18), value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(StringContains_t4036348803, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCONTAINS_T4036348803_H
#ifndef STRINGSWITCH_T3656951454_H
#define STRINGSWITCH_T3656951454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.StringSwitch
struct  StringSwitch_t3656951454  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.StringSwitch::stringVariable
	FsmString_t1785915204 * ___stringVariable_14;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.StringSwitch::compareTo
	FsmStringU5BU5D_t252501805* ___compareTo_15;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.StringSwitch::sendEvent
	FsmEventU5BU5D_t2511618479* ___sendEvent_16;
	// System.Boolean HutongGames.PlayMaker.Actions.StringSwitch::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_stringVariable_14() { return static_cast<int32_t>(offsetof(StringSwitch_t3656951454, ___stringVariable_14)); }
	inline FsmString_t1785915204 * get_stringVariable_14() const { return ___stringVariable_14; }
	inline FsmString_t1785915204 ** get_address_of_stringVariable_14() { return &___stringVariable_14; }
	inline void set_stringVariable_14(FsmString_t1785915204 * value)
	{
		___stringVariable_14 = value;
		Il2CppCodeGenWriteBarrier((&___stringVariable_14), value);
	}

	inline static int32_t get_offset_of_compareTo_15() { return static_cast<int32_t>(offsetof(StringSwitch_t3656951454, ___compareTo_15)); }
	inline FsmStringU5BU5D_t252501805* get_compareTo_15() const { return ___compareTo_15; }
	inline FsmStringU5BU5D_t252501805** get_address_of_compareTo_15() { return &___compareTo_15; }
	inline void set_compareTo_15(FsmStringU5BU5D_t252501805* value)
	{
		___compareTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___compareTo_15), value);
	}

	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(StringSwitch_t3656951454, ___sendEvent_16)); }
	inline FsmEventU5BU5D_t2511618479* get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEventU5BU5D_t2511618479* value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(StringSwitch_t3656951454, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGSWITCH_T3656951454_H
#ifndef TRANSFORMINPUTTOWORLDSPACE_T2611876041_H
#define TRANSFORMINPUTTOWORLDSPACE_T2611876041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.TransformInputToWorldSpace
struct  TransformInputToWorldSpace_t2611876041  : public FsmStateAction_t3801304101
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::horizontalInput
	FsmFloat_t2883254149 * ___horizontalInput_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::verticalInput
	FsmFloat_t2883254149 * ___verticalInput_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::multiplier
	FsmFloat_t2883254149 * ___multiplier_16;
	// HutongGames.PlayMaker.Actions.TransformInputToWorldSpace/AxisPlane HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::mapToPlane
	int32_t ___mapToPlane_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::relativeTo
	FsmGameObject_t3581898942 * ___relativeTo_18;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::storeVector
	FsmVector3_t626444517 * ___storeVector_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.TransformInputToWorldSpace::storeMagnitude
	FsmFloat_t2883254149 * ___storeMagnitude_20;

public:
	inline static int32_t get_offset_of_horizontalInput_14() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___horizontalInput_14)); }
	inline FsmFloat_t2883254149 * get_horizontalInput_14() const { return ___horizontalInput_14; }
	inline FsmFloat_t2883254149 ** get_address_of_horizontalInput_14() { return &___horizontalInput_14; }
	inline void set_horizontalInput_14(FsmFloat_t2883254149 * value)
	{
		___horizontalInput_14 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalInput_14), value);
	}

	inline static int32_t get_offset_of_verticalInput_15() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___verticalInput_15)); }
	inline FsmFloat_t2883254149 * get_verticalInput_15() const { return ___verticalInput_15; }
	inline FsmFloat_t2883254149 ** get_address_of_verticalInput_15() { return &___verticalInput_15; }
	inline void set_verticalInput_15(FsmFloat_t2883254149 * value)
	{
		___verticalInput_15 = value;
		Il2CppCodeGenWriteBarrier((&___verticalInput_15), value);
	}

	inline static int32_t get_offset_of_multiplier_16() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___multiplier_16)); }
	inline FsmFloat_t2883254149 * get_multiplier_16() const { return ___multiplier_16; }
	inline FsmFloat_t2883254149 ** get_address_of_multiplier_16() { return &___multiplier_16; }
	inline void set_multiplier_16(FsmFloat_t2883254149 * value)
	{
		___multiplier_16 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_16), value);
	}

	inline static int32_t get_offset_of_mapToPlane_17() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___mapToPlane_17)); }
	inline int32_t get_mapToPlane_17() const { return ___mapToPlane_17; }
	inline int32_t* get_address_of_mapToPlane_17() { return &___mapToPlane_17; }
	inline void set_mapToPlane_17(int32_t value)
	{
		___mapToPlane_17 = value;
	}

	inline static int32_t get_offset_of_relativeTo_18() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___relativeTo_18)); }
	inline FsmGameObject_t3581898942 * get_relativeTo_18() const { return ___relativeTo_18; }
	inline FsmGameObject_t3581898942 ** get_address_of_relativeTo_18() { return &___relativeTo_18; }
	inline void set_relativeTo_18(FsmGameObject_t3581898942 * value)
	{
		___relativeTo_18 = value;
		Il2CppCodeGenWriteBarrier((&___relativeTo_18), value);
	}

	inline static int32_t get_offset_of_storeVector_19() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___storeVector_19)); }
	inline FsmVector3_t626444517 * get_storeVector_19() const { return ___storeVector_19; }
	inline FsmVector3_t626444517 ** get_address_of_storeVector_19() { return &___storeVector_19; }
	inline void set_storeVector_19(FsmVector3_t626444517 * value)
	{
		___storeVector_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeVector_19), value);
	}

	inline static int32_t get_offset_of_storeMagnitude_20() { return static_cast<int32_t>(offsetof(TransformInputToWorldSpace_t2611876041, ___storeMagnitude_20)); }
	inline FsmFloat_t2883254149 * get_storeMagnitude_20() const { return ___storeMagnitude_20; }
	inline FsmFloat_t2883254149 ** get_address_of_storeMagnitude_20() { return &___storeMagnitude_20; }
	inline void set_storeMagnitude_20(FsmFloat_t2883254149 * value)
	{
		___storeMagnitude_20 = value;
		Il2CppCodeGenWriteBarrier((&___storeMagnitude_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMINPUTTOWORLDSPACE_T2611876041_H
#ifndef USEGUILAYOUT_T3165979122_H
#define USEGUILAYOUT_T3165979122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UseGUILayout
struct  UseGUILayout_t3165979122  : public FsmStateAction_t3801304101
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.UseGUILayout::turnOffGUIlayout
	bool ___turnOffGUIlayout_14;

public:
	inline static int32_t get_offset_of_turnOffGUIlayout_14() { return static_cast<int32_t>(offsetof(UseGUILayout_t3165979122, ___turnOffGUIlayout_14)); }
	inline bool get_turnOffGUIlayout_14() const { return ___turnOffGUIlayout_14; }
	inline bool* get_address_of_turnOffGUIlayout_14() { return &___turnOffGUIlayout_14; }
	inline void set_turnOffGUIlayout_14(bool value)
	{
		___turnOffGUIlayout_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEGUILAYOUT_T3165979122_H
#ifndef GUILAYOUTFLOATFIELD_T805838026_H
#define GUILAYOUTFLOATFIELD_T805838026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutFloatField
struct  GUILayoutFloatField_t805838026  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutFloatField::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutFloatField::style
	FsmString_t1785915204 * ___style_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutFloatField::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_18;

public:
	inline static int32_t get_offset_of_floatVariable_16() { return static_cast<int32_t>(offsetof(GUILayoutFloatField_t805838026, ___floatVariable_16)); }
	inline FsmFloat_t2883254149 * get_floatVariable_16() const { return ___floatVariable_16; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_16() { return &___floatVariable_16; }
	inline void set_floatVariable_16(FsmFloat_t2883254149 * value)
	{
		___floatVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_16), value);
	}

	inline static int32_t get_offset_of_style_17() { return static_cast<int32_t>(offsetof(GUILayoutFloatField_t805838026, ___style_17)); }
	inline FsmString_t1785915204 * get_style_17() const { return ___style_17; }
	inline FsmString_t1785915204 ** get_address_of_style_17() { return &___style_17; }
	inline void set_style_17(FsmString_t1785915204 * value)
	{
		___style_17 = value;
		Il2CppCodeGenWriteBarrier((&___style_17), value);
	}

	inline static int32_t get_offset_of_changedEvent_18() { return static_cast<int32_t>(offsetof(GUILayoutFloatField_t805838026, ___changedEvent_18)); }
	inline FsmEvent_t3736299882 * get_changedEvent_18() const { return ___changedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_18() { return &___changedEvent_18; }
	inline void set_changedEvent_18(FsmEvent_t3736299882 * value)
	{
		___changedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTFLOATFIELD_T805838026_H
#ifndef GUILAYOUTFLOATLABEL_T66359707_H
#define GUILAYOUTFLOATLABEL_T66359707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutFloatLabel
struct  GUILayoutFloatLabel_t66359707  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::prefix
	FsmString_t1785915204 * ___prefix_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::style
	FsmString_t1785915204 * ___style_18;

public:
	inline static int32_t get_offset_of_prefix_16() { return static_cast<int32_t>(offsetof(GUILayoutFloatLabel_t66359707, ___prefix_16)); }
	inline FsmString_t1785915204 * get_prefix_16() const { return ___prefix_16; }
	inline FsmString_t1785915204 ** get_address_of_prefix_16() { return &___prefix_16; }
	inline void set_prefix_16(FsmString_t1785915204 * value)
	{
		___prefix_16 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_16), value);
	}

	inline static int32_t get_offset_of_floatVariable_17() { return static_cast<int32_t>(offsetof(GUILayoutFloatLabel_t66359707, ___floatVariable_17)); }
	inline FsmFloat_t2883254149 * get_floatVariable_17() const { return ___floatVariable_17; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_17() { return &___floatVariable_17; }
	inline void set_floatVariable_17(FsmFloat_t2883254149 * value)
	{
		___floatVariable_17 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_17), value);
	}

	inline static int32_t get_offset_of_style_18() { return static_cast<int32_t>(offsetof(GUILayoutFloatLabel_t66359707, ___style_18)); }
	inline FsmString_t1785915204 * get_style_18() const { return ___style_18; }
	inline FsmString_t1785915204 ** get_address_of_style_18() { return &___style_18; }
	inline void set_style_18(FsmString_t1785915204 * value)
	{
		___style_18 = value;
		Il2CppCodeGenWriteBarrier((&___style_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTFLOATLABEL_T66359707_H
#ifndef GUILAYOUTHORIZONTALSLIDER_T3690373913_H
#define GUILAYOUTHORIZONTALSLIDER_T3690373913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider
struct  GUILayoutHorizontalSlider_t3690373913  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::leftValue
	FsmFloat_t2883254149 * ___leftValue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::rightValue
	FsmFloat_t2883254149 * ___rightValue_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_19;

public:
	inline static int32_t get_offset_of_floatVariable_16() { return static_cast<int32_t>(offsetof(GUILayoutHorizontalSlider_t3690373913, ___floatVariable_16)); }
	inline FsmFloat_t2883254149 * get_floatVariable_16() const { return ___floatVariable_16; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_16() { return &___floatVariable_16; }
	inline void set_floatVariable_16(FsmFloat_t2883254149 * value)
	{
		___floatVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_16), value);
	}

	inline static int32_t get_offset_of_leftValue_17() { return static_cast<int32_t>(offsetof(GUILayoutHorizontalSlider_t3690373913, ___leftValue_17)); }
	inline FsmFloat_t2883254149 * get_leftValue_17() const { return ___leftValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_leftValue_17() { return &___leftValue_17; }
	inline void set_leftValue_17(FsmFloat_t2883254149 * value)
	{
		___leftValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___leftValue_17), value);
	}

	inline static int32_t get_offset_of_rightValue_18() { return static_cast<int32_t>(offsetof(GUILayoutHorizontalSlider_t3690373913, ___rightValue_18)); }
	inline FsmFloat_t2883254149 * get_rightValue_18() const { return ___rightValue_18; }
	inline FsmFloat_t2883254149 ** get_address_of_rightValue_18() { return &___rightValue_18; }
	inline void set_rightValue_18(FsmFloat_t2883254149 * value)
	{
		___rightValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___rightValue_18), value);
	}

	inline static int32_t get_offset_of_changedEvent_19() { return static_cast<int32_t>(offsetof(GUILayoutHorizontalSlider_t3690373913, ___changedEvent_19)); }
	inline FsmEvent_t3736299882 * get_changedEvent_19() const { return ___changedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_19() { return &___changedEvent_19; }
	inline void set_changedEvent_19(FsmEvent_t3736299882 * value)
	{
		___changedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTHORIZONTALSLIDER_T3690373913_H
#ifndef GUILAYOUTINTFIELD_T2927046985_H
#define GUILAYOUTINTFIELD_T2927046985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutIntField
struct  GUILayoutIntField_t2927046985  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutIntField::intVariable
	FsmInt_t874273141 * ___intVariable_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutIntField::style
	FsmString_t1785915204 * ___style_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutIntField::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_18;

public:
	inline static int32_t get_offset_of_intVariable_16() { return static_cast<int32_t>(offsetof(GUILayoutIntField_t2927046985, ___intVariable_16)); }
	inline FsmInt_t874273141 * get_intVariable_16() const { return ___intVariable_16; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_16() { return &___intVariable_16; }
	inline void set_intVariable_16(FsmInt_t874273141 * value)
	{
		___intVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_16), value);
	}

	inline static int32_t get_offset_of_style_17() { return static_cast<int32_t>(offsetof(GUILayoutIntField_t2927046985, ___style_17)); }
	inline FsmString_t1785915204 * get_style_17() const { return ___style_17; }
	inline FsmString_t1785915204 ** get_address_of_style_17() { return &___style_17; }
	inline void set_style_17(FsmString_t1785915204 * value)
	{
		___style_17 = value;
		Il2CppCodeGenWriteBarrier((&___style_17), value);
	}

	inline static int32_t get_offset_of_changedEvent_18() { return static_cast<int32_t>(offsetof(GUILayoutIntField_t2927046985, ___changedEvent_18)); }
	inline FsmEvent_t3736299882 * get_changedEvent_18() const { return ___changedEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_18() { return &___changedEvent_18; }
	inline void set_changedEvent_18(FsmEvent_t3736299882 * value)
	{
		___changedEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTINTFIELD_T2927046985_H
#ifndef GUILAYOUTINTLABEL_T1298772968_H
#define GUILAYOUTINTLABEL_T1298772968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutIntLabel
struct  GUILayoutIntLabel_t1298772968  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutIntLabel::prefix
	FsmString_t1785915204 * ___prefix_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutIntLabel::intVariable
	FsmInt_t874273141 * ___intVariable_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutIntLabel::style
	FsmString_t1785915204 * ___style_18;

public:
	inline static int32_t get_offset_of_prefix_16() { return static_cast<int32_t>(offsetof(GUILayoutIntLabel_t1298772968, ___prefix_16)); }
	inline FsmString_t1785915204 * get_prefix_16() const { return ___prefix_16; }
	inline FsmString_t1785915204 ** get_address_of_prefix_16() { return &___prefix_16; }
	inline void set_prefix_16(FsmString_t1785915204 * value)
	{
		___prefix_16 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_16), value);
	}

	inline static int32_t get_offset_of_intVariable_17() { return static_cast<int32_t>(offsetof(GUILayoutIntLabel_t1298772968, ___intVariable_17)); }
	inline FsmInt_t874273141 * get_intVariable_17() const { return ___intVariable_17; }
	inline FsmInt_t874273141 ** get_address_of_intVariable_17() { return &___intVariable_17; }
	inline void set_intVariable_17(FsmInt_t874273141 * value)
	{
		___intVariable_17 = value;
		Il2CppCodeGenWriteBarrier((&___intVariable_17), value);
	}

	inline static int32_t get_offset_of_style_18() { return static_cast<int32_t>(offsetof(GUILayoutIntLabel_t1298772968, ___style_18)); }
	inline FsmString_t1785915204 * get_style_18() const { return ___style_18; }
	inline FsmString_t1785915204 ** get_address_of_style_18() { return &___style_18; }
	inline void set_style_18(FsmString_t1785915204 * value)
	{
		___style_18 = value;
		Il2CppCodeGenWriteBarrier((&___style_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTINTLABEL_T1298772968_H
#ifndef GUILAYOUTLABEL_T1180917421_H
#define GUILAYOUTLABEL_T1180917421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutLabel
struct  GUILayoutLabel_t1180917421  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutLabel::image
	FsmTexture_t1738787045 * ___image_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutLabel::text
	FsmString_t1785915204 * ___text_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutLabel::tooltip
	FsmString_t1785915204 * ___tooltip_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutLabel::style
	FsmString_t1785915204 * ___style_19;

public:
	inline static int32_t get_offset_of_image_16() { return static_cast<int32_t>(offsetof(GUILayoutLabel_t1180917421, ___image_16)); }
	inline FsmTexture_t1738787045 * get_image_16() const { return ___image_16; }
	inline FsmTexture_t1738787045 ** get_address_of_image_16() { return &___image_16; }
	inline void set_image_16(FsmTexture_t1738787045 * value)
	{
		___image_16 = value;
		Il2CppCodeGenWriteBarrier((&___image_16), value);
	}

	inline static int32_t get_offset_of_text_17() { return static_cast<int32_t>(offsetof(GUILayoutLabel_t1180917421, ___text_17)); }
	inline FsmString_t1785915204 * get_text_17() const { return ___text_17; }
	inline FsmString_t1785915204 ** get_address_of_text_17() { return &___text_17; }
	inline void set_text_17(FsmString_t1785915204 * value)
	{
		___text_17 = value;
		Il2CppCodeGenWriteBarrier((&___text_17), value);
	}

	inline static int32_t get_offset_of_tooltip_18() { return static_cast<int32_t>(offsetof(GUILayoutLabel_t1180917421, ___tooltip_18)); }
	inline FsmString_t1785915204 * get_tooltip_18() const { return ___tooltip_18; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_18() { return &___tooltip_18; }
	inline void set_tooltip_18(FsmString_t1785915204 * value)
	{
		___tooltip_18 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_18), value);
	}

	inline static int32_t get_offset_of_style_19() { return static_cast<int32_t>(offsetof(GUILayoutLabel_t1180917421, ___style_19)); }
	inline FsmString_t1785915204 * get_style_19() const { return ___style_19; }
	inline FsmString_t1785915204 ** get_address_of_style_19() { return &___style_19; }
	inline void set_style_19(FsmString_t1785915204 * value)
	{
		___style_19 = value;
		Il2CppCodeGenWriteBarrier((&___style_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTLABEL_T1180917421_H
#ifndef GUILAYOUTPASSWORDFIELD_T2608979881_H
#define GUILAYOUTPASSWORDFIELD_T2608979881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutPasswordField
struct  GUILayoutPasswordField_t2608979881  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutPasswordField::text
	FsmString_t1785915204 * ___text_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutPasswordField::maxLength
	FsmInt_t874273141 * ___maxLength_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutPasswordField::style
	FsmString_t1785915204 * ___style_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutPasswordField::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutPasswordField::mask
	FsmString_t1785915204 * ___mask_20;

public:
	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(GUILayoutPasswordField_t2608979881, ___text_16)); }
	inline FsmString_t1785915204 * get_text_16() const { return ___text_16; }
	inline FsmString_t1785915204 ** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(FsmString_t1785915204 * value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}

	inline static int32_t get_offset_of_maxLength_17() { return static_cast<int32_t>(offsetof(GUILayoutPasswordField_t2608979881, ___maxLength_17)); }
	inline FsmInt_t874273141 * get_maxLength_17() const { return ___maxLength_17; }
	inline FsmInt_t874273141 ** get_address_of_maxLength_17() { return &___maxLength_17; }
	inline void set_maxLength_17(FsmInt_t874273141 * value)
	{
		___maxLength_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxLength_17), value);
	}

	inline static int32_t get_offset_of_style_18() { return static_cast<int32_t>(offsetof(GUILayoutPasswordField_t2608979881, ___style_18)); }
	inline FsmString_t1785915204 * get_style_18() const { return ___style_18; }
	inline FsmString_t1785915204 ** get_address_of_style_18() { return &___style_18; }
	inline void set_style_18(FsmString_t1785915204 * value)
	{
		___style_18 = value;
		Il2CppCodeGenWriteBarrier((&___style_18), value);
	}

	inline static int32_t get_offset_of_changedEvent_19() { return static_cast<int32_t>(offsetof(GUILayoutPasswordField_t2608979881, ___changedEvent_19)); }
	inline FsmEvent_t3736299882 * get_changedEvent_19() const { return ___changedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_19() { return &___changedEvent_19; }
	inline void set_changedEvent_19(FsmEvent_t3736299882 * value)
	{
		___changedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_19), value);
	}

	inline static int32_t get_offset_of_mask_20() { return static_cast<int32_t>(offsetof(GUILayoutPasswordField_t2608979881, ___mask_20)); }
	inline FsmString_t1785915204 * get_mask_20() const { return ___mask_20; }
	inline FsmString_t1785915204 ** get_address_of_mask_20() { return &___mask_20; }
	inline void set_mask_20(FsmString_t1785915204 * value)
	{
		___mask_20 = value;
		Il2CppCodeGenWriteBarrier((&___mask_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTPASSWORDFIELD_T2608979881_H
#ifndef GUILAYOUTREPEATBUTTON_T642856395_H
#define GUILAYOUTREPEATBUTTON_T642856395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutRepeatButton
struct  GUILayoutRepeatButton_t642856395  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::sendEvent
	FsmEvent_t3736299882 * ___sendEvent_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::storeButtonState
	FsmBool_t163807967 * ___storeButtonState_17;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::image
	FsmTexture_t1738787045 * ___image_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::text
	FsmString_t1785915204 * ___text_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::tooltip
	FsmString_t1785915204 * ___tooltip_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutRepeatButton::style
	FsmString_t1785915204 * ___style_21;

public:
	inline static int32_t get_offset_of_sendEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t642856395, ___sendEvent_16)); }
	inline FsmEvent_t3736299882 * get_sendEvent_16() const { return ___sendEvent_16; }
	inline FsmEvent_t3736299882 ** get_address_of_sendEvent_16() { return &___sendEvent_16; }
	inline void set_sendEvent_16(FsmEvent_t3736299882 * value)
	{
		___sendEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___sendEvent_16), value);
	}

	inline static int32_t get_offset_of_storeButtonState_17() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t642856395, ___storeButtonState_17)); }
	inline FsmBool_t163807967 * get_storeButtonState_17() const { return ___storeButtonState_17; }
	inline FsmBool_t163807967 ** get_address_of_storeButtonState_17() { return &___storeButtonState_17; }
	inline void set_storeButtonState_17(FsmBool_t163807967 * value)
	{
		___storeButtonState_17 = value;
		Il2CppCodeGenWriteBarrier((&___storeButtonState_17), value);
	}

	inline static int32_t get_offset_of_image_18() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t642856395, ___image_18)); }
	inline FsmTexture_t1738787045 * get_image_18() const { return ___image_18; }
	inline FsmTexture_t1738787045 ** get_address_of_image_18() { return &___image_18; }
	inline void set_image_18(FsmTexture_t1738787045 * value)
	{
		___image_18 = value;
		Il2CppCodeGenWriteBarrier((&___image_18), value);
	}

	inline static int32_t get_offset_of_text_19() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t642856395, ___text_19)); }
	inline FsmString_t1785915204 * get_text_19() const { return ___text_19; }
	inline FsmString_t1785915204 ** get_address_of_text_19() { return &___text_19; }
	inline void set_text_19(FsmString_t1785915204 * value)
	{
		___text_19 = value;
		Il2CppCodeGenWriteBarrier((&___text_19), value);
	}

	inline static int32_t get_offset_of_tooltip_20() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t642856395, ___tooltip_20)); }
	inline FsmString_t1785915204 * get_tooltip_20() const { return ___tooltip_20; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_20() { return &___tooltip_20; }
	inline void set_tooltip_20(FsmString_t1785915204 * value)
	{
		___tooltip_20 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_20), value);
	}

	inline static int32_t get_offset_of_style_21() { return static_cast<int32_t>(offsetof(GUILayoutRepeatButton_t642856395, ___style_21)); }
	inline FsmString_t1785915204 * get_style_21() const { return ___style_21; }
	inline FsmString_t1785915204 ** get_address_of_style_21() { return &___style_21; }
	inline void set_style_21(FsmString_t1785915204 * value)
	{
		___style_21 = value;
		Il2CppCodeGenWriteBarrier((&___style_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTREPEATBUTTON_T642856395_H
#ifndef GUILAYOUTTEXTFIELD_T4093699252_H
#define GUILAYOUTTEXTFIELD_T4093699252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutTextField
struct  GUILayoutTextField_t4093699252  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextField::text
	FsmString_t1785915204 * ___text_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutTextField::maxLength
	FsmInt_t874273141 * ___maxLength_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextField::style
	FsmString_t1785915204 * ___style_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutTextField::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_19;

public:
	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t4093699252, ___text_16)); }
	inline FsmString_t1785915204 * get_text_16() const { return ___text_16; }
	inline FsmString_t1785915204 ** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(FsmString_t1785915204 * value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}

	inline static int32_t get_offset_of_maxLength_17() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t4093699252, ___maxLength_17)); }
	inline FsmInt_t874273141 * get_maxLength_17() const { return ___maxLength_17; }
	inline FsmInt_t874273141 ** get_address_of_maxLength_17() { return &___maxLength_17; }
	inline void set_maxLength_17(FsmInt_t874273141 * value)
	{
		___maxLength_17 = value;
		Il2CppCodeGenWriteBarrier((&___maxLength_17), value);
	}

	inline static int32_t get_offset_of_style_18() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t4093699252, ___style_18)); }
	inline FsmString_t1785915204 * get_style_18() const { return ___style_18; }
	inline FsmString_t1785915204 ** get_address_of_style_18() { return &___style_18; }
	inline void set_style_18(FsmString_t1785915204 * value)
	{
		___style_18 = value;
		Il2CppCodeGenWriteBarrier((&___style_18), value);
	}

	inline static int32_t get_offset_of_changedEvent_19() { return static_cast<int32_t>(offsetof(GUILayoutTextField_t4093699252, ___changedEvent_19)); }
	inline FsmEvent_t3736299882 * get_changedEvent_19() const { return ___changedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_19() { return &___changedEvent_19; }
	inline void set_changedEvent_19(FsmEvent_t3736299882 * value)
	{
		___changedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTTEXTFIELD_T4093699252_H
#ifndef GUILAYOUTTEXTLABEL_T882428745_H
#define GUILAYOUTTEXTLABEL_T882428745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutTextLabel
struct  GUILayoutTextLabel_t882428745  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextLabel::text
	FsmString_t1785915204 * ___text_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutTextLabel::style
	FsmString_t1785915204 * ___style_17;

public:
	inline static int32_t get_offset_of_text_16() { return static_cast<int32_t>(offsetof(GUILayoutTextLabel_t882428745, ___text_16)); }
	inline FsmString_t1785915204 * get_text_16() const { return ___text_16; }
	inline FsmString_t1785915204 ** get_address_of_text_16() { return &___text_16; }
	inline void set_text_16(FsmString_t1785915204 * value)
	{
		___text_16 = value;
		Il2CppCodeGenWriteBarrier((&___text_16), value);
	}

	inline static int32_t get_offset_of_style_17() { return static_cast<int32_t>(offsetof(GUILayoutTextLabel_t882428745, ___style_17)); }
	inline FsmString_t1785915204 * get_style_17() const { return ___style_17; }
	inline FsmString_t1785915204 ** get_address_of_style_17() { return &___style_17; }
	inline void set_style_17(FsmString_t1785915204 * value)
	{
		___style_17 = value;
		Il2CppCodeGenWriteBarrier((&___style_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTTEXTLABEL_T882428745_H
#ifndef GUILAYOUTTOGGLE_T251445422_H
#define GUILAYOUTTOGGLE_T251445422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutToggle
struct  GUILayoutToggle_t251445422  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutToggle::storeButtonState
	FsmBool_t163807967 * ___storeButtonState_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GUILayoutToggle::image
	FsmTexture_t1738787045 * ___image_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToggle::text
	FsmString_t1785915204 * ___text_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToggle::tooltip
	FsmString_t1785915204 * ___tooltip_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToggle::style
	FsmString_t1785915204 * ___style_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutToggle::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_21;

public:
	inline static int32_t get_offset_of_storeButtonState_16() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t251445422, ___storeButtonState_16)); }
	inline FsmBool_t163807967 * get_storeButtonState_16() const { return ___storeButtonState_16; }
	inline FsmBool_t163807967 ** get_address_of_storeButtonState_16() { return &___storeButtonState_16; }
	inline void set_storeButtonState_16(FsmBool_t163807967 * value)
	{
		___storeButtonState_16 = value;
		Il2CppCodeGenWriteBarrier((&___storeButtonState_16), value);
	}

	inline static int32_t get_offset_of_image_17() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t251445422, ___image_17)); }
	inline FsmTexture_t1738787045 * get_image_17() const { return ___image_17; }
	inline FsmTexture_t1738787045 ** get_address_of_image_17() { return &___image_17; }
	inline void set_image_17(FsmTexture_t1738787045 * value)
	{
		___image_17 = value;
		Il2CppCodeGenWriteBarrier((&___image_17), value);
	}

	inline static int32_t get_offset_of_text_18() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t251445422, ___text_18)); }
	inline FsmString_t1785915204 * get_text_18() const { return ___text_18; }
	inline FsmString_t1785915204 ** get_address_of_text_18() { return &___text_18; }
	inline void set_text_18(FsmString_t1785915204 * value)
	{
		___text_18 = value;
		Il2CppCodeGenWriteBarrier((&___text_18), value);
	}

	inline static int32_t get_offset_of_tooltip_19() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t251445422, ___tooltip_19)); }
	inline FsmString_t1785915204 * get_tooltip_19() const { return ___tooltip_19; }
	inline FsmString_t1785915204 ** get_address_of_tooltip_19() { return &___tooltip_19; }
	inline void set_tooltip_19(FsmString_t1785915204 * value)
	{
		___tooltip_19 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_19), value);
	}

	inline static int32_t get_offset_of_style_20() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t251445422, ___style_20)); }
	inline FsmString_t1785915204 * get_style_20() const { return ___style_20; }
	inline FsmString_t1785915204 ** get_address_of_style_20() { return &___style_20; }
	inline void set_style_20(FsmString_t1785915204 * value)
	{
		___style_20 = value;
		Il2CppCodeGenWriteBarrier((&___style_20), value);
	}

	inline static int32_t get_offset_of_changedEvent_21() { return static_cast<int32_t>(offsetof(GUILayoutToggle_t251445422, ___changedEvent_21)); }
	inline FsmEvent_t3736299882 * get_changedEvent_21() const { return ___changedEvent_21; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_21() { return &___changedEvent_21; }
	inline void set_changedEvent_21(FsmEvent_t3736299882 * value)
	{
		___changedEvent_21 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTTOGGLE_T251445422_H
#ifndef GUILAYOUTTOOLBAR_T3789590645_H
#define GUILAYOUTTOOLBAR_T3789590645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutToolbar
struct  GUILayoutToolbar_t3789590645  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutToolbar::numButtons
	FsmInt_t874273141 * ___numButtons_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutToolbar::selectedButton
	FsmInt_t874273141 * ___selectedButton_17;
	// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::buttonEventsArray
	FsmEventU5BU5D_t2511618479* ___buttonEventsArray_18;
	// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::imagesArray
	FsmTextureU5BU5D_t2313143784* ___imagesArray_19;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::textsArray
	FsmStringU5BU5D_t252501805* ___textsArray_20;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::tooltipsArray
	FsmStringU5BU5D_t252501805* ___tooltipsArray_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutToolbar::style
	FsmString_t1785915204 * ___style_22;
	// System.Boolean HutongGames.PlayMaker.Actions.GUILayoutToolbar::everyFrame
	bool ___everyFrame_23;
	// UnityEngine.GUIContent[] HutongGames.PlayMaker.Actions.GUILayoutToolbar::contents
	GUIContentU5BU5D_t2445521510* ___contents_24;

public:
	inline static int32_t get_offset_of_numButtons_16() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___numButtons_16)); }
	inline FsmInt_t874273141 * get_numButtons_16() const { return ___numButtons_16; }
	inline FsmInt_t874273141 ** get_address_of_numButtons_16() { return &___numButtons_16; }
	inline void set_numButtons_16(FsmInt_t874273141 * value)
	{
		___numButtons_16 = value;
		Il2CppCodeGenWriteBarrier((&___numButtons_16), value);
	}

	inline static int32_t get_offset_of_selectedButton_17() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___selectedButton_17)); }
	inline FsmInt_t874273141 * get_selectedButton_17() const { return ___selectedButton_17; }
	inline FsmInt_t874273141 ** get_address_of_selectedButton_17() { return &___selectedButton_17; }
	inline void set_selectedButton_17(FsmInt_t874273141 * value)
	{
		___selectedButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectedButton_17), value);
	}

	inline static int32_t get_offset_of_buttonEventsArray_18() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___buttonEventsArray_18)); }
	inline FsmEventU5BU5D_t2511618479* get_buttonEventsArray_18() const { return ___buttonEventsArray_18; }
	inline FsmEventU5BU5D_t2511618479** get_address_of_buttonEventsArray_18() { return &___buttonEventsArray_18; }
	inline void set_buttonEventsArray_18(FsmEventU5BU5D_t2511618479* value)
	{
		___buttonEventsArray_18 = value;
		Il2CppCodeGenWriteBarrier((&___buttonEventsArray_18), value);
	}

	inline static int32_t get_offset_of_imagesArray_19() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___imagesArray_19)); }
	inline FsmTextureU5BU5D_t2313143784* get_imagesArray_19() const { return ___imagesArray_19; }
	inline FsmTextureU5BU5D_t2313143784** get_address_of_imagesArray_19() { return &___imagesArray_19; }
	inline void set_imagesArray_19(FsmTextureU5BU5D_t2313143784* value)
	{
		___imagesArray_19 = value;
		Il2CppCodeGenWriteBarrier((&___imagesArray_19), value);
	}

	inline static int32_t get_offset_of_textsArray_20() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___textsArray_20)); }
	inline FsmStringU5BU5D_t252501805* get_textsArray_20() const { return ___textsArray_20; }
	inline FsmStringU5BU5D_t252501805** get_address_of_textsArray_20() { return &___textsArray_20; }
	inline void set_textsArray_20(FsmStringU5BU5D_t252501805* value)
	{
		___textsArray_20 = value;
		Il2CppCodeGenWriteBarrier((&___textsArray_20), value);
	}

	inline static int32_t get_offset_of_tooltipsArray_21() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___tooltipsArray_21)); }
	inline FsmStringU5BU5D_t252501805* get_tooltipsArray_21() const { return ___tooltipsArray_21; }
	inline FsmStringU5BU5D_t252501805** get_address_of_tooltipsArray_21() { return &___tooltipsArray_21; }
	inline void set_tooltipsArray_21(FsmStringU5BU5D_t252501805* value)
	{
		___tooltipsArray_21 = value;
		Il2CppCodeGenWriteBarrier((&___tooltipsArray_21), value);
	}

	inline static int32_t get_offset_of_style_22() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___style_22)); }
	inline FsmString_t1785915204 * get_style_22() const { return ___style_22; }
	inline FsmString_t1785915204 ** get_address_of_style_22() { return &___style_22; }
	inline void set_style_22(FsmString_t1785915204 * value)
	{
		___style_22 = value;
		Il2CppCodeGenWriteBarrier((&___style_22), value);
	}

	inline static int32_t get_offset_of_everyFrame_23() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___everyFrame_23)); }
	inline bool get_everyFrame_23() const { return ___everyFrame_23; }
	inline bool* get_address_of_everyFrame_23() { return &___everyFrame_23; }
	inline void set_everyFrame_23(bool value)
	{
		___everyFrame_23 = value;
	}

	inline static int32_t get_offset_of_contents_24() { return static_cast<int32_t>(offsetof(GUILayoutToolbar_t3789590645, ___contents_24)); }
	inline GUIContentU5BU5D_t2445521510* get_contents_24() const { return ___contents_24; }
	inline GUIContentU5BU5D_t2445521510** get_address_of_contents_24() { return &___contents_24; }
	inline void set_contents_24(GUIContentU5BU5D_t2445521510* value)
	{
		___contents_24 = value;
		Il2CppCodeGenWriteBarrier((&___contents_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTTOOLBAR_T3789590645_H
#ifndef GUILAYOUTVERTICALSLIDER_T1672453969_H
#define GUILAYOUTVERTICALSLIDER_T1672453969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider
struct  GUILayoutVerticalSlider_t1672453969  : public GUILayoutAction_t1272787235
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::floatVariable
	FsmFloat_t2883254149 * ___floatVariable_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::topValue
	FsmFloat_t2883254149 * ___topValue_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::bottomValue
	FsmFloat_t2883254149 * ___bottomValue_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::changedEvent
	FsmEvent_t3736299882 * ___changedEvent_19;

public:
	inline static int32_t get_offset_of_floatVariable_16() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1672453969, ___floatVariable_16)); }
	inline FsmFloat_t2883254149 * get_floatVariable_16() const { return ___floatVariable_16; }
	inline FsmFloat_t2883254149 ** get_address_of_floatVariable_16() { return &___floatVariable_16; }
	inline void set_floatVariable_16(FsmFloat_t2883254149 * value)
	{
		___floatVariable_16 = value;
		Il2CppCodeGenWriteBarrier((&___floatVariable_16), value);
	}

	inline static int32_t get_offset_of_topValue_17() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1672453969, ___topValue_17)); }
	inline FsmFloat_t2883254149 * get_topValue_17() const { return ___topValue_17; }
	inline FsmFloat_t2883254149 ** get_address_of_topValue_17() { return &___topValue_17; }
	inline void set_topValue_17(FsmFloat_t2883254149 * value)
	{
		___topValue_17 = value;
		Il2CppCodeGenWriteBarrier((&___topValue_17), value);
	}

	inline static int32_t get_offset_of_bottomValue_18() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1672453969, ___bottomValue_18)); }
	inline FsmFloat_t2883254149 * get_bottomValue_18() const { return ___bottomValue_18; }
	inline FsmFloat_t2883254149 ** get_address_of_bottomValue_18() { return &___bottomValue_18; }
	inline void set_bottomValue_18(FsmFloat_t2883254149 * value)
	{
		___bottomValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___bottomValue_18), value);
	}

	inline static int32_t get_offset_of_changedEvent_19() { return static_cast<int32_t>(offsetof(GUILayoutVerticalSlider_t1672453969, ___changedEvent_19)); }
	inline FsmEvent_t3736299882 * get_changedEvent_19() const { return ___changedEvent_19; }
	inline FsmEvent_t3736299882 ** get_address_of_changedEvent_19() { return &___changedEvent_19; }
	inline void set_changedEvent_19(FsmEvent_t3736299882 * value)
	{
		___changedEvent_19 = value;
		Il2CppCodeGenWriteBarrier((&___changedEvent_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTVERTICALSLIDER_T1672453969_H
#ifndef GAMEOBJECTISVISIBLE_T2925201815_H
#define GAMEOBJECTISVISIBLE_T2925201815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsVisible
struct  GameObjectIsVisible_t2925201815  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GameObjectIsVisible::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsVisible::trueEvent
	FsmEvent_t3736299882 * ___trueEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsVisible::falseEvent
	FsmEvent_t3736299882 * ___falseEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsVisible::storeResult
	FsmBool_t163807967 * ___storeResult_19;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectIsVisible::everyFrame
	bool ___everyFrame_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GameObjectIsVisible_t2925201815, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_trueEvent_17() { return static_cast<int32_t>(offsetof(GameObjectIsVisible_t2925201815, ___trueEvent_17)); }
	inline FsmEvent_t3736299882 * get_trueEvent_17() const { return ___trueEvent_17; }
	inline FsmEvent_t3736299882 ** get_address_of_trueEvent_17() { return &___trueEvent_17; }
	inline void set_trueEvent_17(FsmEvent_t3736299882 * value)
	{
		___trueEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___trueEvent_17), value);
	}

	inline static int32_t get_offset_of_falseEvent_18() { return static_cast<int32_t>(offsetof(GameObjectIsVisible_t2925201815, ___falseEvent_18)); }
	inline FsmEvent_t3736299882 * get_falseEvent_18() const { return ___falseEvent_18; }
	inline FsmEvent_t3736299882 ** get_address_of_falseEvent_18() { return &___falseEvent_18; }
	inline void set_falseEvent_18(FsmEvent_t3736299882 * value)
	{
		___falseEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___falseEvent_18), value);
	}

	inline static int32_t get_offset_of_storeResult_19() { return static_cast<int32_t>(offsetof(GameObjectIsVisible_t2925201815, ___storeResult_19)); }
	inline FsmBool_t163807967 * get_storeResult_19() const { return ___storeResult_19; }
	inline FsmBool_t163807967 ** get_address_of_storeResult_19() { return &___storeResult_19; }
	inline void set_storeResult_19(FsmBool_t163807967 * value)
	{
		___storeResult_19 = value;
		Il2CppCodeGenWriteBarrier((&___storeResult_19), value);
	}

	inline static int32_t get_offset_of_everyFrame_20() { return static_cast<int32_t>(offsetof(GameObjectIsVisible_t2925201815, ___everyFrame_20)); }
	inline bool get_everyFrame_20() const { return ___everyFrame_20; }
	inline bool* get_address_of_everyFrame_20() { return &___everyFrame_20; }
	inline void set_everyFrame_20(bool value)
	{
		___everyFrame_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTISVISIBLE_T2925201815_H
#ifndef GETMATERIAL_T2816252130_H
#define GETMATERIAL_T2816252130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMaterial
struct  GetMaterial_t2816252130  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetMaterial::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetMaterial::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.GetMaterial::material
	FsmMaterial_t2057874452 * ___material_18;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMaterial::getSharedMaterial
	bool ___getSharedMaterial_19;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetMaterial_t2816252130, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(GetMaterial_t2816252130, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(GetMaterial_t2816252130, ___material_18)); }
	inline FsmMaterial_t2057874452 * get_material_18() const { return ___material_18; }
	inline FsmMaterial_t2057874452 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(FsmMaterial_t2057874452 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}

	inline static int32_t get_offset_of_getSharedMaterial_19() { return static_cast<int32_t>(offsetof(GetMaterial_t2816252130, ___getSharedMaterial_19)); }
	inline bool get_getSharedMaterial_19() const { return ___getSharedMaterial_19; }
	inline bool* get_address_of_getSharedMaterial_19() { return &___getSharedMaterial_19; }
	inline void set_getSharedMaterial_19(bool value)
	{
		___getSharedMaterial_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMATERIAL_T2816252130_H
#ifndef GETMATERIALTEXTURE_T1739971042_H
#define GETMATERIALTEXTURE_T1739971042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetMaterialTexture
struct  GetMaterialTexture_t1739971042  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.GetMaterialTexture::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetMaterialTexture::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetMaterialTexture::namedTexture
	FsmString_t1785915204 * ___namedTexture_18;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.GetMaterialTexture::storedTexture
	FsmTexture_t1738787045 * ___storedTexture_19;
	// System.Boolean HutongGames.PlayMaker.Actions.GetMaterialTexture::getFromSharedMaterial
	bool ___getFromSharedMaterial_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(GetMaterialTexture_t1739971042, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(GetMaterialTexture_t1739971042, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_namedTexture_18() { return static_cast<int32_t>(offsetof(GetMaterialTexture_t1739971042, ___namedTexture_18)); }
	inline FsmString_t1785915204 * get_namedTexture_18() const { return ___namedTexture_18; }
	inline FsmString_t1785915204 ** get_address_of_namedTexture_18() { return &___namedTexture_18; }
	inline void set_namedTexture_18(FsmString_t1785915204 * value)
	{
		___namedTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___namedTexture_18), value);
	}

	inline static int32_t get_offset_of_storedTexture_19() { return static_cast<int32_t>(offsetof(GetMaterialTexture_t1739971042, ___storedTexture_19)); }
	inline FsmTexture_t1738787045 * get_storedTexture_19() const { return ___storedTexture_19; }
	inline FsmTexture_t1738787045 ** get_address_of_storedTexture_19() { return &___storedTexture_19; }
	inline void set_storedTexture_19(FsmTexture_t1738787045 * value)
	{
		___storedTexture_19 = value;
		Il2CppCodeGenWriteBarrier((&___storedTexture_19), value);
	}

	inline static int32_t get_offset_of_getFromSharedMaterial_20() { return static_cast<int32_t>(offsetof(GetMaterialTexture_t1739971042, ___getFromSharedMaterial_20)); }
	inline bool get_getFromSharedMaterial_20() const { return ___getFromSharedMaterial_20; }
	inline bool* get_address_of_getFromSharedMaterial_20() { return &___getFromSharedMaterial_20; }
	inline void set_getFromSharedMaterial_20(bool value)
	{
		___getFromSharedMaterial_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMATERIALTEXTURE_T1739971042_H
#ifndef MOUSELOOK2_T4105412593_H
#define MOUSELOOK2_T4105412593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MouseLook2
struct  MouseLook2_t4105412593  : public ComponentAction_1_t2499100150
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.MouseLook2::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.Actions.MouseLook2/RotationAxes HutongGames.PlayMaker.Actions.MouseLook2::axes
	int32_t ___axes_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook2::sensitivityX
	FsmFloat_t2883254149 * ___sensitivityX_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook2::sensitivityY
	FsmFloat_t2883254149 * ___sensitivityY_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook2::minimumX
	FsmFloat_t2883254149 * ___minimumX_20;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook2::maximumX
	FsmFloat_t2883254149 * ___maximumX_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook2::minimumY
	FsmFloat_t2883254149 * ___minimumY_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.MouseLook2::maximumY
	FsmFloat_t2883254149 * ___maximumY_23;
	// System.Boolean HutongGames.PlayMaker.Actions.MouseLook2::everyFrame
	bool ___everyFrame_24;
	// System.Single HutongGames.PlayMaker.Actions.MouseLook2::rotationX
	float ___rotationX_25;
	// System.Single HutongGames.PlayMaker.Actions.MouseLook2::rotationY
	float ___rotationY_26;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_axes_17() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___axes_17)); }
	inline int32_t get_axes_17() const { return ___axes_17; }
	inline int32_t* get_address_of_axes_17() { return &___axes_17; }
	inline void set_axes_17(int32_t value)
	{
		___axes_17 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_18() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___sensitivityX_18)); }
	inline FsmFloat_t2883254149 * get_sensitivityX_18() const { return ___sensitivityX_18; }
	inline FsmFloat_t2883254149 ** get_address_of_sensitivityX_18() { return &___sensitivityX_18; }
	inline void set_sensitivityX_18(FsmFloat_t2883254149 * value)
	{
		___sensitivityX_18 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityX_18), value);
	}

	inline static int32_t get_offset_of_sensitivityY_19() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___sensitivityY_19)); }
	inline FsmFloat_t2883254149 * get_sensitivityY_19() const { return ___sensitivityY_19; }
	inline FsmFloat_t2883254149 ** get_address_of_sensitivityY_19() { return &___sensitivityY_19; }
	inline void set_sensitivityY_19(FsmFloat_t2883254149 * value)
	{
		___sensitivityY_19 = value;
		Il2CppCodeGenWriteBarrier((&___sensitivityY_19), value);
	}

	inline static int32_t get_offset_of_minimumX_20() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___minimumX_20)); }
	inline FsmFloat_t2883254149 * get_minimumX_20() const { return ___minimumX_20; }
	inline FsmFloat_t2883254149 ** get_address_of_minimumX_20() { return &___minimumX_20; }
	inline void set_minimumX_20(FsmFloat_t2883254149 * value)
	{
		___minimumX_20 = value;
		Il2CppCodeGenWriteBarrier((&___minimumX_20), value);
	}

	inline static int32_t get_offset_of_maximumX_21() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___maximumX_21)); }
	inline FsmFloat_t2883254149 * get_maximumX_21() const { return ___maximumX_21; }
	inline FsmFloat_t2883254149 ** get_address_of_maximumX_21() { return &___maximumX_21; }
	inline void set_maximumX_21(FsmFloat_t2883254149 * value)
	{
		___maximumX_21 = value;
		Il2CppCodeGenWriteBarrier((&___maximumX_21), value);
	}

	inline static int32_t get_offset_of_minimumY_22() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___minimumY_22)); }
	inline FsmFloat_t2883254149 * get_minimumY_22() const { return ___minimumY_22; }
	inline FsmFloat_t2883254149 ** get_address_of_minimumY_22() { return &___minimumY_22; }
	inline void set_minimumY_22(FsmFloat_t2883254149 * value)
	{
		___minimumY_22 = value;
		Il2CppCodeGenWriteBarrier((&___minimumY_22), value);
	}

	inline static int32_t get_offset_of_maximumY_23() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___maximumY_23)); }
	inline FsmFloat_t2883254149 * get_maximumY_23() const { return ___maximumY_23; }
	inline FsmFloat_t2883254149 ** get_address_of_maximumY_23() { return &___maximumY_23; }
	inline void set_maximumY_23(FsmFloat_t2883254149 * value)
	{
		___maximumY_23 = value;
		Il2CppCodeGenWriteBarrier((&___maximumY_23), value);
	}

	inline static int32_t get_offset_of_everyFrame_24() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___everyFrame_24)); }
	inline bool get_everyFrame_24() const { return ___everyFrame_24; }
	inline bool* get_address_of_everyFrame_24() { return &___everyFrame_24; }
	inline void set_everyFrame_24(bool value)
	{
		___everyFrame_24 = value;
	}

	inline static int32_t get_offset_of_rotationX_25() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___rotationX_25)); }
	inline float get_rotationX_25() const { return ___rotationX_25; }
	inline float* get_address_of_rotationX_25() { return &___rotationX_25; }
	inline void set_rotationX_25(float value)
	{
		___rotationX_25 = value;
	}

	inline static int32_t get_offset_of_rotationY_26() { return static_cast<int32_t>(offsetof(MouseLook2_t4105412593, ___rotationY_26)); }
	inline float get_rotationY_26() const { return ___rotationY_26; }
	inline float* get_address_of_rotationY_26() { return &___rotationY_26; }
	inline void set_rotationY_26(float value)
	{
		___rotationY_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSELOOK2_T4105412593_H
#ifndef SETLIGHTCOLOR_T4240775384_H
#define SETLIGHTCOLOR_T4240775384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightColor
struct  SetLightColor_t4240775384  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetLightColor::lightColor
	FsmColor_t1738900188 * ___lightColor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetLightColor::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightColor_t4240775384, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightColor_17() { return static_cast<int32_t>(offsetof(SetLightColor_t4240775384, ___lightColor_17)); }
	inline FsmColor_t1738900188 * get_lightColor_17() const { return ___lightColor_17; }
	inline FsmColor_t1738900188 ** get_address_of_lightColor_17() { return &___lightColor_17; }
	inline void set_lightColor_17(FsmColor_t1738900188 * value)
	{
		___lightColor_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightColor_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetLightColor_t4240775384, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTCOLOR_T4240775384_H
#ifndef SETLIGHTCOOKIE_T3154943726_H
#define SETLIGHTCOOKIE_T3154943726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightCookie
struct  SetLightCookie_t3154943726  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightCookie::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetLightCookie::lightCookie
	FsmTexture_t1738787045 * ___lightCookie_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightCookie_t3154943726, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightCookie_17() { return static_cast<int32_t>(offsetof(SetLightCookie_t3154943726, ___lightCookie_17)); }
	inline FsmTexture_t1738787045 * get_lightCookie_17() const { return ___lightCookie_17; }
	inline FsmTexture_t1738787045 ** get_address_of_lightCookie_17() { return &___lightCookie_17; }
	inline void set_lightCookie_17(FsmTexture_t1738787045 * value)
	{
		___lightCookie_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightCookie_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTCOOKIE_T3154943726_H
#ifndef SETLIGHTFLARE_T2891235521_H
#define SETLIGHTFLARE_T2891235521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightFlare
struct  SetLightFlare_t2891235521  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightFlare::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// UnityEngine.Flare HutongGames.PlayMaker.Actions.SetLightFlare::lightFlare
	Flare_t1765167167 * ___lightFlare_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightFlare_t2891235521, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightFlare_17() { return static_cast<int32_t>(offsetof(SetLightFlare_t2891235521, ___lightFlare_17)); }
	inline Flare_t1765167167 * get_lightFlare_17() const { return ___lightFlare_17; }
	inline Flare_t1765167167 ** get_address_of_lightFlare_17() { return &___lightFlare_17; }
	inline void set_lightFlare_17(Flare_t1765167167 * value)
	{
		___lightFlare_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightFlare_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTFLARE_T2891235521_H
#ifndef SETLIGHTINTENSITY_T1541723886_H
#define SETLIGHTINTENSITY_T1541723886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightIntensity
struct  SetLightIntensity_t1541723886  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightIntensity::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetLightIntensity::lightIntensity
	FsmFloat_t2883254149 * ___lightIntensity_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetLightIntensity::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightIntensity_t1541723886, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightIntensity_17() { return static_cast<int32_t>(offsetof(SetLightIntensity_t1541723886, ___lightIntensity_17)); }
	inline FsmFloat_t2883254149 * get_lightIntensity_17() const { return ___lightIntensity_17; }
	inline FsmFloat_t2883254149 ** get_address_of_lightIntensity_17() { return &___lightIntensity_17; }
	inline void set_lightIntensity_17(FsmFloat_t2883254149 * value)
	{
		___lightIntensity_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightIntensity_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetLightIntensity_t1541723886, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTINTENSITY_T1541723886_H
#ifndef SETLIGHTRANGE_T1041883866_H
#define SETLIGHTRANGE_T1041883866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightRange
struct  SetLightRange_t1041883866  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightRange::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetLightRange::lightRange
	FsmFloat_t2883254149 * ___lightRange_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetLightRange::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightRange_t1041883866, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightRange_17() { return static_cast<int32_t>(offsetof(SetLightRange_t1041883866, ___lightRange_17)); }
	inline FsmFloat_t2883254149 * get_lightRange_17() const { return ___lightRange_17; }
	inline FsmFloat_t2883254149 ** get_address_of_lightRange_17() { return &___lightRange_17; }
	inline void set_lightRange_17(FsmFloat_t2883254149 * value)
	{
		___lightRange_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightRange_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetLightRange_t1041883866, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTRANGE_T1041883866_H
#ifndef SETLIGHTSPOTANGLE_T1865999512_H
#define SETLIGHTSPOTANGLE_T1865999512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightSpotAngle
struct  SetLightSpotAngle_t1865999512  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightSpotAngle::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetLightSpotAngle::lightSpotAngle
	FsmFloat_t2883254149 * ___lightSpotAngle_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetLightSpotAngle::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightSpotAngle_t1865999512, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightSpotAngle_17() { return static_cast<int32_t>(offsetof(SetLightSpotAngle_t1865999512, ___lightSpotAngle_17)); }
	inline FsmFloat_t2883254149 * get_lightSpotAngle_17() const { return ___lightSpotAngle_17; }
	inline FsmFloat_t2883254149 ** get_address_of_lightSpotAngle_17() { return &___lightSpotAngle_17; }
	inline void set_lightSpotAngle_17(FsmFloat_t2883254149 * value)
	{
		___lightSpotAngle_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightSpotAngle_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetLightSpotAngle_t1865999512, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTSPOTANGLE_T1865999512_H
#ifndef SETLIGHTTYPE_T1689465828_H
#define SETLIGHTTYPE_T1689465828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetLightType
struct  SetLightType_t1689465828  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetLightType::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.SetLightType::lightType
	FsmEnum_t2861764163 * ___lightType_17;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetLightType_t1689465828, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_lightType_17() { return static_cast<int32_t>(offsetof(SetLightType_t1689465828, ___lightType_17)); }
	inline FsmEnum_t2861764163 * get_lightType_17() const { return ___lightType_17; }
	inline FsmEnum_t2861764163 ** get_address_of_lightType_17() { return &___lightType_17; }
	inline void set_lightType_17(FsmEnum_t2861764163 * value)
	{
		___lightType_17 = value;
		Il2CppCodeGenWriteBarrier((&___lightType_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETLIGHTTYPE_T1689465828_H
#ifndef SETMATERIAL_T3715539006_H
#define SETMATERIAL_T3715539006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMaterial
struct  SetMaterial_t3715539006  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMaterial::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetMaterial::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetMaterial::material
	FsmMaterial_t2057874452 * ___material_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetMaterial_t3715539006, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetMaterial_t3715539006, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(SetMaterial_t3715539006, ___material_18)); }
	inline FsmMaterial_t2057874452 * get_material_18() const { return ___material_18; }
	inline FsmMaterial_t2057874452 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(FsmMaterial_t2057874452 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMATERIAL_T3715539006_H
#ifndef SETMATERIALCOLOR_T975309120_H
#define SETMATERIALCOLOR_T975309120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMaterialColor
struct  SetMaterialColor_t975309120  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMaterialColor::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetMaterialColor::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetMaterialColor::material
	FsmMaterial_t2057874452 * ___material_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetMaterialColor::namedColor
	FsmString_t1785915204 * ___namedColor_19;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetMaterialColor::color
	FsmColor_t1738900188 * ___color_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetMaterialColor::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetMaterialColor_t975309120, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetMaterialColor_t975309120, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(SetMaterialColor_t975309120, ___material_18)); }
	inline FsmMaterial_t2057874452 * get_material_18() const { return ___material_18; }
	inline FsmMaterial_t2057874452 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(FsmMaterial_t2057874452 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}

	inline static int32_t get_offset_of_namedColor_19() { return static_cast<int32_t>(offsetof(SetMaterialColor_t975309120, ___namedColor_19)); }
	inline FsmString_t1785915204 * get_namedColor_19() const { return ___namedColor_19; }
	inline FsmString_t1785915204 ** get_address_of_namedColor_19() { return &___namedColor_19; }
	inline void set_namedColor_19(FsmString_t1785915204 * value)
	{
		___namedColor_19 = value;
		Il2CppCodeGenWriteBarrier((&___namedColor_19), value);
	}

	inline static int32_t get_offset_of_color_20() { return static_cast<int32_t>(offsetof(SetMaterialColor_t975309120, ___color_20)); }
	inline FsmColor_t1738900188 * get_color_20() const { return ___color_20; }
	inline FsmColor_t1738900188 ** get_address_of_color_20() { return &___color_20; }
	inline void set_color_20(FsmColor_t1738900188 * value)
	{
		___color_20 = value;
		Il2CppCodeGenWriteBarrier((&___color_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetMaterialColor_t975309120, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMATERIALCOLOR_T975309120_H
#ifndef SETMATERIALFLOAT_T4126184611_H
#define SETMATERIALFLOAT_T4126184611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMaterialFloat
struct  SetMaterialFloat_t4126184611  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMaterialFloat::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetMaterialFloat::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetMaterialFloat::material
	FsmMaterial_t2057874452 * ___material_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetMaterialFloat::namedFloat
	FsmString_t1785915204 * ___namedFloat_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetMaterialFloat::floatValue
	FsmFloat_t2883254149 * ___floatValue_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetMaterialFloat::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t4126184611, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t4126184611, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t4126184611, ___material_18)); }
	inline FsmMaterial_t2057874452 * get_material_18() const { return ___material_18; }
	inline FsmMaterial_t2057874452 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(FsmMaterial_t2057874452 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}

	inline static int32_t get_offset_of_namedFloat_19() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t4126184611, ___namedFloat_19)); }
	inline FsmString_t1785915204 * get_namedFloat_19() const { return ___namedFloat_19; }
	inline FsmString_t1785915204 ** get_address_of_namedFloat_19() { return &___namedFloat_19; }
	inline void set_namedFloat_19(FsmString_t1785915204 * value)
	{
		___namedFloat_19 = value;
		Il2CppCodeGenWriteBarrier((&___namedFloat_19), value);
	}

	inline static int32_t get_offset_of_floatValue_20() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t4126184611, ___floatValue_20)); }
	inline FsmFloat_t2883254149 * get_floatValue_20() const { return ___floatValue_20; }
	inline FsmFloat_t2883254149 ** get_address_of_floatValue_20() { return &___floatValue_20; }
	inline void set_floatValue_20(FsmFloat_t2883254149 * value)
	{
		___floatValue_20 = value;
		Il2CppCodeGenWriteBarrier((&___floatValue_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetMaterialFloat_t4126184611, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMATERIALFLOAT_T4126184611_H
#ifndef SETMATERIALTEXTURE_T742727606_H
#define SETMATERIALTEXTURE_T742727606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetMaterialTexture
struct  SetMaterialTexture_t742727606  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetMaterialTexture::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetMaterialTexture::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetMaterialTexture::material
	FsmMaterial_t2057874452 * ___material_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetMaterialTexture::namedTexture
	FsmString_t1785915204 * ___namedTexture_19;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetMaterialTexture::texture
	FsmTexture_t1738787045 * ___texture_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t742727606, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t742727606, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_material_18() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t742727606, ___material_18)); }
	inline FsmMaterial_t2057874452 * get_material_18() const { return ___material_18; }
	inline FsmMaterial_t2057874452 ** get_address_of_material_18() { return &___material_18; }
	inline void set_material_18(FsmMaterial_t2057874452 * value)
	{
		___material_18 = value;
		Il2CppCodeGenWriteBarrier((&___material_18), value);
	}

	inline static int32_t get_offset_of_namedTexture_19() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t742727606, ___namedTexture_19)); }
	inline FsmString_t1785915204 * get_namedTexture_19() const { return ___namedTexture_19; }
	inline FsmString_t1785915204 ** get_address_of_namedTexture_19() { return &___namedTexture_19; }
	inline void set_namedTexture_19(FsmString_t1785915204 * value)
	{
		___namedTexture_19 = value;
		Il2CppCodeGenWriteBarrier((&___namedTexture_19), value);
	}

	inline static int32_t get_offset_of_texture_20() { return static_cast<int32_t>(offsetof(SetMaterialTexture_t742727606, ___texture_20)); }
	inline FsmTexture_t1738787045 * get_texture_20() const { return ___texture_20; }
	inline FsmTexture_t1738787045 ** get_address_of_texture_20() { return &___texture_20; }
	inline void set_texture_20(FsmTexture_t1738787045 * value)
	{
		___texture_20 = value;
		Il2CppCodeGenWriteBarrier((&___texture_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMATERIALTEXTURE_T742727606_H
#ifndef SETRANDOMMATERIAL_T3250339026_H
#define SETRANDOMMATERIAL_T3250339026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetRandomMaterial
struct  SetRandomMaterial_t3250339026  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetRandomMaterial::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetRandomMaterial::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.Actions.SetRandomMaterial::materials
	FsmMaterialU5BU5D_t3870809373* ___materials_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetRandomMaterial_t3250339026, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetRandomMaterial_t3250339026, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_materials_18() { return static_cast<int32_t>(offsetof(SetRandomMaterial_t3250339026, ___materials_18)); }
	inline FsmMaterialU5BU5D_t3870809373* get_materials_18() const { return ___materials_18; }
	inline FsmMaterialU5BU5D_t3870809373** get_address_of_materials_18() { return &___materials_18; }
	inline void set_materials_18(FsmMaterialU5BU5D_t3870809373* value)
	{
		___materials_18 = value;
		Il2CppCodeGenWriteBarrier((&___materials_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETRANDOMMATERIAL_T3250339026_H
#ifndef SETSHADOWSTRENGTH_T2605251863_H
#define SETSHADOWSTRENGTH_T2605251863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetShadowStrength
struct  SetShadowStrength_t2605251863  : public ComponentAction_1_t2339132012
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetShadowStrength::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetShadowStrength::shadowStrength
	FsmFloat_t2883254149 * ___shadowStrength_17;
	// System.Boolean HutongGames.PlayMaker.Actions.SetShadowStrength::everyFrame
	bool ___everyFrame_18;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetShadowStrength_t2605251863, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_shadowStrength_17() { return static_cast<int32_t>(offsetof(SetShadowStrength_t2605251863, ___shadowStrength_17)); }
	inline FsmFloat_t2883254149 * get_shadowStrength_17() const { return ___shadowStrength_17; }
	inline FsmFloat_t2883254149 ** get_address_of_shadowStrength_17() { return &___shadowStrength_17; }
	inline void set_shadowStrength_17(FsmFloat_t2883254149 * value)
	{
		___shadowStrength_17 = value;
		Il2CppCodeGenWriteBarrier((&___shadowStrength_17), value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(SetShadowStrength_t2605251863, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETSHADOWSTRENGTH_T2605251863_H
#ifndef SETTEXTUREOFFSET_T3529482419_H
#define SETTEXTUREOFFSET_T3529482419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetTextureOffset
struct  SetTextureOffset_t3529482419  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetTextureOffset::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetTextureOffset::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetTextureOffset::namedTexture
	FsmString_t1785915204 * ___namedTexture_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetTextureOffset::offsetX
	FsmFloat_t2883254149 * ___offsetX_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetTextureOffset::offsetY
	FsmFloat_t2883254149 * ___offsetY_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetTextureOffset::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetTextureOffset_t3529482419, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetTextureOffset_t3529482419, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_namedTexture_18() { return static_cast<int32_t>(offsetof(SetTextureOffset_t3529482419, ___namedTexture_18)); }
	inline FsmString_t1785915204 * get_namedTexture_18() const { return ___namedTexture_18; }
	inline FsmString_t1785915204 ** get_address_of_namedTexture_18() { return &___namedTexture_18; }
	inline void set_namedTexture_18(FsmString_t1785915204 * value)
	{
		___namedTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___namedTexture_18), value);
	}

	inline static int32_t get_offset_of_offsetX_19() { return static_cast<int32_t>(offsetof(SetTextureOffset_t3529482419, ___offsetX_19)); }
	inline FsmFloat_t2883254149 * get_offsetX_19() const { return ___offsetX_19; }
	inline FsmFloat_t2883254149 ** get_address_of_offsetX_19() { return &___offsetX_19; }
	inline void set_offsetX_19(FsmFloat_t2883254149 * value)
	{
		___offsetX_19 = value;
		Il2CppCodeGenWriteBarrier((&___offsetX_19), value);
	}

	inline static int32_t get_offset_of_offsetY_20() { return static_cast<int32_t>(offsetof(SetTextureOffset_t3529482419, ___offsetY_20)); }
	inline FsmFloat_t2883254149 * get_offsetY_20() const { return ___offsetY_20; }
	inline FsmFloat_t2883254149 ** get_address_of_offsetY_20() { return &___offsetY_20; }
	inline void set_offsetY_20(FsmFloat_t2883254149 * value)
	{
		___offsetY_20 = value;
		Il2CppCodeGenWriteBarrier((&___offsetY_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetTextureOffset_t3529482419, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTEXTUREOFFSET_T3529482419_H
#ifndef SETTEXTURESCALE_T1664505880_H
#define SETTEXTURESCALE_T1664505880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetTextureScale
struct  SetTextureScale_t1664505880  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetTextureScale::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetTextureScale::materialIndex
	FsmInt_t874273141 * ___materialIndex_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetTextureScale::namedTexture
	FsmString_t1785915204 * ___namedTexture_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetTextureScale::scaleX
	FsmFloat_t2883254149 * ___scaleX_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetTextureScale::scaleY
	FsmFloat_t2883254149 * ___scaleY_20;
	// System.Boolean HutongGames.PlayMaker.Actions.SetTextureScale::everyFrame
	bool ___everyFrame_21;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetTextureScale_t1664505880, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_materialIndex_17() { return static_cast<int32_t>(offsetof(SetTextureScale_t1664505880, ___materialIndex_17)); }
	inline FsmInt_t874273141 * get_materialIndex_17() const { return ___materialIndex_17; }
	inline FsmInt_t874273141 ** get_address_of_materialIndex_17() { return &___materialIndex_17; }
	inline void set_materialIndex_17(FsmInt_t874273141 * value)
	{
		___materialIndex_17 = value;
		Il2CppCodeGenWriteBarrier((&___materialIndex_17), value);
	}

	inline static int32_t get_offset_of_namedTexture_18() { return static_cast<int32_t>(offsetof(SetTextureScale_t1664505880, ___namedTexture_18)); }
	inline FsmString_t1785915204 * get_namedTexture_18() const { return ___namedTexture_18; }
	inline FsmString_t1785915204 ** get_address_of_namedTexture_18() { return &___namedTexture_18; }
	inline void set_namedTexture_18(FsmString_t1785915204 * value)
	{
		___namedTexture_18 = value;
		Il2CppCodeGenWriteBarrier((&___namedTexture_18), value);
	}

	inline static int32_t get_offset_of_scaleX_19() { return static_cast<int32_t>(offsetof(SetTextureScale_t1664505880, ___scaleX_19)); }
	inline FsmFloat_t2883254149 * get_scaleX_19() const { return ___scaleX_19; }
	inline FsmFloat_t2883254149 ** get_address_of_scaleX_19() { return &___scaleX_19; }
	inline void set_scaleX_19(FsmFloat_t2883254149 * value)
	{
		___scaleX_19 = value;
		Il2CppCodeGenWriteBarrier((&___scaleX_19), value);
	}

	inline static int32_t get_offset_of_scaleY_20() { return static_cast<int32_t>(offsetof(SetTextureScale_t1664505880, ___scaleY_20)); }
	inline FsmFloat_t2883254149 * get_scaleY_20() const { return ___scaleY_20; }
	inline FsmFloat_t2883254149 ** get_address_of_scaleY_20() { return &___scaleY_20; }
	inline void set_scaleY_20(FsmFloat_t2883254149 * value)
	{
		___scaleY_20 = value;
		Il2CppCodeGenWriteBarrier((&___scaleY_20), value);
	}

	inline static int32_t get_offset_of_everyFrame_21() { return static_cast<int32_t>(offsetof(SetTextureScale_t1664505880, ___everyFrame_21)); }
	inline bool get_everyFrame_21() const { return ___everyFrame_21; }
	inline bool* get_address_of_everyFrame_21() { return &___everyFrame_21; }
	inline void set_everyFrame_21(bool value)
	{
		___everyFrame_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTEXTURESCALE_T1664505880_H
#ifndef SETVISIBILITY_T1617017921_H
#define SETVISIBILITY_T1617017921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetVisibility
struct  SetVisibility_t1617017921  : public ComponentAction_1_t1209346957
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetVisibility::gameObject
	FsmOwnerDefault_t3590610434 * ___gameObject_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetVisibility::toggle
	FsmBool_t163807967 * ___toggle_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetVisibility::visible
	FsmBool_t163807967 * ___visible_18;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVisibility::resetOnExit
	bool ___resetOnExit_19;
	// System.Boolean HutongGames.PlayMaker.Actions.SetVisibility::initialVisibility
	bool ___initialVisibility_20;

public:
	inline static int32_t get_offset_of_gameObject_16() { return static_cast<int32_t>(offsetof(SetVisibility_t1617017921, ___gameObject_16)); }
	inline FsmOwnerDefault_t3590610434 * get_gameObject_16() const { return ___gameObject_16; }
	inline FsmOwnerDefault_t3590610434 ** get_address_of_gameObject_16() { return &___gameObject_16; }
	inline void set_gameObject_16(FsmOwnerDefault_t3590610434 * value)
	{
		___gameObject_16 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_16), value);
	}

	inline static int32_t get_offset_of_toggle_17() { return static_cast<int32_t>(offsetof(SetVisibility_t1617017921, ___toggle_17)); }
	inline FsmBool_t163807967 * get_toggle_17() const { return ___toggle_17; }
	inline FsmBool_t163807967 ** get_address_of_toggle_17() { return &___toggle_17; }
	inline void set_toggle_17(FsmBool_t163807967 * value)
	{
		___toggle_17 = value;
		Il2CppCodeGenWriteBarrier((&___toggle_17), value);
	}

	inline static int32_t get_offset_of_visible_18() { return static_cast<int32_t>(offsetof(SetVisibility_t1617017921, ___visible_18)); }
	inline FsmBool_t163807967 * get_visible_18() const { return ___visible_18; }
	inline FsmBool_t163807967 ** get_address_of_visible_18() { return &___visible_18; }
	inline void set_visible_18(FsmBool_t163807967 * value)
	{
		___visible_18 = value;
		Il2CppCodeGenWriteBarrier((&___visible_18), value);
	}

	inline static int32_t get_offset_of_resetOnExit_19() { return static_cast<int32_t>(offsetof(SetVisibility_t1617017921, ___resetOnExit_19)); }
	inline bool get_resetOnExit_19() const { return ___resetOnExit_19; }
	inline bool* get_address_of_resetOnExit_19() { return &___resetOnExit_19; }
	inline void set_resetOnExit_19(bool value)
	{
		___resetOnExit_19 = value;
	}

	inline static int32_t get_offset_of_initialVisibility_20() { return static_cast<int32_t>(offsetof(SetVisibility_t1617017921, ___initialVisibility_20)); }
	inline bool get_initialVisibility_20() const { return ___initialVisibility_20; }
	inline bool* get_address_of_initialVisibility_20() { return &___initialVisibility_20; }
	inline void set_initialVisibility_20(bool value)
	{
		___initialVisibility_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETVISIBILITY_T1617017921_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (GUILayoutEndScrollView_t1918562859), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (GUILayoutEndVertical_t1582804470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (GUILayoutFlexibleSpace_t2475786328), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (GUILayoutFloatField_t805838026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[3] = 
{
	GUILayoutFloatField_t805838026::get_offset_of_floatVariable_16(),
	GUILayoutFloatField_t805838026::get_offset_of_style_17(),
	GUILayoutFloatField_t805838026::get_offset_of_changedEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (GUILayoutFloatLabel_t66359707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[3] = 
{
	GUILayoutFloatLabel_t66359707::get_offset_of_prefix_16(),
	GUILayoutFloatLabel_t66359707::get_offset_of_floatVariable_17(),
	GUILayoutFloatLabel_t66359707::get_offset_of_style_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (GUILayoutHorizontalSlider_t3690373913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3305[4] = 
{
	GUILayoutHorizontalSlider_t3690373913::get_offset_of_floatVariable_16(),
	GUILayoutHorizontalSlider_t3690373913::get_offset_of_leftValue_17(),
	GUILayoutHorizontalSlider_t3690373913::get_offset_of_rightValue_18(),
	GUILayoutHorizontalSlider_t3690373913::get_offset_of_changedEvent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (GUILayoutIntField_t2927046985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[3] = 
{
	GUILayoutIntField_t2927046985::get_offset_of_intVariable_16(),
	GUILayoutIntField_t2927046985::get_offset_of_style_17(),
	GUILayoutIntField_t2927046985::get_offset_of_changedEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (GUILayoutIntLabel_t1298772968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[3] = 
{
	GUILayoutIntLabel_t1298772968::get_offset_of_prefix_16(),
	GUILayoutIntLabel_t1298772968::get_offset_of_intVariable_17(),
	GUILayoutIntLabel_t1298772968::get_offset_of_style_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (GUILayoutLabel_t1180917421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[4] = 
{
	GUILayoutLabel_t1180917421::get_offset_of_image_16(),
	GUILayoutLabel_t1180917421::get_offset_of_text_17(),
	GUILayoutLabel_t1180917421::get_offset_of_tooltip_18(),
	GUILayoutLabel_t1180917421::get_offset_of_style_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (GUILayoutPasswordField_t2608979881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[5] = 
{
	GUILayoutPasswordField_t2608979881::get_offset_of_text_16(),
	GUILayoutPasswordField_t2608979881::get_offset_of_maxLength_17(),
	GUILayoutPasswordField_t2608979881::get_offset_of_style_18(),
	GUILayoutPasswordField_t2608979881::get_offset_of_changedEvent_19(),
	GUILayoutPasswordField_t2608979881::get_offset_of_mask_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (GUILayoutRepeatButton_t642856395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[6] = 
{
	GUILayoutRepeatButton_t642856395::get_offset_of_sendEvent_16(),
	GUILayoutRepeatButton_t642856395::get_offset_of_storeButtonState_17(),
	GUILayoutRepeatButton_t642856395::get_offset_of_image_18(),
	GUILayoutRepeatButton_t642856395::get_offset_of_text_19(),
	GUILayoutRepeatButton_t642856395::get_offset_of_tooltip_20(),
	GUILayoutRepeatButton_t642856395::get_offset_of_style_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (GUILayoutSpace_t330277626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[1] = 
{
	GUILayoutSpace_t330277626::get_offset_of_space_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (GUILayoutTextField_t4093699252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[4] = 
{
	GUILayoutTextField_t4093699252::get_offset_of_text_16(),
	GUILayoutTextField_t4093699252::get_offset_of_maxLength_17(),
	GUILayoutTextField_t4093699252::get_offset_of_style_18(),
	GUILayoutTextField_t4093699252::get_offset_of_changedEvent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (GUILayoutTextLabel_t882428745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[2] = 
{
	GUILayoutTextLabel_t882428745::get_offset_of_text_16(),
	GUILayoutTextLabel_t882428745::get_offset_of_style_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (GUILayoutToggle_t251445422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[6] = 
{
	GUILayoutToggle_t251445422::get_offset_of_storeButtonState_16(),
	GUILayoutToggle_t251445422::get_offset_of_image_17(),
	GUILayoutToggle_t251445422::get_offset_of_text_18(),
	GUILayoutToggle_t251445422::get_offset_of_tooltip_19(),
	GUILayoutToggle_t251445422::get_offset_of_style_20(),
	GUILayoutToggle_t251445422::get_offset_of_changedEvent_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (GUILayoutToolbar_t3789590645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[9] = 
{
	GUILayoutToolbar_t3789590645::get_offset_of_numButtons_16(),
	GUILayoutToolbar_t3789590645::get_offset_of_selectedButton_17(),
	GUILayoutToolbar_t3789590645::get_offset_of_buttonEventsArray_18(),
	GUILayoutToolbar_t3789590645::get_offset_of_imagesArray_19(),
	GUILayoutToolbar_t3789590645::get_offset_of_textsArray_20(),
	GUILayoutToolbar_t3789590645::get_offset_of_tooltipsArray_21(),
	GUILayoutToolbar_t3789590645::get_offset_of_style_22(),
	GUILayoutToolbar_t3789590645::get_offset_of_everyFrame_23(),
	GUILayoutToolbar_t3789590645::get_offset_of_contents_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (GUILayoutVerticalSlider_t1672453969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[4] = 
{
	GUILayoutVerticalSlider_t1672453969::get_offset_of_floatVariable_16(),
	GUILayoutVerticalSlider_t1672453969::get_offset_of_topValue_17(),
	GUILayoutVerticalSlider_t1672453969::get_offset_of_bottomValue_18(),
	GUILayoutVerticalSlider_t1672453969::get_offset_of_changedEvent_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (UseGUILayout_t3165979122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[1] = 
{
	UseGUILayout_t3165979122::get_offset_of_turnOffGUIlayout_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (AnyKey_t3425725034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[1] = 
{
	AnyKey_t3425725034::get_offset_of_sendEvent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (GetAxis_t2827048551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[4] = 
{
	GetAxis_t2827048551::get_offset_of_axisName_14(),
	GetAxis_t2827048551::get_offset_of_multiplier_15(),
	GetAxis_t2827048551::get_offset_of_store_16(),
	GetAxis_t2827048551::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (GetAxisVector_t2984782593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[7] = 
{
	GetAxisVector_t2984782593::get_offset_of_horizontalAxis_14(),
	GetAxisVector_t2984782593::get_offset_of_verticalAxis_15(),
	GetAxisVector_t2984782593::get_offset_of_multiplier_16(),
	GetAxisVector_t2984782593::get_offset_of_mapToPlane_17(),
	GetAxisVector_t2984782593::get_offset_of_relativeTo_18(),
	GetAxisVector_t2984782593::get_offset_of_storeVector_19(),
	GetAxisVector_t2984782593::get_offset_of_storeMagnitude_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (AxisPlane_t3942092997)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3321[4] = 
{
	AxisPlane_t3942092997::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (GetButton_t251209652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3322[3] = 
{
	GetButton_t251209652::get_offset_of_buttonName_14(),
	GetButton_t251209652::get_offset_of_storeResult_15(),
	GetButton_t251209652::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (GetButtonDown_t2627436382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[3] = 
{
	GetButtonDown_t2627436382::get_offset_of_buttonName_14(),
	GetButtonDown_t2627436382::get_offset_of_sendEvent_15(),
	GetButtonDown_t2627436382::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (GetButtonUp_t3558816326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[3] = 
{
	GetButtonUp_t3558816326::get_offset_of_buttonName_14(),
	GetButtonUp_t3558816326::get_offset_of_sendEvent_15(),
	GetButtonUp_t3558816326::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (GetKey_t3009196479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[3] = 
{
	GetKey_t3009196479::get_offset_of_key_14(),
	GetKey_t3009196479::get_offset_of_storeResult_15(),
	GetKey_t3009196479::get_offset_of_everyFrame_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (GetKeyDown_t3975729329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3326[3] = 
{
	GetKeyDown_t3975729329::get_offset_of_key_14(),
	GetKeyDown_t3975729329::get_offset_of_sendEvent_15(),
	GetKeyDown_t3975729329::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (GetKeyUp_t845549446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3327[3] = 
{
	GetKeyUp_t845549446::get_offset_of_key_14(),
	GetKeyUp_t845549446::get_offset_of_sendEvent_15(),
	GetKeyUp_t845549446::get_offset_of_storeResult_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (GetMouseButton_t743773439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3328[2] = 
{
	GetMouseButton_t743773439::get_offset_of_button_14(),
	GetMouseButton_t743773439::get_offset_of_storeResult_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (GetMouseButtonDown_t2597630687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[4] = 
{
	GetMouseButtonDown_t2597630687::get_offset_of_button_14(),
	GetMouseButtonDown_t2597630687::get_offset_of_sendEvent_15(),
	GetMouseButtonDown_t2597630687::get_offset_of_storeResult_16(),
	GetMouseButtonDown_t2597630687::get_offset_of_inUpdateOnly_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (GetMouseButtonUp_t4172049925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[4] = 
{
	GetMouseButtonUp_t4172049925::get_offset_of_button_14(),
	GetMouseButtonUp_t4172049925::get_offset_of_sendEvent_15(),
	GetMouseButtonUp_t4172049925::get_offset_of_storeResult_16(),
	GetMouseButtonUp_t4172049925::get_offset_of_inUpdateOnly_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (GetMouseX_t3553668615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[2] = 
{
	GetMouseX_t3553668615::get_offset_of_storeResult_14(),
	GetMouseX_t3553668615::get_offset_of_normalize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (GetMouseY_t824785260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3332[2] = 
{
	GetMouseY_t824785260::get_offset_of_storeResult_14(),
	GetMouseY_t824785260::get_offset_of_normalize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (MouseLook_t1429053425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[11] = 
{
	MouseLook_t1429053425::get_offset_of_gameObject_14(),
	MouseLook_t1429053425::get_offset_of_axes_15(),
	MouseLook_t1429053425::get_offset_of_sensitivityX_16(),
	MouseLook_t1429053425::get_offset_of_sensitivityY_17(),
	MouseLook_t1429053425::get_offset_of_minimumX_18(),
	MouseLook_t1429053425::get_offset_of_maximumX_19(),
	MouseLook_t1429053425::get_offset_of_minimumY_20(),
	MouseLook_t1429053425::get_offset_of_maximumY_21(),
	MouseLook_t1429053425::get_offset_of_everyFrame_22(),
	MouseLook_t1429053425::get_offset_of_rotationX_23(),
	MouseLook_t1429053425::get_offset_of_rotationY_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (RotationAxes_t1378766905)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3334[4] = 
{
	RotationAxes_t1378766905::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (MouseLook2_t4105412593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[11] = 
{
	MouseLook2_t4105412593::get_offset_of_gameObject_16(),
	MouseLook2_t4105412593::get_offset_of_axes_17(),
	MouseLook2_t4105412593::get_offset_of_sensitivityX_18(),
	MouseLook2_t4105412593::get_offset_of_sensitivityY_19(),
	MouseLook2_t4105412593::get_offset_of_minimumX_20(),
	MouseLook2_t4105412593::get_offset_of_maximumX_21(),
	MouseLook2_t4105412593::get_offset_of_minimumY_22(),
	MouseLook2_t4105412593::get_offset_of_maximumY_23(),
	MouseLook2_t4105412593::get_offset_of_everyFrame_24(),
	MouseLook2_t4105412593::get_offset_of_rotationX_25(),
	MouseLook2_t4105412593::get_offset_of_rotationY_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (RotationAxes_t2375808645)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3336[4] = 
{
	RotationAxes_t2375808645::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (MousePick_t1965400047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[9] = 
{
	MousePick_t1965400047::get_offset_of_rayDistance_14(),
	MousePick_t1965400047::get_offset_of_storeDidPickObject_15(),
	MousePick_t1965400047::get_offset_of_storeGameObject_16(),
	MousePick_t1965400047::get_offset_of_storePoint_17(),
	MousePick_t1965400047::get_offset_of_storeNormal_18(),
	MousePick_t1965400047::get_offset_of_storeDistance_19(),
	MousePick_t1965400047::get_offset_of_layerMask_20(),
	MousePick_t1965400047::get_offset_of_invertMask_21(),
	MousePick_t1965400047::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (MousePickEvent_t4003267355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[9] = 
{
	MousePickEvent_t4003267355::get_offset_of_GameObject_14(),
	MousePickEvent_t4003267355::get_offset_of_rayDistance_15(),
	MousePickEvent_t4003267355::get_offset_of_mouseOver_16(),
	MousePickEvent_t4003267355::get_offset_of_mouseDown_17(),
	MousePickEvent_t4003267355::get_offset_of_mouseUp_18(),
	MousePickEvent_t4003267355::get_offset_of_mouseOff_19(),
	MousePickEvent_t4003267355::get_offset_of_layerMask_20(),
	MousePickEvent_t4003267355::get_offset_of_invertMask_21(),
	MousePickEvent_t4003267355::get_offset_of_everyFrame_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (ResetInputAxes_t1994585160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (ScreenPick_t795325859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3340[13] = 
{
	ScreenPick_t795325859::get_offset_of_screenVector_14(),
	ScreenPick_t795325859::get_offset_of_screenX_15(),
	ScreenPick_t795325859::get_offset_of_screenY_16(),
	ScreenPick_t795325859::get_offset_of_normalized_17(),
	ScreenPick_t795325859::get_offset_of_rayDistance_18(),
	ScreenPick_t795325859::get_offset_of_storeDidPickObject_19(),
	ScreenPick_t795325859::get_offset_of_storeGameObject_20(),
	ScreenPick_t795325859::get_offset_of_storePoint_21(),
	ScreenPick_t795325859::get_offset_of_storeNormal_22(),
	ScreenPick_t795325859::get_offset_of_storeDistance_23(),
	ScreenPick_t795325859::get_offset_of_layerMask_24(),
	ScreenPick_t795325859::get_offset_of_invertMask_25(),
	ScreenPick_t795325859::get_offset_of_everyFrame_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (TransformInputToWorldSpace_t2611876041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[7] = 
{
	TransformInputToWorldSpace_t2611876041::get_offset_of_horizontalInput_14(),
	TransformInputToWorldSpace_t2611876041::get_offset_of_verticalInput_15(),
	TransformInputToWorldSpace_t2611876041::get_offset_of_multiplier_16(),
	TransformInputToWorldSpace_t2611876041::get_offset_of_mapToPlane_17(),
	TransformInputToWorldSpace_t2611876041::get_offset_of_relativeTo_18(),
	TransformInputToWorldSpace_t2611876041::get_offset_of_storeVector_19(),
	TransformInputToWorldSpace_t2611876041::get_offset_of_storeMagnitude_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (AxisPlane_t3471555291)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3342[4] = 
{
	AxisPlane_t3471555291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (DontDestroyOnLoad_t3090985723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[1] = 
{
	DontDestroyOnLoad_t3090985723::get_offset_of_gameObject_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (LoadLevel_t1802188777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[7] = 
{
	LoadLevel_t1802188777::get_offset_of_levelName_14(),
	LoadLevel_t1802188777::get_offset_of_additive_15(),
	LoadLevel_t1802188777::get_offset_of_async_16(),
	LoadLevel_t1802188777::get_offset_of_loadedEvent_17(),
	LoadLevel_t1802188777::get_offset_of_dontDestroyOnLoad_18(),
	LoadLevel_t1802188777::get_offset_of_failedEvent_19(),
	LoadLevel_t1802188777::get_offset_of_asyncOperation_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (LoadLevelNum_t3244234815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[5] = 
{
	LoadLevelNum_t3244234815::get_offset_of_levelIndex_14(),
	LoadLevelNum_t3244234815::get_offset_of_additive_15(),
	LoadLevelNum_t3244234815::get_offset_of_loadedEvent_16(),
	LoadLevelNum_t3244234815::get_offset_of_dontDestroyOnLoad_17(),
	LoadLevelNum_t3244234815::get_offset_of_failedEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (RestartLevel_t2278880271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (SetLightColor_t4240775384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[3] = 
{
	SetLightColor_t4240775384::get_offset_of_gameObject_16(),
	SetLightColor_t4240775384::get_offset_of_lightColor_17(),
	SetLightColor_t4240775384::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (SetLightCookie_t3154943726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[2] = 
{
	SetLightCookie_t3154943726::get_offset_of_gameObject_16(),
	SetLightCookie_t3154943726::get_offset_of_lightCookie_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (SetLightFlare_t2891235521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[2] = 
{
	SetLightFlare_t2891235521::get_offset_of_gameObject_16(),
	SetLightFlare_t2891235521::get_offset_of_lightFlare_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (SetLightIntensity_t1541723886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[3] = 
{
	SetLightIntensity_t1541723886::get_offset_of_gameObject_16(),
	SetLightIntensity_t1541723886::get_offset_of_lightIntensity_17(),
	SetLightIntensity_t1541723886::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (SetLightRange_t1041883866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[3] = 
{
	SetLightRange_t1041883866::get_offset_of_gameObject_16(),
	SetLightRange_t1041883866::get_offset_of_lightRange_17(),
	SetLightRange_t1041883866::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (SetLightSpotAngle_t1865999512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[3] = 
{
	SetLightSpotAngle_t1865999512::get_offset_of_gameObject_16(),
	SetLightSpotAngle_t1865999512::get_offset_of_lightSpotAngle_17(),
	SetLightSpotAngle_t1865999512::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (SetLightType_t1689465828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[2] = 
{
	SetLightType_t1689465828::get_offset_of_gameObject_16(),
	SetLightType_t1689465828::get_offset_of_lightType_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (SetShadowStrength_t2605251863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[3] = 
{
	SetShadowStrength_t2605251863::get_offset_of_gameObject_16(),
	SetShadowStrength_t2605251863::get_offset_of_shadowStrength_17(),
	SetShadowStrength_t2605251863::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (BoolAllTrue_t3292571418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[4] = 
{
	BoolAllTrue_t3292571418::get_offset_of_boolVariables_14(),
	BoolAllTrue_t3292571418::get_offset_of_sendEvent_15(),
	BoolAllTrue_t3292571418::get_offset_of_storeResult_16(),
	BoolAllTrue_t3292571418::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (BoolAnyTrue_t565618083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3356[4] = 
{
	BoolAnyTrue_t565618083::get_offset_of_boolVariables_14(),
	BoolAnyTrue_t565618083::get_offset_of_sendEvent_15(),
	BoolAnyTrue_t565618083::get_offset_of_storeResult_16(),
	BoolAnyTrue_t565618083::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (BoolChanged_t1724066173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3357[4] = 
{
	BoolChanged_t1724066173::get_offset_of_boolVariable_14(),
	BoolChanged_t1724066173::get_offset_of_changedEvent_15(),
	BoolChanged_t1724066173::get_offset_of_storeResult_16(),
	BoolChanged_t1724066173::get_offset_of_previousValue_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (BoolNoneTrue_t3829007969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[4] = 
{
	BoolNoneTrue_t3829007969::get_offset_of_boolVariables_14(),
	BoolNoneTrue_t3829007969::get_offset_of_sendEvent_15(),
	BoolNoneTrue_t3829007969::get_offset_of_storeResult_16(),
	BoolNoneTrue_t3829007969::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (BoolOperator_t2450159816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[5] = 
{
	BoolOperator_t2450159816::get_offset_of_bool1_14(),
	BoolOperator_t2450159816::get_offset_of_bool2_15(),
	BoolOperator_t2450159816::get_offset_of_operation_16(),
	BoolOperator_t2450159816::get_offset_of_storeResult_17(),
	BoolOperator_t2450159816::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (Operation_t1809066441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3360[5] = 
{
	Operation_t1809066441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (BoolTest_t4231152335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[4] = 
{
	BoolTest_t4231152335::get_offset_of_boolVariable_14(),
	BoolTest_t4231152335::get_offset_of_isTrue_15(),
	BoolTest_t4231152335::get_offset_of_isFalse_16(),
	BoolTest_t4231152335::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (ColorCompare_t718482581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[6] = 
{
	ColorCompare_t718482581::get_offset_of_color1_14(),
	ColorCompare_t718482581::get_offset_of_color2_15(),
	ColorCompare_t718482581::get_offset_of_tolerance_16(),
	ColorCompare_t718482581::get_offset_of_equal_17(),
	ColorCompare_t718482581::get_offset_of_notEqual_18(),
	ColorCompare_t718482581::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (EnumCompare_t535967535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[6] = 
{
	EnumCompare_t535967535::get_offset_of_enumVariable_14(),
	EnumCompare_t535967535::get_offset_of_compareTo_15(),
	EnumCompare_t535967535::get_offset_of_equalEvent_16(),
	EnumCompare_t535967535::get_offset_of_notEqualEvent_17(),
	EnumCompare_t535967535::get_offset_of_storeResult_18(),
	EnumCompare_t535967535::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (EnumSwitch_t3155250037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[4] = 
{
	EnumSwitch_t3155250037::get_offset_of_enumVariable_14(),
	EnumSwitch_t3155250037::get_offset_of_compareTo_15(),
	EnumSwitch_t3155250037::get_offset_of_sendEvent_16(),
	EnumSwitch_t3155250037::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (FloatChanged_t3801211219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[4] = 
{
	FloatChanged_t3801211219::get_offset_of_floatVariable_14(),
	FloatChanged_t3801211219::get_offset_of_changedEvent_15(),
	FloatChanged_t3801211219::get_offset_of_storeResult_16(),
	FloatChanged_t3801211219::get_offset_of_previousValue_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (FloatCompare_t2247882859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3366[7] = 
{
	FloatCompare_t2247882859::get_offset_of_float1_14(),
	FloatCompare_t2247882859::get_offset_of_float2_15(),
	FloatCompare_t2247882859::get_offset_of_tolerance_16(),
	FloatCompare_t2247882859::get_offset_of_equal_17(),
	FloatCompare_t2247882859::get_offset_of_lessThan_18(),
	FloatCompare_t2247882859::get_offset_of_greaterThan_19(),
	FloatCompare_t2247882859::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (FloatSignTest_t1608327474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[4] = 
{
	FloatSignTest_t1608327474::get_offset_of_floatValue_14(),
	FloatSignTest_t1608327474::get_offset_of_isPositive_15(),
	FloatSignTest_t1608327474::get_offset_of_isNegative_16(),
	FloatSignTest_t1608327474::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (FloatSwitch_t1681728769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[4] = 
{
	FloatSwitch_t1681728769::get_offset_of_floatVariable_14(),
	FloatSwitch_t1681728769::get_offset_of_lessThan_15(),
	FloatSwitch_t1681728769::get_offset_of_sendEvent_16(),
	FloatSwitch_t1681728769::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (FsmStateSwitch_t1581756733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3369[7] = 
{
	FsmStateSwitch_t1581756733::get_offset_of_gameObject_14(),
	FsmStateSwitch_t1581756733::get_offset_of_fsmName_15(),
	FsmStateSwitch_t1581756733::get_offset_of_compareTo_16(),
	FsmStateSwitch_t1581756733::get_offset_of_sendEvent_17(),
	FsmStateSwitch_t1581756733::get_offset_of_everyFrame_18(),
	FsmStateSwitch_t1581756733::get_offset_of_previousGo_19(),
	FsmStateSwitch_t1581756733::get_offset_of_fsm_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (FsmStateTest_t18088997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[9] = 
{
	FsmStateTest_t18088997::get_offset_of_gameObject_14(),
	FsmStateTest_t18088997::get_offset_of_fsmName_15(),
	FsmStateTest_t18088997::get_offset_of_stateName_16(),
	FsmStateTest_t18088997::get_offset_of_trueEvent_17(),
	FsmStateTest_t18088997::get_offset_of_falseEvent_18(),
	FsmStateTest_t18088997::get_offset_of_storeResult_19(),
	FsmStateTest_t18088997::get_offset_of_everyFrame_20(),
	FsmStateTest_t18088997::get_offset_of_previousGo_21(),
	FsmStateTest_t18088997::get_offset_of_fsm_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (GameObjectChanged_t1134353409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[4] = 
{
	GameObjectChanged_t1134353409::get_offset_of_gameObjectVariable_14(),
	GameObjectChanged_t1134353409::get_offset_of_changedEvent_15(),
	GameObjectChanged_t1134353409::get_offset_of_storeResult_16(),
	GameObjectChanged_t1134353409::get_offset_of_previousValue_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (GameObjectCompare_t368592200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[6] = 
{
	GameObjectCompare_t368592200::get_offset_of_gameObjectVariable_14(),
	GameObjectCompare_t368592200::get_offset_of_compareTo_15(),
	GameObjectCompare_t368592200::get_offset_of_equalEvent_16(),
	GameObjectCompare_t368592200::get_offset_of_notEqualEvent_17(),
	GameObjectCompare_t368592200::get_offset_of_storeResult_18(),
	GameObjectCompare_t368592200::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (GameObjectCompareTag_t973493013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[6] = 
{
	GameObjectCompareTag_t973493013::get_offset_of_gameObject_14(),
	GameObjectCompareTag_t973493013::get_offset_of_tag_15(),
	GameObjectCompareTag_t973493013::get_offset_of_trueEvent_16(),
	GameObjectCompareTag_t973493013::get_offset_of_falseEvent_17(),
	GameObjectCompareTag_t973493013::get_offset_of_storeResult_18(),
	GameObjectCompareTag_t973493013::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (GameObjectHasChildren_t1821724050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[5] = 
{
	GameObjectHasChildren_t1821724050::get_offset_of_gameObject_14(),
	GameObjectHasChildren_t1821724050::get_offset_of_trueEvent_15(),
	GameObjectHasChildren_t1821724050::get_offset_of_falseEvent_16(),
	GameObjectHasChildren_t1821724050::get_offset_of_storeResult_17(),
	GameObjectHasChildren_t1821724050::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (GameObjectIsChildOf_t3428217235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[5] = 
{
	GameObjectIsChildOf_t3428217235::get_offset_of_gameObject_14(),
	GameObjectIsChildOf_t3428217235::get_offset_of_isChildOf_15(),
	GameObjectIsChildOf_t3428217235::get_offset_of_trueEvent_16(),
	GameObjectIsChildOf_t3428217235::get_offset_of_falseEvent_17(),
	GameObjectIsChildOf_t3428217235::get_offset_of_storeResult_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (GameObjectIsNull_t425567640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[5] = 
{
	GameObjectIsNull_t425567640::get_offset_of_gameObject_14(),
	GameObjectIsNull_t425567640::get_offset_of_isNull_15(),
	GameObjectIsNull_t425567640::get_offset_of_isNotNull_16(),
	GameObjectIsNull_t425567640::get_offset_of_storeResult_17(),
	GameObjectIsNull_t425567640::get_offset_of_everyFrame_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (GameObjectIsVisible_t2925201815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[5] = 
{
	GameObjectIsVisible_t2925201815::get_offset_of_gameObject_16(),
	GameObjectIsVisible_t2925201815::get_offset_of_trueEvent_17(),
	GameObjectIsVisible_t2925201815::get_offset_of_falseEvent_18(),
	GameObjectIsVisible_t2925201815::get_offset_of_storeResult_19(),
	GameObjectIsVisible_t2925201815::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (GameObjectTagSwitch_t3080400232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[4] = 
{
	GameObjectTagSwitch_t3080400232::get_offset_of_gameObject_14(),
	GameObjectTagSwitch_t3080400232::get_offset_of_compareTo_15(),
	GameObjectTagSwitch_t3080400232::get_offset_of_sendEvent_16(),
	GameObjectTagSwitch_t3080400232::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (IntChanged_t1248712380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[4] = 
{
	IntChanged_t1248712380::get_offset_of_intVariable_14(),
	IntChanged_t1248712380::get_offset_of_changedEvent_15(),
	IntChanged_t1248712380::get_offset_of_storeResult_16(),
	IntChanged_t1248712380::get_offset_of_previousValue_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (IntCompare_t1091840200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3380[6] = 
{
	IntCompare_t1091840200::get_offset_of_integer1_14(),
	IntCompare_t1091840200::get_offset_of_integer2_15(),
	IntCompare_t1091840200::get_offset_of_equal_16(),
	IntCompare_t1091840200::get_offset_of_lessThan_17(),
	IntCompare_t1091840200::get_offset_of_greaterThan_18(),
	IntCompare_t1091840200::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (IntSwitch_t3633690694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[4] = 
{
	IntSwitch_t3633690694::get_offset_of_intVariable_14(),
	IntSwitch_t3633690694::get_offset_of_compareTo_15(),
	IntSwitch_t3633690694::get_offset_of_sendEvent_16(),
	IntSwitch_t3633690694::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (ObjectCompare_t3988140601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[6] = 
{
	ObjectCompare_t3988140601::get_offset_of_objectVariable_14(),
	ObjectCompare_t3988140601::get_offset_of_compareTo_15(),
	ObjectCompare_t3988140601::get_offset_of_equalEvent_16(),
	ObjectCompare_t3988140601::get_offset_of_notEqualEvent_17(),
	ObjectCompare_t3988140601::get_offset_of_storeResult_18(),
	ObjectCompare_t3988140601::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (StringChanged_t1640859722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[4] = 
{
	StringChanged_t1640859722::get_offset_of_stringVariable_14(),
	StringChanged_t1640859722::get_offset_of_changedEvent_15(),
	StringChanged_t1640859722::get_offset_of_storeResult_16(),
	StringChanged_t1640859722::get_offset_of_previousValue_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (StringCompare_t3732240379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3384[6] = 
{
	StringCompare_t3732240379::get_offset_of_stringVariable_14(),
	StringCompare_t3732240379::get_offset_of_compareTo_15(),
	StringCompare_t3732240379::get_offset_of_equalEvent_16(),
	StringCompare_t3732240379::get_offset_of_notEqualEvent_17(),
	StringCompare_t3732240379::get_offset_of_storeResult_18(),
	StringCompare_t3732240379::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (StringContains_t4036348803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[6] = 
{
	StringContains_t4036348803::get_offset_of_stringVariable_14(),
	StringContains_t4036348803::get_offset_of_containsString_15(),
	StringContains_t4036348803::get_offset_of_trueEvent_16(),
	StringContains_t4036348803::get_offset_of_falseEvent_17(),
	StringContains_t4036348803::get_offset_of_storeResult_18(),
	StringContains_t4036348803::get_offset_of_everyFrame_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (StringSwitch_t3656951454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3386[4] = 
{
	StringSwitch_t3656951454::get_offset_of_stringVariable_14(),
	StringSwitch_t3656951454::get_offset_of_compareTo_15(),
	StringSwitch_t3656951454::get_offset_of_sendEvent_16(),
	StringSwitch_t3656951454::get_offset_of_everyFrame_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (GetMaterial_t2816252130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3387[4] = 
{
	GetMaterial_t2816252130::get_offset_of_gameObject_16(),
	GetMaterial_t2816252130::get_offset_of_materialIndex_17(),
	GetMaterial_t2816252130::get_offset_of_material_18(),
	GetMaterial_t2816252130::get_offset_of_getSharedMaterial_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (GetMaterialTexture_t1739971042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[5] = 
{
	GetMaterialTexture_t1739971042::get_offset_of_gameObject_16(),
	GetMaterialTexture_t1739971042::get_offset_of_materialIndex_17(),
	GetMaterialTexture_t1739971042::get_offset_of_namedTexture_18(),
	GetMaterialTexture_t1739971042::get_offset_of_storedTexture_19(),
	GetMaterialTexture_t1739971042::get_offset_of_getFromSharedMaterial_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (SetMaterial_t3715539006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[3] = 
{
	SetMaterial_t3715539006::get_offset_of_gameObject_16(),
	SetMaterial_t3715539006::get_offset_of_materialIndex_17(),
	SetMaterial_t3715539006::get_offset_of_material_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (SetMaterialColor_t975309120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[6] = 
{
	SetMaterialColor_t975309120::get_offset_of_gameObject_16(),
	SetMaterialColor_t975309120::get_offset_of_materialIndex_17(),
	SetMaterialColor_t975309120::get_offset_of_material_18(),
	SetMaterialColor_t975309120::get_offset_of_namedColor_19(),
	SetMaterialColor_t975309120::get_offset_of_color_20(),
	SetMaterialColor_t975309120::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (SetMaterialFloat_t4126184611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[6] = 
{
	SetMaterialFloat_t4126184611::get_offset_of_gameObject_16(),
	SetMaterialFloat_t4126184611::get_offset_of_materialIndex_17(),
	SetMaterialFloat_t4126184611::get_offset_of_material_18(),
	SetMaterialFloat_t4126184611::get_offset_of_namedFloat_19(),
	SetMaterialFloat_t4126184611::get_offset_of_floatValue_20(),
	SetMaterialFloat_t4126184611::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (SetMaterialTexture_t742727606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[5] = 
{
	SetMaterialTexture_t742727606::get_offset_of_gameObject_16(),
	SetMaterialTexture_t742727606::get_offset_of_materialIndex_17(),
	SetMaterialTexture_t742727606::get_offset_of_material_18(),
	SetMaterialTexture_t742727606::get_offset_of_namedTexture_19(),
	SetMaterialTexture_t742727606::get_offset_of_texture_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (SetRandomMaterial_t3250339026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[3] = 
{
	SetRandomMaterial_t3250339026::get_offset_of_gameObject_16(),
	SetRandomMaterial_t3250339026::get_offset_of_materialIndex_17(),
	SetRandomMaterial_t3250339026::get_offset_of_materials_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (SetTextureOffset_t3529482419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3394[6] = 
{
	SetTextureOffset_t3529482419::get_offset_of_gameObject_16(),
	SetTextureOffset_t3529482419::get_offset_of_materialIndex_17(),
	SetTextureOffset_t3529482419::get_offset_of_namedTexture_18(),
	SetTextureOffset_t3529482419::get_offset_of_offsetX_19(),
	SetTextureOffset_t3529482419::get_offset_of_offsetY_20(),
	SetTextureOffset_t3529482419::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (SetTextureScale_t1664505880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[6] = 
{
	SetTextureScale_t1664505880::get_offset_of_gameObject_16(),
	SetTextureScale_t1664505880::get_offset_of_materialIndex_17(),
	SetTextureScale_t1664505880::get_offset_of_namedTexture_18(),
	SetTextureScale_t1664505880::get_offset_of_scaleX_19(),
	SetTextureScale_t1664505880::get_offset_of_scaleY_20(),
	SetTextureScale_t1664505880::get_offset_of_everyFrame_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (SetVisibility_t1617017921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3396[5] = 
{
	SetVisibility_t1617017921::get_offset_of_gameObject_16(),
	SetVisibility_t1617017921::get_offset_of_toggle_17(),
	SetVisibility_t1617017921::get_offset_of_visible_18(),
	SetVisibility_t1617017921::get_offset_of_resetOnExit_19(),
	SetVisibility_t1617017921::get_offset_of_initialVisibility_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (BoolFlip_t2621011387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[1] = 
{
	BoolFlip_t2621011387::get_offset_of_boolVariable_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (FloatAbs_t875865308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[2] = 
{
	FloatAbs_t875865308::get_offset_of_floatVariable_14(),
	FloatAbs_t875865308::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (FloatAdd_t875275478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[4] = 
{
	FloatAdd_t875275478::get_offset_of_floatVariable_14(),
	FloatAdd_t875275478::get_offset_of_add_15(),
	FloatAdd_t875275478::get_offset_of_everyFrame_16(),
	FloatAdd_t875275478::get_offset_of_perSecond_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
