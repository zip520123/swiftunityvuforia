//
//  UnityVC.swift
//  SlinGoldenBell
//
//  Created by Woody on 2018/5/23.
//  Copyright © 2018年 woody_tsai. All rights reserved.
//

import UIKit

class UnityVC: UIViewController {
    var unityView: UIView?
    override var prefersStatusBarHidden: Bool {return true}
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation { return UIStatusBarAnimation.slide}

    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.startUnity()
        unityView = UnityGetGLView()!
        
        self.view!.addSubview(unityView!)
        unityView!.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["view": unityView]
        let w = NSLayoutConstraint.constraints(withVisualFormat: "|-0-[view]-0-|", options: [], metrics: nil, views: views)
        let h = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: [], metrics: nil, views: views)
        view.addConstraints(w + h)
        
        let backButton = UIButton()

        backButton.setTitle("X", for: .normal)
        backButton.addTarget(self, action: #selector(buttonClick), for: .touchUpInside)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(backButton)
        backButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 48).isActive = true
        backButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 12).isActive = true
        backButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        view.bringSubview(toFront: backButton)
    }
    @objc func buttonClick(){
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    override func viewWillDisappear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.stopUnity()
		
    }
}
