//
//  AppDelegate.swift
//  SwiftUnityVuforia
//
//  Created by Woody on 2018/10/9.
//  Copyright © 2018年 Woody. All rights reserved.
//

import UIKit

@UIApplicationMain
@objc class AppDelegate: UIResponder, UIApplicationDelegate {
	var currentUnityController: UnityAppController!
    var window: UIWindow?
    var isUnityRunning = false
	var application : UIApplication?
	var applaunchOptions : [UIApplicationLaunchOptionsKey: Any]?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.application = application
        self.applaunchOptions = launchOptions
        startUnityApp()
        startUnity()
        perform(#selector(stopUnity), with: nil, afterDelay: 3)
        return true
    }
	
    func applicationWillResignActive(_ application: UIApplication) {
        if isUnityRunning {
            currentUnityController?.applicationWillResignActive(application)
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if isUnityRunning {
            currentUnityController?.applicationDidEnterBackground(application)
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if isUnityRunning {
            currentUnityController?.applicationWillEnterForeground(application)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if isUnityRunning {
            currentUnityController?.applicationDidBecomeActive(application)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        if isUnityRunning {
            currentUnityController?.applicationWillTerminate(application)
        }
    }
    func startUnityApp(){
        unity_init(CommandLine.argc, CommandLine.unsafeArgv)
        currentUnityController = UnityAppController()
        currentUnityController!.application(application!, didFinishLaunchingWithOptions: applaunchOptions)
        
    }
    func startUnity() {
        if !isUnityRunning {
            isUnityRunning = true
            currentUnityController!.applicationDidBecomeActive(application!)
        }
    }
    
    @objc func stopUnity() {
        if isUnityRunning {
            currentUnityController!.applicationWillResignActive(application!)
            isUnityRunning = false
        }
    }

}

